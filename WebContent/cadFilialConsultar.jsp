<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="filialHandler" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<HTML 
xmlns="http://www.w3.org/1999/xhtml" lang="pt">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper(); 
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Consultar Filiais</li> </div> 
						<div id="icone"> 
                           <img src="images/cadFiliais.png" /> 
                        </div> 

				<h:form id="myform" styleClass="niceform" prependId="false" >
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
									<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeFilial">Nome Filial:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.nomeFilial}" id="txtNomeFilial" size="40" maxlength="50" tabindex="1"/>
										</td> 
									</tr> 
									
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeCidade">Cidade:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.cidade.nom_cidade}" id="txtNomeCidade" size="30" tabindex="2" >
												<p:ajax event="keyup" update="txtUf" >
													<f:setPropertyActionListener 
														value="#{filialHandler.limparCidade}" 
														target="#{filialHandler.filial.cidade}"/> 
												</p:ajax>												
											</h:inputText>
										   <h:inputText value="#{filialHandler.filial.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" style="width:30px;" disabled="true" styleClass="textinput" />
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeCidade"
								                    suggestionAction="#{filialHandler.cidadeAutocomplete}" 
								                    var="cidade"
								                    fetchValue="#{cidade.nom_cidade}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" reRender="txtUf"> 
														<f:setPropertyActionListener 
														value="#{cidade}" 
														target="#{filialHandler.filial.cidade}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
						</fieldset>							
							<br />
							<h:commandButton actionListener="#{filialHandler.pesquisaFiliais}" value="Consultar" id="pesquisar" styleClass="button" tabindex="10"/>
							<p:commandButton actionListener="#{filialHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="11">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty filialHandler.resultadoPesquisa}"  constrainToViewport="true" fixedCenter="true" underlay="shadow" > 

									<p:dataTable id="resultTable" value="#{filialHandler.resultadoPesquisa}" 
												 var="item" width="100%"  
												 rendered="#{not empty filialHandler.resultadoPesquisa}" 
												 paginator="true" rows="15" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.nomeFilial}" >
										  <f:facet name="header">
										  <h:outputText value="Nome da Filial" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeFilial}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cidade.nom_cidade}" >
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.cidade.cod_estado.sgl_estado}"  >
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.cod_estado.sgl_estado}" />									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.telefone}" >
										  <f:facet name="header">
										  <h:outputText value="Telefone" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.telefone}"/>		
										</p:column>	
										<p:column resizable="true" sortBy="#{item.fax}" >
										  <f:facet name="header">
										  <h:outputText value="Fax" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.fax}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.email}" >
										  <f:facet name="header">
										  <h:outputText value="Email" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.email}"/>									  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  action="#{filialHandler.preparaAlterarFilial}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:setPropertyActionListener target="#{filialHandler.filial}" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{filialHandler.excluirFilial}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirFilial" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>		
									</p:dataTable>
<div align="right">
						<h:commandLink >  
						    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
						    <p:dataExporter type="xls" target="resultTable" fileName="Filiais"  postProcessor="#{filialHandler.postProcessXLS}" excludeColumns="6,7"/>  
						</h:commandLink>
</div>
</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>


</BODY>

</HTML>
</f:view>
