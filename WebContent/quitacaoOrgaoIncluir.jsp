<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="quitacaoOrgaoHandler" />

<HTML xmlns="http://www.w3.org/1999/xhtml" >
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery("#periodoQuitacao").mask("99/9999",{placeholder:"_"});
});

function trataSelecao(){
	if(jQuery('#marcarDesmarcarTodos').is(':checked')){
		jQuery(".selectable-checkbox").attr("checked", true);
	}else{
		jQuery(".selectable-checkbox").attr("checked", false);
	}
	
}

</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Incluir Quita��o �rg�o</li> </div>
						<div id="icone">
                           <img src="images/empresa.png" />
                        </div>
						<h:form prependId="false">
						<div class="titulo_agrupamento_Dados" ></div>
						<br/>
						<div class="divRecuadoEsquerdo">
							<table>
								<tr>
									<td align="left" width="20px;">
									  <h:outputText
											value="M�s/Ano:"
											id="txtTotalPremios"
											style="font-size: 14px;font-weight: bold;">
										</h:outputText>
									</td>
									<td>
										<h:inputText value="#{quitacaoOrgaoHandler.periodoQuitacao}"  id="periodoQuitacao" maxlength="10" size="12"  tabindex="6"/>
									</td>
									<td>
										<h:commandButton actionListener="#{quitacaoOrgaoHandler.buscarDados}" value="Buscar Dados" 
												id="botaoBuscar" styleClass="button" tabindex="3"
										/>
									</td>
								</tr>
								
							</table>
							
							
						</div>
						
							<br />
							<br/>
							<div id="divOrgaoQuitacao" >
								<h:selectBooleanCheckbox id="marcarDesmarcarTodos" onclick="trataSelecao();"  rendered="#{quitacaoOrgaoHandler.exibirGravar}"/>
							   <h:outputLabel value="Marcar/Desmarcar todos" for="marcarDesmarcarTodos" rendered="#{quitacaoOrgaoHandler.exibirGravar}"/> 
							<br/><br/>
								<p:dataTable id="resultTable" value="#{quitacaoOrgaoHandler.listaOrgaoQuitacao}" 
												 var="item" width="100%" 
												 rendered="#{not empty quitacaoOrgaoHandler.listaOrgaoQuitacao}" 
												dynamic="true"
												 >
										
										<p:column resizable="true" sortBy="#{item.orgao.nomeOrgao}" >
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
										</p:column>
										<p:column resizable="true" >
										  <f:facet name="header">
										     <h:outputText value="#{quitacaoOrgaoHandler.periodoQuitacao}" /> 										  	
										  </f:facet>
										  <h:selectBooleanCheckbox styleClass="selectable-checkbox" value="#{item.quitado}" />							  										
										</p:column>	
																																																					
									</p:dataTable>
								</div>
							<br/>
							<p:commandButton actionListener="#{quitacaoOrgaoHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" rendered="#{quitacaoOrgaoHandler.exibirGravar}" ajax="false" tabindex="3" />
							<p:commandButton actionListener="#{quitacaoOrgaoHandler.excluirDados}" value="Excluir" id="cancelar" styleClass="button" async="false" ajax="false" rendered="#{quitacaoOrgaoHandler.podeExibirExcluir}" tabindex="20">
								<p:confirmDialog message="Deseja realmente apagar essa inclus�o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>	
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
