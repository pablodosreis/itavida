<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="funcionarioHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery('#txtNome').focus();
	jQuery("#txtCpf").mask("999.999.999-99",{placeholder:"_"});
	jQuery("#txtCep").mask("99.999-999",{placeholder:"_"});
	jQuery("#txtTelefone").mask("(99)9?9999-9999",{placeholder:"_"});
	jQuery("#txtCelular").mask("(99)9?9999-9999",{placeholder:"_"});
	jQuery("#txtFax").mask("(99)9999-9999",{placeholder:"_"});
	jQuery("#dtDataAdmissao").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtDataDesligamento").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtDataNascimento").mask("99/99/9999",{placeholder:"_"});
	   
	});

</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Funcion�rios</li> </div>
						<div id="icone">
                           <img src="images/cadFuncionarios .png" />
                        </div>

				<h:form id="myform" styleClass="niceform" prependId="false" >
					<h:panelGrid id="cpfMessage">
											<p:dialog header="MENSAGEM" widgetVar="dialog"  
											               fixedCenter="true" modal="true" visible="#{funcionarioHandler.visibleMessage}" >
											   <h:messages errorClass="mensagem_erro" fatalClass="mensagem_erro" infoClass="mensagem_sucesso" warnClass="mensagem_alerta"/>                 
											</p:dialog>
					</h:panelGrid>
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
									<div class="divRecuadoDireito">
										<table> 
											<tr> 
												<td align="left" width="90px;"> 
												   <label for="cmbTipoFuncionario">Tipo de Funcionario:</label> 
												</td> 
											</tr>
											<tr>
												<td align="right">
												   <h:selectOneMenu id="cmbTipoFuncionario" binding="#{funcionarioHandler.tipoFuncionarioSelecionado}" value="#{funcionarioHandler.funcionario.tipoFuncionario}" tabindex="3">
												   	<f:selectItems value="#{funcionarioHandler.tipoFuncionarioCombo}"/>
												   </h:selectOneMenu>
												</td> 
		
											</tr> 
										</table>
									</div>
									<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeFilial">Nome:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{funcionarioHandler.funcionario.nome}" id="txtNome" size="40" maxlength="50" tabindex="1"/>
										</td> 
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtCpf">CPF:</label> 
										</td> 
										<td> 

										   <h:inputText value="#{funcionarioHandler.funcionario.cpf}" id="txtCpf" size="20" maxlength="15" tabindex="2" >
											  <p:ajax actionListener="#{funcionarioHandler.verificaCPF}" event="blur" update="cpfMessage" async="true" />
											</h:inputText>
										</td> 
									</tr> 
								</table>
								<table>			
									<tr>
										<td align="right" width="60px;">
										  <label for="dtDataNascimento" >Data Nascimento:</label>
										</td>
										<td>
											<h:inputText value="#{funcionarioHandler.funcionario.dataNascimento}"  id="dtDataNascimento" maxlength="10" tabindex="4">
											<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
											<p:ajax actionListener="#{funcionarioHandler.calculaIdadeFuncionario}" update="txtIdadeFuncionario" event="blur" async="true"/>
											</h:inputText>
										</td>
										<td>
											 <h:outputText id="txtIdadeFuncionario" value="#{funcionarioHandler.idadeFuncionario}" />
										</td>
									</tr>
								</table>
								<table>								
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
									<tr> 
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtEndereco">Endere�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{funcionarioHandler.funcionario.endereco}" id="txtEndereco" size="40" maxlength="110" tabindex="5"/>
										</td>								
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtBairro">Bairro:</label> 
										</td> 
										<td> 
											<h:inputText value="#{funcionarioHandler.funcionario.bairro}" id="txtBairro" size="40" maxlength="50" tabindex="6"/>
										</td>								
									</tr>										
								</table>	
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeCidade">Cidade:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{funcionarioHandler.funcionario.cidade.nom_cidade}" id="txtNomeCidade" size="30" tabindex="7" >
												<p:ajax event="keyup" update="txtUf" >
													<f:setPropertyActionListener 
														value="#{funcionarioHandler.limparCidade}" 
														target="#{funcionarioHandler.funcionario.cidade}"/> 
												</p:ajax>												
											</h:inputText>
										   <h:inputText value="#{funcionarioHandler.funcionario.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" style="width:30px;" disabled="true" styleClass="textinput" />
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeCidade"
								                    suggestionAction="#{funcionarioHandler.cidadeAutocomplete}" 
								                    var="cidade"
								                    fetchValue="#{cidade.nom_cidade}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" reRender="txtUf"> 
														<f:setPropertyActionListener 
														value="#{cidade}" 
														target="#{funcionarioHandler.funcionario.cidade}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtCep">CEP:</label> 
										</td> 
										<td> 
											<h:inputText value="#{funcionarioHandler.funcionario.cep}" id="txtCep" size="10" maxlength="10" tabindex="8"/>											
										</td> 
									</tr> 
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
									<tr> 
									</tr> 
									<tr> 
										<td align="right" width="40px;"> 
										  <label for="txtTelefone">Telefone:</label> 
										</td> 
										<td> 
									    <h:inputText value="#{funcionarioHandler.funcionario.telefone}" id="txtTelefone" size="12" maxlength="15" tabindex="9"/>	
										</td>
										<td align="right" width="60px;"> 
										  <label for="txtCelular">Celular:</label> 
										</td> 
										<td> 
											<h:inputText value="#{funcionarioHandler.funcionario.celular}" id="txtCelular" size="12" maxlength="15" tabindex="10"/>
										</td>	
										<td align="right" width="40px;"> 
										  <label for="txtFax">Fax:</label> 
										</td> 
										<td> 
											<h:inputText value="#{funcionarioHandler.funcionario.fax}" id="txtFax" size="12" maxlength="15" tabindex="11"/>
										</td>											
									</tr> 
								</table> 
								<br/><br/>
<div class="divRecuadoDireito">
								<table>
									<tr>
										<td align="right" width="140px;">
										  <label for="dtDataAdmissao" >Data Admiss�o:</label>
										</td>
										<td>
											<h:inputText value="#{funcionarioHandler.funcionario.dataAdmissao}"  id="dtDataAdmissao" maxlength="10" tabindex="12">
											<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="140px;">
										  <label for="dtDataDesligamento" >Data Desligamento:</label>
										</td>
										<td>
											<h:inputText value="#{funcionarioHandler.funcionario.dataDesligamento}"  id="dtDataDesligamento" maxlength="10" tabindex="13">
											<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>										
									</tr>	
									<tr>
										<td align="right" width="140px;">
										  <label for="txtPercentual" >Percentual de Comiss�o:</label>
										</td>
										<td>
										   <h:inputText value="#{funcionarioHandler.funcionario.percentualComissao}" id="txtPercentual" size="5" maxlength="5" tabindex="14"/>
										</td>
									</tr>
								</table>
</div>									
							</fieldset>	
							<fieldset>
								<legend>Dados Banc�rios</legend>
								<table>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtBanco" >Banco:</label>
										</td>
										<td>
											<h:inputText value="#{funcionarioHandler.funcionario.banco}" id="txtBanco" size="15" maxlength="20" tabindex="15"/>
										</td>
									</tr>
									<tr>	
										<td align="right" width="90px;">
										  <label for="txtAgencia" >Ag�ncia:</label>
										</td>
										<td>
										   <h:inputText value="#{funcionarioHandler.funcionario.agencia}" id="txtAgencia" size="10" maxlength="15" tabindex="16"/>
										</td>										
									</tr>		
									<tr>	
										<td align="right" width="90px;">
										  <label for="txtConta" >Conta:</label>
										</td>
										<td>
										   <h:inputText value="#{funcionarioHandler.funcionario.conta}" id="txtConta" size="10" maxlength="15" tabindex="17"/>
										</td>										
									</tr>										
								</table>	
							</fieldset>						
							<br />
							<p:commandButton actionListener="#{funcionarioHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="18"/>
							<p:commandButton actionListener="#{funcionarioHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="20">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
