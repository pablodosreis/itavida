<%@ include file="/common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="dashBoardHandler" />
<a4j:keepAlive beanName="contasVencerHandler" />
<a4j:keepAlive beanName="controleApoliceHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<h:panelGroup id="mensagens" rendered="#{controleApoliceHandler.exibirAlertaAniversarioApolice eq true}">
	<div style="background-color: #f9edbe; border-color=#f0c36d; text-align: center;
	border:thin solid;" class="invisivelImpressao">
		 <h:outputText value="#{controleApoliceHandler.mensagemAlertaAniversarioApolice}"/> 
		 <h:outputLink value="#{facesContext.externalContext.requestContextPath}/cadEmpresasConsultar.jsf">
		 Cadastro de Empresas
		 </h:outputLink> 
	</div>
</h:panelGroup>
<jsp:include page="common/head.jsp" />
<BODY class=f-default>
<style>
.fontBlack {
    FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;
}
.fontBlue {
	FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #005580;
}

<style type="text/css">
			.pf-dashboard-column {width:400px; height: 800px;}
		
			.pf-panel {
				margin: 10px;
			}
		
			.pf-panel-hd,  .pf-panel-bd,  .pf-panel-ft {
				border-color:#000000;
			}
			
			 .pf-panel-bd {
				background: #333333;
				color:#CCCCCC;
			}
			
			 .pf-panel-toggler-expanded,  .pf-panel-toggler-collapsed{
				top:3px;
				width:16px;
				height:16px;
			}
			
			 .pf-panel-closer {
				width: 16px;
				height: 16px;
			}
			
			 .pf-panel-hd-controls {
				top:3px;
			}
</style>
		<script type="text/javascript">
			var chartStyle = {
				padding:20,				
				legend: {
					display:"right",
					spacing:10
				}			
			};

		</script>
<p:resources exclude="/jquery/jquery.js" />
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Gest�o</span><li>DASHBOARD</li> </div>
						<div id="icone">
                           <img src="images/dashboard.png" />
                        </div>
						<h:form styleClass="niceform" prependId="false">

					<div class="titulo_agrupamento_Dados" ></div>
<p:growl id="growl" showDetail="true" />  




<br/>

<p>Ol� <h:outputText value="#{usuarioHandler.usuario.nomeUsuario}" />!</p>

<a style="color: black;" href="${facesContext.externalContext.request.contextPath}/propostaConsultar.jsf?fluxo=consulta">Consultar Propostas</a>


	


<h:panelGroup id="panelespaco" rendered="#{usuarioHandler.usuario.perfil eq 'FIL'}">
<br/>
<br/>
<br/>
<br/>
</h:panelGroup>

<h:panelGroup id="panelGraficos" rendered="#{usuarioHandler.usuario.perfil eq 'FIN' || usuarioHandler.usuario.perfil eq 'ADM' || usuarioHandler.usuario.perfil eq 'COM'}">
 
<!--  Parte dos gr�ficos -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="button.css" />

        <script type="text/javascript">
        jQuery(function(jQuery){
	        //jQuery("#dtInicio").mask("99/99/9999",{placeholder:"_"});
			//jQuery("#dtFim").mask("99/99/9999",{placeholder:"_"});
        });

            var dados = [
                        ['Jan', 60 , 5 , 5 , 30 , 40694.00, 30 , 18, 12],
                        ['Fev', 40 , 6 , 5 , 30 , 40, 35 , 33, 12],
                        ['Mar', 50 , 7 , 5 , 30 , 40, 36 , 52, 12],
                        ['Abr', 90 , 7 , 5 , 30 , 40, 37 , 70, 12],
                        ['Mai', 80 , 0, 5 , 30 , 40 , 38 , 50, 12],
                        ['Jun', 40 , 2, 5 , 30 , 40 , 39 , 60, 12],
                        ['Jul', 40 , 8, 5 , 30 , 40 , 30 , 50, 12],
                        ['Ago', 30 , 9, 5 , 30 , 40 , 49 , 10, 12],
                        ['Set', 30 , 10, 5 , 30 , 40, 42 , 54, 12],
                        ['Out', 49 , 3, 5 , 30 , 40 , 54 , 41, 12],
                        ['Nov', 59 , 1, 5 , 30 , 40 , 30 , 90, 12],
                        ['Dez', 68 , 20, 5 , 30 , 40, 31 , 48, 12 ]
                    ];
            

            $(document).ready(function(){
                $("#graph").click(function(){  

                    var vetor = {};
                    $("#grafico input").each(function(index){ 

                        if($(this).is(':checked')){                        
                            vetor[index] = $(this).attr('id');
                        }
                   });
                   drawChart(vetor);
                });
            });

            google.charts.load('current', {'packages':['corechart']});

          

            function drawNow() {
            	$("#graph").click();
            }
            

            function drawChart(vet){
            	
            	var formatter = new google.visualization.NumberFormat({decimalSymbol: ',',groupingSymbol: '.', negativeColor: 'red', negativeParens: true, prefix: ''});
            	
            	dados = eval($('#dadosGrafico').val());
            	
                var elemento = new Array();
                
                var QtdColunasValores = 0;                

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'M�s');

                $.each(vet, function(indice, valor) {
                    if(valor == "media_idade"){
                        data.addColumn('number', 'Media de Idade');
                        QtdColunasValores++;
                    }if(valor == "total_premio"){
                       data.addColumn('number', 'Total de Pr�mios');
                       QtdColunasValores++;
                    }if(valor == "total_capital"){
                        data.addColumn('number', 'Total do Capital');
                        QtdColunasValores++;
                    }if(valor == "total_segurados"){
                        data.addColumn('number', 'Quantidade de Segurados');
                        QtdColunasValores++;
                    }if(valor == "total_orgaos"){
                       data.addColumn('number', 'Quantidade de Org�os');
                       QtdColunasValores++;
                    }if(valor == "total_sinistro"){
                       data.addColumn('number', 'Total de Sinistros');
                       QtdColunasValores++;
                    }if(valor == "total_homens"){
                        data.addColumn('number', 'Total de Homens');
                        QtdColunasValores++;
                    }if(valor == "total_mulheres"){
                         data.addColumn('number', 'Total de Mulheres');
                         QtdColunasValores++;
                    }if(valor == "premio_segurado"){
                        data.addColumn('number', 'Total Pr�mio Segurados');
                        QtdColunasValores++;
                   }if(valor == "premio_conjuge"){
                       data.addColumn('number', 'Total Pr�mio C�njuge');
                       QtdColunasValores++;
	                  }if(valor == "valor_sinistros"){
	                      data.addColumn('number', 'Valor Pago Sinistros');
	                      QtdColunasValores++;
	                 }
                    
                });
               

                $.each(dados, function(indice, valor){ 
                    var indice = 0;
                    var indiceEntry = 0                            
                    elemento[indice++] = valor[indiceEntry];
                    indiceEntry++;              
		
                    if(vet[0] == "media_idade"){                        
                        elemento[indice++] = valor[indiceEntry];
                    }
                     indiceEntry++;
                    if(vet[1] == "total_premio"){                        
                        elemento[indice++] = valor[indiceEntry];
                    }
                     indiceEntry++;
                    if(vet[2]  == "total_capital"){                            
                            elemento[indice++] = valor[indiceEntry];
                    }
                     indiceEntry++;
                    if(vet[3]  == "total_segurados"){                           
                            elemento[indice++] = valor[indiceEntry];
                    }
                     indiceEntry++;
                    if(vet[4]  == "total_orgaos"){                            
                            elemento[indice++] = valor[indiceEntry];
                    }
                     indiceEntry++;
                    if(vet[5]  == "total_sinistro"){                            
                            elemento[indice++] = valor[indiceEntry];
                    }
                     indiceEntry++;
                    if(vet[6]  == "total_homens"){                           
                            elemento[indice++] = valor[indiceEntry];
                    }
                    indiceEntry++;
                    if(vet[7] == "total_mulheres"){                           
                            elemento[indice++] = valor[indiceEntry];
                    }
                    indiceEntry++;
                    if(vet[8] == "premio_segurado"){                           
                          elemento[indice++] = valor[indiceEntry];
                    }
                    indiceEntry++;
                    if(vet[9] == "premio_conjuge"){                           
                          elemento[indice++] = valor[indiceEntry];
                    }
                    indiceEntry++;
                    if(vet[10] == "valor_sinistros"){                           
                          elemento[indice++] = valor[indiceEntry];
                    }
                    data.addRow(elemento);
                 });


             // Set chart options
               var options = {
                   'title':'Temporal',
                   'width':800,
                   'height':600,
                   //'hAxis' : {viewWindow:{max: 5}},
                   
                    explorer: { actions: ['dragToZoom', 'rightClickToReset'] }	
                };
             
               console.log("Quantidade Colunas de Valores: " + QtdColunasValores);
                for (i = 1; i <= QtdColunasValores; i++) { 
                	formatter.format(data, i);
            	}
                
                  	 

                $("#tipo_grafico input").each(function(index)
                		 {     
                		    if($(this).is(':checked'))
                		    {                        
                		         if($(this).attr('id')=="linha")
                		         {
                		             console.log($(this).attr('id'));
                		            
                		                var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                		                chart.draw(data, options);
                		         }
                		          if($(this).attr('id')=="barra")
                		         {
                		               var view = new google.visualization.DataView(data);
                		                
                		      
                		        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

                		        chart.draw(view, options);
                		             
                		            
                		         }
                		    } 
                		 });

            }
            
            google.charts.setOnLoadCallback(drawNow);


        </script>

<br/><br/>
      <div id="chart_div"></div>
    
    <div id="graficoPeriodo">
    	<label>In�cio</label>
    	<h:inputText value="#{dashBoardHandler.inicio}"  id="dtInicio" maxlength="10" size="12" >
			<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
		</h:inputText>
		
		<label>Fim</label>
    	<h:inputText value="#{dashBoardHandler.fim}"  id="dtFim" maxlength="10" size="12" >
			<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
		</h:inputText>
    </div>  
	<div id="grafico">
			<h:selectBooleanCheckbox id="media_idade" value="#{dashBoardHandler.media_idade}" /><label for="media_idade">Media de Idade</label>
            
            
            <h:selectBooleanCheckbox id="total_premio"   value="#{dashBoardHandler.total_premio}" onchange="drawNow()" /><label for="total_premio">Total de Pr�mios</label>
            <h:selectBooleanCheckbox id="total_capital"   value="#{dashBoardHandler.total_capital}" onchange="drawNow()" /><label for="total_capital">Total do Capital</label>
            <h:selectBooleanCheckbox id="total_segurados"  value="#{dashBoardHandler.total_segurados}" onchange="drawNow()" /><label for="total_segurados">Qtd de Segurados</label>
            <h:selectBooleanCheckbox id="total_orgaos"    value="#{dashBoardHandler.total_orgaos}" onchange="drawNow()" /><label for="total_orgaso">Qtd de �rg�os</label>
            <br/>
            <h:selectBooleanCheckbox id="total_sinistro"  value="#{dashBoardHandler.total_sinistro}" onchange="drawNow()" /><label for="total_sinistro">Qtd de Sinistros</label>
            <h:selectBooleanCheckbox id="total_homens"    value="#{dashBoardHandler.total_homens}" onchange="drawNow()" /><label  for="total_homens">Qtd de Homens</label>
            <h:selectBooleanCheckbox id="total_mulheres"  value="#{dashBoardHandler.total_mulheres}" onchange="drawNow()" /><label for="total_mulheres">Qtd de Mulheres</label>
            <h:selectBooleanCheckbox id="premio_segurado"  value="#{dashBoardHandler.premio_segurado}" onchange="drawNow()" /><label for="premio_segurado">Pr�mio Segurados</label>
            <h:selectBooleanCheckbox id="premio_conjuge"  value="#{dashBoardHandler.premio_conjuge}" onchange="drawNow()" /><label for="premio_conjuge">Pr�mio C�njuge</label>
            <h:selectBooleanCheckbox id="valor_sinistros"  value="#{dashBoardHandler.valor_sinistros}" onchange="drawNow()" /><label for="valor_sinistros">Valor Pago Sinistros</label>
            
            
            <h:inputHidden id="dadosGrafico" value="#{dashBoardHandler.dataHolder.graficoConteudo}"/>
      </div>
	<br/>
	<br/>
	
	<div id="tipo_grafico">
        <input id="linha"  name="tg"   type="radio" checked="checked"  onClick="drawNow()">Grafico Linha<br>
        <input id="barra" name="tg"   type="radio"  onClick="drawNow()" >Grafico Barra<br>

    </div>
	<br/><br/>
	
  	    <span id="graph"><p:commandButton actionListener="#{dashBoardHandler.atualizarDadosGrafico}" value="Atualizar dados do Gr�fico" id="Visualizar" styleClass="button" async="false" ajax="false" /></span>
		

    
          
<!-- Fim dos gr�ficos -->

</h:panelGroup>




<!--<p:dashboard model="#{dashBoardHandler.model}"    
    reorderListener="#{dashBoardHandler.handleReorder}" onReorderUpdate="growl" rendered="#{usuarioHandler.usuario.perfil ne 'FIN' || usuarioHandler.usuario.perfil eq 'ADM'}" >  
  
    <p:panel id="propostas" header="Dados dos Segurados"  closable="true"               
            widgetVar="propostasWidget"  >  
			<h:panelGrid columns="2">
        		<h:outputText value="Total de Segurados:" styleClass="fontBlack" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalPropostas}" styleClass="fontBlack"/>
				<h:outputText value="Total de Segurados Ativos:" styleClass="fontBlue" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalPropostasAtivas}" styleClass="fontBlue"/>
				<h:outputText value="Total de Segurados Cancelados:" styleClass="fontBlack" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalPropostasCanceladas}" styleClass="fontBlack" />
				<h:outputText value="Total de Segurados Sem Averba��o:" styleClass="fontBlue"/>
				<h:outputText value="#{dashBoardHandler.dataHolder.totalPropostasSemAverbacao}" styleClass="fontBlue"/>
				
			</h:panelGrid>  
    </p:panel>  
    
    <p:panel id="orgaos" header="Dados dos �rg�os" closable="true"  
            widgetVar="orgaosWidget">  
			<h:panelGrid columns="2">
        		<h:outputText value="Total de �rg�os:" styleClass="fontBlack" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalOrgaos}" styleClass="fontBlack"/>
        		<h:outputText value="Total de �rg�os Estaduais:" styleClass="fontBlue" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalOrgaosEstaduais}" styleClass="fontBlue"/>
        		<h:outputText value="Total de �rg�os Federais:" styleClass="fontBlack" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalOrgaosFederais}" styleClass="fontBlack"/>
        		<h:outputText value="Total de �rg�os Municiais:" styleClass="fontBlue" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalOrgaosMunicipais}" styleClass="fontBlue"/>
        		<h:outputText value="Total de �rg�os Privados:" styleClass="fontBlack" />
				<h:outputText value="#{dashBoardHandler.dataHolder.totalOrgaosPrivados}" styleClass="fontBlack"/>
			</h:panelGrid>       
    </p:panel> 
          
    <p:panel id="sinistros" header="Dados de Sinistros" closable="true"  
            widgetVar="sinistrosWidget" rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}">  
			<h:panelGrid columns="2">
				<h:outputText value="Total de Sinistros:" styleClass="fontBlack"/>
				<h:outputText value="#{dashBoardHandler.dataHolder.totalSinistros}" styleClass="fontBlack"/>
				<h:outputText value="No. de Sinistros (�ltimos 30 dias):" styleClass="fontBlue"/>
				<h:outputText value="#{dashBoardHandler.dataHolder.totalSinistros30dias}" styleClass="fontBlue"/>
			</h:panelGrid>       
    </p:panel>

    <p:panel id="graficoSexo" header="Estat�stica Segurados (Sexo)" closable="true"  
            widgetVar="estatistSexoWidget" >  
		<p:pieChart value="#{dashBoardHandler.listaGraficoSexo}" var="segurado" categoryField="#{segurado.serie}"  
		       				dataField="#{segurado.valor}" style="chartStyle" height="140px" width="220px" />  
    </p:panel>

    <p:panel id="graficoPlano" header="Estat�stica Segurados (Plano)" closable="true"  
            widgetVar="estatistSexoWidget" >  
		<p:pieChart value="#{dashBoardHandler.listaGraficoPlanos}" var="plano" categoryField="#{plano.serie}"  
		       				dataField="#{plano.valor}" style="chartStyle" height="140px" width="220px" />  
    </p:panel>

</p:dashboard> -->  
 
<h:panelGroup rendered="false">
							<fieldset>
								<legend>Contas Vencidas ou A Vencer em: <h:outputText value="#{dashBoardHandler.dataAtual}" style="COLOR: #AC2D2D;" ><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt" /></h:outputText> </legend>


									<p:dataTable value="#{dashBoardHandler.listaContasVencidasOuAVencer}" 
												 var="item" width="100%" 
												 rendered="#{not empty dashBoardHandler.listaContasVencidasOuAVencer }" >
										
		<p:column resizable="true" sortBy="#{item.filial.nomeFilial}" >
										  <f:facet name="header">
										  <h:outputText value="Filial" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.filial.nomeFilial}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.despesa.nomeDespesa}">
										  <f:facet name="header">
										  <h:outputText value="Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.despesa.nomeDespesa}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.grupoDespesa.nomeGrupoDespesa}">
										  <f:facet name="header">
										  <h:outputText value="Grupo de Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.grupoDespesa.nomeGrupoDespesa}"/>									  										
										</p:column>	
										<p:column resizable="true"  sortBy="#{item.funcionario.nome}" >
										  <f:facet name="header">
										  <h:outputText value="Funcion�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.funcionario.nome}"/>									  										
										</p:column>	

										<p:column resizable="true" sortBy="#{item.valorDespesa}" >
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  </f:facet>
										  <h:outputText value="#{item.valorDespesa}" >
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>	
										   </h:outputText>								  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.dataVencimento}">
										  <f:facet name="header">
											  <h:outputText value="Data Vencimento" />
										  </f:facet>
										  <h:outputText value="#{item.dataVencimento}" >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />			
										   </h:outputText>
										</p:column>			

									</p:dataTable>
							</fieldset>
</h:panelGroup>
							</h:form>
							
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
