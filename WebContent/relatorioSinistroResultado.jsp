<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="historicoSinistroHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class="popUp">
<jsp:include page="common/relatorioHeader.jsp" />
<style type="text/css">
.fontBlack {
    FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #000;
}
</style>
<h:form styleClass="niceform" prependId="false">
			<table class="cabecalho_relatorio" >
				<tr>
					<td><IMG title="Slogan" alt="Slogan" src="images/logoRelatorio.png" border=0 /></td>
					<td width="200px;">Relat�rio de Sinistros</td>
					<td>Data:</td>
					<td width="500px;"> <h:outputText value="#{historicoSinistroHandler.dataAtualRelatorio}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm"  /></h:outputText>
					<td class="invisivelImpressao">
						<h:commandLink onclick="javascript:window.print();">  
						    <p:graphicImage value="images/impressora.png" alt="Imprimir" title="Imprimir"/>  
						</h:commandLink>
					</td>

				<tr>
			</table>
<a4j:repeat value="#{historicoSinistroHandler.listaHistoricoSinistro}" var="itemHistorico">

<fieldset style="width: 1000px;" >
	<legend>Proposta: <h:outputText value="#{itemHistorico.proposta.id}"/></legend>
	<h:panelGrid columns="3">
		<h:panelGrid columns="2" width="300">
			<h:outputText value="N�mero do Sinistro:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.numeroSinistro}"/>	
			<h:outputText value="Data do Sinistro:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.dataSinistro}" ><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  /></h:outputText>
			<h:outputText value="Data do Envio p/ Seguradora:" styleClass="fontBlack"/>
			<h:outputText value="#{itemHistorico.dataEnvioSeguradora}" ><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  /></h:outputText>
	 		<h:outputText value="Data do Pagamento:" styleClass="fontBlack"/>
			<h:outputText value="#{itemHistorico.dataPagamento}" ><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  /></h:outputText>
			<h:outputText value="Ap�lice:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.empresa.nomeEmpresa}"/>

		</h:panelGrid>
		<h:panelGrid columns="2" width="450" >
			<h:outputText value="Nome Segurado Principal:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.proposta.nomeSegurado}"/>	
			<h:outputText value="Nome Segurado C�njuge:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.proposta.nomeConjuge}"/>	
	 		<h:outputText value="Motivo do Sinistro:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.motivoSinistro.descricao}"/>	
			<h:outputText value="Motivo do Sinistro Negado:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.motivoSinistroNegado.descricao}"/>
			<h:outputText value="�rg�o:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.proposta.orgao.siglaOrgao}"/>
			

		</h:panelGrid>
		<h:panelGrid columns="2" width="250"  >
			<h:outputText value="Capital Funcion�rio:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.proposta.capitalSegurado}"><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>	
			<h:outputText value="Capital C�njuge:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.proposta.capitalConjuge}"><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
			<h:outputText value="Valor Pago Sinistro:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.valorSinistro == null ? '-' : itemHistorico.valorSinistro}"><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
			<h:outputText value="Valor Pago Cesta B�sica:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.valorCestaBasica == null ? '-' : itemHistorico.valorCestaBasica}"><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
			<h:outputText value="Valor Pago Funeral:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.valorFuneral  == null ? '-' : itemHistorico.valorFuneral}"><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
			<h:outputText value="Seguradora:" styleClass="fontBlack"/> 
			<h:outputText rendered="#{itemHistorico.seguradora == null or itemHistorico.seguradora == ''}" value="-" escape="false"/>
			<h:outputText rendered="#{itemHistorico.seguradora == 'T'}" value="TOKIO MARINE" escape="false"/>
			<h:outputText rendered="#{itemHistorico.seguradora == 'M'}" value="METLIFE" escape="false"/>
			<h:outputText rendered="#{itemHistorico.seguradora == 'I'}" value="ITA� SEGUROS" escape="false"/>
			<h:outputText rendered="#{itemHistorico.seguradora == 'O'}" value="ICATU SEGUROS" escape="false"/>
		</h:panelGrid>
	</h:panelGrid>
	
	<h:panelGrid columns="1" >
		<f:verbatim rendered="#{not empty itemHistorico.beneficiarios}">  
			<table width="100%">
				<tr align="left">
					<th class="fontBlack">Benefici�rio</th>
					<th class="fontBlack" style="padding-left: 3.5em">Valor Recebido</th>
				</tr>
				<a4j:repeat value="#{itemHistorico.beneficiarios}" var="item">
						<tr align="left">
						<td>	
				  	   	<h:outputText value="#{item.nomeBeneficiario}" style="text-transform:uppercase;"/>
				  	   	</td>
				  	   	<td style="padding-left: 4em">
				  	    		<h:outputText value="#{item.valorRecebido}" >
							  	   		<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
							  	   </h:outputText>
					    </td>
						</tr>
				</a4j:repeat>
			</table>
		</f:verbatim>	
	</h:panelGrid>
	
	<h:panelGrid columns="2">			
		<h:panelGrid columns="1">
			<h:outputText value="Situa��o do Processo:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.situacaoProcessoString}" escape="false"/>	
		</h:panelGrid>
	
		<h:panelGrid columns="1" style="padding-left: 4em">
			<h:outputText value="Observa��es:" styleClass="fontBlack"/> 
			<h:outputText value="#{itemHistorico.observacaoString}" escape="false"/>
		</h:panelGrid>
		
	</h:panelGrid>
		
	
</fieldset>
								
</a4j:repeat>
						</h:form>
</BODY>
</HTML>
</f:view>
