<%@ include file="common/cabecalho.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<f:view>

<a4j:keepAlive beanName="seguradoHandler" />

 
<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
	<script>
		jQuery(function(jQuery) {
			jQuery('input').bestupper();
			jQuery("#inicioAniversario").mask("99/9999",{placeholder:"_"});
			jQuery("#fimAniversario").mask("99/9999",{placeholder:"_"});
			jQuery("#dtAverbacao").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtCancelamentoInicio").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtCancelamentoFim").mask("99/99/9999", {
				placeholder : "_"
			});
		});

	</script>
	<DIV id=background_2>
		<DIV id=site_wrapper>
			<jsp:include page="common/menu.jsp" />
			<DIV id=wrap>
				<DIV id=content>
					<DIV id=main>
						<DIV id=main_content>
							<DIV id=main_body>

								<div id="breadcrumb">
									<span>Relat�rios</span>
									<li>Segurados por Empresa</li>
								</div>
								<div id="icone">
									<img src="images/empresa.png" />
								</div>
								<h:form id="myform" styleClass="niceform" prependId="false">


									<div class="titulo_agrupamento_Dados"></div>
									<fieldset>
										<legend>Filtros</legend>
											<table>
											<tr>
												<td align="right"><label for="cmbEmpresa">Empresa:</label>
												</td>
												<td>
													<h:selectOneMenu id="cmbEmpresa"
														value="#{seguradoHandler.empresaSelected}">
														<f:selectItems
															value="#{seguradoHandler.opcoesEmpresaCombo}" />
													</h:selectOneMenu>
												</td>
											</tr>
											<tr>
												<td align="right"><label for="cmbModelo">Modelo Proposta:</label>
												</td>
												<td>
													<h:selectOneMenu id="cmbModelo"
														value="#{seguradoHandler.modeloPropostaSelected}">
														<f:selectItems
															value="#{seguradoHandler.modeloPropostaCombo}" />
													</h:selectOneMenu>
												</td>
											</tr>
											<tr>										
												<td align="right" width="150px;"> 
												   <label for="cmbUf">UF:</label> 
												</td> 
												<td>
												   <h:selectOneMenu id="cmbUf" value="#{seguradoHandler.ufSelected}">
												   		<f:selectItems value="#{seguradoHandler.opcoesUfCombo}" />
												   		<a4j:support actionListener="#{seguradoHandler.atualizarCombo}" reRender="cmbOrgao"  event="onchange"/>
												   </h:selectOneMenu>
												   <p:commandButton actionListener="#{seguradoHandler.atualizarCombo}"
														value="Atualizar �rg�os" id="atualizar" async="false" ajax="false"/>
												</td> 											
											</tr>
											<tr>										
												<td align="right" width="150px;"> 
												   <label for="cmbOrgao">�rg�o:</label> 
												</td> 
												<td>
												   <h:selectOneMenu id="cmbOrgao" value="#{seguradoHandler.orgaoSelected}">
												   		<f:selectItems value="#{seguradoHandler.opcoesOrgaoCombo}" />
												   </h:selectOneMenu>
												</td> 											
											</tr>
											<tr> 
												<td align="right" width="150px;"> 
												   <label for="cmbTipoOrgao">Tipo de �rg�o:</label> 
												</td> 
												<td>
												   <h:selectOneMenu id="cmbTipoOrgao" value="#{seguradoHandler.tipoOrgaoSelected}">
												   	<f:selectItems value="#{seguradoHandler.opcoesTipoOrgaoCombo}"/>
												   </h:selectOneMenu>
												</td> 
		
											</tr> 
											<tr> 
												<td align="right" width="90px;"> 
												  <label for="txtAgenciador">Agenciador:</label> 
												</td> 
												<td> 
												   <h:inputText value="#{seguradoHandler.agenciador.nome}" id="txtAgenciador" size="40"/>
														<rich:suggestionbox id="suggestionBoxFuncionario" 
										                	for="txtAgenciador"
										                    suggestionAction="#{seguradoHandler.funcionarioAutoComplete}" 
										                    var="funcionario"
										                    fetchValue="#{funcionario.nome}"
															bypassUpdates="true"
										                    width="500"
										                    height="250"
										                    cellpadding="4"
		                    								nothingLabel="Nenhum Item Encontrado!">
										                    										                    
															<a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
																<f:setPropertyActionListener 
																value="#{funcionario}" 
																target="#{seguradoHandler.agenciador}"/> 
															</a4j:support> 
										                    <h:column>
										                       <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/> 
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.tipoFuncionarioDescricao}" styleClass="autocompleteFonte"/>
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.percentualComissao}" styleClass="autocompleteFonte"/>
										                    </h:column>
										             </rich:suggestionbox>
												</td> 
											</tr>
											<tr> 
												<td align="right" width="90px;"> 
												  <label for="txtProfissao">Profiss�o:</label> 
												</td> 
												<td> 
												   <h:inputText value="#{seguradoHandler.profissao}" id="txtProfissao" size="40" />
												</td> 
											</tr>
											
											<tr>
												<td align="right" width="150px;"> 
												   <label for="mesAniversario">Anivers�rio (m�s):</label> 
												</td>
												<td width="50px;">
													<h:selectOneMenu id="mesAniversario" value="#{seguradoHandler.mesAniversario}">
												   		<f:selectItems value="#{seguradoHandler.opcoesMesesCombo}" />
												   </h:selectOneMenu>
												</td>
											</tr>
											
											
											<tr>
												<td align="right" width="150px;"> 
												   <label for="dtAverbacao">Data de Averba��o:</label> 
												</td>
												<td>
													<h:inputText value="#{seguradoHandler.dataAverbacao}"
																id="dtAverbacao" size="15" maxlength="15" label="Data de averba��o">
																<f:convertDateTime dateStyle="default"
																	pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
											</tr>
											
											<tr>
												<td align="right" width="150px;"> 
												   <label for="dtCancelamentoInicio">Data de Cancelamento(in�cio):</label> 
												</td>
												<td>
													<h:inputText value="#{seguradoHandler.dataCancelamentoInicio}"
																id="dtCancelamentoInicio" size="15" maxlength="15" label="Data de Cancelamento (in�cio)">
																<f:convertDateTime dateStyle="default"
																	pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
											</tr>
											
											<tr>
												<td align="right" width="150px;"> 
												   <label for="dtCancelamentoFim">Data de Cancelamento(fim):</label> 
												</td>
												<td>
													<h:inputText value="#{seguradoHandler.dataCancelamentoFim}"
																id="dtCancelamentoFim" size="15" maxlength="15" label="Data de Cancelamento (fim)">
																<f:convertDateTime dateStyle="default"
																	pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
											</tr>
											
											<tr style="white-space: nowrap">										
												<td align="right" width="150px;"> 
												   <label for="cmbTipoConsulta">Situa��o da Proposta:</label> 
												</td> 
												<td>
													<table border="0">
														<tr>
														<td>
															<h:selectManyCheckbox id="cmbTipoConsulta" value="#{seguradoHandler.situacaoPropostaSelectedList}">
																<f:selectItems value="#{seguradoHandler.opcoesSituacaoPropostaCombo}" />
															</h:selectManyCheckbox>
													   	</td>
													   	</tr>
												   </table>
												</td>
											</tr> 
										</table>
										
									</fieldset>

									<br />
									<p:commandButton action="#{seguradoHandler.gerarRelatorio}"
										value="Arquivo Seguradora" id="gravar" styleClass="button"
										async="false" ajax="false"  />
									<p:commandButton action="#{seguradoHandler.gerarRelatorioSimplificado}"
										value="Rela��o dos Segurados" id="profissao" styleClass="button"
										async="false" ajax="false" />
									<p:commandButton action="#{seguradoHandler.gerarRelacaoCorreios}"
										value="Rela��o para os Correios" id="correios" styleClass="button"
										async="false" ajax="false" />
									<p:commandButton actionListener="#{seguradoHandler.cancelar}"
										value="Limpar Tela" id="cancelar" styleClass="button"
										async="false" ajax="false" />
									<p:confirmDialog message="Deseja Cancelar esta opera��o?"
										yesLabel="Sim" noLabel="N�o" header="Confirma��o!"
										fixedCenter="true" modal="true" />
								</h:form>

							</DIV>
						</DIV>
						<DIV class=both>
							<!-- -->
						</DIV>
					</DIV>
					<DIV id=footer_top_bg>
						<!-- -->
					</DIV>
				</DIV>
			</DIV>
			<jsp:include page="common/footer.jsp" />
		</DIV>
		<DIV id=foo_glass></DIV>
		<BR> <BR>
	</DIV>
</BODY>

	</HTML>
</f:view>
