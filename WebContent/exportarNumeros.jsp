<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="exportarArquivoMensalHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class=f-default>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Arquivos </span><li>Exportar Solicita��o de N�meros de Sorteios </li> </div>
						<div id="icone">
                           <img src="images/exportNumeroSorteio.png" />
                        </div>

						<h:form  prependId="false" >
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>

<br/><br/><br/>

								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="cmbEmpresa">Empresa:</label>
										</td>
										<td>
										   <h:selectOneMenu id="cmbEmpresa" value="#{exportarArquivoMensalHandler.empresaSelected}"  tabindex="1">
										   		<f:selectItems value="#{exportarArquivoMensalHandler.opcoesEmpresaCombo}" />
										   </h:selectOneMenu>
										</td>								
									</tr>																		
								</table>


<h:selectOneRadio value="#{exportarArquivoMensalHandler.radioSelected}" border="0"  layout="pageDirection" >  
    <f:selectItem itemLabel="Exportar Segurados Ativos Sem N�mero de Sorteio" itemValue="1" />  
    <f:selectItem itemLabel="Exportar Todos Segurados Sem N�mero de Sorteio" itemValue="2" />  
	<f:selectItem itemLabel="Exportar Todos Segurados ( Com ou Sem N�meros de Sorteio)" itemValue="3" />  
	<f:selectItem itemLabel="Exportar Todos Segurados ( Com ou Sem N�meros de Sorteio) Somente Ativos" itemValue="4" />  
</h:selectOneRadio>



						<br /><br/><br/>
							<h:commandButton action="#{exportarArquivoMensalHandler.exportReport}" value="Realizar Exporta��o" id="gerarExport" styleClass="button" tabindex="2"/>
							<p:commandButton actionListener="#{exportarArquivoMensalHandler.limparDados}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="3">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
