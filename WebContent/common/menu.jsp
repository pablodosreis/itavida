
<%@ page language="java" session="true" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f"  uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h"  uri="http://java.sun.com/jsf/html"%>
<%@ taglib uri="http://primefaces.prime.com.tr/ui" prefix="p" %> 
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<f:subview id="menuSubView">
<p:resources exclude="/jquery/jquery.js"/>
<rich:jQuery id="enterTabInput" selector="input" timing="onload" 
                 query="keydown(function(e){if (e.keyCode == 13){ 
			tabindex = jQuery(this).attr('tabindex');
		  tabindex++;
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('textarea[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                            return false;} 
                                            })" /> 
<rich:jQuery id="enterTabSelect" selector="select" timing="onload" 
                 query="keydown(function(e){if (e.keyCode == 13){ 
			tabindex = jQuery(this).attr('tabindex');
		  tabindex++;
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                                    return false;} 
                                            })" />


<h:panelGroup id="mensagens"> 
<p:dialog header="MENSAGEM" id="dialogMensagens" widgetVar="dialog"  
               fixedCenter="true" modal="true" visible="#{facesContext.maximumSeverity != null}"  zindex="10000">
   <h:messages errorClass="mensagem_erro" fatalClass="mensagem_erro" infoClass="mensagem_sucesso" warnClass="mensagem_alerta"/>                 
</p:dialog>
<p:dialog header="MENSAGEM" id="dialogMensagensImport" widgetVar="dialog"  
               fixedCenter="true" modal="true" visible="#{usuarioHandler.mensagemRetorno != null}"  zindex="10000">
   	<h:outputText escape="false" styleClass="mensagem_sucesso" value="#{usuarioHandler.mensagemRetorno}"/>
</p:dialog>
</h:panelGroup>

<DIV class=top_menu>
	<DIV id=top_wrap>
<h:form>
	<SPAN class=pathway>
	<h:outputLink value="#{facesContext.externalContext.requestContextPath}/home.jsf"> 
		<img src="<%= request.getContextPath() %>/images/piramideItavida.png" style="height: 22px;" />
	</h:outputLink> 	 
	Itavida Seguros | SISVIDA WEB
	</SPAN>
<p:idleMonitor timeout="#{usuarioHandler.tempoSessao}" update="panelGrowl"  
                idleListener="#{usuarioHandler.idleListener}" onidle="pollLogoutWidget.start();" /> 
<h:panelGroup id="panelGrowl">
			    <p:growl id="ideMonitorGrowl"  showDetail="true" sticky="true" globalOnly="false"  rendered="#{usuarioHandler.timeOver eq true}" />
				 <p:poll id="pollLogout" interval="3" 
				            actionListener="#{usuarioHandler.logout}" async="false" autoStart="false" widgetVar="pollLogoutWidget"/>    
</h:panelGroup>
		<DIV id=pathway ><STRONG> Usu�rio Logado:</STRONG> <SPAN class=pathway><h:outputText value="#{usuarioHandler.usuario.nomeUsuario}"/> </SPAN>
		  <a style="color: red;" href="${facesContext.externalContext.request.contextPath}/logout.jsp">SAIR</a>

		</DIV>
		</h:form>
	</DIV>
	
	<DIV id=nav>

<DIV style="width:880px; HEIGHT: 50px" >
		<p:menubar autoSubmenuDisplay="false" effect="NONE" >  

		    <p:submenu label="Cadastro"  rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}">				      
				 <p:submenu label="Cidades"  >
					<p:menuitem label="Consultar" url="/cadCidadesConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadCidadesIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Ap�lices"  >
					<p:menuitem label="Consultar" url="/cadEmpresasConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadEmpresasIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Reajuste por Faixa Et�ria"  >
					<p:menuitem label="Consultar" url="/cadFatorConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadFatorIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Despesas"  rendered="#{(usuarioHandler.usuario.perfil eq 'FIN' || usuarioHandler.usuario.perfil eq 'ADM') && usuarioHandler.usuario.perfil ne 'FIL'}">
					<p:menuitem label="Consultar" url="/cadDespesasConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadDespesasIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Filiais"  >
					<p:menuitem label="Consultar" url="/cadFilialConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadFilialIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Funcionarios"  >
					<p:menuitem label="Consultar" url="/cadFuncionariosConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadFuncionariosIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Grupo de Despesas"  rendered="#{usuarioHandler.usuario.perfil eq 'FIN' || usuarioHandler.usuario.perfil eq 'ADM'}">
					<p:menuitem label="Consultar" url="/cadGruposDespesasConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadGruposDespesasIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Motivo Sinistro"  >
					<p:menuitem label="Consultar" url="/cadTipoOcorrenciaConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadTipoOcorrenciaIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Nome da Tabela"  >
					<p:menuitem label="Consultar" url="/cadNomeTabelaConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadNomeTabelaIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="�rg�o"  >
					<p:menuitem label="Consultar" url="/cadOrgaoConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadOrgaoIncluir.jsf" />
					<p:menuitem label="Relat�rio" url="/relatorioOrgaos.jsf?fluxo=consulta"/>
				 </p:submenu>
				 <p:submenu label="Tabela Capital Segurado ( Faixa Et�ria )"  >
					<p:menuitem label="Consultar" url="/cadCapitalSeguradoFaixaEtariaConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadCapitalSeguradoFaixaEtariaIncluir.jsf" />
				 </p:submenu>
				 <p:submenu label="Tabela Capital Segurado ( Limite Idade )"  >
					<p:menuitem label="Consultar" url="/cadCapitalSeguradoLimiteIdadeConsultar.jsf" />
					<p:menuitem label="Incluir" url="/cadCapitalSeguradoLimiteIdadeIncluir.jsf" />
				 </p:submenu>
		    </p:submenu>  
		              
		    <p:submenu label="Gest�o"  >
		    				       
				<p:menuitem label="Consultar Proposta" url="/propostaConsultar.jsf?fluxo=consulta" />
				<p:menuitem label="Incluir Proposta" url="/propostaIncluir.jsf?fluxo=incluir" rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}"/>
				<p:menuitem label="Incluir Super Ap" url="/superApIncluir.jsf?fluxo=incluirSuperAp" rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}"/>
				<p:menuitem label="Consultar Sinistro" url="/sinistroConsultar.jsf" />
				<p:submenu label="Gest�o SIAPE (D8)"  rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}">
					<p:menuitem label="Importa��o Siape (Arquivo D8)" url="/importarSiape.jsf" />
					<p:menuitem label="Visualizar Arquivo D8 Importado" url="/visualizarSiape.jsf" />
					<p:menuitem label="Realizar Confer�ncia de Per�odos" url="/realizarConferenciaSiape.jsf" />
					<p:menuitem label="Cadastrar Novo �rg�o Siape" url="/cadOrgaoSiape.jsf" />
				</p:submenu>
				<p:menuitem style="display: none" label="Nova Proposta (Incluir)" url="/proposta/propostaBase.jsf" rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}"/>-->
		    </p:submenu> 
		    <p:submenu label="Movimenta��o"  rendered="#{usuarioHandler.usuario.perfil eq 'FIN' || usuarioHandler.usuario.perfil eq 'ADM' || usuarioHandler.usuario.perfil eq 'CAD'}">
		    	 <p:menuitem label="Movimenta��o Super AP" url="/MovimentacaoSuperAp.jsf" />				       
				 <p:menuitem label="Movimenta��o Proposta" url="/MovimentacaoProposta.jsf" />
				 <p:menuitem label="Movimenta��o Org�o" url="/MovimentacaoOrgao.jsf" />
				 <p:menuitem label="Arquivo Movimenta��o" url="/geracaoArquivoMensal.jsf" />
		    </p:submenu> 
		    <p:submenu label="Controle Comercial"  rendered="#{usuarioHandler.usuario.perfil eq 'COM' || usuarioHandler.usuario.perfil eq 'ADM'}" >
		    	<p:menuitem label="Movimenta��o Super AP" url="/MovimentacaoSuperAp.jsf" />				       
		    </p:submenu> 
		    <p:submenu label="Faturamento"  rendered="#{usuarioHandler.usuario.perfil eq 'FAT' || usuarioHandler.usuario.perfil eq 'ADM'}">				       
				<p:menuitem label="Consultar Planilha de Faturamento" url="/planilhaFaturamentoConsultar.jsf?fluxo=consulta" />
				<p:menuitem label="Incluir Planilha de Faturamento" url="/planilhaFaturamentoIncluir.jsf?fluxo=incluir" />
		    </p:submenu> 
		    <p:submenu label="Relat�rios" >
		    	<p:menuitem label="Relat�rio de Quita��o" url="/relatorioMensalPagamentos.jsf?fluxo=consulta" />
		    	<p:menuitem label="Relat�rio de Agenciadores" url="/relatorioAgenciadorFiltro.jsf?fluxo=consulta" />
				<p:menuitem label="Relat�rio de Controle de Vendas" url="/relatorioControleComercialFiltro.jsf?fluxo=consulta" rendered="#{usuarioHandler.usuario.perfil eq 'COM' || usuarioHandler.usuario.perfil eq 'ADM'}"/>
				<p:menuitem label="Relat�rio de Faturamento" url="/relatorioFaturamentoFiltro.jsf?fluxo=consulta" rendered="#{usuarioHandler.usuario.perfil eq 'FAT' || usuarioHandler.usuario.perfil eq 'ADM'}"/>
				<p:menuitem label="Relat�rio Financeiro" url="/relatorioPagamentoFiltro.jsf?fluxo=consulta" rendered="#{usuarioHandler.usuario.perfil eq 'FIN' || usuarioHandler.usuario.perfil eq 'ADM'}"/>
				<p:menuitem label="Relat�rio de Segurados" url="/relatorioPropostaFiltro.jsf?fluxo=consulta" />
				<p:menuitem label="Relat�rio de Segurados por Empresa" url="/relatorioSeguradosPorEmpresa.jsf?fluxo=consulta"/>
				<p:menuitem label="Relat�rio de Sinistros" url="/relatorioSinistroFiltro.jsf?fluxo=consulta" />
				<p:menuitem label="Relat�rio de �rg�os" url="/relatorioOrgaos.jsf?fluxo=consulta"/>
				<p:menuitem label="Relat�rio de Movimenta��o Super AP" url="/relatorioMovimentacaoSuperAp.jsf?fluxo=consulta"/>
				<p:menuitem label="Relat�rio de Extrato Super AP" url="/relatorioExtratoSuperAp.jsf?fluxo=consulta"/>
				<p:menuitem label="Mala Direta de Segurados" url="/malaDiretaSegurados.jsf?fluxo=consulta" rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}" />
				<p:menuitem label="Mala Direta de Funcion�rios" url="/malaDiretaFuncionarios.jsf?fluxo=consulta" rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}" />
				<p:menuitem label="Mala Direta de �rg�os" url="/malaDiretaOrgaos.jsf?fluxo=consulta" rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}" />

		    </p:submenu> 
		    <p:submenu label="Arquivos"  rendered="#{usuarioHandler.usuario.perfil eq 'CAD' || usuarioHandler.usuario.perfil eq 'ADM'}">				       
				<p:menuitem label="Importa��o Mensal de Pagamentos" url="/importarPagamentoMensal.jsf" />
				<p:menuitem label="Importa��o de Novos Valores" url="/importarNovosValores.jsf" />
				<p:menuitem label="Importa��o de N�meros de Sorteio" url="/importarNumeros.jsf" />
				<p:menuitem label="Importa��o de Quita��o" url="/importarQuitacao.jsf" />
				
				<p:menuitem label="Exportar Solicita��o Capitaliza��o" url="/exportarNumeros.jsf" />
				<p:menuitem label="Exportar Rela��o Segurados" url="/exportarArquivoMensal.jsf" />
				<p:menuitem label="Exportar Base Dados P/ T�kio Marine" url="/exportarArquivoTokio.jsf" />
				
		    </p:submenu> 
		    <p:submenu label="Administra��o"  rendered="#{usuarioHandler.usuario.perfil eq 'ADM'}" >				       
				<p:menuitem label="Gerenciar Permiss�es" url="/controleAcessoConsultar.jsf"  />
		    </p:submenu> 
		</p:menubar>
		</DIV>
<h:panelGroup rendered="#{usuarioHandler.usuario.perfil ne 'FIL'}">
		<DIV id=searchform>
			<h:form>

				<DIV class=search>
					<h:inputText styleClass="inputbox"  id="mod_search_searchword" onblur="if(this.value=='') this.value='Buscar Proposta...';" 
						   onfocus="if(this.value=='Buscar Proposta...' || this.value=='BUSCAR PROPOSTA...') this.value='';" alt="Buscar Proposta" maxlength="20" 
						   binding="#{usuarioHandler.inputText}"						   						   
						   value="Buscar Proposta..."  >
							<rich:jQuery id="serchWord" selector=".inputbox" timing="onload" 
                 						 query="keydown(function(e){if (e.keyCode == 13){ 
											goProposta();
                                               return false;} 
                                            })" />

							<a4j:jsFunction action="#{usuarioHandler.goProposta}"  name="goProposta"  />
					</h:inputText>

				</DIV>
			</h:form>
		</DIV>
</h:panelGroup>
	</DIV>

</DIV>
</f:subview>