<%@ page language="java" session="true" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f"  uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h"  uri="http://java.sun.com/jsf/html"%>
<%@ taglib uri="http://primefaces.prime.com.tr/ui" prefix="p" %> 

<f:subview id="menuSubView">
<p:resources />

<p:dialog header="MENSAGEM" id="dialog1" widgetVar="dialog"  
               fixedCenter="true" modal="true" visible="#{facesContext.maximumSeverity != null}" >
   <h:messages errorClass="mensagem_erro" fatalClass="mensagem_erro" infoClass="mensagem_sucesso" warnClass="mensagem_alerta"/>                 
</p:dialog>
</f:subview>