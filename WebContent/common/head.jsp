<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib prefix="h"  uri="http://java.sun.com/jsf/html"%>

<span id="ctx" style="display:none;"><%=request.getContextPath()%></span>

<HEAD>

<jsp:include page="title.jsp" />
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">


<!--[if lte IE 6]>

<SCRIPT src="/js/pngfix.js" defer type=text/javascript></SCRIPT>
<![endif]-->


<link rel="icon" type="image/png" href="<%= request.getContextPath() %>/images/piramideItavida.png">
<LINK media=all href="<%= request.getContextPath() %>/css/template_css.css" type=text/css rel=stylesheet>
<LINK media=all href="<%= request.getContextPath() %>/css/tables-styles.css" type=text/css rel=stylesheet>
<LINK media=all href="<%= request.getContextPath() %>/css/jquery-ui-1.7.2.custom.css" type=text/css rel=stylesheet>
<LINK media="print, handheld"  href="<%= request.getContextPath() %>/css/impressao.css" rel="stylesheet" type="text/css" >

<style type="text/css" media="screen">@import url(<%= request.getContextPath() %>/css/niceforms-default.css);</style>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/niceforms.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/itavida.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/mascaras.js"></script>

<a4j:loadScript src="/js/jquery-ui-1.7.2.custom.min.js" />
<a4j:loadScript src="/js/ui.datepicker-pt-BR.js" />
<a4j:loadScript src="/js/jquery.maskedinput-1.2.2.js" />
<a4j:loadScript src="/js/jquery.price_format.1.3.js" />
<a4j:loadScript src="/js/jquery.bestupper.min.js" />

</HEAD>