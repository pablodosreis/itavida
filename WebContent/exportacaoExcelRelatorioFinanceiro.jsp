<%@ page language="java" contentType="application/vnd.ms-excel;charset=iso-8859-1" %>
<%@ taglib prefix="f"  uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h"  uri="http://java.sun.com/jsf/html"%>
<%@ taglib uri="http://primefaces.prime.com.tr/ui" prefix="p" %> 
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="pagamentoHandler" />
<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<HEAD>

<TITLE>SISVIDA WEB 1.0</TITLE>
<META http-equiv=Content-Type content="application/vnd.ms-excel; charset=iso-8859-1">
<!--[if lte IE 6]>

<SCRIPT src="js/pngfix.js" defer type=text/javascript></SCRIPT>
<![endif]-->


</HEAD>
<BODY class="popUp">

<h:form prependId="false">
 			<rich:dataTable 
                    onRowMouseOver="this.style.backgroundColor='#F1F1F1'"
                    onRowMouseOut="this.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'"
                    cellpadding="0" cellspacing="0" 
                    width="700" border="1" var="chave" value="#{pagamentoHandler.mapKeys}" sortMode="single" >
                
                        <f:facet name="header">
                        <rich:columnGroup >
                            <rich:column rowspan="2"  >
                                <h:outputText value="Data:" />
                            </rich:column>
                            <rich:column colspan="6">
                                <h:outputText value="Dados do Documento" />
                            </rich:column>
                            <rich:column breakBefore="true">
                                <h:outputText value="N�mero" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Conta" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Tipo do Documento" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Tipo de Despesa" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Valor Despesa" />
                            </rich:column>
                        </rich:columnGroup>
                    </f:facet>

                    <rich:column colspan="7">
                    	<h:outputText value="#{chave}" />
                    </rich:column>
                    
                    <rich:subTable 
                        onRowMouseOver="this.style.backgroundColor='#F8F8F8'"
                        onRowMouseOut="this.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'"
                        var="detalhe" value="#{pagamentoHandler.mapaRelatorioFinanceiro[chave][0]}">
                        <rich:column >
                            <h:outputText value="#{detalhe.pagamento.dataDocumento}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.pagamento.numeroDocumento}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.pagamento.conta.descricao}"/>
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.pagamento.tipoDocumento.descricao}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.despesa.nomeDespesa}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.valor}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            <f:facet name="footer">
                               <h:outputText value="#{pagamentoHandler.mapaRelatorioFinanceiro[chave][1]}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </f:facet>
                        </rich:column>
                        
                    </rich:subTable>
                  
					<f:facet name="footer">
                
                        <rich:columnGroup>
                            <rich:column colspan="5">Total:</rich:column>
                            <rich:column>
                                 <h:outputText value="#{pagamentoHandler.totalRelatorio}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </rich:column>
                        </rich:columnGroup>
                    </f:facet>
        
                </rich:dataTable>


</h:form>
</BODY>
</HTML>
</f:view>