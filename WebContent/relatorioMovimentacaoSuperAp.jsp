<%@ include file="common/cabecalho.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<f:view>

<a4j:keepAlive beanName="relatorioSuperApHandler" />

 
<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
	<script>
		jQuery(function(jQuery) {
			jQuery('input').bestupper();
			jQuery("#inicioAniversario").mask("99/9999",{placeholder:"_"});
			jQuery("#fimAniversario").mask("99/9999",{placeholder:"_"});
			jQuery("#dtAverbacao").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtInicio").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtFim").mask("99/99/9999", {
				placeholder : "_"
			});
		});

	</script>
	<DIV id=background_2>
		<DIV id=site_wrapper>
			<jsp:include page="common/menu.jsp" />
			<DIV id=wrap>
				<DIV id=content>
					<DIV id=main>
						<DIV id=main_content>
							<DIV id=main_body>

								<div id="breadcrumb">
									<span>Relat�rios</span>
									<li>Extrato Super AP</li>
								</div>
								<div id="icone">
									<img src="images/empresa.png" />
								</div>
								<h:form id="myform" styleClass="niceform" prependId="false">


									<div class="titulo_agrupamento_Dados"></div>
									<fieldset>
										<legend>Filtros</legend>
											<table>
											
											<tr> 
												<td align="right" width="150px;"><label
													for="txtSegurado">Segurado:</label></td>
												<td colspan="3"><h:inputText
														value="#{relatorioSuperApHandler.proposta.nomeSegurado}"
														id="txtNomeSegurado" size="40"  /> 
														<rich:suggestionbox
														id="suggestionBoxNomeSegurado" for="txtNomeSegurado"
														suggestionAction="#{relatorioSuperApHandler.SeguaradoSuperApAutoComplete}"
														var="proposta" fetchValue="#{proposta.nomeSegurado}"
														bypassUpdates="true" width="500" height="250"
														cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
														
														<a4j:support event="onselect" id="a4jsupport"
															ignoreDupResponses="true" reRender="valorPagamento,dataPrevisaoBoleto">
															<f:setPropertyActionListener value="#{proposta}"
																target="#{relatorioSuperApHandler.proposta}" />
														</a4j:support>
														<h:column>
															<h:outputText value="#{proposta.nomeSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{proposta.id}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText
																value="#{proposta.cpfSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>														
													</rich:suggestionbox>
												</td>
											</tr>
											
											<tr>
												<td align="right" width="150px;"> 
												   <label for="dtInicio">Per�odo (in�cio):</label> 
												</td>
												<td>
													<h:inputText value="#{relatorioSuperApHandler.inicio}"
																id="dtInicio" size="15" maxlength="15" label="Data de in�cio">
																<f:convertDateTime dateStyle="default"
																	pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
											</tr>
											
											<tr>
												<td align="right" width="150px;"> 
												   <label for="dtFim">Per�odo (fim):</label> 
												</td>
												<td>
													<h:inputText value="#{relatorioSuperApHandler.fim}"
																id="dtFim" size="15" maxlength="15" label="Data de fim">
																<f:convertDateTime dateStyle="default"
																	pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
											</tr>
											
											<tr style="white-space: nowrap">										
												<td align="right" width="150px;"> 
												   <label for="cmbTipoConsulta">Situa��o da Proposta:</label> 
												</td> 
												<td>
													<table border="0">
														<tr>
														<td>
															<h:selectManyCheckbox id="cmbTipoConsulta" value="#{relatorioSuperApHandler.situacaoPropostaSelectedList}">
																<f:selectItems value="#{relatorioSuperApHandler.opcoesSituacaoPropostaCombo}" />
															</h:selectManyCheckbox>
													   	</td>
													   	</tr>
												   </table>
												</td>
											</tr> 
										</table>
										
									</fieldset>

									<br />
									<p:commandButton action="#{relatorioSuperApHandler.gerarRelatorio}"
										value="Gerar Relat�rio" id="gravar" styleClass="button"
										async="false" ajax="false"  />
<%-- 									<p:commandButton action="#{relatorioSuperApHandler.gerarRelatorioSimplificado}" --%>
<%-- 										value="Rela��o dos Segurados" id="profissao" styleClass="button" --%>
<%-- 										async="false" ajax="false" /> --%>
<%-- 									<p:commandButton action="#{relatorioSuperApHandler.gerarRelacaoCorreios}" --%>
<%-- 										value="Rela��o para os Correios" id="correios" styleClass="button" --%>
<%-- 										async="false" ajax="false" /> --%>
<%-- 									<p:commandButton actionListener="#{relatorioSuperApHandler.cancelar}" --%>
<%-- 										value="Limpar Tela" id="cancelar" styleClass="button" --%>
<%-- 										async="false" ajax="false" /> --%>
<%-- 									<p:confirmDialog message="Deseja Cancelar esta opera��o?" --%>
<%-- 										yesLabel="Sim" noLabel="N�o" header="Confirma��o!" --%>
<%-- 										fixedCenter="true" modal="true" /> --%>
								</h:form>

							</DIV>
						</DIV>
						<DIV class=both>
							<!-- -->
						</DIV>
					</DIV>
					<DIV id=footer_top_bg>
						<!-- -->
					</DIV>
				</DIV>
			</DIV>
			<jsp:include page="common/footer.jsp" />
		</DIV>
		<DIV id=foo_glass></DIV>
		<BR> <BR>
	</DIV>
</BODY>

	</HTML>
</f:view>
