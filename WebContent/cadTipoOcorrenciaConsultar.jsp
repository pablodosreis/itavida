<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="tipoOcorrenciaHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>

<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
}); 
</script> 
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Motivo Sinistro</li> </div> 
						<div id="icone"> 
                           <img src="images/cadTipoOcorrencia.png" /> 
                        </div> 

						<h:form styleClass="niceform" >

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtDescricao">Descri��o:</label>
										</td>
										<td>
										 <h:inputText value="#{tipoOcorrenciaHandler.tipoOcorrencia.descricao}" id="txtDescricao" size="40" maxlength="50" tabindex="1"/>
										  
										</td>
									</tr>
								</table>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										   <label for="cmbSinistroNegado">Sinistro Negado:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbSinistroNegado" binding="#{tipoOcorrenciaHandler.sinistroNegadoSelecionado}" value="#{tipoOcorrenciaHandler.tipoOcorrencia.sinistroNegado}" tabindex="2">
										   	<f:selectItems value="#{tipoOcorrenciaHandler.simNaoCombo}"/>
										   </h:selectOneMenu>
										</td> 

									</tr> 
								</table>
							</fieldset>							
							<br />
							
							<h:commandButton actionListener="#{tipoOcorrenciaHandler.pesquisaTipoOcorrenciasPorNomeOuSinistro}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="3"/>
							<p:commandButton actionListener="#{tipoOcorrenciaHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty tipoOcorrenciaHandler.resultadoPesquisa}" constrainToViewport="true" fixedCenter="true" underlay="shadow"> 
									<p:dataTable id="resultTable" value="#{tipoOcorrenciaHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty tipoOcorrenciaHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true"  sortBy="#{item.descricao}">
										  <f:facet name="header">
										  <h:outputText value="Descri��o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.descricao}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.sinistroNegado}" >
										  <f:facet name="header">
										  <h:outputText value="Sinistro Negado" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.sinistroNegado == 'S' ? 'SIM' : 'N�O'}"/>									  										
										</p:column>		
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink action="#{tipoOcorrenciaHandler.preparaAlterarTipoOcorrencia}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:setPropertyActionListener target="#{tipoOcorrenciaHandler.tipoOcorrencia}" value="#{item}"/>	
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink id="linkExcluir" actionListener="#{tipoOcorrenciaHandler.excluirTipoOcorrencia}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirTipoOcorrencia" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>

								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="TipoOcorrencia"  postProcessor="#{tipoOcorrenciaHandler.postProcessXLS}" excludeColumns="2,3"/>  
										</h:commandLink>
								</div>
						</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
