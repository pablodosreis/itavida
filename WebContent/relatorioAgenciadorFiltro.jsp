<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="relatorioAgenciadorHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery("#dtAverbacaoInicio").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtAverbacaoFim").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtAgenciamentoInicio").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtAgenciamentoFim").mask("99/99/9999",{placeholder:"_"});
	});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rios</span><li>Relat�rios de Agenciadores</li> </div>
						<div id="icone">
                           <img src="images/historicoSinistros.png" />
                        </div>
						<h:form styleClass="niceform" prependId="false">

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
								<tr> 
										<td align="right" width="90px;"> 
										  <label for="txtAgenciador">Agenciador:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{relatorioAgenciadorHandler.agenciador.nome}" id="txtAgenciador" size="40" tabindex="36"/>
												<rich:suggestionbox id="suggestionBoxFuncionario" 
								                	for="txtAgenciador"
								                    suggestionAction="#{relatorioAgenciadorHandler.funcionarioAutoComplete}" 
								                    var="funcionario"
								                    fetchValue="#{funcionario.nome}"
													bypassUpdates="true"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!">
								                    										                    
													<a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{funcionario}" 
														target="#{relatorioAgenciadorHandler.agenciador}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{funcionario.tipoFuncionarioDescricao}" styleClass="autocompleteFonte"/>
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{funcionario.percentualComissao}" styleClass="autocompleteFonte"/>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr>
								<tr>										
									<td align="right" width="150px;"> 
									   <label for="cmbUf">UF:</label> 
									</td> 
									<td>
									   <h:selectOneMenu id="cmbUf" value="#{relatorioAgenciadorHandler.ufSelected}">
									   		<f:selectItems value="#{relatorioAgenciadorHandler.opcoesUfCombo}" />
									   		<a4j:support actionListener="#{relatorioAgenciadorHandler.atualizarCombo}" reRender="cmbOrgao"  event="onchange"/>
									   </h:selectOneMenu>
									   <p:commandButton actionListener="#{relatorioAgenciadorHandler.atualizarCombo}"
											value="Atualizar �rg�os" id="atualizar" async="false" ajax="false"/>
									</td> 											
								</tr>
								<tr>										
									<td align="right" width="150px;"> 
									   <label for="cmbOrgao">�rg�o:</label> 
									</td> 
									<td>
									   <h:selectOneMenu id="cmbOrgao" value="#{relatorioAgenciadorHandler.orgaoSelected}">
									   		<f:selectItems value="#{relatorioAgenciadorHandler.opcoesOrgaoCombo}" />
									   </h:selectOneMenu>
									</td> 											
								</tr>
								<tr>										
									<td align="right" width="150px;"> 
									   <label for="cmbSituacao">Situa��o:</label> 
									</td>
									<td>
										<table border="0">
											<tr>
												<td><h:selectManyCheckbox id="cmbSituacaoProposta"
														value="#{relatorioAgenciadorHandler.situacaoPropostaSelectedList}">
														<f:selectItems
															value="#{relatorioAgenciadorHandler.opcoesSituacaoPropostaCombo}" />
													</h:selectManyCheckbox></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="right" width="150px;">
									  <label for="dtAverbacaoInicio" >Data de Averba��o (in�cio):</label>
									</td>
									<td>
										<h:inputText value="#{relatorioAgenciadorHandler.inicio}"  id="dtAverbacaoInicio" maxlength="10"  tabindex="6">
											<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
										</h:inputText>
									</td>
								</tr>
								<tr>
									<td align="right" width="150px;">
									  <label for="dtAverbacaoFim" >Data de Averba��o (fim):</label>
									</td>
									<td>
										<h:inputText value="#{relatorioAgenciadorHandler.fim}"  id="dtAverbacaoFim" maxlength="10"  tabindex="7">
											<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
										</h:inputText>
									</td>
								</tr>
								<tr>
									<td align="right" width="150px;">
									  <label for="dtAverbacaoFim" >Data do Agenciamento (inicio):</label>
									</td>
									<td>
										<h:inputText value="#{relatorioAgenciadorHandler.dataAgencimentoInicio}"  id="dtAgenciamentoInicio" maxlength="10"  tabindex="8">
											<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
										</h:inputText>
									</td>
								</tr>
								<tr>
									<td align="right" width="150px;">
									  <label for="dtAverbacaoFim" >Data do Agenciamento (fim):</label>
									</td>
									<td>
										<h:inputText value="#{relatorioAgenciadorHandler.dataAgencimentoFim}"  id="dtAgenciamentoFim" maxlength="10"  tabindex="9">
											<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
										</h:inputText>
									</td>
								</tr>
								<tr>
									<td align="right" width="150px;">
									  <label for="cmbEmpresa1" >Empresa:</label>
									</td>
									<td>
										<table border="0">
											<tr>
												<td>
													<h:selectManyCheckbox id="cmbTipoConsulta" value="#{relatorioAgenciadorHandler.empresaSelectedList}" layout="pageDirection">
														<f:selectItems value="#{relatorioAgenciadorHandler.opcoesEmpresaCombo}" />
													</h:selectManyCheckbox>
											   	</td>
											</tr>
										</table>
									</td>
								</tr>
								</table>					
							</fieldset>							
	
							<br />

							<p:commandButton action="#{relatorioAgenciadorHandler.gerarRelatorio}" value="Gerar Relat�rio de Agenciadores" id="pesquisar1" styleClass="button" async="false" ajax="false" tabindex="10" />
							<p:commandButton action="#{relatorioAgenciadorHandler.gerarRelatorioCartonagem}" value="Gerar Relat�rio de Cartonagem" id="pesquisar2" styleClass="button" async="false" ajax="false" tabindex="11"/>
							<p:commandButton actionListener="#{relatorioAgenciadorHandler.limparCampos}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="12">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
