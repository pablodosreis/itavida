<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="propostaHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class="popUp">
<jsp:include page="common/relatorioHeader.jsp" />
<h:form styleClass="niceform" prependId="false">
			<table class="cabecalho_relatorio" >
				<tr>
					<td><IMG title="Slogan" alt="Slogan" src="images/logoRelatorio.png" border=0 /></td>
					<td width="200px;">Relat�rio de Segurados</td>
					<td>Data:</td>
					<td width="500px;"> <h:outputText value="#{propostaHandler.dataAtualRelatorio}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" /></h:outputText>
					<td class="invisivelImpressao">
						<h:commandLink onclick="javascript:window.print();">  
						    <p:graphicImage value="images/impressora.png" alt="Imprimir" title="Imprimir"/>  
						</h:commandLink>
					</td>
					<td class="invisivelImpressao">
						<h:commandLink>  
						    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
						    <p:dataExporter type="xls" target="resultTable" fileName="Relatorio_Segurados"  postProcessor="#{propostaHandler.postProcessXLS}"  />  
						</h:commandLink>
					</td>
					<td>
						<h:commandLink action="#{propostaHandler.exportarExcel}" >  
						    <p:graphicImage value="images/tinyExcel.png"  alt="Exportar Para Excel (Resumido)" title="Exportar Para Excel (Resumido)" width="32" height="32"/>  
						</h:commandLink>
					</td>
				<tr>
			</table>

							<p:dataTable value="#{propostaHandler.resultadoPesquisa}" 
										id="resultTable" var="item" width="100%" 
												 rendered="#{not empty propostaHandler.resultadoPesquisa}" 
								>
										
									
										<p:column resizable="true" sortBy="#{item.id}" >
										  <f:facet name="header">
										  <h:outputText value="Proposta" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.id}" />
											
										</p:column>
										<p:column resizable="true" sortBy="#{item.orgao.nomeOrgao}" >
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
										</p:column>
										<!-- Acrescentado 27/01/2014 -->
										<p:column resizable="true" sortBy="#{item.cpfSegurado}" >
										  <f:facet name="header">
										  <h:outputText value="CPF do Segurado" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cpfSegurado}" />
										</p:column>
										
										
										<!-- Acrescentado 08/04/2014 -->
										<p:column resizable="true" sortBy="#{item.matriculaSegurado}" >
										  <f:facet name="header">
										  <h:outputText value="Matr�cula" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.matriculaSegurado}" />
										</p:column>
											
										<p:column resizable="true" sortBy="#{item.nomeSegurado}" >
										  <f:facet name="header">
										  <h:outputText value="Segurado Principal" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeSegurado}"/>									  										
										</p:column>	
										
										<p:column resizable="true" sortBy="#{item.telefoneCelular}" >
										  <f:facet name="header">
										  <h:outputText value="Celular" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.telefoneCelular}" />
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.dataNascimentoSegurado}" >
										  <f:facet name="header">
										  <h:outputText value="Data Nascimento" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataNascimentoSegurado}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.propostaagenciador_collection[0].funcionario.nome}" rendered="#{propostaHandler.tipoRelatorio eq 5}">
										  <f:facet name="header">
										  <h:outputText value="Agenciador" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.propostaagenciador_collection[0].funcionario.nome}" />									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.propostaagenciador_collection[0].funcionario.percentualComissao}" rendered="#{propostaHandler.tipoRelatorio eq 5}">
										  <f:facet name="header">
										  <h:outputText value="% Comiss�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.propostaagenciador_collection[0].funcionario.percentualComissao}" />									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.propostaagenciador_collection[fn:length(item.propostaagenciador_collection)-1].funcionario.nome}" rendered="#{propostaHandler.tipoRelatorio eq 10}">
										  <f:facet name="header">
										  <h:outputText value="Agenciador" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.propostaagenciador_collection[fn:length(item.propostaagenciador_collection)-1].funcionario.nome}" />									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.propostaagenciador_collection[fn:length(item.propostaagenciador_collection)-1].funcionario.percentualAlteracao}" rendered="#{propostaHandler.tipoRelatorio eq 10}">
										  <f:facet name="header">
										  <h:outputText value="% Comiss�o de Altera��o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.propostaagenciador_collection[fn:length(item.propostaagenciador_collection)-1].funcionario.percentualAlteracao}" />									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.idadeRealSegurado}" >
										  <f:facet name="header">
										  <h:outputText value="Idade Atual" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.idadeRealSegurado}"></h:outputText>									  										
										</p:column>	
										<p:column resizable="true"  sortBy="#{item.idadePrevistaSegurado}" rendered="#{propostaHandler.tipoRelatorio eq 3}" >
										  <f:facet name="header">
										  <h:outputText value="Idade Prevista (30 dias)" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.idadePrevistaSegurado}"></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cidade.nom_cidade}">
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.cidade.cod_estado.sgl_estado}">
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>


										<p:column resizable="true" sortBy="#{item.totalPremios}" parser="number">
										
										  <f:facet name="header">
										  <h:outputText value="Total Pr�mios Atual" /> 
										  	
										  </f:facet>
										  
										  <h:outputText value="#{item.totalPremios}" ><f:convertNumber type="currency" groupingUsed="false" currencySymbol="" /></h:outputText>
										</p:column>	
										<p:column resizable="true"  rendered="#{propostaHandler.tipoRelatorio eq 5}">
										  <f:facet name="header">
										  <h:outputText value="Valor Comiss�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{ (item.propostaagenciador_collection[0].funcionario.percentualComissao * item.totalPremios)/100 - (item.orgao.valorProlabore + item.orgao.valorTaxa ) }" />									  										
										</p:column>	
										<p:column resizable="true"  rendered="#{propostaHandler.tipoRelatorio eq 10}">
										  <f:facet name="header">
										  <h:outputText value="Valor Comiss�o Altera��o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{ (item.propostaagenciador_collection[ fn:length(item.propostaagenciador_collection)-1 ].funcionario.percentualAlteracao * ( item.propostaagenciador_collection[ fn:length(item.propostaagenciador_collection)  -1].valorTotalPremio -  item.propostaagenciador_collection[ fn:length(item.propostaagenciador_collection)  -2].valorTotalPremio))/100 - (item.orgao.valorProlabore + item.orgao.valorTaxa ) }" />									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.novoValorFaixaEtaria}" rendered="#{propostaHandler.tipoRelatorio eq 3}">
										  <f:facet name="header">
										  <h:outputText value="Novo Total Pr�mios" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.novoValorFaixaEtaria}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="#{false}"/></h:outputText>			
										</p:column>	
										<p:column resizable="true" sortBy="#{item.diferencaValores}" rendered="#{propostaHandler.tipoRelatorio eq 3}">
										  <f:facet name="header">
										  <h:outputText value="Diferen�a" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.diferencaValores}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="#{false}"/></h:outputText>			
										</p:column>	
										<p:column resizable="true" sortBy="#{item.dataCadastro}" rendered="false">
										  <f:facet name="header">
										  <h:outputText value="Data Cadastro" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataCadastro}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" /></h:outputText>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.dataAprovacao}">
										  <f:facet name="header">
										  <h:outputText value="Data Aprova��o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataAprovacao}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.numeroSorteio}">
										  <f:facet name="header">
										  <h:outputText value="N�mero Sorteio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.numeroSorteio}" />									  										
										</p:column>	
										<p:column resizable="true" rendered="#{propostaHandler.tipoRelatorio eq 1}" sortBy="#{item.dataCancelamento}">
										  <f:facet name="header">
										  <h:outputText value="Data Cancelamento" /> 
										  </f:facet>
										  <h:outputText value="#{item.dataCancelamento}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" /></h:outputText>									  										
										</p:column>
										<p:column resizable="true" rendered="#{propostaHandler.tipoRelatorio eq 1}" sortBy="#{item.motivoCancelamento.descricao}">
										  <f:facet name="header">
										  <h:outputText value="Motivo Cancelamento" /> 
										  </f:facet>
										  <h:outputText value="#{item.motivoCancelamento.descricao}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" /></h:outputText>									  										
										</p:column>
									</p:dataTable>
<fieldset style="width: 400px;">
<legend style="font-size: 1.2em;">Sum�rio do Relat�rio</legend>
<h:outputText value="Total de Registros: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{propostaHandler.totalRegistrosRelatorio}" />
<br/>
<h:outputText value="Total de Pr�mios: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{propostaHandler.valorTotalRegistrosRelatorio}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>

</fieldset>
						</h:form>
</BODY>
</HTML>
</f:view>
