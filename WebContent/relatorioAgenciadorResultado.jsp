<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="relatorioAgenciadorHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class="popUp">
<jsp:include page="common/relatorioHeader.jsp" />
<h:form styleClass="niceform" prependId="false">
			<table class="cabecalho_relatorio" >
				<tr>
					<td><IMG title="Slogan" alt="Slogan" src="images/logoRelatorio.png" border=0 /></td>
					<td width="70%;">
						ITAVIDA CLUBE DE SEGUROS
						<br/>
						Planilha de Agenciamento - <h:outputText value="#{relatorioAgenciadorHandler.dataAtualRelatorio}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" /></h:outputText>
						<br/>
						Agenciador: <h:outputText value="#{relatorioAgenciadorHandler.agenciador.nome}"/>	
					</td>
					<td class="invisivelImpressao">
						<h:commandLink onclick="javascript:window.print();">  
						    <p:graphicImage value="images/impressora.png" alt="Imprimir" title="Imprimir"/>  
						</h:commandLink>
					</td>
					<td class="invisivelImpressao">
						<h:commandLink>  
						    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
						    <p:dataExporter type="xls" target="resultTable" fileName="Relatorio_Segurados"  postProcessor="#{relatorioAgenciadorHandler.postProcessXLS}"  />  
						</h:commandLink>
					</td>
				<tr>
			</table> 

							<p:dataTable value="#{relatorioAgenciadorHandler.resultadoPesquisa}" 
										id="resultTable" var="item" width="100%" 
												 rendered="#{not empty relatorioAgenciadorHandler.resultadoPesquisa}" 
								>
										
									
										<p:column resizable="true" sortBy="#{item.proposta}" >
										  <f:facet name="header">
										  <h:outputText value="Proposta" /> 
										  </f:facet>
										  <h:outputText value="#{item.proposta}" />
										</p:column>

										<p:column resizable="true" sortBy="#{item.nomeOrgao}" >
										  <f:facet name="header">
										  <h:outputText value="Nome do �rg�o" /> 
										  </f:facet>
										  <h:outputText value="#{item.nomeOrgao}"/>									  										
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.nomeSegurado}" >
										  <f:facet name="header">
										  <h:outputText value="Nome do Segurado" /> 
										  </f:facet>
										  <h:outputText value="#{item.nomeSegurado}"/>									  										
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.dataAverbacao}" >
										  <f:facet name="header">
										  <h:outputText value="Data de Averba��o" /> 
										  </f:facet>
										  <h:outputText value="#{item.dataAverbacao}">
										  	<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
										  </h:outputText>									  										
										</p:column>
											
										<p:column resizable="true" sortBy="#{item.situacaoAverbacao}" >
										  <f:facet name="header">
										  <h:outputText value="Posi��o de Averba��o" /> 
										  </f:facet>
										  <h:outputText value="#{item.situacaoAverbacao}"/>									  										
										</p:column>
											
										<p:column resizable="true" sortBy="#{item.comando}">
										  <f:facet name="header">
										  <h:outputText value="Comando" /> 
										  </f:facet>
										  <h:outputText value="#{item.comando}"/>									  										
										</p:column>
											
										<p:column resizable="true" sortBy="#{item.valorTotalPremio}" >
										  <f:facet name="header">
										  <h:outputText value="Valor Total do Pr�mio" /> 
										  </f:facet>
										  <h:outputText value="#{item.valorTotalPremio}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.valorDescontado}" >
										  <f:facet name="header">
										  <h:outputText value="Valor Descontado" /> 
										  </f:facet>
										  <h:outputText value="#{item.valorDescontado}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.baseCalculo}" >
										  <f:facet name="header">
										  <h:outputText value="Base para C�lculo" /> 
										  </f:facet>
										  <h:outputText value="#{item.baseCalculo}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										  <h:outputText value="-" rendered="#{item.baseCalculo == null}"/>
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.percentual}" >
										  <f:facet name="header">
										  <h:outputText value="Percentual" /> 
										  </f:facet>
										  <h:outputText value="#{item.percentual}">
										  	<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol=""/>
										  </h:outputText>
										  <h:outputText value="-" rendered="#{item.percentual == null}"/>									  										
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.getValorComissao}" >
										  <f:facet name="header">
										  <h:outputText value="Valor Comiss�o" /> 
										  </f:facet>
										  <h:outputText value="#{item.valorComissao}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>
										
											
									</p:dataTable>
<fieldset style="width: 400px;">
<table border="1 solid 1px">
	<tr>
		<th>Percentual</th>
		<th>Comiss�o L�quida</th>
	</tr>
		<tr>
			<td>100%</td>
			<td>
				<h:outputText value="#{relatorioAgenciadorHandler.valorComissao100}">
					<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
				</h:outputText>
			</td>
		</tr>
	<tr>
		<td>50%</td>
		<td>
			<h:outputText value="#{relatorioAgenciadorHandler.valorComissao50}">
				<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
			</h:outputText>
		</td>
	</tr>
	<tr>
		<td>25%</td>
		<td>
			<h:outputText value="#{relatorioAgenciadorHandler.valorComissao25}">
				<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
			</h:outputText>
		</td>
	</tr>
	<tr>
		<td>Outros</td>
		<td>
			<h:outputText value="#{relatorioAgenciadorHandler.valorComissaoOutros}">
				<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
			</h:outputText>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<h:outputText value="#{relatorioAgenciadorHandler.valorTotalComissao}" style="font-weight:bold;">
				<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
			</h:outputText>
		</td>
	</tr>
</table>

</fieldset>
						</h:form>
</BODY>
</HTML>
</f:view>
