<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="faturamentoHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class="popUp">
<jsp:include page="common/relatorioHeader.jsp" />

<h:form styleClass="niceform" prependId="false">
			<table class="cabecalho_relatorio" >
				<tr>
					<td><IMG title="Slogan" alt="Slogan" src="images/logoRelatorio.png" border=0 /></td>
					<td width="200px;">Relat�rio de Faturamento</td>
					<td>Data:</td>
					<td width="500px;"> <h:outputText value="#{faturamentoHandler.dataAtualRelatorio}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" locale="pt_BR" /></h:outputText>
					<td class="invisivelImpressao">
						<h:commandLink onclick="javascript:window.print();">  
						    <p:graphicImage value="images/impressora.png" alt="Imprimir" title="Imprimir"/>  
						</h:commandLink>
					</td>
					<td class="invisivelImpressao">
						<h:commandLink>  
						    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
						    <p:dataExporter type="xls" target="resultTable" fileName="Relatorio_Faturamento"  postProcessor="#{faturamentoHandler.postProcessXLS}" />  
						</h:commandLink>
					</td>
				<tr>
			</table>

			<rich:dataTable 
                    onRowMouseOver="this.style.backgroundColor='#F1F1F1'"
                    onRowMouseOut="this.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'"
                    cellpadding="0" cellspacing="0" 
                    border="0" var="chave" value="#{faturamentoHandler.mapKeys}" sortMode="single" >
                
                        <f:facet name="header">
                        <rich:columnGroup>
                            <rich:column rowspan="2" >
                                <h:outputText value="�rg�o" />
                            </rich:column>
                            <rich:column rowspan="2" >
                                <h:outputText value="Vidas" />
                            </rich:column>
                            <rich:column rowspan="2" >
                                <h:outputText value="Vidas S/ Taxa" />
                            </rich:column>
                            <rich:column rowspan="2" >
                                <h:outputText value="Data do Pagamento" />
                            </rich:column>
                            <rich:column rowspan="2" >
                                <h:outputText value="M�s Vig�ncia" />
                            </rich:column>
                            <rich:column rowspan="2" >
                                <h:outputText value="M�s Cobran�a" />
                            </rich:column>
                            <rich:column colspan="5">
                                <h:outputText value="Valores da Planilha de Faturamento" />
                            </rich:column>
                            <rich:column breakBefore="true">
                                <h:outputText value="Prolabore" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Sem Prolabore" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Rela��o" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Seguradora" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Taxa" />
                            </rich:column>
                        </rich:columnGroup>
                    </f:facet>

                    <rich:column colspan="11">
                    	<h:outputText value="#{chave}" style="FONT: bold 11px  Arial, Helvetica, sans-serif;"/>
                    </rich:column>
                    
                    <rich:subTable 
                        onRowMouseOver="this.style.backgroundColor='#F8F8F8'"
                        onRowMouseOut="this.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'"
                        var="item" value="#{faturamentoHandler.mapaRelatorioFaturamento[chave][0]}">
                        <rich:column >
                            <h:outputText value="#{item.orgao.nomeOrgao}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.numeroVidas}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.numeroVidasSemTaxa}"/>
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.dataPagamento}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.mesAnoVigencia}"><f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" /></h:outputText>
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.mesAnoCobranca}"><f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" /></h:outputText>
             				<f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.valorProlabore}">
									<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
							</h:outputText>	
                            <f:facet name="footer">
                               <h:outputText value="#{faturamentoHandler.mapaRelatorioFaturamento[chave][1]}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.valorSemProlabore}">
									<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
							</h:outputText>	
                            <f:facet name="footer">
                               <h:outputText value="#{faturamentoHandler.mapaRelatorioFaturamento[chave][2]}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.valorRelacao}">
									<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
							</h:outputText>	
                            <f:facet name="footer">
                               <h:outputText value="#{faturamentoHandler.mapaRelatorioFaturamento[chave][3]}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.valorSeguradora}">
									<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
							</h:outputText>	
                            <f:facet name="footer">
                               <h:outputText value="#{faturamentoHandler.mapaRelatorioFaturamento[chave][4]}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </f:facet>
                        </rich:column>       
                        <rich:column>
                            <h:outputText value="#{item.valorTaxa}">
									<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
							</h:outputText>	
                            <f:facet name="footer">
                               <h:outputText value="#{faturamentoHandler.mapaRelatorioFaturamento[chave][5]}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </f:facet>
                        </rich:column>                    
                    </rich:subTable>
                  
					<f:facet name="footer">
                
                        <rich:columnGroup>
                            <rich:column colspan="6">Total:</rich:column>
                            <rich:column>
                                 <h:outputText value="#{faturamentoHandler.totalValor[0]}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </rich:column>
                            <rich:column>
                                 <h:outputText value="#{faturamentoHandler.totalValor[1]}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </rich:column>
                            <rich:column>
                                 <h:outputText value="#{faturamentoHandler.totalValor[2]}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </rich:column>
                            <rich:column>
                                 <h:outputText value="#{faturamentoHandler.totalValor[3]}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </rich:column>
                            <rich:column>
                                 <h:outputText value="#{faturamentoHandler.totalValor[4]}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </rich:column>
                        </rich:columnGroup>
                    </f:facet>
        
                </rich:dataTable>

			</h:form>
</BODY>
</HTML>
</f:view>
