<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="orgaoHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery('#txtNomeOrgao').focus(); 
	   jQuery("#txtCep").mask("99.999-999",{placeholder:"_"});
	   jQuery("#txtTelefone").mask("(99)9?9999-9999",{placeholder:"_"});
	   jQuery("#txtFax").mask("(99)9999-9999",{placeholder:"_"});
	   jQuery("#txtTelefoneContato").mask("(99)9999-9999",{placeholder:"_"});
	   jQuery("#txtCgc").mask("99.999.999/9999-99",{placeholder:"_"});
	   jQuery('#txtValorTaxa').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	 
	   jQuery('#txtValorProlabore').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		  
	});
</script>


<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rios</span><li>Mala Direta por �rg�os</li> </div>
						<div id="icone">
                           <img src="images/malaDireta.png" />
                        </div>

				<h:form id="myform" styleClass="niceform" prependId="false" >
					<p:focus/>
					<h:panelGrid id="cpfMessage">
											<p:dialog header="MENSAGEM" widgetVar="dialog"  
											               fixedCenter="true" modal="true" visible="#{orgaoHandler.visibleMessage}" >
											   <h:messages errorClass="mensagem_erro" fatalClass="mensagem_erro" infoClass="mensagem_sucesso" warnClass="mensagem_alerta"/>                 
											</p:dialog>
					</h:panelGrid>
	<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
			
									<div class="divRecuadoDireito">
										<table> 
											<tr> 
												<td align="left" width="90px;"> 
												   <label for="cmbTipoOrgao">Tipo de �rg�o:</label> 
												</td> 
											</tr>
											<tr>
												<td align="right">
												   <h:selectOneMenu id="cmbTipoOrgao" binding="#{orgaoHandler.tipoOrgaoSelecionado}" value="#{orgaoHandler.orgao.tipoOrgao}" tabindex="4">
												   	<f:selectItems value="#{orgaoHandler.tipoOrgaoCombo}"/>
												   </h:selectOneMenu>
												</td> 
		
											</tr> 
										</table>
									</div>

									<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeOrgao">Nome Org�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.nomeOrgao}" id="txtNomeOrgao" size="40" maxlength="50" tabindex="1"/>
										</td> 
									</tr>
 										<tr>
											<td align="right" width="60px;">
											  <label for="textinput">Sigla:</label>
											</td>
											<td>
											   <h:inputText value="#{orgaoHandler.orgao.siglaOrgao}" id="txtSiglaOrgao" size="10" maxlength="10" tabindex="2" />
											</td>											
										</tr>
									
								</table>
								<table>
									<tr>
										<td align="right" width="60px;"> 
										  <label for="txtCgc">CGC:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.cgc}" id="txtCgc" size="20" maxlength="20" tabindex="3">
											  <p:ajax actionListener="#{orgaoHandler.verificaCNPJ}" event="blur" update="cnpjMessage"  />
											</h:inputText>
										</td> 
									</tr>
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
							
								</table>
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeCidade">Cidade:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.cidade.nom_cidade}" id="txtNomeCidade" size="30" tabindex="4" >
												<p:ajax event="keyup" update="txtUf" >
													<f:setPropertyActionListener 
														value="#{orgaoHandler.limparCidade}" 
														target="#{orgaoHandler.orgao.cidade}"/> 
												</p:ajax>	
											</h:inputText>
										   <h:inputText value="#{orgaoHandler.orgao.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" style="width:30px;" disabled="true" styleClass="textinput" />
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeCidade"
								                    suggestionAction="#{orgaoHandler.cidadeAutocomplete}" 
								                    var="cidade"
								                    fetchValue="#{cidade.nom_cidade}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" reRender="txtUf"> 
														<f:setPropertyActionListener 
														value="#{cidade}" 
														target="#{orgaoHandler.orgao.cidade}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
<div class="divRecuadoDireito">
								<table>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtPercentual" >Valor de Taxa:</label>
										</td>
										<td>
										   <h:inputText value="#{orgaoHandler.orgao.valorTaxa}" id="txtValorTaxa" size="10" maxlength="10" tabindex="5">
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
										  </h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtPercentual" >Valor de Prolabore:</label>
										</td>
										<td>
										  <h:inputText value="#{orgaoHandler.orgao.valorProlabore}" id="txtValorProlabore" size="10" maxlength="10" tabindex="6" >
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
										  </h:inputText>										 
										</td>
									</tr>
								</table>
</div>									

						</fieldset>		
							<br />
							<h:commandButton actionListener="#{orgaoHandler.pesquisaOrgaos}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="14"/>
							<p:commandButton actionListener="#{orgaoHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="15">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
<p:dialog  visible="true" header="SELECIONE OS �RG�OS DESEJADOS" rendered="#{not empty orgaoHandler.resultadoPesquisa}" constrainToViewport="true" fixedCenter="true" underlay="shadow" width="950" height="550">
<div style="overflow: auto; width:950px; height:350px;" >
									<p:dataTable id="resultTable" value="#{orgaoHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 selection="#{orgaoHandler.orgaosSelecionados}"
												 selectionMode="multiple"
												 rendered="#{not empty orgaoHandler.resultadoPesquisa}" 
												 >
										
										<p:column resizable="true" sortBy="#{item.nomeOrgao}">
										  <f:facet name="header">
										  <h:outputText value="Nome do Org�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeOrgao}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.siglaOrgao}">
										  <f:facet name="header">
										  <h:outputText value="Sigla" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.siglaOrgao}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.tipoOrgaoDescricao}">
										  <f:facet name="header">
										  <h:outputText value="Tipo" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.tipoOrgaoDescricao}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cgc}">
										  <f:facet name="header">
										  <h:outputText value="CGC" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cgc}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cidade.nom_cidade}">
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.cidade.cod_estado.sgl_estado}">
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.telefone}">
										  <f:facet name="header">
										  <h:outputText value="Telefone" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.telefone}"/>		
										</p:column>	
										<p:column resizable="true" sortBy="#{item.fax}">
										  <f:facet name="header">
										  <h:outputText value="Fax" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.fax}"/>									  										
										</p:column>																																																						
									</p:dataTable>
</div>
								<div align="right" style="padding-top: 5px;padding-bottom: 10px;" >
										<h:commandLink action="#{orgaoHandler.generateMalaDireta}">  
										    <p:graphicImage value="images/gerarMalaDireta.png"  alt="Gerar Mala Direta" title="Gerar Mala Direta"/>
										</h:commandLink>
								</div>
						</p:dialog>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
