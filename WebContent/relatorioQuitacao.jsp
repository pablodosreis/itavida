<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="relatorioQuitacaoHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery("#dtAverbacaoInicio").mask("99/9999",{placeholder:"_"});
	});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rios</span><li>Relat�rio de Quita��o</li> </div>
						<div id="icone">
                           <img src="images/historicoSinistros.png" />
                        </div>
						<h:form styleClass="niceform" prependId="false">

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>

								<tr>
									<td align="right" width="150px;">
									  <label for="dtAverbacaoInicio" >Data de Refer�ncia:</label>
									</td>
									<td>
										<h:inputText value="#{relatorioQuitacaoHandler.dataReferencia}"  id="dtAverbacaoInicio" maxlength="10"  tabindex="6">
											<f:convertDateTime dateStyle="default" pattern="MM/yyyy" />
										</h:inputText>
									</td>
								</tr>
								</table>					
							</fieldset>							
	
							<br />

							<p:commandButton action="#{relatorioQuitacaoHandler.gerarRelatorio}" value="Gerar Relat�rio de Quita��o" id="pesquisar" styleClass="button" async="false" ajax="false" tabindex="10" />
							<p:commandButton actionListener="#{relatorioQuitacaoHandler.limparCampos}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="12">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
