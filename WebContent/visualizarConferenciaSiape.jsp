<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="siapeHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
	jQuery("#dtPeriodoReferenciaA").mask("99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoReferenciaB").mask("99/9999",{placeholder:"_"});
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Visualiza��o </span><li>Confer�ncia de Arquivos D8 </li></div>
						<div id="icone_tokio">
                           <img src="images/siape.png" />
                        </div>
                          

						<h:form  styleClass="niceform" prependId="false">
						<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Per�odos desta Confer�ncia</legend>
									<B>Per�odo A:</B> <h:outputText value="#{siapeHandler.siape.periodoReferenciaA}"><f:convertDateTime dateStyle="default" pattern="MM/yyyy" /></h:outputText><br/> 
									<B>Per�odo B:</B> <h:outputText value="#{siapeHandler.siape.periodoReferenciaB}"><f:convertDateTime dateStyle="default" pattern="MM/yyyy" /></h:outputText>
							</fieldset>
<br/>
							<fieldset>
								<legend>Servidores que Sa�ram</legend>
								<p:dataTable id="servidoresSairam" value="#{siapeHandler.listaServidoresSairam}" 
												 var="item" width="100%" 
												 rendered="#{not empty siapeHandler.listaServidoresSairam}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.nomeServidor}">
										  <f:facet name="header"><h:outputText value="Nome do Servidor" /> </f:facet>

										  <h:outputText value="#{item.nomeServidor}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.matricula}">
										  <f:facet name="header">
										  <h:outputText value="Matr�cula Siape" /> 
										  </f:facet>
										  <h:outputText value="#{item.matricula}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cpf}">
										  <f:facet name="header">
										  <h:outputText value="Cpf" /> 
										  </f:facet>
										  <h:outputText value="#{item.cpf}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.codigoOrgao.nomeOrgaoSiape}">
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  </f:facet>
										  <h:outputText value="#{item.codigoOrgao.nomeOrgaoSiape}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.valor}">
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  </f:facet>
										 <h:outputText value="#{item.valor}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>									  										
										</p:column>	
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="servidoresSairam" fileName="Siape_D8_Servidores_Sairam"  postProcessor="#{siapeHandler.postProcessXLS}" />  
										</h:commandLink>
								</div>
<h:outputText value="Total de Registros Sa�ram: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{siapeHandler.totalRegistrosSairam}" />
<br/>
<h:outputText value="Valor Total Registros Sa�ram: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{siapeHandler.valorTotalRegistrosSairam}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>

							</fieldset>		
<br/>
							<fieldset>
								<legend>Servidores que Entraram</legend>
								<p:dataTable id="servidoresEntraram" value="#{siapeHandler.listaServidoresEntraram}" 
												 var="item" width="100%" 
												 rendered="#{not empty siapeHandler.listaServidoresEntraram}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.nomeServidor}">
										  <f:facet name="header"><h:outputText value="Nome do Servidor" /> </f:facet>

										  <h:outputText value="#{item.nomeServidor}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.matricula}">
										  <f:facet name="header">
										  <h:outputText value="Matr�cula Siape" /> 
										  </f:facet>
										  <h:outputText value="#{item.matricula}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cpf}">
										  <f:facet name="header">
										  <h:outputText value="Cpf" /> 
										  </f:facet>
										  <h:outputText value="#{item.cpf}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.codigoOrgao.nomeOrgaoSiape}">
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  </f:facet>
										  <h:outputText value="#{item.codigoOrgao.nomeOrgaoSiape}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.valor}">
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  </f:facet>
										 <h:outputText value="#{item.valor}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>									  										
										</p:column>	
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="servidoresEntraram" fileName="Siape_D8_Servidores_Entraram"  postProcessor="#{siapeHandler.postProcessXLS}" />  
										</h:commandLink>
								</div>
<h:outputText value="Total de Registros Entraram: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{siapeHandler.totalRegistrosEntraram}" />
<br/>
<h:outputText value="Valor Total Registros Entraram: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{siapeHandler.valorTotalRegistrosEntraram}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>

							</fieldset>	
<br/>
							<fieldset>
								<legend>Servidores que Mudaram de Valor/�rg�o</legend>
								<p:dataTable id="servidoresMudaram" value="#{siapeHandler.listaServidoresMudaramValorOrgao}" 
												 var="item" width="100%" 
												 rendered="#{not empty siapeHandler.listaServidoresMudaramValorOrgao}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.nomeServidor}">
										  <f:facet name="header"><h:outputText value="Nome do Servidor" /> </f:facet>

										  <h:outputText value="#{item.nomeServidor}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.matricula}">
										  <f:facet name="header">
										  <h:outputText value="Matr�cula Siape" /> 
										  </f:facet>
										  <h:outputText value="#{item.matricula}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cpf}">
										  <f:facet name="header">
										  <h:outputText value="Cpf" /> 
										  </f:facet>
										  <h:outputText value="#{item.cpf}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.codigoOrgao.nomeOrgaoSiape}">
										  <f:facet name="header">
										  <h:outputText value="�rg�o Antigo" /> 
										  </f:facet>
										  <h:outputText value="#{item.codigoOrgao.nomeOrgaoSiape}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.codigoOrgao.nomeOrgaoSiape}">
										  <f:facet name="header">
										  <h:outputText value="�rg�o Novo" /> 
										  </f:facet>
										  <h:outputText value="#{item.siapeB.codigoOrgao.nomeOrgaoSiape}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.valor}">
										  <f:facet name="header">
										  <h:outputText value="Valor Antigo" /> 
										  </f:facet>
										 <h:outputText value="#{item.valor}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.valor}">
										  <f:facet name="header">
										  <h:outputText value="Valor Novo" /> 
										  </f:facet>
										 <h:outputText value="#{item.siapeB.valor}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>									  										
										</p:column>	
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="servidoresMudaram" fileName="Siape_D8_Servidores_Mudaram"  postProcessor="#{siapeHandler.postProcessXLS}" />  
										</h:commandLink>
								</div>
<h:outputText value="Total de Registros Mudaram: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{siapeHandler.totalRegistrosMudaram}" />
<br/>
<h:outputText value="Valor Total Registros Mudaram (Antigo): " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{siapeHandler.valorTotalRegistrosMudaramAntigo}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
<br/>
<h:outputText value="Valor Total Registros Mudaram (Novo): " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{siapeHandler.valorTotalRegistrosMudaramNovo}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>

							</fieldset>						

							<br />
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
