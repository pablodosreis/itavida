<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="cidadeHandler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>import</span><li>arch</li> </div>
						<div id="icone">
                           <img src="images/cadCidades.png" />
                        </div>
						<h:form id="myform" styleClass="niceform" prependId="false">


					<div class="titulo_agrupamento_Dados" ></div>

							<br /><br /><br />
							<h:commandButton actionListener="#{cidadeHandler.processa}" value="PROCESSA_CIDADE" id="processacIDADE" styleClass="button" tabindex="3" />
							<h:commandButton actionListener="#{cidadeHandler.processaCapital}" value="PROCESSA_CAPITAL" id="pesquisar" styleClass="button" tabindex="3" />
							<h:commandButton actionListener="#{cidadeHandler.processaPagamento}" value="PROCESSA_PAGAMENTO" id="PROCESSPAGAMENTO" styleClass="button" tabindex="3" />
							<h:commandButton actionListener="#{cidadeHandler.migraCapitais}" value="MIGRA_CAPITAIS" id="MIGRA_CAPITAIS" styleClass="button" tabindex="3" />
							<h:commandButton actionListener="#{cidadeHandler.migraCapitaisFaixaEtaria}" value="MIGRA_CAPITAIS_FAIXA" id="MIGRA_CAPITAIS_FAIXA" styleClass="button" tabindex="3" />
							<br /><br />
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
