<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="capitalSeguradoLimiteIdadeHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />  
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   
	   if(  jQuery('#txtNomeTabela').val() == null || jQuery('#txtNomeTabela').val() == ''){
	   	jQuery('#txtNomeTabela').focus();
	   }else{
		   jQuery('#dtDataVigencia').focus();  
	   }
	   jQuery("#dtDataVigencia").mask("99/99/9999",{placeholder:"_"});
	   
	   jQuery('#txtFunMorteNatural').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	 
	   jQuery('#txtFuncMorteAcidente').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	   jQuery('#txtFuncInvalidezPerm').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	   jQuery('#txtFuncVLIndividual').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	   jQuery('#txtFuncVLCasado50').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	
	   jQuery('#txtFuncVLCasado100').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	
	   jQuery('#txtConjMorteAcidente').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});				
	   jQuery('#txtConjMorteNatural').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	
	   jQuery('#txtConjInvalidezPerm').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Tabelas de Capital Segurado Por Limite de Idade</li> </div> 
						<div id="icone"> 
                           <img src="images/cadTabelaLimiteIdade.png" /> 
                        </div> 

				<h:form styleClass="niceform" prependId="false">


					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="txtNomeTabela">Nome da Tabela:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.nomeTabela.nomeTabela}" id="txtNomeTabela" size="30" tabindex="1" >
												<p:ajax event="keyup" update="renderizado" >
														<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{capitalSeguradoLimiteIdadeHandler.limparNomeTabela}" 
														target="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.nomeTabela}"/> 
												</p:ajax>
										    </h:inputText>
											
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeTabela"
								                    suggestionAction="#{capitalSeguradoLimiteIdadeHandler.nomeTabelaAutocomplete}" 
								                    var="nomeTabela"
								                    fetchValue="#{nomeTabela.nomeTabela}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{nomeTabela}" 
														target="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.nomeTabela}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabela}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabelaReduzido}" styleClass="autocompleteFonte"/>
								                    </h:column>
								             </rich:suggestionbox>		
									</tr> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="dtDataVigencia">Data Vig�ncia:</label> 
										</td> 
										<td>
											<h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.dataVigencia}"  id="dtDataVigencia" maxlength="10" tabindex="2">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>										
								</table>	
<br/><br/>
<table align="left"> 
	<thead>
		<th> Funcion�rio </th>
		<th> C�njuge </th>
	</thead>
	<tr>
		<td align="right" valign="top">
								<table>
											<tr>
												<td align="right" width="150px;"> 
													<label for="txtFunMorteNatural">Morte Natural</label>
												</td>
												<td>
												   <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.funcVLMorteNatural}" id="txtFunMorteNatural" size="15" maxlength="15"  tabindex="3">
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													  </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;"> 
													<label for="txtFuncMorteAcidente">Morte Por Acidente</label>
												</td>
												<td>
													<h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.funcVLMorteAcidente}" id="txtFuncMorteAcidente" size="15" maxlength="15"  tabindex="4">
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													  </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;"> 
													<label for="txtFuncInvalidezPerm">Invalidez Permanente</label>
												</td>
												<td>
												   <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.funcVLInvalidezPerm}" id="txtFuncInvalidezPerm" size="15" maxlength="15"  tabindex="5">
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													  </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;"> 
													<label for="txtFuncLimiteIdade">Limite de Idade</label>
												</td>
												<td >
													<h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.funcLimiteIdade}" id="txtFuncLimiteIdade" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);"  size="3" maxlength="3" tabindex="6" />
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;"> 
													<label for="txtFuncVLIndividual">Valor Individual</label>
												</td>
												<td>
												   <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.funcVLIndividual}" id="txtFuncVLIndividual" size="15" maxlength="15"  tabindex="7">
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													  </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;"> 
													<label for="txtFuncVLCasado50">Valor Casado (50%)</label>
												</td>
												<td>
													<h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.funcVLCasado50}" id="txtFuncVLCasado50" size="15" maxlength="15"  tabindex="8">
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													  </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;"> 
													<label for="txtFuncVLCasado100">Valor Casado (100%)</label>
												</td>
												<td>
												   <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.funcVLCasado100}" id="txtFuncVLCasado100" size="15" maxlength="15"  tabindex="9">
													<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
												  </h:inputText>
												</td>													
											</tr>
								</table>
</td>
<td align="right" width="290px;" valign="top">
								<table>
											<tr>
												<td align="right" width="110px;"> 
													<label for="txtConjMorteNatural">Morte Natural</label>
												</td>
												<td>
												    <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.conjVLMorteNatural}" id="txtConjMorteNatural" size="15" maxlength="15"  tabindex="10">
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													  </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="110px;"> 
													<label for="txtConjMorteAcidente">Morte Por Acidente</label>
												</td>
												<td>
													 <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.conjVLMorteAcidente}" id="txtConjMorteAcidente" size="15" maxlength="15" tabindex="11" >
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													  </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="110px;"> 
													<label for="txtConjInvalidezPerm">Invalidez Permanente</label>
												</td>
												<td>
												    <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.conjVLInvalidezPerm}" id="txtConjInvalidezPerm" size="15" maxlength="15" tabindex="12" >
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
													 </h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="110px;"> 
													<label for="txtConjLimiteIdade">Limite de Idade</label>
												</td>
												<td>
													 <h:inputText value="#{capitalSeguradoLimiteIdadeHandler.capitalSeguradoLimiteIdade.conjLimiteIdade}" id="txtConjLimiteIdade" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" size="3" maxlength="3" tabindex="13"/>
												</td>										
											</tr>
								</table>
</td>
</tr>
</table>
						</fieldset>							
							<br />
							<p:commandButton actionListener="#{capitalSeguradoLimiteIdadeHandler.gravar}" value="Gravar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="14"/>
							<p:commandButton actionListener="#{capitalSeguradoLimiteIdadeHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="15">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>


						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
