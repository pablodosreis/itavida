<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="empresaHandler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Ap�lices</li> </div>
						<div id="icone">
                           <img src="images/empresa.png" />
                        </div>
						<h:form id="myform" styleClass="niceform" prependId="false">
						

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Consulta</legend>
								<table>
									<tr>
										<td align="right">
										  <label for="txtNomeEmpresa">Nome:</label>
										</td>
										<td>
										 <h:inputText value="#{empresaHandler.empresa.nomeEmpresa}" id="txtNomeEmpresa" size="40" maxlength="50" tabindex="1"/>
										  
										</td>
									</tr>
								</table>					
							</fieldset>							

							<br />
							
							<h:commandButton actionListener="#{empresaHandler.pesquisaEmpresas}" value="Consultar" id="pesquisar" styleClass="button" tabindex="3" />
							<p:commandButton actionListener="#{empresaHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
				<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty empresaHandler.resultadoPesquisa}"  constrainToViewport="true" fixedCenter="true" underlay="shadow" > 

									<p:dataTable id="resultTable" value="#{empresaHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty empresaHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.nomeEmpresa}">
										  <f:facet name="header">
										  <h:outputText value="Nome" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeEmpresa}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.numApolice}">
										  <f:facet name="header">
										  <h:outputText value="Ap�lice" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.numApolice}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.aniversario}">
										  <f:facet name="header">
										  <h:outputText value="In�cio da Ap�lice" /> 
										  </f:facet>
										  <h:outputText value="#{item.aniversario}">
										  	<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
										  </h:outputText>									  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  action="#{empresaHandler.preparaAlterarEmpresa}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:setPropertyActionListener target="#{empresaHandler.empresa}" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{empresaHandler.excluirEmpresa}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirEmpresa" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="Empresas"  excludeColumns="1,2,3"/>  
										</h:commandLink>
								</div>
						</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
