<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="contasVencerHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery("#dtDataVencimentoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataPagamentoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataVencimentoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataPagamentoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery('#txtValorDespesa').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Financeiro</span><li>Consultar Contas a Vencer</li> </div>
						<div id="icone">
                           <img src="images/financeiroContasVencer.png" />
                        </div>

						<h:form styleClass="niceform" prependId="false">
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
<br/><br/>
								<table>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtFilial">Filial:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencerFiltro.filial.nomeFilial}" id="txtFilial" size="40" tabindex="1"/>
											
											<rich:suggestionbox id="suggestionMotivoSinistro" 
								                	for="txtFilial"
								                    suggestionAction="#{contasVencerHandler.filialAutoComplete}" 
								                    var="filial"
								                    fetchValue="#{filial.nomeFilial}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{filial}" 
														target="#{contasVencerHandler.contasVencerFiltro.filial}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Nome da Filial" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.nomeFilial}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtDespesa">Despesa:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencerFiltro.despesa.nomeDespesa}" id="txtDespesa" size="40" tabindex="2"/>
											
											<rich:suggestionbox id="suggestionDespesa" 
								                	for="txtDespesa"
								                    suggestionAction="#{contasVencerHandler.despesaAutoComplete}" 
								                    var="despesa"
								                    fetchValue="#{despesa.nomeDespesa}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{despesa}" 
														target="#{contasVencerHandler.contasVencerFiltro.despesa}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Nome da Despesa" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{despesa.nomeDespesa}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Despesa Fixa" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{despesa.tipoDespesa == 'S' ? 'SIM' : 'N�O'}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtGrupoDespesa">Grupo de Despesa:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencerFiltro.grupoDespesa.nomeGrupoDespesa}" id="txtGrupoDespesa" size="40" tabindex="3"/>
											
											<rich:suggestionbox id="suggestionGrupoDespesa" 
								                	for="txtGrupoDespesa"
								                    suggestionAction="#{contasVencerHandler.grupoDespesaAutoComplete}" 
								                    var="grupoDespesa"
								                    fetchValue="#{grupoDespesa.nomeGrupoDespesa}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{grupoDespesa}" 
														target="#{contasVencerHandler.contasVencerFiltro.grupoDespesa}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
										  				<h:outputText value="Nome do Grupo Despesa" /> 
										  	
										  			</f:facet>
										  					<h:outputText value="#{grupoDespesa.nomeGrupoDespesa}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtFuncionario">Funcion�rio:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencerFiltro.funcionario.nome}" id="txtFuncionario" size="40" tabindex="4"/>
											
											<rich:suggestionbox id="suggestionFuncionario" 
								                	for="txtFuncionario"
								                    suggestionAction="#{contasVencerHandler.funcionarioAutoComplete}" 
								                    var="funcionario"
								                    fetchValue="#{funcionario.nome}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{funcionario}" 
														target="#{contasVencerHandler.contasVencerFiltro.funcionario}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Funcion�rio" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>
								         	</rich:suggestionbox>
										</td> 
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtValorDespesa">Valor:</label>
										</td>
										<td>
										   <h:inputText value="#{contasVencerHandler.contasVencerFiltro.valorDespesa}" id="txtValorDespesa" size="15" maxlength="15" tabindex="5">
													<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>
										</td>											
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataVencimentoInicio" >Data do Vencimento (In�cio):</label>
										</td>
										<td>
											<h:inputText value="#{contasVencerHandler.contasVencerFiltro.dataVencimentoInicio}"  id="dtDataVencimentoInicio" maxlength="10"  tabindex="6">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
											</h:inputText>
										</td>
										<td align="right" width="150px;">
										  <label for="dtDataVencimentoFim" >Data do Vencimento (Fim):</label>
										</td>
										<td>
											<h:inputText value="#{contasVencerHandler.contasVencerFiltro.dataVencimentoFim}"  id="dtDataVencimentoFim" maxlength="10"  tabindex="7">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
											</h:inputText>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataPagamentoInicio" >Data do Pagamento (In�cio):</label>
										</td>
										<td>
											<h:inputText value="#{contasVencerHandler.contasVencerFiltro.dataPagamentoInicio}"  id="dtDataPagamentoInicio" maxlength="10"  tabindex="8">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
											</h:inputText>
										</td>
										<td align="right" width="150px;">
										  <label for="dtDataPagamentoFim" >Data do Pagamento (Fim):</label>
										</td>
										<td>
											<h:inputText value="#{contasVencerHandler.contasVencerFiltro.dataPagamentoFim}"  id="dtDataPagamentoFim" maxlength="10"  tabindex="9">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
											</h:inputText>
										</td>
									</tr>
								</table>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										   <label for="cmbContaPaga">Conta Paga:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbContaPaga" value="#{contasVencerHandler.contasVencerFiltro.contaPaga}" tabindex="10">
										   	<f:selectItems value="#{contasVencerHandler.simNaoCombo}"/>
										   </h:selectOneMenu>
										</td> 

									</tr> 
								</table>
							</fieldset>							
							<br />
							
							<h:commandButton actionListener="#{contasVencerHandler.pesquisaContasVencers}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="11"/>
							<p:commandButton actionListener="#{contasVencerHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="12">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty contasVencerHandler.resultadoPesquisa}" constrainToViewport="true" fixedCenter="true" underlay="shadow"> 
									<p:dataTable value="#{contasVencerHandler.resultadoPesquisa}" 
												 id="resultTable"
												 var="item" width="100%" 
												 rendered="#{not empty contasVencerHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.filial.nomeFilial}" >
										  <f:facet name="header">
										  <h:outputText value="Filial" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.filial.nomeFilial}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.despesa.nomeDespesa}">
										  <f:facet name="header">
										  <h:outputText value="Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.despesa.nomeDespesa}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.grupoDespesa.nomeGrupoDespesa}">
										  <f:facet name="header">
										  <h:outputText value="Grupo de Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.grupoDespesa.nomeGrupoDespesa}"/>									  										
										</p:column>	
										<p:column resizable="true"  sortBy="#{item.funcionario.nome}" >
										  <f:facet name="header">
										  <h:outputText value="Funcion�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.funcionario.nome}"/>									  										
										</p:column>	

										<p:column resizable="true" sortBy="#{item.valorDespesa}" >
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  </f:facet>
										  <h:outputText value="#{item.valorDespesa}" >
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>	
										   </h:outputText>								  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.dataVencimento}">
										  <f:facet name="header">
											  <h:outputText value="Data Vencimento" />
										  </f:facet>
										  <h:outputText value="#{item.dataVencimento}" >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />			
										   </h:outputText>
										</p:column>	

										<p:column resizable="true" sortBy="#{item.contaPaga}">
										  <f:facet name="header">
										  <h:outputText value="Conta Paga" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.contaPaga == 'S' ? 'SIM' : 'N�O'}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.dataPagamento}">
										  <f:facet name="header">
											  <h:outputText value="Data Pagamento" />
										  </f:facet>
										  <h:outputText value="#{item.dataPagamento}" >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />			
										   </h:outputText>
										</p:column>		
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink action="#{contasVencerHandler.preparaAlterarContasVencer}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
											<f:setPropertyActionListener target="#{contasVencerHandler.contasVencer}" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink id="linkExcluir" actionListener="#{contasVencerHandler.excluirContasVencer}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirContasVencer" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
									<div align="right">
											<h:commandLink >  
											    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
											    <p:dataExporter type="xls" target="resultTable" fileName="Contas_a_Vencer"  postProcessor="#{pagamentoHandler.postProcessXLS}" excludeColumns="8,9"/>  
											</h:commandLink>
									</div>
							</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
