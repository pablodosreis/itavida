<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="controleComercialHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery("#dtDataVisitaChegada").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtDataVisitaSaida").mask("99/99/9999",{placeholder:"_"});
	jQuery('#txtCustoViagem').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
	});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Controle</span><li>Controle Comercial  (Incluir/Consultar)</li> </div>
						<div id="icone">
                           <img src="images/controleComercial.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false">


					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao">�rg�o:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{controleComercialHandler.controleComercial.orgao.nomeOrgao}" id="txtOrgao" size="40" tabindex="1"> 
												<p:ajax event="blur" id="a4jsupport" actionListener="#{controleComercialHandler.obterQuantidadeVidasAtivas}" update="txtNumeroVidasAtivas" />
												<p:ajax event="keyup" update="renderizado" >
													<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{controleComercialHandler.limparOrgao}" 
														target="#{controleComercialHandler.controleComercial.orgao}"/> 
												</p:ajax>

											</h:inputText>
											<rich:suggestionbox id="suggestionOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{controleComercialHandler.orgaoAutoComplete}" 
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{controleComercialHandler.controleComercial.orgao}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Org�o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.nomeOrgao}"/>									  										
													</h:column>
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Sigla" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.siglaOrgao}"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroVidasAtivas">N�mero Vidas Ativas:</label> 
										</td> 
										<td> 
										  <b> <h:outputText value="#{controleComercialHandler.controleComercial.numeroVidasAtivas}" id="txtNumeroVidasAtivas" /></b>
										</td> 
									</tr> 
								</table>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroFuncionarios">N�mero Funcion�rios:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{controleComercialHandler.controleComercial.numeroFuncionarios}" id="txtNumeroFuncionarios" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="2"/>
										</td> 
									</tr> 
								</table>	
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtAgenciador">Agenciador:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{controleComercialHandler.controleComercial.funcionario.nome}" id="txtAgenciador" size="40" tabindex="3">
												<p:ajax event="keyup" update="renderizado" >
													<f:setPropertyActionListener 
														value="#{controleComercialHandler.limparFuncionario}" 
														target="#{controleComercialHandler.controleComercial.funcionario}"/> 
												</p:ajax>
										   </h:inputText>
											
												<rich:suggestionbox id="suggestionFuncionario" 
								                	for="txtAgenciador"
								                    suggestionAction="#{controleComercialHandler.funcionarioAutoComplete}" 
								                    var="funcionario"
								                    fetchValue="#{funcionario.nome}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{funcionario}" 
														target="#{controleComercialHandler.controleComercial.funcionario}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Funcion�rio" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>
								         	</rich:suggestionbox>
										</td> 
									</tr> 
								</table> 
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtCustoViagem">Custo da Viagem:</label> 
										</td> 
										<td> 
										   <h:inputText  value="#{controleComercialHandler.controleComercial.custos}" id="txtCustoViagem" size="10" maxlength="15" tabindex="4">
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>
										</td> 
									</tr> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroProducao">Produ��o da Viagem:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{controleComercialHandler.controleComercial.producao}" id="txtNumeroProducao" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="5"/>
										</td> 
									</tr> 
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataVisitaChegada" >Data da Visita Chegada:</label>
										</td>
										<td>
											<h:inputText value="#{controleComercialHandler.controleComercial.dataVisitaChegada}"  id="dtDataVisitaChegada" maxlength="10"  tabindex="6">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataVisitaSaida" >Data da Visita Sa�da </label>
										</td>
										<td>
											<h:inputText value="#{controleComercialHandler.controleComercial.dataVisitaSaida}"  id="dtDataVisitaSaida" maxlength="10"  tabindex="7">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
								</table>
						</fieldset>							
							<br />
							<p:commandButton actionListener="#{controleComercialHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="8"/>
							<p:commandButton action="#{controleComercialHandler.cancelarInclusao}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="9">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
