<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="fatorHandler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
	 jQuery("#dataInicioFator").mask("99/99/9999",{placeholder:"_"});
	 jQuery('#txtFator').priceFormat({
		    prefix: '',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Consulta</span><li>Reajuste por Faixa Et�ria</li> </div>
						<div id="icone">
                           <img src="images/empresa.png" />
                        </div>
						<h:form id="myform" styleClass="niceform" prependId="false">
						

					<div class="titulo_agrupamento_Dados" ></div>
								<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="txtNomeTabela">Nome da Tabela:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{fatorHandler.nomeTabela.nomeTabela}" id="txtNomeTabela" size="30"  >
												<p:ajax event="keyup" update="renderizado" >
													<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{fatorHandler.limparNomeTabela}" 
														target="#{fatorHandler.nomeTabela}" /> 
												</p:ajax>
										   </h:inputText>
										   
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeTabela" 
								                    suggestionAction="#{fatorHandler.nomeTabelaAutocomplete}" 
								                    var="nomeTabela" 
								                    fetchValue="#{nomeTabela.nomeTabela}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" 
								                    	action="#{fatorHandler.carregaComboCapitais}" reRender="boxCapitais"> 
														<f:setPropertyActionListener 
														value="#{nomeTabela}" 
														target="#{fatorHandler.nomeTabela}"/> 
													</a4j:support> 

								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabela}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabelaReduzido}" styleClass="autocompleteFonte"/>
								                    </h:column>
								             </rich:suggestionbox>											
										</td> 

									</tr> 
									<!-- 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="dtDataVigencia">Data Vig�ncia:</label> 
										</td> 
										<td>
											<h:inputText value="#{fator.capitalSeguradoFaixaEtaria.dataVigencia}"  id="dtDataVigencia" maxlength="10" tabindex="2">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
									 -->										
								</table>	
						
						
						<!-- <fieldset>
								<legend>Capital Segurado - Faixa Et�ria</legend>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="boxCapitais">Capital Segurado:</label> 
										</td> 
										<td> 
											<h:selectOneMenu  id="boxCapitais" binding="#{fatorHandler.capitalSegurado}" value="#{fatorHandler.idCapitalSeguradoFaixaEtaria}">
												<f:selectItems value="#{fatorHandler.listaCapitaisFaixaExtarias}" />
											</h:selectOneMenu>
										   									
										</td> 

									</tr> 
								</table>	
						</fieldset> -->	
					
								<table> 
									<!-- <tr> 
										<td align="right" width="90px;"> 
										  <label for="txtFator">Fator:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{fatorHandler.fator.fator}" id="txtFator" >
												<f:convertNumber />
										   </h:inputText>
										  									
										</td> 

									</tr>  -->
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="dataInicioFator">Data de In�cio:</label> 
										</td> 
										<td>
											<h:inputText value="#{fatorHandler.fator.dataInicio}"  id="dataInicioFator" maxlength="10" >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>										
								</table>					
						</fieldset>
						
							<br />
							
							<h:commandButton actionListener="#{fatorHandler.pesquisaFatores}" value="Consultar" id="pesquisar" styleClass="button" tabindex="3" />
							<p:commandButton actionListener="#{fatorHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
				<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty fatorHandler.resultadoPesquisa}"  constrainToViewport="true" fixedCenter="true" underlay="shadow" > 

									<p:dataTable id="resultTable" value="#{fatorHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty fatorHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.dataInicio}">
										  <f:facet name="header">
										  <h:outputText value="Data de In�cio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataInicio}">
										  <f:convertDateTime pattern="dd/MM/yyyy"/>
										  </h:outputText>										  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.fator}">
										  <f:facet name="header">
										  <h:outputText value="Fator" >
										  </h:outputText> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.fator}">
										  </h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.capitalSeguradoFaixaEtaria.nomeTabela.nomeTabela}">
										  <f:facet name="header">
										  <h:outputText value="Capital Segurado" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.capitalSeguradoFaixaEtaria.nomeTabela.nomeTabela}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.capitalSeguradoFaixaEtaria.dataVigencia}">
										  <f:facet name="header">
										  <h:outputText value="Data de Vig�ncia" >
										  	
										  </h:outputText>
										  	
										  </f:facet>
										  <h:outputText value="#{item.capitalSeguradoFaixaEtaria.dataVigencia}">
										  <f:convertDateTime pattern="dd/MM/yyyy"/>
										  </h:outputText>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.processado}">
										  <f:facet name="header">
										  <h:outputText value="J� Processado ?" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.processado ? 'Sim' : 'N�o'}"/>									  										
										</p:column>
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  action="#{fatorHandler.prepararAlterarFator2}" rendered="#{!item.processado}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:setPropertyActionListener target="#{fatorHandler.fator}" value="#{item}"/>
										  	<f:setPropertyActionListener target="#{fatorHandler.nomeTabela}" value="#{item.capitalSeguradoFaixaEtaria.nomeTabela}"/>
										  	<f:setPropertyActionListener target="#{fatorHandler.capitalSeguradoFaixaEtaria}" value="#{item.capitalSeguradoFaixaEtaria}"/>
										  	
										  </h:commandLink>	
										  <h:commandLink  actionListener="#{fatorHandler.prepararAlterarFator}" rendered="#{item.processado}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:param id="alterarFator" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{fatorHandler.excluirFator}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirFator" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="Fatores"  excludeColumns="1,2,3"/>  
										</h:commandLink>
								</div>
						</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
