<%@ include file="/common/cabecalho.jsp"%>
<a name="PROPOSTA"></a>

<script type="text/javascript">


function controlarCampos() {
	
	
	var element = document.getElementById('formElem:cmbModeloProposta');
	var i = element.selectedIndex;
	var option = element.options[i];


	var selecao = option.value;
	
	if (selecao == 1 || selecao == 2) {
		$('#valPremioSegurado').show();
		$('#valPremioConjuge').show();
		jQuery('#valPremioSeguradoFuturo').show();
		jQuery('#lblPremioConjugeFuturo').show();
		
		jQuery('#valCapitalSegurado').show();
		jQuery('#valCapitalConjuge').show();
		jQuery('#valCapitaleguradoFuturo').show();
		jQuery('#lblCapitalConjugeFuturo').show();
	} else {
		jQuery('#valPremioSegurado').hide();
		jQuery('#valPremioConjuge').hide();
		jQuery('#valPremioSeguradoFuturo').hide();
		jQuery('#lblPremioConjugeFuturo').hide();
		
		jQuery('#valCapitalSegurado').hide();
		jQuery('#valCapitalConjuge').hide();
		jQuery('#valCapitaleguradoFuturo').hide();
		jQuery('#lblCapitalConjugeFuturo').hide();
	}

	/*
	//VALOR FIXO
	if (jQuery('#cmbModeloProposta').val() == 1) {
		jQuery('#txtCapital1td').show();
		jQuery('#txtCapital1').show();
		jQuery('#tableValorFuturoSegurado').hide();
		jQuery('#tableValorFuturoFixo').show();
		jQuery('#tableValorConjugeFuturoFixo').show();
		jQuery('#labelCapitalFuturoTD').hide();
		jQuery('#labelPremioFuturoTD').hide();
		jQuery('#txtCapitalConjugeFuturo').hide();
		
	} else {
		jQuery('#txtCapital1').hide();
		jQuery('#txtCapital1td').hide();
		jQuery('#tableValorFuturoSegurado').show();
		jQuery('#tableValorFuturoFixo').hide();
		jQuery('#tableValorConjugeFuturoFixo').hide();
		jQuery('#labelCapitalFuturoTD').show();
		jQuery('#labelPremioFuturoTD').show();
		jQuery('#txtCapitalConjugeFuturo').show();
	}

	
	//VALOR FAIXA ET�RIA
	if (jQuery('#cmbModeloProposta').val() == 2) {
		jQuery('#txtCapital2td').show();
		jQuery('#txtCapital2').show();
		jQuery('#txtCapitalCapitalFuturoFaixaEtariatd').show();
		jQuery('#txtPremioConjugeFuturotd').show();
		
	} else {
		jQuery('#txtCapital2td').hide();
		jQuery('#txtCapital2').hide();
		jQuery('#txtCapitalCapitalFuturoFaixaEtariatd').hide();
		jQuery('#txtPremioConjugeFuturotd').hide();

	}

	//REANGARIADO OU ATUALIZADO
	if (jQuery('#cmbModeloProposta').val() == 3 || jQuery('#cmbModeloProposta').val() == 4) {
		jQuery('#labelNomeTabela').hide();
		jQuery('#txtNomeTabela').hide();
		jQuery('#panelTabelaCap').hide();
		jQuery('#txtCapital3td').show();
		jQuery('#txtCapital3').show();
		jQuery('#txtPremioConjugeDigitadotd').show();
		jQuery('#txtPremioConjugetd').hide();
		jQuery('#txtPremioSegurado22td').show();
		jQuery('#txtPremioSegurado22').show();
		jQuery('#txtPremioSeguradoFuturotd').show();
		jQuery('#txtPremioConjugeDigitadoFuturotd').show();
	} else {
		jQuery('#txtNomeTabela').show();
		jQuery('#labelNomeTabela').show();
		jQuery('#panelTabelaCap').show();
		jQuery('#txtCapital3td').hide();
		jQuery('#txtCapital3').hide();
		jQuery('#txtPremioSeguradoFuturotd').hide();
		jQuery('#txtPremioConjugeDigitadotd').hide();
		jQuery('#txtPremioConjugetd').show();
		jQuery('#txtPremioSegurado22').hide();
		jQuery('#txtPremioSegurado22td').hide();
		jQuery('#txtPremioConjugeDigitadoFuturotd').hide();
	}*/
}

</script>
<div>
	<p>
		<h:outputLabel for="cmbEmpresa" styleClass="campo" value="Ap�lice:" />
		<h:selectOneMenu id="cmbEmpresa" styleClass="campo" style="width: 250px"
						value="#{novaPropostaHandler.proposta.empresa.id}">
						<f:selectItems value="#{novaPropostaHandler.opcoesEmpresaCombo}" />
		</h:selectOneMenu>
	</p>
	<br/>
	<p>
		<label for="cmbModeloProposta" class="campo">Modelo de Proposta:</label>
		<h:selectOneMenu id="cmbModeloProposta" styleClass="campo" style="width: 250px"
				value="#{novaPropostaHandler.proposta.modeloProposta.id}" immediate="true" valueChangeListener="#{novaPropostaHandler.modeloPropostaComboChangedValue}">
				<f:selectItems id="selectItensModeloProposta"
						value="#{novaPropostaHandler.modeloPropostaCombo}" />
				<a4j:support event="onchange"  reRender="cmbTipoDeProposta" oncomplete="controlarCampos()"/>
		</h:selectOneMenu>
	</p>
	<br/>
	<p>
		<label for="lblTipoPropostaProposta" class="campo">Tipo de Proposta:</label>
		<h:selectOneMenu id="cmbTipoDeProposta" styleClass="campo" style="width: 250px"
			value="#{novaPropostaHandler.proposta.tipoProposta.id}">
			<f:selectItems id="selectItensTipoProposta"
					value="#{novaPropostaHandler.tipoPropostaComboFiltro}" />
	
	<!-- 					<a4j:support
							actionListener="#{novaPropostaHandler.ajaxChangeTipoProposta}"
							reRender="txtCapital3, txtPremioSegurado, txtCapital1, txtCapital2"
							ajaxSingle="true" event="onchange"
							oncomplete=" jQuery('#txtOrgao').focus();" /> -->
		</h:selectOneMenu>
	</p>
	<br/>
	<p>
		<label for="lblOrgao" class="campo">Org�o: </label>
		<h:selectOneMenu id="cmbOrgao" styleClass="campo" style="width: 250px"
						value="novaPropostaHandler.proposta.orgao.id" >
						<f:selectItems value="#{novaPropostaHandler.opcoesOrgaoCombo}" />
		</h:selectOneMenu>
	</p>
	<br/>
	<p>
		<label for="lblTabela" class="campo">Tabela de Capitais:</label>
		<h:selectOneMenu id="cmbTabela" styleClass="campo" style="width: 250px"
						value="#{novaPropostaHandler.proposta.nomeTabela.id}">
						<f:selectItems value="#{novaPropostaHandler.opcoesEmpresaCombo}" />
		</h:selectOneMenu>
	</p>
	<br/>
	<h1>Valores Atuais</h1>
	<p>
		<label for="lblPremioSegurado" class="campo">Pr�mio Segurado:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorPremioSegurado}"
					id="valPremioSegurado" size="15" maxlength="20" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
		<h:outputLabel id="lblPremioSegurado" value="#{novaPropostaHandler.proposta.valorPremioSegurado}" style="display: none"/>
		
					
		<label for="lblPremioConjuge" class="campo">Pr�mio C�njuge:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorPremioConjuge}"
					id="valPremioConjuge" size="15" maxlength="20" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
		<h:outputLabel id="lblPremioConjuge" value="#{novaPropostaHandler.proposta.valorPremioConjuge}" style="display: none"/>
	</p>
	
	<br/>
	<p>
		<label for="lblCapitalSegurado" class="campo">Capital Segurado:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorCapitalSegurado}"
					id="valCapitalSegurado" size="40" maxlength="50" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
		<h:outputLabel id="lblCapitalSegurado" value="#{novaPropostaHandler.proposta.valorCapitalSegurado}" style="display: none"/>
					
		<label for="lblCapitalConjuge" class="campo">Capital C�njuge:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorCapitalConjuge}"
					id="valCapitalConjuge" size="40" maxlength="50" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
		<h:outputLabel id="lblCapitalConjuge" value="#{novaPropostaHandler.proposta.valorCapitalConjuge}" style="display: none"/>
	</p>	
	
	<h1>Valores Futuros</h1>
	<br/>
	<p>
		<label for="lblPremioSeguradoFuturo" class="campo">Pr�mio Segurado:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorFuturoPremioSegurado}"
					id="valPremioSeguradoFuturo" size="15" maxlength="20" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
					
		<label for="lblPremioConjugeFuturo" class="campo">Pr�mio C�njuge:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorFuturoPremioConjuge}"
					id="valPremioConjugeFuturo" size="15" maxlength="20" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
	</p>
	
	<br/>
	<p>
		<label for="lblCapitalSeguradoFuturo" class="campo">Capital Segurado:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorFuturoCapitalSegurado}"
					id="valCapitalSeguradoFuturo" size="40" maxlength="50" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
					
		<label for="lblCapitalConjugeFuturo" class="campo">Capital C�njuge:</label>
		<h:inputText styleClass="campo"
					value="#{novaPropostaHandler.proposta.valorFuturoCapitalConjuge}"
					id="valCapitalConjugeFuturo" size="40" maxlength="50" >
					<f:converter converterId="MyDoubleConverter" />
		</h:inputText>
	</p>
</div>