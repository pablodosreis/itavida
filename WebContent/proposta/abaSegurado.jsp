<%@ include file="/common/cabecalho.jsp"%>
	<!--  nome do segurado -->
<%-- 	<a4j:keepAlive beanName="novaPropostaHandler" /> --%>
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="txtNomeSeguradoPrincipal">Nome:</label></td>
			<td><h:inputText
					value="#{novaPropostaHandler.proposta.nomeSegurado}"
					id="txtNomeSeguradoPrincipal" size="40" maxlength="50" /></td>
		</tr>
		<tr>
			<td align="right" width="110px;"><label for="txtCpf">CPF:</label>
			</td>
			<td><h:inputText value="#{novaPropostaHandler.proposta.cpfSegurado}"
					id="txtCpf" size="20" maxlength="14" >
					<h:panelGroup id="cpfMessage">
						<p:dialog header="Aten��o: A��o Bloqueada!" widgetVar="dialog"
							fixedCenter="true" modal="true"
							visible="#{novaPropostaHandler.visibleMessage}"
							close="#{novaPropostaHandler.mostrarClose}">
							<h:messages errorClass="mensagem_erro" fatalClass="mensagem_erro"
								infoClass="mensagem_sucesso" warnClass="mensagem_alerta" />
							<h:panelGroup style="width: 100%; text-align:center; "
								layout="block">
								<p:commandButton
									actionListener="#{novaPropostaHandler.cancelarInclusao}"
									value="Voltar" id="voltar" styleClass="button" async="false"
									ajax="false" rendered="#{!novaPropostaHandler.mostrarClose}" />
							</h:panelGroup>
						</p:dialog>
					</h:panelGroup>
					<p:ajax actionListener="#{novaPropostaHandler.verificaCPFDuplicados}"
						event="change" update="cpfMessage" async="false" />

				</h:inputText></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtEmail">Email:</label></td>
			<td><h:inputText value="#{novaPropostaHandler.proposta.email}"
					id="txtEmail" size="30" maxlength="100"  />
			</td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="cmbSexoSegurado">Sexo:</label></td>
			<td><h:selectOneMenu id="cmbSexoSegurado"
					value="#{novaPropostaHandler.proposta.sexoSegurado}" >
					<f:selectItems value="#{novaPropostaHandler.sexoCombo}" />
				</h:selectOneMenu></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label
				for="dtNascimentoSegurado">Data Nascimento:</label></td>
			<td><h:inputText
					value="#{novaPropostaHandler.proposta.dataNascimentoSegurado}"
					id="dtNascimentoSegurado" maxlength="10" >
					<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
					<p:ajax
						actionListener="#{novaPropostaHandler.calculaIdadeSeguradoPrincipal}"
						update="txtIdadeSegurado" event="blur" />
				</h:inputText>
				<h:outputText id="txtIdadeSegurado"
					value="#{novaPropostaHandler.idadeSegurado}" />
			</td>

		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtRGSegurado">RG:</label></td>
			<td><h:inputText value="#{novaPropostaHandler.proposta.rgSegurado}"
					id="txtRGSegurado" size="20" maxlength="20"  /></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtMatricula">Matr�cula:</label></td>
			<td><h:inputText
					value="#{novaPropostaHandler.proposta.matriculaSegurado}"
					id="txtMatricula" size="10" maxlength="10"  /></td>
			
		</tr>
		<tr>
			<td align="right" width="150px;"><label
				for="cmbEstadoCivilSegurado">Estado Civil:</label></td>
			<td><h:selectOneMenu id="cmbEstadoCivilSegurado"
					value="#{novaPropostaHandler.proposta.estadoCivilSegurado}"
					>
					<f:selectItems value="#{novaPropostaHandler.estadoCivilCombo}" />
				</h:selectOneMenu></td>
		</tr>
	</table>

	<!--   HIST�RICO DO SEGURADO -->
	<h:panelGroup id="agrupamento_Historico_Capital"
		styleClass="agrupamento_Dados">
		<fieldset>
			<legend>
				<IMG title="Expandir" id="expandirconteudoHistoricoCapital"
					alt="Expandir" onclick="expandir('conteudoHistoricoCapital');"
					src="images/expandir.png" border=0 /><IMG
					id="retrairconteudoHistoricoCapital" title="Retrair" alt="Retrair"
					onclick="retrair('conteudoHistoricoCapital');"
					src="images/retrair.png" border=0 style="display: none;" />
				Hist�rico de Altera��es de Capitais
			</legend>
			<div id="conteudoHistoricoCapital" style="display: none;">
				<p:dataTable
					value="#{novaPropostaHandler.proposta.historicocapital_collection}"
					var="historicoCapital" width="50%"
					rendered="#{not empty novaPropostaHandler.proposta.historicocapital_collection}">

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Data Altera��o" />

						</f:facet>
						<h:outputText value="#{historicoCapital.dataAlteracao}"
							style="text-transform:uppercase;">
							<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" />
						</h:outputText>
					</p:column>
					
					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Nome Segurado" />
						</f:facet>

						<h:outputText value="#{historicoCapital.nomeSegurado}" />
					</p:column>
					
					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Capital Antigo" />

						</f:facet>
						<h:outputText value="#{historicoCapital.capitalAntigo}">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</p:column>
					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Pr�mio Antigo" />


						</f:facet>
						<h:outputText value="#{historicoCapital.premioAntigo}">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Sexo" />
						</f:facet>

						<h:outputText value="#{historicoCapital.sexoExibicao}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Data de Nascimento">
								<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
							</h:outputText>
						</f:facet>

						<h:outputText value="#{historicoCapital.dataNascimento}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="CPF" />
						</f:facet>

						<h:outputText value="#{historicoCapital.cpf}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="RG" />
						</f:facet>

						<h:outputText value="#{historicoCapital.rg}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Estado Civil" />
						</f:facet>

						<h:outputText value="#{historicoCapital.estadoCivilExibicao}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Usu�rio Altera��o" />

						</f:facet>
						<h:outputText value="#{historicoCapital.nomeUsuario}" />
					</p:column>
				</p:dataTable>
			</div>
		</fieldset>
	</h:panelGroup>
