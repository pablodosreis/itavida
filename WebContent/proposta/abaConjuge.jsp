<%@ include file="/common/cabecalho.jsp"%>

<script type="text/javascript">
jQuery(function(jQuery) {
	jQuery("#txtCPFConjuge").mask("999.999.999-99", {
		placeholder : "_"
	});
	jQuery("#dtNascimentoConjuge").mask("99/99/9999", {
		placeholder : "_"
	});
}
</script>
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="txtNomeSeguradoConjuge">Nome:</label></td>
			<td><h:inputText value="#{novaPropostaHandler.proposta.nomeConjuge}"
					id="txtNomeSeguradoConjuge" size="50" maxlength="50" /></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtCPFConjuge">CPF:</label></td>
			<td><h:inputText value="#{novaPropostaHandler.proposta.cpfConjuge}"
					id="txtCPFConjuge" size="20" maxlength="14">
					<h:panelGroup id="cpfMessageConjuge">
						<p:dialog header="Aten��o: A��o Bloqueada!" widgetVar="dialog"
							fixedCenter="true" modal="true"
							visible="#{novaPropostaHandler.visibleMessage}"
							close="#{novaPropostaHandler.mostrarClose}">
							<h:messages errorClass="mensagem_erro" fatalClass="mensagem_erro"
								infoClass="mensagem_sucesso" warnClass="mensagem_alerta" />
							<h:panelGroup style="width: 100%; text-align:center; "
								layout="block">
								<p:commandButton
									actionListener="#{novaPropostaHandler.cancelarInclusao}"
									value="Voltar" id="voltarConjuge" styleClass="button"
									async="false" ajax="false"
									rendered="#{!novaPropostaHandler.mostrarClose}" />
							</h:panelGroup>
						</p:dialog>
					</h:panelGroup>
					<p:ajax
						actionListener="#{novaPropostaHandler.verificaCPFDuplicadosConjuge}"
						event="blur" update="cpfMessageConjuge" async="false" />

				</h:inputText></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="cmbSexoConjuge">Sexo:</label></td>
			<td><h:selectOneMenu id="cmbSexoConjuge"
					value="#{novaPropostaHandler.proposta.sexoConjuge}" >
					<f:selectItems value="#{novaPropostaHandler.sexoCombo}" />
				</h:selectOneMenu></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label
				for="dtNascimentoConjuge">Data Nascimento:</label></td>
			<td><h:inputText
					value="#{novaPropostaHandler.proposta.dataNascimentoConjuge}"
					id="dtNascimentoConjuge" maxlength="10" >
					<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
					<p:ajax
						actionListener="#{novaPropostaHandler.calculaIdadeSeguradoConjuge}"
						update="txtIdadeConjuge" event="blur" />
				</h:inputText>
				<h:outputText id="txtIdadeConjuge"
					value="#{novaPropostaHandler.idadeConjuge}" />
			</td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtRGConjuge">RG:</label></td>
			<td><h:inputText value="#{novaPropostaHandler.proposta.rgConjuge}"
					id="txtRGConjuge" size="20" maxlength="20" /></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label
				for="txtMatriculaConjuge">Matr�cula:</label></td>
			<td><h:inputText
					value="#{novaPropostaHandler.proposta.matriculaConjuge}"
					id="txtMatriculaConjuge" size="10" maxlength="10"  /></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label
				for="cmbEstadoCivilConjuge">Estado Civil:</label></td>
			<td><h:selectOneMenu id="cmbEstadoCivilConjuge"
					value="#{novaPropostaHandler.proposta.estadoCivilConjuge}"
					>
					<f:selectItems value="#{novaPropostaHandler.estadoCivilCombo}" />
				</h:selectOneMenu></td>
		</tr>
		
	</table>

<rich:spacer height="20px;" />
<h:panelGroup id="agrupamento_Historico_Conjuge"
	styleClass="agrupamento_Dados">
	<fieldset>
		<legend>
			<IMG title="Expandir" id="expandirconteudoHistoricoConjuge"
				alt="Expandir" onclick="expandir('conteudoHistoricoConjuge');"
				src="images/expandir.png" border=0 /> <IMG
				id="retrairconteudoHistoricoConjuge" title="Retrair" alt="Retrair"
				onclick="retrair('conteudoHistoricoConjuge');"
				src="images/retrair.png" border=0 style="display: none;" />
			Hist�rico de Altera��es C�njuge
		</legend>
		<div id="conteudoHistoricoConjuge" style="display: none;">
			<p:dataTable
				value="#{novaPropostaHandler.proposta.historicoconjuge_collection}"
				var="historicoConjuge" width="50%"
				rendered="#{not empty novaPropostaHandler.proposta.historicoconjuge_collection}">

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Data Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.dataAlteracao}"
						style="text-transform:uppercase;">
						<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" />
					</h:outputText>
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Nome" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.nomeConjuge}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Sexo" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.sexoExibicao}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Cpf" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.cpfConjuge}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="RG" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.rgConjuge}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Matr�cula" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.matriculaConjuge}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Pr�mio" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.premio}">
						<f:convertNumber currencySymbol="R$" type="currency"
							groupingUsed="true" />
					</h:outputText>
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Capital" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.capital}">
						<f:convertNumber currencySymbol="R$" type="currency"
							groupingUsed="true" />
					</h:outputText>
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Data de Nascimento" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.dataNascimentoConjuge}">
						<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
					</h:outputText>
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Estado Civil" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.estadoCivilExibicao}" />
				</p:column>


				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Usu�rio Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.nomeUsuario}" />
				</p:column>
			</p:dataTable>
		</div>
	</fieldset>
</h:panelGroup>