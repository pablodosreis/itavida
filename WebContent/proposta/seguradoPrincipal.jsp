
<%@ include file="/common/cabecalho.jsp"%>
<fieldset>
	<legend>Dados do Segurado Principal</legend>

	<!--  nome do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="txtNomeSeguradoPrincipal">Nome:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.nomeSegurado}"
					id="txtNomeSeguradoPrincipal" size="40" maxlength="50"  /></td>
		</tr>
	</table>

	<!--  email do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtEmail">Email:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.email}"
					id="txtEmail" size="30" maxlength="100"  /></td>
			<td><a href="javascript:sendMail()" >Enviar
					Email</a></td>
		</tr>
	</table>

	<!--  sexo do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="cmbSexoSegurado">Sexo:</label></td>
			<td><h:selectOneMenu id="cmbSexoSegurado"
					value="#{propostaHandler.proposta.sexoSegurado}" >
					<f:selectItems value="#{propostaHandler.sexoCombo}" />
				</h:selectOneMenu></td>
			<td align="right" width="150px;"><label
				for="dtNascimentoSegurado">Data Nascimento:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.dataNascimentoSegurado}"
					id="dtNascimentoSegurado" maxlength="10" >
					<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
					<p:ajax
						actionListener="#{propostaHandler.calculaIdadeSeguradoPrincipal}"
						update="txtIdadeSegurado" event="blur" />
				</h:inputText></td>
			<td><h:outputText id="txtIdadeSegurado"
					value="#{propostaHandler.idadeSegurado}" /></td>

		</tr>
	</table>

	<!--  matr�cula do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtMatricula">Matr�cula:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.matriculaSegurado}"
					id="txtMatricula" size="10" maxlength="10"  /></td>
			<td align="right" width="110px;"><label for="txtCpf">CPF:</label>
			</td>
			<td><h:inputText value="#{propostaHandler.proposta.cpfSegurado}"
					id="txtCpf" size="20" maxlength="14" >
				</h:inputText></td>
		</tr>
	</table>

	<!--  RG DO SEGURADO -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtRGSegurado">RG:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.rgSegurado}"
					id="txtRGSegurado" size="20" maxlength="20"  /></td>
			<td align="right" width="150px;"><label
				for="cmbEstadoCivilSegurado">Estado Civil:</label></td>
			<td><h:selectOneMenu id="cmbEstadoCivilSegurado"
					value="#{propostaHandler.proposta.estadoCivilSegurado}"
					>
					<f:selectItems value="#{propostaHandler.estadoCivilCombo}" />
				</h:selectOneMenu></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label for="lblProfissaoSegurado">Profiss�o:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.profissaoSegurado}"
					id="txtProfissaoSegurado"  size="40" maxlength="100" /></td>
		</tr>
	</table>

	<!--  VALOR FIXO - LIMITE IDADE -->
	<table id="txtCapital1td">
		<tr>
			<td align="right" width="150px;"><label for="txtCapital1"
				style="font-weight: bold;">Capital:</label></td>

			<td><h:inputText id="txtCapital1"
					value="#{propostaHandler.proposta.capitalSeguradoLimiteIdade.funcVLMorteNatural}"
					styleClass="textinput" size="20" style="width: 200px;">
					<p:ajax actionListener="#{propostaHandler.populaCapitalConjuge}"
						update="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios, suggestionBoxNomeTabela,txtPremioSeguradoFuturoOutput"
						event="blur" id="a4jsupportCapital1" />
					<a4j:jsFunction name="autoSelectLimiteIdade"
						actionListener="#{propostaHandler.capitalLimiteIdadeAutoSelect}"
						reRender="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtPremioSeguradoFuturoOutput" />
					<rich:jQuery id="enterTabInput1" selector="input" timing="onload"
						query="keypress(function(e){if (e.keyCode == 13){ 
                 	var idTipoProposta = jQuery('#cmbTipoProposta').val();
                 	var tabindex;
			if( idTipoProposta > 1 && idTipoProposta != 4){
				tabindex = 14;
			}else{
				tabindex = 21
			}
					
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                          return false;} else if (e.keyCode == 59){
                                          	autoSelectLimiteIdade();
						                 	var idTipoProposta = jQuery('#cmbTipoProposta').val();
						                 	var tabindex;
									if( idTipoProposta > 1 && idTipoProposta != 4){
										tabindex = 14;
									}else{
										tabindex = 21
									}
											
								  jQuery('input[tabindex='+tabindex+']').focus();
								  jQuery('select[tabindex='+tabindex+']').focus();                                          
                                          
                                          	return false;
                                          }
                                            })" />
				</h:inputText> <!--  AUTOCOMPLETE LIMITE IDADE --> <rich:suggestionbox
					id="suggestionBoxCapitalLimite" for="txtCapital1"
					suggestionAction="#{propostaHandler.capitalLimiteIdadeAutoComplete}"
					var="capital" fetchValue="#{capital.funcVLMorteNatural}"
					width="650" height="250" cellpadding="4"
					nothingLabel="Nenhum Item Encontrado!">

					<a4j:support event="onselect" id="a4jsupport"
						ignoreDupResponses="true">
						<f:setPropertyActionListener value="#{capital}"
							target="#{propostaHandler.proposta.capitalSeguradoLimiteIdade}" />
					</a4j:support>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Limite Idade (P)" />
						</f:facet>
						<h:outputText value="#{capital.funcLimiteIdade}"
							styleClass="autocompleteFonte" />
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Limite Idade (C)" />

						</f:facet>
						<h:outputText value="#{capital.conjLimiteIdade}"
							styleClass="autocompleteFonte" />
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Morte Natural (P)" />

						</f:facet>
						<h:outputText value="#{capital.funcVLMorteNatural}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Morte Natural (C)" />

						</f:facet>
						<h:outputText value="#{capital.conjVLMorteNatural}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>
				</rich:suggestionbox></td>

		</tr>
	</table>

	<table id="txtCapital2td">
		<tr>

			<td align="right" width="150px;"><label for="txtCapital2"
				style="font-weight: bold;">Capital:</label></td>

			<!--  capital do segurado (FAIXA ET�RIA) -->
			<td><h:inputText id="txtCapital2"
					value="#{propostaHandler.proposta.detalheCapitalFaixaEtaria.capitalSegurado}"
					styleClass="textinput" size="20" style="width: 200px;">

					<p:ajax actionListener="#{propostaHandler.populaCapitalConjuge}"
						update="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtPremioSeguradoFuturoOutput"
						event="blur" id="a4jsupportCapital2" />
					<a4j:jsFunction name="autoSelectFaixaEtaria"
						actionListener="#{propostaHandler.capitalFaixaEtariaAutoSelect}"
						reRender="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtPremioSeguradoFuturoOutput" />

					<rich:jQuery id="enterTabInput2" selector="input" timing="onload"
						query="keypress(function(e){if (e.keyCode == 13){ 
                 	var idTipoProposta = jQuery('#cmbTipoProposta').val();
                 	var tabindex;
			if( idTipoProposta > 1 && idTipoProposta != 4){
				tabindex = 14;
			}else{
				tabindex = 21
			}
					
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                          return false;}else if (e.keyCode == 59){
                                          	autoSelectFaixaEtaria();
						                 	var idTipoProposta = jQuery('#cmbTipoProposta').val();
						                 	var tabindex;
									if( idTipoProposta > 1 && idTipoProposta != 4){
										tabindex = 14;
									}else{
										tabindex = 21
									}
											
								  jQuery('input[tabindex='+tabindex+']').focus();
								  jQuery('select[tabindex='+tabindex+']').focus();                                          
                                          
                                          	return false;
                                          }
                                            });" />

				</h:inputText> <!--  AUTOCOMPLETE - CAPITAL FAIXA ET�RIA --> <rich:suggestionbox
					id="suggestionBoxCapitalFaixaEtaria" for="txtCapital2"
					suggestionAction="#{propostaHandler.capitalFaixaEtariaAutoComplete}"
					var="capital" fetchValue="#{capital.capitalSegurado}" width="550"
					height="250" cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
					<a4j:support event="onselect" id="a4jsupport"
						ignoreDupResponses="true">
						<f:setPropertyActionListener value="#{capital}"
							target="#{propostaHandler.proposta.detalheCapitalFaixaEtaria}" />
					</a4j:support>

					<h:column>
						<f:facet name="header">
							<h:outputText value="Capital Segurado" />

						</f:facet>
						<h:outputText value="#{capital.capitalSegurado}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Idade Inicio" />

						</f:facet>
						<h:outputText value="#{capital.idadeInicial}"
							styleClass="autocompleteFonte"></h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Idade Fim" />

						</f:facet>
						<h:outputText value="#{capital.idadeFinal}"
							styleClass="autocompleteFonte"></h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Valor Titular" />

						</f:facet>
						<h:outputText value="#{capital.titular}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Valor Titular + C�njuge" />

						</f:facet>
						<h:outputText value="#{capital.somaTitularConjuge}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>

				</rich:suggestionbox></td>

		</tr>
	</table>

	<!-- CAPITAL REANGARIADO -->
	<table id="txtCapital3td">
		<tr>
			<td align="right" width="150px;"><label for="txtCapital3"
				style="font-weight: bold;">Capital:</label></td>

			<td><h:inputText id="txtCapital3"
					value="#{propostaHandler.proposta.valorCapitalSegurado}"
					maxlength="15" styleClass="textinput" size="20"
					style="width: 200px;" >
					<f:converter converterId="MyDoubleConverter" />
					<p:ajax actionListener="#{propostaHandler.populaCapitalConjuge}"
						update="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtCapitalConjugeFuturo"
						event="blur" id="a4jsupportCapital3" />

				</h:inputText></td>

			<td></td>
		</tr>
	</table>


	<!--  Premio do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><h:outputLabel
					style="font-weight: bold;" value="Pr�mio:" /></td>

			<td id="txtPremioSegurado2td"><a4j:outputPanel
					id="txtPremioSegurado2">
					<h:outputText value="#{propostaHandler.proposta.premioSegurado}"
						id="txtPremioSegurado" style="font-size: 14px;font-weight: bold;">
						<f:convertNumber currencySymbol="R$" type="currency"
							groupingUsed="true" />
					</h:outputText>
				</a4j:outputPanel></td>

			<td id="txtPremioSegurado22td"><h:inputText
					id="txtPremioSegurado22"
					value="#{propostaHandler.proposta.valorPremioSegurado}" size="20"
					style="width: 200px;" >
					<f:converter converterId="MyDoubleConverter" />
					<p:ajax actionListener="#{propostaHandler.populaPremioConjuge}"
						update="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtPremioConjugeFuturo"
						event="blur" id="a4jsupportSegurado4" />
				</h:inputText></td>
		</tr>
	</table>

	<!-- VALOR FUTURO DEFINIDO - FIXO-->
	<table id="tableValorFuturoFixo">
		<tr>
			<td align="right" width="150px;">
			<label for="txtCapitalFuturoSeguradoFixo"
			style="font-weight: bold;">Capital Futuro:</label></td>
							
			<td><h:inputText id="txtCapitalConjtxtCapitalFuturoSeguradoFixougeDigitadoFixo"
				value="#{propostaHandler.fixoCapitalFuturo}"
				maxlength="15" styleClass="textinput" size="20"
				style="width: 200px;">
				<f:converter converterId="MyDoubleConverter" />
			</h:inputText></td>
		</tr>
		<tr>
			<td align="right" width="150px;">
			<label for="txtCapitalSeguradoFuturoFixo"
			style="font-weight: bold;">Pr�mio Futuro:</label></td>
		
			<td>
			<h:inputText id="txtCapitalSeguradoFuturoFixo"
			value="#{propostaHandler.fixoPremioFuturo}"
			size="20" style="width: 200px;">
			<f:converter converterId="MyDoubleConverter" />
			</h:inputText>
			</td>
		</tr>
	</table>
	<!-- VALOR FUTURO DEFINIDO-->
	<table id="tableValorFuturoSegurado">
		<tr>
			<td align="right" width="150px;"></td>
			<td><p:commandButton
					actionListener="#{propostaHandler.atualizarValorFuturo}"
					value="Atualizar Valor Atual" id="atualizarValorAtual"
					async="false" ajax="false" /></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtCapital3"
				style="font-weight: bold;">Capital Futuro:</label></td>
			<td id="txtCapitalFuturoReangariadotd"><h:inputText
					id="txtCapitalFuturoReangariado"
					value="#{propostaHandler.proposta.valorFuturoCapitalSegurado}"
					maxlength="15" styleClass="textinput" size="20"
					style="width: 200px;" >
					<f:converter converterId="MyDoubleConverter" />
					<p:ajax actionListener="#{propostaHandler.populaCapitalConjuge}"
						update="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtCapitalConjugeFuturo"
						event="blur" id="a4jsupportCapitalFuturo3" />
				</h:inputText></td>


			<!-- AUTOCOMPLET CAPITAL FUTURO FAIXA ETARIA -->
			<td id="txtCapitalCapitalFuturoFaixaEtariatd"><h:inputText
					id="txtCapitalCapitalFuturoFaixaEtaria"
					value="#{propostaHandler.proposta.detalheCapitalFaixaEtariaFuturo.capitalSegurado}"
					styleClass="textinput" size="20" style="width: 200px;">

					<a4j:jsFunction name="autoSelectFaixaEtariaFuturo"
						actionListener="#{propostaHandler.capitalFaixaEtariaFuturoAutoSelect}"
						reRender="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtPremioSeguradoFuturoOutput,txtPremioConjugeFuturo,txtCapitalConjugeFuturo" />
						
						<p:ajax actionListener="#{propostaHandler.capitalFaixaEtariaFuturoAutoSelect}"
						update="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios,txtPremioSeguradoFuturoOutput,txtPremioConjugeFuturo,txtCapitalConjugeFuturo"
						event="blur" id="a4jsupportCapitalFuturo2" />

					<rich:jQuery id="enterTabInput2Futuro" selector="input"
						timing="onload"
						query="keypress(function(e){if (e.keyCode == 13){ 
                 	var idTipoProposta = jQuery('#cmbTipoProposta').val();
                 	var tabindex;
			if( idTipoProposta > 1 && idTipoProposta != 4){
				tabindex = 14;
			}else{
				tabindex = 21
			}
					
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                          return false;}else if (e.keyCode == 59){
                                          	autoSelectFaixaEtaria();
						                 	var idTipoProposta = jQuery('#cmbTipoProposta').val();
						                 	var tabindex;
									if( idTipoProposta > 1 && idTipoProposta != 4){
										tabindex = 14;
									}else{
										tabindex = 21
									}
											
								  jQuery('input[tabindex='+tabindex+']').focus();
								  jQuery('select[tabindex='+tabindex+']').focus();                                          
                                          
                                          	return false;
                                          }
                                            });" />

				</h:inputText> <!--  AUTOCOMPLETE - CAPITAL FAIXA ET�RIA FUTURO --> <rich:suggestionbox
					id="suggestionBoxCapitalFaixaEtariaFuturo"
					for="txtCapitalCapitalFuturoFaixaEtaria"
					suggestionAction="#{propostaHandler.capitalFaixaEtariaIcatuAutoComplete}"
					var="capital" fetchValue="#{capital.capitalSegurado}" width="550"
					height="250" cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
					<a4j:support event="onselect" id="a4jsupport"
						ignoreDupResponses="false">
						<f:setPropertyActionListener value="#{capital}"
							target="#{propostaHandler.proposta.detalheCapitalFaixaEtariaFuturo}" />
					</a4j:support>

					<h:column>
						<f:facet name="header">
							<h:outputText value="Capital Segurado" />

						</f:facet>
						<h:outputText value="#{capital.capitalSegurado}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Idade Inicio" />

						</f:facet>
						<h:outputText value="#{capital.idadeInicial}"
							styleClass="autocompleteFonte"></h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Idade Fim" />

						</f:facet>
						<h:outputText value="#{capital.idadeFinal}"
							styleClass="autocompleteFonte"></h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Valor Titular" />

						</f:facet>
						<h:outputText value="#{capital.titular}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>
					<h:column>
						<f:facet name="header">
							<h:outputText value="Valor Titular + C�njuge" />

						</f:facet>
						<h:outputText value="#{capital.somaTitularConjuge}"
							styleClass="autocompleteFonte">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</h:column>

				</rich:suggestionbox></td>



		</tr>
		<tr>
			<td align="right" width="150px;"><h:outputLabel
					style="font-weight: bold;" value="Pr�mio Futuro:" /></td>

			<td id="txtPremioSeguradoFuturoExibicaotd"><a4j:outputPanel
					id="txtPremioSeguradoFuturoExibicao">
					<h:outputText
						value="#{propostaHandler.proposta.premioSeguradoFuturo}"
						id="txtPremioSeguradoFuturoOutput"
						style="font-size: 14px;font-weight: bold;">
						<f:convertNumber currencySymbol="R$" type="currency"
							groupingUsed="true" />
					</h:outputText>
				</a4j:outputPanel></td>

			<td id="txtPremioSeguradoFuturotd"><h:inputText
					id="txtPremioSeguradoFuturoInput"
					value="#{propostaHandler.proposta.valorFuturoPremioSegurado}"
					size="20" style="width: 200px;" >
					<f:converter converterId="MyDoubleConverter" />
					<p:ajax actionListener="#{propostaHandler.populaPremioConjuge}"
						update="txtCapitalConjuge,txtPremioConjuge,txtPremioSegurado,txtPremioSegurado2,txtTotalPremios"
						event="blur" id="a4jsupportSeguradoFuturo4" />
				</h:inputText></td>
		</tr>
	</table>

	<jsp:include page="../proposta/historicoSeguradoPrincipal.jsp" />
	
</fieldset>
