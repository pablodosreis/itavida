<%@ include file="/common/cabecalho.jsp"%>
<h:panelGroup id="agrupamento_Historico_Proposta" styleClass="agrupamento_Dados">
	<fieldset>
		<legend>
			<IMG title="Expandir" id="expandirconteudoHistoricoProposta"
				alt="Expandir" onclick="expandir('conteudoHistoricoProposta');"
				src="images/expandir.png" border=0 /> <IMG
				id="retrairconteudoHistoricoProposta" title="Retrair" alt="Retrair"
				onclick="retrair('conteudoHistoricoProposta');"
				src="images/retrair.png" border=0 style="display: none;" />
			Hist�rico de Altera��es de Tipo de Proposta
		</legend>
		<div id="conteudoHistoricoProposta" style="display: none;">
			<p:dataTable
				value="#{propostaHandler.proposta.historicoproposta_collection}"
				var="historicoProposta" width="50%"
				rendered="#{not empty propostaHandler.proposta.historicoproposta_collection}">

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Data Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoProposta.dataAlteracao}"
						style="text-transform:uppercase;">
						<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" />
					</h:outputText>
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Ap�lice" />

					</f:facet>
					<h:outputText value="#{historicoProposta.empresa.nomeEmpresa}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Modelo de Proposta" />

					</f:facet>
					<h:outputText value="#{historicoProposta.modeloProposta.descricao}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Tipo de Proposta" />

					</f:facet>
					<h:outputText value="#{historicoProposta.tipoProposta.descricao}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="�rg�o" />

					</f:facet>
					<h:outputText value="#{historicoProposta.orgao.nomeOrgao}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Tabela de Capitais" />

					</f:facet>
					<h:outputText value="#{historicoProposta.nomeTabela.nomeTabela}" />
				</p:column>



				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Usu�rio Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoProposta.nomeUsuario}" />
				</p:column>
			</p:dataTable>
		</div>
	</fieldset>
</h:panelGroup>