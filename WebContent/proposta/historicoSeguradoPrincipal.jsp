<%@ include file="/common/cabecalho.jsp"%>
<!--   HIST�RICO DO CAPITAL SEGURADO -->
	<h:panelGroup id="agrupamento_Historico_Capital"
		styleClass="agrupamento_Dados">
		<fieldset>
			<legend>
				<IMG title="Expandir" id="expandirconteudoHistoricoCapital"
					alt="Expandir" onclick="expandir('conteudoHistoricoCapital');"
					src="images/expandir.png" border=0 /><IMG
					id="retrairconteudoHistoricoCapital" title="Retrair" alt="Retrair"
					onclick="retrair('conteudoHistoricoCapital');"
					src="images/retrair.png" border=0 style="display: none;" />
				Hist�rico de Altera��es de Capitais
			</legend>
			<div id="conteudoHistoricoCapital" style="display: none;">
				<p:dataTable
					value="#{propostaHandler.proposta.historicocapital_collection}"
					var="historicoCapital" width="50%"
					rendered="#{not empty propostaHandler.proposta.historicocapital_collection}">

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Data Altera��o" />

						</f:facet>
						<h:outputText value="#{historicoCapital.dataAlteracao}"
							style="text-transform:uppercase;">
							<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" />
						</h:outputText>
					</p:column>
					
					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Nome Segurado" />
						</f:facet>

						<h:outputText value="#{historicoCapital.nomeSegurado}" />
					</p:column>
					
					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Capital Antigo" />

						</f:facet>
						<h:outputText value="#{historicoCapital.capitalAntigo}">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</p:column>
					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Pr�mio Antigo" />


						</f:facet>
						<h:outputText value="#{historicoCapital.premioAntigo}">
							<f:convertNumber currencySymbol="R$" type="currency"
								groupingUsed="true" />
						</h:outputText>
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Sexo" />
						</f:facet>

						<h:outputText value="#{historicoCapital.sexoExibicao}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Data de Nascimento">
								<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
							</h:outputText>
						</f:facet>

						<h:outputText value="#{historicoCapital.dataNascimento}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="CPF" />
						</f:facet>

						<h:outputText value="#{historicoCapital.cpf}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="RG" />
						</f:facet>

						<h:outputText value="#{historicoCapital.rg}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Estado Civil" />
						</f:facet>

						<h:outputText value="#{historicoCapital.estadoCivilExibicao}" />
					</p:column>
					
					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Profiss�o" />
						</f:facet>

						<h:outputText value="#{historicoCapital.profissaoSegurado}" />
					</p:column>

					<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Usu�rio Altera��o" />

						</f:facet>
						<h:outputText value="#{historicoCapital.nomeUsuario}" />
					</p:column>
				</p:dataTable>
			</div>
		</fieldset>
	</h:panelGroup>
