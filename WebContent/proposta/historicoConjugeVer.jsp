<%@ include file="/common/cabecalho.jsp"%>
<h:panelGroup id="agrupamento_Historico_Conjuge"
	styleClass="agrupamento_Dados">
	<fieldset>
		<legend>
			<IMG title="Expandir" id="expandirconteudoHistoricoConjuge"
				alt="Expandir" onclick="expandir('conteudoHistoricoConjuge');"
				src="images/expandir.png" border=0 /> <IMG
				id="retrairconteudoHistoricoConjuge" title="Retrair" alt="Retrair"
				onclick="retrair('conteudoHistoricoConjuge');"
				src="images/retrair.png" border=0 style="display: none;" />
			Hist�rico de Altera��es C�njuge
		</legend>
		<div id="conteudoHistoricoConjuge" style="display: none;">
			<p:dataTable
				value="#{verPropostaHandler.proposta.historicoconjuge_collection}"
				var="historicoConjuge" width="50%"
				rendered="#{not empty verPropostaHandler.proposta.historicoconjuge_collection}">

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Data Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.dataAlteracao}"
						style="text-transform:uppercase;">
						<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" />
					</h:outputText>
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Nome" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.nomeConjuge}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Sexo" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.sexoExibicao}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Cpf" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.cpfConjuge}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="RG" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.rgConjuge}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Matr�cula" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.matriculaConjuge}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Pr�mio" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.premio}">
						<f:convertNumber currencySymbol="R$" type="currency"
							groupingUsed="true" />
					</h:outputText>
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Capital" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.capital}">
						<f:convertNumber currencySymbol="R$" type="currency"
							groupingUsed="true" />
					</h:outputText>
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Data de Nascimento" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.dataNascimentoConjuge}">
						<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
					</h:outputText>
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Estado Civil" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.estadoCivilExibicao}" />
				</p:column>

				<p:column resizable="true">
						<f:facet name="header">
							<h:outputText value="Profiss�o" />
						</f:facet>

						<h:outputText value="#{historicoConjuge.profissaoConjuge}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Usu�rio Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoConjuge.nomeUsuario}" />
				</p:column>
			</p:dataTable>
		</div>
	</fieldset>
</h:panelGroup>
