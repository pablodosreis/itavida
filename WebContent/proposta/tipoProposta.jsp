<%@ include file="/common/cabecalho.jsp"%>
<fieldset>
	<legend>Tipo de Proposta</legend>

	<table>
		<tr>
			<td><br></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtIdProposta">N�mero
					da Proposta:</label></td>
			<td><h:outputText
					value="#{propostaHandler.numeroPropostaIncluir}" id="txtIdProposta" /></td>
		</tr>
	</table>

	<table>
		<tr>
			<td><br></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="cmbEmpresa">Empresa:</label></td>
			<td><h:selectOneMenu id="cmbEmpresa"
					value="#{propostaHandler.proposta.empresa.id}" >
					<f:selectItems value="#{propostaHandler.opcoesEmpresaCombo}" />
				</h:selectOneMenu></td>
		</tr>
	</table>


	<table>
		<tr>
			<td align="right" width="150px;"><label for="cmbModeloProposta">Modelo
					de Proposta:</label></td>
			<td><h:selectOneMenu id="cmbModeloProposta"
					value="#{propostaHandler.proposta.modeloProposta.id}" 
					onchange="controlarCampos()">
					<f:selectItems id="selectItensModeloProposta"
						value="#{propostaHandler.modeloPropostaCombo}" />

					<p:ajax actionListener="#{propostaHandler.populaTipoPropostaCombo}"
						update="cmbTipoProposta, txtPremioSegurado, txtPremioSegurado2"
						event="change" oncomplete=" jQuery('#cmbTipoProposta').focus();" />

				</h:selectOneMenu></td>
			<td align="right" width="150px;"><label for="cmbTipoProposta">Tipo
					de Proposta:</label></td>
			<td><h:selectOneMenu id="cmbTipoProposta"
					value="#{propostaHandler.proposta.tipoProposta.id}" 
					onchange="controlarCampos()">
					<f:selectItems id="selectItensTipoProposta"
						value="#{propostaHandler.tipoPropostaComboFiltro}" />
					<a4j:support
						actionListener="#{propostaHandler.ajaxChangeTipoProposta}"
						reRender="txtCapital3, txtPremioSegurado, txtCapital1, txtCapital2"
						ajaxSingle="true" event="onchange"
						oncomplete=" jQuery('#txtOrgao').focus();" />
				</h:selectOneMenu></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtOrgao">�rg�o:</label>
			</td>
			<td><h:inputText
					value="#{propostaHandler.proposta.orgao.nomeOrgao}" id="txtOrgao"
					size="40" >
				</h:inputText> <rich:suggestionbox id="suggestionBoxOrgao" for="txtOrgao"
					suggestionAction="#{propostaHandler.orgaoAutoComplete}" var="orgao"
					fetchValue="#{orgao.nomeOrgao}" width="450" height="250"
					cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
					<a4j:support event="onselect" id="a4jsupport1">
						<f:setPropertyActionListener value="#{orgao}"
							target="#{propostaHandler.proposta.orgao}" />
					</a4j:support>
					<h:column>
						<h:outputText value="#{orgao.nomeOrgao}"
							styleClass="autocompleteFonte" />
					</h:column>
					<h:column>
						<b> <h:outputText value="#{orgao.siglaOrgao}"
								styleClass="autocompleteFonte" /></b>
					</h:column>
				</rich:suggestionbox></td>
		</tr>
		
		<tr>
			<td align="right" width="150px;">
				<label for="txtOrgao">Lota��o:</label>
			</td>
		<td>
			<h:inputText value="#{propostaHandler.proposta.lotacao}" id="lotacao" maxlength="255"/>	
		</td>
		</tr>
	</table>

	<table>
		<tr id="panelTabelaCap">
			<td align="right" width="150px;"><label id="labelNomeTabela"
				for="txtNomeTabela">Tabela de Capitais:</label></td>
			<td><a4j:outputPanel id="txtNomeTabelaPanel">
					<h:inputText
						value="#{propostaHandler.proposta.nomeTabela.nomeTabela}"
						id="txtNomeTabela" size="30"  />
					<rich:suggestionbox id="suggestionBoxNomeTabela"
						for="txtNomeTabela"
						suggestionAction="#{propostaHandler.nomeTabelaAutocomplete}"
						var="nomeTabela" fetchValue="#{nomeTabela.nomeTabela}" width="450"
						height="250" cellpadding="4"
						nothingLabel="Nenhum Item Encontrado!">
						<a4j:support event="onselect" id="a4jsupport2">
							<f:setPropertyActionListener value="#{nomeTabela}"
								target="#{propostaHandler.proposta.nomeTabela}" />
						</a4j:support>
						<h:column>
							<h:outputText value="#{nomeTabela.nomeTabela}"
								styleClass="autocompleteFonte" />
						</h:column>
						<h:column>
							<h:outputText value="#{nomeTabela.nomeTabelaReduzido}"
								styleClass="autocompleteFonte" />
						</h:column>
					</rich:suggestionbox>
				</a4j:outputPanel>
		</tr>
	</table>

</fieldset>

<!--  hist�rico -->
<h:panelGroup id="agrupamento_Historico_Proposta"
	styleClass="agrupamento_Dados">
	<fieldset>
		<legend>
			<IMG title="Expandir" id="expandirconteudoHistoricoProposta"
				alt="Expandir" onclick="expandir('conteudoHistoricoProposta');"
				src="images/expandir.png" border=0 /> <IMG
				id="retrairconteudoHistoricoProposta" title="Retrair" alt="Retrair"
				onclick="retrair('conteudoHistoricoProposta');"
				src="images/retrair.png" border=0 style="display: none;" />
			Hist�rico de Altera��es de Tipo de Proposta
		</legend>
		<div id="conteudoHistoricoProposta" style="display: none;">
			<p:dataTable
				value="#{propostaHandler.proposta.historicoproposta_collection}"
				var="historicoProposta" width="50%"
				rendered="#{not empty propostaHandler.proposta.historicoproposta_collection}">

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Data Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoProposta.dataAlteracao}"
						style="text-transform:uppercase;">
						<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" />
					</h:outputText>
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Ap�lice" />

					</f:facet>
					<h:outputText value="#{historicoProposta.empresa.nomeEmpresa}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Modelo de Proposta" />

					</f:facet>
					<h:outputText value="#{historicoProposta.modeloProposta.descricao}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Tipo de Proposta" />

					</f:facet>
					<h:outputText value="#{historicoProposta.tipoProposta.descricao}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="�rg�o" />

					</f:facet>
					<h:outputText value="#{historicoProposta.orgao.nomeOrgao}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Tabela de Capitais" />

					</f:facet>
					<h:outputText value="#{historicoProposta.nomeTabela.nomeTabela}" />
				</p:column>



				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Usu�rio Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoProposta.nomeUsuario}" />
				</p:column>
			</p:dataTable>
		</div>
	</fieldset>
</h:panelGroup>
