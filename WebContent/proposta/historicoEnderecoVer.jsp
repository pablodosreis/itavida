<%@ include file="/common/cabecalho.jsp"%>
<h:panelGroup id="agrupamento_Historico_Endereco"	styleClass="agrupamento_Dados">
	<fieldset>
		<legend>
			<IMG title="Expandir" id="expandirconteudoHistoricoEndereco"
				alt="Expandir"
				onclick="expandir('conteudoHistoricoEndereco');"
				src="images/expandir.png" border=0 /> <IMG
				id="retrairconteudoHistoricoEndereco" title="Retrair"
				alt="Retrair"
				onclick="retrair('conteudoHistoricoEndereco');"
				src="images/retrair.png" border=0 style="display: none;" />
			Hist�rico de Altera��es Endere�o
		</legend>
		<div id="conteudoHistoricoEndereco"
			style="display: none; width: 400px;">
			<p:dataTable
				value="#{verPropostaHandler.proposta.historicoenderecosegurado_collection}"
				var="historicoEndereco" width="50%" style="width: 300px;"
				rendered="#{not empty verPropostaHandler.proposta.historicoenderecosegurado_collection}">

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Data Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoEndereco.dataAlteracao}"
						style="text-transform:uppercase;">
						<f:convertDateTime dateStyle="default"
							pattern="dd/MM/yyyy HH:mm" />
					</h:outputText>
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Endere�o" />

					</f:facet>
					<h:outputText
						value="#{historicoEndereco.enderecoSegurado}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Complemento" />

					</f:facet>
					<h:outputText value="#{historicoEndereco.complemento}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Bairro" />

					</f:facet>
					<h:outputText value="#{historicoEndereco.bairro}" />
				</p:column>
				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Cidade" />

					</f:facet>
					<h:outputText
						value="#{historicoEndereco.cidade.nom_cidade}" />
				</p:column>

				<p:column resizable="true" width="10">
					<f:facet name="header">
						<h:outputText value="Estado" />

					</f:facet>
					<h:outputText
						value="#{historicoEndereco.cidade.cod_estado.sgl_estado}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Cep" />

					</f:facet>
					<h:outputText value="#{historicoEndereco.cep}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Telefone Residencial" />

					</f:facet>
					<h:outputText
						value="#{historicoEndereco.telefoneResidencial}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Telefone Comercial" />

					</f:facet>
					<h:outputText
						value="#{historicoEndereco.telefoneComercial}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Telefone Celular 1" />

					</f:facet>
					<h:outputText
						value="#{historicoEndereco.telefoneCelular1}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Telefone Celular 2" />

					</f:facet>
					<h:outputText
						value="#{historicoEndereco.telefoneCelular2}" />
				</p:column>

				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Observa��es" />

					</f:facet>
					<h:outputText value="#{historicoEndereco.observacao}" />
				</p:column>


				<p:column resizable="true">
					<f:facet name="header">
						<h:outputText value="Usu�rio Altera��o" />

					</f:facet>
					<h:outputText value="#{historicoEndereco.nomeUsuario}" />
				</p:column>
			</p:dataTable>
		</div>
	</fieldset>
</h:panelGroup>
