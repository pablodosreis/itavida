<%@ include file="common/cabecalho.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
	<a4j:keepAlive beanName="ImportarNovosValoresHandler" />
	<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY>
	<jsp:include page="common/relatorioHeader.jsp" />

	<h:form styleClass="niceform" prependId="false">
		<table class="cabecalho_relatorio">
			<tr>
				<td><IMG title="Slogan" alt="Slogan"
					src="images/logoRelatorio.png" border=0 /></td>
				<td width="600px;" >
					<span style="font-size: 16px; color: white">Resultado da Importa��o de Novos Valores</span>
				</td>
				<td class="invisivelImpressao"><h:commandLink
						onclick="javascript:window.print();">
						<p:graphicImage value="images/impressora.png" alt="Imprimir"
							title="Imprimir" />
					</h:commandLink></td>
				<td class="invisivelImpressao">
				<h:commandLink
						action="#{ImportarNovosValoresHandler.gerarRelatorio}">
						<p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>
					</h:commandLink></td>
			<tr>
		</table>

		<h:outputLabel value="Linhas n�o importadas devido a erros: "  style="font-size: 20px">
		<h:outputText  value="#{ImportarNovosValoresHandler.numeroErros}" />
		</h:outputLabel>
		<br/>
		<br/>
		<!-- ><br/>
		<p:dataTable id="errosTable"
			value="#{ImportarNovosValoresHandler.erros}" var="item"
			width="800px"
			style="margin-top: 10px;margin-bottom: 10px;"
			rendered="#{not empty ImportarNovosValoresHandler.erros}">

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Linha" />
				</f:facet>
				<h:outputText value="#{item.linha}" />
			</p:column>
			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Mensagem" />
				</f:facet>
				<h:outputText value="#{item.mensagem}" />
			</p:column>
		</p:dataTable> -->
		
		<h:outputLabel value="Alertas gerados pela importa��o: "  style="font-size: 20px">
			<h:outputText  value="#{ImportarNovosValoresHandler.numeroAlertas}" />
		</h:outputLabel>
		<br/>
		<br/>
		<!--<p:dataTable id="alertasTable"
			value="#{ImportarNovosValoresHandler.alertas}" var="item"
			width="800px"
			style="margin-top: 10px;margin-bottom: 10px;"
			rendered="#{not empty ImportarNovosValoresHandler.alertas}">

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Linha" />
				</f:facet>
				<h:outputText value="#{item.linha}" />
			</p:column>
			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Mensagem" />
				</f:facet>
				<h:outputText value="#{item.mensagem}" />
			</p:column>
		</p:dataTable>-->
		
		<h:outputLabel value="Linhas Importadas com Sucesso: " style="font-size: 20px" >
			<h:outputText  value="#{ImportarNovosValoresHandler.numeroSucessos}" />
		</h:outputLabel>
		<!--<p:dataTable id="sucessoTable"
			value="#{ImportarNovosValoresHandler.sucessos}" var="item"
			width="800px"
			style="margin-top: 10px;margin-bottom: 10px;"
			rendered="#{not empty ImportarNovosValoresHandler.sucessos}">

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Linha" />
				</f:facet>
				<h:outputText value="#{item.linha}" />
			</p:column>
			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="CPF" />
				</f:facet>
				<h:outputText value="#{item.mensagem}" />
			</p:column>
		</p:dataTable>-->
		
		<br/>
		<br/>
		<br/>
		<span>Para mais detalhes, solicite a exporta��o para o excel.</span>
		<h:commandLink
			action="#{ImportarNovosValoresHandler.gerarRelatorio}">
			<p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel" width="24px" height="24px"/>
		</h:commandLink></td>

	</h:form>
</BODY>
	</HTML>
</f:view>
