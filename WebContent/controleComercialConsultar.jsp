<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="controleComercialHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery("#dtDataVisitaChegadaInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataVisitaChegadaFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataVisitaSaidaInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataVisitaSaidaFim").mask("99/99/9999",{placeholder:"_"});	   
	});
</script>
  
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Controle</span><li>Controle Comercial  (Consultar)</li> </div>
						<div id="icone">
                           <img src="images/controleComercial.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false">

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao">�rg�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{controleComercialHandler.controleComercial.orgao.nomeOrgao}" id="txtOrgao" size="40" tabindex="1">
											<p:ajax event="keyup" update="renderizado" >
												<h:panelGroup id="renderizado" />
												<f:setPropertyActionListener 
													value="#{controleComercialHandler.limparOrgao}" 
													target="#{controleComercialHandler.controleComercial.orgao}"/> 
											</p:ajax>

										   </h:inputText>
											
											<rich:suggestionbox id="suggestionOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{controleComercialHandler.orgaoAutoComplete}" 
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >

								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{controleComercialHandler.controleComercial.orgao}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Org�o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.nomeOrgao}"/>									  										
													</h:column>
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Sigla" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.siglaOrgao}"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroFuncionariosInicio">N�mero Funcion�rios (in�cio):</label> 
										</td> 
										<td> 
										   <h:inputText value="#{controleComercialHandler.controleComercial.numeroFuncionariosInicio}" id="txtNumeroFuncionariosInicio" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="2"/>
										</td> 
										<td align="right" width="60px;"> 
										  <label for="txtNumeroFuncionariosFim">(fim):</label> 
										</td> 
										<td> 
										   <h:inputText value="#{controleComercialHandler.controleComercial.numeroFuncionariosFim}" id="txtNumeroFuncionariosFim" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="3"/>
										</td> 
									</tr> 
								</table>	
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtAgenciador">Agenciador:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{controleComercialHandler.controleComercial.funcionario.nome}" id="txtAgenciador" size="40" tabindex="4" >
												<p:ajax event="keyup" update="renderizado" >
													<f:setPropertyActionListener 
														value="#{controleComercialHandler.limparFuncionario}" 
														target="#{controleComercialHandler.controleComercial.funcionario}"/> 
												</p:ajax>

										   </h:inputText>
 												
											<rich:suggestionbox id="suggestionFuncionario" 
								                	for="txtAgenciador"
								                    suggestionAction="#{controleComercialHandler.funcionarioAutoComplete}" 
								                    var="funcionario"
								                    fetchValue="#{funcionario.nome}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
													
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{funcionario}" 
														target="#{controleComercialHandler.controleComercial.funcionario}"/> 
														 
													</a4j:support>

													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Funcion�rio" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>
								         	</rich:suggestionbox>
										</td> 
									</tr> 
								</table> 
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataVisitaChegadaInicio" >Data da Visita Chegada (in�cio):</label>
										</td>
										<td>
											<h:inputText value="#{controleComercialHandler.controleComercial.dataVisitaChegadaInicio}"  id="dtDataVisitaChegadaInicio" maxlength="10"  tabindex="5">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
										<td align="right" width="60px;">
										  <label for="dtDataVisitaChegadaFim" >(fim):</label>
										</td>
										<td>
											<h:inputText value="#{controleComercialHandler.controleComercial.dataVisitaChegadaFim}"  id="dtDataVisitaChegadaFim" maxlength="10"  tabindex="6">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataVisitaSaidaInicio" >Data da Visita Sa�da (in�cio):</label>
										</td>
										<td>
											<h:inputText value="#{controleComercialHandler.controleComercial.dataVisitaSaidaInicio}"  id="dtDataVisitaSaidaInicio" maxlength="10"  tabindex="7">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
										<td align="right" width="60px;">
										  <label for="dtDataVisitaSaidaFim" >(fim):</label>
										</td>
										<td>
											<h:inputText value="#{controleComercialHandler.controleComercial.dataVisitaSaidaFim}"  id="dtDataVisitaSaidaFim" maxlength="10"  tabindex="8">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
								</table>
						</fieldset>							
							<br />
							<h:commandButton actionListener="#{controleComercialHandler.pesquisaControleComercial}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="10"/>
							<p:commandButton actionListener="#{controleComercialHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" tabindex="11">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							<div id="resultado_consulta" >
									<div class="titulo_agrupamento_Resultados" >Resultado da Consulta</div>	
									<p:dataTable value="#{controleComercialHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty controleComercialHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Vidas <br/>Ativas" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.numeroVidasAtivas}"/>		
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="N� de <br/>Funcion�rios" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.numeroFuncionarios}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Agenciador" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.funcionario.nome}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Visita <br/>(Chegada)" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataVisitaChegada}" ><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Visita <br/>(Sa�da)" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataVisitaSaida}" ><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>									  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  action="#{controleComercialHandler.preparaAlterarControleComercial}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
											<f:setPropertyActionListener target="#{controleComercialHandler.controleComercial}" value="#{item}"  />
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{controleComercialHandler.excluirControleComercial}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirControleComercial" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
							</div>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
