
function formatar(src, mask){
  var i = src.value.length;
  var saida = mask.substring(0,1);
  var texto = mask.substring(i);
  if (texto.substring(0,1) != saida)
  {
    src.value += texto.substring(0,1);
  }
}

function expandir(id){
	document.getElementById(id).style.display='block';
	document.getElementById("retrair"+id).style.display='inline';
	document.getElementById("expandir"+id).style.display='none';
}
function retrair(id){
	document.getElementById(id).style.display='none';
	document.getElementById("retrair"+id).style.display='none';
	document.getElementById("expandir"+id).style.display='inline';
}

function abrir( page ) { 

	semx=window.open("","","toolbar=no,location=no,status=no,resizable=no,scrollbars=yes,menubar=no");
	self.close();
	semx.moveTo(0,0);
	semx.resizeTo(semx.screen.width,semx.screen.height);
	semx.focus();
	semx.location=page;
	
} 

function abrirLogin( page ) { 
	semx=window.open("","","toolbar=no,location=no,status=no,resizable=no,scrollbars=no,menubar=no");
	self.close();
	semx.moveTo(500,500);
	semx.resizeTo(600,500);
	semx.focus();
	semx.location=page;
	
} 

function abrirRelatorio( page ) { 

	semx=window.open("","","toolbar=no,location=no,status=no,resizable=no,scrollbars=yes,menubar=no");
	semx.moveTo(0,0);
	semx.resizeTo(semx.screen.width,semx.screen.height);
	semx.focus();
	semx.location=page;
	
} 

function show_resultado(){
	document.getElementById('resultado_consulta').style.display = 'inline';
	document.getElementById('excluir').style.display = 'inline';	
}
function show_sucesso_inclusao(){
	alert('Inclus�o realizada com sucesso!');	
}
function show_sucesso_alteracao(){
	alert('Altera��o realizada com sucesso!');	
	document.getElementById('excluir').style.display = 'none';
	document.getElementById('incluir').style.display = 'inline';
	document.getElementById('alterar').style.display = 'none';
}

function show_sucesso_exclusao(){
	if(confirm('Deseja Excluir os itens selecionados?') ){
		alert('Exclus�o realizada com sucesso!');	
	}	
}
function show_button_alterar(){
	document.getElementById('excluir').style.display = 'none';
	document.getElementById('incluir').style.display = 'none';
	document.getElementById('alterar').style.display = 'inline';
}

function show_cancelar(){
	if(confirm('Deseja Cancelar a Opera��o?') ){
		document.getElementById('excluir').style.display = 'none';
		document.getElementById('incluir').style.display = 'inline';
		document.getElementById('alterar').style.display = 'none';
		document.getElementById('resultado_consulta').style.display = 'none';
		alert('Opera��o Cancelada com sucesso!');	
	}	
}

function show_cancelar_sinistro(){
	if(confirm('Deseja Cancelar a Opera��o?') ){		
		alert('Opera��o Cancelada com sucesso!');	
		window.location.href = "sinistroConsultar.htm";
	}	
}


function openPopup(popupName, pagina, largura, altura ) {
	
	largura = (typeof largura == "undefined" ? 650 : largura);
	altura = (typeof altura == "undefined" ? 550 : altura);
	
	// Definindo meio da tela
	var esquerda = (screen.width - largura)/2;
	var topo = (screen.height - altura)/2;

	// Abre a nova janela
	var popup = window.open(pagina, popupName, 'height=' + altura + ', width=' + largura + ', top=' + topo + ', left=' + esquerda);
	return popup;
}