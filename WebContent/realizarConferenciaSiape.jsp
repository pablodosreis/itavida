<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="siapeHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
	jQuery("#dtPeriodoReferenciaA").mask("99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoReferenciaB").mask("99/9999",{placeholder:"_"});
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Arquivos </span><li>Confer�ncia Arquivo D8 - SIAPE </li></div>
						<div id="icone_tokio">
                           <img src="images/siape.png" />
                        </div>
                          

						<h:form  styleClass="niceform" prependId="false">

						<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right" > 
										   <label for="dtPeriodoReferencia">Per�odo de Refer�ncia A:</label> 
										</td> 
										<td>
											<h:inputText value="#{siapeHandler.siape.periodoReferenciaA}"  id="dtPeriodoReferenciaA"  maxlength="7" tabindex="1">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" /> 
											</h:inputText>
										</td>								
									</tr>
									<tr>
										<td align="right" > 
										   <label for="dtPeriodoReferencia">Per�odo de Refer�ncia B:</label> 
										</td> 
										<td>
											<h:inputText value="#{siapeHandler.siape.periodoReferenciaB}"  id="dtPeriodoReferenciaB"  maxlength="7" tabindex="2">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" /> 
											</h:inputText>
										</td>								
									</tr>
								</table>					
							</fieldset>							

							<br />
							<h:commandButton action="#{siapeHandler.realizarConferencia}" value="Realizar Confer�ncia" id="pesquisar" styleClass="button" tabindex="3" />
							<p:commandButton actionListener="#{siapeHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
