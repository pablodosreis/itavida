<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="despesaHandler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Despesas</li> </div>
						<div id="icone">
                           <img src="images/cadDespesas.png" />
                        </div>

						<h:form id="myform" styleClass="niceform" prependId="false">

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtNomeDespesa">Nome Despesa:</label>
										</td>
										<td>
										 <h:inputText value="#{despesaHandler.despesa.nomeDespesa}" id="txtNomeDespesa" size="40" maxlength="50" tabindex="1"/>
										  
										</td>
									</tr>
								</table>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										   <label for="cmbDespesaFixa">Despesa Fixa:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbDespesaFixa" binding="#{despesaHandler.tipoDespesaSelecionada}" value="#{despesaHandler.despesa.tipoDespesa}" tabindex="2" >
										   	<f:selectItems value="#{despesaHandler.simNaoCombo}"/>
										   </h:selectOneMenu>
										</td> 

									</tr> 
								</table>
							</fieldset>							
							<br />
							
							<p:commandButton actionListener="#{despesaHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" tabindex="3" ajax="false" />
							<p:commandButton actionListener="#{despesaHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="5">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
					
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
