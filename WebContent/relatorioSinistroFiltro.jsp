
<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="historicoSinistroHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery("#dtDataSinistroInicio").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtDataSinistroFim").mask("99/99/9999",{placeholder:"_"});
	});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rios</span><li>Relat�rios de Sinistros</li> </div>
						<div id="icone">
                           <img src="images/historicoSinistros.png" />
                        </div>
						<h:form styleClass="niceform" prependId="false">

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtNumeroProposta">N�mero da Proposta:</label>
										</td>
										<td>
										 <h:inputText value="#{historicoSinistroHandler.historicoSinistro.proposta.id}" id="txtNumeroProposta" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="1"/>
										  
										</td>
									</tr>
									<tr>
										<td align="right"><label for="cmbEmpresa">Ap�lice:</label>
										</td>
										<td>
											<h:selectOneMenu id="cmbEmpresa"
												value="#{historicoSinistroHandler.empresaSelected}">
												<f:selectItems
													value="#{seguradoHandler.opcoesEmpresaCombo}" />
											</h:selectOneMenu>
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="txtNomeSeguradoPrincipal">Nome do Segurado Principal:</label>
										</td>
										<td>
										 <h:inputText value="#{historicoSinistroHandler.historicoSinistro.proposta.nomeSegurado}" id="txtNomeSeguradoPrincipal" size="40" maxlength="50" tabindex="2"/>
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="txtNomeSeguradoConjuge">Nome do Segurado C�njuge:</label>
										</td>
										<td>
										 <h:inputText value="#{historicoSinistroHandler.historicoSinistro.proposta.nomeConjuge}" id="txtNomeSeguradoConjuge" size="40" maxlength="50" tabindex="3"/>
										</td>
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao">�rg�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.proposta.orgao.nomeOrgao}" id="txtOrgao" size="40" tabindex="4"/>
											<rich:suggestionbox id="suggestionBoxOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{historicoSinistroHandler.orgaoAutoComplete}" 
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="450"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{historicoSinistroHandler.historicoSinistro.proposta.orgao}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{orgao.nomeOrgao}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{orgao.siglaOrgao}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao">Cidade:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.proposta.cidade.nom_cidade}" id="txtCidade" size="40" />
											<rich:suggestionbox id="suggestionBoxCidade" 
								                	for="txtCidade"
								                    suggestionAction="#{historicoSinistroHandler.cidadeAutocomplete}" 
								                    var="cidade"
								                    fetchValue="#{cidade.nom_cidade}"
								                    width="450"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{cidade}" 
														target="#{historicoSinistroHandler.historicoSinistro.proposta.cidade}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
									<tr>
										<td align="right">
										  <label for="txtNumeroSinistro">N�mero do Sinistro:</label>
										</td>
										<td>
	
										 <h:inputText value="#{historicoSinistroHandler.historicoSinistro.numeroSinistro}" id="txtNumeroSinistro" size="15" maxlength="15" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="5"/>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataSinistroInicio" >Data do Sinistro (in�cio):</label>
										</td>
										<td>
											<h:inputText value="#{historicoSinistroHandler.historicoSinistro.dataSinistroInicio}"  id="dtDataSinistroInicio" maxlength="10"  tabindex="6">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
											</h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataSinistroFim" >Data do Sinistro (fim):</label>
										</td>
										<td>
											<h:inputText value="#{historicoSinistroHandler.historicoSinistro.dataSinistroFim}"  id="dtDataSinistroFim" maxlength="10"  tabindex="7">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
											</h:inputText>
										</td>
									</tr>
									</table>
									<table>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtMotivoSinistro">Motivo Sinistro:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.motivoSinistro.descricao}" id="txtMotivoSinistro" size="40" tabindex="8"/>
											<rich:suggestionbox id="suggestionMotivoSinistro" 
								                	for="txtMotivoSinistro"
								                    suggestionAction="#{historicoSinistroHandler.motivoSinistroAutoComplete}" 
								                    var="motivo"
								                    fetchValue="#{motivo.descricao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{motivo}" 
														target="#{historicoSinistroHandler.historicoSinistro.motivoSinistro}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Descri��o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivo.descricao}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Sinistro Negado" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivo.sinistroNegado == 'S' ? 'SIM' : 'N�O'}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
								         	</rich:suggestionbox>
										</td>
									</tr> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtMotivoSinistroNegado">Motivo Sinistro Negado:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.motivoSinistroNegado.descricao}" id="txtMotivoSinistroNegado" size="40" tabindex="9"/>
											
											<rich:suggestionbox id="suggestionMotivoSinistroNegado" 
								                	for="txtMotivoSinistroNegado"
								                    suggestionAction="#{historicoSinistroHandler.motivoSinistroNegadoAutoComplete}" 
								                    var="motivoNegado"
								                    fetchValue="#{motivoNegado.descricao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{motivoNegado}" 
														target="#{historicoSinistroHandler.historicoSinistro.motivoSinistro}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Descri��o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivoNegado.descricao}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Sinistro Negado" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivoNegado.sinistroNegado == 'S' ? 'SIM' : 'N�O'}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
								         	</rich:suggestionbox>
										</td> 
									</tr> 
								</table>					
							</fieldset>							

							<br />
							
							<h:commandButton action="#{historicoSinistroHandler.relatorioHistoricoSinistros}" value="Gerar Relatorio Impress�o" id="pesquisar" styleClass="button" tabindex="9"/>
							<p:commandButton action="#{historicoSinistroHandler.gerarRelatorioExcel}"
										value="Gerar Relat�rio Excel" id="gravar" styleClass="button"
										async="false" ajax="false" />
							<p:commandButton actionListener="#{historicoSinistroHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="10">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
