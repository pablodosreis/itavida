<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="movSuperApHandler" />


<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />
<script type="text/javascript" src="<%= request.getContextPath() %>/js/accounting.js"></script>

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery("#periodo").mask("99/9999",{placeholder:"_"});
	
	
	if (jQuery('#elementoAvancou').val() == 'true') {
		jQuery('#divMovimentacao').show();
		jQuery("#dataComissao").mask("99/99/9999",{placeholder:"_"});
		jQuery("#dataPagamento").mask("99/99/9999",{placeholder:"_"});
		jQuery("#dataPrevisaoBoleto").mask("99/99/9999",{placeholder:"_"});
		formataMonetario();
	} else {
		jQuery('#divMovimentacao').hide();
	}
	
});

function formataMonetario() {
	jQuery('#valorComissao').onBlur = function() {accounting.formatMoney(this.val(), "", 2, ",", ".");}
	jQuery('#valorPagamento').onBlur = function() {accounting.formatMoney(this.val(), "", 2, ",", ".");}
	/*jQuery('#valorComissao').priceFormat({
		prefix : '',
		centsSeparator : ',',
		thousandsSeparator : '.',
		centsLimit : 2,
		allowNegative: true,
		insertPlusSign: true
	});
	jQuery('#valorPagamento').priceFormat({
		prefix : '',
		centsSeparator : ',',
		thousandsSeparator : '.',
		centsLimit : 2,
		allowNegative: true,
		insertPlusSign: true
	});*/
	return false;
}
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Movimenta��o</span><li>Movimenta��o Super AP</li> </div>
						<div id="icone">
                           <img src="images/cadOrgaos.png" />
                        </div>
                          

				<h:form styleClass="niceform" prependId="false">
				
					<h:inputHidden value="#{movSuperApHandler.avancou}"  id="elementoAvancou"/>
					
					
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
			
									<div class="divRecuadoEsquerdo">
										<table>
											<tr>
												<td align="left" width="20px;">
												  <label for="periodo">Per�odo:</label> 
												</td>
												<td>
													<h:inputText value="#{movSuperApHandler.periodo}"  id="periodo" maxlength="10" size="12" rendered="#{!movSuperApHandler.avancou}" />
													<h:outputText value="#{movSuperApHandler.periodo}"  id="periodoOutput" rendered="#{movSuperApHandler.avancou}"/>
												</td>												
											</tr>
											<tr>
												<td></td>
												<td>
													<p:commandButton actionListener="#{movSuperApHandler.avancar}" rendered="#{!movSuperApHandler.avancou}" value="Avan�ar" id="avancar" styleClass="button" async="false" ajax="false" />
												</td>
											</tr>											
										</table>
									</div>
									<br />						
									<div class="divRecuadoEsquerdo" id="divMovimentacao" style="display: none; float: none">
									
									<fieldset style="float: left">
									
										<legend>Movimentacao Super AP</legend>
										<h:inputHidden value="#{movSuperApHandler.idMov}"  id="idMov"/>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="txtSegurado">Segurado:</label></td>
												<td colspan="3"><h:inputText
														value="#{movSuperApHandler.proposta.nomeSegurado}"
														id="txtNomeSegurado" size="40"  /> 
														<rich:suggestionbox
														id="suggestionBoxNomeSegurado" for="txtNomeSegurado"
														suggestionAction="#{movSuperApHandler.SeguaradoSuperApAutoComplete}"
														var="proposta" fetchValue="#{proposta.nomeSegurado}"
														bypassUpdates="true" width="500" height="250"
														cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
														
														<a4j:support event="onselect" id="a4jsupport"
															ignoreDupResponses="true" reRender="valorPagamento,dataPrevisaoBoleto,numeroParcela,periodicidade">
															<f:setPropertyActionListener value="#{proposta}"
																target="#{movSuperApHandler.proposta}" />
														</a4j:support>
														<h:column>
															<h:outputText value="#{proposta.nomeSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{proposta.id}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText
																value="#{proposta.cpfSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>														
													</rich:suggestionbox></td>
											</tr>
											<tr>
												<td align="right" width="130px;">
													<label for="dataPagamento">Data do Pagamento:</label>
												</td>
												<td>
													<h:inputText value="#{movSuperApHandler.dataPagamento}"  id="dataPagamento" maxlength="10" size="12" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
												<td align="right" >
													<label for="periodicidade">Periodicidade:</label>
												</td>
												<td><h:inputText value="#{movSuperApHandler.periodicidade}"  id="periodicidade" maxlength="10" size="12" disabled="true"/></td>
											</tr>
											<tr>
												<td align="right" >
													<label for="numeroParcela">N�mero da Parcela:</label>
												</td>
												<td><h:inputText value="#{movSuperApHandler.numeroParcela}"  id="numeroParcela" maxlength="10" size="12" /></td>
												<td align="right" width="130px;">
													<label for="valorPagamento">Valor:</label>
												</td>
												<td>
													<h:inputText value="#{movSuperApHandler.valorPagamento}"  id="valorPagamento" maxlength="10" size="12">
														<f:converter converterId="MyDoubleConverter" />
													</h:inputText>
												</td>
												
												
											</tr>
				
											<tr>
												<td align="right" >
													<label for="dataComissao">Data da Comiss�o:</label>
												</td>
												<td>
													<h:inputText value="#{movSuperApHandler.dataPagamentoComissao}"  id="dataComissao" maxlength="10" size="12" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
												<td align="right">
													<label for="valorComissao">Valor Comiss�o:</label>
												</td>
												<td>
													<h:inputText value="#{movSuperApHandler.valorComissao}"  id="valorComissao" maxlength="10" size="12" >
														<f:converter converterId="MyDoubleConverter" />
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td align="right">
													<label for="dataPrevisaoBoleto">Previs�o do boleto:</label>
												</td>
												<td>
													<h:inputText value="#{movSuperApHandler.dataPrevisaoBoleto}"  id="dataPrevisaoBoleto" maxlength="10" size="12"  >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
													</h:inputText>
												</td>
												
												<td align="right">
													<label for="cmbSituacaoBoleto">Situa��o do boleto:</label>
												</td>
												<td><h:selectOneMenu id="cmbSituacaoBoleto"
												value="#{movSuperApHandler.situacaoBoletoId}"
												>
												<f:selectItems value="#{movSuperApHandler.opcoesSituacaoBoletoCombo}" />
											</h:selectOneMenu></td>
											</tr>
											<tr>
												<td><label for="txtObservacao">Observa��o:</label></td>
												<td colspan="3"><h:inputTextarea value="#{movSuperApHandler.observacao}" id="txtObservacao" cols="50" rows="5"  />
												</td>
											</tr>
											<tr>
												<td></td>
												<td><p:commandButton actionListener="#{movSuperApHandler.incluirMovimentacao}" value="Incluir" id="gravar" styleClass="button" async="false" ajax="false" /></td>
												<td><p:commandButton actionListener="#{movSuperApHandler.limparMovimentacao}" value="Limpar" id="limpar" styleClass="button" async="false" ajax="false" /></td>
												<td><p:commandButton actionListener="#{movSuperApHandler.visualizarMovimentacoes}" value="Visualizar Registros Cadastrados" id="Visualizar" styleClass="button" async="false" ajax="false" /></td>
											</tr>											
										</table>
										
										
										

									</fieldset>
									
									
									
									</div>
									<!--  Fim Propostas inadimpmentes -->
									
									
									
									<p:dialog  id="listaMovimentacoes" visible="true" header="Movimenta��es cadastradas" rendered="#{movSuperApHandler.avancou && not empty movSuperApHandler.movimentacoes}" constrainToViewport="true" x="100" y="50" underlay="shadow" >
									
									
									
									<div style="overflow: auto; height:500px; width: 1200px" >
										<p:lightBox iframe="true" width="95%" height="95%" id="lightBoxProposta" currentTemplate="Mostrando item {current} de {total}" group="true" >
											<p:dataTable
												value="#{movSuperApHandler.movimentacoes}"
												var="item" width="100%">
<%-- 												rendered="#{not empty movSuperApHandler.movimentacoes}"> --%>
												
												<!-- paginator="true" rows="10"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;" >-->
												
												
												
												<p:column width="20">
													<f:facet name="header">
														<h:outputText value="A��o" />
													</f:facet>
													<h:commandLink actionListener="#{movSuperApHandler.editarMovimentacao}">
														<h:graphicImage title="Editar" alt="Editar"
															url="images/edit.png" style="border: none;" />
														<f:param id="editarMovimentacao" name="editarMovimentacao"
															value="#{item}" />
													</h:commandLink>
													<h:commandLink
														actionListener="#{movSuperApHandler.excluirMovimentacao}"
														onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluirMovimentacao" name="excluirMovimentacao"
															value="#{item}" />
													</h:commandLink>
												</p:column>
												
												<p:column resizable="true" sortBy="#{item.numeroParcela}" width="3" >
													<f:facet name="header">
														<h:outputText value="N�" />
													</f:facet>
													<h:outputText value="#{item.numeroParcela}">
													</h:outputText>
												</p:column>		
												
												<p:column resizable="true" sortBy="#{item.proposta.nomeSegurado}" width="120">
													<f:facet name="header">
														<h:outputText value="Proposta"/>
													</f:facet>
													<h:outputText value="N� #{item.proposta.id}" />
													<br/>
													<h:outputText value="#{item.proposta.nomeSegurado}" />
													<br/>
 													<h:outputText value="#{item.proposta.cpfSegurado}"/>
 													<br/>
 													<h:outputText value="Situa��o: #{item.proposta.situacao.nome}" />
												</p:column>
												
												<p:column resizable="true" sortBy="#{item.valor}" width="70">
													<f:facet name="header">
														<h:outputText value="Vr. Pagamento" />

													</f:facet>
													<h:outputText value="#{item.valor}">
														<f:convertNumber type="currency" groupingUsed="true" />
													</h:outputText>
												</p:column>	
												
												<p:column sortBy="#{item.dataPagamento}"  width="70">
													<f:facet name="header">
														<h:outputText value="Dt. Pgto" />
													</f:facet>
													<h:outputText value="#{item.dataPagamentoFormatada}"/>
												</p:column>	
												
												
												<p:column resizable="true" sortBy="#{item.valorComissao}" width="70">
													<f:facet name="header">
														<h:outputText value="Vr. Comiss�o" >															
														</h:outputText>

													</f:facet>
													<h:outputText value="#{item.valorComissao}">
														<f:convertNumber type="currency" groupingUsed="true" />
													</h:outputText>
												</p:column>
												
												<p:column sortBy="#{item.dataPagamentoComissao}"  width="70">
													<f:facet name="header">
														<h:outputText value="Dt. Comiss�o" />
													</f:facet>
													<h:outputText value="#{item.dataComissaoFormatada}"/>
												</p:column>	
												
												<p:column sortBy="#{item.dataPrevisaoBoleto}"  width="70">
													<f:facet name="header">
														<h:outputText value="Dt. Previs�o" />
													</f:facet>
													<h:outputText value="#{item.dataPrevisaoBoletoFormatada}"/>
												</p:column>	
												
												<p:column sortBy="#{item.situacaoBoleto}"  width="70">
													<f:facet name="header">
														<h:outputText value="Plano" />
													</f:facet>
													<h:outputText value="#{item.periodicidadeFormatada}"/>
												</p:column>	
												
												<p:column sortBy="#{item.situacaoBoleto}"  width="70">
													<f:facet name="header">
														<h:outputText value="Situa��o Boleto" />
													</f:facet>
													<h:outputText value="#{item.situacaoBoleto.nome}"/>
												</p:column>	
												
												<p:column sortBy="#{item.observacao}"  width="300">
													<f:facet name="header">
														<h:outputText value="Observa��o" />
													</f:facet>
													<h:outputText value="#{item.observacao}"/>
												</p:column>	
												
																							
												
											</p:dataTable>
											</p:lightBox>
											</div>
											
											<h:panelGrid columns="2" >
												<h:panelGroup>
												<fieldset style="width: 900px;">
												<legend style="font-size: 1.2em;">Sum�rio do Per�odo</legend>
												<h:panelGrid columns="2" >
													<h:panelGrid columns="2" >
														<h:outputText value="Total de Registros: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{movSuperApHandler.qtdRegistros}" />
														<h:outputText value="Total dos Pr�mios: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{movSuperApHandler.totalValor}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
														<h:outputText value="Total das Comiss�es: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{movSuperApHandler.totalComissao}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
													</h:panelGrid>
													<h:panelGrid columns="2" >
														<h:outputText value="Total de Pagos: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{movSuperApHandler.qtdRegistrosPago}" />
														<h:outputText value="Total dos Pr�mios (pagos): " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{movSuperApHandler.totalValorPago}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
														<h:outputText value="Total das Comiss�es (pagas): " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{movSuperApHandler.totalComissaoPago}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
													</h:panelGrid>
												</h:panelGrid>
												</fieldset>
												</h:panelGroup>
												<%--<h:commandLink >  
												    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
												    <p:dataExporter type="xls" target="resultTableView" fileName="Propostas"  postProcessor="#{propostaHandler.postProcessXLS}" />  
												</h:commandLink> --%>
											</h:panelGrid>

											
											
											
										
										
										</p:dialog>
								
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
