<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="controleAcessoHandler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Administra��o</span><li>Controle de Acesso</li> </div>
						<div id="icone">
                           <img src="images/password.png" />
                        </div>
						<h:form id="myform" prependId="false">
						

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right">
										  <label for="txtNomeUsuario">Nome Usuario:</label>
										</td>
										<td>
										 <h:inputText value="#{controleAcessoHandler.usuario.nomeUsuario}" id="txtNomeUsuario" size="60" maxlength="50" tabindex="1" autocomplete="off"/>
										  
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="txtIdUsuario">Identifica��o:</label>
										</td>
										<td>
										 <h:inputText value="#{controleAcessoHandler.usuario.usuario}" id="txtIdUsuario" size="15" maxlength="10" tabindex="1" autocomplete="off"/>
										</td>
									</tr>
									<tr>
									<td align="right">
										  <label for="txtSenhaUsuario">Senha Usuario:</label>
										</td>
										<td>
										 <p:password id="txtSenhaUsuario" value="#{controleAcessoHandler.usuario.senha}" minLength="1" autocomplete="off"
										 size="15" maxlength="10"
										 promptLabel="Digite a Senha de Acesso!" 
										 goodLabel="Essa � uma boa senha!" 
										 strongLabel="Essa � uma excelente senha!" 
										 weakLabel="Essa � uma senha fraca!" 
										 tabindex="2"
										 /> 
										</td>
									</tr>

									<tr>
										<td align="right">
										  <label for="txtPerfilAcesso">Perfil de Acesso:</label>
										</td>
										<td>
										   <h:selectOneMenu id="txtPerfilAcesso" value="#{controleAcessoHandler.usuario.perfil}" tabindex="3" >
										   	<f:selectItems value="#{controleAcessoHandler.perfilAcessoCombo}" />
										   </h:selectOneMenu>
										</td>									
									</tr>
								</table>					
							</fieldset>							

							<br />
							
							<h:commandButton actionListener="#{controleAcessoHandler.pesquisaControlesAcesso}" value="Consultar" id="pesquisar" styleClass="button" tabindex="3" />
							<h:commandButton actionListener="#{controleAcessoHandler.gravar}" value="Salvar" id="salvar" styleClass="button" tabindex="4" />
							<p:commandButton actionListener="#{controleAcessoHandler.cancelar}" value="Limpar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="5">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
				<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty controleAcessoHandler.resultadoPesquisa}"  constrainToViewport="true" fixedCenter="true" underlay="shadow" > 

									<p:dataTable id="resultTable" value="#{controleAcessoHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty controleAcessoHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.nomeUsuario}">
										  <f:facet name="header">
										  <h:outputText value="Nome do Usu�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeUsuario}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.perfil}">
										  <f:facet name="header">
										  <h:outputText value="Perfil do Usu�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.perfil}"/>									  										
										</p:column>		
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  action="#{controleAcessoHandler.preparaAlterarUsuario}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:setPropertyActionListener target="#{controleAcessoHandler.usuario}" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{controleAcessoHandler.excluirUsuario}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirUsuario" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="Usuarios"  postProcessor="#{controleAcessoHandler.postProcessXLS}" excludeColumns="2,3"/>  
										</h:commandLink>
								</div>
						</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
