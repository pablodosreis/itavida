<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="filialHandler" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<HTML 
xmlns="http://www.w3.org/1999/xhtml" lang="pt">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper(); 
	   jQuery("#txtCep").mask("99.999-999",{placeholder:"_"});
	   jQuery("#txtTelefone").mask("(99)9?9999-9999",{placeholder:"_"});
	   jQuery("#txtFax").mask("(99)9999-9999",{placeholder:"_"});
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Filiais</li> </div> 
						<div id="icone"> 
                           <img src="images/cadFiliais.png" /> 
                        </div> 

				<h:form id="myform" styleClass="niceform" prependId="false" >
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
									<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeFilial">Nome Filial:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.nomeFilial}" id="txtNomeFilial" size="40" maxlength="50" tabindex="1"/>
										</td> 
									</tr> 
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
									<tr> 
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtEndereco">Endere�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.endereco}" id="txtEndereco" size="60" maxlength="110" tabindex="2"/>
										</td>								
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtBairro">Bairro:</label> 
										</td> 
										<td> 
											<h:inputText value="#{filialHandler.filial.bairro}" id="txtBairro" size="40" maxlength="50" tabindex="3"/>
										</td>								
									</tr>										
								</table>	
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeCidade">Cidade:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.cidade.nom_cidade}" id="txtNomeCidade" size="30" tabindex="4" >
												<p:ajax event="keyup" update="txtUf" >
													<f:setPropertyActionListener 
														value="#{filialHandler.limparCidade}" 
														target="#{filialHandler.filial.cidade}"/> 
												</p:ajax>												
											</h:inputText>
										   <h:inputText value="#{filialHandler.filial.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" style="width:30px;" disabled="true" styleClass="textinput" />
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeCidade"
								                    suggestionAction="#{filialHandler.cidadeAutocomplete}" 
								                    var="cidade"
								                    fetchValue="#{cidade.nom_cidade}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" reRender="txtUf"> 
														<f:setPropertyActionListener 
														value="#{cidade}" 
														target="#{filialHandler.filial.cidade}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtCep">CEP:</label> 
										</td> 
										<td> 
											<h:inputText value="#{filialHandler.filial.cep}" id="txtCep" size="10" maxlength="10" tabindex="5"/>											
										</td> 
									</tr> 
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
									<tr> 
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtTelefone">Telefone:</label> 
										</td> 
										<td> 
									    <h:inputText value="#{filialHandler.filial.telefone}" id="txtTelefone" size="15" maxlength="15" tabindex="6"/>	
										</td>	
										<td align="right" width="90px;"> 
										  <label for="txtFax">Fax:</label> 
										</td> 
										<td> 
											<h:inputText value="#{filialHandler.filial.fax}" id="txtFax" size="15" maxlength="15" tabindex="7"/>
										</td>											
									</tr> 
								</table> 
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtEmail">Email:</label> 
										</td> 
										<td>
											<h:inputText value="#{filialHandler.filial.email}" id="txtEmail" size="42" maxlength="50" tabindex="8"/> 
										</td>								
									</tr>										
								</table>
						</fieldset>							
							<br />
							<p:commandButton actionListener="#{filialHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="9"/>
							<p:commandButton actionListener="#{filialHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="11">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>


</BODY>

</HTML>
</f:view>
