<%@ include file="/common/cabecalho.jsp"%>
<!-- ENDERE�O -->
<fieldset>
	<legend>Endere�o</legend>
	<table>
		<tr>
			<td align="right" width="90px;"><label
				for="txtEndereco">Endere�o:</label></td>
			<td><h:inputText
				value="#{propostaHandler.proposta.enderecoSegurado}"
				id="txtEndereco" size="50" maxlength="110"  />
		</td>
	</tr>
	<table>

	<tr>
		<td align="right" width="90px;"><label
			for="txtNumeroEndereco">N�mero:</label></td>
		<td><h:inputText
				value="#{propostaHandler.proposta.numeroEnderecoSegurado}"
				id="txtNumeroEndereco" size="25" maxlength="50"
				 /></td>
		<td align="left" width="90px;"><label
			for="txtComplemento">Complemento:</label></td>
		<td><h:inputText
				value="#{propostaHandler.proposta.complemento}"
				id="txtComplemento" size="25" maxlength="100"
				 /></td>
	</tr>
	</table>
</table>
<table>
	<tr>
		<td align="right" width="90px;"><label for="txtBairro">Bairro:</label>
		</td>
		<td><h:inputText
				value="#{propostaHandler.proposta.bairro}" id="txtBairro"
				size="40" maxlength="50"  /></td>
	</tr>
</table>
<table>
	<tr>
		<td align="right" width="90px;"><label
			for="txtNomeCidade">Cidade:</label></td>
		<td><h:inputText
				value="#{propostaHandler.proposta.cidade.nom_cidade}"
				id="txtNomeCidade" size="30"  /> <h:inputText
				value="#{propostaHandler.proposta.cidade.cod_estado.sgl_estado}"
				id="txtUf" size="3" style="width:30px;" disabled="true"
				styleClass="textinput" /> <rich:suggestionbox
				id="suggestionBoxCidade" for="txtNomeCidade"
				suggestionAction="#{propostaHandler.cidadeAutocomplete}"
				var="cidade" fetchValue="#{cidade.nom_cidade}" width="300"
				height="250" cellpadding="4"
				nothingLabel="Nenhum Item Encontrado!">
				<a4j:support event="onselect" id="a4jsupport"
					ignoreDupResponses="true" reRender="txtUf">
					<f:setPropertyActionListener value="#{cidade}"
						target="#{propostaHandler.proposta.cidade}" />
				</a4j:support>
				<h:column>
					<h:outputText value="#{cidade.nom_cidade}"
						styleClass="autocompleteFonte" />
				</h:column>
				<h:column>
					<b> <h:outputText
							value="#{cidade.cod_estado.sgl_estado}"
							styleClass="autocompleteFonte" /></b>
				</h:column>
			</rich:suggestionbox></td>
	</tr>
</table>
<table>
	<tr>
		<td align="right" width="90px;"><label for="txtCep">CEP:</label>
		</td>
		<td><h:inputText
				value="#{propostaHandler.proposta.cep}" id="txtCep"
				size="10" maxlength="10"  /></td>
	</tr>
</table>
<table>
	<tr>
		<td align="right" width="90px;"><label
			for="txtTelefoneResidencial">Telefone Residencial:</label>
		</td>
		<td><h:inputText
				value="#{propostaHandler.proposta.telefoneResidencial}"
				id="txtTelefoneResidencial" size="13" maxlength="13"
				 /></td>
		<td align="right" width="100px;"><label
			for="txtTelefoneComercial">Telefone Comercial:</label></td>
		<td><h:inputText
				value="#{propostaHandler.proposta.telefoneComercial}"
				id="txtTelefoneComercial" size="13" maxlength="13"
				 /></td>
	</tr>
	<tr>
		<td align="right" width="90px;"><label
			for="txtTelefoneCelular">Telefone Celular:</label></td>
		<td><h:inputText
				value="#{propostaHandler.proposta.telefoneCelular}"
				id="txtTelefoneCelular" size="13" maxlength="13"
				 /></td>
		<td align="right" width="100px;"><label
			for="txtTelefoneCelular2">Telefone Celular 2:</label></td>
		<td><h:inputText
				value="#{propostaHandler.proposta.telefoneCelular2}"
				id="txtTelefoneCelular2" size="13" maxlength="13"
				 /></td>
	</tr>
</table>
<table>
	<tr>
		<td align="right" width="90px;"><label
			for="txtObservacao">Observa��es:</label></td>
		<td><h:inputTextarea
				value="#{propostaHandler.proposta.observacao}"
				id="txtObservacao" cols="50" rows="5"  /></td>
		</tr>
	</table>
</fieldset>

<rich:spacer height="20px;" />
<h:panelGroup id="agrupamento_Historico_Endereco"
	styleClass="agrupamento_Dados">
<fieldset>
	<legend>
		<IMG title="Expandir" id="expandirconteudoHistoricoEndereco"
			alt="Expandir"
			onclick="expandir('conteudoHistoricoEndereco');"
			src="images/expandir.png" border=0 /> <IMG
			id="retrairconteudoHistoricoEndereco" title="Retrair"
			alt="Retrair"
			onclick="retrair('conteudoHistoricoEndereco');"
			src="images/retrair.png" border=0 style="display: none;" />
		Hist�rico de Altera��es Endere�o
	</legend>
	<div id="conteudoHistoricoEndereco"
		style="display: none; width: 400px;">
		<p:dataTable
			value="#{propostaHandler.proposta.historicoenderecosegurado_collection}"
			var="historicoEndereco" width="50%" style="width: 300px;"
			rendered="#{not empty propostaHandler.proposta.historicoenderecosegurado_collection}">

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Data Altera��o" />

				</f:facet>
				<h:outputText value="#{historicoEndereco.dataAlteracao}"
					style="text-transform:uppercase;">
					<f:convertDateTime dateStyle="default"
						pattern="dd/MM/yyyy HH:mm" />
				</h:outputText>
			</p:column>
			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Endere�o" />

				</f:facet>
				<h:outputText
					value="#{historicoEndereco.enderecoSegurado}" />
			</p:column>
			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Complemento" />

				</f:facet>
				<h:outputText value="#{historicoEndereco.complemento}" />
			</p:column>
			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Bairro" />

				</f:facet>
				<h:outputText value="#{historicoEndereco.bairro}" />
			</p:column>
			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Cidade" />

				</f:facet>
				<h:outputText
					value="#{historicoEndereco.cidade.nom_cidade}" />
			</p:column>

			<p:column resizable="true" width="10">
				<f:facet name="header">
					<h:outputText value="Estado" />

				</f:facet>
				<h:outputText
					value="#{historicoEndereco.cidade.cod_estado.sgl_estado}" />
			</p:column>

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Cep" />

				</f:facet>
				<h:outputText value="#{historicoEndereco.cep}" />
			</p:column>

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Telefone Residencial" />

				</f:facet>
				<h:outputText
					value="#{historicoEndereco.telefoneResidencial}" />
			</p:column>

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Telefone Comercial" />

				</f:facet>
				<h:outputText
					value="#{historicoEndereco.telefoneComercial}" />
			</p:column>

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Telefone Celular 1" />

				</f:facet>
				<h:outputText
					value="#{historicoEndereco.telefoneCelular1}" />
			</p:column>

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Telefone Celular 2" />

				</f:facet>
				<h:outputText
					value="#{historicoEndereco.telefoneCelular2}" />
			</p:column>

			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Observa��es" />

				</f:facet>
				<h:outputText value="#{historicoEndereco.observacao}" />
			</p:column>


			<p:column resizable="true">
				<f:facet name="header">
					<h:outputText value="Usu�rio Altera��o" />

				</f:facet>
				<h:outputText value="#{historicoEndereco.nomeUsuario}" />
			</p:column>
		</p:dataTable>
	</div>
</fieldset>
</h:panelGroup>
