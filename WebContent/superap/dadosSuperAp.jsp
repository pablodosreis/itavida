<%@ include file="/common/cabecalho.jsp"%>
<fieldset>
	<legend>Super Ap</legend>

	<table>
		
		<tr>
			<td align="right" width="150px;"><label for="txtIdProposta">N�mero
					da Proposta:</lab	el></td>
			<td><h:outputText
					value="#{propostaHandler.numeroPropostaIncluir}" id="txtIdProposta" /></td>
		
		</tr>


		<tr>
			<td align="right" width="150px;"><label for="cmbEmpresa">Empresa:</label></td>
			<td><h:selectOneMenu id="cmbEmpresa"
					value="#{propostaHandler.proposta.empresa.id}" >
					<f:selectItems value="#{propostaHandler.opcoesEmpresaCombo}" />
				</h:selectOneMenu></td>
	
			<td align="right" width="150px;">
				<label for="cmbDadosPlano">
					Plano:
				</label>
			</td>
			
			
		
			<td>
				<h:selectOneMenu id="cmbDadosPlano" value="#{propostaHandler.proposta.superAp.plano.id}" >
					<f:selectItems value="#{propostaHandler.opcoesPlanoSuperAp}" />
				</h:selectOneMenu>
			</td>
			
			</tr>


			<tr>
				
			<td align="right" width="150px;">
				<label for="cobertura1">
					Cobertura1:
				</label>
			</td>
			
			<td>
				<h:selectOneMenu id="cobertura1"
					value="#{propostaHandler.proposta.superAp.coberturaOpcional1.id}" >
					<f:selectItems value="#{propostaHandler.opcoesCoberturasSuperAp}" />
			</h:selectOneMenu>
			</td>
				
		
			<td align="right" width="150px;">
				<label for="cmbDadosPlano2">
					Cobertura2:
				</label>
			</td>
			
			<td>
				<h:selectOneMenu id="cmbDadosPlano2"
					value="#{propostaHandler.proposta.superAp.coberturaOpcional2.id}" >
					<f:selectItems value="#{propostaHandler.opcoesCoberturasSuperAp2}" />
			</h:selectOneMenu>
			</td>
				
		</tr>
		
		<tr>
			<td align="right" width="150px;">
				<label for="cmbRenda">
					Renda:
				</label>
			</td>
			
			<td>
				<h:selectOneMenu id="cmbrenda" value="#{propostaHandler.proposta.superAp.renda}" >
					<f:selectItems value="#{propostaHandler.opcoesRenda}" />
				</h:selectOneMenu>
			</td>
			
			<td align="right" width="150px;">
				<label for="diaVencimento">
					Dia do Vencimento:
				</label>
			</td>
			
			<td>
				<h:inputText
					value="#{propostaHandler.proposta.superAp.diaVencimento}"
					id="diaVencimento" size="10" maxlength="50"  />
			</td>
				
		</tr>


		<tr>
			<td align="right" width="150px;">
				<label for="exposta">
					Pessoa politicamente exposta:
				</label>
			</td>
			
			<td>
				<h:selectOneMenu id="exposta" value="#{propostaHandler.proposta.superAp.pessoaPoliticamenteExposta}" >
					<f:selectItems value="#{propostaHandler.simNaoComboBoolean}" />
				</h:selectOneMenu>
			</td>
				
			<td align="right" width="150px;">
				<label for="periodicidade">
					Periodicidade:
				</label>
			</td>
			
			<td>
				<h:selectOneMenu id="periodicidade" value="#{propostaHandler.proposta.superAp.periodicidade}" >
					<f:selectItems value="#{propostaHandler.periodicidade}" />
				</h:selectOneMenu>
			</td>
				
		</tr>
		<tr>
			<td align="right" width="150px;">
				<label for="certificado">
					Certificado
				</label>
			</td>
			
			<td>
				<h:inputText
					value="#{propostaHandler.proposta.superAp.certificado}"
					id="certificado" size="10" maxlength="50"  />
			</td>
			
			<td align="right" width="150px;">
				<label for="dtEnvioCertificado">
					Data de envio do Certificado
				</label>
			</td>
			
			<td>
				<h:inputText
					value="#{propostaHandler.proposta.superAp.dataEnvioCertificado}"
					id="dtEnvioCertificado" size="10" maxlength="50"  >
					<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
				</h:inputText>
			</td>
		
		
		</tr>


	</table>

</fieldset>