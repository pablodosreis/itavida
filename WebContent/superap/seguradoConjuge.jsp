<%@ include file="/common/cabecalho.jsp"%>
<!-- SEGURADO CONJUGE -->
<fieldset>
	<legend>Dados do Segurado C�njuge</legend>
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="txtNomeSeguradoConjuge">Nome:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.nomeConjuge}"
					id="txtNomeSeguradoConjuge" size="50" maxlength="50"  /></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label for="cmbSexoConjuge">Sexo:</label></td>
			<td><h:selectOneMenu id="cmbSexoConjuge"
					value="#{propostaHandler.proposta.sexoConjuge}" >
					<f:selectItems value="#{propostaHandler.sexoCombo}" />
				</h:selectOneMenu></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="dtNascimentoConjuge">Data Nascimento:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.dataNascimentoConjuge}"
					id="dtNascimentoConjuge" maxlength="10" >
					<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
					<p:ajax
						actionListener="#{propostaHandler.calculaIdadeSeguradoConjuge}"
						update="txtIdadeConjuge" event="blur" />
				</h:inputText></td>
			<td><h:outputText id="txtIdadeConjuge"
					value="#{propostaHandler.idadeConjuge}" /></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="txtMatriculaConjuge">Matr�cula:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.matriculaConjuge}"
					id="txtMatriculaConjuge" size="10" maxlength="10"  /></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtCPFConjuge">CPF:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.cpfConjuge}"
					id="txtCPFConjuge" size="20" maxlength="14" >
				</h:inputText></td>
		</tr>
		<tr>
			<td align="right" width="150px;"><label for="txtRGConjuge">RG:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.rgConjuge}"
					id="txtRGConjuge" size="20" maxlength="20"  /></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label for="lblProfissaConjuge">Profiss�o:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.profissaoConjuge}"
					id="txtProfissaoConjuge" size="40" maxlength="100" /></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="cmbEstadoCivilConjuge">Estado Civil:</label></td>
			<td><h:selectOneMenu id="cmbEstadoCivilConjuge"
					value="#{propostaHandler.proposta.estadoCivilConjuge}"
					>
					<f:selectItems value="#{propostaHandler.estadoCivilCombo}" />
				</h:selectOneMenu></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtCapitalConjuge"
				style="font-weight: bold;">Capital:</label></td>
			<td><h:outputText
					value="#{propostaHandler.proposta.capitalConjuge}"
					id="txtCapitalConjuge" style="font-size: 14px;font-weight: bold;">
					<f:convertNumber currencySymbol="R$" type="currency"
						groupingUsed="true" />
				</h:outputText></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtPremioConjuge"
				style="font-weight: bold;">Pr�mio:</label></td>
			<td id="txtPremioConjugetd"><h:outputText
					value="#{propostaHandler.proposta.premioConjuge}"
					id="txtPremioConjuge" style="font-size: 14px;font-weight: bold;">
					<f:convertNumber currencySymbol="R$" type="currency"
						groupingUsed="true" />
				</h:outputText></td>
			<td id="txtPremioConjugeDigitadotd"><h:inputText
					id="txtPremioConjugeDigitado"
					value="#{propostaHandler.proposta.valorPremioConjuge}" size="20"
					style="width: 200px;">
					<f:converter converterId="MyDoubleConverter" />
					<p:ajax actionListener="#{propostaHandler.populaPremioConjuge}"
						update="txtTotalPremios" event="blur" id="a4jsupportConjuge4" />

				</h:inputText></td>
		</tr>

		<tr>
			<td align="right" width="150px;" id="labelCapitalFuturoTD"><label for="txtCapitalConjugeFuturo"
				style="font-weight: bold;">Capital Futuro:</label></td>
			<td><h:outputText
					value="#{propostaHandler.proposta.capitalConjugeFuturo}"
					id="txtCapitalConjugeFuturo" style="font-size: 14px;font-weight: bold;">
					<f:convertNumber currencySymbol="R$" type="currency"
						groupingUsed="true" />
				</h:outputText></td>
		</tr>
		
		<tr>
			<td align="right" width="150px;" id="labelPremioFuturoTD"><label for="txtPremioConjuge"
				style="font-weight: bold;">Pr�mio Futuro:</label></td>
			<td id="txtPremioConjugeFuturotd"><h:outputText
					value="#{propostaHandler.proposta.premioConjugeFuturo}"
					id="txtPremioConjugeFuturo" style="font-size: 14px;font-weight: bold;">
					<f:convertNumber currencySymbol="R$" type="currency"
						groupingUsed="true" />
				</h:outputText></td>
			<td id="txtPremioConjugeDigitadoFuturotd"><h:inputText
					id="txtPremioConjugeDigitadoFuturo"
					value="#{propostaHandler.proposta.valorFuturoPremioConjuge}" size="20"
					style="width: 200px;">
					<f:converter converterId="MyDoubleConverter" />
					<p:ajax actionListener="#{propostaHandler.populaPremioConjuge}"
						update="txtTotalPremios" event="blur" id="a4jsupportConjugeFuturo4" />

				</h:inputText></td>
		</tr>
	</table>
	
	<table id="tableValorConjugeFuturoFixo">
		<tr>
			<td align="right" width="150px;">
			<label for="txtCapitalFuturoConjugeFixo"
			style="font-weight: bold;">Capital Futuro:</label></td>
							
			<td><h:inputText id="txtCapitalConjugeDigitadoFixo"
				value="#{propostaHandler.fixoCapitalConjugeFuturo}"
				maxlength="15" styleClass="textinput" size="20"
				style="width: 200px;">
				<f:converter converterId="MyDoubleConverter" />
			</h:inputText></td>
		</tr>
		<tr>
			<td align="right" width="150px;">
			<label for="txtCapitalConjugeFuturoFixo"
			style="font-weight: bold;">Pr�mio Futuro:</label></td>
		
			<td id="txtPremioConjugeFuturoFixo">
			<h:inputText id="txtPremioConjugeFuturoFixoInput"
			value="#{propostaHandler.fixoPremioConjugeFuturo}"
			size="20" style="width: 200px;">
			<f:converter converterId="MyDoubleConverter" />
			</h:inputText>
			</td>
		</tr>
	</table>
</fieldset>

<rich:spacer height="20px;" />
<jsp:include page="../proposta/historicoConjuge.jsp" />
