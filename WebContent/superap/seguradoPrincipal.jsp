<%@ include file="/common/cabecalho.jsp"%>
<fieldset>
	<legend>Dados do Segurado Principal</legend>

	<!--  nome do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label
				for="txtNomeSeguradoPrincipal">Nome:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.nomeSegurado}"
					id="txtNomeSeguradoPrincipal" size="40" maxlength="50"  /></td>
		</tr>
	</table>

	<!--  email do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtEmail">Email:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.email}"
					id="txtEmail" size="30" maxlength="100"  /></td>
			<td><a href="javascript:sendMail()" >Enviar
					Email</a></td>
		</tr>
	</table>

	<!--  sexo do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="cmbSexoSegurado">Sexo:</label></td>
			<td><h:selectOneMenu id="cmbSexoSegurado"
					value="#{propostaHandler.proposta.sexoSegurado}" >
					<f:selectItems value="#{propostaHandler.sexoCombo}" />
				</h:selectOneMenu></td>
			<td align="right" width="150px;"><label
				for="dtNascimentoSegurado">Data Nascimento:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.dataNascimentoSegurado}"
					id="dtNascimentoSegurado" maxlength="10" >
					<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
					<p:ajax
						actionListener="#{propostaHandler.calculaIdadeSeguradoPrincipal}"
						update="txtIdadeSegurado" event="blur" />
				</h:inputText></td>
			<td><h:outputText id="txtIdadeSegurado"
					value="#{propostaHandler.idadeSegurado}" /></td>

		</tr>
	</table>

	<!--  matr�cula do segurado -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtMatricula">Matr�cula:</label></td>
			<td><h:inputText
					value="#{propostaHandler.proposta.matriculaSegurado}"
					id="txtMatricula" size="10" maxlength="10"  /></td>
			<td align="right" width="110px;"><label for="txtCpf">CPF:</label>
			</td>
			<td><h:inputText value="#{propostaHandler.proposta.cpfSegurado}"
					id="txtCpf" size="20" maxlength="14" >
				</h:inputText></td>
		</tr>
	</table>

	<!--  RG DO SEGURADO -->
	<table>
		<tr>
			<td align="right" width="150px;"><label for="txtRGSegurado">RG:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.rgSegurado}"
					id="txtRGSegurado" size="20" maxlength="20"  /></td>
			<td align="right" width="150px;"><label
				for="cmbEstadoCivilSegurado">Estado Civil:</label></td>
			<td><h:selectOneMenu id="cmbEstadoCivilSegurado"
					value="#{propostaHandler.proposta.estadoCivilSegurado}"
					>
					<f:selectItems value="#{propostaHandler.estadoCivilCombo}" />
				</h:selectOneMenu></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="right" width="150px;"><label for="lblProfissaoSegurado">Profiss�o:</label></td>
			<td><h:inputText value="#{propostaHandler.proposta.profissaoSegurado}"
					id="txtProfissaoSegurado"  size="40" maxlength="100" /></td>
		</tr>
	</table>

	
	<jsp:include page="../proposta/historicoSeguradoPrincipal.jsp" />
	
	
</fieldset>

