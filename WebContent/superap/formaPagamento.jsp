<%@ include file="/common/cabecalho.jsp"%>
<fieldset>
	<legend>Forma de Pagamento</legend>

	<table>
		
		<tr>
			<td align="right" width="150px;">
				<label for="tipoPagamento">Tipo:</label>
			</td>
			
			<td>
				<h:selectOneMenu id="tipoPagamento" value="#{propostaHandler.proposta.formaPagamento.tipo}" onchange="prepararformaPagamento()" >
					<f:selectItems value="#{propostaHandler.tiposPagamento}" />
				</h:selectOneMenu>
			</td>
		</tr>
		
	
	</table>
	
	<table id="tableTitular">	
		
		<tr>
			<td align="right" width="150px;"><label
				for="txtnomeTitular">Nome do Titular:</label></td>
			<td><h:inputText
				value="#{propostaHandler.proposta.formaPagamento.nomeTitular}"
				id="txtnomeTitular" size="10" maxlength="100"  />
				
			<td align="right" width="90px;"><label
				for="txtcpfTitular">CPF do Titular:</label></td>
			
			<td><h:inputText
				value="#{propostaHandler.proposta.formaPagamento.cpfTitular}"
				id="txtcpfTitular" size="10" maxlength="11"  />
			</td>
			
			<td align="right" width="90px;"><label
				for="txtParentesco">Parentesco:</label></td>
				
			<td><h:inputText
				value="#{propostaHandler.proposta.formaPagamento.parentesco}"
				id="txtParentesco" size="10" maxlength="15"  />
			</td>
		</tr>
	</table>
	
	
	
	
	
	
	
	<table id="tableBanco">
		
		<tr>
			<td align="right" width="150px;">
				<label for=cmbBanco>Banco:</label>
			</td>
			
			<td>
				<h:selectOneMenu id="cmbBanco" value="#{propostaHandler.proposta.formaPagamento.banco.id}" style="width: 90px;">
					<f:selectItems value="#{propostaHandler.bancos}" />
				</h:selectOneMenu>
			</td>
		
			<td align="right" width="90px;"><label
				for="txtNumeroAgencia">N� da Ag�ncia:</label></td>
			<td><h:inputText
				value="#{propostaHandler.proposta.formaPagamento.numeroAgencia}"
				id="txtNumeroAgencia" size="7" maxlength="10"  />
				
			<td align="right" width="90px;"><label
				for="txtNumeroConta">N� da Conta:</label></td>
			
			<td><h:inputText
				value="#{propostaHandler.proposta.formaPagamento.numeroConta}"
				id="txtNumeroConta" size="10" maxlength="20"  />
			</td>
			
			
		</tr>
	</table>
	<table id="tableCartao">
		
		<tr>
			<td align="right" width="150px;">
				<label for="cartaoCredito">Cart�o de Cr�dito:</label>
			</td>
			
			<td>
				<h:selectOneMenu id="cartaoCredito" value="#{propostaHandler.proposta.formaPagamento.cartaoCredito}" style="width: 90px;">
					<f:selectItems value="#{propostaHandler.cartoesCredito}" />
				</h:selectOneMenu>
			</td>
		
			<td align="right" width="150px;"><label
				for="txtNumeroCartao">�ltimos 4 d�gitos do cart�o:</label></td>		
			
			
			<td><h:inputText
				value="#{propostaHandler.proposta.formaPagamento.numeroCartao}"
				id="txtNumeroCartao" size="10" maxlength="4"  />
			</td>
			
			<td align="right" width="90px;"><label
				for="txtValidadeCartao">Validade:</label></td>
				
			<td><h:inputText
				value="#{propostaHandler.proposta.formaPagamento.validadeCartao}"
				id="txtValidadeCartao" size="10" maxlength="5"  />
			</td>
			
			
			
		</tr>
	</table>	
		


</fieldset>