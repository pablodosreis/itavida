<%@ include file="/common/cabecalho.jsp"%>
<!-- Ao atualizar, verificar se as altera��es impactam a tela de visualiza�ao propostaVisualizar.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
	<a4j:keepAlive beanName="novaPropostaHandler" />

<html xmlns="http://www.w3.org/1999/xhtml">
<jsp:include page="/common/head2.jsp" />

<head>
<link rel="stylesheet"
	href="${facesContext.externalContext.request.contextPath}/css/style.css"
	type="text/css" media="screen" />

<script type="text/javascript"
	src="${facesContext.externalContext.request.contextPath}/js/sliding.form.js"></script>

<script type="text/javascript">
	var jQuery = jQuery.noConflict();
	jQuery(function(jQuery) {
		startTabs();
		jQuery('#cmbOrgao').select2();
		jQuery('#cmbExemplo').select2();
		inicializar();
		
	});


	function inicializar() {

		//jQuery('#cmbEmpresa').focus();
		jQuery("#dtDataAprovacao").mask("99/99/9999", {
			placeholder : "_"
		});
		jQuery("#dtDataCadastro").mask("99/99/9999", {
			placeholder : "_"
		});
		jQuery("#dtDataCancelamento").mask("99/99/9999", {
			placeholder : "_"
		});
		jQuery("#dtContemplacaoSorteio").mask("99/99/9999", {
			placeholder : "_"
		});
		jQuery("#dtDataAgenciamento").mask("99/99/9999", {
			placeholder : "_"
		});
		jQuery("#dtDataExclusao").mask("99/99/9999", {
			placeholder : "_"
		});
		
	}
</script>
</head>

<DIV id=background_2>
	<jsp:include page="/common/menu.jsp" />
	<DIV id=wrap>
		<h:form styleClass="niceform" prependId="false" id="formulario">
			<!--  conteudo das abas -->

			<div id="content">
				<div id="wrapper" style="width: 100%;">
					<div id="steps">
						<h:form id="formElem">
							<fieldset class="step" >
								<legend>Segurado</legend>
								<jsp:include page="/proposta/abaSegurado.jsp" />
							</fieldset>
							<fieldset class="step">
								<legend>C�njuge</legend>
								<jsp:include page="/proposta/abaConjuge.jsp" />
							</fieldset>
							<fieldset class="step">
								<legend>Endere�o</legend>
								<jsp:include page="/proposta/abaEndereco.jsp" />
							</fieldset>
							<fieldset class="step" >
								<legend>Settings</legend>
								<jsp:include page="/proposta/abaTipoProposta.jsp" />
							</fieldset>
						</h:form>
					</div>
					<div id="navigation" style="display: none;">
						<ul>
							<li class="selected"><a href="#">Segurado</a></li>
							<li><a href="#">C�njuge</a></li>
							<li><a href="#">Endere�o</a></li>
							<li><a href="#">Pr�mio e Capital</a></li>
							<li><a href="#">Confirm</a></li>
						</ul>
					</div>
				</div>
			</div>

			<!-- fim do conteudo das abas -->

		</h:form>
	</DIV>
	<jsp:include page="/common/footer.jsp" />
</DIV>
</DIV>
</DIV>
</DIV>
</DIV>

</body>
	</html>
</f:view>