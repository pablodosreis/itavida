<%@ include file="/common/cabecalho.jsp"%>
<h:panelGroup id="agrupamento_Historico_Beneficiario"
											styleClass="agrupamento_Dados">
											<fieldset>
												<legend>
													<IMG title="Expandir"
														id="expandirconteudoHistoricoBeneficiarios" alt="Expandir"
														onclick="expandir('conteudoHistoricoBeneficiarios');"
														src="images/expandir.png" border=0 /><IMG
														id="retrairconteudoHistoricoBeneficiarios" title="Retrair"
														alt="Retrair"
														onclick="retrair('conteudoHistoricoBeneficiarios');"
														src="images/retrair.png" border=0 style="display: none;" />
													Hist�rico de Altera��es de Benefici�rios
												</legend>
												<div id="conteudoHistoricoBeneficiarios"
													style="display: none;">
													<p:dataTable
														value="#{propostaHandler.proposta.historicobeneficiario_collection}"
														var="historicoBeneficiario" width="50%"
														rendered="#{not empty propostaHandler.proposta.historicobeneficiario_collection}">

														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Data Altera��o" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.dataAlteracao}"
																style="text-transform:uppercase;">
																<f:convertDateTime dateStyle="default"
																	pattern="dd/MM/yyyy HH:mm" />
															</h:outputText>
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Nome Benefici�rio Antigo" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.nomeBeneficiario}" />
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Percentual Antigo" />

															</f:facet>
															<h:outputText value="#{historicoBeneficiario.percentual}" />
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Grau de Parentesco Antigo" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.grauParentesco.descricao}" />
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Usu�rio Altera��o" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.nomeUsuario}" />
														</p:column>
													</p:dataTable>
												</div>
											</fieldset>
										</h:panelGroup>
