<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="quitacaoOrgaoHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery("#periodoQuitacao").mask("99/9999",{placeholder:"_"});
	jQuery("#periodoFim").mask("99/9999",{placeholder:"_"});
});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Consultar Quita��o �rg�o</li> </div>
						<div id="icone">
                           <img src="images/cadOrgaos.png" />
                        </div>


				<h:form styleClass="niceform" prependId="false">

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
			
									<div class="divRecuadoEsquerdo">
										<table> 
											<tr> 
												<td align="left" width="90px;"> 
												   <label for="comboOrgao">�rg�o:</label> 
												</td> 
												<td align="right" colspan="3">
												   <h:selectOneMenu id="comboOrgao" binding="#{quitacaoOrgaoHandler.itemOrgaoSelecionado}" value="#{quitacaoOrgaoHandler.orgaoSelecionado}" tabindex="4">
												   	<f:selectItems value="#{quitacaoOrgaoHandler.orgaoCombo}"/>
												   </h:selectOneMenu>
												</td> 		
											</tr> 
											<tr>
												<td align="left" width="20px;">
												  <label for="periodoQuitacao">Per�odo:</label> 
												</td>
												<td>
													<h:inputText value="#{quitacaoOrgaoHandler.periodoQuitacao}"  id="periodoQuitacao" maxlength="10" size="12"  tabindex="6"/>
												</td>
												<td align="left" width="20px;">
												  <label for="periodoFim">at�:</label> 
												</td>
												<td>
													<h:inputText value="#{quitacaoOrgaoHandler.periodoFim}"  id="periodoFim" maxlength="10" size="12"  tabindex="6"/>
												</td>
											</tr>
											<tr> 
												<td align="left" width="90px;"> 
												   <label for="cmbQuitado">Quitado:</label> 
												</td> 
												<td colspan="3">
												   <h:selectOneMenu id="cmbQuitado" binding="#{quitacaoOrgaoHandler.itemBuscarQuitado}" value="#{quitacaoOrgaoHandler.buscarQuitado}" tabindex="2" >
												   	<f:selectItems value="#{quitacaoOrgaoHandler.simNaoCombo}"/>
												   </h:selectOneMenu>
												</td> 
	
											</tr> 
										</table>
									</div>
									<br />
									<div class="divRecuadoEsquerdo">
										<h:commandButton actionListener="#{quitacaoOrgaoHandler.pesquisaQuitacao}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="7"/>
									</div>
									<p/>
									<p/>
									<div class="divRecuadoEsquerdo">
										<p:dataTable id="resultTable" value="#{quitacaoOrgaoHandler.listaOrgaoQuitacao}" 
													 var="item" width="100%" 
													 rendered="#{not empty quitacaoOrgaoHandler.listaOrgaoQuitacao}" 
													dynamic="true"
													 >
											
											<p:column resizable="true" sortBy="#{item.orgao.nomeOrgao}" >
											  <f:facet name="header">
											 	 <h:outputText value="�rg�o" /> 
											  </f:facet>
											  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
											</p:column>
											<p:column resizable="true" >
											  <f:facet name="header">
											     <h:outputText value="M�s/Ano" /> 										  	
											  </f:facet>
											   <h:outputText value="#{item.mesAno}"/>						  										
											</p:column>	
											<p:column resizable="true" >
											  <f:facet name="header">
											     <h:outputText value="Quitado" /> 										  	
											  </f:facet>
											   <h:outputText value="#{item.labelQuitado}"/>						  										
											</p:column>	
																																																						
										</p:dataTable>
									</div>
						</fieldset>		
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
