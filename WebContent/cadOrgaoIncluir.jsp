<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="orgaoHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('#txtNomeOrgao').bestupper();
	   jQuery('#txtSiglaOrgao').bestupper();
	   jQuery('#txtNomeOrgao').focus(); 
	   jQuery("#txtCep").mask("99.999-999",{placeholder:"_"});
	   jQuery("#txtTelefone").mask("(99)9?9999-9999",{placeholder:"_"});
	   jQuery("#txtFax").mask("(99)9999-9999",{placeholder:"_"});
	   jQuery("#txtTelefoneContato").mask("(99)9?9999-9999",{placeholder:"_"});
	   jQuery("#txtCgc").mask("99.999.999/9999-99",{placeholder:"_"});
	   jQuery('#txtValorTaxa').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	 
	   jQuery('#txtValorProlabore').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		  
	});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>�rg�os</li> </div>
						<div id="icone">
                           <img src="images/cadOrgaos.png" />
                        </div>


				<h:form styleClass="niceform" prependId="false">

					<h:panelGrid id="cnpjMessage">
											<p:dialog header="MENSAGEM" widgetVar="dialog"  
											               fixedCenter="true" modal="true" visible="#{orgaoHandler.visibleMessage}" >
											   <h:messages errorClass="mensagem_erro" fatalClass="mensagem_erro" infoClass="mensagem_sucesso" warnClass="mensagem_alerta"/>                 
											</p:dialog>
					</h:panelGrid>

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
			
									<div class="divRecuadoDireito">
										<table> 
											<tr> 
												<td align="left" width="90px;"> 
												   <label for="cmbTipoOrgao">Tipo de �rg�o:</label> 
												</td> 
											</tr>
											<tr>
												<td align="right">
												   <h:selectOneMenu id="cmbTipoOrgao" binding="#{orgaoHandler.tipoOrgaoSelecionado}" value="#{orgaoHandler.orgao.tipoOrgao}" tabindex="4">
												   	<f:selectItems value="#{orgaoHandler.tipoOrgaoCombo}"/>
												   </h:selectOneMenu>
												</td> 
		
											</tr> 
										</table>
									</div>

									<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeOrgao">Nome Org�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.nomeOrgao}" id="txtNomeOrgao" size="40" maxlength="300" tabindex="1"/>
										</td> 
									</tr>
 										<tr>
											<td align="right" width="60px;">
											  <label for="textinput">Sigla:</label>
											</td>
											<td>
											   <h:inputText value="#{orgaoHandler.orgao.siglaOrgao}" id="txtSiglaOrgao" size="10" maxlength="300" tabindex="2" />
											</td>											
										</tr>
									
								</table>
								<table>
									<tr>
										<td align="right" width="60px;"> 
										  <label for="txtCgc">CGC:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.cgc}" id="txtCgc" size="20" maxlength="20" tabindex="3">
											  
											</h:inputText>
										</td> 
									</tr>
									<tr>
										<td align="right" width="60px;"> 
										  <label for="txtEmail">E-mail:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.email}" id="txtEmail" size="20" maxlength="60">
											</h:inputText>
										</td> 
									</tr>
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
							
								</table>
								<table>

									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtEndereco">Endere�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.endereco}" id="txtEndereco" size="60" maxlength="110" tabindex="5" />
										</td>								
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtBairro">Bairro:</label> 
										</td> 
										<td> 
											<h:inputText value="#{orgaoHandler.orgao.bairro}" id="txtBairro" size="40" maxlength="50" tabindex="6"/>
										</td>								
									</tr>										
								</table>	
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeCidade">Cidade:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{orgaoHandler.orgao.cidade.nom_cidade}" id="txtNomeCidade" size="30" tabindex="7" >
												<p:ajax event="keyup" update="txtUf" >
													<f:setPropertyActionListener 
														value="#{orgaoHandler.limparCidade}" 
														target="#{orgaoHandler.orgao.cidade}"/> 
												</p:ajax>	
											</h:inputText>
										   <h:inputText value="#{orgaoHandler.orgao.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" style="width:30px;" disabled="true" styleClass="textinput" />
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeCidade"
								                    suggestionAction="#{orgaoHandler.cidadeAutocomplete}" 
								                    var="cidade"
								                    fetchValue="#{cidade.nom_cidade}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" reRender="txtUf"> 
														<f:setPropertyActionListener 
														value="#{cidade}" 
														target="#{orgaoHandler.orgao.cidade}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtCep">CEP:</label> 
										</td> 
										<td> 
											<h:inputText value="#{orgaoHandler.orgao.cep}" id="txtCep" size="10" maxlength="10" tabindex="8"/>											
										</td> 
									</tr> 
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
									<tr> 
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtTelefone">Telefone:</label> 
										</td> 
										<td> 
									    <h:inputText value="#{orgaoHandler.orgao.telefone}" id="txtTelefone" size="15" maxlength="13" tabindex="9"/>	
										</td>	
										<td align="right" width="90px;"> 
										  <label for="txtFax">Fax:</label> 
										</td> 
										<td> 
											<h:inputText value="#{orgaoHandler.orgao.fax}" id="txtFax" size="15" maxlength="13" tabindex="10"/>
										</td>											
									</tr> 
								</table> 
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtHomePage">Home Page:</label> 
										</td> 
										<td>
											<h:inputText value="#{orgaoHandler.orgao.homePage}" id="txtHomePage" size="42" maxlength="50" tabindex="11"/> 
										</td>								
									</tr>										
								</table>
<div class="divRecuadoDireito">
								<table>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtPercentual" >Valor de Taxa:</label>
										</td>
										<td>
										   <h:inputText value="#{orgaoHandler.orgao.valorTaxa}" id="txtValorTaxa" size="10" maxlength="10" tabindex="12">
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
										  </h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtPercentual" >Valor de Prolabore:</label>
										</td>
										<td>
										  <h:inputText value="#{orgaoHandler.orgao.valorProlabore}" id="txtValorProlabore" size="10" maxlength="10" tabindex="13" >
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
										  </h:inputText>										 
										</td>
									</tr>
								</table>
</div>									

						</fieldset>		
						<fieldset>
								<legend>Contatos do �rg�o</legend>
								
								<table>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtNomeContato">Nome Contato:</label>
											</td>
											<td>
											   <h:inputText value="#{orgaoHandler.contatoOrgao.nomeContato}" id="txtNomeContato" size="40" maxlength="50" tabindex="14"/>
											</td>
										</tr>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtEmailContato">Email Contato:</label>
											</td>
											<td>
											   <h:inputText value="#{orgaoHandler.contatoOrgao.emailContato}" id="txtEmailContato" size="30" maxlength="50" tabindex="15"/>
											</td>
										</tr>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtTelefoneContato">Telefone Contato:</label>
											</td>
											<td>
											   <h:inputText value="#{orgaoHandler.contatoOrgao.telefoneContato}" id="txtTelefoneContato" size="15" maxlength="15" tabindex="16"/>
											</td>
										</tr>
								</table>
								<p:commandButton actionListener="#{orgaoHandler.incluirContatoOrgao}" value="Incluir Contato" id="gravarContato" styleClass="buttonDetalhe" async="false" ajax="false" tabindex="17"/>
								<p:commandButton actionListener="#{orgaoHandler.cancelarContatoOrgao}" value="Cancelar" id="cancelarContato" styleClass="buttonDetalhe" async="false" ajax="false" tabindex="18">
									<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
								</p:commandButton>	

								<div id="agrupamento_Dados" class="agrupamento_Dados">								
									<p:dataTable value="#{orgaoHandler.orgao.contatoorgao_collection}" 
												 var="contato" width="100%" 
												 rendered="#{not empty orgaoHandler.orgao.contatoorgao_collection}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Nome do Contato" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{contato.nomeContato}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Email" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{contato.emailContato}"/>									  										
										</p:column>		
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Telefone" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{contato.telefoneContato}"/>									  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  actionListener="#{orgaoHandler.alterarContatoOrgao}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:param id="editContatoOrgao" name="idAlterar" value="#{contato}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{orgaoHandler.excluirContatoOrgao}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirContatoOrgao" name="idExcluir" value="#{contato}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
							</div>
							</fieldset>	
					
							<br />
							<p:commandButton actionListener="#{orgaoHandler.gravar}" value="Gravar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="19"/>
							<p:commandButton actionListener="#{orgaoHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="20">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
