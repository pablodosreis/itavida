<%@ include file="common/cabecalho.jsp"%>
<!-- Ao atualizar, verificar se as altera��es impactam a tela de visualiza�ao propostaVisualizar.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
	<a4j:keepAlive beanName="propostaHandler" />

<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
	<script>

	function controlarCampos() {

		if (jQuery('#cmbModeloProposta').val() == 1 || jQuery('#cmbModeloProposta').val() == 2) {
			jQuery('#txtPremioSegurado2td').show();
			jQuery('#txtPremioSegurado2').show();
			jQuery('#txtPremioSeguradoFuturoExibicaotd').show();
			jQuery('#txtCapitalFuturoReangariadotd').hide();
		} else {
			jQuery('#txtPremioSegurado2').hide();
			jQuery('#txtPremioSegurado2td').hide();
			jQuery('#txtPremioSeguradoFuturoExibicaotd').hide();
			jQuery('#txtCapitalFuturoReangariadotd').show();
		}

		//VALOR FIXO
		if (jQuery('#cmbModeloProposta').val() == 1) {
			jQuery('#txtCapital1td').show();
			jQuery('#txtCapital1').show();
			jQuery('#tableValorFuturoSegurado').hide();
			jQuery('#tableValorFuturoFixo').show();
			jQuery('#tableValorConjugeFuturoFixo').show();
			jQuery('#labelCapitalFuturoTD').hide();
			jQuery('#labelPremioFuturoTD').hide();
			jQuery('#txtCapitalConjugeFuturo').hide();
			
		} else {
			jQuery('#txtCapital1').hide();
			jQuery('#txtCapital1td').hide();
			jQuery('#tableValorFuturoSegurado').show();
			jQuery('#tableValorFuturoFixo').hide();
			jQuery('#tableValorConjugeFuturoFixo').hide();
			jQuery('#labelCapitalFuturoTD').show();
			jQuery('#labelPremioFuturoTD').show();
			jQuery('#txtCapitalConjugeFuturo').show();
		}

		
		//VALOR FAIXA ET�RIA
		if (jQuery('#cmbModeloProposta').val() == 2) {
			jQuery('#txtCapital2td').show();
			jQuery('#txtCapital2').show();
			jQuery('#txtCapitalCapitalFuturoFaixaEtariatd').show();
			jQuery('#txtPremioConjugeFuturotd').show();
			
		} else {
			jQuery('#txtCapital2td').hide();
			jQuery('#txtCapital2').hide();
			jQuery('#txtCapitalCapitalFuturoFaixaEtariatd').hide();
			jQuery('#txtPremioConjugeFuturotd').hide();

		}

		//REANGARIADO OU ATUALIZADO
		if (jQuery('#cmbModeloProposta').val() == 3 || jQuery('#cmbModeloProposta').val() == 4) {
			jQuery('#labelNomeTabela').hide();
			jQuery('#txtNomeTabela').hide();
			jQuery('#panelTabelaCap').hide();
			jQuery('#txtCapital3td').show();
			jQuery('#txtCapital3').show();
			jQuery('#txtPremioConjugeDigitadotd').show();
			jQuery('#txtPremioConjugetd').hide();
			jQuery('#txtPremioSegurado22td').show();
			jQuery('#txtPremioSegurado22').show();
			jQuery('#txtPremioSeguradoFuturotd').show();
			jQuery('#txtPremioConjugeDigitadoFuturotd').show();
		} else {
			jQuery('#txtNomeTabela').show();
			jQuery('#labelNomeTabela').show();
			jQuery('#panelTabelaCap').show();
			jQuery('#txtCapital3td').hide();
			jQuery('#txtCapital3').hide();
			jQuery('#txtPremioSeguradoFuturotd').hide();
			jQuery('#txtPremioConjugeDigitadotd').hide();
			jQuery('#txtPremioConjugetd').show();
			jQuery('#txtPremioSegurado22').hide();
			jQuery('#txtPremioSegurado22td').hide();
			jQuery('#txtPremioConjugeDigitadoFuturotd').hide();
		}
		
		prepararformaPagamento();
		
		
		
	}

	
		function sendMail() {

			var email = jQuery('#txtEmail').val();

			if (email == undefined || email == '') {
				alert("O campo email n�o foi informado.");
				return;
			}

			var link = "mailto:" + email;

			window.location.href = link;
		}

		jQuery(function(jQuery) {

			jQuery('#cmbEmpresa').focus();

			jQuery('input').bestupper();
			jQuery("#dtNascimentoSegurado").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtNascimentoConjuge").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtDataAprovacao").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtDataCadastro").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtDataCancelamento").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtContemplacaoSorteio").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtDataAgenciamento").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#dtDataExclusao").mask("99/99/9999", {
				placeholder : "_"
			});
			jQuery("#txtCpf").mask("999.999.999-99", {
				placeholder : "_"
			});
			jQuery("#txtCPFConjuge").mask("999.999.999-99", {
				placeholder : "_"
			});
			jQuery("#txtCep").mask("99.999-999", {
				placeholder : "_"
			});
			jQuery("#txtTelefoneResidencial").mask("(99)9999-9999", {
				placeholder : "_"
			});
			jQuery("#txtTelefoneComercial").mask("(99)9999-9999", {
				placeholder : "_"
			});
			jQuery("#txtTelefoneCelular").mask("(99)9?9999-9999", {
				placeholder : "_"
			});
			jQuery("#txtTelefoneCelular2").mask("(99)9?9999-9999", {
				placeholder : "_"
			});
			
			jQuery("#dtEnvioCertificado").mask("99/99/9999",{placeholder:"_"});

			controlarCampos();

		});

		function formataMonetario() {
			jQuery('#txtCapital1').priceFormat({
				prefix : 'R$ ',
				centsSeparator : ',',
				thousandsSeparator : '.',
				centsLimit : 2
			});
			jQuery('#txtCapital2').priceFormat({
				prefix : 'R$ ',
				centsSeparator : ',',
				thousandsSeparator : '.',
				centsLimit : 2
			});
			return false;
		}

		function clearAgenciador() {
			jQuery('#txtAgenciador').val("");
			jQuery('#txtOrdemAgenciamento').val("");
		}

		function clearBeneficiario() {
			jQuery('#txtNomeBeneficiario').val("");
			jQuery('#txtGrauParentesco').val("");
		}

		function atualizar() {
			//jQuery('#comandoAtualizar');
			jQuery('#formulario').submit();

		}
		
		function prepararformaPagamento() {
			if (jQuery("#tipoPagamento")) {
				if (jQuery('#tipoPagamento').val() == 'DEBITO_EM_CONTA') {
					jQuery('#tableTitular').show();
					jQuery('#tableBanco').show();
					jQuery('#tableCartao').hide();
					
					
				} else if (jQuery('#tipoPagamento').val() == 'CARTAO_CREDITO') {
					jQuery('#tableTitular').show();
					jQuery('#tableBanco').hide();
					jQuery('#tableCartao').show();
					
				} else {
					jQuery('#tableTitular').hide();
					jQuery('#tableBanco').hide();
					jQuery('#tableCartao').hide();
				}
			}
		}
		
	</script>
	<DIV id=background_2>
		<DIV id=site_wrapper>
			<jsp:include page="common/menu.jsp" />
			<DIV id=wrap>
				<DIV id=content>
					<DIV id=main>
						<DIV id=main_content>
							<DIV id=main_body>

								<div id="breadcrumb">
									<span>Gest�o</span>
									<li>Proposta - Super Ap</li>
								</div>
<!-- 								<div id="icone"> -->
<!-- 									<img src="images/cadProposta.png" /> -->
<!-- 								</div> -->

								<h:form styleClass="niceform" prependId="false" id="formulario">

									<h:inputHidden value="#{propostaHandler.fluxo}"  id="fluxo"/>
									<div class="titulo_agrupamento_Dados"></div>
									
									
									<jsp:include page="superap/dadosSuperAp.jsp" />
									
									<jsp:include page="superap/seguradoPrincipal.jsp" />
									
									<jsp:include page="superap/formaPagamento.jsp" />
									
									<jsp:include page="superap/endereco.jsp" />
									
									
									
									
									

									<fieldset>
										<legend>Total dos Pr�mios</legend>
											<table>
												<tr>
													<td align="right" width="150px;"><label
														for="txtTotalPremios">Total de Pr�mio:</label></td>
													<td><h:outputText
															value="#{propostaHandler.proposta.totalPremios}"
															id="txtTotalPremios"
															style="font-size: 14px;font-weight: bold;">
															<f:convertNumber currencySymbol="R$" type="currency"
																groupingUsed="true" />
														</h:outputText></td>
												</tr>
											</table>
									</fieldset>

									



									<fieldset>
										<legend>Benefici�rios</legend>

										<h:panelGrid columns="2" id="panelGridDadosBeneficiarios">
											<h:outputLabel for="txtNomeBeneficiario"
												value="Nome do Benefici�rio:" />
											<h:inputText
												value="#{propostaHandler.beneficiario.nomeBeneficiario}"
												id="txtNomeBeneficiario" size="50" maxlength="60"
												 style="text-transform:uppercase;">

											</h:inputText>
											<h:outputLabel for="txtGrauParentesco"
												value="Grau de Parentesco:" />
											<h:inputText
												value="#{propostaHandler.beneficiario.grauParentesco.descricao}"
												id="txtGrauParentesco" size="20" maxlength="20"
												 />
											<rich:suggestionbox id="suggestionBoxGrauParentesco"
												for="txtGrauParentesco"
												suggestionAction="#{propostaHandler.grauParentescoAutoComplete}"
												var="grauParentesco"
												fetchValue="#{grauParentesco.descricao}" width="300"
												height="250" cellpadding="4"
												nothingLabel="Nenhum Item Encontrado!">
												<a4j:support event="onselect" id="a4jsupport"
													ignoreDupResponses="true">
													<f:setPropertyActionListener value="#{grauParentesco}"
														target="#{propostaHandler.beneficiario.grauParentesco}" />
												</a4j:support>
												<h:column>
													<h:outputText value="#{grauParentesco.descricao}"
														styleClass="autocompleteFonte" />
												</h:column>
											</rich:suggestionbox>
										</h:panelGrid>
										<h:panelGrid columns="3" id="panelGridPercentualBeneficiarios">
											<h:outputLabel for="cmbDivisaoBeneficiario"
												value="Percentual:" />
											<h:selectOneMenu id="cmbDivisaoBeneficiario"
												value="#{propostaHandler.beneficiario.tipoPercentual}"
												>
												<f:selectItems
													value="#{propostaHandler.tipoPercentualCombo}" />
												<p:ajax
													actionListener="#{propostaHandler.ajaxChangeTipoPercentualCombo}"
													update="panelPercentual" event="change"
													oncomplete="jQuery('#txtInformarPercentual').focus();" />
												<rich:jQuery id="enterTabInputNomeBeneficiario"
													selector="input" timing="onload"
													query="keydown(function(e){if (e.keyCode == 13){ 
			tabindex = jQuery(this).attr('tabindex');
		  tabindex++;
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('textarea[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                                    return false;} 
                                            })" />
												<rich:jQuery id="enterTabInputNomeBeneficiario2"
													selector="select" timing="onload"
													query="keydown(function(e){if (e.keyCode == 13){ 
			tabindex = jQuery(this).attr('tabindex');
		  tabindex++;
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('textarea[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                                    return false;} 
                                            })" />
											</h:selectOneMenu>
											<h:panelGroup id="panelPercentual">
												<h:inputText
													value="#{propostaHandler.beneficiario.percentual}"
													id="txtInformarPercentual" size="5" maxlength="5"
													rendered="#{propostaHandler.beneficiario.tipoPercentual eq 2}"
													>
													<rich:jQuery id="enterTabInputInformarPercentual"
														selector="input" timing="onload"
														query="keydown(function(e){if (e.keyCode == 13){ 
			tabindex = jQuery(this).attr('tabindex');
		  tabindex++;
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('textarea[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                                    return false;} 
                                            })" />
												</h:inputText>
												<h:outputText value="%"
													rendered="#{propostaHandler.beneficiario.tipoPercentual eq 2}" />
											</h:panelGroup>

										</h:panelGrid>

										<a4j:commandButton
											actionListener="#{propostaHandler.incluirBeneficiario}"
											value="Incluir Benefici�rio" id="gravarBeneficiario"
											styleClass="buttonDetalhe"
											reRender="agrupamento_Dados_Beneficiarios,menuSubView:mensagens,panelPercentual,panelGridPercentualBeneficiarios"
											
											oncomplete="jQuery('#txtNomeBeneficiario').focus();if( #{facesContext.maximumSeverity == null} ){clearBeneficiario();}" />
										<p:commandButton
											actionListener="#{propostaHandler.cancelarBeneficiario}"
											value="Limpar" id="cancelarBeneficiario"
											styleClass="buttonDetalhe" async="true"
											update="agrupamento_Dados_Beneficiarios,menuSubView:mensagens,panelGridDadosBeneficiarios,panelGridPercentualBeneficiarios"
											
											oncomplete="jQuery('#txtNomeBeneficiario').focus();">
											<p:confirmDialog message="Deseja Cancelar esta opera��o?"
												yesLabel="Sim" noLabel="N�o" header="Confirma��o!"
												fixedCenter="true" modal="true" />
										</p:commandButton>
										<rich:spacer height="30px;" />
										<h:panelGroup id="agrupamento_Dados_Beneficiarios"
											styleClass="agrupamento_Dados">

											<p:dataTable
												value="#{propostaHandler.proposta.beneficiario_collection}"
												var="beneficiario" width="50%"
												rendered="#{not empty propostaHandler.proposta.beneficiario_collection}"
												paginator="true" rows="20"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">

												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Nome do Benefici�rio" />

													</f:facet>
													<h:outputText value="#{beneficiario.nomeBeneficiario}"
														style="text-transform:uppercase;" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Parentesco" />

													</f:facet>
													<h:outputText
														value="#{beneficiario.grauParentesco.descricao}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Percentual" />

													</f:facet>
													<h:outputText value="#{beneficiario.percentual}" />
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="Alterar" />

													</f:facet>
													<p:commandLink
														actionListener="#{propostaHandler.alterarBeneficiario}"
														ajax="true"
														update="panelGridDadosBeneficiarios,panelGridPercentualBeneficiarios">
														<h:graphicImage title="Alterar" alt="Alterar"
															url="images/edit.png" style="border: none;" />
														<f:param id="editBeneficiario" name="idAlterar"
															value="#{beneficiario}" />
													</p:commandLink>
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="Excluir" />
													</f:facet>
													<h:commandLink
														actionListener="#{propostaHandler.excluirBeneficiario}"
														onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluirBeneficiario" name="idExcluir"
															value="#{beneficiario}" />
													</h:commandLink>
												</p:column>
											</p:dataTable>
										</h:panelGroup>
										<rich:spacer height="20px;" />

										<h:panelGroup id="agrupamento_Historico_Beneficiario"
											styleClass="agrupamento_Dados">
											<fieldset>
												<legend>
													<IMG title="Expandir"
														id="expandirconteudoHistoricoBeneficiarios" alt="Expandir"
														onclick="expandir('conteudoHistoricoBeneficiarios');"
														src="images/expandir.png" border=0 /><IMG
														id="retrairconteudoHistoricoBeneficiarios" title="Retrair"
														alt="Retrair"
														onclick="retrair('conteudoHistoricoBeneficiarios');"
														src="images/retrair.png" border=0 style="display: none;" />
													Hist�rico de Altera��es de Benefici�rios
												</legend>
												<div id="conteudoHistoricoBeneficiarios"
													style="display: none;">
													<p:dataTable
														value="#{propostaHandler.proposta.historicobeneficiario_collection}"
														var="historicoBeneficiario" width="50%"
														rendered="#{not empty propostaHandler.proposta.historicobeneficiario_collection}">

														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Data Altera��o" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.dataAlteracao}"
																style="text-transform:uppercase;">
																<f:convertDateTime dateStyle="default"
																	pattern="dd/MM/yyyy HH:mm" />
															</h:outputText>
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Nome Benefici�rio Antigo" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.nomeBeneficiario}" />
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Percentual Antigo" />

															</f:facet>
															<h:outputText value="#{historicoBeneficiario.percentual}" />
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Grau de Parentesco Antigo" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.grauParentesco.descricao}" />
														</p:column>
														<p:column resizable="true">
															<f:facet name="header">
																<h:outputText value="Usu�rio Altera��o" />

															</f:facet>
															<h:outputText
																value="#{historicoBeneficiario.nomeUsuario}" />
														</p:column>
													</p:dataTable>
												</div>
											</fieldset>
										</h:panelGroup>

									</fieldset>

									<fieldset>
										<legend>Agenciador</legend>
										<table>
											<tr>
												<td align="right" width="90px;"><label
													for="txtAgenciador">Agenciador:</label></td>
												<td><h:inputText
														value="#{propostaHandler.agenciador.funcionario.nome}"
														id="txtAgenciador" size="40"  /> <rich:suggestionbox
														id="suggestionBoxFuncionario" for="txtAgenciador"
														suggestionAction="#{propostaHandler.funcionarioAutoComplete}"
														var="funcionario" fetchValue="#{funcionario.nome}"
														bypassUpdates="true" width="500" height="250"
														cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
														<a4j:support event="onselect" id="a4jsupport"
															ignoreDupResponses="true">
															<f:setPropertyActionListener value="#{funcionario}"
																target="#{propostaHandler.agenciador.funcionario}" />
														</a4j:support>
														<h:column>
															<h:outputText value="#{funcionario.nome}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{funcionario.cidade.nom_cidade}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText
																value="#{funcionario.cidade.cod_estado.sgl_estado}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText
																value="#{funcionario.tipoFuncionarioDescricao}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{funcionario.percentualComissao}"
																styleClass="autocompleteFonte" />
														</h:column>
													</rich:suggestionbox></td>
											</tr>
											<tr>
												<td align="right" width="90px;"><label
													for="txtOrdemAgenciamento">Ordem do Agenciamento:</label></td>
												<td><h:inputText
														value="#{propostaHandler.agenciador.ordemAgenciamento}"
														id="txtOrdemAgenciamento" size="4"
														onkeydown="Mascara(this,Integer);"
														onkeypress="Mascara(this,Integer);"
														onkeyup="Mascara(this,Integer);"  /></td>
											</tr>
										</table>
										<table>
											<tr>
												<td align="right" width="90px;"><label
													for="cmbOpcoesAgenciador">Op��o:</label></td>
												<td><h:selectOneMenu id="cmbOpcoesAgenciador"
														value="#{propostaHandler.agenciador.tipoCalculoUtilizado}"
														>
														<f:selectItems
															value="#{propostaHandler.opcoesAgenciadorCombo}" />
														<p:ajax
															actionListener="#{propostaHandler.ajaxChangeOpcaoAgenciador}"
															update="panelComissaoAdicional" event="change"
															oncomplete="jQuery('#txtValorPagoDefinido').focus();jQuery('#txtComissaoAdicional').focus();" />
													</h:selectOneMenu></td>
												<td><h:panelGroup id="panelComissaoAdicional">
														<h:graphicImage url="images/input_left.png"
															styleClass="inputCorner"
															rendered="#{propostaHandler.agenciador.tipoCalculoUtilizado eq 'D'}" />
														<h:graphicImage url="images/input_left.png"
															styleClass="inputCorner"
															rendered="#{propostaHandler.agenciador.tipoCalculoUtilizado eq 'C'}" />

														<h:inputText
															value="#{propostaHandler.agenciador.valorPagoDefinido}"
															id="txtValorPagoDefinido" size="15" maxlength="15"
															style="width:150px;" styleClass="textinput"
															rendered="#{propostaHandler.agenciador.tipoCalculoUtilizado eq 'D'}"
															onkeydown="Mascara(this,Valor);"
															onkeypress="Mascara(this,Valor);"
															onkeyup="Mascara(this,Valor);" >
															<f:convertNumber currencySymbol="R$" type="currency"
																groupingUsed="true" />
															<rich:jQuery id="enterTabInputAgenciador1"
																selector="input" timing="onload"
																query="keydown(function(e){if (e.keyCode == 13){ 
			tabindex = jQuery(this).attr('tabindex');
		  tabindex++;
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('textarea[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                                    return false;} 
                                            })" />
														</h:inputText>
														<h:inputText
															value="#{propostaHandler.agenciador.percentual}"
															id="txtComissaoAdicional" size="5" maxlength="5"
															style="width:50px;" styleClass="textinput"
															rendered="#{propostaHandler.agenciador.tipoCalculoUtilizado eq 'C'}"
															onkeydown="Mascara(this,Integer);"
															onkeypress="Mascara(this,Integer);"
															onkeyup="Mascara(this,Integer);" >
															<rich:jQuery id="enterTabInputAgenciador2"
																selector="input" timing="onload"
																query="keydown(function(e){if (e.keyCode == 13){ 
			tabindex = jQuery(this).attr('tabindex');
		  tabindex++;
		  jQuery('input[tabindex='+tabindex+']').focus();
		  jQuery('textarea[tabindex='+tabindex+']').focus();
		  jQuery('select[tabindex='+tabindex+']').focus();
                                                    return false;} 
                                            })" />
														</h:inputText>
														<h:graphicImage url="images/input_right.png"
															styleClass="inputCorner"
															rendered="#{propostaHandler.agenciador.tipoCalculoUtilizado eq 'D'}" />
														<h:graphicImage url="images/input_right.png"
															styleClass="inputCorner"
															rendered="#{propostaHandler.agenciador.tipoCalculoUtilizado eq 'C'}" />
														<h:outputText value="%"
															rendered="#{propostaHandler.agenciador.tipoCalculoUtilizado eq 'C'}" />
													</h:panelGroup></td>
											</tr>
										</table>
										<a4j:commandButton
											actionListener="#{propostaHandler.incluirAgenciador}"
											value="Incluir Agenciador" id="gravarAgenciador"
											styleClass="buttonDetalhe"
											reRender="agrupamentoAgenciador,menuSubView:mensagens,panelComissaoAdicional"
											
											oncomplete="jQuery('#dtDataAgenciamento').focus();clearAgenciador();" />
										<p:commandButton
											actionListener="#{propostaHandler.cancelarAgenciador}"
											value="Limpar" id="cancelarAgenciador"
											styleClass="buttonDetalhe" async="false" >
											<p:confirmDialog message="Deseja Cancelar esta opera��o?"
												yesLabel="Sim" noLabel="N�o" header="Confirma��o!"
												fixedCenter="true" modal="true" />
										</p:commandButton>
										<rich:spacer height="30px;" />
										<h:panelGroup id="agrupamentoAgenciador"
											styleClass="agrupamento_Dados">

											<p:dataTable
												value="#{propostaHandler.proposta.propostaagenciador_collection}"
												var="agenciador" width="50%"
												rendered="#{not empty propostaHandler.proposta.propostaagenciador_collection}"
												paginator="true" rows="10"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">

												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Nome do Agenciador" />

													</f:facet>
													<h:outputText value="#{agenciador.funcionario.nome}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Percentual" />

													</f:facet>
													<h:outputText
														value="#{agenciador.funcionario.percentualComissao}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Comiss�o Adicional" />

													</f:facet>
													<h:outputText value="#{agenciador.percentual}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Valor Definido" />

													</f:facet>
													<h:outputText value="#{agenciador.valorPagoDefinido}">
														<f:convertNumber currencySymbol="R$" type="currency"
															groupingUsed="true" />
													</h:outputText>
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="Alterar" />

													</f:facet>
													<h:commandLink
														actionListener="#{propostaHandler.alterarAgenciador}">
														<h:graphicImage title="Alterar" alt="Alterar"
															url="images/edit.png" style="border: none;" />
														<f:param id="editAgenciador" name="idAlterar"
															value="#{agenciador}" />
													</h:commandLink>
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="Excluir" />
													</f:facet>
													<h:commandLink
														actionListener="#{propostaHandler.excluirAgenciador}"
														onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluirAgenciador" name="idExcluir"
															value="#{agenciador}" />
													</h:commandLink>
												</p:column>
											</p:dataTable>
										</h:panelGroup>
									</fieldset>
									<fieldset>
										<legend>Aprova��o</legend>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="txtTipoComissao">Comando Comiss�o:</label>
												</td>
												<td><h:selectOneMenu id="cmbTipoComissao"
														value="#{propostaHandler.proposta.comandoComissao}"
														>
														<f:selectItems
															value="#{propostaHandler.opcoesTipoComissao}" />
													</h:selectOneMenu></td>
											</tr>
											<tr>
												<td align="right" width="150px;"><label
													for="dtDataAgenciamento">Data Agenciamento:</label></td>
												<td><h:inputText
														value="#{propostaHandler.proposta.dataAgenciamento}"
														id="dtDataAgenciamento" maxlength="10" >
														<f:convertDateTime dateStyle="default"
															pattern="dd/MM/yyyy" />
													</h:inputText></td>
											</tr>
											<tr>
												<td align="right" width="150px;"><label
													for="dtDataAprovacao">Data Aprova��o:</label></td>
												<td><h:inputText
														value="#{propostaHandler.proposta.dataAprovacao}"
														id="dtDataAprovacao" maxlength="10" >
														<f:convertDateTime dateStyle="default"
															pattern="dd/MM/yyyy" />
													</h:inputText></td>
											</tr>
											<tr>
												<td align="right" width="150px;"><label
													for="dtDataCadastro">Data do Cadastro:</label></td>
												<td><h:inputText
														value="#{propostaHandler.proposta.dataCadastro}"
														id="dtDataCadastro" maxlength="10" >
														<f:convertDateTime dateStyle="default"
															pattern="dd/MM/yyyy" />
													</h:inputText></td>
											</tr>
										</table>
									</fieldset>
									<fieldset>
										<legend>Informa��es</legend>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="txtNumPropostaReadOnly">N�mero da Proposta:</label></td>
												<td><h:outputText
														value="#{propostaHandler.proposta.id}"
														id="txtNumPropostaReadOnly" /></td>
											</tr>
										</table>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="txtNumeroSorteioA">N�meros do Sorteio :</label></td>
												<td><h:inputText
														value="#{propostaHandler.numeroSorteio}"
														id="txtNumeroSorteio" size="10" maxlength="5"
														onkeydown="Mascara(this,Integer);"
														onkeypress="Mascara(this,Integer);"
														onkeyup="Mascara(this,Integer);"  /></td>
											</tr>
										</table>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="txtUltimaAlteracao">Data �ltima Altera��o:</label></td>
												<td><h:outputText
														value="#{propostaHandler.proposta.dataAlteracao}"
														id="txtUltimaAlteracao">
														<f:convertDateTime dateStyle="default"
															pattern="dd/MM/yyyy" />
													</h:outputText></td>
											</tr>
											<tr>
												<td align="right" width="150px;"><label
													for="txtUsuarioUltimaAlteracao">Usu�rio �ltima
														Altera��o:</label></td>
												<td><h:outputText
														value="#{propostaHandler.proposta.nomeUsuarioAlteracao}"
														id="txtUsuarioUltimaAlteracao" /></td>
											</tr>
										</table>
									</fieldset>
									<fieldset>
										<legend>Sorteio</legend>
										Segurado Contemplado com Sorteio em:
										<table>
											<tr>
												<td><h:inputText
														value="#{propostaHandler.proposta.dataContemplacao}"
														id="dtContemplacaoSorteio" maxlength="10" >
														<f:convertDateTime dateStyle="default"
															pattern="dd/MM/yyyy" />
													</h:inputText></td>
											</tr>
										</table>
									</fieldset>
									<fieldset>
										<legend>Cancelamento</legend>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="dtDataCancelamento">Data Cancelamento:</label></td>
												<td><h:inputText
														value="#{propostaHandler.proposta.dataCancelamento}"
														id="dtDataCancelamento" maxlength="10" >
														<f:convertDateTime dateStyle="default"
															pattern="dd/MM/yyyy" />
													</h:inputText></td>
											</tr>
										</table>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="txtMotivoCancelamento">Motivo do Cancelamento:</label>
												</td>
												<td><h:selectOneMenu id="cmbMotivoCancelamento"
														value="#{propostaHandler.proposta.motivoCancelamento.id}"
														>
														<f:selectItems
															value="#{propostaHandler.opcoesMotivoCancelamento}" />
													</h:selectOneMenu></td>
											</tr>
										</table>
										
										<h:panelGroup id="agrupamento_Historico_Movimentacoes"
										styleClass="agrupamento_Dados">
										<fieldset>
											<legend>
												<IMG title="Expandir" id="expandirconteudoHistoricoMovimentacoes"
													alt="Expandir"
													onclick="expandir('conteudoHistoricoPagamentos');"
													src="images/expandir.png" border=0 /> <IMG
													id="retrairconteudoHistoricoPagamentos" title="Retrair"
													alt="Retrair"
													onclick="retrair('conteudoHistoricoPagamentos');"
													src="images/retrair.png" border=0 style="display: none;" />
												Hist�rico de Inadimpl�ncias
											</legend>
										<div id="conteudoHistoricoPagamentos"
												style="display: none; width: 400px;">
												<p:dataTable
													value="#{propostaHandler.historicoMovimentacoes}"
													var="item" width="50%" style="width: 300px;"
													rendered="#{not empty propostaHandler.historicoMovimentacoes}">

													<p:column resizable="true">
														<f:facet name="header">
															<h:outputText value="Referencia" />

														</f:facet>
														<h:outputText value="#{item.periodo}"
															style="text-transform:uppercase;">
														</h:outputText>
													</p:column>
													<p:column resizable="true">
														<f:facet name="header">
															<h:outputText value="Valor" />

														</f:facet>
														<h:outputText
															value="#{item.valor}" />
													</p:column>
													<p:column resizable="true">
														<f:facet name="header">
															<h:outputText value="Pago?" />

														</f:facet>
														<h:outputText value="#{item.pago}" />
													</p:column>
													

													<p:column resizable="true">
														<f:facet name="header">
															<h:outputText value="Pago em" />

														</f:facet>
														<h:outputText
															value="#{item.quitacao}" />
													</p:column>

													
												</p:dataTable>
											</div>
											</fieldset>
											</h:panelGroup>
										
									</fieldset>
									<fieldset>
										<legend>Exclus�o</legend>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="dtDataExclusao">Data Exclusao:</label></td>
												<td><h:outputText
														value="#{propostaHandler.proposta.dataExclusao}"
														id="dtDataExclusao">
														<f:convertDateTime dateStyle="default"
															pattern="dd/MM/yyyy" />
													</h:outputText></td>
											</tr>
										</table>
									</fieldset>



									<br />
									<p:commandButton actionListener="#{propostaHandler.gravar}"
										value="Salvar" id="gravar" styleClass="button" async="false"
										ajax="false"  />
									<p:commandButton
										actionListener="#{propostaHandler.cancelarFiltro}"
										value="Cancelar" id="cancelar" styleClass="button"
										async="false" ajax="false" >
										<p:confirmDialog message="Deseja Cancelar esta opera��o?"
											yesLabel="Sim" noLabel="N�o" header="Confirma��o!"
											fixedCenter="true" modal="true" />
									</p:commandButton>

									<h:commandLink id="comandoAtualizar"
										actionListener="#{propostaHandler.atualizar}"
										onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
									</h:commandLink>
								</h:form>

							</DIV>
						</DIV>
						<DIV class="both">
							<!-- -->
						</DIV>
					</DIV>
					<DIV id="footer_top_bg">
						<!-- -->
					</DIV>
				</DIV>
			</DIV>
			<jsp:include page="common/footer.jsp" />
		</DIV>
		<DIV id="foo_glass"></DIV>
		<BR> <BR>
	</DIV>
</BODY>

	</HTML>
</f:view>
