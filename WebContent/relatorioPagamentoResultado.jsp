<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="pagamentoHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class="popUp" style="width: 50%">
<jsp:include page="common/relatorioHeader.jsp" />


<h:form prependId="false">
<input type="hidden" id="dados_a_enviar" name="dados_a_enviar" />
			<table class="cabecalho_relatorio" >
				<tr>
					<td><IMG title="Slogan" alt="Slogan" src="images/logoRelatorio.png" border=0 /></td>
					<td width="200px;">Relat�rio de Pagamentos</td>
					<td>Data:</td>
					<td width="500px;"> <h:outputText value="#{pagamentoHandler.dataAtualRelatorio}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" locale="pt_BR" /></h:outputText>
					<td class="invisivelImpressao">
						<h:commandLink onclick="javascript:window.print();">  
						    <p:graphicImage value="images/impressora.png" alt="Imprimir" title="Imprimir"/>  
						</h:commandLink>
					</td>
					<td class="invisivelImpressao">
						<h:commandLink action="#{pagamentoHandler.exportarExcel}" >  
						    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
						</h:commandLink>
					</td>
				<tr>
			</table>

 			<rich:dataTable 
                    onRowMouseOver="this.style.backgroundColor='#F1F1F1'"
                    onRowMouseOut="this.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'"
                    cellpadding="0" cellspacing="0" 
                    width="700" border="0" var="chave" value="#{pagamentoHandler.mapKeys}" sortMode="single" >
                
                        <f:facet name="header">
                        <rich:columnGroup>
                            <rich:column rowspan="2" >
                                <h:outputText value="Data:" />
                            </rich:column>
                            <rich:column colspan="6">
                                <h:outputText value="Dados do Documento" />
                            </rich:column>
                            <rich:column breakBefore="true">
                                <h:outputText value="N�mero" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Conta" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Tipo do Documento" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Tipo de Despesa" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Valor Despesa" />
                            </rich:column>
                        </rich:columnGroup>
                    </f:facet>

                    <rich:column colspan="7">
                    	<h:outputText value="#{chave}" style="FONT: bold 11px  Arial, Helvetica, sans-serif;"/>
                    </rich:column>
                    
                    <rich:subTable 
                        onRowMouseOver="this.style.backgroundColor='#F8F8F8'"
                        onRowMouseOut="this.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'"
                        var="detalhe" value="#{pagamentoHandler.mapaRelatorioFinanceiro[chave][0]}">
                        <rich:column >
                            <h:outputText value="#{detalhe.pagamento.dataDocumento}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.pagamento.numeroDocumento}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.pagamento.conta.descricao}"/>
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.pagamento.tipoDocumento.descricao}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.despesa.nomeDespesa}" />
                            <f:facet name="footer">
                                <rich:spacer />
                            </f:facet>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{detalhe.valor}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            <f:facet name="footer">
                               <h:outputText value="#{pagamentoHandler.mapaRelatorioFinanceiro[chave][1]}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </f:facet>
                        </rich:column>
                        
                    </rich:subTable>
                  
					<f:facet name="footer">
                
                        <rich:columnGroup>
                            <rich:column colspan="5">Total:</rich:column>
                            <rich:column>
                                 <h:outputText value="#{pagamentoHandler.totalRelatorio}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
                            </rich:column>
                        </rich:columnGroup>
                    </f:facet>
        
                </rich:dataTable>


</h:form>
</BODY>
</HTML>
</f:view>
