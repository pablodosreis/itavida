<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="faturamentoHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>

<script type="text/javascript">
jQuery(function(jQuery){
	jQuery("#dtPeriodoPagamentoInicio").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoPagamentoFim").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoVigenciaInicio").mask("99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoVigenciaFim").mask("99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoCobrancaInicio").mask("99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoCobrancaFim").mask("99/9999",{placeholder:"_"});
	});

</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rio</span><li>Relat�rio  de Faturamento (Filtro)</li> </div>
						<div id="icone">
                           <img src="images/faturamento.png" />
                        </div>

						<h:form styleClass="niceform" prependId="false">
							<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao"><h:outputText id="orgaoLabel" value="�rg�o:"/></label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamentoConsultar.orgao.nomeOrgao}" id="txtOrgao" size="40" tabindex="1">
												<p:ajax event="keyup" update="renderizado" >
													<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{faturamentoHandler.limparOrgao}" 
														target="#{faturamentoHandler.planilhaFaturamentoConsultar.orgao}"/> 
												</p:ajax>
											</h:inputText>
											<rich:suggestionbox id="suggestionOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{faturamentoHandler.orgaoAutoComplete}" 
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{faturamentoHandler.planilhaFaturamentoConsultar.orgao}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Org�o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.nomeOrgao}"/>									  										
													</h:column>
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Sigla" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.siglaOrgao}"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
								</table> 
								<table>
									<tr>
										<td align="right" width="150px;"> 
										   <label for="dtPeriodoVigenciaInicio">Per�odo Vig�ncia (In�cio):</label> 
										</td> 
										<td>
											<h:inputText value="#{faturamentoHandler.planilhaFaturamentoConsultar.mesAnoVigenciaInicio}"  id="dtPeriodoVigenciaInicio" maxlength="7"  tabindex="2">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td> 
										<td align="right" width="150px;"> 
										   <label for="dtPeriodoVigenciaFim">(Fim):</label> 
										</td> 
										<td>
											<h:inputText value="#{faturamentoHandler.planilhaFaturamentoConsultar.mesAnoVigenciaFim}"  id="dtPeriodoVigenciaFim" maxlength="7"  tabindex="3">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td> 
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;"> 
										   <label for="dtPeriodoCobrancaInicio">Per�odo Cobran�a (In�cio):</label> 
										</td> 
										<td>
											<h:inputText value="#{faturamentoHandler.planilhaFaturamentoConsultar.mesAnoCobrancaInicio}"  id="dtPeriodoCobrancaInicio" maxlength="7"  tabindex="4">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td> 
										<td align="right" width="150px;"> 
										   <label for="dtPeriodoCobrancaFim">(Fim):</label> 
										</td> 
										<td>
											<h:inputText value="#{faturamentoHandler.planilhaFaturamentoConsultar.mesAnoCobrancaFim}"  id="dtPeriodoCobrancaFim" maxlength="7"  tabindex="5">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td> 
									</tr>
								</table>
								<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="dtPeriodoPagamentoInicio" >Per�odo Pagamento (In�cio):</label>
											</td>
											<td>
												<h:inputText value="#{faturamentoHandler.planilhaFaturamentoConsultar.dataPagamentoInicio}"  id="dtPeriodoPagamentoInicio" maxlength="10"  tabindex="6">
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:inputText>
											</td>
											<td align="right" width="150px;">
											  <label for="dtPeriodoPagamentoFim" >(Fim):</label>
											</td>
											<td>
												<h:inputText value="#{faturamentoHandler.planilhaFaturamentoConsultar.dataPagamentoFim}"  id="dtPeriodoPagamentoFim" maxlength="10"  tabindex="7">
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:inputText>
											</td>
										</tr>	
									</table>
								<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtPorEstado" >Por Estado:</label>
											</td>
											<td>
											   <h:selectOneMenu id="txtPorEstado" value="#{faturamentoHandler.estadoSelecionado}" tabindex="3" >
											   	<f:selectItems value="#{faturamentoHandler.estadosCombo}" />
											   </h:selectOneMenu>
											</td>
										</tr>	
									</table>
							</fieldset>								
							<br />
							
							<h:commandButton action="#{faturamentoHandler.relatorioFaturamentoPesquisar}" value="Gerar Relat�rio" id="pesquisar" styleClass="button" tabindex="8"/>
							<p:commandButton actionListener="#{faturamentoHandler.cancelarConsulta}" value="Cancelar" id="cancelar" styleClass="button" async="false" tabindex="9">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
