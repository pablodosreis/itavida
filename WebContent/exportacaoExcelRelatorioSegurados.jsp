<%@ page language="java" contentType="application/vnd.ms-excel;charset=iso-8859-1" %>
<%@ taglib prefix="f"  uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h"  uri="http://java.sun.com/jsf/html"%>
<%@ taglib uri="http://primefaces.prime.com.tr/ui" prefix="p" %> 
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="propostaHandler" />
<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<HEAD>

<TITLE>SISVIDA WEB 1.0</TITLE>
<META http-equiv=Content-Type content="application/vnd.ms-excel; charset=iso-8859-1" >
<!--[if lte IE 6]>

<SCRIPT src="js/pngfix.js" defer type=text/javascript></SCRIPT>
<![endif]-->


</HEAD>
<BODY class="popUp">

<h:form prependId="false">
 			<rich:dataTable 
                    onRowMouseOver="this.style.backgroundColor='#F1F1F1'"
                    onRowMouseOut="this.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'"
                    cellpadding="0" cellspacing="0" 
                    width="700" border="1" var="item" value="#{propostaHandler.resultadoPesquisa}" sortMode="single" >

 <f:facet name="header">
                        <rich:columnGroup>

                            <rich:column >
                                <h:outputText value="Nome Segurado Principal" />
                            </rich:column>
                            <rich:column >
                                <h:outputText value="�rg�o" />
                            </rich:column>
		                    <rich:column >
                                <h:outputText value="Plano" />
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Data Nascimento" />
                            </rich:column>
		                    <rich:column >
                                <h:outputText value="CPF Segurado" />
                            </rich:column>
                            <rich:column rendered="#{propostaHandler.tipoRelatorio eq 5}">
                                 <h:outputText value="Agenciador" /> 
                            </rich:column>
                      		<rich:column rendered="#{propostaHandler.tipoRelatorio eq 5}">
                                 <h:outputText value="% Comiss�o" />  
                            </rich:column>
                            <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}" >
                                <h:outputText value="Idade Atual" />
                            </rich:column>
                            <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}">
                                <h:outputText value="Idade Prevista (30 dias)" /> 
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Cidade" /> 
                            </rich:column>
                            <rich:column>
                                <h:outputText value="UF" /> 
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Capital" /> 
                            </rich:column>
                            <rich:column>
                                <h:outputText value="Total Pr�mios Atual" /> 
                            </rich:column>
                            <rich:column rendered="#{propostaHandler.tipoRelatorio eq 5}">
                                <h:outputText value="Valor Comiss�o" />
                            </rich:column>
                             <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}">
                                <h:outputText value="Novo Total Pr�mios" />
                            </rich:column>
                             <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}">
                               <h:outputText value="Diferen�a" /> 
                            </rich:column>
                             <rich:column rendered="#{propostaHandler.tipoRelatorio eq 1}">
                               <h:outputText value="Data Cancelamento" /> 
                            </rich:column>
                            <rich:column rendered="#{propostaHandler.tipoRelatorio eq 1}">
                                 <h:outputText value="Motivo Cancelamento" />
                            </rich:column>
                            <rich:column  >
                                <h:outputText value="Proposta" />
                            </rich:column>

                        </rich:columnGroup>
 </f:facet>
                        <rich:column>
                           <h:outputText value="#{item.nomeSegurado}"/>	
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.orgao.nomeOrgao}"/>	
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.tipoProposta.descricao}"/>	
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.dataNascimentoSegurado}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" /></h:outputText>
                        </rich:column>
                        <rich:column>
                           <h:outputText value="#{item.cpfSegurado}"/>	
                        </rich:column>
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 5}">
                            <h:outputText value="#{item.propostaagenciador_collection[0].funcionario.nome}" />	
                        </rich:column>
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 5}">
                            <h:outputText value="#{item.propostaagenciador_collection[0].funcionario.percentualComissao}" />
                        </rich:column>
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}">
                            <h:outputText value="#{item.idadeRealSegurado}"></h:outputText>
                        </rich:column>

                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}">
                          <h:outputText value="#{item.idadePrevistaSegurado}"></h:outputText>
                        </rich:column>
                        <rich:column>
                            <h:outputText value="#{item.cidade.nom_cidade}"/>
                        </rich:column>
                        <rich:column>
                           <h:outputText value="#{item.cidade.cod_estado.sgl_estado}"/>
                        </rich:column>											
                        <rich:column>
                           <h:outputText value="#{item.capitalSegurado}" ><f:convertNumber type="number" groupingUsed="true" /></h:outputText>
                        </rich:column>	
                        <rich:column>
                           <h:outputText value="#{item.totalPremios}" ><f:convertNumber type="number" groupingUsed="true" /></h:outputText>
                        </rich:column>	
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 5}">
                            <h:outputText value="#{ (item.propostaagenciador_collection[0].funcionario.percentualComissao * item.totalPremios)/100 - (item.orgao.valorProlabore + item.orgao.valorTaxa ) }" />
                        </rich:column>
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}">
                          <h:outputText value="#{item.novoValorFaixaEtaria}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
                        </rich:column>
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 3}">
                          <h:outputText value="#{item.diferencaValores}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
                        </rich:column>																			
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 1}">
                          <h:outputText value="#{item.dataCancelamento}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" /></h:outputText>
                        </rich:column>	
                        <rich:column rendered="#{propostaHandler.tipoRelatorio eq 1}">
                          <h:outputText value="#{item.motivoCancelamento.descricao}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  /></h:outputText>
                        </rich:column>	
						<rich:column >
                            <h:outputText value="#{item.id}" />
                        </rich:column>
                        
</rich:dataTable>


<h:outputText value="Total de Registros: " style="FONT: bold 14px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #000;" /><h:outputText value="#{propostaHandler.totalRegistrosRelatorio}" />
<br/>
<h:outputText value="Total de Pr�mios: " style="FONT: bold 14px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #000;" /> <h:outputText value="#{propostaHandler.valorTotalRegistrosRelatorio}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>

</h:form>
</BODY>
</HTML>
</f:view>