<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="movPropHandler" />


<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery("#periodo").mask("99/9999",{placeholder:"_"});	
	jQuery("#periodoQuitacaoAnterior").mask("99/9999",{placeholder:"_"});
	
	if (jQuery('#elementoAvancou').val() == 'true') {
		jQuery('#divInadimplente').show();
		jQuery('#divQuitacoesAnteriores').show();
	} else {
		jQuery('#divInadimplente').hide();
		jQuery('#divQuitacoesAnteriores').hide();
	}
	
	if (jQuery('#elementoConfimarInadimplentesInc').val() == 'true') {
		dlg2.show();
	}
	
});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Movimenta��o</span><li>Movimenta��o Proposta</li> </div>
						<div id="icone">
                           <img src="images/cadOrgaos.png" />
                        </div>
                          

				<h:form styleClass="niceform" prependId="false">
				
					<p:dialog header="Confirma��o" widgetVar="dlg2" modal="true" height="100">
    						<h:inputHidden value="#{movPropHandler.periodo}"  id="periodoHidden"/>
    						<h:outputText value="Existem propostas Fora de Folha, deseja adicion�-las � movimenta��o como inadimplentes?" />
    					    <br/>
    					    <h:outputText value="Per�odo: #{movPropHandler.periodo}" />
    					    <br/>
    					    <h:outputText value="Propostas: #{movPropHandler.inadimplentesAInserir}" />
    						<br/>
    						<br/>
    						<p:commandButton actionListener="#{movPropHandler.avancarAddInadimplentes}" value="Sim" id="avancarAddInadimplentes" styleClass="button" async="false" ajax="false" />
    						<p:commandButton actionListener="#{movPropHandler.avancarSimples}"  value="N�o" id="avancarSimples" styleClass="button" async="false" ajax="false" />
					</p:dialog>
				
					<h:inputHidden value="#{movPropHandler.avancou}"  id="elementoAvancou"/>
					<h:inputHidden value="#{movPropHandler.confimarInadimplentesInc}"  id="elementoConfimarInadimplentesInc"/>
					
					<p:commandButton actionListener="#{movPropHandler.avancarConfirmado}" rendered="#{!movPropHandler.confimarInadimplentesInc}" value="avancarConfirmado" id="avancarConfirmado" styleClass="button" async="false" ajax="false" />
					
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
			
									<div class="divRecuadoEsquerdo">
										<table>
											<tr>
												<td align="left" width="20px;">
												  <label for="periodo">Per�odo:</label> 
												</td>
												<td>
													<h:inputText value="#{movPropHandler.periodo}"  id="periodo" maxlength="10" size="12" rendered="#{!movPropHandler.avancou}" />
													<h:outputText value="#{movPropHandler.periodo}"  id="periodoOutput" rendered="#{movPropHandler.avancou}"/>
												</td>												
											</tr>
											<tr>
												<td></td>
												<td>
													<p:commandButton actionListener="#{movPropHandler.avancar}" rendered="#{!movPropHandler.avancou}" value="Avan�ar" id="avancar" styleClass="button" async="false" ajax="false" />
												</td>
											</tr>											
										</table>
									</div>
									<br />						
									<div class="divRecuadoEsquerdo" id="divInadimplente" style="display: none; float: none">
									
									<fieldset style="float: left">
									
										<legend>Movimentacao Proposta Inadimplente</legend>
										<table>
											<tr>
												<td align="right" width="90px;"><label
													for="txtSegurado">Segurado:</label></td>
												<td><h:inputText
														value="#{movPropHandler.proposta.nomeSegurado}"
														id="txtNomeSegurado" size="40"  /> <rich:suggestionbox
														id="suggestionBoxNomeSegurado" for="txtNomeSegurado"
														suggestionAction="#{movPropHandler.SeguaradoAutoComplete}"
														var="proposta" fetchValue="#{proposta.nomeSegurado}"
														bypassUpdates="true" width="500" height="250"
														cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
														<a4j:support event="onselect" id="a4jsupport"
															ignoreDupResponses="true">
															<f:setPropertyActionListener value="#{proposta}"
																target="#{movPropHandler.proposta}" />
														</a4j:support>
														<h:column>
															<h:outputText value="#{proposta.nomeSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{proposta.id}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText
																value="#{proposta.cpfSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>														
													</rich:suggestionbox></td>
											</tr>
											<tr>
												<td></td>
												<td><p:commandButton actionListener="#{movPropHandler.incluirInadimplente}" value="Incluir" id="gravar" styleClass="button" async="false" ajax="false" /></td>
											</tr>											
										</table>
										
										<h:panelGroup id="agrupamentoAgenciador"
											styleClass="agrupamento_Dados">

											<p:dataTable
												value="#{movPropHandler.movimentacoes}"
												var="item" width="50%"
												rendered="#{not empty movPropHandler.movimentacoes}"
												paginator="true" rows="10"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">

												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Proposta" />
													</f:facet>
													<h:outputText value="#{item.proposta.id}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Nome" />

													</f:facet>
													<h:outputText
														value="#{item.proposta.nomeSegurado}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="�rg�o" />

													</f:facet>
													<h:outputText value="#{item.proposta.orgao.nomeOrgao}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="CPF" />

													</f:facet>
													<h:outputText value="#{item.proposta.cpfSegurado}">
														<f:convertNumber currencySymbol="R$" type="currency"
															groupingUsed="true" />
															
													</h:outputText>
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="Premio" />

													</f:facet>
													<h:outputText value="#{item.premio}">
														<f:convertNumber currencySymbol="R$" type="currency"
															groupingUsed="true" />
													</h:outputText>
												</p:column>												
												<p:column>
													<f:facet name="header">
														<h:outputText value="Excluir" />
													</f:facet>
													<h:commandLink
														actionListener="#{movPropHandler.excluirInadimplente}"
														onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluirInadimplente" name="excluirInadimplente"
															value="#{item}" />
													</h:commandLink>
												</p:column>
											</p:dataTable>
										</h:panelGroup>
										

									</fieldset>
									
									</div>
									<!--  Fim Propostas inadimpmentes -->
									
									
									
									
									<!--  Propostas com quita��es anteriores -->
									<div class="divRecuadoEsquerdo" id="divQuitacoesAnteriores" style="display: none; float: none">
									
									<fieldset style="float: left">
									
										<legend>Propostas com quita��es de meses anteriores</legend>
										<table>
											<tr>
												<td align="right" width="90px;"><label
													for="txtSeguradoQA">Segurado:</label></td>
												<td><h:inputText
														value="#{movPropHandler.propostaAnterior.nomeSegurado}"
														id="txtNomeSeguradoQA" size="40"  /> <rich:suggestionbox
														id="suggestionBoxNomeSeguradoQA" for="txtNomeSeguradoQA"
														suggestionAction="#{movPropHandler.SeguaradoAutoComplete}"
														var="propostaAnterior" fetchValue="#{propostaAnterior.nomeSegurado}"
														bypassUpdates="true" width="500" height="250"
														cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
														<a4j:support event="onselect" id="a4jsupport"
															ignoreDupResponses="true">
															<f:setPropertyActionListener value="#{propostaAnterior}"
																target="#{movPropHandler.propostaAnterior}" />
														</a4j:support>
														<h:column>
															<h:outputText value="#{propostaAnterior.nomeSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{propostaAnterior.id}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText
																value="#{propostaAnterior.cpfSegurado}"
																styleClass="autocompleteFonte" />
														</h:column>														
													</rich:suggestionbox></td>
											</tr>	
											<tr>
												<td align="right" width="90px;"><label for="periodoQuitacaoAnterior">M�s/Ano:</label></td>
												<td><h:inputText value="#{movPropHandler.periodoQuitacaoAnterior}"  id="periodoQuitacaoAnterior" maxlength="10" size="12" /></td>
											</tr>										
											<tr>
												<td></td>
												<td><p:commandButton actionListener="#{movPropHandler.incluirQuitacaoAnterior}" value="Incluir" id="gravarQA" styleClass="button" async="false" ajax="false" /></td>
											<tr/>												
										</table>
										
										<h:panelGroup id="agrupamentoAgenciadorQA"
											styleClass="agrupamento_Dados">

											<p:dataTable
												value="#{movPropHandler.movimentacoesPropostasAtrasadas}"
												var="item" width="50%"
												rendered="#{not empty movPropHandler.movimentacoesPropostasAtrasadas}"
												paginator="true" rows="10"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">

												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Proposta" />

													</f:facet>
													<h:outputText value="#{item.proposta.id}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Nome" />

													</f:facet>
													<h:outputText
														value="#{item.proposta.nomeSegurado}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="�rg�o" />

													</f:facet>
													<h:outputText value="#{item.proposta.orgao.nomeOrgao}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="CPF" />

													</f:facet>
													<h:outputText value="#{item.proposta.cpfSegurado}"/>
												</p:column>
												<p:column resizable="true">													
													<f:facet name="header">
														<h:outputText value="Premio" />
													</f:facet>
													<h:outputText value="#{item.premio}">
														<f:convertNumber currencySymbol="R$" type="currency"
															groupingUsed="true" />
													</h:outputText>
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="M�s/Ano" />
													</f:facet>
													<h:outputText value="#{item.periodoQuitacaoFormatado}"/>
												</p:column>												
												<p:column>
													<f:facet name="header">
														<h:outputText value="Excluir" />
													</f:facet>
													<h:commandLink
														actionListener="#{movPropHandler.excluirQuitacaoAnterior}"
														onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluirQA" name="excluirQA"
															value="#{item}" />
													</h:commandLink>
												</p:column>
											</p:dataTable>
										</h:panelGroup>
										

									</fieldset>
									
									</div>
									<!-- Fim das Propostas com quita��es anteriores -->
								
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
