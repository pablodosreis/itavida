<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="siapeHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
	jQuery("#dtPeriodoReferencia").mask("99/9999",{placeholder:"_"});
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Gest�o Siape (D8) </span><li>Cadastro de �rg�o SIAPE </li></div>
						<div id="icone_tokio">
                           <img src="images/siape.png" />
                        </div>
                          

						<h:form  styleClass="niceform" prependId="false">

						<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right" > 
										   <label for="txtCodigoSiape">C�digo Siape:</label> 
										</td> 
										<td>
											<h:inputText value="#{siapeHandler.orgaoSiape.id}"  id="txtCodigoSiape"  maxlength="5" size="5" tabindex="1"/>
										</td>								
									</tr>
									<tr>
										<td align="right">
										  <label for="txtNomeOrgaoSiape">Nome Org�o Siape:</label>
										</td>
										<td>
										 	<h:inputText value="#{siapeHandler.orgaoSiape.nomeOrgaoSiape}" id="txtNomeOrgaoSiape" size="40" maxlength="50" tabindex="2"/>
										</td>
									</tr>

								</table>					
							</fieldset>							

							<br />
							<h:commandButton actionListener="#{siapeHandler.gravarOrgaoSiape}" value="Gravar" id="gravar" styleClass="button" tabindex="3" />
							<p:commandButton actionListener="#{siapeHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>						
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
