<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<t:saveState value="#{filialHandler.resultadoPesquisa}" />
<t:saveState value="#{filialHandler.filial}" />
<t:saveState value="#{filialHandler.filial.cidade}" />
<t:saveState value="#{filialHandler.listaCidades}" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<HEAD>

<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><!--[if lte IE 6]>

<SCRIPT src="js/pngfix.js" defer type=text/javascript></SCRIPT>
<![endif]-->
<LINK media=all href="css/template_css.css" type=text/css rel=stylesheet>
<LINK media=all href="css/tables-styles.css" type=text/css rel=stylesheet>
<SCRIPT src=js/mootools_release-1.11.js" type=text/javascript></SCRIPT>
<script language="javascript" type="text/javascript" src="js/itavida.js"></script>

</HEAD>

<BODY class=f-default>
 <p:resources/>  
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Filiais</li> </div> 
						<div id="icone"> 
                           <img src="images/cadFiliais.png" /> 
                        </div> 

				<h:form prependId="false">

<p:dialog id="dialog1" header="Selecine uma Cidade" width="400px" widgetVar="dlg" visible="#{ not empty filialHandler.listaCidades}"  close="true" fixedCenter="true">  
			<p:dataTable value="#{filialHandler.listaCidades}" 
												 var="item" width="50%" 
												 rendered="#{not empty filialHandler.listaCidades}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Nome da Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nom_cidade}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cod_estado.sgl_estado}"/>									  										
										</p:column>		
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Selecionar" /> 
										  </f:facet>	
										  <h:commandLink  id="comandLnk"  actionListener="#{filialHandler.selecionarCidade}"  >
												<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
												<f:param id="cidadeSelecionada" name="id" value="#{item}"/>
										  </h:commandLink>	
										 						  										
										</p:column>	
			</p:dataTable>          
</p:dialog> 

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
									<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeFilial">Nome Filial:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.nomeFilial}" id="txtNomeFilial" size="40" maxlength="50" />
										</td> 
									</tr> 
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
									<tr> 
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtEndereco">Endere�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.endereco}" id="txtEndereco" size="60" maxlength="110" />
										</td>								
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtBairro">Bairro:</label> 
										</td> 
										<td> 
											<h:inputText value="#{filialHandler.filial.bairro}" id="txtBairro" size="40" maxlength="50" />
										</td>								
									</tr>										
								</table>	
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtNomeCidade">Cidade:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{filialHandler.filial.cidade.nom_cidade}" id="txtNomeCidade" size="30" />
										   <h:inputText value="#{filialHandler.filial.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" onkeyup="" />
											
											<f:param id="idCidade" value="#{filialHandler.filial.cidade.id}"/> 
										</td> 
										<td> 	
											<h:commandLink id="lnk" actionListener="#{filialHandler.pesquisaCidadesPorNome}">								
												<img src="images/popupImg.png" /> 
											</h:commandLink >
										</td> 
									</tr> 
								</table>  
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtCep">CEP:</label> 
										</td> 
										<td> 
											<h:inputText value="#{filialHandler.filial.cep}"  id="txtCep" size="10" maxlength="10" />											
										</td> 
									</tr> 
									<tr> 
										<td> 
											<br> 
										</td> 
									</tr> 
									<tr> 
									</tr> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtTelefone">Telefone:</label> 
										</td> 
										<td> 
									    <h:inputText value="#{filialHandler.filial.telefone}"  id="txtTelefone" size="15" maxlength="15" />	
										</td>	
										<td align="right" width="90px;"> 
										  <label for="txtFax">Fax:</label> 
										</td> 
										<td> 
											<h:inputText value="#{filialHandler.filial.fax}"  id="txtFax" size="15" maxlength="15" />
										</td>											
									</tr> 
								</table> 
								<table> 
									<tr> 
										<td align="right" width="60px;"> 
										  <label for="txtEmail">Email:</label> 
										</td> 
										<td>
											<h:inputText value="#{filialHandler.filial.email}" id="txtEmail" size="42" maxlength="50" /> 
										</td>								
									</tr>										
								</table>
						</fieldset>							
							<br />
							<p:commandButton actionListener="#{filialHandler.gravar}" value="Gravar" id="gravar" styleClass="button" />
							<h:commandButton actionListener="#{filialHandler.pesquisaFiliais}" value="Pesquisar" id="pesquisar" styleClass="button"/>
							<p:commandButton actionListener="#{filialHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" >
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							<div id="resultado_consulta" >
									<div class="titulo_agrupamento_Resultados" >Resultado da Consulta</div>	
									<p:dataTable value="#{filialHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty filialHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Nome da Filial" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeFilial}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Telefone" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.telefone}"/>		
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Fax" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.fax}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Email" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.email}"/>									  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
							  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{filialHandler.excluirFilial}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirFilial" name="id" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
							</div>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
