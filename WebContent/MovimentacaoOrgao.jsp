<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="movOrgHandler" />


<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery("#periodo").mask("99/9999",{placeholder:"_"});	
	jQuery("#periodoQuitacaoAnterior").mask("99/9999",{placeholder:"_"});
	if (jQuery('#elementoAvancou').val() == 'true') {
		jQuery('#divInadimplente').show();
		jQuery('#divQuitacoesAnteriores').show();
	} else {
		jQuery('#divInadimplente').hide();
		jQuery('#divQuitacoesAnteriores').hide();
	}
	
});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Movimenta��o</span><li>Movimenta��o Org�o</li> </div>
						<div id="icone">
                           <img src="images/cadOrgaos.png" />
                        </div>


				<h:form styleClass="niceform" prependId="false">
					<h:inputHidden value="#{movOrgHandler.avancou}"  id="elementoAvancou"/>
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
			
									<div class="divRecuadoEsquerdo">
										<table>
											<tr>
												<td align="left" width="20px;">
												  <label for="periodo">Per�odo:</label> 
												</td>
												<td>
													<h:inputText value="#{movOrgHandler.periodo}"  id="periodo" maxlength="10" size="12"  rendered="#{!movOrgHandler.avancou}" />
													<h:outputText value="#{movOrgHandler.periodo}"  id="periodoOutput"  rendered="#{movOrgHandler.avancou}"/>
												</td>												
											</tr>
											<tr>
												<td></td>
												<td>
													<p:commandButton actionListener="#{movOrgHandler.avancar}" rendered="#{!movOrgHandler.avancou}" value="Avan�ar" id="avancar" styleClass="button" async="false" ajax="false" />
												</td>
											</tr>											
										</table>
									</div>
									<br />						
									<div class="divRecuadoEsquerdo" id="divInadimplente" style="display: none; float: none">
									
									<fieldset style="float: left">
									
										<legend>Movimentacao Org�o Inadimplente</legend>
										<table>
											<tr>
												<td align="right" width="90px;"><label
													for="txtNomeOrgao">Org�o:</label></td>
												<td><h:inputText
														value="#{movOrgHandler.orgao.nomeOrgao}"
														id="txtNomeOrgao" size="40"  /> <rich:suggestionbox id="suggestionBoxNomeOrgao" for="txtNomeOrgao"
														suggestionAction="#{movOrgHandler.orgaoAutoComplete}"
														var="org" fetchValue="#{org.nomeOrgao}"
														bypassUpdates="true" width="500" height="250"
														cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
														<a4j:support event="onselect" id="a4jsupport"
															ignoreDupResponses="true">
															<f:setPropertyActionListener value="#{org}"
																target="#{movOrgHandler.orgao}" />
														</a4j:support>
														<h:column>
															<h:outputText value="#{org.nomeOrgao}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{org.cidade.cod_estado.sgl_estado}"
																styleClass="autocompleteFonte" />
														</h:column>																												
													</rich:suggestionbox></td>
											</tr>
											<tr>
												<td></td>
												<td><p:commandButton actionListener="#{movOrgHandler.incluirInadimplente}" value="Incluir" id="gravar" styleClass="button" async="false" ajax="false" /></td>
											</tr>											
										</table>
										
										<h:panelGroup id="agrupamentoAgenciador"
											styleClass="agrupamento_Dados">

											<p:dataTable value="#{movOrgHandler.movimentacoes}"
												var="item" width="50%"
												rendered="#{not empty movOrgHandler.movimentacoes}"
												paginator="true" rows="10"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">
												<p:column resizable="true" sortBy="#{item.orgao.nomeOrgao}">
													<f:facet name="header">
														<h:outputText value="�rg�o" />
													</f:facet>
													<h:outputText value="#{item.orgao.nomeOrgao}" />
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="Excluir" />
													</f:facet>
													<h:commandLink
														actionListener="#{movOrgHandler.excluirInadimplente}"
														onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluirInadimplente" name="excluirInadimplente"
															value="#{item}" />
													</h:commandLink>
												</p:column>
											</p:dataTable>
										</h:panelGroup>
										

									</fieldset>
									
									</div>
									<!--  Fim Orgaos inadimpmentes -->
									
									
									
									
									<!--  Orgaos com quita��es anteriores -->
									<div class="divRecuadoEsquerdo" id="divQuitacoesAnteriores" style="display: none; float: none">
									
									<fieldset style="float: left">
									
										<legend>�rg�os com quita��es de meses anteriores</legend>
										<table>
											<tr>
												<td align="right" width="90px;"><label
													for="txtOrgaoQA">�rg�o:</label></td>
												<td><h:inputText
														value="#{movOrgHandler.orgaoAnterior.nomeOrgao}"
														id="txtNomeOrgaoQA" size="40"  /> <rich:suggestionbox
														id="suggestionBoxNomeOrgaoQA" for="txtNomeOrgaoQA"
														suggestionAction="#{movOrgHandler.orgaoAutoComplete}"
														var="orgAnterior" fetchValue="#{orgAnterior.nomeOrgao}"
														bypassUpdates="true" width="500" height="250"
														cellpadding="4" nothingLabel="Nenhum Item Encontrado!">
														<a4j:support event="onselect" id="a4jsupport"
															ignoreDupResponses="true">
															<f:setPropertyActionListener value="#{orgAnterior}"
																target="#{movOrgHandler.orgaoAnterior}" />
														</a4j:support>
														<h:column>
															<h:outputText value="#{orgAnterior.nomeOrgao}"
																styleClass="autocompleteFonte" />
														</h:column>
														<h:column>
															<h:outputText value="#{orgAnterior.cidade.cod_estado.sgl_estado}"
																styleClass="autocompleteFonte" />
														</h:column>														
													</rich:suggestionbox></td>
											</tr>	
											<tr>
												<td align="right" width="90px;"><label for="periodoQuitacaoAnterior">M�s/Ano:</label></td>
												<td><h:inputText value="#{movOrgHandler.periodoQuitacaoAnterior}"  id="periodoQuitacaoAnterior" maxlength="10" size="12" /></td>
											</tr>										
											<tr>
												<td></td>
												<td><p:commandButton actionListener="#{movOrgHandler.incluirQuitacaoAnterior}" value="Incluir" id="gravarQA" styleClass="button" async="false" ajax="false" /></td>
											<tr/>												
										</table>
										
										<h:panelGroup id="agrupamentoAgenciadorQA"
											styleClass="agrupamento_Dados">

											<p:dataTable
												value="#{movOrgHandler.movimentacoesOrgaosAtrasados}"
												var="item" width="50%"
												rendered="#{not empty movOrgHandler.movimentacoesOrgaosAtrasados}"
												paginator="true" rows="10"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">

												<p:column resizable="true" sortBy="#{item.orgao.cidade.cod_estado.sgl_estado}">
													<f:facet name="header">
														<h:outputText value="UF" />

													</f:facet>
													<h:outputText value="#{item.orgao.cidade.cod_estado.sgl_estado}" />
												</p:column >
												<p:column resizable="true" sortBy="#{item.orgao.nomeOrgao}">
													<f:facet name="header">
														<h:outputText value="Nome" />

													</f:facet>
													<h:outputText
														value="#{item.orgao.nomeOrgao}" />
												</p:column>											
												<p:column sortBy="#{item.periodoQuitacaoAnterior}">
													<f:facet name="header">
														<h:outputText value="M�s/Ano" />
													</f:facet>
													<h:outputText value="#{item.periodoQuitacaoFormatado}"/>
												</p:column>												
												<p:column>
													<f:facet name="header">
														<h:outputText value="Excluir" />
													</f:facet>
													<h:commandLink
														actionListener="#{movOrgHandler.excluirQuitacaoAnterior}"
														onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluirQA" name="excluirQA"
															value="#{item}" />
													</h:commandLink>
												</p:column>
											</p:dataTable>
										</h:panelGroup>
										

									</fieldset>
									
									</div>
									<!-- Fim das Propostas com quita��es anteriores -->
								
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
