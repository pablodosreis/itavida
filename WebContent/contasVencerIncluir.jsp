<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="contasVencerHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery("#dtDataVencimento").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataPagamento").mask("99/99/9999",{placeholder:"_"});
	   jQuery('#txtValorDespesa').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Financeiro</span><li>Contas a Vencer</li> </div>
						<div id="icone">
                           <img src="images/financeiroContasVencer.png" />
                        </div>

						<h:form styleClass="niceform" prependId="false">
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
<br/><br/>
								<table>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtFilial">Filial:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencer.filial.nomeFilial}" id="txtFilial" size="40" tabindex="1"/>
											
											<rich:suggestionbox id="suggestionMotivoSinistro" 
								                	for="txtFilial"
								                    suggestionAction="#{contasVencerHandler.filialAutoComplete}" 
								                    var="filial"
								                    fetchValue="#{filial.nomeFilial}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{filial}" 
														target="#{contasVencerHandler.contasVencer.filial}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Nome da Filial" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.nomeFilial}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtDespesa">Despesa:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencer.despesa.nomeDespesa}" id="txtDespesa" size="40" tabindex="2"/>
											
											<rich:suggestionbox id="suggestionDespesa" 
								                	for="txtDespesa"
								                    suggestionAction="#{contasVencerHandler.despesaAutoComplete}" 
								                    var="despesa"
								                    fetchValue="#{despesa.nomeDespesa}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{despesa}" 
														target="#{contasVencerHandler.contasVencer.despesa}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Nome da Despesa" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{despesa.nomeDespesa}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Despesa Fixa" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{despesa.tipoDespesa == 'S' ? 'SIM' : 'N�O'}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtGrupoDespesa">Grupo de Despesa:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencer.grupoDespesa.nomeGrupoDespesa}" id="txtGrupoDespesa" size="40" tabindex="3"/>
											
											<rich:suggestionbox id="suggestionGrupoDespesa" 
								                	for="txtGrupoDespesa"
								                    suggestionAction="#{contasVencerHandler.grupoDespesaAutoComplete}" 
								                    var="grupoDespesa"
								                    fetchValue="#{grupoDespesa.nomeGrupoDespesa}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{grupoDespesa}" 
														target="#{contasVencerHandler.contasVencer.grupoDespesa}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
										  				<h:outputText value="Nome do Grupo Despesa" /> 
										  	
										  			</f:facet>
										  					<h:outputText value="#{grupoDespesa.nomeGrupoDespesa}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtFuncionario">Funcion�rio:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{contasVencerHandler.contasVencer.funcionario.nome}" id="txtFuncionario" size="40" tabindex="4"/>
											
											<rich:suggestionbox id="suggestionFuncionario" 
								                	for="txtFuncionario"
								                    suggestionAction="#{contasVencerHandler.funcionarioAutoComplete}" 
								                    var="funcionario"
								                    fetchValue="#{funcionario.nome}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{funcionario}" 
														target="#{contasVencerHandler.contasVencer.funcionario}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Funcion�rio" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>
								         	</rich:suggestionbox>
										</td> 
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtValorDespesa">Valor:</label>
										</td>
										<td>
										   <h:inputText value="#{contasVencerHandler.contasVencer.valorDespesa}" id="txtValorDespesa" size="15" maxlength="15" tabindex="5">
													<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>
										</td>											
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataVencimento" >Data do Vencimento:</label>
										</td>
										<td>
											<h:inputText value="#{contasVencerHandler.contasVencer.dataVencimento}"  id="dtDataVencimento" maxlength="10"  tabindex="6">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										   <label for="cmbReplicar">Replicar Pagamento:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbReplicar" value="#{contasVencerHandler.contasVencer.replicas}"  tabindex="7">
										   	<f:selectItems value="#{contasVencerHandler.replicas}" />
										   </h:selectOneMenu> Vezes
		
										</td> 

									</tr> 
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataPagamento" >Data do Pagamento:</label>
										</td>
										<td>
											<h:inputText value="#{contasVencerHandler.contasVencer.dataPagamento}"  id="dtDataPagamento" readonly="true" maxlength="10"  tabindex="8">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
								</table>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										   <label for="cmbContaPaga">Conta Paga:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbContaPaga" value="#{contasVencerHandler.contasVencer.contaPaga}" readonly="true" disabled="true" tabindex="9">
										   	<f:selectItems value="#{contasVencerHandler.simNaoCombo}" />
										   </h:selectOneMenu>
										</td> 

									</tr> 
								</table>
							</fieldset>							
							<br />
							
							<p:commandButton actionListener="#{contasVencerHandler.gravar}" value="Gravar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="10"/>
							<p:commandButton actionListener="#{contasVencerHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="11">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
