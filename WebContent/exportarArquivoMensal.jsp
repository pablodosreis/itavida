<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="exportarArquivoMensalHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery("#dtPeriodoReferencia").mask("99/9999",{placeholder:"_"});

	});

</script>
<BODY class=f-default>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Exporta��o</span><li>Arquivo Mensal Seguradora</li> </div>
						<div id="icone">
                           <img src="images/financeiroContasVencer.png" />
                        </div>

						<h:form styleClass="niceform" prependId="false">
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>

<br/><br/>
						        <rich:pickList switchByDblClick="true" listsHeight="290" sourceListWidth="300" targetListWidth="300" copyAllControlLabel="Selecionar Todos" removeAllControlLabel="Remover Todos" removeControlLabel="Remover" copyControlLabel="Selecionar" value="#{exportarArquivoMensalHandler.listaOrgaosSelecionados}"  >
									<f:selectItems id="selectItensOrgaos" value="#{exportarArquivoMensalHandler.orgaosSelectItens}" />
						        </rich:pickList> 
							</fieldset>
							<br />
							<h:commandButton action="#{exportarArquivoMensalHandler.generateReport}" value="Gerar Exporta��o" id="gerarExport" styleClass="button" tabindex="2"/>
							<p:commandButton actionListener="#{exportarArquivoMensalHandler.limparDados}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="3">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
