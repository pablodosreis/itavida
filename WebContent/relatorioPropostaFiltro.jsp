<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="propostaHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery("#dtNascimentoSeguradoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoSeguradoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoConjugeInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoConjugeFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAprovacaoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAprovacaoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCadastroInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCadastroFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCancelamentoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCancelamentoFim").mask("99/99/9999",{placeholder:"_"});	  	   
	   
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rio</span><li>Relat�rio de Segurados </li> </div>
						<div id="icone">
                           <img src="images/cadProposta.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false">


							<div class="titulo_agrupamento_Dados" ></div>
							
							<fieldset>
								<legend>Dados Para Consulta</legend>
								<br/> <br/>
								<table >				
									<tr>
										<td align="right" width="150px;">
										  <label for="txtIdProposta">N�mero da Proposta:</label>
										</td>
										<td>
										   <h:inputText value="#{propostaHandler.propostaFiltro.id}" id="txtIdProposta" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="1"/>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="cmbEmpresa">Empresa:</label>
										</td>
										<td>
										   <h:selectOneMenu id="cmbEmpresa" value="#{propostaHandler.propostaFiltro.empresa.id}"  tabindex="2">
										   		<f:selectItems value="#{propostaHandler.opcoesEmpresaCombo}" />
										   </h:selectOneMenu>
										</td>								
									</tr>																		
								</table>
									<div class="divRecuadoDireito">
										<table> 
											<tr> 
												<td align="left" width="90px;"> 
												   <label for="cmbTipoOrgao">Tipo de �rg�o:</label> 
												</td> 
											</tr>
											<tr>
												<td align="right">
												   <h:selectOneMenu id="cmbTipoOrgao" binding="#{propostaHandler.tipoOrgaoSelecionado}" value="#{propostaHandler.propostaFiltro.orgao.tipoOrgao}" >
												   	<f:selectItems value="#{propostaHandler.tipoOrgaoCombo}"/>
												   </h:selectOneMenu>
												</td> 
		
											</tr> 
										</table>
									</div>
								<table>
									<tr> 
										<td align="right" width="150px;"> 
										   <label for="cmbModeloProposta">Modelo de Proposta:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbModeloProposta" value="#{propostaHandler.propostaFiltro.modeloProposta.id}" tabindex="2">
										   	<f:selectItems value="#{propostaHandler.modeloPropostaCombo}" />
												<p:ajax actionListener="#{propostaHandler.populaTipoPropostaComboFiltro}" update="cmbTipoProposta" event="change"/>
										   </h:selectOneMenu>
										</td> 
										<td align="right" width="150px;"> 
										   <label for="cmbTipoProposta">Tipo de Proposta:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbTipoProposta" value="#{propostaHandler.propostaFiltro.tipoProposta.id}" tabindex="3">
										   	<f:selectItems value="#{propostaHandler.tipoPropostaComboFiltro}" />
										   </h:selectOneMenu>
										</td> 
									</tr> 
								</table>

								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao">�rg�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{propostaHandler.propostaFiltro.orgao.nomeOrgao}" id="txtOrgao" size="40" tabindex="4"/>
											<rich:suggestionbox id="suggestionBoxOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{propostaHandler.orgaoAutoComplete}" 													
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="450"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{propostaHandler.propostaFiltro.orgao}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{orgao.nomeOrgao}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{orgao.siglaOrgao}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  

								<table>	
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNomeTabela">Nome da Tabela:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{propostaHandler.propostaFiltro.nomeTabela.nomeTabela}" id="txtNomeTabela" size="30" tabindex="5" />
											<rich:suggestionbox id="suggestionBoxNomeTabela" 
								                	for="txtNomeTabela"
								                    suggestionAction="#{propostaHandler.nomeTabelaAutocomplete}" 
								                    var="nomeTabela"
								                    fetchValue="#{nomeTabela.nomeTabela}"
								                    width="450"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{nomeTabela}" 
														target="#{propostaHandler.propostaFiltro.nomeTabela}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabela}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabelaReduzido}" styleClass="autocompleteFonte"/>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>								
							</fieldset>	
							<fieldset>
									<legend>Dados do Segurado Principal</legend>
									
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoPrincipal">Nome:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.nomeSegurado}" id="txtNomeSeguradoPrincipal" size="50" maxlength="50" tabindex="6"/>
											</td>
										</tr>
									</table>
									<table>
										<tr>
										<td align="right" width="150px;"> 
										   <label for="cmbSexoSegurado">Sexo:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbSexoSegurado" value="#{propostaHandler.propostaFiltro.sexoSegurado}" tabindex="7">
										   		<f:selectItems value="#{propostaHandler.sexoCombo}" />
										   </h:selectOneMenu>
										</td> 
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="dtNascimentoSeguradoInicio" >Data Nascimento (in�cio):</label>
											</td>
											<td>
												<h:inputText value="#{propostaHandler.propostaFiltro.dataNascimentoSeguradoInicio}"  id="dtNascimentoSeguradoInicio" maxlength="10" tabindex="8">
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
												</h:inputText>
											</td>
											<td align="right" >
											  <label for="dtNascimentoSeguradoFim" >(fim):</label>
											</td>
											<td>
												<h:inputText value="#{propostaHandler.propostaFiltro.dataNascimentoSeguradoFim}"  id="dtNascimentoSeguradoFim" maxlength="10" tabindex="9">
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
												</h:inputText>
											</td>
										</tr>									
										<tr>
											<td align="right" width="150px;">
											  <label for="txtMatricula">Matr�cula:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.matriculaSegurado}" id="txtMatricula" size="10" maxlength="10" tabindex="10"/>
											</td>
										</tr>
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtCpf">CPF:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.cpfSegurado}" id="txtCpf" size="20" maxlength="20" tabindex="11"/>
											</td>								
										</tr>									
									</table>
									<table>
										<tr>										
										<td align="right" width="150px;"> 
										   <label for="cmbEstadoCivilSegurado">Estado Civil:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbEstadoCivilSegurado" value="#{propostaHandler.propostaFiltro.estadoCivilSegurado}" tabindex="12">
										   		<f:selectItems value="#{propostaHandler.estadoCivilCombo}" />
										   </h:selectOneMenu>
										</td> 											
										</tr>
									</table>									
								</fieldset>

<!-- SEGURADO CONJUGE -->
								<fieldset>
									<legend><IMG title="Expandir" id="expandirconteudoConjuge" alt="Expandir" onclick="expandir('conteudoConjuge');" src="images/expandir.png" border=0 /><IMG id="retrairconteudoConjuge" title="Retrair" alt="Retrair" onclick="retrair('conteudoConjuge');" src="images/retrair.png" border=0 style="display:none;" /> Dados do Segurado C�njuge</legend>
									<div id="conteudoConjuge" style="display:none;">
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoConjuge">Nome:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.nomeConjuge}" id="txtNomeSeguradoConjuge" size="50" maxlength="50" tabindex="13"/>
											</td>
										</tr>
									</table>
									<table>
										<tr>
										<td align="right" width="150px;"> 
										   <label for="cmbSexoConjuge">Sexo:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbSexoConjuge" value="#{propostaHandler.propostaFiltro.sexoConjuge}" tabindex="14">
										   		<f:selectItems value="#{propostaHandler.sexoCombo}" />
										   </h:selectOneMenu>
										</td> 
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="dtNascimentoConjugeInicio" >Data Nascimento (in�cio):</label>
											</td>
											<td>
												<h:inputText value="#{propostaHandler.propostaFiltro.dataNascimentoConjugeInicio}"  id="dtNascimentoConjugeInicio" maxlength="10" tabindex="15">
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
												</h:inputText>
											</td>
											<td align="right" >
											  <label for="dtNascimentoConjugeFim" >(fim):</label>
											</td>
											<td>
												<h:inputText value="#{propostaHandler.propostaFiltro.dataNascimentoConjugeFim}"  id="dtNascimentoConjugeFim" maxlength="10" tabindex="16">
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
												</h:inputText>
											</td>
										</tr>									
										<tr>
											<td align="right" width="150px;">
											  <label for="txtMatriculaConjuge">Matr�cula:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.matriculaConjuge}" id="txtMatriculaConjuge" size="10" maxlength="10" tabindex="17"/>
											</td>
										</tr>
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtCPFConjuge">CPF:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.cpfConjuge}" id="txtCPFConjuge" size="20" maxlength="20" tabindex="18"/>
											</td>								
										</tr>									
									</table>
									<table>
										<tr>										
										<td align="right" width="150px;"> 
										   <label for="cmbEstadoCivilConjuge">Estado Civil:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbEstadoCivilConjuge" value="#{propostaHandler.propostaFiltro.estadoCivilConjuge}" tabindex="19">
										   		<f:selectItems value="#{propostaHandler.estadoCivilCombo}" />
										   </h:selectOneMenu>
										</td> 											
										</tr>
									</table>
								</div>	
								</fieldset>	
<!-- ENDERE�O -->								
								<fieldset>	
									<legend><IMG title="Expandir" id="expandirconteudoEndereco" alt="Expandir" onclick="expandir('conteudoEndereco');" src="images/expandir.png" border=0 /><IMG id="retrairconteudoEndereco" title="Retrair" alt="Retrair" onclick="retrair('conteudoEndereco');" src="images/retrair.png" border=0 style="display:none;" />Endere�o</legend>							
									<div id="conteudoEndereco" style="display:none;">
										<table>
											<tr>
												<td align="right" width="90px;">
												  <label for="txtEndereco">Endere�o:</label>
												</td>
												<td>
												   <h:inputText value="#{propostaHandler.propostaFiltro.enderecoSegurado}" id="txtEndereco" size="50" maxlength="110" tabindex="20"/>
												</td>								
											</tr>
										</table>
										<table>
											<tr>
												<td align="right" width="90px;">
												  <label for="txtBairro">Bairro:</label>
												</td>
												<td>
												   <h:inputText value="#{propostaHandler.propostaFiltro.bairro}" id="txtBairro" size="40" maxlength="50" tabindex="21"/>
												</td>								
											</tr>									
										</table>	
										<table>
										<tr> 
											<td align="right" width="90px;"> 
											  <label for="txtNomeCidade">Cidade:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{propostaHandler.propostaFiltro.cidade.nom_cidade}" id="txtNomeCidade" size="30"  tabindex="22"/>
											   <h:inputText value="#{propostaHandler.propostaFiltro.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" style="width:30px;" disabled="true" styleClass="textinput" />
												<rich:suggestionbox id="suggestionBoxCidade" 
									                	for="txtNomeCidade"
									                    suggestionAction="#{propostaHandler.cidadeAutocomplete}" 
									                    var="cidade"
									                    fetchValue="#{cidade.nom_cidade}"
									                    width="300"
									                    height="250"
									                    cellpadding="4"
	                    								nothingLabel="Nenhum Item Encontrado!"
									                    >
									                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" reRender="txtUf"> 
															<f:setPropertyActionListener 
															value="#{cidade}" 
															target="#{propostaHandler.propostaFiltro.cidade}"/> 
														</a4j:support> 
									                    <h:column>
									                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
									                    </h:column>
									                    <h:column>
									                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
									                    </h:column>
									             </rich:suggestionbox>
											</td> 
										</tr> 
										</table>	
									</div>	
								</fieldset>													
								<fieldset>	
									<legend><IMG title="Expandir" id="expandirconteudoAgenciador" alt="Expandir" onclick="expandir('conteudoAgenciador');" src="images/expandir.png" border=0 /><IMG id="retrairconteudoAgenciador" title="Retrair" alt="Retrair" onclick="retrair('conteudoAgenciador');" src="images/retrair.png" border=0 style="display:none;" />Agenciador</legend>
									<div id="conteudoAgenciador" style="display:none;">									
										<table>
											<tr> 
												<td align="right" width="90px;"> 
												  <label for="txtAgenciador">Agenciador:</label> 
												</td> 
												<td> 
												   <h:inputText value="#{propostaHandler.propostaFiltro.funcionario.nome}" id="txtAgenciador" size="40" tabindex="23"/>
													
													<rich:suggestionbox id="suggestionBoxFuncionario" 
										                	for="txtAgenciador"
										                    suggestionAction="#{propostaHandler.funcionarioAutoComplete}" 
										                    var="funcionario"
										                    fetchValue="#{funcionario.nome}"
										                    width="500"
										                    height="250"
										                    cellpadding="4"
		                    								nothingLabel="Nenhum Item Encontrado!"
										                    >
										                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
																<f:setPropertyActionListener 
																value="#{funcionario}" 
																target="#{propostaHandler.propostaFiltro.funcionario}"/> 
															</a4j:support> 
										                    <h:column>
										                       <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/> 
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.tipoFuncionarioDescricao}" styleClass="autocompleteFonte"/>
										                    </h:column>
										                    <h:column>
										                       <h:outputText value="#{funcionario.percentualComissao}" styleClass="autocompleteFonte"/>
										                    </h:column>
										             </rich:suggestionbox>
												</td>
											</tr> 
										</table>
									</div>	
								</fieldset>
								<fieldset>
									<legend><IMG title="Expandir" id="expandirconteudoInfoGerais" alt="Expandir" onclick="expandir('conteudoInfoGerais');" src="images/expandir.png" border=0 /><IMG id="retrairconteudoInfoGerais" title="Retrair" alt="Retrair" onclick="retrair('conteudoInfoGerais');" src="images/retrair.png" border=0 style="display:none;" />Informa��es Gerais</legend>										
									<div id="conteudoInfoGerais" style="display:none;">	
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="dtDataAprovacaoInicio" >Data Aprova��o (in�cio):</label>
												</td>
												<td>
													<h:inputText value="#{propostaHandler.propostaFiltro.dataAprovacaoInicio}"  id="dtDataAprovacaoInicio" maxlength="10"  tabindex="24">
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
													</h:inputText>
												</td>
												<td align="right" width="140px;">
												  <label for="dtDataAprovacaoFim" >(fim):</label>
												</td>
												<td>
													<h:inputText value="#{propostaHandler.propostaFiltro.dataAprovacaoFim}"  id="dtDataAprovacaoFim" maxlength="10" tabindex="25">
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
													</h:inputText>
												</td>
											</tr>									
										</table>							
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="txtNumeroSorteio">N�mero do Sorteio:</label>
												</td>
												<td>
												   <h:inputText value="#{propostaHandler.propostaFiltro.numeroSorteioFiltro}" id="txtNumeroSorteio" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="28"/>
												</td>								
											</tr>																		
										</table>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="dtDataCancelamentoInicio" >Data Cancelamento (in�cio):</label>
												</td>
												<td>
													<h:inputText value="#{propostaHandler.propostaFiltro.dataCancelamentoInicio}"  id="dtDataCancelamentoInicio" maxlength="10" tabindex="29">
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
													</h:inputText>
												</td>
												<td align="right" width="140px;">
												  <label for="dtDataCancelamentoFim" >(fim):</label>
												</td>
												<td>
													<h:inputText value="#{propostaHandler.propostaFiltro.dataCancelamentoFim}"  id="dtDataCancelamentoFim" maxlength="10" tabindex="30">
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
													</h:inputText>
												</td>
											</tr>											
										</table>
									</div>
								</fieldset>		
								<fieldset>
									<legend><IMG title="Expandir" id="expandirtipoConsulta" alt="Expandir" onclick="expandir('tipoConsulta');" src="images/expandir.png" border=0 style="display:none;" /><IMG id="retrairtipoConsulta" title="Retrair" alt="Retrair" onclick="retrair('tipoConsulta');" src="images/retrair.png" border=0  /> Tipo de Consulta</legend>
									<div id="tipoConsulta" style="display:block;">
								
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="cmbTipoRelatorio">Tipo de Consulta:</label>
											</td>
											<td align="left">
											   <h:selectOneMenu id="cmbTipoRelatorio"  value="#{propostaHandler.tipoRelatorio}" tabindex="9">
											   	<f:selectItems value="#{propostaHandler.tipoRelatorioCombo}"/>
											   </h:selectOneMenu>
											</td> 										
										</tr>
									</table>
								</div>	
								</fieldset>							
												
							<br />
							<h:commandButton action="#{propostaHandler.pesquisaPropostasRelatorio}" value="Gerar Relat�rio" id="pesquisar" styleClass="button" tabindex="31"/>
							<p:commandButton actionListener="#{propostaHandler.cancelarFiltro}" value="Cancelar" id="cancelar" styleClass="button" async="false" tabindex="32">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
