<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="siapeHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
	jQuery("#dtPeriodoReferencia").mask("99/9999",{placeholder:"_"});
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Arquivos </span><li>Visualizar Importa��o de Arquivo D8 - SIAPE </li></div>
						<div id="icone_tokio">
                           <img src="images/siape.png" />
                        </div>
                          

						<h:form  styleClass="niceform" prependId="false">

						<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right" > 
										   <label for="dtPeriodoReferencia">Per�odo de Refer�ncia:</label> 
										</td> 
										<td>
											<h:inputText value="#{siapeHandler.siape.dataReferencia}"  id="dtPeriodoReferencia"  maxlength="7" tabindex="1">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" /> 
											</h:inputText>
										</td>								
									</tr>
									<tr>
										<td align="right">
										  <label for="txtNomeServidor">Nome Servidor:</label>
										</td>
										<td>
										 	<h:inputText value="#{siapeHandler.siape.nomeServidor}" id="txtNomeServidor" size="40" maxlength="50" tabindex="2"/>
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="ufPag">UF:</label>
										</td>
										<td>
										 	<h:inputText value="#{siapeHandler.siape.ufpag}" id="ufPag" size="2" maxlength="2" tabindex="3"/>
										</td>									
									</tr>

								</table>					
							</fieldset>							

							<br />
							<h:commandButton actionListener="#{siapeHandler.pesquisa}" value="Consultar" id="pesquisar" styleClass="button" tabindex="4" />
							<p:commandButton actionListener="#{siapeHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="5">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty siapeHandler.resultadoPesquisa}"  constrainToViewport="true" fixedCenter="true" underlay="shadow" > 

									<p:dataTable id="resultTable" value="#{siapeHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty siapeHandler.resultadoPesquisa}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.dataReferencia}" >
										  <f:facet name="header">
										  <h:outputText value="Data Refer�ncia" /> 
										  </f:facet>
										  <h:outputText value="#{item.dataReferencia}"><f:convertDateTime dateStyle="default" pattern="MM/yyyy"  /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.nomeServidor}">
										  <f:facet name="header"><h:outputText value="Nome do Servidor" /> </f:facet>

										  <h:outputText value="#{item.nomeServidor}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.matricula}">
										  <f:facet name="header">
										  <h:outputText value="Matr�cula Siape" /> 
										  </f:facet>
										  <h:outputText value="#{item.matricula}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cpf}">
										  <f:facet name="header">
										  <h:outputText value="Cpf" /> 
										  </f:facet>
										  <h:outputText value="#{item.cpf}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.codigoOrgao.nomeOrgaoSiape}">
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  </f:facet>
										  <h:outputText value="#{item.codigoOrgao.nomeOrgaoSiape}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.ufpag}">
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  </f:facet>
										  <h:outputText value="#{item.ufpag}"/>									  										
										</p:column>	

										<p:column resizable="true" sortBy="#{item.valor}">
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  </f:facet>
										 <h:outputText value="#{item.valor}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>									  										
										</p:column>	
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="Siape_D8"  postProcessor="#{siapeHandler.postProcessXLS}" />  
										</h:commandLink>
								</div>
						</p:dialog>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
