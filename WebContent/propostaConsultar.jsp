<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="propostaHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery("#txtCpf").mask("999.999.999-99",{placeholder:"_"});
	   jQuery("#dtNascimentoSeguradoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoSeguradoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoConjugeInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoConjugeFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAprovacaoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAprovacaoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCadastroInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCadastroFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCancelamentoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCancelamentoFim").mask("99/99/9999",{placeholder:"_"});	  	   

	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Gest�o</span><li>Proposta - Consultar</li> </div>
						<div id="icone">
                           <img src="images/cadProposta.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false">


							<div class="titulo_agrupamento_Dados" ></div>
							
							<fieldset>
								<legend>Dados Para Consulta</legend>
								<br/> <br/>
								<table >				
									<tr>
										<td align="right" width="150px;">
										  <label for="txtIdProposta">N�mero da Proposta:</label>
										</td>
										<td>
										   <h:inputText value="#{propostaHandler.propostaFiltro.id}" id="txtIdProposta" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="1"/>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="cmbEmpresa">Empresa:</label>
										</td>
										<td>
										   <h:selectOneMenu id="cmbEmpresa" value="#{propostaHandler.propostaFiltro.empresa.id}" >
										   		<f:selectItems value="#{propostaHandler.opcoesEmpresaCombo}" />
										   </h:selectOneMenu>
										</td>								
									</tr>																		
								</table>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao">�rg�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{propostaHandler.propostaFiltro.orgao.nomeOrgao}" id="txtOrgao" size="40">
												<p:ajax event="keyup" update="renderizado" >
													<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{propostaHandler.limparOrgaoFiltro}" 
														target="#{propostaHandler.propostaFiltro.orgao}"/> 
												</p:ajax>
											</h:inputText>
											<rich:suggestionbox id="suggestionBoxOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{propostaHandler.orgaoAutoComplete}" 
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="450"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{propostaHandler.propostaFiltro.orgao}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{orgao.nomeOrgao}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{orgao.siglaOrgao}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
								<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoPrincipal">Nome Segurado:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.nomeSegurado}" size="25" id="txtNomeSeguradoPrincipal"/>
											</td>
										</tr>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtProfissaoSeguradoPrincipal">Profiss�o do Segurado:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.profissaoSegurado}" size="25" id="txtProfissaoSeguradoPrincipal"/>
											</td>
										</tr>
									</table>
									<table>
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtCpf">CPF Segurado:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.cpfSegurado}" id="txtCpf" size="25" maxlength="20"/>
											</td>								
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;"> 
											   <label for="mesAniversario">Anivers�rio (m�s):</label> 
											</td>
											<td width="50px;">
												<h:selectOneMenu id="mesAniversario" value="#{propostaHandler.propostaFiltro.mesAniversario}">
											   		<f:selectItems value="#{propostaHandler.opcoesMesesCombo}" />
											   </h:selectOneMenu>
											</td>
										</tr>									
								</table>
																
								<table>
										<tr> 
											<td align="right" width="90px;"> 
											  <label for="txtNomeCidade">Cidade:</label> 
											</td> 
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.cidade.nom_cidade}" id="txtNomeCidade" size="30"  autocomplete="off">
											   	<p:ajax event="keyup" update="renderizadoCidade" >
													<h:panelGroup id="renderizadoCidade" />
													<f:setPropertyActionListener 
														value="#{propostaHandler.limparCidadeFiltro}" 
														target="#{propostaHandler.propostaFiltro.cidade}"/> 
												</p:ajax>
											   </h:inputText>
											   <h:inputText value="#{propostaHandler.propostaFiltro.cidade.cod_estado.sgl_estado}" id="txtUf" size="3" style="width:30px;" disabled="true" styleClass="textinput" />
												<rich:suggestionbox id="suggestionBoxCidade" 
									                	for="txtNomeCidade"
									                    suggestionAction="#{propostaHandler.cidadeAutocomplete}" 
									                    var="cidade"
									                    fetchValue="#{cidade.nom_cidade}"
									                    width="300"
									                    height="250"
									                    cellpadding="4"
	                    								nothingLabel="Nenhum Item Encontrado!"
									                    >
									                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" reRender="txtUf"> 
															<f:setPropertyActionListener 
															value="#{cidade}" 
															target="#{propostaHandler.propostaFiltro.cidade}"/> 
														</a4j:support> 
									                    <h:column>
									                       <h:outputText value="#{cidade.nom_cidade}" styleClass="autocompleteFonte"/> 
									                    </h:column>
									                    <h:column>
									                       <b> <h:outputText value="#{cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/></b>
									                    </h:column>
									             </rich:suggestionbox>
											</td> 
										</tr> 
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoConjuge">Nome C�njuge:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.nomeConjuge}" style="width: 150px" id="txtNomeSeguradoConjuge"/>
											</td>
										</tr>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtProfissaoConjuge">Profiss�o do Conjuge:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.profissaoConjuge}" style="width: 150px" id="txtProfissaoConjuge"/>
											</td>
										</tr>
										<tr>										
												<td align="right" width="150px;"> 
												   <label for="cmbTipoConsulta">Situa��o da Proposta:</label> 
												</td> 
												<td>
													<table border="0">
														<tr>
														<td>
															<h:selectManyCheckbox id="cmbTipoConsulta" value="#{propostaHandler.situacaoPropostaSelectedList}">
																<f:selectItems value="#{propostaHandler.opcoesSituacaoPropostaCombo}" />
															</h:selectManyCheckbox>
													   	</td>
													   	</tr>
												   </table>
												</td>
											</tr>
								</table>
						</fieldset>
							<br />
							<h:commandButton actionListener="#{propostaHandler.pesquisaPropostas}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="7"/>
							<p:commandButton actionListener="#{propostaHandler.cancelarFiltro}" value="Cancelar" id="cancelar" styleClass="button" async="false"  ajax="false" tabindex="8">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>



<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty propostaHandler.resultadoPesquisa && usuarioHandler.usuario.perfil ne 'FIL' }" constrainToViewport="true" x="0" y="50" underlay="shadow" > 

									<p:dataTable value="#{propostaHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 id="resultTable"
												 rendered="#{not empty propostaHandler.resultadoPesquisa}" 												
												 paginator="true" rows="9" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;" dynamic="false" update="resultTable" >
										
										<p:column resizable="true" sortBy="#{item.id}" width="35" >

										  <f:facet name="header">
										  <h:outputText value="Proposta" /> 
										  </f:facet>
											 <h:outputLink value="#{facesContext.externalContext.requestContextPath}/verProposta.jsf?id=#{item.id}" title="Dados da Proposta #{item.id}">    
											 <h:outputText value="#{item.id}"/>
											</h:outputLink>
										</p:column>

										<p:column resizable="true" sortBy="#{item.empresa.nomeEmpresa}" width="50">
										  <f:facet name="header">
										  <h:outputText value="Empresa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.empresa.nomeEmpresa}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.orgao.nomeOrgao}" width="120">
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.nomeSegurado}" width="150" >
										  <f:facet name="header">
										  <h:outputText value="Segurado Principal" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeSegurado}"/>									  										
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.telefoneCelular}" width="80" >
										  <f:facet name="header">
										  <h:outputText value="Celular" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.telefoneCelular}"/>									  										
										</p:column>		
										
										<p:column resizable="true" sortBy="#{item.profissaoSegurado}" width="100" >
										  <f:facet name="header">
										  <h:outputText value="Profiss�o Segurado" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.profissaoSegurado}"/>									  										
										</p:column>	
										
										<p:column resizable="true" sortBy="#{item.dataNascimentoSegurado}"  width="60">
										  <f:facet name="header">
										  <h:outputText value="Dt Nascimento" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataNascimentoSegurado}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cidade.nom_cidade}" width="120">
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.cidade.cod_estado.sgl_estado}" width="13">
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.totalPremios}" width="50">
										  <f:facet name="header">
										  <h:outputText value="Pr�mio Total" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.totalPremios}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="#{false}"/></h:outputText>
										</p:column>	
										<p:column resizable="true"  width="55">
										  <f:facet name="header">
										  <h:outputText value="Situa��o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.status}" />
										</p:column>
										
										<!-- Pagamento -->
										<p:column resizable="true"  width="75">
										  <f:facet name="header">
										  <h:outputText value="Cancelamento" /> 
										  </f:facet>
										  <h:outputText value="#{item.cancelamento}" />
										</p:column>
										<!-- Fim Pagamento -->
											
										<p:column styleClass="centralizado" width="25" rendered="#{ usuarioHandler.usuario.perfil eq 'ADM' || usuarioHandler.usuario.perfil eq 'CAD' || usuarioHandler.usuario.perfil eq 'SIN'  }">
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <p:commandLink  action="#{propostaHandler.preparaAlterar}" ajax="false" async="false" >
										  	<p:graphicImage url="images/edit.png" title="Alterar" alt="Alterar" />
											<f:setPropertyActionListener target="#{propostaHandler.proposta}" value="#{item}"/>
										  </p:commandLink>								  										
										</p:column>	
										<p:column styleClass="centralizado" width="27"  rendered="#{ usuarioHandler.usuario.perfil eq 'ADM' || usuarioHandler.usuario.perfil eq 'SIN'  }">
										  <f:facet name="header">
										  <h:outputText value="Sinistro" /> 
										  	
										  </f:facet>
												<p:commandLink action="#{propostaHandler.goIncluirHistorico}" ajax="false" async="false">								
													<p:graphicImage  title="Incluir Sinistro" alt="Incluir Sinistro" url="images/incluirSinistro.png" /> 
													<f:setPropertyActionListener target="#{propostaHandler.numeroPropostaParameter}" value="#{item.id}" />
												</p:commandLink >
										</p:column>	

										<p:column styleClass="centralizado" width="25">
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										   <h:commandLink actionListener="#{propostaHandler.excluirProposta}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
											<p:graphicImage url="images/excluir.png" title="Excluir" alt="Excluir"  >										  	
										  		<f:param id="excluirProposta" name="id" value="#{item}"/>
											</p:graphicImage>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
								<div align="right">

										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="Propostas"  postProcessor="#{propostaHandler.postProcessXLS}" excludeColumns="8,9,10"/>  
										</h:commandLink>
								</div>
						</p:dialog>

<p:dialog visible="true" header="Resultado da Consulta" rendered="#{not empty propostaHandler.resultadoPesquisa && usuarioHandler.usuario.perfil eq 'FIL'}" constrainToViewport="true" x="0" y="50" underlay="shadow" > 
										

<div style="overflow: auto; height:400px; width: 1400px" >
<p:lightBox iframe="true" width="95%" height="95%" id="lightBoxProposta" currentTemplate="Mostrando item {current} de {total}" group="true" >
									<p:dataTable value="#{propostaHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 id="resultTableView"												 
												 rendered="#{not empty propostaHandler.resultadoPesquisa}" 												
												 dblClickSelect="true" update="resultTableView,lightBoxProposta" >
										


										<p:column resizable="true"  width="35" >

										  <f:facet name="header">
										  <h:outputText value="Proposta" /> 
										  </f:facet>

											<h:outputLink value="#{facesContext.externalContext.requestContextPath}/propostaVisualizar.jsf?propostaVisualizar=#{item.id}" title="Dados da Proposta #{item.id}">    
											 <h:outputText value="#{item.id}"/>
											</h:outputLink> 
										</p:column>

										<p:column resizable="true"  width="60" >
										  <f:facet name="header" >
										  <h:outputText value="Empresa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.empresa.nomeEmpresa}"/>									  										
										</p:column>	
										<p:column resizable="true"  width="170">
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
										</p:column>	
										<p:column resizable="true" width="220" >
										  <f:facet name="header">
										  <h:outputText value="Segurado Principal" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeSegurado}"/>									  										
										</p:column>
										
										<p:column resizable="true" width="100" >
										  <f:facet name="header">
										  <h:outputText value="Celular" /> 
										  </f:facet>
										  <h:outputText value="#{item.telefoneCelular}"/>									  										
										</p:column>
										
										<p:column resizable="true" sortBy="#{item.profissaoSegurado}" width="100" >
										  <f:facet name="header">
										  <h:outputText value="Profiss�o Segurado" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.profissaoSegurado}"/>									  										
										</p:column>			
										
										<p:column resizable="true"   width="58">
										  <f:facet name="header">
										  <h:outputText value="Dt Nascimento" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataNascimentoSegurado}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true"  width="120">
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true"  width="13">
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>
										<p:column resizable="true"  width="50">
										  <f:facet name="header">
										  <h:outputText value="Pr�mio Total" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.totalPremios}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="#{false}"/></h:outputText>
										</p:column>	
										<p:column resizable="true"  width="75">
										  <f:facet name="header">
										  <h:outputText value="Situa��o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.status}" />
										</p:column>
										<p:column resizable="true"  width="75">
										  <f:facet name="header">
										  <h:outputText value="Cancelamento" /> 
										  </f:facet>
										  <h:outputText value="#{item.cancelamento}" />
										</p:column>
									</p:dataTable>
</p:lightBox> 

</div>
						
<h:panelGrid columns="2" >
<h:panelGroup>
<fieldset style="width: 900px;">
<legend style="font-size: 1.2em;">Sum�rio da Pesquisa</legend>
<h:panelGrid columns="2" >
	<h:panelGrid columns="2" >
		<h:outputText value="Total de Registros: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{propostaHandler.totalRegistrosRelatorio}" />
		<h:outputText value="Total de Pr�mios: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{propostaHandler.valorTotalRegistrosRelatorio}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>
<h:outputText value="Total de Pr�mios (Ativos): " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{propostaHandler.valorTotalPremioAtivos}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="true"/></h:outputText>

	</h:panelGrid>
	<h:panelGrid columns="2" >
		<h:outputText value="Segurados Ativos: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{propostaHandler.totalRegistrosAtivos}" />
		<h:outputText value="Segurados Cancelados: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /><h:outputText value="#{propostaHandler.totalRegistrosCancelados}" />
		<h:outputText value="Segurados Sem Averba��o: " style="FONT: bold 12px Arial, Helvetica, sans-serif; TEXT-TRANSFORM: none; COLOR: #3C3C3C;" /> <h:outputText value="#{propostaHandler.totalRegistrosSemAverbacao}" />
	</h:panelGrid>
</h:panelGrid>
</fieldset>
</h:panelGroup>
<h:commandLink >  
    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
    <p:dataExporter type="xls" target="resultTableView" fileName="Propostas"  postProcessor="#{propostaHandler.postProcessXLS}" />  
</h:commandLink>
</h:panelGrid>
						</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
