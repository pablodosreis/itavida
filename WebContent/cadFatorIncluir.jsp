<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="empresaHandler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="fatorHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
	 jQuery("#dataInicioFator").mask("99/99/9999",{placeholder:"_"});	
}); 
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Reajuste por Faixa Et�ria</li> </div>
						<div id="icone">
                           <img src="images/empresa.png" />
                        </div>
						<h:form id="myform" styleClass="niceform" prependId="false">
						

					<div class="titulo_agrupamento_Dados" ></div>
					
						<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="txtNomeTabela">Nome da Tabela:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{fatorHandler.nomeTabela.nomeTabela}" id="txtNomeTabela" size="30"  >
												<p:ajax event="keyup" update="renderizado" >
													<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{fatorHandler.limparNomeTabela}" 
														target="#{fatorHandler.nomeTabela}" /> 
												</p:ajax>
										   </h:inputText>
										   
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeTabela" 
								                    suggestionAction="#{fatorHandler.nomeTabelaAutocomplete}" 
								                    var="nomeTabela" 
								                    fetchValue="#{nomeTabela.nomeTabela}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" 
								                    	action="#{fatorHandler.carregaComboCapitais}" reRender="boxCapitais"> 
														<f:setPropertyActionListener 
														value="#{nomeTabela}" 
														target="#{fatorHandler.nomeTabela}"/> 
													</a4j:support> 

								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabela}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabelaReduzido}" styleClass="autocompleteFonte"/>
								                    </h:column>
								             </rich:suggestionbox>											
										</td> 

									</tr> 
									<!-- 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="dtDataVigencia">Data Vig�ncia:</label> 
										</td> 
										<td>
											<h:inputText value="#{fator.capitalSeguradoFaixaEtaria.dataVigencia}"  id="dtDataVigencia" maxlength="10" tabindex="2">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
									 -->										
								</table>	
						</fieldset>	
						
						
						<fieldset>
								<legend>Capital Segurado - Faixa Et�ria</legend>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="boxCapitais">Capital Segurado:</label> 
										</td> 
										<td> 
											<h:selectOneMenu  id="boxCapitais" binding="#{fatorHandler.capitalSegurado}" value="#{fatorHandler.idCapitalSeguradoFaixaEtaria}">
												<f:selectItems value="#{fatorHandler.listaCapitaisFaixaExtarias}" />
											</h:selectOneMenu>
										   									
										</td> 

									</tr> 
								</table>	
						</fieldset>	
					
							<fieldset>
								<legend>Ajuste de Valores</legend>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="txtFator">Fator(%):</label> 
										</td> 
										<td> 
										   <h:inputText value="#{fatorHandler.fator.fator}" id="txtFator" >
												<f:convertNumber/>
										   </h:inputText>
										  									
										</td> 

									</tr> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="dataInicioFator">Data de In�cio:</label> 
										</td> 
										<td>
											<h:inputText value="#{fatorHandler.fator.dataInicio}"  id="dataInicioFator" maxlength="10" >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>										
								</table>					
							</fieldset>							

							<br />
							
							<p:commandButton actionListener="#{fatorHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="3" />
							<p:commandButton actionListener="#{fatorHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
