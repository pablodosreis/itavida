<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="empresaHandler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery("#dtAniversario").mask("99/99/9999",{placeholder:"_"});
	jQuery('input').bestupper();
	exibirAjuste();  
}); 

function exibirAjuste() {	
	var exibir = jQuery("#cbAjusteAtivo").attr('checked');
	if(exibir) {
		jQuery("#fieldset_ajuste_01").show();
		jQuery("#fieldset_ajuste_02").show();
		jQuery("#fieldset_ajuste_03").show();
		jQuery("#proximaAtualizacao").show();
		jQuery("#atualizar_agora").show();
	} else {
		jQuery("#fieldset_ajuste_01").hide();
		jQuery("#fieldset_ajuste_02").hide();
		jQuery("#fieldset_ajuste_03").hide();
		jQuery("#proximaAtualizacao").hide();
		jQuery("#atualizar_agora").hide();
	}
}
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Ap�lices</li> </div>
						<div id="icone">
                           <img src="images/empresa.png" />
                        </div>
						<h:form id="myform" styleClass="niceform" prependId="false">
						

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados da Ap�lice</legend>
								<table>
									<tr>
										<td align="right">
										  <label for="txtNomeEmpresa">Nome:</label>
										</td>
										<td>
										 <h:inputText value="#{empresaHandler.empresa.nomeEmpresa}" id="txtNomeEmpresa" size="40" maxlength="50"/>
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="txtNumApolice">N�mero da Ap�lice:</label>
										</td>
										<td>
										 <h:inputText value="#{empresaHandler.empresa.numApolice}" id="txtNumAplice" size="15" maxlength="15"/>
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="dtAniversario">In�cio da �police:</label>
										</td>
										<td>
										 <h:inputText value="#{empresaHandler.empresa.aniversario}" id="dtAniversario" size="15" maxlength="15">
										 	<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />
										 </h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="cbAjusteAtivo">Reajustar pela Ap�lice:</label>
										</td>
										<td>
										 <h:selectBooleanCheckbox value="#{empresaHandler.empresa.ajusteAtivo}" id="cbAjusteAtivo" onchange="exibirAjuste()"/>
										</td>
									</tr>
									<tr id="proximaAtualizacao">
										<td align="right">
										  <label for="txtProximaAtualizacao">Pr�ximo Anivers�rio:</label>
										</td>
										<td>
										 <h:outputLabel value="#{empresaHandler.proximaAtualizacao}" id="txtProximaAtualizacao"/>
										</td>
									</tr>
								</table>					
							</fieldset>
							
							<fieldset id="fieldset_ajuste_01">
								<legend>Ajuste por idade</legend>
								<table>
									<tr>
										<td align="right">
										  <label for="vl14_65">14 a 65 anos (%):</label>
										</td>
										<td>
										 <h:inputText value="#{empresaHandler.empresa.ajuste14a65}" id="vl14_65" size="10" maxlength="50">
										 	<f:converter converterId="MyDoubleConverter"/>
										 </h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="vl66_mais">66 anos ou mais (%):</label>
										</td>
										<td>
										 <h:inputText value="#{empresaHandler.empresa.ajuste66mais}" id="vl66_mais" size="10" maxlength="50">
										 	<f:converter converterId="MyDoubleConverter"/>
										 </h:inputText>
										</td>
									</tr>							
								</table>
							</fieldset>
							
							<fieldset id="fieldset_ajuste_02">
								<legend>IPCA do Per�odo</legend>
								<table>
									<tr>
										<td align="right">
										  <label for="vlIPCA">IPCA Acumulado (%):</label>
										</td>
										<td>
										 <h:inputText value="#{empresaHandler.empresa.ipcaUltimoPeriodo}" id="vlIPCA" size="10" maxlength="12">
										 	<f:converter converterId="MyDoubleConverter"/>
										 </h:inputText>
										</td>
									</tr>							
								</table>
							</fieldset>
							
							<fieldset id="fieldset_ajuste_03">
								<legend>Hist�rico de Reajustes</legend>
									<p:dataTable value="#{empresaHandler.empresa.listaHistoricoAjuste}" 
												 var="historico" width="50%" 
												 rendered="#{not empty empresaHandler.empresa.listaHistoricoAjuste}" 
												 paginator="true" rows="20" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" >
											<f:facet name="header">
											<h:outputText value="Data do Ajuste" /> 
											</f:facet>
											<h:outputText value="#{historico.dataAjuste}" style="text-transform:uppercase;">
											  	<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" locale="pt_BR" />
											</h:outputText>									  										
										</p:column>
										
										<p:column resizable="true" >
											<f:facet name="header">
											<h:outputText value="Usu�rio" /> 
											</f:facet>
											<h:outputText value="#{historico.nomeUsuario}" style="text-transform:uppercase;"/>									  										
										</p:column>
											
										<p:column resizable="true" >
											<f:facet name="header">
											<h:outputText value="IPCA (%)" /> 
											</f:facet>
											<h:outputText value="#{historico.ajusteIpca}" style="text-transform:uppercase;"/>									  										
										</p:column>
										
										<p:column resizable="true" >
											<f:facet name="header">
											<h:outputText value="14 a 65 anos (%)" /> 
											</f:facet>
											<h:outputText value="#{historico.ajuste14a65}" style="text-transform:uppercase;"/>									  										
										</p:column>		
										
										<p:column resizable="true" >
											<f:facet name="header">
											<h:outputText value="66 anos ou mais (%)" /> 
											</f:facet>
											<h:outputText value="#{historico.ajuste66mais}" style="text-transform:uppercase;"/>									  										
										</p:column>		
												
									</p:dataTable>
							</fieldset>
							<br />
							
							<p:commandButton actionListener="#{empresaHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="3" />
							<p:commandButton actionListener="#{empresaHandler.atualizar}" value="Atualizar Agora" id="atualizar_agora" styleClass="button" async="false" ajax="false" tabindex="3" />
							<p:commandButton actionListener="#{empresaHandler.cancelar}" value="Limpar Tela" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
