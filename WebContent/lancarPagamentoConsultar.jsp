<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="pagamentoHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery("#dtDataCancelamento").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtDataDocumento").mask("99/99/9999",{placeholder:"_"});
	jQuery('#txtValorDocumento').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
	jQuery('#txtValorSubTotal').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	jQuery('#txtValor').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	   
	jQuery('.monetario').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	

	});

</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />

<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Financeiro</span><li>Consultar Lan�amento de Pagamentos</li> </div>
						<div id="icone">
                           <img src="images/financeiroLancarPagamentos.png" />
                        </div>


				<h:form styleClass="niceform" prependId="false">
				<p:focus/>

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
										<tr> 
											<td align="right" width="150px;"> 

											   <label for="cmbConta">Conta:</label> 
											</td> 
											<td align="right">
											   <h:selectOneMenu id="cmbConta" binding="#{pagamentoHandler.tipoContaSelecionado}" value="#{pagamentoHandler.pagamento.conta.id}" tabindex="1">
											   	<f:selectItems value="#{pagamentoHandler.tipoContaCombo}"/>
											   </h:selectOneMenu>
											</td> 
											<td align="right" width="150px;"> 
											   <label for="cmbTipoDocumento">Tipo do Documento</label> 
											</td> 
											<td align="right">
											   <h:selectOneMenu id="cmbTipoDocumento" binding="#{pagamentoHandler.tipoDocumentoSelecionado}" value="#{pagamentoHandler.pagamento.tipoDocumento.id}" tabindex="2">
											   	<f:selectItems value="#{pagamentoHandler.tipoDocumentosCombo}"/>
											   </h:selectOneMenu>
											</td> 
	
										</tr> 
									</table>

									<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroDocumento">N�mero do Cheque/Documento:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{pagamentoHandler.pagamentoFiltro.numeroDocumento}" id="txtNumeroDocumento" size="15" maxlength="15" tabindex="3"/>
										</td> 
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtNominal">Nominal a:</label>
										</td>
										<td>
										   <h:inputText value="#{pagamentoHandler.pagamentoFiltro.nominal}" id="txtNominal" size="40" maxlength="60" tabindex="4"/>
										</td>											
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataDocumento" >Data do Documento:</label>
										</td>
										<td>
											<h:inputText value="#{pagamentoHandler.pagamentoFiltro.dataDocumento}"  id="dtDataDocumento" maxlength="10"  tabindex="5">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtValorDocumento">Valor do Documento:</label>
										</td>
										<td>
										   <h:inputText value="#{pagamentoHandler.pagamentoFiltro.valor}" id="txtValorDocumento" size="15" maxlength="15" tabindex="6">
													<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>
										</td>											
									</tr>
								</table>
<table>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtFilial">Filial:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.filial.nomeFilial}" id="txtFilial" size="40" tabindex="7"/>
											
											<rich:suggestionbox id="suggestionMotivoSinistro" 
								                	for="txtFilial"
								                    suggestionAction="#{pagamentoHandler.filialAutoComplete}" 
								                    var="filial"
								                    fetchValue="#{filial.nomeFilial}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{filial}" 
														target="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.filial}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Nome da Filial" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.nomeFilial}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{filial.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtDespesa">Despesa:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.despesa.nomeDespesa}" id="txtDespesa" size="40" tabindex="8"/>
											
											<rich:suggestionbox id="suggestionDespesa" 
								                	for="txtDespesa"
								                    suggestionAction="#{pagamentoHandler.despesaAutoComplete}" 
								                    var="despesa"
								                    fetchValue="#{despesa.nomeDespesa}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{despesa}" 
														target="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.despesa}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Nome da Despesa" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{despesa.nomeDespesa}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Despesa Fixa" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{despesa.tipoDespesa == 'S' ? 'SIM' : 'N�O'}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtGrupoDespesa">Grupo de Despesa:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.grupoDespesa.nomeGrupoDespesa}" id="txtGrupoDespesa" size="40" tabindex="9"/>
											
											<rich:suggestionbox id="suggestionGrupoDespesa" 
								                	for="txtGrupoDespesa"
								                    suggestionAction="#{pagamentoHandler.grupoDespesaAutoComplete}" 
								                    var="grupoDespesa"
								                    fetchValue="#{grupoDespesa.nomeGrupoDespesa}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{grupoDespesa}" 
														target="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.grupoDespesa}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
										  				<h:outputText value="Nome do Grupo Despesa" /> 
										  	
										  			</f:facet>
										  					<h:outputText value="#{grupoDespesa.nomeGrupoDespesa}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtFuncionario">Funcion�rio:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.funcionario.nome}" id="txtFuncionario" size="40" tabindex="10"/>
											
											<rich:suggestionbox id="suggestionFuncionario" 
								                	for="txtFuncionario"
								                    suggestionAction="#{pagamentoHandler.funcionarioAutoComplete}" 
								                    var="funcionario"
								                    fetchValue="#{funcionario.nome}"
								                    width="500"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{funcionario}" 
														target="#{pagamentoHandler.pagamentoFiltro.detalhePagamentoFiltro.funcionario}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Funcion�rio" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.nome}" styleClass="autocompleteFonte"/>									  										
													</h:column>	
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Cidade" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.nom_cidade}" styleClass="autocompleteFonte"/>									  										
													</h:column>		
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="UF" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{funcionario.cidade.cod_estado.sgl_estado}" styleClass="autocompleteFonte"/>									  										
													</h:column>
								         	</rich:suggestionbox>
										</td> 
									</tr>
								</table>
							</fieldset>	
					
							<br />
							<h:commandButton actionListener="#{pagamentoHandler.pesquisaPagamentos}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="11" />
							<p:commandButton actionListener="#{pagamentoHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="12">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
<p:dialog  visible="true" header="Resultado da Consulta" rendered="#{not empty pagamentoHandler.resultadoPesquisa}" constrainToViewport="true" fixedCenter="true" underlay="shadow"> 
									<p:dataTable id="resultTable" value="#{pagamentoHandler.resultadoPesquisa}" 
												binding="#{pagamentoHandler.dataTable}"
												 var="item" width="100%" 
												 rendered="#{not empty pagamentoHandler.resultadoPesquisa}" 
												dynamic="true"
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" sortBy="#{item.conta.descricao}" >
										  <f:facet name="header">
										  <h:outputText value="Conta" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.conta.descricao}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.tipoDocumento.descricao}" >
										  <f:facet name="header">
										  <h:outputText value="Tipo Documento" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.tipoDocumento.descricao}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.numeroDocumento}" >
										  <f:facet name="header">
										  <h:outputText value="N�mero do Documento" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.numeroDocumento}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.dataDocumento}"  parser="date" >
										  <f:facet name="header">
										  <h:outputText value="Data" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataDocumento}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.valor}" >
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.valor}" styleClass="monetario"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>		
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  action="#{pagamentoHandler.preparaAlterarPagamento}">
										  	<p:graphicImage title="Alterar" alt="Alterar" value="images/edit.png"  />
										  	<f:setPropertyActionListener target="#{pagamentoHandler.pagamento}" value="#{item}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
 										<h:commandLink actionListener="#{pagamentoHandler.excluirPagamento}"  onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))" >
											<p:graphicImage url="images/excluir.png" title="Excluir" alt="Excluir"  >										  	
										  		<f:param id="excluirPagamento" name="id" value="#{item}"/>
											</p:graphicImage>	
										</h:commandLink>									  								  									
										</p:column>																																																
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="LancamentoPagamentos"  postProcessor="#{pagamentoHandler.postProcessXLS}" excludeColumns="5,6"/>  
										</h:commandLink>
								</div>
						</p:dialog>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
