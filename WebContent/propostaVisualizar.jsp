<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="propostaHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>

jQuery(function(jQuery){

	
	   jQuery('#cmbEmpresa').focus();
	   jQuery('input').bestupper();
	   jQuery("#dtNascimentoSegurado").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoConjuge").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAprovacao").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCadastro").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCancelamento").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtContemplacaoSorteio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAgenciamento").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#txtCpf").mask("999.999.999-99",{placeholder:"_"});
	   jQuery("#txtCPFConjuge").mask("999.999.999-99",{placeholder:"_"});
	   jQuery("#txtCep").mask("99.999-999",{placeholder:"_"});
	   jQuery("#txtTelefoneResidencial").mask("(99)9999-9999",{placeholder:"_"});
	   jQuery("#txtTelefoneComercial").mask("(99)9999-9999",{placeholder:"_"});
		jQuery("#txtTelefoneCelular").mask("(99)9?9999-9999",{placeholder:"_"});
		jQuery("#txtTelefoneCelular2").mask("(99)9?9999-9999",{placeholder:"_"});
		
	   
	});

function formataMonetario(){
		jQuery('#txtCapital1').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.',
		    centsLimit: 2
		});
		jQuery('#txtCapital2').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.',
		    centsLimit: 2
		});
		return false;
}


</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Gest�o</span><li>Visualiza��o da Proposta: <h:outputText value="#{propostaHandler.numeroPropostaIncluir}"/> </li> </div>
						<div id="icone">
                           <img src="images/cadProposta.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false" >


							<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Tipo de Proposta</legend>
								
								<table >
									<tr>
										<td align="right" width="150px;">
										  <label for="txtIdProposta">N�mero da Proposta:</label>
										</td>
										<td>
										   <h:outputText value="#{propostaHandler.numeroPropostaIncluir}" id="txtIdProposta" />
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="cmbEmpresa">Empresa:</label>
										</td>
										<td>
										   <h:selectOneMenu id="cmbEmpresa" value="#{propostaHandler.proposta.empresa.id}"  disabled="true">
										   		<f:selectItems value="#{propostaHandler.opcoesEmpresaCombo}" />
										   </h:selectOneMenu>
										</td>								
									</tr>																		
								</table>
								
								<h:panelGroup id="planodivsuperap" styleClass="agrupamento_Dados" rendered="#{propostaHandler.proposta.superAp == null}">
									<table>
										<tr> 
											<td align="right" width="150px;"> 
											   <label>Plano:</label> 
											</td> 
											<td>
												
												<h:outputText value="#{propostaHandler.proposta.superAp.plano.descricao}" id="txtPlanoSuperAp" />
											</td>
										</tr>
										<h:panelGroup id="cobertura1div" styleClass="agrupamento_Dados" rendered="#{propostaHandler.proposta.superAp.coberturaOpcional1 != null}">
											<tr> 
												<td align="right" width="150px;"> 
												   <label>Cobertura 1:</label> 
												</td> 
												<td>
													<h:outputText value="#{propostaHandler.proposta.superAp.coberturaOpcional1.descricao}" id="txCoberturaOpcional1" />
												</td>
											</tr>
										</h:panelGroup>
										<h:panelGroup id="cobertura2div" styleClass="agrupamento_Dados" rendered="#{propostaHandler.proposta.superAp.coberturaOpcional2 != null}">
											<tr> 
												<td align="right" width="150px;"> 
												   <label>Cobertura 2:</label> 
												</td> 
												<td>
													<h:outputText value="#{propostaHandler.proposta.superAp.coberturaOpcional2.descricao}" id="txCoberturaOpcional2" />
												</td>
											</tr>
										</h:panelGroup>
									</table>
								</h:panelGroup>
								
								<h:panelGroup id="agrupamento_modelo" styleClass="agrupamento_Dados" rendered="#{propostaHandler.proposta.superAp == null}">
									<table>
										<tr> 
											<td align="right" width="150px;"> 
											   <label for="cmbModeloProposta" >Modelo de Proposta:</label> 
											</td> 
											<td>
											   <h:selectOneMenu id="cmbModeloProposta" value="#{propostaHandler.proposta.modeloProposta.id}" disabled="true" rendered="#{propostaHandler.proposta.superAp == null}">
											   	<f:selectItems id="selectItensModeloProposta" value="#{propostaHandler.modeloPropostaCombo}" />
											   </h:selectOneMenu>
											</td> 
											<td align="right" width="150px;"> 
											   <label for="cmbTipoProposta">Tipo de Proposta:</label> 
											</td> 
											<td>
											   <h:selectOneMenu id="cmbTipoProposta" value="#{propostaHandler.proposta.tipoProposta.id}" disabled="true" rendered="#{propostaHandler.proposta.superAp == null}">
											   	<f:selectItems id="selectItensTipoProposta" value="#{propostaHandler.tipoPropostaComboFiltro}" />
											   </h:selectOneMenu>
											</td> 
										</tr>  
									</table>
									<table> 
										<tr> 
											<td align="right" width="150px;"> 
											  <label for="txtOrgao">�rg�o:</label> 
											</td> 
											<td> 
											   <h:outputText value="#{propostaHandler.proposta.orgao.nomeOrgao}" id="txtOrgao" />
											</td> 
										</tr> 
									</table>  
								</h:panelGroup>
								<table>	
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNomeTabela">Tabela de Capitais:</label> 
										</td> 
										<td> 
										   <h:outputText value="#{propostaHandler.proposta.nomeTabela.nomeTabela}" id="txtNomeTabela" rendered="#{propostaHandler.proposta.nomeTabela != null}" />
										</td> 
									</tr>
									
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtLotacao">Lota��o:</label> 
										</td> 
										<td> 
										   <h:outputText value="#{propostaHandler.proposta.lotacao}" id="txtlotacao" rendered="#{propostaHandler.proposta.lotacao != null}" />
										</td> 
									</tr> 
									
									
								</table>								
							</fieldset>
							<!--  hist�rico tipo da proposta-->
							<jsp:include page="proposta/historicoTipoProposta.jsp" />	
							<fieldset>
									<legend>Dados do Segurado Principal</legend>
									
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoPrincipal">Nome:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.nomeSegurado}" id="txtNomeSeguradoPrincipal" />
											</td>
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtEmail">Email:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.email}" id="txtEmail" />
											</td>
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;"> 
											   <label for="cmbSexoSegurado">Sexo:</label> 
											</td> 
											<td>
											   <h:selectOneMenu id="cmbSexoSegurado" value="#{propostaHandler.proposta.sexoSegurado}" disabled="true">
											   		<f:selectItems value="#{propostaHandler.sexoCombo}" />
											   </h:selectOneMenu>
											</td> 
											<td align="right" width="150px;">
												  <label for="dtNascimentoSegurado" >Data Nascimento:</label>
											</td>
											<td>
												<h:outputText value="#{propostaHandler.proposta.dataNascimentoSegurado}"  id="dtNascimentoSegurado" >
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:outputText>
											</td>
											<td>
												 <h:outputText id="txtIdadeSegurado" value="#{propostaHandler.idadeSegurado}" />
											</td>

										</tr>
									</table>

									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtMatricula">Matr�cula:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.matriculaSegurado}" id="txtMatricula" />
											</td>
											<td align="right" width="110px;">
											  <label for="txtCpf">CPF:</label>
											</td>
											<td>

											   <h:outputText value="#{propostaHandler.proposta.cpfSegurado}" id="txtCpf" />
											</td>								
										</tr>									
									</table>
									<table>
										<tr>	
											<td align="right" width="150px;">
											  <label for="txtRGSegurado">RG:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.rgSegurado}" id="txtRGSegurado" />
											</td>									
											<td align="right" width="150px;"> 
											   <label for="cmbEstadoCivilSegurado">Estado Civil:</label> 
											</td> 
											<td>
											   <h:selectOneMenu id="cmbEstadoCivilSegurado" value="#{propostaHandler.proposta.estadoCivilSegurado}"  disabled="true" >
											   		<f:selectItems value="#{propostaHandler.estadoCivilCombo}" />
											   </h:selectOneMenu>
											</td> 											
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtProfissaoSegurado">Profiss�o:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.profissaoSegurado}" id="txtProfissaoSegurado" />
											</td>
										</tr>
									</table>	
									<table> 
										<tr> 
											<td align="right" width="150px;"> 
											  <label for="txtCapital" style="font-weight: bold;">Capital:</label> 
											</td> 
											<td>
												<h:outputText value="#{propostaHandler.proposta.capitalSegurado}" id="txtCapital" 
													style="font-size: 14px;font-weight: bold;"><f:convertNumber currencySymbol="R$" 
													type="currency" groupingUsed="true"/>
												</h:outputText>
											</td>
										</tr>
									</table>
									<table>
										<tr> 
											<td align="right" width="150px;">	
												<label for="txtPremioSegurado" style="font-weight: bold;">Pr�mio</label>
											</td>
											<td>	
	  										    <h:outputText value="#{propostaHandler.proposta.premioSegurado}" id="txtPremioSegurado" style="font-size: 14px;font-weight: bold;"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
	  										 </td>
										</tr> 
									</table> 								
								</fieldset>
								<jsp:include page="proposta/historicoSeguradoPrincipal.jsp" />

								<!-- SEGURADO CONJUGE -->
								<fieldset>
										<legend>Dados do Segurado C�njuge</legend>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoConjuge">Nome:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.nomeConjuge}" id="txtNomeSeguradoConjuge"  />
											</td>
										</tr>
									</table>
									<table>
										<tr>
										<td align="right" width="150px;"> 
										   <label for="cmbSexoConjuge">Sexo:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbSexoConjuge" value="#{propostaHandler.proposta.sexoConjuge}" disabled="true">
										   		<f:selectItems value="#{propostaHandler.sexoCombo}" />
										   </h:selectOneMenu>
										</td> 
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="dtNascimentoConjuge" >Data Nascimento:</label>
											</td>
											<td>
												<h:outputText value="#{propostaHandler.proposta.dataNascimentoConjuge}"  id="dtNascimentoConjuge" >
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:outputText>
											</td>
											<td>
												 <h:outputText id="txtIdadeConjuge" value="#{propostaHandler.idadeConjuge}" />
											</td>
										</tr>	
									</table>
									<table>								
										<tr>
											<td align="right" width="150px;">
											  <label for="txtMatriculaConjuge">Matr�cula:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.matriculaConjuge}" id="txtMatriculaConjuge" />
											</td>
										</tr>
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtCPFConjuge">CPF:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.cpfConjuge}" id="txtCPFConjuge" />
											</td>								
										</tr>		
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtRGConjuge">RG:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.rgConjuge}" id="txtRGConjuge" />
											</td>								
										</tr>
									</table>
									<table>
										<tr>										
										<td align="right" width="150px;"> 
										   <label for="cmbEstadoCivilConjuge">Estado Civil:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbEstadoCivilConjuge" value="#{propostaHandler.proposta.estadoCivilConjuge}" disabled="true">
										   		<f:selectItems value="#{propostaHandler.estadoCivilCombo}" />
										   </h:selectOneMenu>
										</td> 											
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtProfissaoConjuge">Profiss�o:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.profissaoConjuge}" id="txtProfissaoConjuge" />
											</td>
										</tr>
									</table>
									<table>
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtCapitalConjuge" style="font-weight: bold;">Capital:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.capitalConjuge}" id="txtCapitalConjuge" style="font-size: 14px;font-weight: bold;" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
											</td>
										</tr>
									</table>
									<table>
										<tr>		
											<td align="right" width="150px;">
											  <label for="txtPremioConjuge" style="font-weight: bold;">Pr�mio:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.premioConjuge}" id="txtPremioConjuge" style="font-size: 14px;font-weight: bold;" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
											</td>	
										</tr>
									</table>
								</fieldset>	

								<fieldset>
										<legend>Total dos Pr�mios</legend>
									<b>
									<table>
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtTotalPremios">Total dos Pr�mios:</label>
											</td>
											<td>
											   <h:outputText value="#{propostaHandler.proposta.totalPremios}" id="txtTotalPremios" style="font-size: 14px;font-weight: bold;" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
											</td>	
										</tr>
									</table>		
									</b>				
								</fieldset>
								<jsp:include page="proposta/historicoConjuge.jsp" />

								<!-- ENDERE�O -->								
								<fieldset>	
									<legend>Endere�o</legend>
										<table>
											<tr>
												<td align="right" width="100px;">
												  <label for="txtEndereco">Endere�o:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.enderecoSegurado}" id="txtEndereco" />
												</td>								
											</tr>
											<tr>
												<td align="right" width="100px;">
												  <label for="txtNumero">N�mero:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.numeroEnderecoSegurado}" id="txtNumero" />
												</td>								
											</tr>
											<tr>
												<td align="right" width="100px;">
												  <label for="txtComplemento">Complemento:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.complemento}" id="txtComplemento" />
												</td>								
											</tr>
										</table>
										<table>
											<tr>
												<td align="right" width="100px;">
												  <label for="txtBairro">Bairro:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.bairro}" id="txtBairro" />
												</td>								
											</tr>									
										</table>	
										<table>
										<tr> 
											<td align="right" width="100px;"> 
											  <label for="txtNomeCidade">Cidade:</label> 
											</td> 
											<td> 
											   <h:outputText value="#{propostaHandler.proposta.cidade.nom_cidade}" id="txtNomeCidade" />
											   <h:outputText value="#{propostaHandler.proposta.cidade.cod_estado.sgl_estado}" id="txtUf"  />
											</td> 
										</tr> 
										</table>	
										<table>
											<tr>
												<td align="right" width="100px;">
												  <label for="txtCep">CEP:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.cep}" id="txtCep" />
												</td>								
											</tr>									
										</table>
										<table>
											<tr>
												<td align="right" width="100px;">
												  <label for="txtTelefoneResidencial">Telefone Residencial:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.telefoneResidencial}" id="txtTelefoneResidencial" />
												</td>	
												<td align="right" width="100px;">
												  <label for="txtTelefoneComercial">Telefone Comercial:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.telefoneComercial}" id="txtTelefoneComercial" />
												</td>
											</tr>		
											<tr>
												<td align="right" width="100px;">
												  <label for="txtTelefoneCelular">Telefone Celular:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.telefoneCelular}" id="txtTelefoneCelular" />
												</td>	
												<td align="right" width="100px;">
												  <label for="txtTelefoneCelular2">Telefone Celular 2:</label>
												</td>
												<td>
												   <h:outputText value="#{propostaHandler.proposta.telefoneCelular2}" id="txtTelefoneCelular2" />
												</td>
											</tr>
										</table>
										<table>
											<tr>
												<td align="right" width="100px;">
												  <label for="txtObservacao">Observa��es:</label>
												</td>
												<td>
												   <h:inputTextarea value="#{propostaHandler.proposta.observacao}" id="txtObservacao" cols="50" rows="5" disabled="true" />
												</td>								
											</tr>									
										</table>
								</fieldset>
								<jsp:include page="proposta/historicoEndereco.jsp" />
								
								<!--  Dados dos benefici�rios-->	
								<fieldset>	
									<legend>Benefici�rios</legend>
									<p:dataTable value="#{propostaHandler.proposta.beneficiario_collection}" 
												 var="beneficiario" width="50%" 
												 rendered="#{not empty propostaHandler.proposta.beneficiario_collection}" 
												 >
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Nome do Benefici�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{beneficiario.nomeBeneficiario}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Parentesco" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{beneficiario.grauParentesco.descricao}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Percentual" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{beneficiario.percentual}"/>									  										
										</p:column>
									</p:dataTable>
								</fieldset>
								<jsp:include page="proposta/historicoBeneficiarios.jsp" />
								
								<fieldset>	
									<legend>Agenciador</legend>
					
									<p:dataTable value="#{propostaHandler.proposta.propostaagenciador_collection}" 
												 var="agenciador" width="50%" 
												 rendered="#{not empty propostaHandler.proposta.propostaagenciador_collection}" 
												 >
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Nome do Agenciador" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.funcionario.nome}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Percentual" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.funcionario.percentualComissao}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Comiss�o Adicional" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.percentual}"/>									  										
										</p:column>
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Valor Definido" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.valorPagoDefinido}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>
									</p:dataTable>
								</fieldset>
								<fieldset>
									<legend>Aprova��o</legend>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="dtDataAgenciamento" >Data Agenciamento:</label>
												</td>
												<td>
													<h:outputText value="#{propostaHandler.proposta.dataAgenciamento}"  id="dtDataAgenciamento" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;">
												  <label for="dtDataAprovacao" >Data Aprova��o:</label>
												</td>
												<td>
													<h:outputText value="#{propostaHandler.proposta.dataAprovacao}"  id="dtDataAprovacao" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>									
											<tr>
												<td align="right" width="150px;">
												  <label for="dtDataCadastro" >Data do Cadastro:</label>
												</td>
												<td>
													<h:outputText value="#{propostaHandler.proposta.dataCadastro}"  id="dtDataCadastro" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>	
										</table>	
							</fieldset>
							<fieldset>
									<legend>Informa��es</legend>
										<table>
											<tr> 
												<td align="right" width="150px;"> 
												  <label for="txtNumPropostaReadOnly">N�mero da Proposta:</label> 
												</td> 
												<td> 
												   <h:outputText value="#{propostaHandler.proposta.id}" id="txtNumPropostaReadOnly" />
												</td> 
											</tr> 
										</table>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="txtNumeroSorteioA">N�mero do Sorteio:</label>
												</td>
												<td>
													<h:outputText value="#{propostaHandler.numeroSorteio}" />
													
												</td>								
											</tr>																		
										</table>
								</fieldset>
								<fieldset>
									<legend>Sorteio</legend>
										Segurado Contemplado com Sorteio em:
										<table>
											<tr>
												<td>
													<h:outputText value="#{propostaHandler.proposta.dataContemplacao}"  id="dtContemplacaoSorteio" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>											
										</table>
								</fieldset>
								<fieldset>
									<legend>Cancelamento</legend>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="dtDataCancelamento" >Data Cancelamento:</label>
												</td>
												<td>
													<h:outputText value="#{propostaHandler.proposta.dataCancelamento}"  id="dtDataCancelamento" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>											
										</table>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="txtMotivoCancelamento">Motivo do Cancelamento:</label>
												</td>
												<td>
												   <h:selectOneMenu id="cmbMotivoCancelamento" value="#{propostaHandler.proposta.motivoCancelamento.id}" disabled="true">
												   		<f:selectItems value="#{propostaHandler.opcoesMotivoCancelamento}" />
												   </h:selectOneMenu>
												</td>								
											</tr>																		
										</table>
									</fieldset>	
								
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>
</BODY>

</HTML>
</f:view>
