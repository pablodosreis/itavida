<%@ include file="common/cabecalho.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

	<a4j:keepAlive beanName="ImportarNovosValoresHandler" />
	<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<style>
.top {
	vertical-align: top;
}

.info {
	height: 202px;
	overflow: auto;
}
</style>
<script type="text/javascript">
	jQuery(function(jQuery) {
		jQuery("#periodoImportacao").mask("99/99/9999", {
			placeholder : "_"
		});
	});
</script>
<BODY class=f-default>

	<DIV id=background_2>
		<DIV id=site_wrapper>
			<jsp:include page="common/menu.jsp" />
			<DIV id=wrap>
				<DIV id=content>
					<DIV id=main>
						<DIV id=main_content>
							<DIV id=main_body>

								<div id="breadcrumb">
									<span>Arquivos </span>
									<li>Importa��o de Novos Valores</li>
								</div>
								<div id="icone">
									<img src="images/historicoSinistros.png" />
								</div>

								<h:form prependId="false">
									<div class="titulo_agrupamento_Dados"></div>
									<fieldset>
										<legend>Dados</legend>
										<br />

										<table>
											<tr>
												<td align="right"><label for="periodoImportacao"
													style="margin-right: 5px">Atualizar propostas
														averbadas at� o dia: </label></td>
												<td><h:inputText
														value="#{ImportarNovosValoresHandler.periodoImportacao}"
														id="periodoImportacao" maxlength="10" tabindex="6" /></td>
											</tr>
										</table>
										<br />
										<h:panelGrid columns="2" id="uploadGrid"
											columnClasses="top,top">
											<rich:fileUpload
												fileUploadListener="#{ImportarNovosValoresHandler.listener}"
												maxFilesQuantity="#{ImportarNovosValoresHandler.uploadsAvailable}"
												id="upload" ajaxSingle="false" immediateUpload="true"
												listHeight="80" listWidth="300"
												cancelEntryControlLabel="Cancelar"
												clearControlLabel="Remover" progressLabel="Progresso"
												doneLabel="Pronto!"
												sizeErrorLabel="Tamanho do arquivo maior que o permitido."
												stopControlLabel="Parar"
												transferErrorLabel="Erro ao tentar obter o arquivo!"
												stopEntryControlLabel="Cancelar"
												addControlLabel="Selecionar Arquivo"
												clearAllControlLabel="Remover todos" acceptedTypes="xls"
												allowFlash="false">
												<a4j:support event="onuploadcomplete"
													reRender="info,uploadGrid" ajaxSingle="false"
													oncomplete="document.forms[0].submit();" />
												<a4j:support event="onclear" reRender="info,uploadGrid"
													ajaxSingle="false" />
											</rich:fileUpload>
											<h:panelGroup id="info">
												<rich:panel bodyClass="info" style="height: 118px;">
													<f:facet name="header">
														<h:outputText value="Informa��es do Arquivo Selecionado" />
													</f:facet>
													<h:outputText value="Nenhum arquivo foi selecionado ainda!"
														rendered="#{ImportarNovosValoresHandler.size eq 0}" />
													<rich:dataGrid columns="1"
														value="#{ImportarNovosValoresHandler.files}" var="file"
														rowKeyVar="row">
														<rich:panel bodyClass="rich-laguna-panel-no-header">
															<h:panelGrid columns="2">

																<h:panelGrid columns="2">
																	<h:outputText value="Nome do Arquivo:" />
																	<h:outputText value="#{file.name}" />
																	<h:outputText value="Tamanho(bytes):" />
																	<h:outputText value="#{file.length}" />
																</h:panelGrid>
															</h:panelGrid>
														</rich:panel>
													</rich:dataGrid>
												</rich:panel>
												<rich:spacer height="3" />
												<br />
												<a4j:commandButton
													action="#{ImportarNovosValoresHandler.clearUploadData}"
													reRender="info, upload" value="Limpar dados Selecionados"
													styleClass="button"
													rendered="#{ImportarNovosValoresHandler.size > 0}" />
											</h:panelGroup>
										</h:panelGrid>
									</fieldset>
									<br />
									<h:commandButton
										action="#{ImportarNovosValoresHandler.importReport}"
										value="Realizar Importa��o" id="gerarExport"
										styleClass="button" tabindex="2" />
									<p:commandButton
										actionListener="#{ImportarNovosValoresHandler.limparDados}"
										value="Cancelar" id="cancelar" styleClass="button"
										async="false" ajax="false" tabindex="3">
										<p:confirmDialog message="Deseja Cancelar esta opera��o?"
											yesLabel="Sim" noLabel="N�o" header="Confirma��o!"
											fixedCenter="true" modal="true" />
									</p:commandButton>

								</h:form>
								
								
								<p:dataTable
												value="#{ImportarNovosValoresHandler.resultados}"
												var="item" width="50%"
												rendered="#{not empty ImportarNovosValoresHandler.resultados}"
												paginator="true" rows="10"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">

												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="data" />
													</f:facet>
													<h:outputText value="#{item.data}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="descricao" />

													</f:facet>
													<h:outputText
														value="#{item.conteudo}"  escape="false"/>
												</p:column>
											</p:dataTable>

							</DIV>
						</DIV>
						<DIV class=both>
							<!-- -->
						</DIV>
					</DIV>
					<DIV id=footer_top_bg>
						<!-- -->
					</DIV>
				</DIV>
			</DIV>
			<jsp:include page="common/footer.jsp" />
		</DIV>
		<DIV id=foo_glass></DIV>
		<BR>
		<BR>
	</DIV>

</BODY>

	</HTML>
</f:view>
