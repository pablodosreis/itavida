<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="historicoSinistroHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>

<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
		jQuery("#dtDataSinistro").mask("99/99/9999",{placeholder:"_"});
		jQuery("#dtDataEnvioSeguradora").mask("99/99/9999",{placeholder:"_"});
		jQuery("#dtDataPagamento").mask("99/99/9999",{placeholder:"_"});
		
		/*
		jQuery("#valorFuneral").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		
		jQuery("#txtValorSinistro").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#txtValorCestaBasica").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
		jQuery("#valorBeneficiario2").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario1").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});

		jQuery("#valorBeneficiario3").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario4").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario5").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario6").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario7").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario8").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario9").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
		jQuery("#valorBeneficiario10").priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});*/
		
	});
	
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Gest�o</span><li>Hist�rico de Sinistros - Consulta</li> </div>
						<div id="icone">
                           <img src="images/historicoSinistros.png" />
                        </div>
						<h:form styleClass="niceform" prependId="false">


					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<div class="divRecuadoDireito">
									<table>
										<tr>	
											<td>
												<h:selectBooleanCheckbox id="checkSegurado" value="#{historicoSinistroHandler.tipoSinistroSegurado}" />
											</td>										
											<td>
											   <label for="checkSegurado">Segurado Principal</label>												   
											</td>
										</tr>
										<tr>	
											<td>
											   	<h:selectBooleanCheckbox id="checkConjuge" value="#{historicoSinistroHandler.tipoSinistroConjuge}" />			   
											</td>										
											<td>
											   <label for="checkConjuge">Segurado C�njuge</label>												   
											</td>
										</tr>	
										<tr>	
											<td>
											   	<h:selectBooleanCheckbox id="checkAuxFuneral" value="#{historicoSinistroHandler.tipoSinistroAuxilioFuneral}" />				   
											</td>										
											<td>
											   <label for="checkAuxFuneral">Aux�lio Funeral</label>												   
											</td>
										</tr>											
									</table>	
								</div>	
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtNumeroProposta">N�mero da Proposta:</label>
										</td>
										<td>

  										<p:lightBox iframe="true" width="90%" height="80%">
											<h:outputLink value="#{facesContext.externalContext.requestContextPath}/propostaVisualizar.jsf?propostaVisualizar=#{historicoSinistroHandler.historicoSinistro.proposta.id}" title="Dados da Proposta #{historicoSinistroHandler.historicoSinistro.proposta.id}">    
											 <h:outputText value="#{historicoSinistroHandler.historicoSinistro.proposta.id}" id="txtNumeroProposta" />
											</h:outputLink>  
										</p:lightBox> 

										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="txtNomeSeguradoPrincipal">Segurado Principal:</label>
										</td>
										<td>
										 <h:outputText value="#{historicoSinistroHandler.historicoSinistro.proposta.nomeSegurado}" id="txtNomeSeguradoPrincipal" />
										</td>
									</tr>
									<tr>
										<td align="right">
										  <label for="txtNomeSeguradoConjuge">Segurado C�njuge:</label>
										</td>
										<td>
										 <h:outputText value="#{historicoSinistroHandler.historicoSinistro.proposta.nomeConjuge}" id="txtNomeSeguradoConjuge" />
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtNumeroSinistro">N�mero do Sinistro:</label>
										</td>
										<td>
										 <h:inputText value="#{historicoSinistroHandler.historicoSinistro.numeroSinistro}" id="txtNumeroSinistro" size="15" maxlength="15" />
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataSinistro" >Data do Sinistro:</label>
										</td>
										<td>
											<h:inputText value="#{historicoSinistroHandler.historicoSinistro.dataSinistro}"  id="dtDataSinistro" maxlength="10"  >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataEnvioSeguradora" >Data de Envio p/ Seguradora:</label>
										</td>
										<td>
											<h:inputText value="#{historicoSinistroHandler.historicoSinistro.dataEnvioSeguradora}"  id="dtDataEnvioSeguradora" maxlength="10"  >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>
								</table>
									<table>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtMotivoSinistro">Motivo Sinistro (Tipo Ocorr�ncia):</label> 
										</td> 
										<td> 
										   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.motivoSinistro.descricao}" id="txtMotivoSinistro" size="40" />
											<rich:suggestionbox id="suggestionMotivoSinistro" 
								                	for="txtMotivoSinistro"
								                    suggestionAction="#{historicoSinistroHandler.motivoSinistroAutoComplete}" 
								                    var="motivo"
								                    fetchValue="#{motivo.descricao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{motivo}" 
														target="#{historicoSinistroHandler.historicoSinistro.motivoSinistro}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Descri��o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivo.descricao}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Sinistro Negado" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivo.sinistroNegado == 'S' ? 'SIM' : 'N�O'}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
								         	</rich:suggestionbox>
										</td>
									</tr> 
								</table>	
								
								<table>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtValorSinistro">Valor do Sinistro:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.valorSinistro}"  id="txtValorSinistro" >
													  <f:converter converterId="MyDoubleConverter" />
												</h:inputText>
										</td>
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtValorCestaBasica">Valor da Cesta B�sica:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.valorCestaBasica}"  id="txtValorCestaBasica" >
													  <f:converter converterId="MyDoubleConverter" />
												</h:inputText>
										</td>
									</tr>  
								</table>
										<table>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 1:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b1.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario1"
															value="#{historicoSinistroHandler.b1.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 2:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b2.nomeBeneficiario}"
															maxlength="200" >
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario2"
															value="#{historicoSinistroHandler.b2.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 3:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b3.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario3"
															value="#{historicoSinistroHandler.b3.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 4:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b4.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario4"
															value="#{historicoSinistroHandler.b4.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 5:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b5.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario5"
															value="#{historicoSinistroHandler.b5.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 6:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b6.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario6"
															value="#{historicoSinistroHandler.b6.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 7:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b7.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario7"
															value="#{historicoSinistroHandler.b7.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 8:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b8.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario8"
															value="#{historicoSinistroHandler.b8.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 9:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b9.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario9"
															value="#{historicoSinistroHandler.b9.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												<tr>
													<td align="right" width="150px;"><label>Nome
															do Benefici�rio 10:</label></td>
													<td><h:inputText value="#{historicoSinistroHandler.b10.nomeBeneficiario}"
															maxlength="200">
														</h:inputText></td>
													<td align="right" width="150px;"><label>Valor Recebido:</label></td>
													<td><h:inputText id="valorBeneficiario10"
															value="#{historicoSinistroHandler.b10.valorRecebido}">
															<f:converter converterId="MyDoubleConverter" />
														</h:inputText></td>
												</tr>
												
										</table>


									</fieldset>							
							<fieldset style="width:600px">
									<legend>Pagamento Funeral</legend>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="valorFuneral" >Valor do Funeral:</label>
											</td>
											<td>
												<h:inputText value="#{historicoSinistroHandler.historicoSinistro.valorFuneral}"  id="valorFuneral"  >
													  <f:converter converterId="MyDoubleConverter" />
												</h:inputText>
											</td>
										</tr>
									</table>
									<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="dtDataPagamento" >Data de Pagamento:</label>
											</td>
											<td>
												<h:inputText value="#{historicoSinistroHandler.historicoSinistro.dataPagamento}"  id="dtDataPagamento" maxlength="10"  >
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:inputText>
											</td>
										</tr>
									</table>
									<table> 
									<tr> 
										<td align="right" width="150px;"> 
										   <label for="cmbTipoPagamento">Tipo de Pagamento:</label> 
										</td> 
										<td>
										   <h:selectOneMenu id="cmbTipoPagamento"  value="#{historicoSinistroHandler.historicoSinistro.tipoPagamento}" >
										   	<f:selectItems value="#{historicoSinistroHandler.tipoPagamentoCombo}"/>
										   </h:selectOneMenu>
										</td> 

									</tr> 
								</table>
							</fieldset>
							<fieldset style="width:600px">
									<legend>Sinistro Negado</legend>
									<table>
										<tr> 
											<td align="right" width="150px;"> 
											  <label for="txtMotivoSinistroNegado">Motivo Sinistro Negado:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{historicoSinistroHandler.historicoSinistro.motivoSinistroNegado.descricao}" id="txtMotivoSinistroNegado" size="30" />
												
											<rich:suggestionbox id="suggestionMotivoSinistroNegado" 
								                	for="txtMotivoSinistroNegado"
								                    suggestionAction="#{historicoSinistroHandler.motivoSinistroNegadoAutoComplete}" 
								                    var="motivoNegado"
								                    fetchValue="#{motivoNegado.descricao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{motivoNegado}" 
														target="#{historicoSinistroHandler.historicoSinistro.motivoSinistroNegado}"/> 
													</a4j:support> 
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Descri��o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivoNegado.descricao}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
													<h:column>
													  <f:facet name="header">
													  <h:outputText value="Sinistro Negado" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{motivoNegado.sinistroNegado == 'S' ? 'SIM' : 'N�O'}" styleClass="autocompleteFonte"/>									  										
													 </h:column>
								         	</rich:suggestionbox>
										</td> 
										</tr> 
									</table>
							</fieldset>
							<table>
								<tr>										
								<td align="right" width="150px;"> 
								   <label for="cmbSeguradora">Seguradora:</label> 
								</td> 
								<td>
								   <h:selectOneMenu id="cmbSeguradora" value="#{historicoSinistroHandler.historicoSinistro.seguradora}" >
								   		<f:selectItems value="#{historicoSinistroHandler.seguradoraCombo}" />
								   </h:selectOneMenu>
								</td> 											
								</tr>
							</table>
							<table>
								<tr>
									<td width="800px;" height="150px;"  >
								<p:editor id="editorSituacaoProcesso" width="800px;" height="150px;" title="Situa��o do Processo"  language="pt" value="#{historicoSinistroHandler.situacaoProcesso}" />
									</td>
								</tr>
								<tr>
									<td width="800px;" height="150px;" >
									<p:editor id="editorObservacao"  width="800px;" height="150px;" title="Observa��o" language="pt"  value="#{historicoSinistroHandler.observacao}" />
									</td>
								</tr>
							</table>
							<p:commandButton actionListener="#{historicoSinistroHandler.gravar}" value="Salvar" id="Gravar" styleClass="button"  async="false"  ajax="false" />
							<p:commandButton action="#{historicoSinistroHandler.cancelarInclusao}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" >
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
