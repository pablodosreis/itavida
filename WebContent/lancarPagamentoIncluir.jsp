<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="pagamentoHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery("#dtDataCancelamento").mask("99/99/9999",{placeholder:"_"});
	jQuery("#dtDataDocumento").mask("99/99/9999",{placeholder:"_"});
	jQuery('#txtValorDocumento').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
	jQuery('#txtValorSubTotal').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	jQuery('#txtValor').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	   
	jQuery('.monetario').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	

	});

</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />


<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Financeiro</span><li>Lan�ar Pagamentos</li> </div>
						<div id="icone">
                           <img src="images/financeiroLancarPagamentos.png" />
                        </div>


				<h:form styleClass="niceform" prependId="false">
				<p:focus/>

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
										<tr> 
											<td align="right" width="150px;"> 

											   <label for="cmbConta">Conta:</label> 
											</td> 
											<td align="right">
											   <h:selectOneMenu id="cmbConta" binding="#{pagamentoHandler.tipoContaSelecionado}" value="#{pagamentoHandler.pagamento.conta.id}" tabindex="1">
											   	<f:selectItems value="#{pagamentoHandler.tipoContaCombo}"/>
											   </h:selectOneMenu>
											</td> 
											<td align="right" width="150px;"> 
											   <label for="cmbTipoDocumento">Tipo do Documento</label> 
											</td> 
											<td align="right">
											   <h:selectOneMenu id="cmbTipoDocumento" binding="#{pagamentoHandler.tipoDocumentoSelecionado}" value="#{pagamentoHandler.pagamento.tipoDocumento.id}" tabindex="2">
											   	<f:selectItems value="#{pagamentoHandler.tipoDocumentosCombo}"/>
											   </h:selectOneMenu>
											</td> 
	
										</tr> 
									</table>

									<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroDocumento">N�mero do Cheque/Documento:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{pagamentoHandler.pagamento.numeroDocumento}" id="txtNumeroDocumento" size="15" maxlength="15" tabindex="3"/>
										</td> 
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtNominal">Nominal a:</label>
										</td>
										<td>
										   <h:inputText value="#{pagamentoHandler.pagamento.nominal}" id="txtNominal" size="40" maxlength="60" tabindex="4"/>
										</td>											
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataDocumento" >Data do Documento:</label>
										</td>
										<td>
											<h:inputText value="#{pagamentoHandler.pagamento.dataDocumento}"  id="dtDataDocumento" maxlength="10"  tabindex="5">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
											</h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtValorDocumento">Valor do Documento:</label>
										</td>
										<td>
										   <h:inputText value="#{pagamentoHandler.pagamento.valor}" id="txtValorDocumento" size="15" maxlength="15" tabindex="6">
													<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>
										</td>											
									</tr>
								</table>
							</fieldset>	
							<fieldset>
								<legend>Informa��o de Cancelamento</legend>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="dtDataCancelamento" >Data do Cancelamento:</label>
										</td>
										<td>
											<h:inputText value="#{pagamentoHandler.pagamento.dataCancelamento}"  id="dtDataCancelamento" maxlength="10"  >
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" />
											</h:inputText>
										</td>
									</tr>
									<tr>
										<td align="right" width="90px;">
										  <label for="txtMotivoCancelamento">Motivo do Cancelamento:</label>
										</td>
										<td>
										   <h:inputText value="#{pagamentoHandler.pagamento.motivoCancelamento}" id="txtMotivoCancelamento" size="40" maxlength="60" />
										</td>											
									</tr> 
								</table>
							</fieldset>
								
						<fieldset>
								<legend>Detalhes</legend>
								
								<p:commandButton action="#{pagamentoHandler.ajaxCall}" value="Selecionar Contas" id="selecionarContas" onclick="dialogContas.show();" styleClass="buttonDetalhe" />
								
									<p:dataTable value="#{pagamentoHandler.pagamento.detalhepagamento_collection}" 
												 var="detalhe" width="100%" 
												 rendered="#{not empty pagamentoHandler.pagamento.detalhepagamento_collection}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Filial" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.filial.nomeFilial}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.despesa.nomeDespesa}"/>									  										
										</p:column>		
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Grupo de Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.grupoDespesa.nomeGrupoDespesa}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Funcion�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.funcionario.nome}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.valor}" styleClass="monetario"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>	
										<p:column styleClass="centralizado">
										  <f:facet name="header">
										  <h:outputText value="Remover" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{pagamentoHandler.excluirDetalhePagamento}" onclick="javascript:return(confirm('Deseja remover esse item da Sele��o realizada ?'))">											
										  	<IMG title="Remover Sele��o" alt="Remover Sele��o" src="images/removerSelecao.png" border=0>
										  	<f:param id="excluirDetalhePagamento" name="idExcluir" value="#{detalhe}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
								<table>
									<tr> 
											<td align="right" width="550px;"> 
											  <label for="txtValorSubTotal">Total:</label> 
											</td> 
											<td> 
											  <b> <h:outputText value="#{pagamentoHandler.subTotal}" id="txtValorSubTotal" >
														<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
												   </h:outputText>
 											  </b>
											</td> 
										</tr>
								</table>
							</fieldset>	
					
							<br />
							<p:commandButton actionListener="#{pagamentoHandler.gravar}" value="Gravar" id="gravarPagamento" styleClass="button" async="false" ajax="false" tabindex="16"/>
							<p:commandButton actionListener="#{pagamentoHandler.cancelarIncluir}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="17">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

<p:dialog  visible="false" header="Resultado da Consulta" widgetVar="dialogContas" constrainToViewport="true" fixedCenter="true" underlay="shadow" height="380">
<div style="overflow: auto; width:800px; height:280px;" > 
									<p:dataTable value="#{pagamentoHandler.listaContasNaoPagas}" 
												 var="item" width="100%" 
												 rendered="#{not empty pagamentoHandler.listaContasNaoPagas}" 
												 selection="#{pagamentoHandler.contasVencerSelecionadas}"
												 selectionMode="multiple"	
												 
												 >
										
										<p:column resizable="true" sortBy="#{item.filial.nomeFilial}" >
										  <f:facet name="header">
										  <h:outputText value="Filial" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.filial.nomeFilial}" style="FONT: bold 11px Arial, Helvetica, sans-serif; text-shadow: black;	 color: DarkRed;" rendered="#{item.contaVencida}"/>	
										  <h:outputText value="#{item.filial.nomeFilial}" rendered="#{!item.contaVencida}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.despesa.nomeDespesa}">
										  <f:facet name="header">
										  <h:outputText value="Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.despesa.nomeDespesa}" style="FONT: bold 11px Arial, Helvetica, sans-serif; text-shadow: black;	 color: DarkRed;" rendered="#{item.contaVencida}"/>		
										  <h:outputText value="#{item.despesa.nomeDespesa}" rendered="#{!item.contaVencida}"/>							  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.grupoDespesa.nomeGrupoDespesa}">
										  <f:facet name="header">
										  <h:outputText value="Grupo de Despesa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.grupoDespesa.nomeGrupoDespesa}" style="FONT: bold 11px Arial, Helvetica, sans-serif; text-shadow: black;	 color: DarkRed;" rendered="#{item.contaVencida}"/>
										  <h:outputText value="#{item.grupoDespesa.nomeGrupoDespesa}" rendered="#{!item.contaVencida}"/>										  										
										</p:column>	
										<p:column resizable="true"  sortBy="#{item.funcionario.nome}" >
										  <f:facet name="header">
										  <h:outputText value="Funcion�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.funcionario.nome}" style="FONT: bold 11px Arial, Helvetica, sans-serif; text-shadow: black;	 color: DarkRed;" rendered="#{item.contaVencida}"/>									  										
										  <h:outputText value="#{item.funcionario.nome}" rendered="#{!item.contaVencida}"/>
										</p:column>	

										<p:column resizable="true" sortBy="#{item.valorDespesa}" >
										  <f:facet name="header">
										  <h:outputText value="Valor" /> 
										  </f:facet>
										  <h:outputText value="#{item.valorDespesa}" style="FONT: bold 11px Arial, Helvetica, sans-serif; text-shadow: black;	 color: DarkRed;" rendered="#{item.contaVencida}">
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>	
										   </h:outputText>	
										  <h:outputText value="#{item.valorDespesa}" rendered="#{!item.contaVencida}">
											<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>	
										   </h:outputText>							  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.dataVencimento}" >
										  <f:facet name="header">
											  <h:outputText value="Data Vencimento" />
										  </f:facet>
										  <h:outputText value="#{item.dataVencimento}" style="FONT: bold 11px Arial, Helvetica, sans-serif; text-shadow: black;	 color: DarkRed;" rendered="#{item.contaVencida}">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />			
										   </h:outputText>
							  			  <h:outputText value="#{item.dataVencimento}" rendered="#{!item.contaVencida}">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  />			
										   </h:outputText>
										</p:column>	

									</p:dataTable>
						</div>
								<div align="right" style="padding-top: 5px; padding-bottom: 30px;">
										<h:commandLink actionListener="#{pagamentoHandler.popularContasSelecionadas}">  
										    <p:graphicImage value="images/adicionarContas.png"  alt="Adicionar Contas Selecionadas" title="Adicionar Contas Selecionadas"/>
										</h:commandLink>
								</div>
							</p:dialog>


						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
