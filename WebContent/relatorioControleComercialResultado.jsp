<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="controleComercialHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class="popUp">
<jsp:include page="common/relatorioHeader.jsp" />

<h:form styleClass="niceform" prependId="false">
			<table class="cabecalho_relatorio" >
				<tr>
					<td><IMG title="Slogan" alt="Slogan" src="images/logoRelatorio.png" border=0 /></td>
					<td width="200px;">Relat�rio de Controle Comercial</td>
					<td>Data:</td>
					<td width="500px;"> <h:outputText value="#{controleComercialHandler.dataAtualRelatorio}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy HH:mm" locale="pt_BR" /></h:outputText>
					<td class="invisivelImpressao">
						<h:commandLink onclick="javascript:window.print();">  
						    <p:graphicImage value="images/impressora.png" alt="Imprimir" title="Imprimir"/>  
						</h:commandLink>
					</td>
					<td class="invisivelImpressao">
						<h:commandLink>  
						    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
						    <p:dataExporter type="xls" target="resultTable" fileName="Relatorio_Controle_Comercial"  postProcessor="#{controleComercialHandler.postProcessXLS}" />  
						</h:commandLink>
					</td>
				<tr>
			</table>

						<p:dataTable id="resultTable" value="#{controleComercialHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 rendered="#{not empty controleComercialHandler.resultadoPesquisa}" >
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Vidas Ativas" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.numeroVidasAtivas}"/>		
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="N� de Funcion�rios" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.numeroFuncionarios}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Produ��o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.producao}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Agenciador" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.funcionario.nome}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Data da Chegada" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataVisitaChegada}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Data da Sa�da" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataVisitaSaida}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" /></h:outputText>									  										
										</p:column>	

										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Custos da Viagem" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.custos}">
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
										  </h:outputText>									  										
										</p:column>	
							</p:dataTable>

						</h:form>
</BODY>
</HTML>
</f:view>
