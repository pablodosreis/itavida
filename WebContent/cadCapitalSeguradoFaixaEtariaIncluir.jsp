<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="capitalSeguradoFaixaEtariaHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery('#txtNomeTabela').focus();
	   jQuery("#dtDataVigencia").mask("99/99/9999",{placeholder:"_"});
	   jQuery('#txtCapitalSegurado').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	 
	   jQuery('#txtTitular').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	   jQuery('#txtTitularConjuge').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	   jQuery("#dataInicioFator").mask("99/99/9999",{placeholder:"_"});

	   
	});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Tabelas de Capital Segurado Por Faixa Etaria</li> </div>
						<div id="icone">
                           <img src="images/cadTabelaFaixaEtaria.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false">
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="txtNomeTabela">Nome da Tabela:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{capitalSeguradoFaixaEtariaHandler.capitalSeguradoFaixaEtaria.nomeTabela.nomeTabela}" id="txtNomeTabela" size="30"  tabindex="1" >
												<p:ajax event="keyup" update="renderizado" >
													<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{capitalSeguradoFaixaEtariaHandler.limparNomeTabela}" 
														target="#{capitalSeguradoFaixaEtariaHandler.capitalSeguradoFaixaEtaria.nomeTabela}"/> 
												</p:ajax>
										   </h:inputText>
										   
											<rich:suggestionbox id="suggestionBoxId" 
								                	for="txtNomeTabela" 
								                    suggestionAction="#{capitalSeguradoFaixaEtariaHandler.nomeTabelaAutocomplete}" 
								                    var="nomeTabela" 
								                    fetchValue="#{nomeTabela.nomeTabela}"
								                    width="300"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{nomeTabela}" 
														target="#{capitalSeguradoFaixaEtariaHandler.capitalSeguradoFaixaEtaria.nomeTabela}"/> 
													</a4j:support> 

								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabela}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <h:outputText value="#{nomeTabela.nomeTabelaReduzido}" styleClass="autocompleteFonte"/>
								                    </h:column>
								             </rich:suggestionbox>											
										</td> 

									</tr> 
									<tr> 
										<td align="right" width="90px;"> 
										  <label for="dtDataVigencia">Data Vig�ncia:</label> 
										</td> 
										<td>
											<h:inputText value="#{capitalSeguradoFaixaEtariaHandler.capitalSeguradoFaixaEtaria.dataVigencia}"  id="dtDataVigencia" maxlength="10" tabindex="2">
												<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>
									</tr>										
								</table>	
						</fieldset>	
						
						
									
						<fieldset>
								<legend>Detalhes</legend>
								
								<table>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtIdadeInicial">Idade Inicial:</label>
											</td>
											<td>
											   <h:inputText value="#{capitalSeguradoFaixaEtariaHandler.detalheCapital.idadeInicial}" id="txtIdadeInicial" size="5" maxlength="3" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="3"/>
											</td>
										</tr>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtIdadeFinal">Idade Final:</label>
											</td>
											<td>
											   <h:inputText value="#{capitalSeguradoFaixaEtariaHandler.detalheCapital.idadeFinal}" id="txtIdadeFinal" size="5" maxlength="3" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="4" />
											</td>
										</tr>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtCapitalSegurado">Capital Segurado:</label>
											</td>
											<td>
											   <h:inputText value="#{capitalSeguradoFaixaEtariaHandler.detalheCapital.capitalSegurado}" id="txtCapitalSegurado" size="15" maxlength="15" tabindex="5" >
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
											  </h:inputText>
											</td>
										</tr>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtTitular">Titular:</label>
											</td>
											<td>
											   <h:inputText value="#{capitalSeguradoFaixaEtariaHandler.detalheCapital.titular}" id="txtTitular" size="15" maxlength="15" tabindex="6" >
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
											  </h:inputText>
											</td>
										</tr>
										<tr>
											<td align="right" width="90px;">
											  <label for="txtTitularConjuge">Titular + C�njuge:</label>
											</td>
											<td>
											   <h:inputText value="#{capitalSeguradoFaixaEtariaHandler.detalheCapital.somaTitularConjuge}" id="txtTitularConjuge" size="15" maxlength="15" tabindex="7" >
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/>
											  </h:inputText>
											</td>
										</tr>
								</table>
								<p:commandButton actionListener="#{capitalSeguradoFaixaEtariaHandler.incluirDetalheCapital}" value="Incluir Detalhe" id="gravarContato" styleClass="buttonDetalhe" async="false" tabindex="8" ajax="false" oncomplete="JQuery('txtIdadeInicial').focus();"/>
								<p:commandButton actionListener="#{capitalSeguradoFaixaEtariaHandler.cancelarDetalhe}" value="Cancelar" id="cancelarContato" styleClass="buttonDetalhe" async="false" tabindex="9">
									<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
								</p:commandButton>	

								<div id="agrupamento_Dados" class="agrupamento_Dados">								
									<p:dataTable id="resultTable" value="#{capitalSeguradoFaixaEtariaHandler.capitalSeguradoFaixaEtaria.detalhecapitalfaixaetaria_collection}" 
												 var="detalhe" width="100%" 
												 rendered="#{not empty capitalSeguradoFaixaEtariaHandler.capitalSeguradoFaixaEtaria.detalhecapitalfaixaetaria_collection}" 
												 paginator="true" rows="10" previousPageLinkLabel="&lt;&lt;Anterior" lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira" nextPageLinkLabel="Pr�ximo&gt;&gt;">
										
										<p:column resizable="true"  sortBy="#{detalhe.idadeInicial}" >
										  <f:facet name="header">
										  <h:outputText value="Idade Inicial" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.idadeInicial}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{detalhe.idadeFinal}" >
										  <f:facet name="header">
										  <h:outputText value="Idade Final" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.idadeFinal}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{detalhe.capitalSegurado}" >
										  <f:facet name="header">
										  <h:outputText value="Capital Segurado" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.capitalSegurado}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{detalhe.titular}" >
										  <f:facet name="header">
										  <h:outputText value="Valor Titular" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.titular}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{detalhe.somaTitularConjuge}" >
										  <f:facet name="header">
										  <h:outputText value="Titular + C�njuge" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.somaTitularConjuge}"><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{detalhe.dataAjuste}" >
										  <f:facet name="header">
										  <h:outputText value="�ltimo Ajuste" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{detalhe.dataAjusteFormatada}"></h:outputText>									  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Alterar" /> 
										  	
										  </f:facet>
										  <h:commandLink  actionListener="#{capitalSeguradoFaixaEtariaHandler.alterarDetalheCapital}">
										  	<IMG title="Alterar" alt="Alterar" src="images/edit.png" border=0 />
										  	<f:param id="editDetalheCapital" name="idAlterar" value="#{detalhe}"/>
										  </h:commandLink>								  										
										</p:column>	
										<p:column>
										  <f:facet name="header">
										  <h:outputText value="Excluir" /> 										  	
										  </f:facet>
										  <h:commandLink actionListener="#{capitalSeguradoFaixaEtariaHandler.excluirDetalheCapital}" onclick="javascript:return(confirm('Deseja realmente deletar este item ?'))">											
										  	<IMG title="Excluir" alt="Excluir" src="images/excluir.png" border=0>
										  	<f:param id="excluirDetalheCapital" name="idExcluir" value="#{detalhe}"/>
										  </h:commandLink>								  										
										</p:column>																																																
									</p:dataTable>
								<div align="right">
										<h:commandLink >  
										    <p:graphicImage value="images/exportarExcel.png"  alt="Exportar Para Excel" title="Exportar Para Excel"/>  
										    <p:dataExporter type="xls" target="resultTable" fileName="TabelaCapitalFaixaEtaria_#{capitalSeguradoFaixaEtariaHandler.capitalSeguradoFaixaEtaria.nomeTabela.nomeTabela}"  postProcessor="#{capitalSeguradoFaixaEtariaHandler.postProcessXLS}" excludeColumns="5,6"/>  
										</h:commandLink>
								</div>
							</div>
							</fieldset>		
							<br />
							<p:commandButton actionListener="#{capitalSeguradoFaixaEtariaHandler.gravar}" value="Gravar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="10"/>
							<p:commandButton actionListener="#{capitalSeguradoFaixaEtariaHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="11">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
