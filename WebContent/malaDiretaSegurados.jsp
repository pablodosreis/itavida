<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="propostaHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper();
	   jQuery("#dtNascimentoSeguradoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoSeguradoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoConjugeInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtNascimentoConjugeFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAprovacaoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataAprovacaoFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCadastroInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCadastroFim").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCancelamentoInicio").mask("99/99/9999",{placeholder:"_"});
	   jQuery("#dtDataCancelamentoFim").mask("99/99/9999",{placeholder:"_"});	  	   
	   
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rio</span><li>Mala Direta de Segurados</li> </div>
						<div id="icone">
                           <img src="images/malaDireta.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false">


							<div class="titulo_agrupamento_Dados" ></div>
							
							<fieldset>
								<legend>Dados Para Consulta</legend>
								<br/> <br/>
								<table >				
									<tr>
										<td align="right" width="150px;">
										  <label for="txtIdProposta">N�mero da Proposta:</label>
										</td>
										<td>
										   <h:inputText value="#{propostaHandler.propostaFiltro.id}" id="txtIdProposta" size="10" maxlength="10" onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="1"/>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="cmbEmpresa">Empresa:</label>
										</td>
										<td>
										   <h:selectOneMenu id="cmbEmpresa" value="#{propostaHandler.propostaFiltro.empresa.id}"  tabindex="2">
										   		<f:selectItems value="#{propostaHandler.opcoesEmpresaCombo}" />
										   </h:selectOneMenu>
										</td>								
									</tr>																		
								</table>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao">�rg�o:</label> 
										</td> 
										<td> 
										   <h:inputText value="#{propostaHandler.propostaFiltro.orgao.nomeOrgao}" id="txtOrgao" size="40" tabindex="3"/>
											<rich:suggestionbox id="suggestionBoxOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{propostaHandler.orgaoAutoComplete}" 
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="450"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{propostaHandler.propostaFiltro.orgao}"/> 
													</a4j:support> 
								                    <h:column>
								                       <h:outputText value="#{orgao.nomeOrgao}" styleClass="autocompleteFonte"/> 
								                    </h:column>
								                    <h:column>
								                       <b> <h:outputText value="#{orgao.siglaOrgao}" styleClass="autocompleteFonte"/></b>
								                    </h:column>
								             </rich:suggestionbox>
										</td> 
									</tr> 
								</table>  
								<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoPrincipal">Nome Segurado:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.nomeSegurado}" id="txtNomeSeguradoPrincipal" size="50" maxlength="50" tabindex="4"/>
											</td>
										</tr>
										<tr>										
											<td align="right" width="150px;">
											  <label for="txtCpf">CPF Segurado:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.cpfSegurado}" id="txtCpf" size="20" maxlength="20" tabindex="5"/>
											</td>								
										</tr>									
								</table>
																
								<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="txtNomeSeguradoConjuge">Nome C�njuge:</label>
											</td>
											<td>
											   <h:inputText value="#{propostaHandler.propostaFiltro.nomeConjuge}" id="txtNomeSeguradoConjuge" size="50" maxlength="50" tabindex="6"/>
											</td>
										</tr>
								</table>
						</fieldset>
							<br />
							<h:commandButton actionListener="#{propostaHandler.pesquisaPropostasRelatorioMalaDireta}" value="Pesquisar" id="pesquisar" styleClass="button" tabindex="7"/>
							<p:commandButton actionListener="#{propostaHandler.cancelarFiltro}" value="Cancelar" id="cancelar" styleClass="button" async="false"  ajax="false" tabindex="8">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
<p:dialog  visible="true" header="SELECIONE OS SEGURADOS DESEJADOS" rendered="#{not empty propostaHandler.resultadoPesquisa}" constrainToViewport="true" x="0" y="50" underlay="shadow" width="950" height="550" > 
<div style="overflow: auto; width:950px; height:350px;" >
									<p:dataTable value="#{propostaHandler.resultadoPesquisa}" 
												 var="item" width="100%" 
												 id="resultTable"
												 selection="#{propostaHandler.propostasSelecionadas}"
													selectionMode="multiple"
													scrollable="true"
												  rendered="#{not empty propostaHandler.resultadoPesquisa}" 
												 >
										<p:column resizable="true" sortBy="#{item.id}" width="35" >
										  <f:facet name="header">
										  <h:outputText value="Proposta" /> 
										  </f:facet>
										  <h:outputText value="#{item.id}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.empresa.nomeEmpresa}" width="100">
										  <f:facet name="header">
										  <h:outputText value="Empresa" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.empresa.nomeEmpresa}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.orgao.nomeOrgao}" width="280">
										  <f:facet name="header">
										  <h:outputText value="�rg�o" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.orgao.nomeOrgao}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.nomeSegurado}" width="210" >
										  <f:facet name="header">
										  <h:outputText value="Segurado Principal" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.nomeSegurado}"/>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.dataNascimentoSegurado}"  width="60">
										  <f:facet name="header">
										  <h:outputText value="Dt Nascimento" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.dataNascimentoSegurado}"><f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy"  /></h:outputText>									  										
										</p:column>	
										<p:column resizable="true" sortBy="#{item.cidade.nom_cidade}" width="150">
										  <f:facet name="header">
										  <h:outputText value="Cidade" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.nom_cidade}"/>									  										
										</p:column>		
										<p:column resizable="true" sortBy="#{item.cidade.cod_estado.sgl_estado}" >
										  <f:facet name="header">
										  <h:outputText value="UF" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.cidade.cod_estado.sgl_estado}"/>									  										
										</p:column>
										<p:column resizable="true" sortBy="#{item.totalPremios}" width="50">
										  <f:facet name="header">
										  <h:outputText value="Pr�mio Total" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{item.totalPremios}" ><f:convertNumber currencySymbol="R$" type="currency"  groupingUsed="#{false}"/></h:outputText>
										</p:column>	
																																																					
									</p:dataTable>
</div>
								<div align="right" style="padding-top: 5px; padding-bottom: 10px;">
										<h:commandLink action="#{propostaHandler.generateMalaDireta}">  
										    <p:graphicImage value="images/gerarMalaDireta.png"  alt="Gerar Mala Direta" title="Gerar Mala Direta"/>
 
										</h:commandLink>
								</div>
						</p:dialog>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
