<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="relatorioMensalPagamentosHandler" />

<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery("#filtroMes").mask("99",{placeholder:"_"});
	   jQuery("#filtroAno").mask("9999",{placeholder:"_"});
	   
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Relat�rio</span><li>Relat�rio de Quita��o</li> </div>
						<div id="icone">
                           <img src="images/cadProposta.png" />
                        </div>

				<h:form styleClass="niceform" prependId="false">


							<div class="titulo_agrupamento_Dados" ></div>
							
							<fieldset>
								<legend>Dados Para Consulta</legend>
								<br/>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="cmbEmpresa">Empresa:</label>
										</td>
										<td>
										   <h:selectOneMenu id="cmbEmpresa" value="#{relatorioMensalPagamentosHandler.empresaId}"  tabindex="2">
										   		<f:selectItems value="#{relatorioMensalPagamentosHandler.opcoesEmpresaCombo}" />
										   </h:selectOneMenu>
										</td>								
									</tr>																		
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="txtNomeSeguradoPrincipal">Nome Segurado:</label>
										</td>
										<td>
										   <h:inputText value="#{relatorioMensalPagamentosHandler.nomeSegurado}" id="txtNomeSeguradoPrincipal" size="50" maxlength="50" tabindex="6"/>
										</td>
									</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="mesAnoInicio" >M�s:</label>
										</td>
										<td>
											<h:inputText value="#{relatorioMensalPagamentosHandler.filtroMes}"  id="filtroMes"  size="8"  tabindex="8"/>
										</td>
										<td align="right" >
										  <label for="mesAnoFim" >Ano:</label>
										</td>
										<td>
											<h:inputText value="#{relatorioMensalPagamentosHandler.filtroAno}"  id="filtroAno" size="8" tabindex="9"/>
										</td>
									</tr>		
								</table>
								<table>
									<tr>										
										<td align="right" width="150px;">
										  <label for="txtCPF">CPF:</label>
										</td>
										<td>
										   <h:inputText value="#{relatorioMensalPagamentosHandler.cpfSegurado}" id="txtCPF" size="20" maxlength="20" tabindex="18"/>
										</td>								
									</tr>									
								</table>
										<table>
											<tr>
												<td align="right" width="150px;"><label
													for="cmbTipoConsulta">Situa��o da Proposta:</label></td>
												<td>
													<table border="0">
														<tr>
															<td><h:selectManyCheckbox id="cmbTipoConsulta"
																	value="#{relatorioMensalPagamentosHandler.situacaoPropostaSelectedList}">
																	<f:selectItems
																		value="#{relatorioMensalPagamentosHandler.opcoesSituacaoPropostaCmb}" />
																</h:selectManyCheckbox></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>

									</fieldset>	
												
							<br />
							<h:commandButton action="#{relatorioMensalPagamentosHandler.pesquisaCompetencias}" value="Gerar Relat�rio" id="pesquisar" styleClass="button" tabindex="31"/>
							<p:commandButton actionListener="#{relatorioMensalPagamentosHandler.cancelarFiltro}" value="Cancelar" id="cancelar" styleClass="button" async="false" tabindex="32">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>

						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
