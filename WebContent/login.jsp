<%@ include file="./common/cabecalho.jsp" %>
<html>
<body>
<f:view>

<HEAD>

<jsp:include page="common/title.jsp" />
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><!--[if lte IE 6]>

<SCRIPT src="js/pngfix.js" defer type=text/javascript></SCRIPT>
<![endif]-->
<style type="text/css" media="screen">@import url(css/niceforms-default.css);</style>
<script language="javascript" type="text/javascript" src="js/niceforms.js"></script>
<script language="javascript" type="text/javascript" src="js/itavida.js"></script>
<p:resources /> 

<p:dialog header="Erro no Login" id="dialog1" widgetVar="dialog"  
               fixedCenter="true" modal="false" visible="#{facesContext.maximumSeverity != null}" zindex="1000"  >
   <h:messages errorClass="mensagem_erro_login" fatalClass="mensagem_erro_login" infoClass="mensagem_sucesso" warnClass="mensagem_alerta"/>                 
</p:dialog>

<h:form prependId="false" >
<p:focus/>
<p:dialog close="false" constrainToViewport="true" draggable="false" modal="true" resizable="false" visible="true" zindex="0" underlay="shadow" fixedCenter="true">

<div class="divLogin">
<br/><br/><br/><br/><br/><br/><br/>
		<table> 
				<tr> 
					<td align="right" width="220px;"> 
					  <font color="white" size="3">Usu�rio:</font>
					</td> 
					<td>
						<h:inputText value="#{usuarioHandler.usuario.usuario}" id="txtUsuario" size="15" />
					</td> 
				</tr> 
				<tr> 
					<td align="right" width="220px;"> 
					    <font color="white" size="3">Senha:</font> 
					</td> 
					<td>
						<h:inputSecret value="#{usuarioHandler.usuario.senha}" id="txtSenha" size="15" />
					</td> 
				</tr>
			</table> 
			<table>
				<tr>
					<td align="right" width="390px;"> 
						<h:commandButton  image="images/password.png" id="linkLogin" action="#{usuarioHandler.autenticar}">								
						</h:commandButton >
					</td>
				</tr>				
			</table>
			
			
<div>
</p:dialog>
</h:form>
</f:view>
</body>
</html>