<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<a4j:keepAlive beanName="exportarArquivoTokioHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />
<BODY class=f-default>
<script>
<script>

jQuery(function(jQuery){

	   jQuery('#cmbEmpresa').focus();
	   
	});
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Arquivos </span><li>Exportar Base de Dados para T�kio Marine </li> </div>
						<div id="icone_tokio">
                           <img src="images/logo_tokio.png" />
                        </div>

						<h:form  prependId="false" >
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>

<br/><br/><br/>
								<table>
									<tr>
										<td align="right" width="150px;">
										  <label for="cmbEmpresa">Empresa:</label>
										</td>
										<td>
										   <h:selectOneMenu id="cmbEmpresa" value="#{exportarArquivoTokioHandler.empresaSelected}"  tabindex="1">
										   		<f:selectItems value="#{exportarArquivoTokioHandler.opcoesEmpresaCombo}" />
										   </h:selectOneMenu>
										</td>								
									</tr>																		
								</table>

<h:selectOneRadio value="#{exportarArquivoTokioHandler.radioSelected}" border="0"  layout="pageDirection" tabindex="2">  
    <f:selectItem itemLabel="Exportar Base de Dados Segurados Ativos (Plano Individual)" itemValue="1" />  
    <f:selectItem itemLabel="Exportar Base de Dados Segurados Ativos (Plano Conjugado)" itemValue="2" />
	<f:selectItem itemLabel="Exportar Base de Dados Segurados Ativos (Todos Campos)" itemValue="3" />    
</h:selectOneRadio>

</fieldset>

						<br /><br/><br/>
							<h:commandButton action="#{exportarArquivoTokioHandler.exportReport}" value="Realizar Exporta��o" id="gerarExport" styleClass="button" tabindex="2"/>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
