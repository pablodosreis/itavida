<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="nomeTabelaHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js"/>
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery('input').bestupper();  
}); 
</script> 
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Cadastro</span><li>Nomes de Tabelas de Capital Segurado</li> </div> 
						<div id="icone"> 
                           <img src="images/cadNomesTabelas.png" /> 
                        </div> 

						<h:form id="myform" styleClass="niceform" prependId="false" >

					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table>
									<tr>
										<td align="right" width="150px;"> 
										 	 <label for="txtNomeTabela">Nome da Tabela:</label>
										</td>
										<td>
										 	<h:inputText value="#{nomeTabelaHandler.nomeTabela.nomeTabela}" id="txtNomeTabela" size="40" maxlength="50" tabindex="1"/>										  
										</td>
									</tr>
									<tr>
										<td align="right" width="150px;"> 
										 	 <label for="txtNomeReduzidoTabela">Nome da Tabela Reduzido:</label>
										</td>
										<td>
										 	<h:inputText value="#{nomeTabelaHandler.nomeTabela.nomeTabelaReduzido}" id="txtNomeReduzidoTabela" size="30" maxlength="30" tabindex="2"/>										  
										</td>
									</tr>
								</table>
							</fieldset>							
							<br />
							
							<p:commandButton actionListener="#{nomeTabelaHandler.gravar}" value="Salvar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="3"/>
							<p:commandButton actionListener="#{nomeTabelaHandler.cancelar}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="4">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
