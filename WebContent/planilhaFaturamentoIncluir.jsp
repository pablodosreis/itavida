<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="faturamentoHandler" />
<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>

<script type="text/javascript">
jQuery(function(jQuery){
	jQuery('input').bestupper();
	jQuery("#dtPeriodoVigencia").mask("99/9999",{placeholder:"_"});
	jQuery("#dtPeriodoCobranca").mask("99/9999",{placeholder:"_"});
	jQuery("#dtPagamento").mask("99/99/9999",{placeholder:"_"});
	
	
	jQuery('#txtValorRelacao').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});

	jQuery('#txtValorProlabore').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});
	jQuery('#txtValorSemProlabore').priceFormat({
	    prefix: 'R$ ',
	    centsSeparator: ',',
	    thousandsSeparator: '.'
	});
	
	jQuery('#txtValorTaxa').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});		
	jQuery('#txtValorSeguradora').priceFormat({
		    prefix: 'R$ ',
		    centsSeparator: ',',
		    thousandsSeparator: '.'
		});	   
	});

</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Faturamento</span><li>Planilha de Faturamento (Inclus�o/Altera��o)</li> </div>
						<div id="icone">
                           <img src="images/faturamento.png" />
                        </div>

						<h:form styleClass="niceform" prependId="false">
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Dados</legend>
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtOrgao"><h:outputText id="orgaoLabel" value="�rg�o:"/></label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.orgao.nomeOrgao}" id="txtOrgao" size="40" tabindex="1">
												<p:ajax event="keyup" update="renderizado" >
													<h:panelGroup id="renderizado" />
													<f:setPropertyActionListener 
														value="#{faturamentoHandler.limparOrgao}" 
														target="#{faturamentoHandler.planilhaFaturamento.orgao}"/> 
												</p:ajax>
											</h:inputText>
											<rich:suggestionbox id="suggestionOrgao" 
								                	for="txtOrgao"
								                    suggestionAction="#{faturamentoHandler.orgaoAutoComplete}" 
								                    var="orgao"
								                    fetchValue="#{orgao.nomeOrgao}"
								                    width="400"
								                    height="250"
								                    cellpadding="4"
                    								nothingLabel="Nenhum Item Encontrado!"
								                    >
								                    <a4j:support event="onselect" id="a4jsupport" ignoreDupResponses="true" > 
														<f:setPropertyActionListener 
														value="#{orgao}" 
														target="#{faturamentoHandler.planilhaFaturamento.orgao}"/> 
													</a4j:support> 
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Nome do Org�o" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.nomeOrgao}"/>									  										
													</h:column>
													<h:column >
													  <f:facet name="header">
													  <h:outputText value="Sigla" /> 
													  	
													  </f:facet>
													  <h:outputText value="#{orgao.siglaOrgao}"/>									  										
													</h:column>	
								         	</rich:suggestionbox>
										</td> 
									</tr> 
								</table> 
								<table> 
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroVidas">N�mero de Vidas:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.numeroVidas}" id="txtNumeroVidas" size="10"  onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="2"/>
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtNumeroVidasSemTaxa">N�mero de Vidas Sem Taxa:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.numeroVidasSemTaxa}" id="txtNumeroVidasSemTaxa" size="10"  onkeydown="Mascara(this,Integer);" onkeypress="Mascara(this,Integer);" onkeyup="Mascara(this,Integer);" tabindex="3"/>
										</td> 
									</tr>
								</table>
								<table>
										<tr>
											<td align="right" width="150px;">
											  <label for="dtPagamento" >Data do Pagamento:</label>
											</td>
											<td>
												<h:inputText value="#{faturamentoHandler.planilhaFaturamento.dataPagamento}"  id="dtPagamento" maxlength="10"  tabindex="4">
													<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:inputText>
											</td>
										</tr>
								</table>
								<table>
									<tr>
										<td align="right" width="150px;"> 
										   <label for="dtPeriodoVigencia">Per�odo Vig�ncia:</label> 
										</td> 
										<td>
											<h:inputText value="#{faturamentoHandler.planilhaFaturamento.mesAnoVigencia}"  id="dtPeriodoVigencia" maxlength="7"  tabindex="5">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>  
									</tr>
									<tr>
										<td align="right" width="150px;"> 
										   <label for="dtPeriodoCobranca">Per�odo Cobran�a:</label> 
										</td> 
										<td>
											<h:inputText value="#{faturamentoHandler.planilhaFaturamento.mesAnoCobranca}"  id="dtPeriodoCobranca" maxlength="7"  tabindex="6">
												<f:convertDateTime dateStyle="default" pattern="MM/yyyy" locale="pt_BR" />
											</h:inputText>
										</td>  
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtBanco">Banco:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.banco}" id="txtBanco" size="20"  tabindex="7"/>
										</td> 
									</tr>
								</table>
							</fieldset>		
							<fieldset>
								<legend>Valores</legend>
								<table>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtValorRelacao">Valor Rela��o:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.valorRelacao}" id="txtValorRelacao" size="15"  tabindex="8">
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>						
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtValorProlabore">Valor Prolabore:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.valorProlabore}" id="txtValorProlabore" size="15"  tabindex="9">
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
												<p:ajax actionListener="#{faturamentoHandler.calculaValorSemProlabore}" event="blur" update="txtValorSemProlabore" oncomplete="jQuery(function(jQuery){ jQuery('#txtValorTaxa').focus(); });" />
											</h:inputText>		
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtValorSemProlabore">Valor Sem Prolabore:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.valorSemProlabore}" id="txtValorSemProlabore" size="15" style="width:150px;" styleClass="textinput" tabindex="10">
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>		
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtValorTaxa">Valor Taxa:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.valorTaxa}" id="txtValorTaxa" size="15"  tabindex="11">
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											 	<p:ajax actionListener="#{faturamentoHandler.calculaValorSeguradora}" event="blur" update="txtValorSeguradora" oncomplete="jQuery(function(jQuery){ jQuery('#gravar').focus(); });"/>
											</h:inputText>	
										</td> 
									</tr>
									<tr> 
										<td align="right" width="150px;"> 
										  <label for="txtValorSeguradora">Valor Seguradora:</label> 
										</td> 
										<td> 
										    <h:inputText value="#{faturamentoHandler.planilhaFaturamento.valorSeguradora}" id="txtValorSeguradora" size="15" style="width:150px;" styleClass="textinput" tabindex="12">
												<f:convertNumber currencySymbol="R$" type="currency" groupingUsed="#{false}"/>
											</h:inputText>	
										</td> 
									</tr>
								</table>								
							</fieldset>					
							<br />
							
							<p:commandButton actionListener="#{faturamentoHandler.gravar}" value="Gravar" id="gravar" styleClass="button" async="false" ajax="false" tabindex="13"/>
							<p:commandButton action="#{faturamentoHandler.cancelarInclusao}" value="Cancelar" id="cancelar" styleClass="button" async="false" ajax="false" tabindex="14">
								<p:confirmDialog message="Deseja Cancelar esta opera��o?" yesLabel="Sim" noLabel="N�o" header="Confirma��o!" fixedCenter="true" modal="true" />
							</p:commandButton>
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class="both"><!-- --></DIV>
		</DIV>
		<DIV id="footer_top_bg"><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id="foo_glass"></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>
