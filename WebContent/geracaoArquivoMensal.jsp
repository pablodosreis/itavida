<%@ include file="common/cabecalho.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<a4j:keepAlive beanName="geraArquivoHandler" />


<HTML 
xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" /> 
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	jQuery("#competencia").mask("99/9999",{placeholder:"_"});	
	
	
});

function ativarGerando(obj) {
	var texto = obj.value;
	var qtd = (texto.split(".").length - 1);
	
	if (document.getElementById('GeracaoAtiva').value == 1) {
		
		obj.disabled = 'disabled';
		
		if (qtd <= 0) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo.'};
		if (qtd == 1) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo..'};
		if (qtd == 2) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo...'};
		if (qtd == 3) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo....'};
		if (qtd == 4) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo.....'};
		if (qtd == 5) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo......'};
		if (qtd == 6) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo.......'};
		if (qtd == 7) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo........'};
		if (qtd == 8) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo.........'};
		if (qtd == 9) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo..........'};
		if (qtd == 10) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo...........'};
		if (qtd == 11) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo............'};
		if (qtd >= 12) {obj.value = 'Esta opera��o pode levar v�rios minutos. Gerando arquivo'};
		
		setTimeout(function() {
			ativarGerando(obj);
		}, 1000);
		
	} else {
		obj.value = 'Gerar arquivo';
		obj.disabled = '';
	}

	
	
	
}
</script>

<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Movimenta��o</span><li>Gerar Arquivo Movimenta��o</li> </div>
						<div id="icone">
                           <img src="images/cadOrgaos.png" />
                        </div>


				<h:form styleClass="niceform" prependId="false">
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Gerar Arquivo</legend>
									<div class="divRecuadoEsquerdo">
										<table>
											<tr>
												<td align="left" width="20px;">
												  <label for="periodo">Compet�ncia:</label> 
												</td>
												<td>
													<h:inputText value="#{geraArquivoHandler.competencia}"  id="competencia" maxlength="10" size="12"  />
												</td>												
											</tr>
											<tr>
												<td></td>
												<td>
													<input type="hidden" id="GeracaoAtiva" name="GeracaoAtiva" value="0">
													<p:commandButton actionListener="#{geraArquivoHandler.gerarArquivo}" value="Gerar Arquivo" id="gerar" styleClass="button" async="true" ajax="true"
													    update="messages"
													    onclick="document.getElementById('GeracaoAtiva').value=1;ativarGerando(this);" 
														oncomplete="document.getElementById('GeracaoAtiva').value=0;"
													/>
													<p:growl id="messages" showDetail="true"/>
												</td>
											</tr>											
										</table>
									</div>
									<br />
									<fieldset style="float: left">
									
										<legend>Arquivos Gerados</legend>
																				
										<h:panelGroup id="agrupamentoAgenciadorQA"
											styleClass="agrupamento_Dados">

											<p:dataTable
												value="#{geraArquivoHandler.arquivos}"
												var="item" width="50%"
												paginator="true" rows="12"
												previousPageLinkLabel="&lt;&lt;Anterior"
												lastPageLinkLabel="�ltima" firstPageLinkLabel="Primeira"
												nextPageLinkLabel="Pr�ximo&gt;&gt;">

												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Competencia" />

													</f:facet>
													<h:outputText value="#{item.referenciaTxt}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Data/Hora Gera��o" />

													</f:facet>
													<h:outputText
														value="#{item.geracaoTxt}" />
												</p:column>
												<p:column resizable="true">
													<f:facet name="header">
														<h:outputText value="Respons�vel" />

													</f:facet>
													<h:outputText value="#{item.usuario}" />
												</p:column>
												<p:column>
													<f:facet name="header">
														<h:outputText value="A��es" />
													</f:facet>
													<h:commandLink	actionListener="#{geraArquivoHandler.excluirArquivo}" 	onclick="javascript:return(confirm('Deseja realmente deletar esta gera��o ?'))">
														<h:graphicImage title="Excluir" alt="Excluir"
															url="images/excluir.png" style="border: none;" />
														<f:param id="excluir" name="excluir" value="#{item}" />
													</h:commandLink>
													<h:commandLink	actionListener="#{geraArquivoHandler.baixarArquivo}">
														<h:graphicImage title="Baixar Arquivo" alt="Baixar Arquivo"
															url="images/exportarExcel.png" style="border: none;width: 16px; margin-left: 5px" />
														<f:param id="baixar" name="baixar" value="#{item}" />
													</h:commandLink>
												</p:column>
											</p:dataTable>
										</h:panelGroup>
										

									</fieldset>
								
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>

</BODY>

</HTML>
</f:view>

