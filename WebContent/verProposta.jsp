<%@ include file="common/cabecalho.jsp" %>
<a4j:keepAlive beanName="verPropostaHandler" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>

<HTML 
xmlns="http://www.w3.org/1999/xhtml" lang="pt">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
<script>
jQuery(function(jQuery){
	   jQuery('input').bestupper(); 
	   jQuery("#txtCep").mask("99.999-999",{placeholder:"_"});
	   jQuery("#txtTelefone").mask("(99)9?9999-9999",{placeholder:"_"});
	   jQuery("#txtFax").mask("(99)9999-9999",{placeholder:"_"});
	});
</script>
<DIV id=background_2>
<DIV id=site_wrapper>
<jsp:include page="common/menu.jsp" />
<DIV id=wrap>
	<DIV id=content>
		<DIV id=main>
			<DIV id=main_content>
				<DIV id=main_body>

						<div id="breadcrumb"><span>Proposta</span><li>Visualizar</li> </div> 
						<div id="icone"> 
                           <img src="images/cadFiliais.png" /> 
                        </div> 

				<h:form id="myform" styleClass="niceform" prependId="false" >
					<div class="titulo_agrupamento_Dados" ></div>
							<fieldset>
								<legend>Tipo de Proposta</legend>
									<table> 
										<tr> 
											<td align="right" width="150px;"> 
											  <label >N�mero da Proposta:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.id}"  size="10" disabled="true"/>
											</td> 
										</tr> 
										<tr> 
											<td align="right"> 
											  <label >Empresa:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.empresa.nomeEmpresa}" size="20" disabled="true"/>
											</td> 
										</tr>
									
									
									<h:panelGroup id="planodivsuperap" styleClass="agrupamento_Dados" rendered="#{verPropostaHandler.proposta.superAp != null}">
									
										<tr> 
											<td align="right" width="150px;"> 
											   <label>Plano:</label> 
											</td> 
											<td>
												
												<h:inputText value="#{verPropostaHandler.proposta.superAp.plano.descricao}" id="txtPlanoSuperAp" disabled="true" />
											</td>
										</tr>
										<h:panelGroup id="cobertura1div" styleClass="agrupamento_Dados" rendered="#{verPropostaHandler.proposta.superAp.coberturaOpcional1 != null}">
											<tr> 
												<td align="right" width="150px;"> 
												   <label>Cobertura 1:</label> 
												</td> 
												<td>
													<h:inputText value="#{verPropostaHandler.proposta.superAp.coberturaOpcional1.descricao}" id="txCoberturaOpcional1" disabled="true"/>
												</td>
											</tr>
										</h:panelGroup>
										<h:panelGroup id="cobertura2div" styleClass="agrupamento_Dados" rendered="#{verPropostaHandler.proposta.superAp.coberturaOpcional2 != null}">
											<tr> 
												<td align="right" width="150px;"> 
												   <label>Cobertura 2:</label> 
												</td> 
												<td>
													<h:inputText value="#{verPropostaHandler.proposta.superAp.coberturaOpcional2.descricao}" id="txCoberturaOpcional2" disabled="true"/>
												</td>
											</tr>
										</h:panelGroup>
									
								</h:panelGroup>
								
										<tr> 
											<td align="right"  width="150px;"> 
											  <label >Modelo de Proposta:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.modeloProposta.descricao}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right"> 
											  <label >Tipo de Proposta:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.tipoProposta.descricao}" size="10" disabled="true"/>
											</td> 
										</tr>
										<tr> 
											<td align="right"> 
											  <label >�rg�o:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.orgao.nomeOrgao}" size="20" disabled="true"/>
											</td> 
										</tr>
										
										<tr> 
											<td align="right"> 
											  <label >Lota��o:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.lotacao}"  size="20" disabled="true"/>
											</td> 
										</tr>
									</table>
									
									
						</fieldset>
						
						<jsp:include page="proposta/historicoTipoPropostaVer.jsp" />
						
						
						
							<fieldset>
								<legend>Dados do Segurado Principal</legend>
									<table> 
										<tr> 
											<td align="right" width="75px"> 
											  <label>Nome:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.nomeSegurado}"  size="30" disabled="true"/>
											</td> 
										</tr> 
										<tr> 
											<td align="right"> 
											  <label>E-mail:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.email}" size="20" disabled="true"/>
											</td> 
										</tr>
									</table> 
									<table> 
										<tr> 
											<td align="right" width="75px"> 
											  <label >Sexo:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.sexoSegurado}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right"> 
											  <label >Data de Nascimento:</label> 
											</td> 
											<td> 
												<h:inputText value="#{verPropostaHandler.proposta.dataNascimentoSegurado}" disabled="true"  id="dtDataAgenciamento" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:inputText>
												<h:outputText id="txtIdadeSegurado" value="#{verPropostaHandler.idadeSegurado}" />
											</td> 
										
										</tr>
										<tr> 
											<td align="right"> 
											  <label >Matr�cula:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.matriculaSegurado}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right"> 
											  <label>CPF:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.cpfSegurado}" size="10" disabled="true"/>
											</td> 
										</tr>
										<tr> 
											<td align="right"> 
											  <label >RG:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.rgSegurado}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right"> 
											  <label>Estado Civil:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.estadoCivilSegurado}" size="10" disabled="true"/>
											</td> 
										</tr>
										<tr> 
											<td align="right"> 
											  <label >Profiss�o:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.profissaoSegurado}" size="20" disabled="true"/>
											</td> 
										</tr>
										
										<tr> 
											<td align="right"> 
											  <label  >Capital:</label> 
											</td> 
											<td> 
											   <h:outputText value="#{verPropostaHandler.proposta.valorCapitalSegurado}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
											</td> 
										</tr>
										
										<tr> 
											<td align="right"> 
											  <label  >Pr�mio:</label> 
											</td> 
											<td> 
												<h:outputText value="#{verPropostaHandler.proposta.valorPremioSegurado}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>
											</td> 
										</tr>
										
									</table>
						</fieldset>	
						
						<jsp:include page="proposta/historicoSeguradoPrincipalVer.jsp" />
						
						

							<fieldset>
								<legend>Dados do C�njuge</legend>
									<table> 
										<tr> 
											<td align="right" width="150px;"> 
											  <label>Nome:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.nomeConjuge}"  size="30" disabled="true"/>
											</td> 
										</tr> 
										<tr> 
											<td align="right"> 
											  <label >Sexo:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.sexoConjuge}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right"> 
											  <label >Data de Nascimento:</label> 
											</td> 
											<td> 
											   <h:outputText value="#{verPropostaHandler.proposta.dataNascimentoConjuge}"   >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
												</h:outputText>
											</td> 
										</tr>
										<tr> 
											<td align="right"> 
											  <label >Matr�cula:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.matriculaConjuge}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right"> 
											  <label>CPF:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.cpfConjuge}" size="10" disabled="true"/>
											</td> 
										</tr>
										<tr> 
											<td align="right"> 
											  <label >RG:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.rgConjuge}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right"> 
											  <label>Estado Civil:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.estadoCivilConjuge}" size="10" disabled="true"/>
											</td> 
										</tr>
										<tr> 
											<td align="right"> 
											  <label >Profiss�o:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.profissaoConjuge}" size="20" disabled="true"/>
											</td> 
										</tr>
										
										<tr> 
											<td align="right"> 
											  <label  >Capital:</label> 
											</td> 
											<td>
												<h:outputText value="#{verPropostaHandler.proposta.valorCapitalConjuge}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText> 
											</td> 
										</tr>
										
										<tr> 
											<td align="right"> 
											  <label  >Pr�mio:</label> 
											</td> 
											<td>
												<h:outputText value="#{verPropostaHandler.proposta.valorPremioConjuge}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText> 
											</td> 
										</tr>
										
									</table>
						</fieldset>		
						
						<jsp:include page="proposta/historicoConjugeVer.jsp" />
						
						<fieldset>
								<legend>Total dos Pr�mios</legend>
									<table> 
										<tr> 
											<td align="right" width="150px;"> 
											  <label>Total de Pr�mios:</label> 
											</td> 
											<td>
												<h:outputText value="#{verPropostaHandler.proposta.totalPremios}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText> 
											</td> 
										</tr> 
									</table>
						</fieldset>	
				
							<fieldset>
								<legend>Endere�o</legend>
									<table> 
										<tr> 
											<td align="right" width="150px;"> 
											  <label>Endere�o:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.enderecoSegurado}"  size="30" disabled="true"/>
											</td> 
										</tr>
										
										<tr> 
											<td align="right" width="150px;"> 
											 	<label>N�mero:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.numeroEnderecoSegurado}"  size="10" disabled="true"/>
											</td> 
											
											<td align="right" width="150px;"> 
											 	<label>Complemento:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.complemento}"  size="10" disabled="true"/>
											</td> 
										</tr> 
										
										<tr> 
											<td align="right" width="150px;"> 
											 	<label>Bairro:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.bairro}"  size="20" disabled="true"/>
											</td> 
											
										</tr> 
										
										<tr> 
											<td align="right" width="150px;"> 
											 	<label>Cidade:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.cidade.nom_cidade}"  size="15" disabled="true"/>
											</td> 
											
											<td align="right" width="25px;"> 
											 	<label>UF:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.cidade.cod_estado.sgl_estado}"  size="3" disabled="true"/>
											</td> 
										</tr> 
										
										<tr> 
											<td align="right" width="150px;"> 
											 	<label>Telefone Residencial:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.telefoneResidencial}"  size="12" disabled="true"/>
											</td> 
											
											<td align="right" width="75px;"> 
											 	<label>Telefone Comercial:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.telefoneComercial}"  size="12" disabled="true"/>
											</td> 
										</tr> 
										
										<tr> 
											<td align="right" width="150px;"> 
											 	<label>Telefone Celular:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.telefoneCelular}"  size="12" disabled="true"/>
											</td> 
											
											<td align="right" width="75px;"> 
											 	<label>Telefone Celular2:</label> 
											</td> 
											<td> 
											   <h:inputText value="#{verPropostaHandler.proposta.telefoneCelular2}"  size="12" disabled="true"/>
											</td> 
										</tr> 
										
										<tr> 
											<td align="right" width="150px;"> 
											 	<label>Observa��es:</label> 
											</td> 
											<td> 
											   <h:inputTextarea value="#{verPropostaHandler.proposta.observacao}"  cols="30" disabled="true"/>
											</td> 
									</table>
						</fieldset>
						
						<jsp:include page="proposta/historicoEnderecoVer.jsp" />
						
						<fieldset>	
									<legend>Benefici�rios</legend>
									<p:dataTable value="#{verPropostaHandler.proposta.beneficiario_collection}" 
												 var="beneficiario" width="50%" 
												 rendered="#{not empty verPropostaHandler.proposta.beneficiario_collection}" 
												 >
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Nome do Benefici�rio" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{beneficiario.nomeBeneficiario}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Parentesco" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{beneficiario.grauParentesco.descricao}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Percentual" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{beneficiario.percentual}"/>									  										
										</p:column>
									</p:dataTable>
									
									
								</fieldset>
								
								<jsp:include page="proposta/historicoBeneficiariosVer.jsp" />
								
								<fieldset>	
									<legend>Agenciador</legend>
					
									<p:dataTable value="#{verPropostaHandler.proposta.propostaagenciador_collection}" 
												 var="agenciador" width="50%" 
												 rendered="#{not empty verPropostaHandler.proposta.propostaagenciador_collection}" 
												 >
										
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Nome do Agenciador" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.funcionario.nome}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Percentual" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.funcionario.percentualComissao}"/>									  										
										</p:column>	
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Comiss�o Adicional" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.percentual}"/>									  										
										</p:column>
										<p:column resizable="true" >
										  <f:facet name="header">
										  <h:outputText value="Valor Definido" /> 
										  	
										  </f:facet>
										  <h:outputText value="#{agenciador.valorPagoDefinido}" ><f:convertNumber currencySymbol="R$" type="currency" groupingUsed="true"/></h:outputText>									  										
										</p:column>
									</p:dataTable>
								</fieldset>
								<fieldset>
									<legend>Aprova��o</legend>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label  >Data Agenciamento:</label>
												</td>
												<td>
													<h:outputText value="#{verPropostaHandler.proposta.dataAgenciamento}"  >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>
											<tr>
												<td align="right" width="150px;">
												  <label  >Data Aprova��o:</label>
												</td>
												<td>
													<h:outputText value="#{verPropostaHandler.proposta.dataAprovacao}"   >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>									
											<tr>
												<td align="right" width="150px;">
												  <label  >Data do Cadastro:</label>
												</td>
												<td>
													<h:outputText value="#{verPropostaHandler.proposta.dataCadastro}"  >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>	
										</table>	
							</fieldset>
							<fieldset>
									<legend>Informa��es</legend>
										<table>
											<tr> 
												<td align="right" width="150px;"> 
												  <label >N�mero da Proposta:</label> 
												</td> 
												<td> 
												   <h:outputText value="#{verPropostaHandler.proposta.id}" id="txtNumPropostaReadOnly" />
												</td> 
											</tr> 
										</table>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label >N�mero do Sorteio:</label>
												</td>
												<td>
													<h:outputText value="#{verPropostaHandler.proposta.numeroSorteio}" />
													
												</td>								
											</tr>																		
										</table>
								</fieldset>
								<fieldset>
									<legend>Sorteio</legend>
										<table>
											<tr>
												<td align="right" width="300px;">
												  <label >Segurado Contemplado com Sorteio em:</label>
												</td>
												<td>
													<h:outputText value="#{verPropostaHandler.proposta.dataContemplacao}"  id="dtContemplacaoSorteio" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>											
										</table>
								</fieldset>
								<fieldset>
									<legend>Cancelamento</legend>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label for="dtDataCancelamento" >Data Cancelamento:</label>
												</td>
												<td>
													<h:outputText value="#{verPropostaHandler.proposta.dataCancelamento}"  id="dtDataCancelamento" >
														<f:convertDateTime dateStyle="default" pattern="dd/MM/yyyy" locale="pt_BR" />
													</h:outputText>
												</td>
											</tr>											
										</table>
										<table>
											<tr>
												<td align="right" width="150px;">
												  <label >Motivo do Cancelamento:</label>
												</td>
												<td>
												<h:outputText value="#{verPropostaHandler.proposta.motivoCancelamento.descricao}" /> 	
												</td>								
											</tr>																		
										</table>
									</fieldset>	
														
							
							
						</h:form>
						
				</DIV>
			</DIV>
			<DIV class=both><!-- --></DIV>
		</DIV>
		<DIV id=footer_top_bg><!-- --></DIV>
	</DIV>
</DIV>
<jsp:include page="common/footer.jsp" />
</DIV>
<DIV id=foo_glass></DIV><BR><BR></DIV>


</BODY>

</HTML>
</f:view>
