<%@ include file="common/cabecalho.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
	<a4j:keepAlive beanName="geraArquivoHandler" />


	<HTML xmlns="http://www.w3.org/1999/xhtml">
<p:resources exclude="/jquery/jquery.js" />
<jsp:include page="common/head.jsp" />

<BODY class=f-default>
	<script>
		jQuery(function(jQuery) {
			jQuery("#competencia").mask("99/9999", {
				placeholder : "_"
			});

		});
	</script>

	<DIV id=background_2>
		<DIV id=site_wrapper>
			<jsp:include page="common/menu.jsp" />
			<DIV id=wrap>
				<DIV id=content>
					<DIV id=main>
						<DIV id=main_content>
							<DIV id=main_body>

								<div id="breadcrumb">
									<span>Movimentação</span>
									<li>Atualizar Valor das Propostas</li>
								</div>
								<div id="icone">
									<img src="images/cadOrgaos.png" />
								</div>


								<h:form styleClass="niceform" prependId="false">
									<div class="titulo_agrupamento_Dados"></div>
									<fieldset>
										<legend>Atualizar</legend>
										<div class="divRecuadoEsquerdo">
											<table>
												<tr>
													<td></td>
													<td><p:commandButton
															actionListener="#{geraArquivoHandler.atualizarPropostas}"
															value="Atualizar Valor das Propostas" id="gerar" styleClass="button"
															async="false" ajax="false" /></td>
												</tr>
											</table>
										</div>
									</fieldset>
								</h:form>
							</DIV>
						</DIV>
						<DIV class=both>
							<!-- -->
						</DIV>
					</DIV>
					<DIV id=footer_top_bg>
						<!-- -->
					</DIV>
				</DIV>
			</DIV>
			<jsp:include page="common/footer.jsp" />
		</DIV>
		<DIV id=foo_glass></DIV>
		<BR> <BR>
	</DIV>

</BODY>

	</HTML>
</f:view>
