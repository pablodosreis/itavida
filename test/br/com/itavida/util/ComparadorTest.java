package br.com.itavida.util;

import java.util.Date;

import org.apache.tools.ant.util.DateUtils;

public class ComparadorTest {

	public static void testHouveAlteracaoTextual() {
		
		if(Comparador.houveAlteracao("ABC", "ABC")) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao("AB", "ABC")) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao(null, "ABC")) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao("ABC", null)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(Comparador.houveAlteracao(null, null)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		System.out.println("Test texto ok");
	}
	
	public static void testHouveAlteracaoDouble() {
		
		if(Comparador.houveAlteracao(1.001, 1.001)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao(1.2, 1.199999)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao(null, 1.2)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao(1.9090, null)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(Comparador.houveAlteracao(null, null)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		System.out.println("Test double ok");
	}

	public static void testHouveAlteracaoData() {
		
		Date hoje = DateUtil.getHoje();
		Date amanha = DateUtil.getAmanha();
		
		if(Comparador.houveAlteracao(hoje, hoje)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao(hoje, amanha)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao(null, amanha)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(!Comparador.houveAlteracao(amanha, null)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		if(Comparador.houveAlteracao(null, null)) {
			throw new IllegalArgumentException("ERRO");
		}
		
		System.out.println("Test date ok");
	}

	public static void main(String[] args) {
		testHouveAlteracaoTextual();
		testHouveAlteracaoDouble();
		testHouveAlteracaoData();
	}
	
}
