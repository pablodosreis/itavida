package br.com.itavida.backup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.quartz.JobExecutionException;

public class TestBackup {

	public static void main (String[] args) throws JobExecutionException, IOException {
		//BackupJob job = new BackupJob();
		//job.execute(null);
		
		final URL url = new URL("https://www.ifood.com.br/delivery/complementos-item?rid=27797&code=25036642&qty=1&index=0");
		final URLConnection urlConnection = url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setRequestProperty("Content-Type", "text/html; charset=utf-8");
		urlConnection.connect();
		final OutputStream outputStream = urlConnection.getOutputStream();
		final InputStream inputStream = urlConnection.getInputStream();
		
		String result = getStringFromInputStream(inputStream);

		System.out.println(result);
		System.out.println("Done");

		
	}
	
	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {

			BufferedReader br = null;
			StringBuilder sb = new StringBuilder();

			String line;
			try {

				br = new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			return sb.toString();

		}

}
