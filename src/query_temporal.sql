select 
concat('''', lpad(month(a.competencia),2, '0'), '/', year(a.competencia), '''') competencia
,0 as media_idade 
,sum(IFNULL(i.premio_segurado,0) + IFNULL(i.premio_conjuge,0)) premios_total
,sum(IFNULL(i.capital_segurado,0)) capital
,count(distinct i.proposta_id)  quantidade_propostas
,count(distinct i.orgao_id) total_orgaos
,sinistros.quantidade total_sinistros 
,0 homens
,0 mulheres


,sum(IFNULL(i.premio_segurado,0))  premio_segurado
,sum(IFNULL(i.premio_conjuge,0))  premio_conjuge
,sinistros.valor valor_sinistros
,concat('''',a.competencia, '''')  competencia_completa

from arquivo_mensal a 
inner join item_mensal i on i.arquivo_mensal_id = a.id 
inner join 
(select 
concat( year(dataSinistro), '-', lpad(month(dataSinistro),2, '0'), '-01' ) competencia,
count(*) quantidade,
sum(ifnull(valorSinistro,0) + ifnull(valorCestaBasica,0) + ifnull(valorFuneral,0)) valor 
from historicosinistro
group by year(dataSinistro),month(dataSinistro)
) sinistros on sinistros.competencia = a.competencia
group by a.competencia