package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.hibernate.Session;

import br.com.itavida.dao.ContasVencerDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.DetalhePagamentoDAO;
import br.com.itavida.dao.PagamentoDAO;
import br.com.itavida.entidades.Conta;
import br.com.itavida.entidades.ContasVencer;
import br.com.itavida.entidades.Despesa;
import br.com.itavida.entidades.DetalhePagamento;
import br.com.itavida.entidades.Filial;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.GrupoDespesa;
import br.com.itavida.entidades.Pagamento;
import br.com.itavida.entidades.TipoDocumento;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class PagamentoHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private Pagamento pagamento;
	private Pagamento pagamentoFiltro;
	private List<Pagamento> resultadoPesquisa;
	private List<DetalhePagamento> resultadoPesquisaRelatorio;
	private HtmlSelectOneMenu tipoContaSelecionado;
	private HtmlSelectOneMenu tipoDocumentoSelecionado;
	private Double subTotal;
	Map<String, Object[]> mapaRelatorioFinanceiro;
	private Integer tipoRelatorio;
	
	private DetalhePagamento detalhePagamento;
	private DetalhePagamento detalhePagamentoFiltro;
	private List<DetalhePagamento> detalhePagamentoExcluidos;
	private boolean setarFoco;
	private List<ContasVencer> listaContasNaoPagas;
	private ContasVencer[] contasVencerSelecionadas;
	org.primefaces.component.datatable.DataTable dataTable;

	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public PagamentoHandler(){
		Session session = HibernateUtil.currentSession();
		ContasVencerDAO contasDao = new ContasVencerDAO( session );
		listaContasNaoPagas = contasDao.buscaContasNaoPagas();
		
		initFiltros();
		
		cancelarIncluir( null );
	}

	private void initFiltros() {
		pagamentoFiltro = new Pagamento();
		pagamentoFiltro.setConta( new Conta() );
		pagamentoFiltro.setTipoDocumento( new TipoDocumento() );
		pagamentoFiltro.setDetalhePagamentoFiltro( new DetalhePagamento() );
		pagamentoFiltro.getDetalhePagamentoFiltro().setFilial(new Filial());
		pagamentoFiltro.getDetalhePagamentoFiltro().setFuncionario( new Funcionario() );
		pagamentoFiltro.getDetalhePagamentoFiltro().setGrupoDespesa( new GrupoDespesa() );
		pagamentoFiltro.getDetalhePagamentoFiltro().setDespesa( new Despesa() );
	}
	
	/**
	 * Popula um combo com valores das contas
	 * @return
	 */
	public List<SelectItem> getTipoContaCombo(){
		
		Session session = HibernateUtil.currentSession();
		Dao<Conta, Integer> dao = new Dao<Conta, Integer>( session, Conta.class );
		List<Conta> listaContas = dao.list();
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		for (Iterator iterator = listaContas.iterator(); iterator.hasNext();) {
			Conta conta = (Conta) iterator.next();
			retorno.add(new SelectItem(conta.getId(), conta.getDescricao()));
		}
		
		return retorno;
	}
	
	/**
	 * Popula um combo com valores dos tipos de documentos
	 * @return
	 */
	public List<SelectItem> getTipoDocumentosCombo(){
		
		Session session = HibernateUtil.currentSession();
		Dao<TipoDocumento, Integer> dao = new Dao<TipoDocumento, Integer>( session, TipoDocumento.class );
		List<TipoDocumento> listaTipoDocumentos = dao.list();
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		for (Iterator iterator = listaTipoDocumentos.iterator(); iterator.hasNext();) {
			TipoDocumento tipoDocumento = (TipoDocumento) iterator.next();
			retorno.add(new SelectItem(tipoDocumento.getId(), tipoDocumento.getDescricao()));
		}
		
		return retorno;
	}
	
	/**
	 * Popula um combo com valores dos tipos de relatorio
	 * @return
	 */
	public List<SelectItem> getTipoRelatorioCombo(){
		
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add(new SelectItem(1, "Por Filial"));
		retorno.add(new SelectItem(2, "Por Funcion�rio"));
		retorno.add(new SelectItem(3, "Por Grupo de Despesa"));
		retorno.add(new SelectItem(4, "Por Despesa"));
		retorno.add(new SelectItem(5, "Por Conta"));
		retorno.add(new SelectItem(6, "Por Tipo de Documento"));
		
		return retorno;
	}
	
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaPagamentos(ActionEvent e){
		
		org.primefaces.component.datatable.DataTable dataTableEL = ( org.primefaces.component.datatable.DataTable )e.getComponent().findComponent("resultTable");
		dataTableEL.setPage(0);
		dataTableEL.setFirst(0);
		
		//Recupera o item escolhido
		Session session = HibernateUtil.currentSession();
		PagamentoDAO dao = new PagamentoDAO( session );
		
		
		List<Pagamento> lista = dao.buscaPorFiltro( pagamentoFiltro  );
		
		setResultadoPesquisa(lista);
			
	}
	
	public String ajaxCall(){
		return null;
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public String relatorioPagamentoPesquisar( ){
		
		Session session = HibernateUtil.currentSession();
		PagamentoDAO dao = new PagamentoDAO( session );
		List<DetalhePagamento> lista = dao.buscaPorFiltroRelatorio( pagamentoFiltro  );
		//Realiza a quebra por filial
		Map<String, Object[]> mapa = new HashMap<String, Object[]>();
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			DetalhePagamento detalhePagamento = (DetalhePagamento) iterator.next();
			String chave = "";
			boolean inserirMap = true;
			if( tipoRelatorio == 1 ){
				chave = "Filial: " + detalhePagamento.getFilial().getNomeFilial();
			}
			if( tipoRelatorio == 2 ){
				if( detalhePagamento.getFuncionario() != null ){
					chave = "Funcion�rio: " + detalhePagamento.getFuncionario().getNome();
				}else{
					inserirMap = false;
				}
			}
			if( tipoRelatorio == 3 ){
				chave = "Grupo de Despesa: " + detalhePagamento.getGrupoDespesa().getNomeGrupoDespesa();
			}
			if( tipoRelatorio == 4 ){
				chave = "Despesa: " + detalhePagamento.getDespesa().getNomeDespesa();
			}
			if( tipoRelatorio == 5 ){
				chave = "Conta: " +detalhePagamento.getPagamento().getConta().getDescricao();
			}
			
			if( tipoRelatorio == 6 ){
				chave = "Tipo de Documento: " +detalhePagamento.getPagamento().getTipoDocumento().getDescricao();
			}
			if( inserirMap ){
				if( mapa.containsKey(chave) ){
					Object[] listaValorChave = mapa.get(chave);
					List<DetalhePagamento> listaDetalhes =  ( List<DetalhePagamento> )	listaValorChave[0];		
					listaDetalhes.add( detalhePagamento );
					Double subTotal = ( Double ) listaValorChave[1];
					subTotal += detalhePagamento.getValor();
					listaValorChave[1] = subTotal;
				}else{
					Object[] listaValorChave = new Object[2];
					List<DetalhePagamento> listaDetalhes = new ArrayList<DetalhePagamento>();
					listaDetalhes.add( detalhePagamento );
					listaValorChave[0] = listaDetalhes;
					listaValorChave[1] = detalhePagamento.getValor();
					mapa.put( chave , listaValorChave );
				}
			}
		}
		
		setMapaRelatorioFinanceiro( mapa );
		
		return "resultado";
			
	}
	
    public List<String> getMapKeys(){
        List<String> ret = new ArrayList<String>();
        for (String s : mapaRelatorioFinanceiro.keySet())
            ret.add(s);
        return ret;
    }
	
    public Double getTotalRelatorio(){
        Double total = 0.0;
        for (String chave : mapaRelatorioFinanceiro.keySet()){
        	Object[] listaValorChave = mapaRelatorioFinanceiro.get(chave);
        	Double subTotal = ( Double ) listaValorChave[1];
        	total += subTotal;
        }
        return total;
    }
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<Pagamento, Integer> dao = new Dao<Pagamento, Integer>(session, Pagamento.class);
			Dao<DetalhePagamento, Integer> daoDetalhePagamento = new Dao<DetalhePagamento, Integer>(session, DetalhePagamento.class);
			Dao<ContasVencer, Integer> daoContasVencer = new Dao<ContasVencer, Integer>(session, ContasVencer.class);
			ContasVencerDAO contasVencerDAO = new ContasVencerDAO(session);
			Pagamento pagamentoGravar = getPagamento();
			
			for (Iterator iterator = pagamentoGravar.getDetalhepagamento_collection().iterator(); iterator.hasNext();) {
				DetalhePagamento element = (DetalhePagamento) iterator.next();
				if( element.getFuncionario() != null && ( element.getFuncionario().getId() == null ||  element.getFuncionario().getId() == 0) ){
					element.setFuncionario( null );
				}
				element.setPagamento( pagamentoGravar );
				
				//Realiza baixa na conta
				ContasVencer contaVencer = element.getContasVencer();
				if( contaVencer != null ){
					contaVencer.setContaPaga("S");
					contaVencer.setDataPagamento( element.getPagamento().getDataDocumento() );
					daoContasVencer.merge( contaVencer );
				}
				
			}
			
			//Exclui os detalhes pagamentos marcados para exclus�o
			for (Iterator iterator = getDetalhePagamentoExcluidos().iterator(); iterator.hasNext();) {
				DetalhePagamento contatoExcluido = (DetalhePagamento) iterator.next();	
				daoDetalhePagamento.delete( contatoExcluido );
				
				//Busca a Conta Vencer relacionada que j� foi paga para voltar ao status de aberta
				ContasVencer contasFiltro = new ContasVencer();
				contasFiltro.setFilial( contatoExcluido.getFilial() );
				contasFiltro.setDespesa( contatoExcluido.getDespesa() );
				contasFiltro.setFuncionario( contatoExcluido.getFuncionario() );
				contasFiltro.setGrupoDespesa( contatoExcluido.getGrupoDespesa() );	
				contasFiltro.setValorDespesa( contatoExcluido.getValor() );
				contasFiltro.setDataPagamento( pagamentoGravar.getDataDocumento() );
				contasFiltro.setContaPaga("S");
				List<ContasVencer> contasRecuperadas = contasVencerDAO.busca( contasFiltro );
				if( contasRecuperadas != null && contasRecuperadas.size() == 1 ){
					ContasVencer contaRecuperada = contasRecuperadas.get(0);
					contaRecuperada.setContaPaga("N");
					contaRecuperada.setDataPagamento(null);
					daoContasVencer.merge( contaRecuperada );
				}
				
			}
			//Zera a lista de detalhes pagamentos excluidos
			setDetalhePagamentoExcluidos( new ArrayList<DetalhePagamento>() );
			
					
			//persiste os dados
			dao.merge( pagamentoGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelarGravar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		
		if( getPagamento().getDetalhepagamento_collection() == null ||  getPagamento().getDetalhepagamento_collection().size() == 0 ){
			addErrorMessage( "� necess�rio incluir pelo menos uma Conta a ser paga!" );
			return false;
		}
		
		//Verifica se o total e o subtotal bateram
		if( !( getPagamento().getValor().doubleValue() == getSubTotal().doubleValue() )  ){
			addErrorMessage( "O Som�torio de Despesas N�O est� igual ao Valor Total informado. Favor rever os c�lculos!" );
			return false;
		}
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("cmbConta", "Conta");
		mapaCampos.put("cmbTipoDocumento", "Tipo de Documento");
		mapaCampos.put("txtNumeroDocumento", "N�mero do Documento");
		mapaCampos.put("dtDataDocumento", "Data do Documento");
		mapaCampos.put("txtValorDocumento", "Valor do Documento");		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	public String exportarExcel(){
		return "exportarExcelRelatorioPagamento";
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormularioDetalhePagamento(ActionEvent e){
		
		boolean retorno = validaPreenchimentoObrigatorioListaSelecao(getDetalhePagamento().getFilial().getId(), "Filial");
		if( !retorno ){
			return retorno;
		}
		
		retorno = validaPreenchimentoObrigatorioListaSelecao(getDetalhePagamento().getDespesa().getId(), "Despesa" );
		if( !retorno ){
			return retorno;
		}
		
		retorno = validaPreenchimentoObrigatorioListaSelecao(getDetalhePagamento().getGrupoDespesa().getId(), "Grupo de Despesa" );
		if( !retorno ){
			return retorno;
		}
		
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarPagamento( ){
		
		Session session = HibernateUtil.currentSession();
		DetalhePagamentoDAO contatoDAO =  new DetalhePagamentoDAO( session );
		List<DetalhePagamento> listaDetalhesPagamentos = contatoDAO.buscaPorPagamento( pagamento.getId() );
		getPagamento().setDetalhepagamento_collection(listaDetalhesPagamentos);
		
		double total = 0.0;
		for (Iterator iterator = getPagamento().getDetalhepagamento_collection().iterator(); iterator.hasNext();) {
			DetalhePagamento element = (DetalhePagamento) iterator.next();
			total += element.getValor();
		}
		
		setSubTotal(total);
		
		resultadoPesquisa = null;
		
		return "incluirPagamento";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirPagamento(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirPagamento");
		Pagamento pagamentoDeletado  = ( Pagamento ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<Pagamento, Integer> dao = new Dao<Pagamento, Integer>(session, Pagamento.class);
		Dao<ContasVencer, Integer> daoContasVencer = new Dao<ContasVencer, Integer>(session, ContasVencer.class);
		ContasVencerDAO contasVencerDao = new ContasVencerDAO( session );
		dao.delete(pagamentoDeletado);	
		
		//Busca as contas associadas e seta como n�o vencidas
		
		for (DetalhePagamento detalhePagamento : pagamentoDeletado.getDetalhepagamento_collection() ) {
			ContasVencer contasFiltro = new ContasVencer();
			contasFiltro.setFilial( detalhePagamento.getFilial() );
			contasFiltro.setDespesa( detalhePagamento.getDespesa() );
			contasFiltro.setFuncionario( detalhePagamento.getFuncionario() );
			contasFiltro.setGrupoDespesa( detalhePagamento.getGrupoDespesa() );			
			contasFiltro.setContaPaga( "S" );
			contasFiltro.setDataPagamento( pagamentoDeletado.getDataDocumento() );
			
			List<ContasVencer> contas = contasVencerDao.busca( contasFiltro );
			if( contas != null && contas.size() > 0 ){
				ContasVencer conta = contas.get(0);
				conta.setContaPaga( "N" );
				conta.setDataPagamento( null );
				daoContasVencer.merge(conta);
			}
			
		}
		
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaPagamentos(e);
	}	
	
	
	
	/**
	 * Realiza a inclus�o de um novo contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void incluirDetalhePagamento( ActionEvent e ){
		
		if( validarFormularioDetalhePagamento(e) ){
			
			getPagamento().getDetalhepagamento_collection().remove( getDetalhePagamento() );
			getPagamento().getDetalhepagamento_collection().add( getDetalhePagamento() );
			
			double total = 0.0;
			for (Iterator iterator = getPagamento().getDetalhepagamento_collection().iterator(); iterator.hasNext();) {
				DetalhePagamento element = (DetalhePagamento) iterator.next();
				total += element.getValor();
			}
			
			setSubTotal(total);
			
			cancelarDetalhePagamento(e);
			setarFoco = true;
		}
	}
	
	/**
	 * Realiza a exclus�o de um contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void excluirDetalhePagamento( ActionEvent e ){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirDetalhePagamento");
		DetalhePagamento detalhePagamento = ( DetalhePagamento ) param.getValue();
		if( detalhePagamentoExcluidos == null ){
			detalhePagamentoExcluidos = new ArrayList<DetalhePagamento>();
		}
		getDetalhePagamentoExcluidos().add( detalhePagamento );
		getPagamento().getDetalhepagamento_collection().remove( detalhePagamento );
		
		//Recalcula total
		double total = 0.0;
		for (Iterator iterator = getPagamento().getDetalhepagamento_collection().iterator(); iterator.hasNext();) {
			DetalhePagamento element = (DetalhePagamento) iterator.next();
			total += element.getValor();
		}
		
		setSubTotal(total);
						
	}
	
	
	/**
	 * Realiza a altera��o de um contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void alterarDetalhePagamento( ActionEvent e ){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("editDetalhePagamento");
		DetalhePagamento detalhePagamento = ( DetalhePagamento ) param.getValue();
		setDetalhePagamento( detalhePagamento );
		
		double total = 0.0;
		for (Iterator iterator = getPagamento().getDetalhepagamento_collection().iterator(); iterator.hasNext();) {
			DetalhePagamento element = (DetalhePagamento) iterator.next();
			total += element.getValor();
		}
		
		setSubTotal(total);
						
	}
	
	/**
	 * Realiza a inclus�o de um novo contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void cancelarDetalhePagamento( ActionEvent e ){		
		setDetalhePagamento( new DetalhePagamento() );	
		getDetalhePagamento().setFilial(new Filial());
		getDetalhePagamento().setFuncionario( new Funcionario() );
		getDetalhePagamento().setGrupoDespesa( new GrupoDespesa() );
		getDetalhePagamento().setDespesa( new Despesa() );
		setarFoco = false;
	}
	
	/**
	 * Realiza a inclus�o de um novo contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void popularContasSelecionadas( ActionEvent e ){		
	
		if( contasVencerSelecionadas == null || contasVencerSelecionadas.length == 0 ){
			addErrorMessage( "� necess�rio selecionar pelo menos uma conta." );
		}else{
		
			List<DetalhePagamento> listaDetalhes = new ArrayList<DetalhePagamento>();
			DetalhePagamento detalhe = null;
			boolean existeConta = false;
			for (int i = 0; i < contasVencerSelecionadas.length; i++) {
				ContasVencer conta = contasVencerSelecionadas[i];
				
				//Verifica se j� n�o existe a conta inserida:
				if( getPagamento().getDetalhepagamento_collection() != null ){
					for (DetalhePagamento detalhePagamentoTemp : getPagamento().getDetalhepagamento_collection()) {
						if( detalhePagamentoTemp.getDespesa().getId().equals( conta.getDespesa().getId() ) 
						 && detalhePagamentoTemp.getFilial().getId().equals( conta.getFilial().getId() ) 
						 && detalhePagamentoTemp.getGrupoDespesa().getId().equals( conta.getGrupoDespesa().getId() )
						 && detalhePagamentoTemp.getValor().equals( conta.getValorDespesa() ) ){
							addErrorMessage( "Existem Contas selecionadas que j� foram inclu�das nos Detalhes do Pagamento." );
							addErrorMessage( "Por favor, revise sua sele��o!" );
							existeConta = true;
							break;
						}
					}
				}
				
				if( existeConta ){
					break;
				}
				
				detalhe = new DetalhePagamento();
				detalhe.setDespesa( conta.getDespesa() );
				detalhe.setFilial( conta.getFilial() );
				detalhe.setFuncionario( conta.getFuncionario() );
				detalhe.setGrupoDespesa( conta.getGrupoDespesa() );
				detalhe.setValor( conta.getValorDespesa() );
				detalhe.setContasVencer( conta );
				listaDetalhes.add( detalhe );
			}
			
			if( !existeConta ){
				pagamento.getDetalhepagamento_collection().addAll( listaDetalhes );
				double total = 0.0;
				for (Iterator iterator = getPagamento().getDetalhepagamento_collection().iterator(); iterator.hasNext();) {
					DetalhePagamento element = (DetalhePagamento) iterator.next();
					total += element.getValor();
				}
				
				setSubTotal(total);
				
				cancelarDetalhePagamento(e);
				setarFoco = true;				
			}
			
			
		}
		
		
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelarGravar( ActionEvent e ){
		Conta conta = getPagamento().getConta();
		TipoDocumento tipoDoc = getPagamento().getTipoDocumento();
		// Realiza o cancelamento das a��es
		setPagamento( new Pagamento() );
		getPagamento().setConta( conta );
		getPagamento().setTipoDocumento( tipoDoc );
		setDetalhePagamento( new DetalhePagamento() );
		getPagamento().setDetalhepagamento_collection( new ArrayList<DetalhePagamento>() );
		detalhePagamentoExcluidos = new ArrayList<DetalhePagamento>();
		this.resultadoPesquisa = null;
		subTotal = 0.0;
		contasVencerSelecionadas = null;
		listaContasNaoPagas = new ArrayList<ContasVencer>();
		cancelarDetalhePagamento( e );
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelarIncluir( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setPagamento( new Pagamento() );
		getPagamento().setConta(new Conta());
		getPagamento().setTipoDocumento( new TipoDocumento() );
		setDetalhePagamento( new DetalhePagamento() );
		getPagamento().setDetalhepagamento_collection( new ArrayList<DetalhePagamento>() );
		detalhePagamentoExcluidos = new ArrayList<DetalhePagamento>();
		this.resultadoPesquisa = null;
		subTotal = 0.0;
		cancelarDetalhePagamento( e );
	}
	
	
// Getters and Setters =============================================================================================
	
	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public List<Pagamento> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Pagamento> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public HtmlSelectOneMenu getTipoContaSelecionado() {
		return tipoContaSelecionado;
	}

	public void setTipoContaSelecionado(HtmlSelectOneMenu tipoContaSelecionado) {
		this.tipoContaSelecionado = tipoContaSelecionado;
	}


	public DetalhePagamento getDetalhePagamento() {
		return detalhePagamento;
	}

	public void setDetalhePagamento(DetalhePagamento detalhePagamento) {
		this.detalhePagamento = detalhePagamento;
	}


	public List<DetalhePagamento> getDetalhePagamentoExcluidos() {
		return detalhePagamentoExcluidos;
	}

	public void setDetalhePagamentoExcluidos(List<DetalhePagamento> detalhePagamentoExcluidos) {
		this.detalhePagamentoExcluidos = detalhePagamentoExcluidos;
	}	
	
	public HtmlSelectOneMenu getTipoDocumentoSelecionado() {
		return tipoDocumentoSelecionado;
	}

	public void setTipoDocumentoSelecionado(
			HtmlSelectOneMenu tipoDocumentoSelecionado) {
		this.tipoDocumentoSelecionado = tipoDocumentoSelecionado;
	}	
	

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	
	public boolean isSetarFoco() {
		return setarFoco;
	}

	public void setSetarFoco(boolean setarFoco) {
		this.setarFoco = setarFoco;
	}
	
	public List<DetalhePagamento> getResultadoPesquisaRelatorio() {
		return resultadoPesquisaRelatorio;
	}

	public void setResultadoPesquisaRelatorio(
			List<DetalhePagamento> resultadoPesquisaRelatorio) {
		this.resultadoPesquisaRelatorio = resultadoPesquisaRelatorio;
	}
	
	public Map<String, Object[]> getMapaRelatorioFinanceiro() {
		return mapaRelatorioFinanceiro;
	}
	
	public void setMapaRelatorioFinanceiro(
			Map<String, Object[]> mapaRelatorioFinanceiro) {
		this.mapaRelatorioFinanceiro = mapaRelatorioFinanceiro;
	}
	
	public Integer getTipoRelatorio() {
		return tipoRelatorio;
	}
	
	public void setTipoRelatorio(Integer tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	public List<ContasVencer> getListaContasNaoPagas() {
		return listaContasNaoPagas;
	}

	public void setListaContasNaoPagas(List<ContasVencer> listaContasNaoPagas) {
		this.listaContasNaoPagas = listaContasNaoPagas;
	}

	public ContasVencer[] getContasVencerSelecionadas() {
		return contasVencerSelecionadas;
	}

	public void setContasVencerSelecionadas(ContasVencer[] contasVencerSelecionadas) {
		this.contasVencerSelecionadas = contasVencerSelecionadas;
	}

	public Pagamento getPagamentoFiltro() {
		return pagamentoFiltro;
	}

	public void setPagamentoFiltro(Pagamento pagamentoFiltro) {
		this.pagamentoFiltro = pagamentoFiltro;
	}

	public DetalhePagamento getDetalhePagamentoFiltro() {
		return detalhePagamentoFiltro;
	}

	public void setDetalhePagamentoFiltro(DetalhePagamento detalhePagamentoFiltro) {
		this.detalhePagamentoFiltro = detalhePagamentoFiltro;
	}

	public org.primefaces.component.datatable.DataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(org.primefaces.component.datatable.DataTable dataTable) {
		this.dataTable = dataTable;
	}
	
	
	
	// Fim Getters and Setters =====================================================================================================
	


	
}
