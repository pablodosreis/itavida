package br.com.itavida.handlers;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import br.com.itavida.dao.BeneficiarioDAO;
import br.com.itavida.dao.CapitalSeguradoFaixaEtariaDAO;
import br.com.itavida.dao.CapitalSeguradoLimiteIdadeDAO;
import br.com.itavida.dao.ComissaoAgenciadorDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.HistoricoBeneficiarioDAO;
import br.com.itavida.dao.HistoricoCapitalDAO;
import br.com.itavida.dao.HistoricoConjugeDAO;
import br.com.itavida.dao.HistoricoEnderecoSeguradoDAO;
import br.com.itavida.dao.HistoricoPropostaDAO;
import br.com.itavida.dao.MotivoCancelamentoDAO;
import br.com.itavida.dao.MovimentacaoPropostaInadimplenteDAO;
import br.com.itavida.dao.NomeTabelaDAO;
import br.com.itavida.dao.NumeroSorteioDAO;
import br.com.itavida.dao.OrgaoQuitacaoDAO;
import br.com.itavida.dao.PropostaAgenciadorDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dao.TipoPropostaDAO;
import br.com.itavida.dto.MovimentacaoPagamento;
import br.com.itavida.entidades.Beneficiario;
import br.com.itavida.entidades.CapitalSeguradoFaixaEtaria;
import br.com.itavida.entidades.CapitalSeguradoLimiteIdade;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.ComissaoAgenciador;
import br.com.itavida.entidades.ComportamentoPropostaEnum;
import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.GrauParentesco;
import br.com.itavida.entidades.HistoricoBeneficiario;
import br.com.itavida.entidades.HistoricoCapital;
import br.com.itavida.entidades.HistoricoConjuge;
import br.com.itavida.entidades.HistoricoEnderecoSegurado;
import br.com.itavida.entidades.HistoricoProposta;
import br.com.itavida.entidades.ModeloProposta;
import br.com.itavida.entidades.MotivoCancelamento;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.entidades.NumeroSorteio;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.OrgaoQuitacao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.PropostaAgenciador;
import br.com.itavida.entidades.TipoProposta;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.util.Comparador;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;
import br.com.itavida.util.NumberUtil;

public class SuperApHandler extends MainHandler implements Serializable{

	private static Logger logger = Logger.getLogger( SuperApHandler.class );
	
	private static final long serialVersionUID = 1L;
	//Dados para Tela de Filtro
	private Proposta propostaFiltro;
	private Integer numeroPropostaIncluir;
	private Integer numeroPropostaCopiar;
	private Integer numeroPropostaParameter;
	private List<Proposta> resultadoPesquisa;
	private HtmlSelectOneMenu modeloPropostaSelecionado;
	private HtmlSelectOneMenu tipoPropostaSelecionado;
	private List<SelectItem> tipoPropostaComboFiltro;
	private List<SelectItem> opcoesAgenciadorCombo;
	private List<SelectItem> opcoesEmpresaCombo;
	private List<SelectItem> opcoesMotivoCancelamento;
	private List<SelectItem> opcoesTipoComissao;

	private Boolean fluxoInclusao;
	private Beneficiario beneficiario;
	private Beneficiario beneficiarioTemp;
	private List<Beneficiario> beneficiariosExcluidos;
	private PropostaAgenciador agenciador;
	private List<PropostaAgenciador> agenciadoresExcluidos;
	private String idadeSegurado;
	private String idadeConjuge;
	private Integer tipoRelatorio;
	private Boolean alterarBeneficiario;
	
	//Dados para Tela de Inclus�o/Altera��o
	private Proposta proposta;
	private String numeroSorteio;
	private Double valorCapitalSegurado;
	private Double valorCapitalSeguradoFuturo;
	private Proposta[] propostasSelecionadas;	
	private Integer totalRegistrosRelatorio;
	private Double valorTotalRegistrosRelatorio;
	private Integer totalRegistrosCancelados;
	private Integer totalRegistrosAtivos;
	private Integer totalRegistrosSemAverbacao;
	private Double valorTotalPremioAtivos;
	private HtmlSelectOneMenu tipoOrgaoSelecionado;
	
	
	/*valores fixo*/
	private Double fixoCapitalFuturo;
	private Double fixoPremioFuturo;
	private Double fixoCapitalConjugeFuturo;
	private Double fixoPremioConjugeFuturo;
	
	private String tipoComissao;
	
	/*filtros da consulta*/
	private List<String> situacaoPropostaSelectedList = new ArrayList<String>();
	private List<SelectItem> opcoesSituacaoPropostaCombo;
	
	private List<MovimentacaoPagamento> historicoMovimentacoes;
	
	public void populaOpcoesSituacaoPropostaCombo(){
		opcoesSituacaoPropostaCombo = new ArrayList<SelectItem>();
		for(SituacaoProposta situacao : SituacaoProposta.values()) {
			opcoesSituacaoPropostaCombo.add( new SelectItem( situacao.getId(), situacao.getNome()));
		}
		
	}
	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 * 
	 */
	public SuperApHandler(){
		String fluxo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fluxo");
		String propostaVisualizar = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("propostaVisualizar");
		Session session = HibernateUtil.currentSession();
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		PropostaDAO pDao = new PropostaDAO(session);
		Proposta propostaRecuperada = null;

		if( !StringUtils.isBlank( propostaVisualizar ) ){
			
			try{
				propostaRecuperada = daoProposta.get( new Integer(propostaVisualizar) );
				pDao.populaCapital(propostaRecuperada);
			}catch (Exception e) {
				addErrorMessage( "Valor inv�lido!" );
			}
			if( propostaRecuperada == null ){
				addErrorMessage( "Proposta n�o encontrada!" );
					preparaIncluir();						
				
			}else{
				cancelarInclusao(null);
				setProposta(  propostaRecuperada );								
				preparaAlterar();
			}
			
		}else{
			
			if( fluxo == null ){
			HttpServletRequest request = ( HttpServletRequest ) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			fluxo = (String )request.getSession().getAttribute("fluxo");

				if( fluxo != null && fluxo.equals ( "fastSearch" ) ){
					
					String idProposta = (String) request.getSession().getAttribute("idPropostaFastSearch");

					try{
						propostaRecuperada = daoProposta.get( new Integer(idProposta) );
					}catch (Exception e) {
						addErrorMessage( "Valor inv�lido! Os termos da busca r�pida devem conter apenas n�meros!" );
					}
					if( propostaRecuperada == null ){
						addErrorMessage("Proposta n�o encontrada!" );
							preparaIncluir();						
						
					}else{
						cancelarInclusao(null);
						setProposta(  propostaRecuperada );								
						preparaAlterar();
					}
				}
				
			}else if( fluxo.equals( "incluir" ) ){
				preparaIncluir();
			}else if( fluxo.equals( "consulta" ) ){
				cancelarFiltro( null );
			} else {
				try{
					propostaRecuperada = daoProposta.get( new Integer(fluxo) );
					pDao.populaCapital(propostaRecuperada);
					setProposta(propostaRecuperada);
					preparaAlterar();
				}catch (Exception e) {
					addErrorMessage( "Valor inv�lido!" );
				}

			}
		}
		populaOpcoesEmpresaCombo();
		populaOpcoesSituacaoPropostaCombo();
		populaOpcoesMesesCombo(true);
	}
	
	
	/**
	 * Popula um combo com valores dos Tipos de Proposta
	 * @return
	 */
	public void populaTipoPropostaComboFiltro(ActionEvent e){
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		tipoPropostaComboFiltro = new ArrayList<SelectItem>();

		TipoPropostaDAO tipoPropostaDao = new TipoPropostaDAO();
		List listaTiposPropostasAll =  tipoPropostaDao.busca( getPropostaFiltro().getModeloProposta().getId() );
		if( listaTiposPropostasAll!= null && listaTiposPropostasAll.size() > 0 ){
			tipoPropostaComboFiltro.add(new SelectItem("", "Selecione:"));
		}
		
		for (Iterator iterator = listaTiposPropostasAll.iterator(); iterator.hasNext();) {
			TipoProposta tipoProposta = (TipoProposta) iterator.next();
			tipoPropostaComboFiltro.add( new SelectItem( tipoProposta.getId(), tipoProposta.getDescricao() ) );
		}		
		
		
	}	
	
	/**
	 * Popula um combo com valores dos Tipos de Proposta
	 * @return
	 */
	public void populaTipoPropostaCombo(ActionEvent e){
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		tipoPropostaComboFiltro = new ArrayList<SelectItem>();
		
		if (proposta.getModeloProposta() != null
				&& proposta.getModeloProposta().getId() != null
				&& (proposta.getModeloProposta().getId()
						.equals(Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO) || proposta
						.getModeloProposta().getId()
						.equals(Constantes.MODELO_PROPOSTA_ATUALIZADO))) {
			if(proposta.getNomeTabela() == null) {
				proposta.setNomeTabela(new NomeTabela());
			}
			if(proposta.getCapitalSeguradoFaixaEtaria() == null) {
				proposta.setCapitalSeguradoFaixaEtaria(new CapitalSeguradoFaixaEtaria());
			}
			if(proposta.getCapitalSeguradoLimiteIdade() == null) {
				proposta.setCapitalSeguradoLimiteIdade(new CapitalSeguradoLimiteIdade());
			}
			proposta.setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
		}
		
		if(proposta.getDetalheCapitalFaixaEtariaFuturo() == null) {
			proposta.setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
		}
		
		session.evict( proposta );
		
		TipoPropostaDAO tipoPropostaDao = new TipoPropostaDAO();
		List listaTiposPropostasAll =  tipoPropostaDao.busca( getProposta().getModeloProposta().getId() );
		if( listaTiposPropostasAll!= null && listaTiposPropostasAll.size() > 0 ){
			tipoPropostaComboFiltro.add(new SelectItem("", "Selecione:"));
		}
		
		for (Iterator iterator = listaTiposPropostasAll.iterator(); iterator.hasNext();) {
			TipoProposta tipoProposta = (TipoProposta) iterator.next();
			tipoPropostaComboFiltro.add( new SelectItem( tipoProposta.getId(), tipoProposta.getDescricao() ) );
		}
		
	}

	public String exportarExcel(){
        // Em ordem crescente 
        Collections.sort (resultadoPesquisa, new Comparator() {  
            public int compare(Object o1, Object o2) {  
                Proposta p1 = (Proposta) o1;  
                Proposta p2 = (Proposta) o2;  
                return p1.getNomeSegurado().compareToIgnoreCase( p2.getNomeSegurado() );  
            }  
        }); 
		
		return "exportacaoExcelRelatorioSegurados";
	}
	
	/**
	 * � partir de um capital escolhido, popula o do conjuge se o plano for casado
	 * @param e
	 */
	public void populaCapitalConjuge( ActionEvent e ){
		
		TipoPropostaDAO tipoPropostaDAO = new TipoPropostaDAO();
		getProposta().setTipoProposta(tipoPropostaDAO.load(getProposta().getTipoProposta().getId()));
		
		// Para propostas do modelo de Limite de Idade
		if( getProposta().getModeloProposta() != null && getProposta().getModeloProposta().getId() != null 
				&& getProposta().getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_VALOR_FIXO ) ){
			
			if( getProposta().getCapitalSeguradoLimiteIdade() != null ){
			
				valorCapitalSegurado = proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural();
					
				if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_50 ) 
						&& getProposta().getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
					
					proposta.setCapitalConjuge( proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() );
					proposta.setPremioConjuge( proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado50() - proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
					proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
					proposta.setTotalPremios(  proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado50() );
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_100 )
						&& getProposta().getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
					proposta.setCapitalConjuge( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
					proposta.setPremioConjuge( proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado100() - proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
					proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
					proposta.setTotalPremios( proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado100() );
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_INDIVIDUAL ) 
						&& getProposta().getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){

					proposta.setCapitalConjuge( null );
					proposta.setPremioConjuge( null );
					proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
					proposta.setTotalPremios( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );

					
				}else{
					getProposta().setCapitalConjuge( null );
					proposta.setPremioConjuge( null );
					proposta.setPremioSegurado( null );
					proposta.setTotalPremios( null );
				}
			}
			
		}
		
		// Para propostas do modelo de Faixa Etaria
		if( getProposta().getModeloProposta() != null && getProposta().getModeloProposta().getId() != null 
				&& getProposta().getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_ETARIA ) ){
			
			if( getProposta().getDetalheCapitalFaixaEtaria() != null ){
			
				valorCapitalSegurado = proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado();
				
				if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_CASADO ) 
						&& getProposta().getDetalheCapitalFaixaEtaria().getCapitalSegurado() != null ){
					proposta.setCapitalConjuge( getProposta().getDetalheCapitalFaixaEtaria().getCapitalSegurado() );
					proposta.setPremioConjuge( proposta.getDetalheCapitalFaixaEtaria().getSomaTitularConjuge() - proposta.getDetalheCapitalFaixaEtaria().getTitular()  );
					proposta.setPremioSegurado( proposta.getDetalheCapitalFaixaEtaria().getTitular() );
					proposta.setTotalPremios( proposta.getDetalheCapitalFaixaEtaria().getSomaTitularConjuge() );					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_INDIVIDUAL ) 
						&& getProposta().getDetalheCapitalFaixaEtaria().getCapitalSegurado() != null ){
					proposta.setCapitalConjuge( null );
					proposta.setPremioConjuge( null  );
					proposta.setPremioSegurado( proposta.getDetalheCapitalFaixaEtaria().getTitular() );
					proposta.setTotalPremios( proposta.getDetalheCapitalFaixaEtaria().getTitular() );					
					
				}else{
					getProposta().setCapitalConjuge( null );
					proposta.setPremioConjuge( null );
					proposta.setPremioSegurado( null );
					proposta.setTotalPremios( null );					

				}
			}
			
			//DETALHE CAPITAL FAIXA ETARIA FUTURO
			if( getProposta().getDetalheCapitalFaixaEtariaFuturo() != null && getProposta().getDetalheCapitalFaixaEtariaFuturo().getId() != null) {
				
				valorCapitalSeguradoFuturo = proposta.getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado();
				
				if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_CASADO ) 
						&& getProposta().getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() != null ){
					proposta.setCapitalConjugeFuturo( getProposta().getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() );
					proposta.setPremioConjugeFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getSomaTitularConjuge() - proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular()  );
					proposta.setPremioSeguradoFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular() );
					proposta.setTotalPremiosFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getSomaTitularConjuge() );					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_INDIVIDUAL ) 
						&& getProposta().getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() != null ){
					proposta.setCapitalConjugeFuturo( null );
					proposta.setPremioConjugeFuturo( null  );
					proposta.setPremioSeguradoFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular() );
					proposta.setTotalPremiosFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular() );					
					
				}else{
					getProposta().setCapitalConjugeFuturo( null );
					proposta.setPremioConjugeFuturo( null );
					proposta.setPremioSeguradoFuturo( null );
					proposta.setTotalPremiosFuturo( null );					
					proposta.setValorFuturoCapitalConjuge(null);
					proposta.setValorFuturoPremioConjuge(null);
				}
			} else {
				getProposta().setCapitalConjugeFuturo( null );
				proposta.setPremioConjugeFuturo( null );
				proposta.setPremioSeguradoFuturo( null );
				proposta.setTotalPremiosFuturo( null );
				proposta.setValorFuturoCapitalConjuge(null);
				proposta.setValorFuturoPremioConjuge(null);
				proposta.setValorFuturoCapitalSegurado(null);
				proposta.setValorFuturoPremioSegurado(null);
			}
		}
		
		// Para propostas do modelo Reangariado
		if( getProposta().getModeloProposta() != null && getProposta().getModeloProposta().getId() != null 
				&& getProposta().getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO )){
			
			if( getProposta().getValorCapitalSegurado() != null ){
			
				valorCapitalSegurado = proposta.getValorCapitalSegurado();
				
				if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_100 ) 
						&& proposta.getValorCapitalSegurado() != null){
					
					proposta.setValorCapitalSegurado(valorCapitalSegurado);
					proposta.setCapitalConjuge( proposta.getValorCapitalSegurado() );
					proposta.setValorCapitalConjuge(proposta.getValorCapitalSegurado());
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_50 )
						&& proposta.getValorCapitalSegurado() != null){
					
					proposta.setValorCapitalSegurado(valorCapitalSegurado);
					proposta.setCapitalConjuge( proposta.getValorCapitalSegurado() / 2 );
					proposta.setValorCapitalConjuge(proposta.getValorCapitalSegurado() / 2);
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL )
						&& proposta.getValorCapitalSegurado() != null){
					proposta.setValorCapitalSegurado(valorCapitalSegurado);
					
					proposta.setCapitalConjuge( null );
					proposta.setValorCapitalConjuge(null);
					
				}
			}
		}
		
		// Para propostas do modelo Atualizado
		if( getProposta().getModeloProposta() != null && getProposta().getModeloProposta().getId() != null 
				&& getProposta().getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_ATUALIZADO )){
			
			if( getProposta().getValorCapitalSegurado() != null ){
			
				valorCapitalSegurado = proposta.getValorCapitalSegurado();
				
				if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_100 ) 
						&& proposta.getValorCapitalSegurado() != null){
					
					proposta.setValorCapitalSegurado(valorCapitalSegurado);
					proposta.setCapitalConjuge( proposta.getValorCapitalSegurado() );
					proposta.setValorCapitalConjuge(proposta.getValorCapitalSegurado());
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_50 )
						&& proposta.getValorCapitalSegurado() != null){
					
					proposta.setValorCapitalSegurado(valorCapitalSegurado);
					proposta.setCapitalConjuge( proposta.getValorCapitalSegurado() / 2 );
					proposta.setValorCapitalConjuge(proposta.getValorCapitalSegurado() / 2);
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL )
						&& proposta.getValorCapitalSegurado() != null){
					proposta.setValorCapitalSegurado(valorCapitalSegurado);
					
					proposta.setCapitalConjuge( null );
					proposta.setValorCapitalConjuge(null);
					
				}
			}
		}
				
		// Para propostas do modelo Reangariado - VALOR FUTURO
		if( getProposta().getModeloProposta() != null && getProposta().getModeloProposta().getId() != null 
				&& getProposta().getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO ) ){
			
			if( getProposta().getValorFuturoCapitalSegurado() != null ){
			
				valorCapitalSeguradoFuturo = proposta.getValorFuturoCapitalSegurado();
				
				if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null 
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_100 ) 
						&& proposta.getValorFuturoCapitalSegurado() != null){
					
					proposta.setValorFuturoCapitalSegurado(valorCapitalSeguradoFuturo);
					proposta.setCapitalConjugeFuturo( proposta.getValorFuturoCapitalSegurado() );
					proposta.setValorFuturoCapitalConjuge(proposta.getValorFuturoCapitalSegurado());
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_50 )
						&& proposta.getValorFuturoCapitalSegurado() != null){
					
					proposta.setValorFuturoCapitalSegurado(valorCapitalSeguradoFuturo);
					proposta.setCapitalConjugeFuturo(proposta.getValorFuturoCapitalSegurado() / 2 );
					proposta.setValorFuturoCapitalConjuge(proposta.getValorFuturoCapitalSegurado() / 2);
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL) 
						&& proposta.getValorFuturoCapitalSegurado() != null) {

					proposta.setValorFuturoCapitalSegurado(valorCapitalSeguradoFuturo);
					proposta.setCapitalConjugeFuturo( null );
					proposta.setValorFuturoCapitalConjuge(null);
					
				}
			} 
			
		}
		
		// Para propostas do modelo Atualizado - VALOR FUTURO
		if( getProposta().getModeloProposta() != null && getProposta().getModeloProposta().getId() != null 
				&& getProposta().getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_ATUALIZADO ) ){
			
			if( getProposta().getValorFuturoCapitalSegurado() != null ){
			
				valorCapitalSeguradoFuturo = proposta.getValorFuturoCapitalSegurado();
				
				if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_100 ) 
						&& proposta.getValorFuturoCapitalSegurado() != null){
					
					proposta.setValorFuturoCapitalSegurado(valorCapitalSeguradoFuturo);
					proposta.setCapitalConjugeFuturo( proposta.getValorFuturoCapitalSegurado() );
					proposta.setValorFuturoCapitalConjuge(proposta.getValorFuturoCapitalSegurado());
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_50 )
						&& proposta.getValorFuturoCapitalSegurado() != null){
					
					proposta.setValorFuturoCapitalSegurado(valorCapitalSeguradoFuturo);
					proposta.setCapitalConjugeFuturo(proposta.getValorFuturoCapitalSegurado() / 2 );
					proposta.setValorFuturoCapitalConjuge(proposta.getValorFuturoCapitalSegurado() / 2);
					
				}else if( getProposta().getTipoProposta() != null && getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL) 
						&& proposta.getValorFuturoCapitalSegurado() != null) {

					proposta.setValorFuturoCapitalSegurado(valorCapitalSeguradoFuturo);
					proposta.setCapitalConjugeFuturo( null );
					proposta.setValorFuturoCapitalConjuge(null);
					
				}
			} 
			
		}
		

	}

	public void populaPremioConjuge(ActionEvent e) {
		// Para propostas do modelo Reangariado
		if (getProposta().getModeloProposta() != null
				&& getProposta().getModeloProposta().getId() != null
				&& getProposta().getModeloProposta().getId()
						.equals(Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO)) {

			if (getProposta().getValorCapitalSegurado() != null) {

				if (getProposta().getTipoProposta() != null
						&& getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_100)
						&& proposta.getValorCapitalSegurado() != null) {

					proposta.setPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setPremioConjuge(proposta.getValorPremioConjuge());

					if (proposta.getValorPremioSegurado() != null
							&& proposta.getValorPremioConjuge() != null) {
						proposta.setTotalPremios(proposta
								.getValorPremioSegurado()
								+ proposta.getValorPremioConjuge());
					}

				} else if (getProposta().getTipoProposta() != null
						&& getProposta().getTipoProposta().getId() != null
						//&& getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_50)
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						&& proposta.getValorCapitalSegurado() != null) {

					proposta.setPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setPremioConjuge(proposta.getValorPremioConjuge());

					if (proposta.getValorPremioSegurado() != null
							&& proposta.getValorPremioConjuge() != null) {
						proposta.setTotalPremios(proposta
								.getValorPremioSegurado()
								+ proposta.getValorPremioConjuge());
					}

				} else if (getProposta().getTipoProposta() != null
						&& getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL)
						&& proposta.getValorCapitalSegurado() != null) {

					proposta.setPremioConjuge(null);
					proposta.setValorPremioConjuge(null);

					proposta.setPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setValorPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setTotalPremios(proposta.getValorPremioSegurado());
				}
			}
		}

		// Para propostas do modelo Atualizado
		if (getProposta().getModeloProposta() != null
				&& getProposta().getModeloProposta().getId() != null
				&& getProposta().getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_ATUALIZADO)) {

			if (getProposta().getValorCapitalSegurado() != null) {

				if (getProposta().getTipoProposta() != null
						&& getProposta().getTipoProposta().getId() != null
						//&& getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_100)
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						&& proposta.getValorCapitalSegurado() != null) {

					proposta.setPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setPremioConjuge(proposta.getValorPremioConjuge());

					if (proposta.getValorPremioSegurado() != null
							&& proposta.getValorPremioConjuge() != null) {
						proposta.setTotalPremios(proposta
								.getValorPremioSegurado()
								+ proposta.getValorPremioConjuge());
					}

				} else if (getProposta().getTipoProposta() != null
						&& getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_50)
						&& proposta.getValorCapitalSegurado() != null) {

					proposta.setPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setPremioConjuge(proposta.getValorPremioConjuge());

					if (proposta.getValorPremioSegurado() != null
							&& proposta.getValorPremioConjuge() != null) {
						proposta.setTotalPremios(proposta
								.getValorPremioSegurado()
								+ proposta.getValorPremioConjuge());
					}

				} else if (getProposta().getTipoProposta() != null
						&& getProposta().getTipoProposta().getId() != null
						&& getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL)
						&& proposta.getValorCapitalSegurado() != null) {

					proposta.setPremioConjuge(null);
					proposta.setValorPremioConjuge(null);

					proposta.setPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setValorPremioSegurado(proposta
							.getValorPremioSegurado());
					proposta.setTotalPremios(proposta.getValorPremioSegurado());
				}
			}
		}
	}
	
	//Chamado ao limpar um �rg�o
	public Orgao getLimparOrgao() {
		if(proposta.getOrgao() == null || proposta.getOrgao().getNomeOrgao() == null || proposta.getOrgao().getNomeOrgao().isEmpty()) {
			return  new Orgao() ;
		}
		return proposta.getOrgao();
	}
	
	public Orgao getLimparOrgaoFiltro() {
		if(getPropostaFiltro().getOrgao().getNomeOrgao() == null || getPropostaFiltro().getOrgao().getNomeOrgao().isEmpty()) {
			return  new Orgao() ;
		}
		return getPropostaFiltro().getOrgao();
	}
	
	public Cidade getLimparCidadeFiltro() {
		if(getPropostaFiltro().getCidade().getNom_cidade() == null || getPropostaFiltro().getCidade().getNom_cidade().isEmpty()) {
			Cidade cidade = new Cidade();
			cidade.setCod_estado(new Estado());
			return  cidade ;
		}
		return getPropostaFiltro().getCidade();
	}
	
	private void pesquisaPropostasPai(boolean filtrarAtivas) {
		pesquisaPropostasPai(filtrarAtivas, null);
	}
	
	private void pesquisaPropostasPai(boolean filtrarAtivas, Integer limite) {
		valorTotalRegistrosRelatorio = 0.0;
		totalRegistrosCancelados = 0;
		totalRegistrosAtivos = 0;
		totalRegistrosSemAverbacao = 0;
		valorTotalPremioAtivos = 0.0;
		
		Session session = HibernateUtil.currentSession();
		PropostaDAO dao = new PropostaDAO( session );
		
		Proposta propostaFiltro = getPropostaFiltro();
		String cpf = propostaFiltro.getCpfSegurado();
		if( cpf != null && cpf.trim().length() > 0 ){			
			propostaFiltro.setCpfSegurado( cpf );
			propostaFiltro.setCpfSeguradoFormatado( cpf.replace(".", "").replace("-", "").replace("/", "").replace("_", "") );
		}
		
		propostaFiltro.setFiltroSituacao(new ArrayList<SituacaoProposta>());
		for (String s : situacaoPropostaSelectedList) {
			propostaFiltro.getFiltroSituacao().add(SituacaoProposta.get(Integer.valueOf(s)));
		}
		
		List<Proposta> lista = dao.buscaPorFiltroConsulta( propostaFiltro, filtrarAtivas, limite );
		
		dao.populaCapital(lista);
		
	
		//TODO TESTAR
		for(Proposta proposta: lista) {
			if( proposta.getTotalPremios() != null ){
				double arredonado = NumberUtil.round(proposta.getTotalPremios(), 2);
				valorTotalRegistrosRelatorio += arredonado;
			}
		}
		
		setResultadoPesquisa(lista);
		totalRegistrosRelatorio = lista.size();
		
		if (totalRegistrosRelatorio.intValue() == 0) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
		}
		
		if (limite != null && totalRegistrosRelatorio.intValue() == limite.intValue()) {
			String message = JSFUtils.getMessageResourceString("Messages", "mensagem-limite-200", null);
			addWarnMessage( message );	
		}
	}
	
	/**
	 * Realiza a pesquisa utilizando filtro
	 * @param e
	 */
	public void pesquisaPropostas(ActionEvent e){
		pesquisaPropostasPai(false, 200);
	}

	
	/**
	 * Realiza a pesquisa utilizando filtro
	 * @param e
	 */
	public void pesquisaPropostasRelatorioMalaDireta (ActionEvent e){
		pesquisaPropostasPai(true);
		if (totalRegistrosRelatorio == null || totalRegistrosRelatorio.equals(0)) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
		}
	}
	
	public boolean validarCPF(Proposta proposta) {
		String cpf = proposta.getCpfSegurado();
		if(cpf == null) {
			return true;
		}
		PropostaDAO propostaDAO = new PropostaDAO(HibernateUtil.currentSession());
		Proposta propostaFiltro = new Proposta();
		propostaFiltro.setId(proposta.getId());
		propostaFiltro.setCpfSegurado(cpf);
		propostaFiltro.setCpfSeguradoFormatado(cpf.replace(".", "").replace("-", "").replace("/", "").replace("_", "") );
		Integer count = propostaDAO.countPropostasCPFDuplicidade( propostaFiltro );
		if( count != null && count > 0 ){			
			return false;
		}
		return true;
	}
	
	public boolean validarCamposRelatorio() {
		
		if(tipoRelatorio == null) {
			String message = JSFUtils.getMessageResourceString("Messages", "javax.faces.component.UIInput.REQUIRED", new Object[] {"Tipo de Relat�rio"});
			addErrorMessage( message );
			return true;
		}
		
		return false;
	}
	
	/**
	 * Realiza a pesquisa utilizando filtro
	 * @param e
	 */
	public String pesquisaPropostasRelatorio( ){
		
		if(validarCamposRelatorio()) {
			return null;
		}
		
		valorTotalRegistrosRelatorio = 0.0;
		Session session = HibernateUtil.currentSession();
		PropostaDAO dao = new PropostaDAO( session );
		
		Proposta propostaFiltro = getPropostaFiltro();
		
		List<Proposta> lista = dao.buscaPorFiltro( propostaFiltro, tipoRelatorio  );
		
		for (Proposta proposta : lista) {
			if( proposta.getTotalPremios() != null ){
				double arredonado = NumberUtil.round(proposta.getTotalPremios(), 2);
				valorTotalRegistrosRelatorio += arredonado;
			}
		}
		totalRegistrosRelatorio = lista.size();
		
		setResultadoPesquisa(lista);
		
		return "resultado";
	}

	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		
		try {
			//Recupera o item escolhido
			UIComponent link = e.getComponent();
			HtmlInputText param = (HtmlInputText) link.findComponent("txtCapitalCapitalFuturoFaixaEtaria");
			
			//Formata entrada
			Double capitalFormatado = ((Double) param.getValue() );
			if(capitalFormatado == null) {
				getProposta().setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
			}
					
			UsuarioHandler usuarioHandler = (UsuarioHandler) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioHandler");
			Usuario usuario = usuarioHandler.getUsuario();		
			
			//Realiza as a��es de banco: Busca e seta os valores
			Session session = HibernateUtil.currentSession();
			PropostaDAO propostaDAO = new PropostaDAO(session);
			TipoPropostaDAO tipoPropostaDAO = new TipoPropostaDAO();
			
			Dao<Proposta, Integer> dao = new Dao<Proposta, Integer>(session, Proposta.class);
			Dao<ModeloProposta, Integer> modeloDAO = new Dao<ModeloProposta, Integer>(session, ModeloProposta.class);
			Proposta propostaGravar = getProposta();
			
			if (propostaGravar.getModeloProposta() != null && propostaGravar.getModeloProposta().getId() != null) {
				propostaGravar.setModeloProposta(modeloDAO.load(propostaGravar.getModeloProposta().getId()));
			}
			
			if (propostaGravar.getTipoProposta() != null && propostaGravar.getTipoProposta().getId() != null) {
				propostaGravar.setTipoProposta(tipoPropostaDAO.load(propostaGravar.getTipoProposta().getId()));
			}
	
			
			//Realiza as valida��es necess�rias antes de gravar a proposta no banco
			boolean passou = validarPropostaGravar( propostaGravar );
	
			if( passou ){
				
				//Remove referencias inv�lidas
				if(propostaGravar.getDetalheCapitalFaixaEtaria() == null || propostaGravar.getDetalheCapitalFaixaEtaria().getId() == null ){
					propostaGravar.setCapitalSeguradoFaixaEtaria(null);
					propostaGravar.setDetalheCapitalFaixaEtaria( null );
				}else{
					propostaGravar.setCapitalSeguradoFaixaEtaria( propostaGravar.getDetalheCapitalFaixaEtaria().getCapitalSeguradoFaixaEtaria() );
				}
	
				if(propostaGravar.getDetalheCapitalFaixaEtariaFuturo() == null || propostaGravar.getDetalheCapitalFaixaEtariaFuturo().getId() == null) {
					propostaGravar.setDetalheCapitalFaixaEtariaFuturo( null );
				}
				
				if(propostaGravar.getCapitalSeguradoLimiteIdade() == null || propostaGravar.getCapitalSeguradoLimiteIdade().getId() == null ){
					propostaGravar.setCapitalSeguradoLimiteIdade(null);
				}
				
				if(getProposta().getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO)) {
					getProposta().setNomeTabela(null);
				
					
					//if(getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_50 )) {
					if (getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50) {
						proposta.setValorCapitalConjuge( proposta.getValorCapitalSegurado() / 2 );
					//} else if (getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_100 )) {
					} else if (getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100) {
						proposta.setValorCapitalConjuge( proposta.getValorCapitalSegurado());
					} else  { //individual
						proposta.setValorCapitalConjuge( 0.0);
						proposta.setValorPremioConjuge(0.0);
					}
					
				} else if(getProposta().getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_ATUALIZADO)) {
					getProposta().setNomeTabela(null);
				
					//if(getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_50 )) {
					if (getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50) {
						proposta.setValorCapitalConjuge( proposta.getValorCapitalSegurado() / 2 );
					//} else if (getProposta().getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_100 )) {
					} else if (getProposta().getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100) {
						proposta.setValorCapitalConjuge( proposta.getValorCapitalSegurado());
					} else  { //individual
						proposta.setValorCapitalConjuge( 0.0);
						proposta.setValorPremioConjuge(0.0);
					}
					
				} else {
					proposta.setValorCapitalSegurado( null);
					proposta.setValorPremioSegurado(null);
					proposta.setValorCapitalConjuge(null );
					proposta.setValorPremioConjuge(null);
				}
				
				if( propostaGravar.getMotivoCancelamento() == null || propostaGravar.getMotivoCancelamento().getId() == null || propostaGravar.getMotivoCancelamento().getId() == 0  ){
					propostaGravar.setMotivoCancelamento( null );
				}
				
				//BENEFICI�RIOS
				Dao<Beneficiario, Integer> daoBeneficiario = new Dao<Beneficiario, Integer>(session, Beneficiario.class);
		
				//Grava os Benefici�rios
				for (Iterator iterator = propostaGravar.getBeneficiario_collection().iterator(); iterator.hasNext();) {
					Beneficiario element = (Beneficiario) iterator.next();				
					element.setProposta( propostaGravar );
				}
				
				
				//Exclui os Benefici�rios marcados para exclus�o
				for (Iterator iterator = getBeneficiariosExcluidos().iterator(); iterator.hasNext();) {
					Beneficiario beneficiarioExcluido = (Beneficiario) iterator.next();	
					daoBeneficiario.delete( beneficiarioExcluido );
				}
				
				//Zera a lista de Benefici�rios excluidos
				setBeneficiariosExcluidos( new ArrayList<Beneficiario>() );
				
				//AGENCIADORES		
				Dao<PropostaAgenciador, Integer> daoPropostaAgenciador = new Dao<PropostaAgenciador, Integer>(session, PropostaAgenciador.class);
		
				//NUMERO DE SORTEIO
				Dao<NumeroSorteio, Integer> daoNumeroSorteio = new Dao<NumeroSorteio, Integer>(session, NumeroSorteio.class);
				
				//Grava os Agenciadores
				for (Iterator iter = propostaGravar.getPropostaagenciador_collection().iterator(); iter.hasNext();) {
					PropostaAgenciador element = (PropostaAgenciador) iter.next();
					if( element.getProposta() == null || element.getProposta().getId() == null ){
						element.setProposta( propostaGravar );
					}
					
					element.setComando(getProposta().getComandoComissao());
					
					try {
						populaCapitalConjuge(null);
						populaPremioConjuge(null);
						element.setDiferenca(calcularDiferenca(session, propostaGravar));
						element.setValorTotalPremio(propostaGravar.getTotalPremios());
					} catch (Exception e2) {
						if(element.getDiferenca() == null) {
							element.setDiferenca(0.0);
						}
						logger.warn("Erro inesperado ao calcular pr�mio para a comiss�o", e2);
					}
				}
				
				//Exclui os Agenciadores marcados para exclus�o
				for (Iterator iterat = getAgenciadoresExcluidos().iterator(); iterat.hasNext();) {
					PropostaAgenciador agenciadorExcluido = (PropostaAgenciador) iterat.next();	
					daoPropostaAgenciador.delete( agenciadorExcluido );
				}
				
				//Zera a lista de Benefici�rios excluidos
				setAgenciadoresExcluidos( new ArrayList<PropostaAgenciador>() );		
				
				//Exclui os N�meros de Sorteio
				if(  getProposta().getNumerosorteio_collection() != null ){
					for (Iterator iterat = getProposta().getNumerosorteio_collection().iterator(); iterat.hasNext();) {
						NumeroSorteio numeroSorteioExcluido = (NumeroSorteio) iterat.next();	
						daoNumeroSorteio.delete( numeroSorteioExcluido );
					}
				}else{
					 getProposta().setNumerosorteio_collection( new ArrayList<NumeroSorteio>() );
				}
				
				if( getNumeroSorteio() != null && !"".equals( getNumeroSorteio() ) ){
					// Prepara o N�mero de Sorteio para inclus�o
					NumeroSorteio numeroSorteio = new NumeroSorteio();
					numeroSorteio.setProposta( getProposta() );
					numeroSorteio.setNumeroSorteio( getNumeroSorteio() );
					
					getProposta().getNumerosorteio_collection().clear();
					getProposta().getNumerosorteio_collection().add( numeroSorteio );
				}
	
				if(propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_VALOR_FIXO)) {
					propostaGravar.setValorFuturoCapitalConjuge(fixoCapitalConjugeFuturo);
					propostaGravar.setValorFuturoCapitalSegurado(fixoCapitalFuturo);
					propostaGravar.setValorFuturoPremioConjuge(fixoPremioConjugeFuturo);
					propostaGravar.setValorFuturoPremioSegurado(fixoPremioFuturo);
				}
				
				if(getProposta().getDataCancelamento() == null || getProposta().getSituacao() != SituacaoProposta.CANCELADA) {
					getProposta().setDataExclusao(null);
				}
				
				if(getProposta().getDataCancelamento() != null && getProposta().getSituacao() != SituacaoProposta.CANCELADA) {
					//marca a proposta como cancelada
					getProposta().setSituacao(SituacaoProposta.CANCELADA);
					getProposta().setDataExclusao(new Date());	
					getProposta().setDataUltimaAtualizacaoSituacao(new Date());
				} else {
					boolean valorDetalheCapital = getProposta().getDetalheCapitalFaixaEtariaFuturo() != null && getProposta().getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() != null;
					if (getProposta().getValorFuturoCapitalSegurado() != null || valorDetalheCapital) {
						//marca a proposta como pendente
						getProposta().setSituacao(SituacaoProposta.PENDENTE);
						getProposta().setDataUltimaAtualizacaoSituacao(new Date());
					} else if (getProposta().getDataAprovacao() == null) {
						getProposta().setSituacao(SituacaoProposta.AGUARDANDO_AVERBACAO);
						getProposta().setDataUltimaAtualizacaoSituacao(new Date());
						getProposta().setDataUltimaAtualizacaoSituacao(new Date());
						getProposta().setValorFuturoCapitalConjuge(null);
						getProposta().setValorFuturoCapitalSegurado(null);
						getProposta().setValorFuturoPremioSegurado(null);
						getProposta().setValorFuturoPremioConjuge(null);
						getProposta().setPremioConjugeFuturo(null);
						getProposta().setPremioSeguradoFuturo(null);
						getProposta().setCapitalConjugeFuturo(null);
					} else {
						getProposta().setSituacao(SituacaoProposta.ATIVA);
						getProposta().setDataUltimaAtualizacaoSituacao(new Date());
						getProposta().setValorFuturoCapitalConjuge(null);
						getProposta().setValorFuturoCapitalSegurado(null);
						getProposta().setValorFuturoPremioSegurado(null);
						getProposta().setValorFuturoPremioConjuge(null);
						getProposta().setPremioConjugeFuturo(null);
						getProposta().setPremioSeguradoFuturo(null);
						getProposta().setCapitalConjugeFuturo(null);
					}
				}
				
				
				
				//cria comissao dos agenciadores MODO INCLUS�O
				if(getProposta().getId() == null) {
					propostaGravar.setComissaoagenciador_collection(new ArrayList<ComissaoAgenciador>());
					for(PropostaAgenciador pa : propostaGravar.getPropostaagenciador_collection()) {
						Double valorTotalPremio = pa.getValorTotalPremio();
						ComissaoAgenciador comissaoAgenciador = new ComissaoAgenciador();
						comissaoAgenciador.setProposta(propostaGravar);
						comissaoAgenciador.setComando("Inclus�o");
						comissaoAgenciador.setDataCriacao(new Date());
						comissaoAgenciador.setFuncionario(pa.getFuncionario());
						comissaoAgenciador.setNomeUsuario(usuario.getNomeUsuario());
						comissaoAgenciador.setPercentual(pa.getFuncionario().getPercentualComissao());
						comissaoAgenciador.setValorDescontado(0.0);
						comissaoAgenciador.setValorTotalPremio(valorTotalPremio);
						Double percentual = comissaoAgenciador.getPercentual() != null ? comissaoAgenciador.getPercentual() : 0.0;
						if (pa.getTipoCalculoUtilizado().equals(Constantes.CALCULAR_AUTOMATICAMENTE)) {
							if(!pa.getFuncionario().getTipoFuncionario().equals(Constantes.FUNCIONARIO_AUTONOMO)) { //aut�nomo
								comissaoAgenciador.setValorDescontado(2.0);
							}
							comissaoAgenciador.setValorComissao((valorTotalPremio-comissaoAgenciador.getValorDescontado())*percentual/100); 
						} else if (pa.getTipoCalculoUtilizado().equals(Constantes.DEFINIR_VALOR_PAGAR)) {
							comissaoAgenciador.setValorComissao(pa.getValorPagoDefinido());
						} else if (pa.getTipoCalculoUtilizado().equals(Constantes.COMISSAO_ADICIONAL)) {
							comissaoAgenciador.setPercentual(pa.getPercentual());
							comissaoAgenciador.setValorComissao((valorTotalPremio-comissaoAgenciador.getValorDescontado())*percentual/100);
						}
						
						propostaGravar.getComissaoagenciador_collection().add(comissaoAgenciador);
					}
				} else {
					verificaMudancasTotalPremio(session, propostaGravar, usuario);
				}
				
				if(  propostaGravar.getId() != null &&  propostaGravar.getId() != 0 ){
					//SOMENTE PARA FLUXO DE ALTERA��O
					
					//Realiza a verifica��o de Hist�rico de Mudan�as para Benefici�rios
					verificaMudancasBeneficiarios(session, propostaGravar, usuario );
					
					//Realiza a verifica��o de Hist�rico de Mudan�as para Capital
					verificaMudancasCapital(session, propostaGravar, usuario );
					
					//Realiza a verifica��o de Hist�rico de Mudan�as para Capital
					verificaMudancasConjuge(session, propostaGravar, usuario );
					
					// Realiza a verifica��o de Hist�rico de Mudan�as para Capital
					verificaMudancasEndereco(session, propostaGravar, usuario );
					
					// Realiza a verifica��o de Hist�rico de Mudan�as para Capital
					verificaMudancasProposta(session, propostaGravar, usuario );
					
					//� uma altera��o
					propostaGravar.setDataAlteracao( new Date() );
					propostaGravar.setNomeUsuarioAlteracao( usuario.getNomeUsuario() );
				}
				
				
				propostaDAO.populaCapital(propostaGravar);
				//persiste os dados
				dao.merge( getProposta() );
				
				//executa o flush
				session.flush();
				
				//atualizar situa��o das propostas
				propostaDAO.atualizarSituacao();
				
				setVisibleMessage( false );			
				String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
				addGlobalMessage( message );
				
				cancelarInclusao(e);
				 Integer ultimoIdInserido = dao.retornaUltimoIdInserido( Proposta.class );
				 setNumeroPropostaIncluir( ultimoIdInserido + 1 );
			}
		} catch (Exception eProposta) {
			logger.error("Erro ao cadastrar proposta", eProposta);
			addGlobalMessage( "Erro inesperado: " + eProposta.getMessage() + ". Tente novamente, caso o erro persista contate o administrador do sistema.");
			
			getProposta().setNomeTabela(new NomeTabela());
			getProposta().setCapitalSeguradoLimiteIdade(new CapitalSeguradoLimiteIdade());
			getProposta().setDetalheCapitalFaixaEtaria(new DetalheCapitalFaixaEtaria());
			getProposta().setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
			getProposta().setMotivoCancelamento(new MotivoCancelamento());
			populaOpcoesEmpresaCombo();
			populaOpcoesSituacaoPropostaCombo();
			populaOpcoesMesesCombo(true);
			
		}
		
	}
	
	private Proposta getPropostaAnterior(Session session, Proposta propostaGravar) {
		Dao<Proposta, Integer> daoGenericoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		Proposta propostaAnterior = daoGenericoProposta.get(propostaGravar.getId());
		PropostaDAO daoProposta = new PropostaDAO(session);
		daoProposta.populaCapital(propostaAnterior);
		return propostaAnterior;
	}
	
	private Double calcularDiferenca(Session session, Proposta propostaGravar) {
 		if(propostaGravar.getId() != null) {
			Proposta propostaAnterior = getPropostaAnterior(session, propostaGravar);
	 		if(propostaAnterior.getTotalPremios() != null && propostaGravar.getTotalPremios() != null) {
	 			return propostaGravar.getTotalPremios() - propostaAnterior.getTotalPremios();
	 		}
 		}
 		return 0.0;
	}
	
	private boolean houveAlteracaoParaComissao(Session session, Proposta propostaGravar) {
		Proposta propostaAnterior = getPropostaAnterior(session, propostaGravar);
		
		boolean modeloPropostaDiferente = !propostaAnterior.getModeloProposta().equals(propostaGravar.getModeloProposta());
		
		boolean propostaReangariada = propostaGravar.getModeloProposta().getId().intValue() == 3;
		
		boolean alteracaoApolice = !propostaAnterior.getEmpresa().equals(propostaGravar.getEmpresa());
		
		boolean comissaoIntegral = (modeloPropostaDiferente && propostaReangariada) || alteracaoApolice;
		
		return modeloPropostaDiferente || alteracaoApolice || comissaoIntegral;
	}
	
	private void verificaMudancasTotalPremio(Session session, Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de valor do premios
		
		double diff = calcularDiferenca(session, propostaGravar);
	
		if(propostaGravar.getComissaoagenciador_collection() == null) {
			proposta.setComissaoagenciador_collection(new ArrayList<ComissaoAgenciador>());
		}
		for(PropostaAgenciador pa : propostaGravar.getPropostaagenciador_collection()) {
			
			if(houveAlteracaoParaComissao(session, propostaGravar)) {
				ComissaoAgenciador comissaoAgenciador = new ComissaoAgenciador();
				comissaoAgenciador.setProposta(propostaGravar);
				comissaoAgenciador.setComando("Altera��o");
				comissaoAgenciador.setDataCriacao(new Date());
				comissaoAgenciador.setFuncionario(pa.getFuncionario());
				comissaoAgenciador.setNomeUsuario(usuario.getNomeUsuario());
				comissaoAgenciador.setPercentual(pa.getFuncionario().getPercentualComissao());
				comissaoAgenciador.setValorDescontado(0.0);
				Double valorBase = propostaGravar.getTotalPremios() - comissaoAgenciador.getValorDescontado();
				comissaoAgenciador.setValorComissao(valorBase*comissaoAgenciador.getPercentual()/100);
				comissaoAgenciador.setValorTotalPremio(pa.getValorTotalPremio());
				propostaGravar.getComissaoagenciador_collection().add(comissaoAgenciador);
			} else {
				ComissaoAgenciador comissaoAgenciador = new ComissaoAgenciador();
				comissaoAgenciador.setProposta(propostaGravar);
				comissaoAgenciador.setComando("Altera��o");
				comissaoAgenciador.setDataCriacao(new Date());
				comissaoAgenciador.setFuncionario(pa.getFuncionario());
				comissaoAgenciador.setNomeUsuario(usuario.getNomeUsuario());
				comissaoAgenciador.setPercentual(25.0);
				comissaoAgenciador.setValorComissao(diff*comissaoAgenciador.getPercentual()/100); // 25% do valor percentual do c�lculo
				comissaoAgenciador.setValorDescontado(0.0);
				comissaoAgenciador.setValorTotalPremio(pa.getValorTotalPremio());
				if(diff > 0 ) { //se o valor do premio aumentou
					propostaGravar.getComissaoagenciador_collection().add(comissaoAgenciador);
				}
			}
		}
	}

	/**
	 * @param session
	 * @param propostaGravar
	 * @param usuario 
	 */
	private void verificaMudancasBeneficiarios(Session session,	Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		BeneficiarioDAO beneficiarioDAO =  new BeneficiarioDAO( session );
		List<Beneficiario> listaBeneficiariosBanco = beneficiarioDAO.buscaPorProposta( propostaGravar.getId() );
		for (Beneficiario beneficiarioTmp : listaBeneficiariosBanco) {
			session.evict( beneficiarioTmp );
		}
		
		session.flush();
		List<HistoricoBeneficiario> listaAlterados = new ArrayList<HistoricoBeneficiario>();
		
		for (Beneficiario beneficiarioBanco : listaBeneficiariosBanco) {
			boolean achou = false;
			for (Beneficiario beneficiario : propostaGravar.getBeneficiario_collection()) {
				if( beneficiarioBanco.getNomeBeneficiario().trim().toUpperCase().equals( beneficiario.getNomeBeneficiario().trim().toUpperCase() ) &&
					beneficiarioBanco.getGrauParentesco().getId().equals( beneficiario.getGrauParentesco().getId() ) &&
					beneficiarioBanco.getPercentual().equals( beneficiario.getPercentual() ) ){
					achou = true;
				}
			}
			
			if( !achou ){
				HistoricoBeneficiario historicoBeneficiario = new HistoricoBeneficiario();
				historicoBeneficiario.setNomeBeneficiario( beneficiarioBanco.getNomeBeneficiario() );
				historicoBeneficiario.setGrauParentesco( beneficiarioBanco.getGrauParentesco() );
				historicoBeneficiario.setPercentual( beneficiarioBanco.getPercentual() );
				historicoBeneficiario.setDataAlteracao( new Date() );
				historicoBeneficiario.setProposta( propostaGravar );
				historicoBeneficiario.setNomeUsuario( usuario.getNomeUsuario() );
				listaAlterados.add( historicoBeneficiario );
			}
		}
		
		if( propostaGravar.getHistoricobeneficiario_collection() == null  ){
			propostaGravar.setHistoricobeneficiario_collection( new ArrayList<HistoricoBeneficiario>() );
		}
		propostaGravar.getHistoricobeneficiario_collection().addAll( listaAlterados );
	}
	private boolean verificaString (String s) {
		return s != null && !s.trim().equals("");
	}
	private void verificaMudancasConjuge(Session session,	Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		Proposta p = daoProposta.get(propostaGravar.getId());
		List<HistoricoConjuge> listaAlterados = new ArrayList<HistoricoConjuge>();
			boolean achouMudanda = false;
			boolean cpfDiferente = verificaString(propostaGravar.getCpfConjuge()) && verificaString(p.getCpfConjuge()) && !propostaGravar.getCpfConjuge().equals(p.getCpfConjuge());
			boolean dataNascimentoDiferente = propostaGravar.getDataNascimentoConjuge() != null && p.getDataNascimentoConjuge() != null &&  
				propostaGravar.getDataNascimentoConjuge().compareTo(p.getDataNascimentoConjuge()) != 0;
			boolean estadoCivilDiferente = verificaString(propostaGravar.getEstadoCivilConjuge()) && !propostaGravar.getEstadoCivilConjuge().equals(p.getEstadoCivilConjuge());
			boolean matriculaDiferente = verificaString(propostaGravar.getMatriculaConjuge()) && !propostaGravar.getMatriculaConjuge().equals(p.getMatriculaConjuge());
			boolean nomeConjugeDiferente = verificaString(propostaGravar.getNomeConjuge()) && !propostaGravar.getNomeConjuge().equals(p.getNomeConjuge());
			boolean rgConjuge = verificaString(propostaGravar.getRgConjuge()) && !propostaGravar.getRgConjuge().equals(p.getRgConjuge());
			boolean sexoConjuge = verificaString(propostaGravar.getSexoConjuge()) && !propostaGravar.getSexoConjuge().equals(p.getSexoConjuge());
			boolean premioDiferente = Comparador.houveAlteracao(p.getValorPremioConjuge(), propostaGravar.getValorPremioConjuge());
			boolean capitalDiferente = Comparador.houveAlteracao(p.getValorCapitalConjuge(), propostaGravar.getValorCapitalConjuge());
			boolean profissaoConjugeAlterado = Comparador.houveAlteracao(p.getProfissaoConjuge(), propostaGravar.getProfissaoConjuge());
			
			achouMudanda = cpfDiferente || dataNascimentoDiferente || estadoCivilDiferente || matriculaDiferente || nomeConjugeDiferente ||
				rgConjuge || sexoConjuge || premioDiferente || capitalDiferente || profissaoConjugeAlterado;
			
			if(achouMudanda){
				HistoricoConjuge historicoConjuge = new HistoricoConjuge();
				historicoConjuge.setCpfConjuge(p.getCpfConjuge());
				historicoConjuge.setDataAlteracao(new Date());
				historicoConjuge.setDataNascimentoConjuge(p.getDataNascimentoConjuge());
				historicoConjuge.setEstadoCivilConjuge(p.getEstadoCivilConjuge());
				historicoConjuge.setMatriculaConjuge(p.getMatriculaConjuge());
				historicoConjuge.setNomeConjuge(p.getNomeConjuge());
				historicoConjuge.setNomeUsuario(usuario.getNomeUsuario());
				historicoConjuge.setProposta(propostaGravar);
				historicoConjuge.setRgConjuge(p.getRgConjuge());
				historicoConjuge.setSexoConjuge(p.getSexoConjuge());
				historicoConjuge.setPremio(p.getValorPremioConjuge());
				historicoConjuge.setCapital(p.getValorCapitalConjuge());
				historicoConjuge.setProfissaoConjuge(p.getProfissaoConjuge());
				listaAlterados.add( historicoConjuge );
			}
		
		if( propostaGravar.getHistoricobeneficiario_collection() == null  ){
			propostaGravar.setHistoricobeneficiario_collection( new ArrayList<HistoricoBeneficiario>() );
		}
		propostaGravar.getHistoricoconjuge_collection().addAll( listaAlterados );
	}
	
	private void verificaMudancasProposta(Session session,	Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		Proposta p = daoProposta.get(propostaGravar.getId());
		List<HistoricoProposta> listaAlterados = new ArrayList<HistoricoProposta>();
		boolean achouDiferenca = false;
		boolean empresaIgual = propostaGravar.getEmpresa() != null && propostaGravar.getEmpresa().getId().equals(p.getEmpresa().getId());
		boolean nomeTabelaIgual = true;
		if(propostaGravar.getNomeTabela() != null && p.getNomeTabela() != null) {
			if(propostaGravar.getNomeTabela().getId() != null && p.getNomeTabela().getId() != null) {
				nomeTabelaIgual = propostaGravar.getNomeTabela() != null && propostaGravar.getNomeTabela().getId().equals(p.getNomeTabela().getId());
			}
		}
		boolean orgaoIgual = propostaGravar.getOrgao() != null && propostaGravar.getOrgao().getId().equals(p.getOrgao().getId());
		boolean modeloIgual = propostaGravar.getModeloProposta() != null && propostaGravar.getModeloProposta().getId().equals(p.getModeloProposta().getId());
		boolean tipoIgual = propostaGravar.getTipoProposta() != null && propostaGravar.getTipoProposta().getId().equals(p.getTipoProposta().getId());
		if (!empresaIgual || !nomeTabelaIgual || !orgaoIgual || !modeloIgual || !tipoIgual) {
			achouDiferenca = true;
		}
		if (achouDiferenca) {
			HistoricoProposta historicoProposta = new HistoricoProposta();
			historicoProposta.setEmpresa(p.getEmpresa());
			historicoProposta.setDataAlteracao(new Date());
			historicoProposta.setNomeTabela(p.getNomeTabela());
			historicoProposta.setOrgao(p.getOrgao());
			historicoProposta.setProposta(propostaGravar);
			historicoProposta.setModeloProposta(p.getModeloProposta());
			historicoProposta.setTipoProposta(p.getTipoProposta());
			historicoProposta.setNomeUsuario(usuario.getNomeUsuario());
			listaAlterados.add(historicoProposta);
		}
		
		if( propostaGravar.getHistoricobeneficiario_collection() == null  ){
			propostaGravar.setHistoricobeneficiario_collection( new ArrayList<HistoricoBeneficiario>() );
		}
		propostaGravar.getHistoricoproposta_collection().addAll( listaAlterados );
	}
	
	private void verificaMudancasEndereco(Session session,	Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		Proposta p = daoProposta.get(propostaGravar.getId());
		List<HistoricoEnderecoSegurado> listaAlterados = new ArrayList<HistoricoEnderecoSegurado>();
			boolean houveMudancas = false;
			boolean mudouBairro = verificaString(propostaGravar.getBairro()) && !propostaGravar.getBairro().equals(p.getBairro());
			boolean mudouCep = verificaString(propostaGravar.getCep()) && !propostaGravar.getCep().equals(p.getCep());
			boolean mudouCidade = propostaGravar.getCidade() != null && !propostaGravar.getCidade().getId().equals(p.getCidade().getId());
			boolean mudouComplemento = verificaString(propostaGravar.getComplemento()) && !propostaGravar.getComplemento().equals(p.getComplemento());
			boolean mudouEndereco = verificaString(propostaGravar.getEnderecoSegurado()) && !propostaGravar.getEnderecoSegurado().equals(p.getEnderecoSegurado());
			boolean mudouObs = verificaString(propostaGravar.getObservacao()) && !propostaGravar.getObservacao().equals(p.getObservacao());
			boolean mudouTelCel1 = verificaString(propostaGravar.getTelefoneCelular()) && !propostaGravar.getTelefoneCelular().equals(p.getTelefoneCelular());
			boolean mudouTelCel2 = verificaString(propostaGravar.getTelefoneCelular2()) && !propostaGravar.getTelefoneCelular2().equals(p.getTelefoneCelular2());
			boolean mudouTelRes = verificaString(propostaGravar.getTelefoneResidencial()) && !propostaGravar.getTelefoneResidencial().equals(p.getTelefoneResidencial());
			boolean mudouTelCom = verificaString(propostaGravar.getTelefoneComercial()) && !propostaGravar.getTelefoneComercial().equals(p.getTelefoneComercial());
			houveMudancas = mudouBairro || mudouCep || mudouCidade || mudouComplemento || mudouEndereco || mudouObs || mudouTelCel1 || mudouTelCel2 ||
					mudouTelRes || mudouTelCom;
			if( houveMudancas ){
				HistoricoEnderecoSegurado historicoEnderecoSegurado = new HistoricoEnderecoSegurado();
				historicoEnderecoSegurado.setBairro(p.getBairro());
				historicoEnderecoSegurado.setCep(p.getCep());
				historicoEnderecoSegurado.setCidade(p.getCidade());
				historicoEnderecoSegurado.setComplemento(p.getComplemento());
				historicoEnderecoSegurado.setDataAlteracao(new Date());
				historicoEnderecoSegurado.setEnderecoSegurado(p.getEnderecoSegurado());
				historicoEnderecoSegurado.setNomeUsuario(usuario.getNomeUsuario());
				historicoEnderecoSegurado.setObservacao(p.getObservacao());
				historicoEnderecoSegurado.setProposta(propostaGravar);
				historicoEnderecoSegurado.setTelefoneCelular1(p.getTelefoneCelular());
				historicoEnderecoSegurado.setTelefoneCelular2(p.getTelefoneCelular2());
				historicoEnderecoSegurado.setTelefoneComercial(p.getTelefoneComercial());
				historicoEnderecoSegurado.setTelefoneResidencial(p.getTelefoneResidencial());
				listaAlterados.add( historicoEnderecoSegurado );
			}
		
		if( propostaGravar.getHistoricoenderecosegurado_collection() == null  ){
			propostaGravar.setHistoricoenderecosegurado_collection( new ArrayList<HistoricoEnderecoSegurado>() );
		}
		propostaGravar.getHistoricoenderecosegurado_collection().addAll( listaAlterados );
	}
	

	
	
	
	/**
	 * @param session
	 * @param propostaGr
	 * avar
	 */
	private void verificaMudancasCapital(Session session, Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		PropostaDAO propostaDAO = new PropostaDAO(session);
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		Proposta propostaBanco = daoProposta.get( propostaGravar.getId() );
		propostaDAO.populaCapital(propostaBanco);
		propostaDAO.populaCapital(propostaGravar);
		
		boolean capitalAlterado = Comparador.houveAlteracao(propostaBanco.getCapitalSegurado(),propostaGravar.getCapitalSegurado());
		boolean premioAlterado = Comparador.houveAlteracao(propostaBanco.getPremioSegurado(),propostaGravar.getPremioSegurado());
		boolean modeloProposta = Comparador.houveAlteracao(propostaBanco.getModeloProposta(),propostaGravar.getModeloProposta());
		boolean emailAlterado = Comparador.houveAlteracao(propostaBanco.getEmail(), propostaGravar.getEmail());
		boolean sexoAlterado = Comparador.houveAlteracao(propostaBanco.getSexoSegurado(), propostaGravar.getSexoSegurado());
		boolean dataNascimentoAlterado = Comparador.houveAlteracao(propostaBanco.getDataNascimentoSegurado(), propostaGravar.getDataNascimentoSegurado());
		boolean matriculaAlterado = Comparador.houveAlteracao(propostaBanco.getMatriculaSegurado(), propostaGravar.getMatriculaSegurado());
		boolean cpfAlterado = Comparador.houveAlteracao(propostaBanco.getCpfSegurado(), propostaGravar.getCpfSegurado());
		boolean rgAlterado = Comparador.houveAlteracao(propostaBanco.getRgSegurado(), propostaGravar.getRgSegurado());
		boolean estadoCivilAlterado = Comparador.houveAlteracao(propostaBanco.getEstadoCivilSegurado(), propostaGravar.getEstadoCivilSegurado());
		boolean nomeSeguradoAlterado = Comparador.houveAlteracao(propostaBanco.getNomeSegurado(), propostaGravar.getNomeSegurado());
		boolean profissaoSeguradoAlterado = Comparador.houveAlteracao(propostaBanco.getProfissaoSegurado(), propostaGravar.getProfissaoSegurado());
		
		boolean houveAlteracao = capitalAlterado || premioAlterado 
				|| modeloProposta || emailAlterado || sexoAlterado
				|| dataNascimentoAlterado || matriculaAlterado || cpfAlterado
				|| rgAlterado || estadoCivilAlterado || nomeSeguradoAlterado || profissaoSeguradoAlterado; 
		
		if(!houveAlteracao) {
			return;
		}
		
		session.evict( propostaBanco );
		List<HistoricoCapital> listaHistoricoCapital = new ArrayList<HistoricoCapital>();
		
		HistoricoCapital historicoCapital = new HistoricoCapital();
		historicoCapital.setDataAlteracao(new Date());
		historicoCapital.setNomeUsuario( usuario.getNomeUsuario() );
		historicoCapital.setTipoModeloProposta(propostaBanco.getModeloProposta().getDescricao());
		historicoCapital.setProposta( propostaGravar );
		historicoCapital.setPremioAntigo(propostaBanco.getPremioSegurado());
		historicoCapital.setCapitalAntigo(propostaBanco.getCapitalSegurado());
		historicoCapital.setEmail(propostaBanco.getEmail());
		historicoCapital.setSexo(propostaBanco.getSexoSegurado());
		historicoCapital.setDataNascimento(propostaBanco.getDataNascimentoSegurado());
		historicoCapital.setMatricula(propostaBanco.getMatriculaSegurado());
		historicoCapital.setCpf(propostaBanco.getCpfSegurado());
		historicoCapital.setRg(propostaBanco.getRgSegurado());
		historicoCapital.setEstadocivil(propostaBanco.getEstadoCivilSegurado());
		historicoCapital.setNomeSegurado(propostaBanco.getNomeSegurado());
		historicoCapital.setProfissaoSegurado(propostaBanco.getProfissaoSegurado());
		
		listaHistoricoCapital.add( historicoCapital );
		
		Collections.sort(listaHistoricoCapital);
		
		propostaGravar.getHistoricocapital_collection().addAll( listaHistoricoCapital );
		
		
		
	}	
	
	
	/**
	 * Realiza a valida��o da proposta antes de gravar
	 * @param propostaGravar
	 * @return
	 */
	private boolean validarPropostaGravar(Proposta propostaGravar) {
		
		boolean passou = true;
		
		boolean motivoCancelamentoVazio = proposta.getMotivoCancelamento() == null || proposta.getMotivoCancelamento().getId() == null;
		if(proposta.getDataCancelamento() != null && motivoCancelamentoVazio) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.motivo.cancelamento", null) );
			passou = false;
		}
		
		if(propostaGravar.getComandoComissao() == null) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "erro.data.comando", null));
			passou = false;
		}
		
		if (propostaGravar.getDataCancelamento() != null) {
			Calendar dtCancelamento = Calendar.getInstance();
			dtCancelamento.setTime(propostaGravar.getDataCancelamento());
			if (dtCancelamento.get(Calendar.DAY_OF_MONTH) != dtCancelamento.getActualMaximum(Calendar.DAY_OF_MONTH)) {
				addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.data.cancelamento.ultimo.dia", new Object[]{dtCancelamento.getActualMaximum(Calendar.DAY_OF_MONTH)}) );
				passou = false;
			}
		}
		
		if( propostaGravar.getEmpresa() == null || propostaGravar.getEmpresa().getId() == null ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.empresa", null) );
			passou = false;
		}
		
		if( propostaGravar.getModeloProposta() == null || propostaGravar.getModeloProposta().getId() == null ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.modeloProposta", null) );
			passou = false;
		}
		
		if( propostaGravar.getTipoProposta() == null || propostaGravar.getTipoProposta().getId() == null ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.tipoProposta", null) );
			passou = false;
		}	
		
		if( propostaGravar.getOrgao() == null || propostaGravar.getOrgao().getId() == null ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.orgao", null) );
			passou = false;
		}	
		
		if( !propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_ATUALIZADO) && !propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO) && (propostaGravar.getNomeTabela() == null || propostaGravar.getNomeTabela().getId() == null )){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.tabelaCapitais", null) );
			passou = false;
		}
		
		if(propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO) && propostaGravar.getValorPremioSegurado() == null) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.premio", null) );
			passou = false;
		}
		
		if(propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_ATUALIZADO) && propostaGravar.getValorPremioSegurado() == null) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.premio", null) );
			passou = false;
		}
		
		if(propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO) && propostaGravar.getValorPremioConjuge() == null
			&& propostaGravar.getTipoProposta().getComportamento() != ComportamentoPropostaEnum.INDIVIDUAL) {
			//&& !propostaGravar.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL)) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.premio.conjuge", null) );
			passou = false;
		}
		
		if(propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_ATUALIZADO) && propostaGravar.getValorPremioConjuge() == null
				&& getProposta().getTipoProposta().getComportamento() != ComportamentoPropostaEnum.INDIVIDUAL
				//&& !propostaGravar.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL)
				) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.premio.conjuge", null) );
			passou = false;
		}
		
		if( propostaGravar.getNomeSegurado() == null || "".equals( propostaGravar.getNomeSegurado() ) ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.nomeSeguradoPrincipal", null) );
			passou = false;
		}
		
		if( propostaGravar.getDataNascimentoSegurado() == null || "".equals( propostaGravar.getDataNascimentoSegurado() ) ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.dataNascimentoSegPrincipal", null) );
			passou = false;
		}
		
		if( propostaGravar.getCpfSegurado() == null || "".equals( propostaGravar.getCpfSegurado() ) ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.cpfSeguradoPrincipal", null) );
			passou = false;
		}
		
//		if( (  propostaGravar.getDetalheCapitalFaixaEtaria() == null || propostaGravar.getDetalheCapitalFaixaEtaria().getId() == null )  
//			&& ( propostaGravar.getCapitalSeguradoLimiteIdade() == null  || propostaGravar.getCapitalSeguradoLimiteIdade().getId() == null) 
//			&& (propostaGravar.getValorCapitalSegurado() == null)){
//			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.capitalSeguradoPrincipal", null) );
//			passou = false;
//		}
		
		
		if (propostaGravar.getModeloProposta().getId().equals(1) && (propostaGravar.getCapitalSeguradoLimiteIdade() == null  || propostaGravar.getCapitalSeguradoLimiteIdade().getId() == null)) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.capitalSeguradoPrincipal", null) );
			passou = false;
		}
		
		if (propostaGravar.getModeloProposta().getId().equals(2) && (propostaGravar.getDetalheCapitalFaixaEtaria() == null || propostaGravar.getDetalheCapitalFaixaEtaria().getId() == null )) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.capitalSeguradoPrincipal", null) );
			passou = false;
		}
		
		if (propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO) && propostaGravar.getValorCapitalSegurado() == null) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.capitalSeguradoPrincipal", null) );
			passou = false;
		}
		
		if (propostaGravar.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_ATUALIZADO) && propostaGravar.getValorCapitalSegurado() == null) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.capitalSeguradoPrincipal", null) );
			passou = false;
		}
		
		
		if( propostaGravar.getCidade() == null || propostaGravar.getCidade().getId() == null ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.cidade", null) );
			passou = false;
		}
		
		if( propostaGravar.getBeneficiario_collection() == null || propostaGravar.getBeneficiario_collection().size() == 0  ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.aoMenosUm.beneficiario", null) );
			passou = false;
		}

		if( propostaGravar.getPropostaagenciador_collection() == null || propostaGravar.getPropostaagenciador_collection().size() == 0  ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.aoMenosUm.agenciador", null) );
			passou = false;
		}	
		
		if( propostaGravar.getDataCadastro() == null ){
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.dataCadastro", null) );
			passou = false;
		}
		
		if(propostaGravar.getSexoSegurado() == null) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.sexo.segurado", null) );
			passou = false;
		}
		
		if(propostaGravar.getEstadoCivilSegurado() == null) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.estadocivil.segurado", null) );
			passou = false;
		}
		
		boolean validarcpf = true;
		boolean validarCpfConjuge = true;
		
		if (propostaGravar.getCpfSegurado() != null) {
			String CNPJ_CPF = propostaGravar.getCpfSegurado().replace(".", "").replace("-", "").replace("/", "").replace("_", "");
			if (CNPJ_CPF.equals("00000000000")) {
				validarcpf = false;
			}
		}
		
		if (propostaGravar.getId() != null) {
			if (propostaGravar.getId() == 12306 || propostaGravar.getId() == 21562 || propostaGravar.getId() == 26014 || propostaGravar.getId() == 21643 ) {
				validarcpf = false;
			}
		}
		
		if (validarcpf) {
			if (!validarCNPJCPF(propostaGravar.getCpfSegurado())) {
				addErrorMessage("O CPF do Segurado � inv�lido.");
				passou = false;
			}
			
			if(!StringUtils.isEmpty(propostaGravar.getCpfConjuge()) && !validarCNPJCPF(propostaGravar.getCpfConjuge())) {
				addErrorMessage("O CPF do C�njuge � inv�lido.");
				passou = false;
			}
			
			if(propostaGravar.getCpfSegurado() != null && !validarCPF(propostaGravar)) {
				addErrorMessage("O CPF do Segurado Principal j� est� cadastrado na base de dados. Favor verificar atrav�s da funcionalidade de Relat�rio de Segurados.");
				passou = false;
			}
		}
		
		if (propostaGravar.getTipoProposta() != null) {
			boolean propostaCasado = propostaGravar.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO;
			propostaCasado = propostaCasado || propostaGravar.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100;
			propostaCasado = propostaCasado || propostaGravar.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50;
		
			//Proposta do tipo casado, verifica se o nome do c�njuge foi preenchido
			if(propostaCasado) {
				if(StringUtils.isEmpty(propostaGravar.getNomeConjuge())) {
					addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.proposta.conjuge.nome.obrigatorio", null) );
					passou = false;
				}
				
				if (validarCpfConjuge) {
					if (!validarCNPJCPF(propostaGravar.getCpfConjuge())) {
						addErrorMessage("O CPF do C�njuge � inv�lido.");
						passou = false;
					}
				}
				
				if(StringUtils.isEmpty(propostaGravar.getCpfConjuge())) {
					addErrorMessage("O CPF do C�njuge � obrigat�rio.");
					passou = false;
				}
				
				if(propostaGravar.getDataNascimentoConjuge() == null) {
					addErrorMessage("A data de nascimento do c�njuge � obrigat�ria.");
					passou = false;
				}
				
			}
			
		}
		
		
		
		return passou;
	}

	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelarFiltro( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setPropostaFiltro( new Proposta() );
		getPropostaFiltro().setEmpresa( new Empresa() );
		getPropostaFiltro().setCidade(new Cidade());
		getPropostaFiltro().setOrgao(new Orgao() );
		getPropostaFiltro().setNomeTabela( new NomeTabela() );
		getPropostaFiltro().setFuncionario( new Funcionario() );
		getPropostaFiltro().setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
		getPropostaFiltro().setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
		getPropostaFiltro().setTipoProposta( new TipoProposta() );
		getPropostaFiltro().setModeloProposta( new ModeloProposta() );
		getPropostaFiltro().setBeneficiario_collection( new ArrayList<Beneficiario>() );
		getPropostaFiltro().setPropostaagenciador_collection( new ArrayList<PropostaAgenciador>() );
		situacaoPropostaSelectedList = new ArrayList<String>();
		setTipoPropostaComboFiltro(new ArrayList<SelectItem>() );
		populaOpcoesEmpresaCombo();
		this.resultadoPesquisa = null;
		this.fluxoInclusao = false;
		
	}
	
	/**
	 * @return the situacaoPropostaSelectedList
	 */
	public List<String> getSituacaoPropostaSelectedList() {
		return situacaoPropostaSelectedList;
	}

	/**
	 * @return the opcoesSituacaoPropostaCombo
	 */
	public List<SelectItem> getOpcoesSituacaoPropostaCombo() {
		return opcoesSituacaoPropostaCombo;
	}

	/**
	 * @param situacaoPropostaSelectedList the situacaoPropostaSelectedList to set
	 */
	public void setSituacaoPropostaSelectedList(
			List<String> situacaoPropostaSelectedList) {
		this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
	}

	/**
	 * @param opcoesSituacaoPropostaCombo the opcoesSituacaoPropostaCombo to set
	 */
	public void setOpcoesSituacaoPropostaCombo(
			List<SelectItem> opcoesSituacaoPropostaCombo) {
		this.opcoesSituacaoPropostaCombo = opcoesSituacaoPropostaCombo;
	}

	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelarInclusao( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setProposta( new Proposta() );
		
		getProposta().setEmpresa( new Empresa() );
		getProposta().setCidade(new Cidade());
		getProposta().setOrgao(new Orgao() );
		getProposta().setNomeTabela( new NomeTabela() );
		getProposta().setFuncionario( new Funcionario() );
		getProposta().setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
		getProposta().setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
		getProposta().setDetalheCapitalFaixaEtaria( new DetalheCapitalFaixaEtaria() );
		getProposta().setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
		getProposta().setTipoProposta( new TipoProposta() );
		getProposta().setModeloProposta( new ModeloProposta() );
		getProposta().setBeneficiario_collection( new ArrayList<Beneficiario>() );
		getProposta().setPropostaagenciador_collection( new ArrayList<PropostaAgenciador>() );
		getProposta().setMotivoCancelamento( new MotivoCancelamento() );
		getProposta().setDataCadastro( new Date() );
		setTipoPropostaComboFiltro(new ArrayList<SelectItem>() );
		setBeneficiario( new Beneficiario() );
		setBeneficiariosExcluidos(new ArrayList<Beneficiario>());
		getBeneficiario().setGrauParentesco( new GrauParentesco() );
		setAgenciador( new PropostaAgenciador() );
		getAgenciador().setFuncionario( new Funcionario() );
		setAgenciadoresExcluidos( new ArrayList<PropostaAgenciador>() );
		setIdadeConjuge("");
		setIdadeSegurado("");
		this.fluxoInclusao = true;
		setVisibleMessage( false );
		
	}
	
	/**
	 * S� para a chamada ajax!
	 * @return
	 */
	public void ajaxChangeTipoProposta( ActionEvent e ){
		getProposta().setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
		getProposta().setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
		getProposta().setDetalheCapitalFaixaEtaria( new DetalheCapitalFaixaEtaria() );
		getProposta().setDetalheCapitalFaixaEtariaFuturo( new DetalheCapitalFaixaEtaria());
	}
	
	
	/**
	 * Utilizado na popup de Capital Segurado Limite de Idade
	 * @param e
	 */
	public void capitalLimiteIdadeAutoSelect( ActionEvent e ){
		if(getProposta().getNomeTabela() == null || getProposta().getNomeTabela().getId() == null) {
			return;
		}
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		HtmlInputText param = (HtmlInputText) link.findComponent("txtCapital1");
		
		//Formata entrada
		Double capitalFormatado = ((Double) param.getValue() );
		Session session = HibernateUtil.currentSession();
		
		List<CapitalSeguradoLimiteIdade> resultado = new ArrayList<CapitalSeguradoLimiteIdade>();
		CapitalSeguradoLimiteIdadeDAO capitalDAO = new CapitalSeguradoLimiteIdadeDAO(session);
		//Converte para um formato entendivel
		try {
			resultado = capitalDAO.buscaPorCapital( capitalFormatado, getProposta().getNomeTabela() );
		} catch (Exception exc) {
			// N�o faz nada, pois o usu�rio entrou com um caracter inv�lido
		}
		
		int idadeSegurado = proposta.getIdadeRealSegurado();
		CapitalSeguradoLimiteIdade capitalLimiteTemp = null;
		for (CapitalSeguradoLimiteIdade capitalLimite : resultado) {
			
			// Se a idade est� no range, retorna o detalhe
			if( idadeSegurado <= 45 && capitalLimite.getFuncLimiteIdade() == 45 ){
				capitalLimiteTemp = capitalLimite;
				break;
			}else if( idadeSegurado > 45 &&  idadeSegurado <= 50 && capitalLimite.getFuncLimiteIdade() == 50 ){
				capitalLimiteTemp = capitalLimite;
				break;
			}else if( idadeSegurado > 50 && idadeSegurado <= 55 && capitalLimite.getFuncLimiteIdade() == 55 ){
				capitalLimiteTemp = capitalLimite;
				break;
			}else if( idadeSegurado > 55 &&  idadeSegurado <= 60 && capitalLimite.getFuncLimiteIdade() == 60 ){
				capitalLimiteTemp = capitalLimite;
				break;
			}
		}
		proposta.setCapitalSeguradoLimiteIdade( capitalLimiteTemp );
		
		populaCapitalConjuge(e);
	}
	
	/**
	 * Utilizado na popup de Capital Segurado Faixa Et�ria
	 * @param e
	 */
	public void capitalFaixaEtariaAutoSelect( ActionEvent e ){
		
		if(getProposta().getNomeTabela() == null || getProposta().getNomeTabela().getId() == null) {
			return;
		}
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		HtmlInputText param = (HtmlInputText) link.findComponent("txtCapital2");
		
		//Formata entrada
		Double capitalFormatado = ((Double) param.getValue() );
		Session session = HibernateUtil.currentSession();
		List<DetalheCapitalFaixaEtaria> resultado = new ArrayList<DetalheCapitalFaixaEtaria>();
		CapitalSeguradoFaixaEtariaDAO capitalDAO = new CapitalSeguradoFaixaEtariaDAO(session);
		try {
			resultado = capitalDAO.buscaPorCapital( capitalFormatado, getProposta().getNomeTabela() );
		} catch (Exception exc) {
			// N�o faz nada, pois o usu�rio entrou com um caracter inv�lido
		}

		int idadeSegurado = proposta.getIdadeRealSegurado();
		DetalheCapitalFaixaEtaria detalheCapitalFaixaTemp = null;
		for (DetalheCapitalFaixaEtaria detalheCapitalFaixa : resultado) {
			
			// Se a idade est� no range, retorna o detalhe
			if( idadeSegurado >= detalheCapitalFaixa.getIdadeInicial() && idadeSegurado <= detalheCapitalFaixa.getIdadeFinal() ){
				detalheCapitalFaixaTemp = detalheCapitalFaixa;
				break;
			}
		}
		proposta.setDetalheCapitalFaixaEtaria( detalheCapitalFaixaTemp );
		
		if(detalheCapitalFaixaTemp == null) {
			proposta.setDetalheCapitalFaixaEtaria(new DetalheCapitalFaixaEtaria());
		}
		
		populaCapitalConjuge(e);
		
		
	}	

	public void capitalFaixaEtariaFuturoAutoSelect( ActionEvent e ){
		
		if(getProposta().getNomeTabela() == null || getProposta().getNomeTabela().getId() == null) {
			return;
		}
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		HtmlInputText param = (HtmlInputText) link.findComponent("txtCapitalCapitalFuturoFaixaEtaria");

		
		//Formata entrada
		Double capitalFormatado = ((Double) param.getValue() );
		
//		Session session = HibernateUtil.currentSession();
//		List<DetalheCapitalFaixaEtaria> resultado = new ArrayList<DetalheCapitalFaixaEtaria>();
//		CapitalSeguradoFaixaEtariaDAO capitalDAO = new CapitalSeguradoFaixaEtariaDAO(session);
//		try {
//			resultado = capitalDAO.buscaPorCapital( capitalFormatado, getProposta().getNomeTabela() );
//		} catch (Exception exc) {
//			// N�o faz nada, pois o usu�rio entrou com um caracter inv�lido
//		}
//
//		int idadeSegurado = proposta.getIdadeRealSegurado();
//		DetalheCapitalFaixaEtaria detalheCapitalFaixaTemp = null;
//		for (DetalheCapitalFaixaEtaria detalheCapitalFaixa : resultado) {
//			
//			// Se a idade est� no range, retorna o detalhe
//			if( idadeSegurado >= detalheCapitalFaixa.getIdadeInicial() && idadeSegurado <= detalheCapitalFaixa.getIdadeFinal() ){
//				detalheCapitalFaixaTemp = detalheCapitalFaixa;
//				break;
//			}
//		}
//		proposta.setDetalheCapitalFaixaEtaria( detalheCapitalFaixaTemp );
//		
//		if(detalheCapitalFaixaTemp == null) {
//			proposta.setDetalheCapitalFaixaEtaria(new DetalheCapitalFaixaEtaria());
//		}
		
		populaCapitalConjuge(e);
		
		
	}	

	/**
	 * Utilizado na popup de Capital Segurado Limite de Idade
	 * @param e
	 */
	public List<CapitalSeguradoLimiteIdade> capitalLimiteIdadeAutoComplete( Object capital ){
		if(getProposta().getNomeTabela() == null || getProposta().getNomeTabela().getId() == null) {
			return null;
		}
		
		Session session = HibernateUtil.currentSession();
		
		List<CapitalSeguradoLimiteIdade> resultado = new ArrayList<CapitalSeguradoLimiteIdade>();
		CapitalSeguradoLimiteIdadeDAO capitalDAO = new CapitalSeguradoLimiteIdadeDAO(session);
		//Converte para um formato entendivel
		try {
			String formatado = ((String) capital).replace("R$ ", "");
			formatado = formatado.replace(".", "");
			formatado = formatado.replace(",", ".");
			resultado = capitalDAO.buscaPorCapital( new Double( (String) formatado), getProposta().getNomeTabela() );
		} catch (Exception e) {
			// N�o faz nada, pois o usu�rio entrou com um caracter inv�lido
		}
	
		return resultado;
	}
	
	/**
	 * Utilizado na popup de Capital Segurado Faixa Et�ria
	 * @param e
	 */
	public List<DetalheCapitalFaixaEtaria> capitalFaixaEtariaAutoComplete( Object capital ){
		if(getProposta().getNomeTabela() == null || getProposta().getNomeTabela().getId() == null) {
			return null;
		}
		
		//Formata entrada
		String capitalFormatado = ((String) capital).replace("R$ ", "");
		capitalFormatado = capitalFormatado.replace(".", "");
		capitalFormatado = capitalFormatado.replace(",", ".");
		Session session = HibernateUtil.currentSession();
		List<DetalheCapitalFaixaEtaria> resultado = new ArrayList<DetalheCapitalFaixaEtaria>();
		CapitalSeguradoFaixaEtariaDAO capitalDAO = new CapitalSeguradoFaixaEtariaDAO(session);
		try {
			resultado = capitalDAO.buscaPorCapital( new Double( (String) capitalFormatado), getProposta().getNomeTabela() );
		} catch (Exception e) {
			// N�o faz nada, pois o usu�rio entrou com um caracter inv�lido
		}
		return resultado;
	}

	/**
	 * Utilizado na popup de Capital Segurado Faixa Et�ria
	 * @param e
	 */
	public List<DetalheCapitalFaixaEtaria> capitalFaixaEtariaIcatuAutoComplete( Object capital ){
		if(getProposta().getNomeTabela() == null || getProposta().getNomeTabela().getId() == null) {
			return null;
		}
		
		//Formata entrada
		String capitalFormatado = ((String) capital).replace("R$ ", "");
		capitalFormatado = capitalFormatado.replace(".", "");
		capitalFormatado = capitalFormatado.replace(",", ".");
		Session session = HibernateUtil.currentSession();
		List<DetalheCapitalFaixaEtaria> resultado = new ArrayList<DetalheCapitalFaixaEtaria>();
		CapitalSeguradoFaixaEtariaDAO capitalDAO = new CapitalSeguradoFaixaEtariaDAO(session);
		NomeTabelaDAO nomeTabelaDAO = new NomeTabelaDAO(session);
		try {
			resultado = capitalDAO.buscaPorCapital( new Double( (String) capitalFormatado), nomeTabelaDAO.bucaTabelaIcatu());
		} catch (Exception e) {
			// N�o faz nada, pois o usu�rio entrou com um caracter inv�lido
		}
		return resultado;
	}
	
	// Fluxos de INCLUS�O / EXCLUS�O / ALTERA��O
	
	public String preparaIncluir(){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		cancelarInclusao(null);
		
		 Dao<Proposta, Integer> dao = new Dao<Proposta, Integer>( session, Proposta.class );
		 Integer ultimoIdInserido = dao.retornaUltimoIdInserido( Proposta.class );
		 setNumeroPropostaIncluir( ultimoIdInserido + 1 );
		 populaOpcoesAgenciadorCombo();
		 populaMotivosCancelamento();
		 populaOpcoesEmpresaCombo();
		 
		 //Seta a data de cadastro como a data atual
		 proposta.setDataCadastro( new Date() );
		 proposta.setNomeTabela(new NomeTabela());
		 proposta.setOrgao(new Orgao());
		 
		return "incluirProposta";
	}
		
	
	/**
	 * Fluxo de Altera��o de Proposta
	 * @return
	 */
	public String preparaAlterar(){
		//Recupera os benefici�rios desta proposta
		Session session = HibernateUtil.currentSession();
		
		setTipoPropostaComboFiltro(new ArrayList<SelectItem>() );
		setBeneficiario( new Beneficiario() );
		setBeneficiariosExcluidos(new ArrayList<Beneficiario>());
		getBeneficiario().setGrauParentesco( new GrauParentesco() );
		setAgenciador( new PropostaAgenciador() );
		getAgenciador().setFuncionario( new Funcionario() );
		setAgenciadoresExcluidos( new ArrayList<PropostaAgenciador>() );
		
		BeneficiarioDAO beneficiarioDAO =  new BeneficiarioDAO( session );
		List<Beneficiario> listaBeneficiarios = beneficiarioDAO.buscaPorProposta( proposta.getId() );
		getProposta().setBeneficiario_collection( listaBeneficiarios);
		
		//Recupera o hist�rico de altera��es do benefici�rio (se houver)
		HistoricoBeneficiarioDAO historicoBeneficiarioDAO =  new HistoricoBeneficiarioDAO( session );
		List<HistoricoBeneficiario> listaHistoricoBeneficiarios = historicoBeneficiarioDAO.buscaPorProposta( proposta.getId() );
		getProposta().setHistoricobeneficiario_collection( listaHistoricoBeneficiarios);
		
		//Recupera o hist�rico de altera��es do benefici�rio (se houver)
		HistoricoCapitalDAO historicoCapitalDAO =  new HistoricoCapitalDAO( session );
		List<HistoricoCapital> listaHistoricoCapitais = historicoCapitalDAO.buscaPorProposta( proposta.getId() );
		getProposta().setHistoricocapital_collection( listaHistoricoCapitais );
		
		//Recupera as comiss�es agenciadores (se houver)
		ComissaoAgenciadorDAO comissaoAgenciadorDAO = new ComissaoAgenciadorDAO(session);
		List<ComissaoAgenciador> comissaoAgenciadorCollection = comissaoAgenciadorDAO.buscaPorProposta(proposta.getId());
		getProposta().setComissaoagenciador_collection(comissaoAgenciadorCollection);
		
		// Recupera o hist�rico de altera��es do  (se houver)
		HistoricoConjugeDAO historicoConjuge = new HistoricoConjugeDAO(
				session);
		List<HistoricoConjuge> listaHistoricoConjuge = historicoConjuge
				.buscaPorProposta(proposta.getId());
		getProposta().setHistoricoconjuge_collection(
				listaHistoricoConjuge);
		
		//Recupera o hist�rico de altera��es do  (se houver)
		HistoricoEnderecoSeguradoDAO historicoEnderecoSeguradoDAO = new HistoricoEnderecoSeguradoDAO(
				session);
		List<HistoricoEnderecoSegurado> listaHistoricoEnderecoSegurado = historicoEnderecoSeguradoDAO
				.buscaPorProposta(proposta.getId());
		getProposta().setHistoricoenderecosegurado_collection(
				listaHistoricoEnderecoSegurado);
		
		// Recupera o hist�rico de altera��es do (se houver)
		HistoricoPropostaDAO historicoPropostaDAO = new HistoricoPropostaDAO(
				session);
		List<HistoricoProposta> listaHistoricoProposta = historicoPropostaDAO
				.buscaPorProposta(proposta.getId());
		getProposta().setHistoricoproposta_collection(
				listaHistoricoProposta);		
		
		//Recupera os agenciadores desta proposta
		PropostaAgenciadorDAO propostaAgenciadorDao =  new PropostaAgenciadorDAO( session );
		List<PropostaAgenciador> listaAgenciadores = propostaAgenciadorDao.buscaPorProposta( proposta.getId() );
		getProposta().setPropostaagenciador_collection( listaAgenciadores);
			
		
		//Recupera os n�meros de sorteio desta proposta
		NumeroSorteioDAO numeroSorteioDao =  new NumeroSorteioDAO( session );
		List<NumeroSorteio> listaNumerosSorteio = numeroSorteioDao.buscaPorProposta( proposta.getId() );
		String numeroSorteioStr = "";
		if( listaNumerosSorteio != null && listaNumerosSorteio.size() > 0 ){
			NumeroSorteio numeroSorteioTemp = listaNumerosSorteio.get( listaNumerosSorteio.size() - 1 );
			numeroSorteioStr = numeroSorteioTemp.getNumeroSorteio();
		}
		getProposta().setNumeroSorteio( numeroSorteioStr );
		setNumeroSorteio( numeroSorteioStr );
		getProposta().setNumerosorteio_collection( listaNumerosSorteio );
		
	
		setNumeroPropostaIncluir( proposta.getId() );
		populaOpcoesAgenciadorCombo();
		populaTipoPropostaCombo(null);
		populaMotivosCancelamento();
		populaOpcoesEmpresaCombo();
		calculaIdadeSeguradoPrincipal(null);
		calculaIdadeSeguradoConjuge(null);
		populaCapitalConjuge( null );
		fluxoInclusao = false;
		if( proposta.getMotivoCancelamento() == null ){
			proposta.setMotivoCancelamento( new MotivoCancelamento() );
		}
		
		//Tratando situa��es onde n�o h� dados na base
		if( proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_VALOR_FIXO ) && proposta.getCapitalSeguradoLimiteIdade() == null){
			proposta.setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
		}
		
		if( proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_ETARIA ) && proposta.getCapitalSeguradoFaixaEtaria() == null ){
			proposta.setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
		}
		
		if( proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_VALOR_FIXO )) {
			proposta.setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
			proposta.setDetalheCapitalFaixaEtaria(new DetalheCapitalFaixaEtaria());
			proposta.setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
		}
		
		if( proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_ETARIA )) {
			proposta.setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
		}

		if( proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO )) {
			proposta.setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
			proposta.setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
			proposta.setDetalheCapitalFaixaEtaria(new DetalheCapitalFaixaEtaria());
			proposta.setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
		}
		
		if( proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_ATUALIZADO )) {
			proposta.setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
			proposta.setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
			proposta.setDetalheCapitalFaixaEtaria(new DetalheCapitalFaixaEtaria());
			proposta.setDetalheCapitalFaixaEtariaFuturo(new DetalheCapitalFaixaEtaria());
		}
		
		if(proposta.getModeloProposta().getId().equals(Constantes.MODELO_PROPOSTA_VALOR_FIXO)) {
			fixoCapitalConjugeFuturo = proposta.getValorFuturoCapitalConjuge();
			fixoCapitalFuturo = proposta.getValorFuturoCapitalSegurado();
			fixoPremioConjugeFuturo = proposta.getValorFuturoPremioConjuge();
			fixoPremioFuturo = proposta.getValorFuturoPremioSegurado();
		}
		
		if (proposta.getSuperAp() != null) {
			return "superApIncluir";
		} else {
			return "incluirProposta";
		}
	}	
	
	public void calculaIdadeSeguradoPrincipal(ActionEvent e){
		Date dataNascimento = getProposta().getDataNascimentoSegurado();
		DateTime start = new DateTime(dataNascimento);
		DateTime end = new DateTime(Calendar.getInstance());
		
		if (dataNascimento != null) {
			 Period per = new Period (start, end);
		        // Isto imprime "27 years, 6 months, 3 weeks and 3 days"
		        System.out.println (PeriodFormat.getDefault().print (per));
		        // Isto imprime "27 anos, 6 meses, 3 semanas e 3 dias"
		        PeriodFormatter pf = new PeriodFormatterBuilder()
				    .appendYears()
					.appendSuffix (" ano, ", " anos, ")
		            .appendMonths() 
		            .appendSuffix (" m�s, ", " meses, ")
		            .appendWeeks() 
		            .appendSuffix (" semana e ", " semanas e ")
		            .appendDays()
		            .appendSuffix (" dia ", " dias ")
		            .toFormatter();
		     
		     setIdadeSegurado( pf.print (per));
			 proposta.setIdadeRealSegurado( per.getYears() );
		} else {
			setIdadeSegurado("");
		}
	}
	
	
	
	public void calculaIdadeSeguradoConjuge(ActionEvent e){
		Date dataNascimento = getProposta().getDataNascimentoConjuge();
		
		if (dataNascimento != null) {
			DateTime start = new DateTime(dataNascimento);
			DateTime end = new DateTime(Calendar.getInstance());
			
			 Period per = new Period (start, end);
		        // Isto imprime "27 years, 6 months, 3 weeks and 3 days"
		        System.out.println (PeriodFormat.getDefault().print (per));
		        // Isto imprime "27 anos, 6 meses, 3 semanas e 3 dias"
		        PeriodFormatter pf = new PeriodFormatterBuilder()
				    .appendYears()
					.appendSuffix (" ano, ", " anos, ")
		            .appendMonths() 
		            .appendSuffix (" m�s, ", " meses, ")
		            .appendWeeks() 
		            .appendSuffix (" semana e ", " semanas e ")
		            .appendDays()
		            .appendSuffix (" dia ", " dias ")
		            .toFormatter();
		     
			 setIdadeConjuge(pf.print (per));
		} else {
			setIdadeConjuge("");
		}
	}
	 
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirProposta(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirProposta");
		Proposta propostaDeletada  = ( Proposta ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<Proposta, Integer> dao = new Dao<Proposta, Integer>(session, Proposta.class);
		dao.delete(propostaDeletada);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaPropostas(e);
	}		
	
	/**
	 * Realiza a c�pia de uma proposta
	 * @param e
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public void copiarProposta( ActionEvent e ) throws IllegalAccessException, InvocationTargetException{
		Session session = HibernateUtil.currentSession();
		Dao<Proposta, Integer> dao = new Dao<Proposta, Integer>( session, Proposta.class );
		if( numeroPropostaCopiar != null && numeroPropostaCopiar != 0){
			Proposta propostaCopy = null; 
				propostaCopy = dao.get( numeroPropostaCopiar );
			if( propostaCopy == null || propostaCopy.getId() == null ){
				validaGenerico( Constantes.KEY_MENSAGEM_PROPOSTA_NAO_ENCONTRADA, new ArrayList<String>()  );
			}else{
				Proposta novaProposta = new Proposta();
				
				//Seta os id�s para null
				if( proposta.getId() != null ){
					novaProposta.setId( proposta.getId() );
				}else{
					novaProposta.setId(null);
				}
				novaProposta.setTipoProposta( propostaCopy.getTipoProposta() );
				novaProposta.setModeloProposta( propostaCopy.getModeloProposta() );
				novaProposta.setOrgao( propostaCopy.getOrgao() );
				novaProposta.setNomeTabela( propostaCopy.getNomeTabela() );
				novaProposta.setCapitalSeguradoFaixaEtaria( propostaCopy.getCapitalSeguradoFaixaEtaria() );
				novaProposta.setCapitalSeguradoLimiteIdade( propostaCopy.getCapitalSeguradoLimiteIdade() );
				novaProposta.setBairro( propostaCopy.getBairro() );
				novaProposta.setCidade( propostaCopy.getCidade() );
				novaProposta.setTelefoneComercial( propostaCopy.getTelefoneComercial() );
				novaProposta.setTelefoneCelular2( propostaCopy.getTelefoneCelular2() );
				novaProposta.setPropostaagenciador_collection( propostaCopy.getPropostaagenciador_collection() );
				setProposta( novaProposta );
				
			}
		}else{
			numeroPropostaCopiar = null;
			ArrayList<String> array = new ArrayList<String>();
			array.add( "Copiar Proposta" );
			validaGenerico( Constantes.KEY_VALIDACAO_CAMPO_DEVE_SER_PREENCHIDO, array );
		}
		
	}	
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getTipoPercentualCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add( new SelectItem( new Integer(1), "Dividir em Partes Iguais" ) );
		retorno.add( new SelectItem( new Integer(2), "Informar Percentual" ) );
		
		return retorno;
	}
	

	
// INCLUS�O DO BENEFICI�RIO ===================================================	INCLUS�O DO BENEFICI�RIO
	
	/**
	 * S� para a chamada ajax!
	 * @return
	 */
	public void ajaxChangeTipoPercentualCombo( ActionEvent e ){
		getBeneficiario().setPercentual( null );
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormularioBeneficiario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeBeneficiario", "Nome do Benefici�rio");
		mapaCampos.put("txtGrauParentesco", "Grau de Parentesco");
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		if( !retorno ){ 
			return retorno;
		}
		
		retorno = validaPreenchimentoObrigatorioListaSelecao(getBeneficiario().getGrauParentesco(), "Grau de Parentesco" );
		
		return retorno;
	}
	
	/**
	 * Realiza a inclus�o de um novo Benefici�rio que ser� persistido no gravar
	 * @param e
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public void incluirBeneficiario( ActionEvent e ) throws IllegalAccessException, InvocationTargetException{
		
		if( validarFormularioBeneficiario(e) ){
			
			if( alterarBeneficiario != null && alterarBeneficiario ){
				removeBeneficiario(beneficiario);
				boolean erro = addBeneficiario(e);
				if( !erro ){
					alterarBeneficiario = false;
				}
			}else{
				beneficiarioTemp = getBeneficiario();
				addBeneficiario(e);
			}
			
		}
			
	}


	private boolean addBeneficiario(ActionEvent e) throws IllegalAccessException, InvocationTargetException {
		//Verifica se j� excedeu 100%
		double totalizador = 0.0;
		for (Iterator iterator = getProposta().getBeneficiario_collection().iterator(); iterator.hasNext();) {
			Beneficiario beneficiario = (Beneficiario) iterator.next();
			totalizador += ( new BigDecimal( beneficiario.getPercentual() ) ).setScale(2, BigDecimal.ROUND_DOWN ).doubleValue();
		}
		
		if( beneficiario.getPercentual() != null ){
			totalizador += ( new BigDecimal( beneficiario.getPercentual() ) ).setScale(2, BigDecimal.ROUND_DOWN ).doubleValue();
		}
		boolean erro = false;
		if( totalizador > 100 ){
			addErrorMessage("O somat�rio dos percentuais n�o pode exceder 100%. ");
			//se � fluxo de alteracao
			if( alterarBeneficiario ){
				BeanUtils.copyProperties(beneficiario, beneficiarioTemp);
				getProposta().getBeneficiario_collection().add( beneficiario );
			}
			
			erro = true;
		}else{
			getProposta().getBeneficiario_collection().add( beneficiario );
		}

		if( getBeneficiario().getTipoPercentual().equals( Constantes.TIPO_PERCENTUAL_PARTES_IGUAIS ) ){
			double tamanho = getProposta().getBeneficiario_collection().size();
			for (Iterator iterator = getProposta().getBeneficiario_collection().iterator(); iterator.hasNext();) {
				Beneficiario beneficiario = (Beneficiario) iterator.next();
				beneficiario.setPercentual(  100/tamanho );
			}
		}
		

		if( !erro ){
			cancelarBeneficiario(e);
			beneficiarioTemp = null;
		}
		
		return erro;
	}
	
	public void valorAlterado(ValueChangeEvent e){
		System.out.println("alterado");
	}
	
	public void atualizarValorFuturo(ActionEvent e) {
		if( getProposta().getModeloProposta() != null && getProposta().getModeloProposta().getId() != null 
				&& getProposta().getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_ETARIA ) ) {
			if(getProposta().getDetalheCapitalFaixaEtariaFuturo() != null) {
				
				Session session = HibernateUtil.currentSession();
				NomeTabelaDAO nomeTabelaDAO = new NomeTabelaDAO(session);
				
				//passa o valor futuro para o presente e apaga o valor futuro
				getProposta().getDetalheCapitalFaixaEtaria().setCapitalSegurado(getProposta().getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado());
				getProposta().setCapitalConjuge(getProposta().getCapitalConjugeFuturo());
				
				getProposta().setPremioSegurado(getProposta().getPremioSeguradoFuturo());
				getProposta().setPremioConjuge(getProposta().getPremioConjugeFuturo());
				
				getProposta().setNomeTabela(nomeTabelaDAO.bucaTabelaIcatu());
				
				getProposta().setPremioSeguradoFuturo(null);
				getProposta().getDetalheCapitalFaixaEtariaFuturo().setCapitalSegurado(null);
				getProposta().setPremioConjugeFuturo(null);
				getProposta().setCapitalConjugeFuturo(null);
			}
		} else {
			getProposta().setValorCapitalSegurado(getProposta().getValorFuturoCapitalSegurado());
			getProposta().setValorPremioSegurado(getProposta().getValorFuturoPremioSegurado());
			getProposta().setValorCapitalConjuge(getProposta().getCapitalConjugeFuturo());
			getProposta().setValorPremioConjuge(getProposta().getValorFuturoPremioConjuge());
			getProposta().setCapitalConjuge(getProposta().getCapitalConjugeFuturo());
			getProposta().setPremioConjuge(getProposta().getValorFuturoPremioConjuge());
			
			getProposta().setValorFuturoCapitalSegurado(null);
			getProposta().setValorFuturoCapitalConjuge(null);
			getProposta().setValorFuturoPremioSegurado(null);
			getProposta().setValorFuturoPremioConjuge(null);
			getProposta().setCapitalConjugeFuturo(null);
			getProposta().setPremioConjugeFuturo(null);
			
		}
		
	}
	
	public void atualizar(ActionEvent e) {
		System.out.println("atualizar");
	}
	
	/**
	 * Realiza a exclus�o de um Benefici�rio que ser� persistido no gravar
	 * @param e
	 */
	public void excluirBeneficiario( ActionEvent e ){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirBeneficiario");
		Beneficiario beneficiario = ( Beneficiario ) param.getValue();
		removeBeneficiario(beneficiario);
	}


	private void removeBeneficiario(Beneficiario beneficiario) {
		if( beneficiariosExcluidos == null ){
			beneficiariosExcluidos = new ArrayList<Beneficiario>();
		}
		getBeneficiariosExcluidos().add( beneficiario );
		getProposta().getBeneficiario_collection().remove( beneficiario );

		
		if( getBeneficiario().getTipoPercentual().equals( Constantes.TIPO_PERCENTUAL_PARTES_IGUAIS ) ){
			double tamanho = getProposta().getBeneficiario_collection().size();
			for (Iterator iterator = getProposta().getBeneficiario_collection().iterator(); iterator.hasNext();) {
				Beneficiario beneficiarioTemp = (Beneficiario) iterator.next();
				beneficiarioTemp.setPercentual(  100/tamanho );
			}
		}
	}
	
	
	/**
	 * Realiza a altera��o de um Benefici�rio que ser� persistido no gravar
	 * @param e
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public void alterarBeneficiario( ActionEvent e ) throws IllegalAccessException, InvocationTargetException{
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("editBeneficiario");
		Beneficiario beneficiario = ( Beneficiario ) param.getValue();
		if( beneficiario.getTipoPercentual() == null ){
			beneficiario.setTipoPercentual( 2 );
		}
		beneficiarioTemp = new Beneficiario();
		BeanUtils.copyProperties(beneficiarioTemp, beneficiario);
		setBeneficiario( beneficiario );
		setAlterarBeneficiario( true );
		
						
	}
	
	/**
	 * Realiza a inclus�o de um novo Benefici�rio que ser� persistido no gravar
	 * @param e
	 */
	public void cancelarBeneficiario( ActionEvent e ){		
		setBeneficiario( new Beneficiario() );	
		getBeneficiario().setTipoPercentual( 1 );
		getBeneficiario().setGrauParentesco( new GrauParentesco() );
		setAlterarBeneficiario( false );
	}

	
	// INCLUS�O DO AGENCIADOR ===================================================	INCLUS�O DO AGENCIADOR
		
		/**
		 * S� para a chamada ajax!
		 * @return
		 */
		public void ajaxChangeOpcaoAgenciador( ActionEvent e ){
			getAgenciador().setPercentual( null );
			getAgenciador().setValorPagoDefinido( null );
		}	
	
		/**
		 * Popula um combo com op��es do Agenciador 
		 * @return
		 */
		public void populaOpcoesAgenciadorCombo(){
			
			//Realiza as a��es de banco: Busca e seta os valores
			opcoesAgenciadorCombo = new ArrayList<SelectItem>();
			
			opcoesAgenciadorCombo.add( new SelectItem( "A", "Calcular Automaticamente" ) );
			opcoesAgenciadorCombo.add( new SelectItem( "D", "Definir Valor a Pagar" ) );
			opcoesAgenciadorCombo.add( new SelectItem( "C", "Comiss�o Adicional" ) );
			
		}
		
		/**
		 * Popula um combo com op��es do Agenciador 
		 * @return
		 */
		public void populaOpcoesEmpresaCombo(){
			
			Session session = HibernateUtil.currentSession();
			EmpresaDAO empresaDAO =  new EmpresaDAO( session );
			
			List<Empresa> listaEmpresas = empresaDAO.buscaEmpresas();
			
			//Realiza as a��es de banco: Busca e seta os valores
			opcoesEmpresaCombo = new ArrayList<SelectItem>();
			opcoesEmpresaCombo.add( new SelectItem( null, "Selecione:" ) );
			for (Iterator iterator = listaEmpresas.iterator(); iterator.hasNext();) {
				Empresa empresa = (Empresa) iterator.next();
				opcoesEmpresaCombo.add( new SelectItem( empresa.getId(), empresa.getNomeEmpresa()) );
			}
			
		}
		
		/**
		 * Popula um combo com op��es do Agenciador 
		 * @return
		 */
		public void populaMotivosCancelamento(){
			
			Session session = HibernateUtil.currentSession();
			MotivoCancelamentoDAO beneficiarioDAO =  new MotivoCancelamentoDAO( session );

			List<MotivoCancelamento> listaMotivoCancelamento = beneficiarioDAO.buscaMotivosCancelamentos();
			
			//Realiza as a��es de banco: Busca e seta os valores
			opcoesMotivoCancelamento = new ArrayList<SelectItem>();
			opcoesMotivoCancelamento.add( new SelectItem( null, "Selecione:" ) );
			for (Iterator iterator = listaMotivoCancelamento.iterator(); iterator.hasNext();) {
				MotivoCancelamento motivoCancelamento = (MotivoCancelamento) iterator.next();
				opcoesMotivoCancelamento.add( new SelectItem( motivoCancelamento.getId(), motivoCancelamento.getDescricao() ));
			}
			
		}		
		
		/**
		 * Realiza todas as valida��es
		 * @return
		 */
		public boolean validarFormularioAgenciador(ActionEvent e){
			
			HashMap<String, String> mapaCampos = new HashMap<String, String>();
			mapaCampos.put("txtAgenciador", "Agenciador");
			mapaCampos.put("txtOrdemAgenciamento", "Ordem de Agenciamento");
			boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
			if( !retorno ){ 
				return retorno;
			}
			
			retorno = validaPreenchimentoObrigatorioListaSelecao(getAgenciador().getFuncionario().getId(), "Agenciador" );
			
			return retorno;
		}
		
		public String goIncluirHistorico( ){
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			request.getSession().setAttribute( "idProposta", getNumeroPropostaParameter() );
			
			return "incluirSinistro";
		}
		
		
		/**
		 * Realiza a inclus�o de um novo Benefici�rio que ser� persistido no gravar
		 * @param e
		 */
		public void incluirAgenciador( ActionEvent e ){
			
			if( validarFormularioAgenciador(e) ){
				agenciador.setValorTotalPremio( getProposta().getTotalPremios() );
				getProposta().getPropostaagenciador_collection().remove( getAgenciador() );
				getProposta().getPropostaagenciador_collection().add( getAgenciador() );
				cancelarAgenciador(e);
			}
			
		}

		/**
		 * Realiza a exclus�o de um Agenciador que ser� persistido no gravar
		 * @param e
		 */
		public void excluirAgenciador( ActionEvent e ){
			
			//Recupera o item escolhido
			UIComponent link = e.getComponent();
			UIParameter param = (UIParameter) link.findComponent("excluirAgenciador");
			PropostaAgenciador agenciador = ( PropostaAgenciador ) param.getValue();
			if( agenciadoresExcluidos == null ){
				agenciadoresExcluidos = new ArrayList<PropostaAgenciador>();
			}
			getAgenciadoresExcluidos().add( agenciador );
			getProposta().getPropostaagenciador_collection().remove( agenciador );
							
		}
		
		
		/**
		 * Realiza a altera��o de um Agenciador que ser� persistido no gravar
		 * @param e
		 */
		public void alterarAgenciador( ActionEvent e ){
			
			//Recupera o item escolhido
			UIComponent link = e.getComponent();
			UIParameter param = (UIParameter) link.findComponent("editAgenciador");
			PropostaAgenciador agenciador = ( PropostaAgenciador ) param.getValue();
			setAgenciador( agenciador );
							
		}
		
		
		/**
		 * Realiza o cancelamento das a��es de inclus�o/altera��o de um Agenciador
		 * @param e
		 */
		public void cancelarAgenciador( ActionEvent e ){		
			setAgenciador( new PropostaAgenciador() );
			getAgenciador().setFuncionario( new Funcionario() );
		}
		
		/**
		 * Popula um combo com valores dos tipos de relatorio
		 * @return
		 */
		public List<SelectItem> getTipoRelatorioCombo(){
			
			
			List<SelectItem> retorno = new ArrayList<SelectItem>();
			retorno.add(new SelectItem("", "Selecione:"));
			retorno.add(new SelectItem(1, "Segurados Cancelados"));
			retorno.add(new SelectItem(2, "Segurados Sem Aprovacao"));
			retorno.add(new SelectItem(3, "Mudan�a de Plano Faixa Et�ria "));
			retorno.add(new SelectItem(4, "Segurados Ativos"));
			retorno.add(new SelectItem(5, "Comiss�o do Agenciador"));
			retorno.add(new SelectItem(10, "Comiss�o de Altera��o"));
			
			return retorno;
		}
		
		private void buscaCapitalFaixaEtaria( ){

			//Realiza as a��es de banco: Busca e seta os valores
			Session session = HibernateUtil.currentSession();

			Dao<DetalheCapitalFaixaEtaria, Long> daoDetalheCapital = new Dao<DetalheCapitalFaixaEtaria, Long>(session, DetalheCapitalFaixaEtaria.class);

			List<DetalheCapitalFaixaEtaria> detalheCapitalFaixaFull = daoDetalheCapital.list();
			Map<Integer, List<DetalheCapitalFaixaEtaria> > mapaDetalheCapitalFaixaChaveFull = new HashMap<Integer, List<DetalheCapitalFaixaEtaria> >();
			for (DetalheCapitalFaixaEtaria detalheCapitalFaixa : detalheCapitalFaixaFull ) {

				Integer chave = detalheCapitalFaixa.getCapitalSeguradoFaixaEtaria().getNomeTabela().getId();
				if( mapaDetalheCapitalFaixaChaveFull.containsKey( chave ) ){
					List<DetalheCapitalFaixaEtaria> listaDetalhesFaixaEtaria = mapaDetalheCapitalFaixaChaveFull.get(chave);
					listaDetalhesFaixaEtaria.add( detalheCapitalFaixa );
				}else{
					List<DetalheCapitalFaixaEtaria> listaDetalhesFaixaEtaria = new ArrayList<DetalheCapitalFaixaEtaria>();
					listaDetalhesFaixaEtaria.add( detalheCapitalFaixa );
					mapaDetalheCapitalFaixaChaveFull.put( chave , listaDetalhesFaixaEtaria );
				}
			}

			//Para o relat�rio de faixa et�ria devemos pegar o segurado, pegar o DetalheCapitalFaixaEtaria dele e verificar se a idade � condizente com o valor que ele est� pagando.

			Proposta proposta = getProposta();

			if( proposta.getModeloProposta() != null && 
					Constantes.MODELO_PROPOSTA_FAIXA_ETARIA.equals( proposta.getModeloProposta().getId() ) ){

				Integer idadeSegurado = proposta.getIdadeRealSegurado();
				DetalheCapitalFaixaEtaria detalheCapitalSegurado = proposta.getDetalheCapitalFaixaEtaria();

				//Se o range est� fora, ent�o verifica a qual range pertence usando o mapa de detalhes
				Integer chave = proposta.getNomeTabela().getId();
				List<DetalheCapitalFaixaEtaria> listaDetalhesFaixaEtaria = mapaDetalheCapitalFaixaChaveFull.get(chave);
				DetalheCapitalFaixaEtaria detalheCapitalFaixaEtariaTemp = new DetalheCapitalFaixaEtaria();
				for (DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria : listaDetalhesFaixaEtaria) {
					if( idadeSegurado >= detalheCapitalFaixaEtaria.getIdadeInicial() && idadeSegurado <= detalheCapitalFaixaEtaria.getIdadeFinal() ){
						// Verifica o mesmo capital
						if( detalheCapitalSegurado.getCapitalSegurado().equals( detalheCapitalFaixaEtaria.getCapitalSegurado() ) ){
							//Se encontrou o range adquire o capital para extrair informa��es
							detalheCapitalFaixaEtariaTemp = detalheCapitalFaixaEtaria;
							break;
						}
					}
				}

				proposta.setDetalheCapitalFaixaEtaria( detalheCapitalFaixaEtariaTemp );
				proposta.setCapitalSeguradoFaixaEtaria( detalheCapitalFaixaEtariaTemp.getCapitalSeguradoFaixaEtaria() );



			}
			
		}
		
		
		public String generateMalaDireta(  ){
			
	        try {  
	        	InputStream excelFIS = this.getClass().getResourceAsStream(Constantes.SISVIDA_FILES + "etiqueta.xls");
			    HSSFWorkbook wb = new HSSFWorkbook(excelFIS);
	            HSSFSheet sheet = wb.getSheetAt(0); 
	              
	            HSSFRow row;  
	            
	            
	            int rowCountEsquerdo = 1;
	            int rowCountDireito = 1;
	            row = sheet.getRow(rowCountEsquerdo);	
				HSSFFont fontCabecalho = wb.createFont();  
	            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); 
	            if(  propostasSelecionadas != null && propostasSelecionadas.length > 0 ){
	            
		            for (int i = 0; i < propostasSelecionadas.length; i++) {
		            	Proposta propostaMalaDireta = propostasSelecionadas[i];
		            	HSSFRichTextString richTextNomeSegurado = new HSSFRichTextString( propostaMalaDireta.getNomeSegurado() ); 
		            	HSSFRichTextString richTextEndereco = new HSSFRichTextString( propostaMalaDireta.getEnderecoSegurado() + propostaMalaDireta.getComplemento() );
		            	HSSFRichTextString richTextBairro = new HSSFRichTextString( propostaMalaDireta.getBairro() );
		            	HSSFRichTextString richCEPCidade = new HSSFRichTextString( propostaMalaDireta.getCep() + " / " + propostaMalaDireta.getCidade().getNom_cidade() + " - " + propostaMalaDireta.getCidade().getCod_estado().getSgl_estado()  );
		            	if( i % 2 == 0){
				            row = sheet.getRow( rowCountEsquerdo );
				            if( row == null ){
				            	sheet.createRow( rowCountEsquerdo );
				            	row = sheet.getRow( rowCountEsquerdo );
				            	row.createCell(1);
				            }
				            row.getCell(1).setCellValue(richTextNomeSegurado); 		            
			            	rowCountEsquerdo++;
				            row = sheet.getRow( rowCountEsquerdo );
				            if( row == null ){
				            	sheet.createRow( rowCountEsquerdo );
				            	row = sheet.getRow( rowCountEsquerdo );
				            	row.createCell(1);
				            }
				            row.getCell(1).setCellValue(richTextEndereco); 		            
			            	rowCountEsquerdo++;	
				            row = sheet.getRow( rowCountEsquerdo );
				            if( row == null ){
				            	sheet.createRow( rowCountEsquerdo );
				            	row = sheet.getRow( rowCountEsquerdo );
				            	row.createCell(1);
				            }
				            row.getCell(1).setCellValue(richTextBairro); 		            
			            	rowCountEsquerdo++;			
				            row = sheet.getRow( rowCountEsquerdo );
				            if( row == null ){
				            	sheet.createRow( rowCountEsquerdo );
				            	row = sheet.getRow( rowCountEsquerdo );
				            	row.createCell(1);
				            }
				            row.getCell(1).setCellValue(richCEPCidade); 		            
			            	rowCountEsquerdo++;	
			            	rowCountEsquerdo++;	
			            	rowCountEsquerdo++;	
			            	rowCountEsquerdo++;	
			            }
		            	if( i % 2 != 0){
				            row = sheet.getRow( rowCountDireito );
				            if( row == null ){
				            	sheet.createRow( rowCountDireito );
				            	row = sheet.getRow( rowCountDireito );
				            	row.createCell(3);
				            }
				            row.getCell(3).setCellValue(richTextNomeSegurado); 	
				            
				            rowCountDireito++;
				            row = sheet.getRow( rowCountDireito );
				            if( row == null ){
				            	sheet.createRow( rowCountDireito );
				            	row = sheet.getRow( rowCountDireito );
				            	row.createCell(3);
				            }
				            row.getCell(3).setCellValue(richTextEndereco); 		            
				            rowCountDireito++;	
				            row = sheet.getRow( rowCountDireito );
				            if( row == null ){
				            	sheet.createRow( rowCountDireito );
				            	row = sheet.getRow( rowCountDireito );
				            	row.createCell(3);
				            }
				            row.getCell(3).setCellValue(richTextBairro); 		            
				            rowCountDireito++;			
				            row = sheet.getRow( rowCountDireito );
				            if( row == null ){
				            	sheet.createRow( rowCountDireito );
				            	row = sheet.getRow( rowCountDireito );
				            	row.createCell(3);
				            }
				            row.getCell(3).setCellValue(richCEPCidade); 		            
				            rowCountDireito++;	
				            rowCountDireito++;	
				            rowCountDireito++;	
				            rowCountDireito++;	
			            }
					}	          
		            
		            
		            HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();  
		            res.setContentType("application/vnd.ms-excel");  
		            res.setHeader("Content-disposition",  "attachment; filename=MalaDiretaSegurados.xls");  
		 
		            
		           try {  
		                 ServletOutputStream out = res.getOutputStream();  
		                 wb.write(out);  
		                 out.flush();  
		                 out.close();  
		           } catch (IOException ex) {   
		                   ex.printStackTrace();  
		           }  
		           
		           FacesContext faces = FacesContext.getCurrentInstance();  
		           faces.responseComplete();  
	  
	            }else{
	            	addErrorMessage("Selecione um ou mais Segurado(s) para Gera��o do Arquivo de Impress�o." );
	            }
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        
	        return "export";
			
		}
		
		/**
		 * Popula um combo com valores dos tipos de org�o
		 * @return
		 */
		public List<SelectItem> getTipoOrgaoCombo(){
			
			List<SelectItem> retorno = new ArrayList<SelectItem>();
			retorno.add(new SelectItem("", "Selecione:"));
			retorno.add( new SelectItem( "E", "Estadual" ) );
			retorno.add( new SelectItem( "F", "Federal" ) );
			retorno.add( new SelectItem( "M", "Municipal" ) );
			retorno.add( new SelectItem( "P", "Privado" ) );
			
			return retorno;
		}
		
		public String ultimoPagamento(Proposta proposta) {
			Orgao orgao = proposta.getOrgao();
			Session session = HibernateUtil.currentSession();			
			OrgaoQuitacaoDAO orgaoQuitacaoDAO = new OrgaoQuitacaoDAO(session);
			OrgaoQuitacao orgaoQuitacao = orgaoQuitacaoDAO.getUltimoPagamento(orgao);
			
			if(orgaoQuitacao == null || proposta.getSituacao() == SituacaoProposta.CANCELADA) {
				return "-";
			} else {
				return orgaoQuitacao.getMesAno();
			}
		}
		
		public List<MovimentacaoPagamento> getHistoricoMovimentacoes() {
			
			Session session = HibernateUtil.currentSession();
			MovimentacaoPropostaInadimplenteDAO dao = new MovimentacaoPropostaInadimplenteDAO(session);
			historicoMovimentacoes = dao.buscarHistoricoMovimentacoes(proposta.getId());
			return historicoMovimentacoes;
			
		}
		
		
		
		
		//==============================================================================Getters and Setters
		
		public void setHistoricoMovimentacoes(
				List<MovimentacaoPagamento> historicoMovimentacoes) {
			this.historicoMovimentacoes = historicoMovimentacoes;
		}

		public Proposta getPropostaFiltro() {
			return propostaFiltro;
		}

		public void setPropostaFiltro(Proposta propostaFiltro) {
			this.propostaFiltro = propostaFiltro;
		}

		public List<Proposta> getResultadoPesquisa() {
			return resultadoPesquisa;
		}

		public void setResultadoPesquisa(List<Proposta> resultadoPesquisa) {
			this.resultadoPesquisa = resultadoPesquisa;
		}

		public HtmlSelectOneMenu getModeloPropostaSelecionado() {
			return modeloPropostaSelecionado;
		}

		public void setModeloPropostaSelecionado(
				HtmlSelectOneMenu modeloPropostaSelecionado) {
			this.modeloPropostaSelecionado = modeloPropostaSelecionado;
		}

		public HtmlSelectOneMenu getTipoPropostaSelecionado() {
			return tipoPropostaSelecionado;
		}

		public void setTipoPropostaSelecionado(HtmlSelectOneMenu tipoPropostaSelecionado) {
			this.tipoPropostaSelecionado = tipoPropostaSelecionado;
		}


		public Proposta getProposta() {
			return proposta;
		}

		public void setProposta(Proposta proposta) {
			this.proposta = proposta;
		}

		
		public List<SelectItem> getTipoPropostaComboFiltro() {
			return tipoPropostaComboFiltro;
		}

		public void setTipoPropostaComboFiltro(List<SelectItem> tipoPropostaComboFiltro) {
			this.tipoPropostaComboFiltro = tipoPropostaComboFiltro;
		}

		public Integer getNumeroPropostaIncluir() {
			return numeroPropostaIncluir;
		}

		public void setNumeroPropostaIncluir(Integer numeroPropostaIncluir) {
			this.numeroPropostaIncluir = numeroPropostaIncluir;
		}
		

		public Integer getNumeroPropostaCopiar() {
			return numeroPropostaCopiar;
		}

		public void setNumeroPropostaCopiar(Integer numeroPropostaCopiar) {
			this.numeroPropostaCopiar = numeroPropostaCopiar;
		}	
		
		public Boolean getFluxoInclusao() {
			return fluxoInclusao;
		}

		public void setFluxoInclusao(Boolean fluxoInclusao) {
			this.fluxoInclusao = fluxoInclusao;
		}
		
		public Beneficiario getBeneficiario() {
			return beneficiario;
		}

		public void setBeneficiario(Beneficiario beneficiario) {
			this.beneficiario = beneficiario;
		}
		
		public List<Beneficiario> getBeneficiariosExcluidos() {
			return beneficiariosExcluidos;
		}

		public void setBeneficiariosExcluidos(List<Beneficiario> beneficiariosExcluidos) {
			this.beneficiariosExcluidos = beneficiariosExcluidos;
		}

		public List<SelectItem> getOpcoesAgenciadorCombo() {
			return opcoesAgenciadorCombo;
		}

		public void setOpcoesAgenciadorCombo(List<SelectItem> opcoesAgenciadorCombo) {
			this.opcoesAgenciadorCombo = opcoesAgenciadorCombo;
		}
		
		public PropostaAgenciador getAgenciador() {
			return agenciador;
		}

		public void setAgenciador(PropostaAgenciador agenciador) {
			this.agenciador = agenciador;
		}

		public List<PropostaAgenciador> getAgenciadoresExcluidos() {
			return agenciadoresExcluidos;
		}

		public void setAgenciadoresExcluidos(
				List<PropostaAgenciador> agenciadoresExcluidos) {
			this.agenciadoresExcluidos = agenciadoresExcluidos;
		}
		
		public String getIdadeSegurado() {
			return idadeSegurado;
		}

		public void setIdadeSegurado(String idadeSegurado) {
			this.idadeSegurado = idadeSegurado;
		}

		public String getIdadeConjuge() {
			return idadeConjuge;
		}

		public void setIdadeConjuge(String idadeConjuge) {
			this.idadeConjuge = idadeConjuge;
		}
		
		public List<SelectItem> getOpcoesMotivoCancelamento() {
			return opcoesMotivoCancelamento;
		}

		public void setOpcoesMotivoCancelamento(
				List<SelectItem> opcoesMotivoCancelamento) {
			this.opcoesMotivoCancelamento = opcoesMotivoCancelamento;
		}


		public Integer getTipoRelatorio() {
			return tipoRelatorio;
		}


		public void setTipoRelatorio(Integer tipoRelatorio) {
			this.tipoRelatorio = tipoRelatorio;
		}


		public Double getValorCapitalSegurado() {
			return valorCapitalSegurado;
		}


		public void setValorCapitalSegurado(Double valorCapitalSegurado) {
			this.valorCapitalSegurado = valorCapitalSegurado;
		}


		public List<SelectItem> getOpcoesEmpresaCombo() {
			return opcoesEmpresaCombo;
		}


		public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
			this.opcoesEmpresaCombo = opcoesEmpresaCombo;
		}


		public Integer getNumeroPropostaParameter() {
			return numeroPropostaParameter;
		}


		public void setNumeroPropostaParameter(Integer numeroPropostaParameter) {
			this.numeroPropostaParameter = numeroPropostaParameter;
		}


		public Boolean getAlterarBeneficiario() {
			return alterarBeneficiario;
		}


		public void setAlterarBeneficiario(Boolean alterarBeneficiario) {
			this.alterarBeneficiario = alterarBeneficiario;
		}


		public Beneficiario getBeneficiarioTemp() {
			return beneficiarioTemp;
		}


		public void setBeneficiarioTemp(Beneficiario beneficiarioTemp) {
			this.beneficiarioTemp = beneficiarioTemp;
		}


		public Proposta[] getPropostasSelecionadas() {
			return propostasSelecionadas;
		}


		public void setPropostasSelecionadas(Proposta[] propostasSelecionadas) {
			this.propostasSelecionadas = propostasSelecionadas;
		}


		public Integer getTotalRegistrosRelatorio() {
			return totalRegistrosRelatorio;
		}


		public void setTotalRegistrosRelatorio(Integer totalRegistrosRelatorio) {
			this.totalRegistrosRelatorio = totalRegistrosRelatorio;
		}


		public Double getValorTotalRegistrosRelatorio() {
			return valorTotalRegistrosRelatorio;
		}


		public void setValorTotalRegistrosRelatorio(Double valorTotalRegistrosRelatorio) {
			this.valorTotalRegistrosRelatorio = valorTotalRegistrosRelatorio;
		}


		public Integer getTotalRegistrosCancelados() {
			return totalRegistrosCancelados;
		}


		public void setTotalRegistrosCancelados(Integer totalRegistrosCancelados) {
			this.totalRegistrosCancelados = totalRegistrosCancelados;
		}


		public Integer getTotalRegistrosAtivos() {
			return totalRegistrosAtivos;
		}


		public void setTotalRegistrosAtivos(Integer totalRegistrosAtivos) {
			this.totalRegistrosAtivos = totalRegistrosAtivos;
		}


		public Integer getTotalRegistrosSemAverbacao() {
			return totalRegistrosSemAverbacao;
		}


		public void setTotalRegistrosSemAverbacao(Integer totalRegistrosSemAverbacao) {
			this.totalRegistrosSemAverbacao = totalRegistrosSemAverbacao;
		}


		public Double getValorTotalPremioAtivos() {
			return valorTotalPremioAtivos;
		}


		public void setValorTotalPremioAtivos(Double valorTotalPremioAtivos) {
			this.valorTotalPremioAtivos = valorTotalPremioAtivos;
		}


		public HtmlSelectOneMenu getTipoOrgaoSelecionado() {
			return tipoOrgaoSelecionado;
		}

		public void setTipoOrgaoSelecionado(HtmlSelectOneMenu tipoOrgaoSelecionado) {
			this.tipoOrgaoSelecionado = tipoOrgaoSelecionado;
		}


		public String getNumeroSorteio() {
			return numeroSorteio;
		}


		public void setNumeroSorteio(String numeroSorteio) {
			this.numeroSorteio = numeroSorteio;
		}


		public String getTipoComissao() {
			return tipoComissao;
		}


		public void setTipoComissao(String tipoComissao) {
			this.tipoComissao = tipoComissao;
		}
		

		public List<SelectItem> getOpcoesTipoComissao() {
			//Realiza as a��es de banco: Busca e seta os valores
			opcoesTipoComissao = new ArrayList<SelectItem>();
			opcoesTipoComissao.add( new SelectItem( null, "Selecione:" ) );
			opcoesTipoComissao.add( new SelectItem( "0", "Inclus\u00E3o"));
			opcoesTipoComissao.add( new SelectItem( "1", "Altera\u00E7\u00E3o"));
			opcoesTipoComissao.add( new SelectItem( "2", "Reangaria\u00E7\u00E3o"));
			
			return opcoesTipoComissao;
		}


		public void setOpcoesTipoComissao(List<SelectItem> opcoesTipoComissao) {
			this.opcoesTipoComissao = opcoesTipoComissao;
		}


		public Double getFixoCapitalFuturo() {
			return fixoCapitalFuturo;
		}


		public void setFixoCapitalFuturo(Double fixoCapitalFuturo) {
			this.fixoCapitalFuturo = fixoCapitalFuturo;
		}


		public Double getFixoPremioFuturo() {
			return fixoPremioFuturo;
		}


		public void setFixoPremioFuturo(Double fixoPremioFuturo) {
			this.fixoPremioFuturo = fixoPremioFuturo;
		}


		public Double getFixoCapitalConjugeFuturo() {
			return fixoCapitalConjugeFuturo;
		}


		public void setFixoCapitalConjugeFuturo(Double fixoCapitalConjugeFuturo) {
			this.fixoCapitalConjugeFuturo = fixoCapitalConjugeFuturo;
		}


		public Double getFixoPremioConjugeFuturo() {
			return fixoPremioConjugeFuturo;
		}


		public void setFixoPremioConjugeFuturo(Double fixoPremioConjugeFuturo) {
			this.fixoPremioConjugeFuturo = fixoPremioConjugeFuturo;
		}
		

		
	//===============================================================================FIM GETTERS AND SETTERS		
		
		
}

