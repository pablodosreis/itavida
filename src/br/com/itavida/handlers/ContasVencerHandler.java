package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.hibernate.Session;

import br.com.itavida.dao.ContasVencerDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.entidades.ContasVencer;
import br.com.itavida.entidades.Despesa;
import br.com.itavida.entidades.Filial;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.GrupoDespesa;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class ContasVencerHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 2L;
	private ContasVencer contasVencer;
	private ContasVencer contasVencerFiltro;
	private List<ContasVencer> resultadoPesquisa;
	

	

	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public ContasVencerHandler(){
		String fluxo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fluxo");
		if( fluxo!= null && fluxo.equals( "incluir" ) ){
			preparaIncluir();
		}else if( fluxo !=null && fluxo.equals( "consulta" ) ){
			cancelar( null );
		}
	}
	
	
	// Fluxos de INCLUS�O / EXCLUS�O / ALTERA��O
	
	public String preparaIncluir( ){
		cancelar(null);
		getContasVencer().setContaPaga("N");
		
		
		
		
		return "incluirContasVencer";
	}
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getReplicas(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add( new SelectItem( 1, "01" ) );
		retorno.add( new SelectItem( 2, "02" ) );
		retorno.add( new SelectItem( 3, "03" ) );
		retorno.add( new SelectItem( 4, "04" ) );
		retorno.add( new SelectItem( 5, "05" ) );
		retorno.add( new SelectItem( 6, "06" ) );
		retorno.add( new SelectItem( 7, "07" ) );
		retorno.add( new SelectItem( 8, "08" ) );
		retorno.add( new SelectItem( 9, "09" ) );
		retorno.add( new SelectItem( 10, "10" ) );
		retorno.add( new SelectItem( 11, "11" ) );
		retorno.add( new SelectItem( 12, "12" ) );
		
		return retorno;
	}	
	
	public String cancelarInclusao( ){
		cancelar(null);
		return "cancelarInclusao";
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaContasVencers(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		ContasVencerDAO dao = new ContasVencerDAO(session);
		
		ContasVencer contasVencerFiltro = getContasVencerFiltro();

		List<ContasVencer> lista = dao.busca( contasVencerFiltro );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao(getContasVencer().getDespesa().getId(), "Despesa")){
			Dao<ContasVencer, Integer> dao = new Dao<ContasVencer, Integer>(session, ContasVencer.class);
			ContasVencer contasVencerGravar = getContasVencer();
			
			for (int i = 0; i < getContasVencer().getReplicas(); i++) {
			
				if( contasVencerGravar.getFuncionario() != null && ( contasVencerGravar.getFuncionario().getId() == null || contasVencerGravar.getFuncionario().getId() == 0 ) ){
					contasVencerGravar.setFuncionario( null );
				}
				
				dao.merge( contasVencerGravar );
				
				Calendar calendario = new GregorianCalendar( );
				calendario.setTime( contasVencerGravar.getDataVencimento()  );
				calendario.add( Calendar.MONTH , 1 );
				
				contasVencerGravar.setDataVencimento( calendario.getTime() );
			} 
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
			getContasVencer().setContaPaga("N");
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtFilial", "Filial");
		mapaCampos.put("txtDespesa", "Despesa");
		mapaCampos.put("txtGrupoDespesa", "Grupo Despesa");		
		mapaCampos.put("txtValorDespesa", "Valor da Despesa");
		mapaCampos.put("dtDataVencimento", "Data do Vencimento");
		mapaCampos.put("cmbContaPaga", "Conta Paga");
		
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarContasVencer( ){
		if( getContasVencer().getFuncionario() == null ){
			getContasVencer().setFuncionario( new Funcionario() );
		}
		return "incluirContasVencer";
	}
	
	public void pagarConta( ActionEvent e ){
		Session session = HibernateUtil.currentSession();
		Dao<ContasVencer, Integer> dao = new Dao<ContasVencer, Integer>(session, ContasVencer.class);
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("pagarContasVencer");
		ContasVencer contasVencerPagar = ( ContasVencer ) param.getValue();

		contasVencerPagar.setContaPaga( Constantes.SIM );
		dao.merge( contasVencerPagar );
	}
	
	/**
	 * Realiza a exclus�o de uma ContasVencer
	 * @param e
	 */
	public void excluirContasVencer(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirContasVencer");
		ContasVencer contasVencerDeletada = ( ContasVencer ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<ContasVencer, Integer> dao = new Dao<ContasVencer, Integer>(session, ContasVencer.class);
		dao.delete(contasVencerDeletada);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaContasVencers(e);
	}	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setContasVencer( new ContasVencer() );
		getContasVencer().setDespesa( new Despesa() );
		getContasVencer().setFilial( new Filial() );
		getContasVencer().setGrupoDespesa( new GrupoDespesa() );
		getContasVencer().setFuncionario( new Funcionario() );
		
		setContasVencerFiltro( new ContasVencer() );
		getContasVencerFiltro().setDespesa( new Despesa() );
		getContasVencerFiltro().setFilial( new Filial() );
		getContasVencerFiltro().setGrupoDespesa( new GrupoDespesa() );
		getContasVencerFiltro().setFuncionario( new Funcionario() );
		
		this.resultadoPesquisa = null;
		
	}

	
	// Getters and Setters


	public ContasVencer getContasVencer() {
		return contasVencer;
	}	

	public void setContasVencer(ContasVencer contasVencer) {
		this.contasVencer = contasVencer;
	}
	public List<ContasVencer> getResultadoPesquisa() {
		return resultadoPesquisa;
	}
	public void setResultadoPesquisa(List<ContasVencer> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}


	public ContasVencer getContasVencerFiltro() {
		return contasVencerFiltro;
	}


	public void setContasVencerFiltro(ContasVencer contasVencerFiltro) {
		this.contasVencerFiltro = contasVencerFiltro;
	}
	
	
	// Fim Getters and Setters
	
}
