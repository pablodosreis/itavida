package br.com.itavida.handlers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.MovimentacaoFinanceiraDAO;
import br.com.itavida.dto.LinhaMovimentacao;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.MovimentacaoFinanceira;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.PropostaAgenciador;
import br.com.itavida.enums.RegiaoSegurado;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.Contexto;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;
import br.com.itavida.util.NumberUtil;

public class RelatorioSuperApHandler extends ExcelGenerateHandler {
	
	private static final long serialVersionUID = 1L;
	
	private MovimentacaoFinanceiraDAO movimentacaoDAO = Contexto.getMovimentacaoFinanceiraDAO();
	
	private Map<String, List<MovimentacaoFinanceira>> mapaMovimentacao;
	
	private Set<LinhaMovimentacao> linhas;

	private Integer empresaSelected;
	
	private Integer ufSelected;
	
	private Integer situacaoPropostaSelected;
	
	private Integer modeloPropostaSelected;
	
	private List<String> situacaoPropostaSelectedList = new ArrayList<String>();
	
	private String profissao;
	
	private Integer mesAniversario;
	
	private Date dataAverbacao;
	
	private Date inicio;
	
	private Date fim;
	
	private List<SelectItem> opcoesUfCombo;
	
	private List<SelectItem> opcoesSituacaoPropostaCombo;
	
	private Proposta proposta;
	
	private int maximoMovimentacoes;
	
	
	public RelatorioSuperApHandler() {
		limparDados();
	}
	
	@PostConstruct
	public void init() {
		//limparDados();
	}

	private void limparDados() {
		empresaSelected = null;
		ufSelected = null;
		situacaoPropostaSelected = -1;
		dataAverbacao = null;
		populaOpcoesSituacaoPropostaCombo();
		populaOpcoesMesesCombo(true);
		setProposta(new Proposta());
	}
	
	public void gerarRelatorio() {
		
		if (inicio == null) {
			addWarnMessage( "O campo inicio deve ser informado" );
		}
		
		if (fim == null) {
			addWarnMessage( "O campo fim deve ser informado" );			
		}
		
		if (inicio == null || fim == null) {
			return;
		}
		
		this.mapaMovimentacao = getMapMovimentacao();
		if(this.mapaMovimentacao == null || this.mapaMovimentacao.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		super.gerarRelatorio(this.mapaMovimentacao.keySet(), 3, "MovimentacaoSuperaAp.xls");
	}
	

	public Map<String, List<MovimentacaoFinanceira>> getMapMovimentacao() {
		List<SituacaoProposta> lista = new ArrayList<SituacaoProposta>();
		for (String s : situacaoPropostaSelectedList) {
			lista.add(SituacaoProposta.get(Integer.valueOf(s)));
		}
		Integer idProposta = proposta != null ? proposta.getId() : null;
		return movimentacaoDAO.buscarMovimentacoesPorSituacao(inicio, fim, null, idProposta, lista);
	}

	public LinhaMovimentacao getLinhaMovimentacao(LinhaMovimentacao ref, Set<LinhaMovimentacao> lista) {
		for (LinhaMovimentacao linhaMovimentacao : lista) {
			if (ref.equals(linhaMovimentacao)) {
				return linhaMovimentacao;
			}
		}
		return null;
	}
	
	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		
		List<MovimentacaoFinanceira> movimentacoes = mapaMovimentacao.get(aba);
		linhas = new HashSet<LinhaMovimentacao>();
		maximoMovimentacoes = 0;
		for (MovimentacaoFinanceira mov : movimentacoes) {
			
			LinhaMovimentacao novaLinha = new LinhaMovimentacao(mov.getProposta(), mov.getNumeroParcela(),mov.getDataPagamentoComissao(), new ArrayList<MovimentacaoFinanceira>());
			LinhaMovimentacao linha = getLinhaMovimentacao(novaLinha, linhas);
			
			if (linha == null) {
				linha = novaLinha;
			}
			
			linha.getMovimentacoes().add(mov);
			linhas.add(linha);
			
			if (linha.getMovimentacoes().size() > maximoMovimentacoes) {
				maximoMovimentacoes = linha.getMovimentacoes().size();
			}
		}
		
		
		int indiceLinha = 0;
		//Cabe�alho
		addRow(aba);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Controle Super AP", indiceLinha, 1, 0, 8);
		addRow(aba);
		indiceLinha++;
		
		/*Descri��o*/
		addRow(aba);
		indiceLinha++;
		addTextoMesclado(aba, getStiloTextoTitulo(), "Propostas", indiceLinha, 1, 0, 2);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Produtividade", indiceLinha, 1, 3, 4	);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Implanta��o/Comiss�o", indiceLinha, 1, 5, 6	);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Informa��es do Plano", indiceLinha, 1, 7, 9	);
		
		int colunaProximaColuna = 10;
		
		for (int i =0; i < maximoMovimentacoes; i++) {
			addTextoMesclado(aba, getStiloTextoCabecalho(), "Movimenta��o", indiceLinha, 1, colunaProximaColuna, colunaProximaColuna+3);
		}
		
		/*Calendar ponteiro = Calendar.getInstance();
		ponteiro.setTime(inicio);
		Calendar fim = Calendar.getInstance();
		fim.setTime(this.fim);
		while (ponteiro.getTimeInMillis() <= fim.getTimeInMillis()) {
			String referencia = DateUtil.getFormattedDate(ponteiro.getTime(), "MM/yyyy");
			addTextoMesclado(aba, getStiloTextoCabecalho(), "REF." + referencia, indiceLinha, 1, colunaProximaColuna, colunaProximaColuna+3);
			colunaProximaColuna = colunaProximaColuna+4;
			ponteiro.add(Calendar.MONTH, 1);
		}*/
		
		addRow(aba);
		indiceLinha++;
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Agenciador");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Segurado");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "CPF");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Valor venda");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "PROT DIG ICATU");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Com. Agenciador");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Averba��o");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Plano");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Vencimento");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Forma de Pgto");
		
		for (int i =0; i < maximoMovimentacoes; i++) {
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Parcela");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Pgto");		
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Dt. Comiss�o");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Vr. Comiss�o");
		}
		/*ponteiro.setTime(inicio);
		while (ponteiro.getTimeInMillis() <= fim.getTimeInMillis()) {
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Parcela");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Pgto");		
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Dt. Comiss�o");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Vr. Comiss�o");
			ponteiro.add(Calendar.MONTH, 1);
		}*/
	}

	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		
		/*Map<String, MovimentacaoFinanceira> mapa = new HashMap<String, MovimentacaoFinanceira>();
		for (LinhaMovimentacao linhaMov : linhas) {
			for (MovimentacaoFinanceira movimentacaoFinanceira : linhaMov.getMovimentacoes() ) {
				if (movimentacaoFinanceira.getDataPagamentoComissao() != null) {
					String dataAnoMes = DateUtil.getFormattedDate(movimentacaoFinanceira.getDataPagamentoComissao(), "MM-yyyy");
					String chave = movimentacaoFinanceira.getProposta().getId() + "-" + linhaMov.getParcela() + "-"  + dataAnoMes;
					mapa.put(chave, movimentacaoFinanceira);
				}
			}
		}	*/	
		
		ArrayList<LinhaMovimentacao> listaMovimentacoes = new ArrayList<LinhaMovimentacao>(linhas);
		Collections.sort(listaMovimentacoes);
		for (LinhaMovimentacao linhaMov : listaMovimentacoes) { 
			Proposta proposta = linhaMov.getProposta();
       		
       		addRow(aba);
       	
       		
       		Double valorComissao;
       		String plano;
       		if (proposta.getSuperAp().getPeriodicidade().equals("0")) {
       			valorComissao = NumberUtil.round(proposta.getValorPremioSegurado()/2, 2);
       			plano = "MENSAL";
       		} else {
       			valorComissao = NumberUtil.round(proposta.getValorPremioSegurado()/10, 2);
       			plano = "ANUAL";
       		}
       		
       		String agenciador = null;
       		List<PropostaAgenciador> listaAgenciadores = new ArrayList<PropostaAgenciador>(proposta.getPropostaagenciador_collection());
			if (listaAgenciadores != null && !listaAgenciadores.isEmpty()) {
       			agenciador = listaAgenciadores.get(0).getFuncionario().getNome();
       		}
       			
       		addCelulaTexto(aba, getStiloTextoSimples(), agenciador, "-");
       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getNomeSegurado());
       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getCpfSegurado());
       		addCelulaNumero(aba, getStiloMonetarioSimples(), proposta.getValorPremioSegurado(), "-");
       		addCelulaData(aba, getStiloDataSimples(), proposta.getDataCadastro(), "-");
       		
       		addCelulaNumero(aba, getStiloMonetarioSimples(), valorComissao, "");
       		addCelulaData(aba, getStiloTextoSimples(), proposta.getDataAprovacao(), "-");
       		
       		

       		addCelulaTexto(aba, getStiloTextoSimples(), plano, "-");
       		addCelulaNumero(aba, getStiloTextoSimples(), proposta.getSuperAp().getDiaVencimento().doubleValue(), "-");
       		String formaPagamento = proposta.getFormaPagamento() != null && proposta.getFormaPagamento().getTipo() != null ? proposta.getFormaPagamento().getTipo().getNome() : null;
			addCelulaTexto(aba, getStiloTextoSimples(), formaPagamento, "-");
       		
			Calendar ponteiro = Calendar.getInstance();
			ponteiro.setTime(inicio);
			Calendar fim = Calendar.getInstance();
			fim.setTime(this.fim);
			
			for (MovimentacaoFinanceira mov : linhaMov.getMovimentacoes()) {
				String parcelas = mov.getNumeroParcela().toString();
				String dtPagamentos = mov.getDataPagamentoFormatada();
				String dtPagamentosComissao = mov.getDataComissaoFormatada();
				Double vrTotalComissao = mov.getValorComissao();
				addCelulaTexto(aba, getStiloTextoSimples(), parcelas, "");  //parcela
				addCelulaTexto(aba, getStiloTextoSimples(), dtPagamentos, ""); // data do pagamento
				addCelulaTexto(aba, getStiloTextoSimples(), dtPagamentosComissao, ""); //cr�dito comiss�o itavida
	       		addCelulaNumero(aba, getStiloMonetarioSimples(),vrTotalComissao , "");
			}
			
			/*while (ponteiro.getTimeInMillis() <= fim.getTimeInMillis()) {
				String parcelas = "";
				String dtPagamentos = "";
				String dtPagamentosComissao = "";
				Double vrTotalComissao = 0.0;
				
				String dataAnoMes = DateUtil.getFormattedDate(ponteiro.getTime(), "MM-yyyy");
				List<MovimentacaoFinanceira> movs = linhaMov.getMovimentacoes(dataAnoMes);
				for (Iterator iterator = movs.iterator(); iterator.hasNext();) {
					MovimentacaoFinanceira movFin2 = (MovimentacaoFinanceira) iterator.next();
					parcelas += movFin2.getNumeroParcela();
					dtPagamentos += movFin2.getDataPagamentoFormatada();
					dtPagamentosComissao += movFin2.getDataComissaoFormatada();
					vrTotalComissao += movFin2.getValorComissao();
					
					
					if (iterator.hasNext()) {
						parcelas += ",";
						dtPagamentos += ",";
						dtPagamentosComissao += ",";
					}
				}
				
				addCelulaTexto(aba, getStiloTextoSimples(), parcelas, "");  //parcela
				addCelulaTexto(aba, getStiloTextoSimples(), dtPagamentos, ""); // data do pagamento
				addCelulaTexto(aba, getStiloTextoSimples(), dtPagamentosComissao, ""); //cr�dito comiss�o itavida
	       		addCelulaNumero(aba, getStiloMonetarioSimples(),vrTotalComissao , "");
	       		
	       		
				ponteiro.add(Calendar.MONTH, 1);
			}*/
	       		
       	}
	}

	

	
	/**
	 * Popula um combo de situa��es da proposta.
	 *  
	 */
	public void populaOpcoesSituacaoPropostaCombo(){
		
		opcoesSituacaoPropostaCombo = new ArrayList<SelectItem>();
		for(SituacaoProposta situacao : SituacaoProposta.values()) {
			opcoesSituacaoPropostaCombo.add( new SelectItem( situacao.getId(), situacao.getNome()));
		}
		
	}
	
	
	/**
	 * Popula um combo de UF
	 */
	public void populaOpcoesUfCombo() {
		
		Session session = HibernateUtil.currentSession();
		CidadeDAO cidadeDAO = new CidadeDAO(session);
		
		List<Estado> estados = cidadeDAO.obterTodosEstados();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (Estado estado : estados) {
			opcoesUfCombo.add( new SelectItem( estado.getCod_estado(), estado.getNom_estado()) );
		}
		
	}
	
	/**
	 * Popula um combo com os tipos de UF existentes no sistema.
	 */
	public void populaOpcoesRegiaoSeguradoCombo() {
		//Busca os valores de regi�o do enum RegiaoSegurado
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (RegiaoSegurado regiao : RegiaoSegurado.values()) {
			opcoesUfCombo.add( new SelectItem( regiao.getId(), regiao.getNome()));
		}
	}
	
	public void cancelar(ActionEvent e) {
		limparDados();
	}
	
	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public Integer getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Integer empresaSelected) {
		this.empresaSelected = empresaSelected;
	}

	public Date getDataAverbacao() {
		return dataAverbacao;
	}

	public void setDataAverbacao(Date dataAverbacao) {
		this.dataAverbacao = dataAverbacao;
	}

	
	public Integer getSituacaoPropostaSelected() {
		return situacaoPropostaSelected;
	}

	public void setSituacaoPropostaSelected(Integer tipoConsultaSelected) {
		this.situacaoPropostaSelected = tipoConsultaSelected;
	}

	public Integer getModeloPropostaSelected() {
		return modeloPropostaSelected;
	}

	public void setModeloPropostaSelected(Integer modeloPropostaSelected) {
		this.modeloPropostaSelected = modeloPropostaSelected;
	}


	
	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public List<SelectItem> getOpcoesUfCombo() {
		return opcoesUfCombo;
	}

	public void setOpcoesUfCombo(List<SelectItem> opcoesUfCombo) {
		this.opcoesUfCombo = opcoesUfCombo;
	}

	public Integer getUfSelected() {
		return ufSelected;
	}

	public void setUfSelected(Integer ufSelected) {
		this.ufSelected = ufSelected;
	}

	public List<SelectItem> getOpcoesSituacaoPropostaCombo() {
		return opcoesSituacaoPropostaCombo;
	}

	public void setOpcoesSituacaoPropostaCombo(List<SelectItem> opcoesTipoConsultaCombo) {
		this.opcoesSituacaoPropostaCombo = opcoesTipoConsultaCombo;
	}

	public List<String> getSituacaoPropostaSelectedList() {
		return situacaoPropostaSelectedList;
	}

	public void setSituacaoPropostaSelectedList(
			List<String> situacaoPropostaSelectedList) {
		this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public Integer getMesAniversario() {
		return mesAniversario;
	}

	public void setMesAniversario(Integer mesAniversario) {
		this.mesAniversario = mesAniversario;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}


}
