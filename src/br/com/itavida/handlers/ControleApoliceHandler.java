package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.hibernate.Session;

import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;

public class ControleApoliceHandler implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private boolean exibirAlerta = true;
	
	public Usuario getUsuario() {
		UsuarioHandler usuarioHandler = (UsuarioHandler) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioHandler");
		Usuario usuario = usuarioHandler.getUsuario();
		return usuario;
	}
	
	public boolean isExibirAlertaAniversarioApolice () {
		Usuario usuario = getUsuario();
		return getMensagemAlertaAniversarioApolice().length() > 30 && exibirAlerta && usuario.getPerfil().equals("ADM");
	}
	
	public String getMensagemAlertaAniversarioApolice() {
		
		StringBuilder texto = new StringBuilder();
		texto.append("O anivers�rio da ap�lice ");
		
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO = new EmpresaDAO(session);
		boolean possuiApolice = false;
		for (Empresa empresa : empresaDAO.buscaEmpresas()) {
			if(empresa.isAjusteAtivo()) {
				int dias = DateUtil.diffEmDias(empresa.getProximaAtualizacao(), new Date());
				if(dias <= 3) {
					texto.append("\"");
					texto.append(empresa.getNumApolice());
					texto.append("\"");
					texto.append(" ");
					possuiApolice = true;
				}
			}
		}
		
		if(possuiApolice) {
			texto.append("est� pr�ximo. Atualize os valores de ajuste na tela ");
		}
		
		return texto.toString();
	}
}
