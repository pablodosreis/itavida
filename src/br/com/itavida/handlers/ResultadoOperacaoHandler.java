package br.com.itavida.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.NovosValoresDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dto.LinhaMensagem;
import br.com.itavida.entidades.ComportamentoPropostaEnum;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.ModeloProposta;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.TipoProposta;
import br.com.itavida.exceptions.ImportacaoExcelException;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Carteiro;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.File;
import br.com.itavida.util.HibernateUtil;

@SuppressWarnings("serial")
public class ResultadoOperacaoHandler extends MainHandler implements Serializable {

	
}
