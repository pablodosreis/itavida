package br.com.itavida.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dto.RelatorioOrgaoDTO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.enums.RegiaoSegurado;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class RelatorioOrgaoHandler extends ExcelGenerateHandler {
	
	private static final long serialVersionUID = 1L;
	
	private Integer ufSelected;
	
	private String nomeOrgao;
	
	private List<SelectItem> opcoesOrgaoCombo;
	
	private List<SelectItem> opcoesUfCombo;
	
	private Collection<RelatorioOrgaoDTO> lista;
	
	
	public RelatorioOrgaoHandler() {
		limparDados();
	}
	
	@PostConstruct
	public void init() {
		//limparDados();
	}

	private void limparDados() {
		ufSelected = null;
		nomeOrgao = "";
		populaOpcoesUfCombo();
	}
	
	public void gerarRelatorio() {
		OrgaoDAO orgaoDAO = new OrgaoDAO(HibernateUtil.currentSession());
		this.lista = orgaoDAO.getOrgaosDto(ufSelected, nomeOrgao);
		if(this.lista == null || this.lista.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		gerarRelatorio(2, "Relat�rio de �rg�os");
	}
	
	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		int indiceLinha = 0;
		addRow(aba);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Listagem de �rg�os", indiceLinha, 1, 0, 6);
		addRow(aba);
		indiceLinha++;
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","uf", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","orgao", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","qtd-vidas-pendentes", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","qtd-vidas-ativas", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","qtd-total-vidas", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","total-premios", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","qtd-vidas-inadimplentes", null));
		
	}

	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
       	for(RelatorioOrgaoDTO dto : this.lista) {
       		addRow(aba);
       		addCelulaTexto(aba, getStiloTextoSimples(), dto.getUf() , "-");
       		addCelulaTexto(aba, getStiloTextoSimples(), dto.getNome(), "-");
       		addCelulaNumeroInteiro(aba, getStiloTextoSimples(), dto.getVidasPendentes(), "-");
       		addCelulaNumeroInteiro(aba, getStiloTextoSimples(), dto.getVidasAtivas(), "-");
       		addCelulaNumeroInteiro(aba, getStiloTextoSimples(), dto.getTotalVidas(), "-");
       		addCelulaNumero(aba, getStiloTextoSimples(), dto.getTotalPremios(), "-");
       		addCelulaNumeroInteiro(aba, getStiloTextoSimples(), dto.getVidasInadimplentes(), "-");
       	}
	}

	/**
	 * Popula um combo de UF
	 */
	public void populaOpcoesUfCombo() {
		
		Session session = HibernateUtil.currentSession();
		CidadeDAO cidadeDAO = new CidadeDAO(session);
		
		List<Estado> estados = cidadeDAO.obterTodosEstados();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (Estado estado : estados) {
			opcoesUfCombo.add( new SelectItem( estado.getCod_estado(), estado.getNom_estado()) );
		}
		
	}

	public void cancelar(ActionEvent e) {
		limparDados();
	}
	
	public String navegar() {
		limparDados();
		return "resultado";
	}

	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public List<SelectItem> getOpcoesUfCombo() {
		return opcoesUfCombo;
	}

	public void setOpcoesUfCombo(List<SelectItem> opcoesUfCombo) {
		this.opcoesUfCombo = opcoesUfCombo;
	}

	public Integer getUfSelected() {
		return ufSelected;
	}

	public void setUfSelected(Integer ufSelected) {
		this.ufSelected = ufSelected;
	}

	public List<SelectItem> getOpcoesSituacaoPropostaCombo() {
		return opcoesSituacaoPropostaCombo;
	}

	public void setOpcoesSituacaoPropostaCombo(List<SelectItem> opcoesTipoConsultaCombo) {
		this.opcoesSituacaoPropostaCombo = opcoesTipoConsultaCombo;
	}


	public List<SelectItem> getOpcoesOrgaoCombo() {
		return opcoesOrgaoCombo;
	}

	public void setOpcoesOrgaoCombo(List<SelectItem> opcoesOrgaoCombo) {
		this.opcoesOrgaoCombo = opcoesOrgaoCombo;
	}

	/**
	 * @return the nomeOrgao
	 */
	public String getNomeOrgao() {
		return nomeOrgao;
	}

	/**
	 * @return the lista
	 */
	public Collection<RelatorioOrgaoDTO> getLista() {
		return lista;
	}

	/**
	 * @param nomeOrgao the nomeOrgao to set
	 */
	public void setNomeOrgao(String nomeOrgao) {
		this.nomeOrgao = nomeOrgao;
	}

	/**
	 * @param lista the lista to set
	 */
	public void setLista(Collection<RelatorioOrgaoDTO> lista) {
		this.lista = lista;
	}

	public List<String> getSituacaoPropostaSelectedList() {
		return situacaoPropostaSelectedList;
	}

	public void setSituacaoPropostaSelectedList(
			List<String> situacaoPropostaSelectedList) {
		this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
	}

}
