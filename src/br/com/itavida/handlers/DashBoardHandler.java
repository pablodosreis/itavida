package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.xhtmlrenderer.protocols.data.DataURLConnection;

import br.com.itavida.dao.ArquivoMensalDAO;
import br.com.itavida.dao.ContasVencerDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.HistoricoSinistroDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.ContasVencer;
import br.com.itavida.entidades.DashBoardDataHolder;
import br.com.itavida.entidades.GraphicEntity;
import br.com.itavida.entidades.HistoricoSinistro;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;

public class DashBoardHandler implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ContasVencer> listaContasVencidasOuAVencer;
	private DashboardModel model;
	private DashBoardDataHolder dataHolder;
	private List<GraphicEntity> listaGraficoSexo;
	private List<GraphicEntity> listaGraficoPlanos;
	private Date dataAtual;
	
	/*Filtros*/
	private Date inicio;
	private Date fim;
	private boolean media_idade;
	private boolean total_premio;
	private boolean total_capital;
	private boolean total_segurados;
	private boolean total_orgaos;
	private boolean total_sinistro;
	private boolean total_homens;
	private boolean total_mulheres;
	private boolean premio_segurado;
	private boolean premio_conjuge;
	private boolean valor_sinistros;
	
	
	
	
	public List<ContasVencer> getListaContasVencidasOuAVencer() {
		return listaContasVencidasOuAVencer;
	}
	public void setListaContasVencidasOuAVencer(
			List<ContasVencer> listaContasVencidasOuAVencer) {
		this.listaContasVencidasOuAVencer = listaContasVencidasOuAVencer;
	}


	public DashBoardHandler(){
		model = new DefaultDashboardModel(); 
		dataHolder = new DashBoardDataHolder();
		dataAtual = new Date();
		
		
        DashboardColumn column1 = new DefaultDashboardColumn();  
        DashboardColumn column2 = new DefaultDashboardColumn();  
        DashboardColumn column3 = new DefaultDashboardColumn();   
        
        column1.addWidget("propostas");  
        column1.addWidget("graficoSexo"); 
        
        column2.addWidget("orgaos");  
        column2.addWidget("graficoPlano"); 
        
        column3.addWidget("sinistros"); 
        
  
        model.addColumn(column1);  
        model.addColumn(column2);  
        model.addColumn(column3);
        
		buscarContasVencidasOuAVencer();
		obterDadosPropostas();
		obterDadosSinistros();
		obterDadosOrgaos();
		obterDadosGraficoSexo();
		obterDadosGraficoTiposPlanos();
		
		valor_sinistros = true;
		total_premio = true;
		
		
		atualizarDadosGrafico(null);
		
	}
	
	public void atualizarDadosGrafico(ActionEvent e) {
		
		if (inicio == null) {
			inicio = DateUtil.getMesesAnteriores(12);
		}
		
		if (fim == null) {
			fim = DateUtil.getUltimaDiaMes(new Date());
		}
		
		
		List<Object[]> list = getDadosGrafico();
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Object[] objects = (Object[]) iterator.next();
			builder.append("[");
			builder.append(StringUtils.join(objects, ","));
			builder.append("]");
			if (iterator.hasNext()) {
				builder.append(",");
			}
		}
		
		builder.append("]");
		
		dataHolder.setGraficoConteudo(builder.toString());
	}
	
	public List<Object[]> getDadosGrafico() {
		Session session = HibernateUtil.currentSession();
		ArquivoMensalDAO arquivoMensalDAO = new ArquivoMensalDAO(session);
		return arquivoMensalDAO.getDadosHistoricos(inicio, fim);
	}
	

	public void buscarContasVencidasOuAVencer( ){
		
		Session session = HibernateUtil.currentSession();
		ContasVencerDAO dao = new ContasVencerDAO( session );
		setListaContasVencidasOuAVencer( dao.buscaContasVencidasOuAVencer() );
		
	}
	
	private void obterDadosPropostas(){
		Session session = HibernateUtil.currentSession();
		Dao<Proposta, Integer> dao = new Dao<Proposta, Integer>( session, Proposta.class );
		PropostaDAO propostaDao = new PropostaDAO( session );
		
		dataHolder.setTotalPropostas(  dao.countList() );
		dataHolder.setTotalPropostasAtivas( propostaDao.countPropostasAtivas() );
		dataHolder.setTotalPropostasCanceladas( propostaDao.countPropostasCanceladas() );
		dataHolder.setTotalPropostasSemAverbacao( propostaDao.countPropostasSemAverbacao() );
		
	}
	
	private void obterDadosSinistros(){
		Session session = HibernateUtil.currentSession();
		Dao<HistoricoSinistro, Integer> dao = new Dao<HistoricoSinistro, Integer>( session, HistoricoSinistro.class );
		HistoricoSinistroDAO historicoSinistroDAO = new HistoricoSinistroDAO();
		dataHolder.setTotalSinistros( dao.countList() );
		dataHolder.setTotalSinistros30dias( historicoSinistroDAO.countSinistrosNosUltimos30dias() );
	}
	
	private void obterDadosGraficoSexo(){
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDao = new PropostaDAO( session );
		
		listaGraficoSexo = new ArrayList<GraphicEntity>();
		GraphicEntity dash = new GraphicEntity();
		dash.setSerie("Masculino");
		dash.setValor( Double.valueOf( propostaDao.countPropostasPorSexo( "M" ) ) );
		listaGraficoSexo.add( dash );
		dash = new GraphicEntity();
		dash.setSerie("Feminino");
		dash.setValor( Double.valueOf( propostaDao.countPropostasPorSexo( "F" ) ) );
		listaGraficoSexo.add( dash );
	}
	
	private void obterDadosGraficoTiposPlanos() {
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDao = new PropostaDAO( session );
		listaGraficoPlanos = new ArrayList<GraphicEntity>();
		GraphicEntity dash = new GraphicEntity();
		dash.setSerie( "Plano Individual" );
		dash.setValor( Double.valueOf(  propostaDao.countPropostasPlanoIndividual() ) );
		listaGraficoPlanos.add(dash);
		dash = new GraphicEntity();
		dash.setSerie( "Plano Casado" );
		dash.setValor( Double.valueOf(  propostaDao.countPropostasPlanoConjugado() ) );	
		listaGraficoPlanos.add(dash);
	}
	
	private void obterDadosOrgaos(){
		Session session = HibernateUtil.currentSession();
		Dao<Orgao, Integer> dao = new Dao<Orgao, Integer>( session, Orgao.class );
		OrgaoDAO orgaoDAO = new OrgaoDAO( session );
		dataHolder.setTotalOrgaos( dao.countList() );
		dataHolder.setTotalOrgaosEstaduais( orgaoDAO.countOrgaosPorTipo( "E" ) );
		dataHolder.setTotalOrgaosMunicipais( orgaoDAO.countOrgaosPorTipo( "M" ) );
		dataHolder.setTotalOrgaosFederais( orgaoDAO.countOrgaosPorTipo( "F" ) );
		dataHolder.setTotalOrgaosPrivados( orgaoDAO.countOrgaosPorTipo( "P" ) );
	}
	
	public void handleReorder( DashboardReorderEvent event ) {  
        FacesMessage message = new FacesMessage();  
        message.setSeverity(FacesMessage.SEVERITY_INFO);  
        message.setSummary("DashBoard Reorganizado" );  
        message.setDetail("O DashBoard atual foi reorganizado com Sucesso!");  
          
        addMessage(message);  
    }  
      
	
    private void addMessage( FacesMessage message ) {  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }  
	
	public DashboardModel getModel() {
		return model;
	}
	public void setModel ( DashboardModel model ) {
		this.model = model;
	}
	public DashBoardDataHolder getDataHolder() {
		return dataHolder;
	}
	public void setDataHolder( DashBoardDataHolder dataHolder ) {
		this.dataHolder = dataHolder;
	}
	public List<GraphicEntity> getListaGraficoSexo() {
		return listaGraficoSexo;
	}
	public void setListaGraficoSexo(List<GraphicEntity> listaGraficoSexo) {
		this.listaGraficoSexo = listaGraficoSexo;
	}
	public Date getDataAtual() {
		return dataAtual;
	}
	public void setDataAtual(Date dataAtual) {
		this.dataAtual = dataAtual;
	}
	public List<GraphicEntity> getListaGraficoPlanos() {
		return listaGraficoPlanos;
	}
	public void setListaGraficoPlanos(List<GraphicEntity> listaGraficoPlanos) {
		this.listaGraficoPlanos = listaGraficoPlanos;
	}
	public boolean isPossuiPermissao() {
		/*if (usuarioRecuperado.getPerfil().equals("FIL")) {
			return false;
		}*/
		return true;
	}
	public Date getInicio() {
		return inicio;
	}
	public Date getFim() {
		return fim;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public void setFim(Date fim) {
		this.fim = fim;
	}
	public boolean isMedia_idade() {
		return media_idade;
	}
	public boolean isTotal_premio() {
		return total_premio;
	}
	public boolean isTotal_capital() {
		return total_capital;
	}
	public boolean isTotal_segurados() {
		return total_segurados;
	}
	public boolean isTotal_orgaos() {
		return total_orgaos;
	}
	public boolean isTotal_sinistro() {
		return total_sinistro;
	}
	public boolean isTotal_homens() {
		return total_homens;
	}
	public boolean isTotal_mulheres() {
		return total_mulheres;
	}
	public boolean isPremio_segurado() {
		return premio_segurado;
	}
	public boolean isPremio_conjuge() {
		return premio_conjuge;
	}
	public boolean isValor_sinistros() {
		return valor_sinistros;
	}
	public void setMedia_idade(boolean media_idade) {
		this.media_idade = media_idade;
	}
	public void setTotal_premio(boolean total_premio) {
		this.total_premio = total_premio;
	}
	public void setTotal_capital(boolean total_capital) {
		this.total_capital = total_capital;
	}
	public void setTotal_segurados(boolean total_segurados) {
		this.total_segurados = total_segurados;
	}
	public void setTotal_orgaos(boolean total_orgaos) {
		this.total_orgaos = total_orgaos;
	}
	public void setTotal_sinistro(boolean total_sinistro) {
		this.total_sinistro = total_sinistro;
	}
	public void setTotal_homens(boolean total_homens) {
		this.total_homens = total_homens;
	}
	public void setTotal_mulheres(boolean total_mulheres) {
		this.total_mulheres = total_mulheres;
	}
	public void setPremio_segurado(boolean premio_segurado) {
		this.premio_segurado = premio_segurado;
	}
	public void setPremio_conjuge(boolean premio_conjuge) {
		this.premio_conjuge = premio_conjuge;
	}
	public void setValor_sinistros(boolean valor_sinistros) {
		this.valor_sinistros = valor_sinistros;
	}
	
	
	
	
	
	
}
