package br.com.itavida.handlers;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.RegiaoSegurado;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.CpfCnpjUtils;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;
import br.com.itavida.util.NumberUtil;

public class SeguradoHandler extends ExcelGenerateHandler {
	
	private static final long serialVersionUID = 1L;
	
	private Map<String, List<Proposta>> mapaPropostas;

	private Integer empresaSelected;
	
	private Integer ufSelected;
	
	private Integer orgaoSelected;
	
	private String tipoOrgaoSelected;
	
	private Integer situacaoPropostaSelected;
	
	private Integer modeloPropostaSelected;
	
	private List<String> situacaoPropostaSelectedList = new ArrayList<String>();
	
	private String profissao;
	
	private Integer mesAniversario;
	
	private Date dataAverbacao;
	
	private Date dataCancelamentoInicio;
	
	private Date dataCancelamentoFim;
	
	private List<SelectItem> opcoesUfCombo;
	
	private List<SelectItem> opcoesEmpresaCombo;
	
	private List<SelectItem> opcoesOrgaoCombo;
	
	private List<SelectItem> opcoesTipoOrgaoCombo;
	
	private List<SelectItem> opcoesSituacaoPropostaCombo;
	
	private Funcionario agenciador;
	
	private int tipo = 0;
	
	public SeguradoHandler() {
		limparDados();
	}
	
	@PostConstruct
	public void init() {
		//limparDados();
	}

	private void limparDados() {
		empresaSelected = null;
		ufSelected = null;
		orgaoSelected = null;
		situacaoPropostaSelected = -1;
		dataAverbacao = null;
		
		populaOpcoesTipoOrgaoCombo();
		populaOpcoesOrgaoCombo();
		populaOpcoesRegiaoSeguradoCombo();
		populaOpcoesEmpresaCombo();
		populaOpcoesSituacaoPropostaCombo();
		populaOpcoesMesesCombo(true);
		setAgenciador(new Funcionario());
	}
	
	public void gerarRelatorio() {
		tipo = 0;
		this.mapaPropostas = getMapPropostas();
		if(this.mapaPropostas == null || this.mapaPropostas.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		super.gerarRelatorio(this.mapaPropostas.keySet(), 6);
	}
	
	public void gerarRelatorioSimplificado() {
		tipo = 1;
		this.mapaPropostas = getMapPropostas();
		if(this.mapaPropostas == null || this.mapaPropostas.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		super.gerarRelatorio(this.mapaPropostas.keySet(), 0);
	}
	
	public String limparSeparadorNumero(String texto) {
		
		texto = texto.replace("n�", "");
		texto = texto.replace(",n�", "");
		texto = texto.replace("N�", "");
		texto = texto.replace(",N�", "");
		texto = texto.replace("N", "");
		texto = texto.replace("n", "");
		texto = texto.replace(", N", "");
		texto = texto.replace(", n", "");
		texto = texto.replace(",", "");
		
		return texto.trim();
	}
	
	public void gerarRelacaoCorreios() {
		this.mapaPropostas = getMapPropostas();
		if(this.mapaPropostas == null || this.mapaPropostas.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		String separator = ";";
		
		
		try {

	        String filename = "RelacaoCorreios.csv";

	        FacesContext fc = FacesContext.getCurrentInstance();
	        HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();

	        response.reset();
	        response.setContentType("text/comma-separated-values");
	        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

	        OutputStream output = response.getOutputStream();

	        StringBuilder builder = new StringBuilder();
	        
	        for (List<Proposta> lista : this.mapaPropostas.values()) {
				for (Proposta p : lista) {
					
					String logradouro = "";
					String numero = StringUtils.isNotEmpty(p.getNumeroEnderecoSegurado()) ? p.getNumeroEnderecoSegurado() :  "";
					if (p.getEnderecoSegurado() != null) {
 						int indiceVirgula = 0;
 						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().toLowerCase().indexOf(",n") : indiceVirgula;
						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().toLowerCase().indexOf(", n") : indiceVirgula;
						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().toLowerCase().indexOf(", n�") : indiceVirgula;
 						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().toLowerCase().indexOf("n�") : indiceVirgula;
 						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().indexOf(",N�") : indiceVirgula;
 						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().indexOf(", N�") : indiceVirgula;
 						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().indexOf("N�") : indiceVirgula;
						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().lastIndexOf(" ") : indiceVirgula;
						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().lastIndexOf(",") : indiceVirgula;
						indiceVirgula = indiceVirgula <= 0 ?  p.getEnderecoSegurado().lastIndexOf(";") : indiceVirgula;
						
						
						if (indiceVirgula > 0) {
							numero = p.getEnderecoSegurado().substring(indiceVirgula);
							numero = numero.replace(".", "");
							numero = limparSeparadorNumero(numero);
							
							logradouro = p.getEnderecoSegurado().substring(0, indiceVirgula);
							logradouro = logradouro.trim();
							
							if (!NumberUtil.isInt(numero)) {
								numero = p.getNumeroEnderecoSegurado();
								logradouro = p.getEnderecoSegurado();
							} else {
								logradouro = limparSeparadorNumero(logradouro);
							}
							
							
							
						} else {
							logradouro = p.getEnderecoSegurado();
						}
					}
					
					String cepLimpo = "";
					if (p.getCep() != null) {
						cepLimpo = p.getCep().replace(".", "").replace("-", "").trim();
					}
					
					
					//CODIGO
					builder.append(p.getId().toString());
					builder.append(separator);
					//DESTINATARIO
					builder.append(p.getNomeSegurado());
					builder.append(separator);
					//ENDERECO - Ok o endere�o do segurado
					builder.append(logradouro);
					builder.append(separator);
					//NUMERO 	- ok o n�mero do segurado
					builder.append(numero != null ? numero : "");
					builder.append(separator);
					//COMPLEMENTO - ok o complemento do segurado
					builder.append(p.getComplemento());
					builder.append(separator);
					//BAIRRO 	- ok o bairro do segurado
					builder.append(p.getBairro());
					builder.append(separator);
					//CIDADE 	 - ok a cidade do segurado
					builder.append(p.getCidade().getNom_cidade());
					builder.append(separator);
					//UF 	- ok a uf do segurado
					builder.append(p.getCidade().getCod_estado().getSgl_estado());
					builder.append(separator);
					//CEP - Ok o cep do segurado
					builder.append(cepLimpo);
					builder.append(separator);
					
					builder.append("\r\n");
				}
			}
	        
	        output.write(builder.toString().getBytes());
	        output.flush();
	        output.close();

	        fc.responseComplete();

	    } catch (IOException e) {
	        throw new RuntimeException("Erro ao gerar rela��o para os correios", e);
	    }
		
	}

	public Map<String, List<Proposta>> getMapPropostas() {
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		List<SituacaoProposta> lista = new ArrayList<SituacaoProposta>();
		for (String s : situacaoPropostaSelectedList) {
			lista.add(SituacaoProposta.get(Integer.valueOf(s)));
		}
		return propostaDAO.buscarPorEmpresa(lista, empresaSelected, ufSelected, orgaoSelected,agenciador.getId(), dataAverbacao, modeloPropostaSelected, dataCancelamentoInicio, dataCancelamentoFim, tipoOrgaoSelected, profissao, mesAniversario);
	}

	
	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		
		if (tipo == 0) {
			int indiceLinha = 0;
			addRow(aba);
			addTextoMesclado(aba, getStiloTextoTitulo(), "Formul�rio de Movimenta��o - Vida", indiceLinha, 1, 0, 21);
			addRow(aba);
			indiceLinha++;
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoTitulo(), "Dados da Empresa", indiceLinha, 1, 0, 9);
			addTextoMesclado(aba, getStiloTextoTitulo(), "Dados da Ap�lice", indiceLinha, 1, 10, 11	);
			addTextoMesclado(aba, getStiloTextoTitulo(), "", indiceLinha, 1, 11, 20	);
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoSimples(), "*Raz�o Social/Estipulante: ITAVIDA SEGUROS", indiceLinha, 1, 0, 9);
			addTextoMesclado(aba, getStiloTextoSimples(), "*Compet�ncia Fatura:", indiceLinha, 1, 10, 10 );
			addTextoMesclado(aba, getStiloTextoSimples(), "",  indiceLinha, 1, 11, 11);
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoSimples(), getCNPJ(aba), indiceLinha, 1, 0, 8);
			addTextoMesclado(aba, getStiloTextoSimples(), "*Sub-Estipulante",  indiceLinha, 1, 9, 9);
			addTextoMesclado(aba, getStiloTextoSimples(), "*N� Ap�lice",  indiceLinha, 1, 10, 10);
			addTextoMesclado(aba, getStiloTextoSimples(), getNumeroApolice(aba),  indiceLinha, 1, 11, 11);
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoSimples(), "Contato:", indiceLinha, 1, 0, 8);
			addTextoMesclado(aba, getStiloTextoSimples(), "Telefone: (31)3274-5170",  indiceLinha, 1, 9, 9);
			addTextoMesclado(aba, getStiloTextoSimples(), "*N� Sub Estipulante",  indiceLinha, 1, 10, 10);
			addTextoMesclado(aba, getStiloTextoSimples(), "",  indiceLinha, 1, 11, 11);
			addRow(aba);
			indiceLinha++;
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","orgao", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","lotacao", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","nome-segurado-principal", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","plano", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-conjuge", null));		
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-total", null));
			
			
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-cancelamento", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","motivo-cancelamento", null));
			
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Av/Rua, N�");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Bairro");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Cidade");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Estado");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Cep");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "E-mail");
		} else if (tipo == 1 ) {
			addRow(aba);
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","orgao", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","lotacao", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","nome-segurado-principal", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","celular", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","profissao", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","plano", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","profissao-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-conjuge", null));		
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-total", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-cancelamento", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","motivo-cancelamento", null));			
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Av/Rua, N�");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Bairro");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Cidade");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Estado");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Cep");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "E-mail");
		} else {
			addWarnMessage( "Tipo de relat�rio n�o identificado." );
		}
		

		
	}

	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
       	for(Proposta proposta : mapaPropostas.get(aba)) {
       		
       		String email = StringUtils.isEmpty(proposta.getEmail()) ? proposta.getOrgao().getEmail() : proposta.getEmail();
       		email = StringUtils.isNotEmpty(email) ? email : "contato@itavida.com.br"; 
       		
       		if (email.contains(",")) {
       			email = email.split(",")[0];
       		}
       		
			if (tipo == 0) {
	       		addRow(aba);
	       	
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getOrgao() !=null ? proposta.getOrgao().getNomeOrgao() : null, "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getLotacao(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getNomeSegurado(), "-");
	       		addCelulaData(aba, getStiloDataSimples(), proposta.getDataNascimentoSegurado(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(proposta.getCpfSegurado()), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getTipoProposta().getDescricao());
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getNomeConjuge(), "-");
	       		addCelulaData(aba, getStiloDataSimples(), proposta.getDataNascimentoConjuge(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(proposta.getCpfConjuge()), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), proposta.getCapitalSegurado(), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), proposta.getCapitalConjuge(), "-");
	       		
	       		
	       		Double premioSeguradoArredondado = NumberUtil.round(proposta.getPremioSegurado(), 2);
	       		Double premioConjugeArredondado = NumberUtil.round(proposta.getPremioConjuge(), 2);
	       		Double totalArredondado = NumberUtil.round(proposta.getTotalPremios(), 2);
	       		
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), premioSeguradoArredondado, "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), premioConjugeArredondado, "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), totalArredondado, "-");
	       		
	       		addCelulaData(aba, getStiloTextoSimples(), proposta.getDataCancelamento(), "-");
	   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getMotivoCancelamento() != null? proposta.getMotivoCancelamento().getDescricao(): null, "-");
	   			
	   			if (StringUtils.isNotEmpty(proposta.getBairro()) && StringUtils.isNotEmpty(proposta.getEnderecoSegurado()) && proposta.getCidade() != null && StringUtils.isNotEmpty(proposta.getCep())) { 
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getEnderecoSegurado() + (!StringUtils.isEmpty(proposta.getNumeroEnderecoSegurado())? (" " + proposta.getNumeroEnderecoSegurado()) : ""));
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getBairro());
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getCidade().getNom_cidade());
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getCidade().getCod_estado().getSgl_estado());
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getCep());
	   			} else {
	   				addCelulaTexto(aba, getStiloTextoSimples(), "Rua S�o Paulo, 900");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "Centro");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "Belo Horizonte");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "MG");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "30170-131");
	   			}
	   			
	   			
	   			addCelulaTexto(aba, getStiloTextoSimples(),StringUtils.isNotEmpty(email) ? email : "contato@itavida.com.br" );
	   			
       		} else if (tipo == 1) {
       			
       			addRow(aba);
    	       	
       			
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getOrgao() !=null ? proposta.getOrgao().getSiglaOrgao() : null, "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getLotacao(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getNomeSegurado(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getTelefoneCelular(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getProfissaoSegurado(), "-");
	       		addCelulaData(aba, getStiloDataSimples(), proposta.getDataNascimentoSegurado(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(proposta.getCpfSegurado()), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getTipoProposta().getDescricao());
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getNomeConjuge(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getProfissaoConjuge(), "-");
	       		addCelulaData(aba, getStiloDataSimples(), proposta.getDataNascimentoConjuge(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(proposta.getCpfConjuge()), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), proposta.getCapitalSegurado(), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), proposta.getCapitalConjuge(), "-");
	       		
	       		
	       		Double premioSeguradoArredondado = NumberUtil.round(proposta.getPremioSegurado(), 2);
	       		Double premioConjugeArredondado = NumberUtil.round(proposta.getPremioConjuge(), 2);
	       		Double totalArredondado = NumberUtil.round(proposta.getTotalPremios(), 2);
	       		
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), premioSeguradoArredondado, "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), premioConjugeArredondado, "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), totalArredondado, "-");
	       		
	       		addCelulaData(aba, getStiloTextoSimples(), proposta.getDataCancelamento(), "-");
	   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getMotivoCancelamento() != null? proposta.getMotivoCancelamento().getDescricao(): null, "-");
	   			
	   			if (StringUtils.isNotEmpty(proposta.getBairro()) && StringUtils.isNotEmpty(proposta.getEnderecoSegurado()) && proposta.getCidade() != null && StringUtils.isNotEmpty(proposta.getCep())) { 
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getEnderecoSegurado() + (!StringUtils.isEmpty(proposta.getNumeroEnderecoSegurado())? (" " + proposta.getNumeroEnderecoSegurado()) : ""));
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getBairro());
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getCidade().getNom_cidade());
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getCidade().getCod_estado().getSgl_estado());
		   			addCelulaTexto(aba, getStiloTextoSimples(), proposta.getCep());
	   			} else {
	   				addCelulaTexto(aba, getStiloTextoSimples(), "Rua S�o Paulo, 900");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "Centro");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "Belo Horizonte");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "MG");
		   			addCelulaTexto(aba, getStiloTextoSimples(), "30170-131");
	   			}
	   			
	   			addCelulaTexto(aba, getStiloTextoSimples(),StringUtils.isNotEmpty(email) ? email : "contato@itavida.com.br" );
       			
       		}
       	}
	}

	/**
	 * Popula um combo de empresas.
	 *  
	 * @return
	 */
	public void populaOpcoesOrgaoCombo(){
		 
		List<Orgao> orgaos = buscarOrgaosPorRegiao();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesOrgaoCombo = new ArrayList<SelectItem>();
		opcoesOrgaoCombo.add( new SelectItem( null, "--Todos--" ) );
		for (Orgao orgao : orgaos) {
			opcoesOrgaoCombo.add( new SelectItem( orgao.getId(), orgao.getNomeOrgao()) );
		}
	}
	
	public void populaOpcoesTipoOrgaoCombo(){
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesTipoOrgaoCombo = new ArrayList<SelectItem>();
		opcoesTipoOrgaoCombo.add( new SelectItem( null, "--Todos--" ) );
		opcoesTipoOrgaoCombo.add( new SelectItem( "E", "Estadual"));
		opcoesTipoOrgaoCombo.add( new SelectItem( "M", "Municipal"));
		opcoesTipoOrgaoCombo.add( new SelectItem( "F", "Federal"));
		
	}
	

	public List<Orgao> buscarOrgaosPorRegiao() {
		Session session = HibernateUtil.currentSession();
		OrgaoDAO orgaoDAO = new OrgaoDAO(session);
		
		if(ufSelected != null && ufSelected > 0) {
			return orgaoDAO.buscarPorEstado(ufSelected);
		} else if (ufSelected != null && ufSelected < 0) {
			return orgaoDAO.buscarOrgaosVariosEstados();
		} else {
			return new ArrayList<Orgao>();
		}
	}
	
	/**
	 * Popula um combo de situa��es da proposta.
	 *  
	 */
	public void populaOpcoesSituacaoPropostaCombo(){
		
		opcoesSituacaoPropostaCombo = new ArrayList<SelectItem>();
//		opcoesSituacaoPropostaCombo.add( new SelectItem( -1, "--Todas--" ) );
		for(SituacaoProposta situacao : SituacaoProposta.values()) {
			opcoesSituacaoPropostaCombo.add( new SelectItem( situacao.getId(), situacao.getNome()));
		}
		
	}
	
	/**
	 * Popula um combo de empresas.
	 *  
	 * @return
	 */
	public void populaOpcoesEmpresaCombo(){
		
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO =  new EmpresaDAO( session );
		
		List<Empresa> listaEmpresas = empresaDAO.buscaEmpresas();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesEmpresaCombo = new ArrayList<SelectItem>();
		opcoesEmpresaCombo.add( new SelectItem( null, "--Todas--" ) );
		for (Empresa empresa : listaEmpresas) {
			opcoesEmpresaCombo.add( new SelectItem( empresa.getId(), empresa.getNomeEmpresa()) );
		}
	}
	
	/**
	 * Popula um combo de UF
	 */
	public void populaOpcoesUfCombo() {
		
		Session session = HibernateUtil.currentSession();
		CidadeDAO cidadeDAO = new CidadeDAO(session);
		
		List<Estado> estados = cidadeDAO.obterTodosEstados();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (Estado estado : estados) {
			opcoesUfCombo.add( new SelectItem( estado.getCod_estado(), estado.getNom_estado()) );
		}
		
	}
	
	/**
	 * Popula um combo com os tipos de UF existentes no sistema.
	 */
	public void populaOpcoesRegiaoSeguradoCombo() {
		//Busca os valores de regi�o do enum RegiaoSegurado
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (RegiaoSegurado regiao : RegiaoSegurado.values()) {
			opcoesUfCombo.add( new SelectItem( regiao.getId(), regiao.getNome()));
		}
	}
	
	public void atualizarCombo(ActionEvent e) {
		populaOpcoesOrgaoCombo();
	}
	
	public void atualizarCombo(ValueChangeEvent e) {
		populaOpcoesOrgaoCombo();
	}

	public void cancelar(ActionEvent e) {
		limparDados();
	}
	
	public String navegar() {
		limparDados();
		return "resultado";
	}

	public String getTipoOrgaoSelected() {
		return tipoOrgaoSelected;
	}

	public void setTipoOrgaoSelected(String tipoOrgaoSelected) {
		this.tipoOrgaoSelected = tipoOrgaoSelected;
	}

	public List<SelectItem> getOpcoesTipoOrgaoCombo() {
		return opcoesTipoOrgaoCombo;
	}

	public void setOpcoesTipoOrgaoCombo(List<SelectItem> opcoesTipoOrgaoCombo) {
		this.opcoesTipoOrgaoCombo = opcoesTipoOrgaoCombo;
	}

	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public Integer getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Integer empresaSelected) {
		this.empresaSelected = empresaSelected;
	}

	public Date getDataAverbacao() {
		return dataAverbacao;
	}

	public void setDataAverbacao(Date dataAverbacao) {
		this.dataAverbacao = dataAverbacao;
	}

	public Funcionario getAgenciador() {
		return agenciador;
	}

	public Integer getSituacaoPropostaSelected() {
		return situacaoPropostaSelected;
	}

	public void setSituacaoPropostaSelected(Integer tipoConsultaSelected) {
		this.situacaoPropostaSelected = tipoConsultaSelected;
	}

	public Integer getModeloPropostaSelected() {
		return modeloPropostaSelected;
	}

	public void setModeloPropostaSelected(Integer modeloPropostaSelected) {
		this.modeloPropostaSelected = modeloPropostaSelected;
	}

	public void setAgenciador(Funcionario agenciador) {
		this.agenciador = agenciador;
	}

	public List<SelectItem> getOpcoesUfCombo() {
		return opcoesUfCombo;
	}

	public void setOpcoesUfCombo(List<SelectItem> opcoesUfCombo) {
		this.opcoesUfCombo = opcoesUfCombo;
	}

	public Integer getUfSelected() {
		return ufSelected;
	}

	public void setUfSelected(Integer ufSelected) {
		this.ufSelected = ufSelected;
	}

	public List<SelectItem> getOpcoesSituacaoPropostaCombo() {
		return opcoesSituacaoPropostaCombo;
	}

	public void setOpcoesSituacaoPropostaCombo(List<SelectItem> opcoesTipoConsultaCombo) {
		this.opcoesSituacaoPropostaCombo = opcoesTipoConsultaCombo;
	}

	public Integer getOrgaoSelected() {
		return orgaoSelected;
	}

	public void setOrgaoSelected(Integer orgaoSelected) {
		this.orgaoSelected = orgaoSelected;
	}

	public List<SelectItem> getOpcoesOrgaoCombo() {
		return opcoesOrgaoCombo;
	}

	public void setOpcoesOrgaoCombo(List<SelectItem> opcoesOrgaoCombo) {
		this.opcoesOrgaoCombo = opcoesOrgaoCombo;
	}

	public List<String> getSituacaoPropostaSelectedList() {
		return situacaoPropostaSelectedList;
	}

	public void setSituacaoPropostaSelectedList(
			List<String> situacaoPropostaSelectedList) {
		this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
	}

	public Date getDataCancelamentoInicio() {
		return dataCancelamentoInicio;
	}

	public void setDataCancelamentoInicio(Date dataCancelamentoInicio) {
		this.dataCancelamentoInicio = dataCancelamentoInicio;
	}

	public Date getDataCancelamentoFim() {
		return dataCancelamentoFim;
	}

	public void setDataCancelamentoFim(Date dataCancelamentoFim) {
		this.dataCancelamentoFim = dataCancelamentoFim;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public Integer getMesAniversario() {
		return mesAniversario;
	}

	public void setMesAniversario(Integer mesAniversario) {
		this.mesAniversario = mesAniversario;
	}

	
	

}
