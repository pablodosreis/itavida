package br.com.itavida.handlers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import com.mysql.jdbc.log.Log;

import br.com.itavida.dao.MovimentacaoFinanceiraDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.MovimentacaoFinanceira;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoBoleto;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.enums.TipoPagamentoEnum;
import br.com.itavida.exceptions.SistemaException;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.Contexto;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class MovimentacaoSuperApHandler extends MainHandler {
	
	private MovimentacaoFinanceiraDAO movimentacaoDAO = Contexto.getMovimentacaoFinanceiraDAO();
	
	
	private List<MovimentacaoFinanceira> movimentacoes;
	
	private String periodo;
	private boolean avancou = false;
	private Integer qtdRegistros;
	private Double totalValor;
	private Double totalComissao;
	
	private Integer qtdRegistrosPago;
	private Double totalValorPago;
	private Double totalComissaoPago;
	
	private Integer idMov;
	private Proposta proposta;
	private Double valorPagamento;
	private Double valorComissao;
	private Date dataPagamento;
	private Date dataPagamentoComissao;
	private Integer numeroParcela;
	private Date dataPrevisaoBoleto;
	private Integer situacaoBoletoId;
	private String observacao;
	private String periodicidade;
	
	
	
	public MovimentacaoSuperApHandler() {		
		super();		
		proposta = new Proposta();
		movimentacoes = new ArrayList<MovimentacaoFinanceira>();
		populaOpcoesSituacaoBoletoCombo();
	}	
	

	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		try {
			this.valorPagamento = proposta.getValorPremioSegurado();
			this.numeroParcela = movimentacaoDAO.proximaParcela(proposta).intValue();
			int mes = getMesReferencia();
			int ano = getAnoReferencia();
			this.dataPrevisaoBoleto = DateUtil.getData(proposta.getSuperAp().getDiaVencimento(), mes, ano);
			this.proposta = proposta;
			this.periodicidade = proposta.getPeriodicidadeFormatada(); 
		} catch (Exception e ) {
			addErrorMessage("N�o foi poss�vel obter os dados da proposta: " + e.getMessage());
			e.printStackTrace();
		}
	
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}


	public Integer getMesReferencia() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getReferencia());
		return calendar.get(Calendar.MONTH) +1 ;
	}
	
	public Integer getAnoReferencia() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getReferencia());
		return calendar.get(Calendar.YEAR);
	}
	
	public Date getReferencia() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = format.parse("01/" + periodo);
		} catch (ParseException e1) {
			//faz nada
		}
		return date;
	}
	
	public List<MovimentacaoFinanceira> getMovimentacoes() {
		qtdRegistros = movimentacoes.size();
		qtdRegistrosPago = 0;
		
		totalValorPago = 0.0;
		totalComissaoPago = 0.0;
		
		totalValor = 0.0;
		totalComissao = 0.0;
		for (MovimentacaoFinanceira moviFinanceira : movimentacoes) {
			
			if (moviFinanceira.getDataPagamento() != null) {
				qtdRegistrosPago++;
			}
			
			if (moviFinanceira.getValor() != null) {
				totalValor += moviFinanceira.getValor(); 
				if (moviFinanceira.getDataPagamento() != null) {
					totalValorPago += moviFinanceira.getValor();
				}
			}
			if (moviFinanceira.getValorComissao() != null) {
				totalComissao += moviFinanceira.getValorComissao();
				if (moviFinanceira.getDataPagamentoComissao() != null) {
					totalComissaoPago += moviFinanceira.getValorComissao();
				}
			}
		}
		return movimentacoes;
	}
	

	public void avancar( ActionEvent e ) {
		avancou = true;
	}
	
	public void avancarSimples(ActionEvent e) {
		avancou = true;
	}
	
	
	public void incluirMovimentacao( ActionEvent e ){
		movimentacoes = new ArrayList<MovimentacaoFinanceira>();
		
		Session currentSession = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(currentSession);
		
		if (proposta ==  null || proposta.getId() == null) {
			addErrorMessage("Voc� precisa selecionar uma proposta antes de solicitar incluir");
			proposta = new Proposta();
			return;
		}		
		
		proposta = propostaDAO.load(proposta.getId());
		
		if (proposta.getSuperAp() == null) {
			addErrorMessage("Voc� precisa selecionar uma proposta Super Ap.");
			proposta = new Proposta();
			return;
		}
		
		if (SituacaoBoleto.get(situacaoBoletoId) == null) {
			addErrorMessage("Voc� precisa selecionar uma situa��o para a movimenta��o.");
			return;
		}
		
		/*if (proposta.getSituacao() == SituacaoProposta.CANCELADA) {
			addErrorMessage("Voc� n�o pode adicionar movimenta��o para uma proposta cancelada.");
			proposta = new Proposta();
			return;
		}*/

		//antes de acessar valor de pr�mio ou capital de uma proposta � necess�rio chamar este m�todo
		propostaDAO.populaCapital(proposta);
		incluirMovimentacao(proposta);
	}


	private void incluirMovimentacao(Proposta proposta) {
		
		try {
			
			
			MovimentacaoFinanceira mov;
			
			if (idMov != null && idMov > 0) {
				mov = movimentacaoDAO.load(idMov);
			} else {
				 mov = new MovimentacaoFinanceira();
			}
			
			mov.setProposta(proposta);
			mov.setReferencia(getReferencia());
			
			/*Dados do pagamento*/
			mov.setValor(valorPagamento);
			mov.setValorComissao(valorComissao);
			mov.setDataPagamento(dataPagamento);
			mov.setDataPagamentoComissao(dataPagamentoComissao);
			mov.setNumeroParcela(numeroParcela);
			mov.setDataPrevisaoBoleto(dataPrevisaoBoleto);
			mov.setSituacaoBoleto(SituacaoBoleto.get(situacaoBoletoId));
			mov.setObservacao(observacao);
			
			//salva a data de averba��o
			if (dataPagamento != null && proposta.getSituacao() != SituacaoProposta.ATIVA) {
				proposta.setDataAprovacao(dataPagamento);
				proposta.setSituacao(SituacaoProposta.ATIVA);
				movimentacaoDAO.getSession().saveOrUpdate(proposta);
			}
			
			
			/*Calcular idade*/
			Long idadeMilis = new Date().getTime() - proposta.getDataNascimentoSegurado().getTime();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(idadeMilis);
			
			/*Registra campos no momento do pagamento*/
			mov.setSituacao(proposta.getSituacao());
			mov.setValorPremio(proposta.getValorPremioSegurado());
			mov.setValorPremioConjuge(proposta.getValorPremioConjuge());
			mov.setValorCapital(proposta.getValorCapitalSegurado());
			mov.setValorCapitalConjuge(proposta.getValorCapitalConjuge());
			mov.setEmpresa(proposta.getEmpresa());
			mov.setIdade(calendar.get(Calendar.YEAR));
			mov.setOrgao(proposta.getOrgao());
			mov.setSexo(proposta.getSexoSegurado());
			
			movimentacaoDAO.saveOrUpdate(mov);
			
			proposta = new Proposta();
			valorPagamento = null;
			valorComissao = null;
			dataPagamento = null;
			dataPagamentoComissao = null;
			numeroParcela = null;
			idMov = null;
			dataPrevisaoBoleto = null;
			observacao = null;
			situacaoBoletoId = null;
			periodicidade = null;
			
			visualizarMovimentacoes(null);
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
		} catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}
		
		proposta = new Proposta();
		this.proposta = new Proposta();
	}
	
	public void excluirMovimentacao(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirMovimentacao");
		MovimentacaoFinanceira movimentacao = ( MovimentacaoFinanceira ) param.getValue();
		try {
			movimentacaoDAO.delete(movimentacao);
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			visualizarMovimentacoes(null);
		} catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}
		
	}
	
	public void editarMovimentacao(ActionEvent e){
		
		movimentacoes = new ArrayList<MovimentacaoFinanceira>();
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("editarMovimentacao");
		MovimentacaoFinanceira mov = ( MovimentacaoFinanceira ) param.getValue();
		proposta = mov.getProposta();
		valorPagamento = mov.getValor();
		valorComissao = mov.getValorComissao();
		dataPagamento = mov.getDataPagamento();
		dataPagamentoComissao = mov.getDataPagamentoComissao();
		numeroParcela = mov.getNumeroParcela();
		dataPrevisaoBoleto = mov.getDataPrevisaoBoleto();
		observacao = mov.getObservacao();
		situacaoBoletoId = mov.getSituacaoBoleto() != null ? mov.getSituacaoBoleto().getId() : null;
		periodicidade = mov.getPeriodicidadeFormatada();
		idMov = mov.getId();

	}
	
	public void limparMovimentacao(ActionEvent e){
		proposta = new Proposta();
		movimentacoes = new ArrayList<MovimentacaoFinanceira>();
		valorPagamento = null;
		valorComissao = null;
		dataPagamento = null;
		dataPagamentoComissao = null;
		numeroParcela = null;
		idMov = null;
		dataPrevisaoBoleto = null;
		observacao = null;
		situacaoBoletoId = null;
	}
		
	
	public void visualizarMovimentacoes(ActionEvent e){
		movimentacoes = movimentacaoDAO.buscarMovimentacoes(getReferencia());
	}

	
	public boolean isBoleto() {
		if (proposta != null && proposta.getFormaPagamento() != null) {
			if (proposta.getFormaPagamento().getTipo() == TipoPagamentoEnum.BOLETO) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	public boolean isAvancou() {
		return avancou;
	}

	public void setAvancou(boolean avancou) {
		this.avancou = avancou;
	}


	public Integer getQtdRegistros() {
		return qtdRegistros;
	}


	public void setQtdRegistros(Integer qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}


	public Double getValorPagamento() {
		return valorPagamento;
	}


	public void setValorPagamento(Double valorPagamento) {
		this.valorPagamento = valorPagamento;
	}


	public Double getValorComissao() {
		return valorComissao;
	}


	public void setValorComissao(Double valorComissao) {
		this.valorComissao = valorComissao;
	}


	public Date getDataPagamento() {
		return dataPagamento;
	}


	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}


	public Date getDataPagamentoComissao() {
		return dataPagamentoComissao;
	}


	public void setDataPagamentoComissao(Date dataPagamentoComissao) {
		this.dataPagamentoComissao = dataPagamentoComissao;
	}


	public Integer getNumeroParcela() {
		return numeroParcela;
	}


	public void setNumeroParcela(Integer numeroParcela) {
		this.numeroParcela = numeroParcela;
	}


	public Integer getIdMov() {
		return idMov;
	}


	public void setIdMov(Integer idMov) {
		this.idMov = idMov;
	}


	public Date getDataPrevisaoBoleto() {
		return dataPrevisaoBoleto;
	}


	public void setDataPrevisaoBoleto(Date dataPrevisaoBoleto) {
		this.dataPrevisaoBoleto = dataPrevisaoBoleto;
	}


	public Integer getSituacaoBoletoId() {
		return situacaoBoletoId;
	}


	public void setSituacaoBoletoId(Integer situacaoBoletoId) {
		this.situacaoBoletoId = situacaoBoletoId;
	}


	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public Double getTotalValor() {
		return totalValor;
	}


	public Double getTotalComissao() {
		return totalComissao;
	}


	public void setTotalValor(Double totalValor) {
		this.totalValor = totalValor;
	}


	public void setTotalComissao(Double totalComissao) {
		this.totalComissao = totalComissao;
	}


	public Integer getQtdRegistrosPago() {
		return qtdRegistrosPago;
	}


	public Double getTotalValorPago() {
		return totalValorPago;
	}


	public Double getTotalComissaoPago() {
		return totalComissaoPago;
	}


	public void setQtdRegistrosPago(Integer qtdRegistrosPago) {
		this.qtdRegistrosPago = qtdRegistrosPago;
	}


	public void setTotalValorPago(Double totalValorPago) {
		this.totalValorPago = totalValorPago;
	}


	public void setTotalComissaoPago(Double totalComissaoPago) {
		this.totalComissaoPago = totalComissaoPago;
	}


	public String getPeriodicidade() {
		return periodicidade;
	}


	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}
	
	
	
	
	
}
