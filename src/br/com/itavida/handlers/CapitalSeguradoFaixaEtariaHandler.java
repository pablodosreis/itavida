package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.CapitalSeguradoFaixaEtariaDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.DetalheCapitalSeguradoFaixaEtariaDAO;
import br.com.itavida.dao.FatorDAO;
import br.com.itavida.entidades.CapitalSeguradoFaixaEtaria;
import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;
import br.com.itavida.entidades.Fator;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class CapitalSeguradoFaixaEtariaHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria;
	private List<CapitalSeguradoFaixaEtaria> resultadoPesquisa;
	private List<Fator> resultadoPesquisaFator;
	private DetalheCapitalFaixaEtaria detalheCapital;
	private Fator fator;
	private List<DetalheCapitalFaixaEtaria> detalheCapitalExcluidos;
	private List<NomeTabela> listaNomesTabelas;
	
	// Getters and Setters
	
	public List<NomeTabela> getListaNomesTabelas() {
		return listaNomesTabelas;
	}

	public DetalheCapitalFaixaEtaria getDetalheCapital() {
		return detalheCapital;
	}

	public void setDetalheCapital(DetalheCapitalFaixaEtaria detalheCapital) {
		this.detalheCapital = detalheCapital;
	}

	public List<DetalheCapitalFaixaEtaria> getDetalheCapitalExcluidos() {
		return detalheCapitalExcluidos;
	}


	public void setDetalheCapitalExcluidos(
			List<DetalheCapitalFaixaEtaria> detalheCapitalExcluidos) {
		this.detalheCapitalExcluidos = detalheCapitalExcluidos;
	}

	public void setListaNomesTabelas(List<NomeTabela> listaNomesTabelas) {
		this.listaNomesTabelas = listaNomesTabelas;
	}
	
	public CapitalSeguradoFaixaEtaria getCapitalSeguradoFaixaEtaria() {
		return capitalSeguradoFaixaEtaria;
	}

	public void setCapitalSeguradoFaixaEtaria(CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria) {
		this.capitalSeguradoFaixaEtaria = capitalSeguradoFaixaEtaria;
	}
	
	
	public List<CapitalSeguradoFaixaEtaria> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<CapitalSeguradoFaixaEtaria> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	// Fim Getters and Setters
	


	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public CapitalSeguradoFaixaEtariaHandler(){
		cancelar( null );
	}
	

	public void gravarFator (ActionEvent e) {
		Session session = HibernateUtil.currentSession();
		Dao<Fator, Date> dao = new Dao<Fator, Date>(session, Fator.class);
		dao.merge(fator);
	}
	

	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaCapitaisSegurados(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		CapitalSeguradoFaixaEtariaDAO dao = new CapitalSeguradoFaixaEtariaDAO( session );
		
		CapitalSeguradoFaixaEtaria capitalFiltro = getCapitalSeguradoFaixaEtaria();
		
		List<CapitalSeguradoFaixaEtaria> lista = dao.buscaPorFiltro( capitalFiltro  );
		
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao(getCapitalSeguradoFaixaEtaria().getNomeTabela().getId(), "Nome da Tabela") ){
		
			Dao<CapitalSeguradoFaixaEtaria, Integer> dao = new Dao<CapitalSeguradoFaixaEtaria, Integer>(session, CapitalSeguradoFaixaEtaria.class);
			Dao<DetalheCapitalFaixaEtaria, Integer> daoDetalhe = new Dao<DetalheCapitalFaixaEtaria, Integer>(session, DetalheCapitalFaixaEtaria.class);
			CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtariaGravar = getCapitalSeguradoFaixaEtaria();
			
			for (Iterator iterator = capitalSeguradoFaixaEtariaGravar.getDetalhecapitalfaixaetaria_collection().iterator(); iterator.hasNext();) {
				DetalheCapitalFaixaEtaria element = (DetalheCapitalFaixaEtaria) iterator.next();				
				element.setCapitalSeguradoFaixaEtaria(capitalSeguradoFaixaEtariaGravar);
			}
			
			//Exclui os contatos marcados para exclus�o
			for (Iterator iterator = getDetalheCapitalExcluidos().iterator(); iterator.hasNext();) {
				DetalheCapitalFaixaEtaria detalheExcluido = (DetalheCapitalFaixaEtaria) iterator.next();	
				daoDetalhe.delete( detalheExcluido );
			}
			//Zera a lista de contatos excluidos
			setDetalheCapitalExcluidos(new ArrayList<DetalheCapitalFaixaEtaria>() );
			
			//persiste os dados
			dao.merge( capitalSeguradoFaixaEtariaGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			limparCamposPosInclusao(e);
		}else{
			//Limpa a lista de cidades;
			listaNomesTabelas = null;
		}
		
		if (fator.getDataInicio() != null && fator.getFator() != null) {
			gravarFator(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeTabela", "Nome da Tabela");
		mapaCampos.put("dtDataVigencia", "Data de Vig�ncia");
				
		boolean retorno = validaInclusaoObrigatoria( getCapitalSeguradoFaixaEtaria().getDetalhecapitalfaixaetaria_collection() , "Detalhe Capital Faixa Et�ria");
		retorno = (validaObrigatoriedadeFormulario(mapaCampos, e) && retorno);
		
		return retorno;
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormularioDetalhe(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtIdadeInicial", "Idade Inicial");
		mapaCampos.put("txtIdadeFinal", "Idade Final");
		mapaCampos.put("txtCapitalSegurado", "Capital Segurado");
		mapaCampos.put("txtTitular", "Titular");	
		mapaCampos.put("txtTitularConjuge", "Titular + C�njuge");			
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		if( retorno ){
			// Valida se a idade final � menor que a idade inicial
			if( getDetalheCapital().getIdadeInicial().intValue() > getDetalheCapital().getIdadeFinal().intValue() ){
				String key = Constantes.KEY_VALIDACAO_CAMPO1_MAIORQ_CAMPO2;
				ArrayList<String> array = new ArrayList<String>();
				array.add("Idade Final");				
				array.add("Idade Inicial");
				String message = JSFUtils.getMessageResourceString("Messages", key, array.toArray());
				addErrorMessage(message);
				retorno = false;
			}
		}
		
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarCapitalSegurado( ){
		
		//Recupera o item escolhido
		Session session = HibernateUtil.currentSession();
		DetalheCapitalSeguradoFaixaEtariaDAO detalheCapitalDAO =  new DetalheCapitalSeguradoFaixaEtariaDAO( session );
		List<DetalheCapitalFaixaEtaria> listaDetalhes = detalheCapitalDAO.buscaPorCapitalFaixaEtaria( capitalSeguradoFaixaEtaria.getId() );
		getCapitalSeguradoFaixaEtaria().setDetalhecapitalfaixaetaria_collection( listaDetalhes );
		
		resultadoPesquisa = null;
		
		return "incluirCapitalFaixaEtaria";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirCapitalSegurado(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirCapitalSegurado");
		CapitalSeguradoFaixaEtaria capitalDeletado  = ( CapitalSeguradoFaixaEtaria ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<CapitalSeguradoFaixaEtaria, Integer> dao = new Dao<CapitalSeguradoFaixaEtaria, Integer>(session, CapitalSeguradoFaixaEtaria.class);
		dao.delete(capitalDeletado);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaCapitaisSegurados(e);
	}	
	
	
	/**
	 * Realiza a inclus�o de um novo Detalhe de capital que ser� persistido no gravar
	 * @param e
	 */
	public void incluirDetalheCapital( ActionEvent e ){
		
		if( validarFormularioDetalhe(e) ){
			
			getCapitalSeguradoFaixaEtaria().getDetalhecapitalfaixaetaria_collection().remove( getDetalheCapital() );
			getCapitalSeguradoFaixaEtaria().getDetalhecapitalfaixaetaria_collection().add( getDetalheCapital() );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_OPERACAO_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelarDetalhe(e);
		}
	}
	
	/**
	 * Realiza a exclus�o de um Detalhe do Capital de Faixa Etaria e que ser� persistido no gravar
	 * @param e
	 */
	public void excluirDetalheCapital( ActionEvent e ){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirDetalheCapital");
		DetalheCapitalFaixaEtaria detalheCapital = ( DetalheCapitalFaixaEtaria ) param.getValue();
		if( detalheCapitalExcluidos == null ){
			detalheCapitalExcluidos = new ArrayList<DetalheCapitalFaixaEtaria>();
		}
		getDetalheCapitalExcluidos().add( detalheCapital );
		getCapitalSeguradoFaixaEtaria().getDetalhecapitalfaixaetaria_collection().remove( detalheCapital );
						
	}
	
	
	/**
	 * Realiza a altera��o de um Detalhe Capital que ser� persistido no gravar
	 * @param e
	 */
	public void alterarDetalheCapital( ActionEvent e ){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("editDetalheCapital");
		DetalheCapitalFaixaEtaria detalheCapital = ( DetalheCapitalFaixaEtaria ) param.getValue();
		setDetalheCapital( detalheCapital );
						
	}
	
	/**
	 * Realiza a inclus�o de um novo contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void cancelarDetalhe( ActionEvent e ){		
		setDetalheCapital( new DetalheCapitalFaixaEtaria() );						
	}	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		
		// Realiza o cancelamento das a��es
		setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
		getCapitalSeguradoFaixaEtaria().setNomeTabela(new NomeTabela());
		setDetalheCapital( new DetalheCapitalFaixaEtaria() );
		setFator (new Fator());
		getCapitalSeguradoFaixaEtaria().setDetalhecapitalfaixaetaria_collection( new ArrayList<DetalheCapitalFaixaEtaria>() );
		resultadoPesquisaFator = new ArrayList<Fator>();
		detalheCapitalExcluidos = new ArrayList<DetalheCapitalFaixaEtaria>();		
		this.resultadoPesquisa = null;
		
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void limparCamposPosInclusao( ActionEvent e ){
		NomeTabela nomeTabela = getCapitalSeguradoFaixaEtaria().getNomeTabela();
		// Realiza o cancelamento das a��es
		setCapitalSeguradoFaixaEtaria( new CapitalSeguradoFaixaEtaria() );
		getCapitalSeguradoFaixaEtaria().setNomeTabela( nomeTabela );
		setDetalheCapital( new DetalheCapitalFaixaEtaria() );
		getCapitalSeguradoFaixaEtaria().setDetalhecapitalfaixaetaria_collection( new ArrayList<DetalheCapitalFaixaEtaria>() );
		detalheCapitalExcluidos = new ArrayList<DetalheCapitalFaixaEtaria>();		
		this.resultadoPesquisa = null;
		
	}
	
	public NomeTabela getLimparNomeTabela(){
		return new NomeTabela();
	}


	public Fator getFator() {
		return fator;
	}


	public void setFator(Fator fator) {
		this.fator = fator;
	}


	public List<Fator> getResultadoPesquisaFator() {
		return resultadoPesquisaFator;
	}


	public void setResultadoPesquisaFator(List<Fator> resultadoPesquisaFator) {
		this.resultadoPesquisaFator = resultadoPesquisaFator;
	}

	
}
