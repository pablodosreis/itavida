package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.HistoricoAjusteEmpresa;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class EmpresaHandler extends MainHandler implements Serializable {

	private Empresa empresa = new Empresa();
	
	private List<Empresa> resultadoPesquisa;
	
	private HistoricoAjusteEmpresa historico;


	@PostConstruct
	public void init() {
		if(empresa.getListaHistoricoAjuste() == null) {
			empresa.setListaHistoricoAjuste(new ArrayList<HistoricoAjusteEmpresa>());
		}
	}
	
	public void agendarAtualizacao(ActionEvent e){
		
	}
	
	public String getProximaAtualizacao() {
		Date proximaAtualizacao = empresa.getProximaAtualizacao();
		if(proximaAtualizacao != null ) {
			return DateUtil.getFormattedDate(proximaAtualizacao, "dd/MM/yyyy");
		}
		return "";
	}
	
	
	public void atualizar(ActionEvent e){

		UsuarioHandler usuarioHandler = (UsuarioHandler) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioHandler");
		Usuario usuario = usuarioHandler.getUsuario();
		
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<Empresa, Integer> dao = new Dao<Empresa, Integer>(session, Empresa.class);
			dao.merge( getEmpresa() );
		
			EmpresaDAO empresaDAO = new EmpresaDAO(session);
			empresaDAO.atualizarValorPropostas(empresa, usuario.getNomeUsuario());
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_OPERACAO_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);

		}
	}
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<Empresa, Integer> dao = new Dao<Empresa, Integer>(session, Empresa.class);
			dao.merge( getEmpresa() );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	private boolean validarExclusaoEmpresa (Empresa empresa) {
		boolean exclusaoValida = true;
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		Integer propostas = propostaDAO.countPropostasPorEmpresa(empresa.getId());
		if (propostas != null && !propostas.equals(0)) {
			exclusaoValida = false;
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_VALIDACAO_EMPRESA_VINCULADA_PROPOSTAS, new Object[]{empresa.getNomeEmpresa()});
			addErrorMessage(message);
		}
		return exclusaoValida;
	}
	
	public void excluirEmpresa(ActionEvent e){
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirEmpresa");
		Empresa empresaDeletada = ( Empresa ) param.getValue();
		if (validarExclusaoEmpresa(empresaDeletada)) {
			//Realiza as a��es de banco: Busca e seta os valores
			Session session = HibernateUtil.currentSession();
			Dao<Empresa, Integer> dao = new Dao<Empresa, Integer>(session, Empresa.class);
			dao.delete(empresaDeletada);
			// Dispara novamente a pesquisa para manter o v�nculo
			pesquisaEmpresas(e);
		}
	}	
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarEmpresa( ){
		Session session = HibernateUtil.currentSession();
		EmpresaDAO dao = new EmpresaDAO(session);
		empresa = dao.getEmpresa(empresa.getId());
		return "incluirEmpresa";
	}
	
	public void pesquisaEmpresas(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		EmpresaDAO dao = new EmpresaDAO(session);	
		
		List<Empresa> lista = dao.buscaPeloNome(getEmpresa().getNomeEmpresa());
		setResultadoPesquisa(lista);
		if (lista == null || lista.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
		}
			
	}

	
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setEmpresa( new Empresa() );
		//this.resultadoPesquisa = null;
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeEmpresa", "Nome da Empresa");
		mapaCampos.put("dtAniversario", "Anivers�rio");
		
		if(empresa.isAjusteAtivo()) {
			mapaCampos.put("vlIPCA", "IPCA Acumulado");
		}
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}

	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Empresa> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Empresa> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public HistoricoAjusteEmpresa getHistorico() {
		return historico;
	}

	public void setHistorico(HistoricoAjusteEmpresa historico) {
		this.historico = historico;
	}
	
}
