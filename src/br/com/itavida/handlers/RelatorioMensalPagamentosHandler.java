package br.com.itavida.handlers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.Session;

import br.com.itavida.dao.CompetenciaDAO;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dto.PagamentoMensalDTO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.CpfCnpjUtils;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class RelatorioMensalPagamentosHandler extends ExcelGenerateHandler implements Serializable {

	private static Logger logger = Logger.getLogger( RelatorioMensalPagamentosHandler.class );
	
	private static final long serialVersionUID = 1L;
	//Dados para Tela de Filtro
	
	private String cpfSegurado;
	private Integer empresaId;
	private String nomeSegurado;
	private String filtroMes;
	private String filtroAno;
	private List<PagamentoMensalDTO> competenciasRetornadas; 
	private List<SelectItem> opcoesEmpresaCombo;
	private List<String> situacaoPropostaSelectedList = new ArrayList<String>();
	

	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 * 
	 */
	public RelatorioMensalPagamentosHandler(){
		populaOpcoesEmpresaCombo();
	}
	
		/**
		 * Popula um combo com op��es do Agenciador 
		 * @return
		 */
		public void populaOpcoesEmpresaCombo(){
			
			Session session = HibernateUtil.currentSession();
			EmpresaDAO empresaDAO =  new EmpresaDAO( session );
			
			List<Empresa> listaEmpresas = empresaDAO.buscaEmpresas();
			
			//Realiza as a��es de banco: Busca e seta os valores
			opcoesEmpresaCombo = new ArrayList<SelectItem>();
			opcoesEmpresaCombo.add( new SelectItem( null, "Selecione:" ) );
			for (Iterator iterator = listaEmpresas.iterator(); iterator.hasNext();) {
				Empresa empresa = (Empresa) iterator.next();
				opcoesEmpresaCombo.add( new SelectItem( empresa.getId(), empresa.getNomeEmpresa()) );
			}
			
		}
	
	public String pesquisaCompetencias(){
		List<SituacaoProposta> lista = new ArrayList<SituacaoProposta>();
		for (String s : situacaoPropostaSelectedList) {
			lista.add(SituacaoProposta.get(Integer.valueOf(s)));
		}
		
		boolean vA = validaAno();
		boolean vM = validaMes();
		if(vA && vM) {
			CompetenciaDAO competenciaDao = new CompetenciaDAO();
			competenciasRetornadas = competenciaDao.buscaCompetenciasPorPagamentoMensal(filtroMes, filtroAno, cpfSegurado, empresaId, nomeSegurado, lista);
			gerarRelatorio(Arrays.asList("Relat�rio de Quita��o"), 6, "RelatorioQuitacao.xls");
		}
		return null;
	}

	private boolean validaMes() {
		if(filtroMes == null || filtroMes.isEmpty()) {
			addErrorMessage("O campo M�s deve ser informado");
			return false;
		}
		
		double valor = 0;
		try{
			valor = Double.parseDouble(filtroMes);
			if(valor < 1 || valor > 12){
				throw new NumberFormatException("Invalido");
			}
		}catch(NumberFormatException ne){
			addErrorMessage("O M�s deve ser um n�mero entre 1 e 12.");
			return false;
		}
		
		return true;
	}
	
	private boolean validaAno() {
		if(filtroAno == null || filtroMes.isEmpty()) {
			addErrorMessage("O campo Ano deve ser informado");
			return false;
		}
		return true;
	}
	
	public String generateReport(  ){
		
        try {  
        	
            HSSFWorkbook wb = new HSSFWorkbook();  
            HSSFSheet sheet = wb.createSheet("Planilha 1");  
              
            HSSFRow row;  
            
            //Estilos
            short formatoMonetario = wb.createDataFormat().getFormat("R$ #,##0.00;[Red]R$ #,##0.00");
			
            HSSFCellStyle estiloMonetario = wb.createCellStyle();
            estiloMonetario.setDataFormat( formatoMonetario );
            
			HSSFCellStyle estiloCabecalho = wb.createCellStyle();
			
			HSSFFont fontBold = wb.createFont();
			fontBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			estiloCabecalho.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			estiloCabecalho.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			fontBold.setFontHeightInPoints((short) 16); 
			estiloCabecalho.setFont( fontBold );
			estiloCabecalho.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			estiloCabecalho.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			
            //Cabe�alho  
			row = sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			cell.setCellValue(new HSSFRichTextString("Relat�rio Mensal de Pagamentos - "+ DateUtil.getFormattedDate(new Date(),"dd/MM/yyyy HH:mm:ss")));  
			cell.setCellStyle(estiloCabecalho);
			sheet.addMergedRegion(new CellRangeAddress(0,1,0,16));
			
            row = sheet.createRow(2);  
            
            HSSFFont fontCabecalho = wb.createFont();  
            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
            HSSFRichTextString richTextOrgao = new HSSFRichTextString("�rg�o");  
            richTextOrgao.applyFont(fontCabecalho);  
            HSSFRichTextString richTextSeguradoPrincipal = new HSSFRichTextString("Segurado Principal");  
            richTextSeguradoPrincipal.applyFont(fontCabecalho); 
            HSSFRichTextString richTextDataNascimento = new HSSFRichTextString("Data Nasc.Funcion�rio");  
            richTextDataNascimento.applyFont(fontCabecalho);  
            HSSFRichTextString richTextCPFFuncionario = new HSSFRichTextString("CPF Funcion�rio");  
            richTextCPFFuncionario.applyFont(fontCabecalho);
            HSSFRichTextString richTextEnderecoFuncionario = new HSSFRichTextString("Endere�o");  
            richTextEnderecoFuncionario.applyFont(fontCabecalho);
            HSSFRichTextString richTextCidadeFuncionario = new HSSFRichTextString("Cidade/Estado");  
            richTextCidadeFuncionario.applyFont(fontCabecalho);
            HSSFRichTextString richTextCepFuncionario = new HSSFRichTextString("Cidade/Estado");  
            richTextCepFuncionario.applyFont(fontCabecalho);
            HSSFRichTextString richTextPlano = new HSSFRichTextString("Plano");  
            richTextPlano.applyFont(fontCabecalho);  
            HSSFRichTextString richTextConjuge = new HSSFRichTextString("C�njuge");  
            richTextConjuge.applyFont(fontCabecalho);   
            HSSFRichTextString richTextDataNascimentoConjuge = new HSSFRichTextString("Data Nasc. C�njugue");  
            richTextDataNascimentoConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCPFConjuge = new HSSFRichTextString("CPF C�njugue");  
            richTextCPFConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCapitalFuncionario = new HSSFRichTextString("Capital Funcion�rio");  
            richTextCapitalFuncionario.applyFont(fontCabecalho);   
            HSSFRichTextString richTextCapitalConjuge = new HSSFRichTextString("Capital C�njuge");  
            richTextCapitalConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextValorFuncionario= new HSSFRichTextString("Valor Funcion�rio");  
            richTextValorFuncionario.applyFont(fontCabecalho);
            HSSFRichTextString richTextValorConjuge= new HSSFRichTextString("Valor C�njuge");  
            richTextValorConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextValorTotal= new HSSFRichTextString("Valor Total");  
            richTextValorTotal.applyFont(fontCabecalho);              
            HSSFRichTextString richTextMes= new HSSFRichTextString(filtroMes +"/" + filtroAno);  
            richTextMes.applyFont(fontCabecalho); 
//            HSSFRichTextString richTextAno= new HSSFRichTextString("Ano");  
//            richTextAno.applyFont(fontCabecalho); 
           
            int indice = 0;
            cell = row.createCell(indice++); cell.setCellValue(richTextOrgao); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextSeguradoPrincipal); cell.setCellStyle(estiloCabecalho); 
            cell = row.createCell(indice++); cell.setCellValue(richTextDataNascimento); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextCPFFuncionario); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextPlano);cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextConjuge); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextDataNascimentoConjuge);  cell.setCellStyle(estiloCabecalho);        
            cell = row.createCell(indice++); cell.setCellValue(richTextCPFConjuge); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextCapitalFuncionario); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextCapitalConjuge);    cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextValorFuncionario); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextValorConjuge);  cell.setCellStyle(estiloCabecalho);     
            cell = row.createCell(indice++); cell.setCellValue(richTextValorTotal);cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextMes);  cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextEnderecoFuncionario); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextCidadeFuncionario); cell.setCellStyle(estiloCabecalho);
            cell = row.createCell(indice++); cell.setCellValue(richTextCepFuncionario); cell.setCellStyle(estiloCabecalho);
            
//            cell = row.createCell(indice++); cell.setCellValue(richTextAno);  cell.setCellStyle(estiloCabecalho);
            
            int inicio = 3;
            for( PagamentoMensalDTO pgt : competenciasRetornadas){
            		indice = 0;
            	 	row = sheet.createRow(inicio);  
            	 	row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getOrgao() ));  
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getNomeSegurado() ));  
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getDataNascimento() ) );  
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getCpfSegurado()) );	                
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getTipoProposta()) ); 
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getNomeConjugue()) ); 
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getNascimentoConjugue() ) ); 
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getCpfConjugue() ) );
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( (pgt.getCapitalSegurado() != null?"R$ "+pgt.getCapitalSegurado().toString():"-")));
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( (pgt.getCapitalConjugue() != null?"R$ "+pgt.getCapitalConjugue().toString():"-")));
	                row.createCell(indice++).setCellValue( new HSSFRichTextString (pgt.getValorFuncionario() != null?"R$ "+pgt.getValorFuncionario().toString():"-"));
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getValorConjugue() != null?"R$ "+pgt.getValorConjugue().toString():"-"));
	                row.createCell(indice++).setCellValue( pgt.getValorTotal());
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getQuitacao()));
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getEnderecoSegurado()) );
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getCidadeEstadoSegurado()) );
	                row.createCell(indice++).setCellValue( new HSSFRichTextString( pgt.getCep()));
	                
	                
//	                row.createCell(indice++).setCellValue( pgt.getAno());
	                
	                row.getCell(12).setCellStyle( estiloMonetario );
	                
	                inicio++;
            }
          
            HSSFRow header = sheet.getRow(2); 
		    for(short column=0;column<header.getLastCellNum(); column++){
		    	sheet.autoSizeColumn(column);
		    }    
		    
            HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();  
            res.setContentType("application/vnd.ms-excel");  
            res.setHeader("Content-disposition",  "attachment; filename=RelatorioMensalPagamentos.xls");  
 
            
           try {  
                 ServletOutputStream out = res.getOutputStream();  
                 wb.write(out);  
                 out.flush();  
                 out.close();  
           } catch (IOException ex) {   
                   ex.printStackTrace();  
           }  
           
           FacesContext faces = FacesContext.getCurrentInstance();  
           faces.responseComplete();  
  
              
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        
        return "export";
		
	}
		
		
		//==============================================================================Getters and Setters
		

		public List<SelectItem> getOpcoesEmpresaCombo() {
			return opcoesEmpresaCombo;
		}


		public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
			this.opcoesEmpresaCombo = opcoesEmpresaCombo;
		}


		public Integer getEmpresaId() {
			return empresaId;
		}


		public void setEmpresaId(Integer empresaId) {
			this.empresaId = empresaId;
		}


		public String getNomeSegurado() {
			return nomeSegurado;
		}


		public void setNomeSegurado(String nomeSegurado) {
			this.nomeSegurado = nomeSegurado;
		}


		public String getFiltroMes() {
			return filtroMes;
		}


		public void setFiltroMes(String filtroMesAnoInicio) {
			this.filtroMes = filtroMesAnoInicio;
		}


		public String getFiltroAno() {
			return filtroAno;
		}


		public void setFiltroAno(String filtroMesAnoFim) {
			this.filtroAno = filtroMesAnoFim;
		}

		public List<PagamentoMensalDTO> getCompetenciasRetornadas() {
			return competenciasRetornadas;
		}

		public void setCompetenciasRetornadas(
				List<PagamentoMensalDTO> competenciasRetornadas) {
			this.competenciasRetornadas = competenciasRetornadas;
		}

		public String getCpfSegurado() {
			return cpfSegurado;
		}

		public void setCpfSegurado(String cpfSegurado) {
			this.cpfSegurado = cpfSegurado;
		}
		
		public List<String> getSituacaoPropostaSelectedList() {
			return situacaoPropostaSelectedList;
		}

		public void setSituacaoPropostaSelectedList(
				List<String> situacaoPropostaSelectedList) {
			this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
		}
		
		public List<SelectItem> getOpcoesSituacaoPropostaCmb(){
			
			List<SelectItem> opcoesSituacaoPropostaCombo = new ArrayList<SelectItem>();
			for(SituacaoProposta situacao : SituacaoProposta.values()) {
				opcoesSituacaoPropostaCombo.add( new SelectItem( situacao.getId(), situacao.getNome()));
			}
			return opcoesSituacaoPropostaCombo;
			
		}

		@Override
		public void criarCabecalho(HSSFWorkbook wb, String aba) {
			int indiceLinha = 0;
			addRow(aba);
			addTextoMesclado(aba, getStiloTextoTitulo(), "Formul�rio de Movimenta��o - Vida - " + filtroMes + "/" + filtroAno, indiceLinha, 1, 0, 11);
			addRow(aba);
			indiceLinha++;
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoTitulo(), "Dados da Empresa", indiceLinha, 1, 0, 9);
			addTextoMesclado(aba, getStiloTextoTitulo(), "Dados da Ap�lice", indiceLinha, 1, 10, 11	);
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoSimples(), "*Raz�o Social/Estipulante: ITAVIDA SEGUROS", indiceLinha, 1, 0, 9);
			addTextoMesclado(aba, getStiloTextoSimples(), "*Compet�ncia Fatura:", indiceLinha, 1, 10, 10 );
			addTextoMesclado(aba, getStiloTextoSimples(), "",  indiceLinha, 1, 11, 11);
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoSimples(), getCNPJ(aba), indiceLinha, 1, 0, 8);
			addTextoMesclado(aba, getStiloTextoSimples(), "*Sub-Estipulante",  indiceLinha, 1, 9, 9);
			addTextoMesclado(aba, getStiloTextoSimples(), "*N� Ap�lice",  indiceLinha, 1, 10, 10);
			addTextoMesclado(aba, getStiloTextoSimples(), getNumeroApolice(aba),  indiceLinha, 1, 11, 11);
			addRow(aba);
			indiceLinha++;
			addTextoMesclado(aba, getStiloTextoSimples(), "Contato:", indiceLinha, 1, 0, 8);
			addTextoMesclado(aba, getStiloTextoSimples(), "Telefone: (31)3274-5170",  indiceLinha, 1, 9, 9);
			addTextoMesclado(aba, getStiloTextoSimples(), "*N� Sub Estipulante",  indiceLinha, 1, 10, 10);
			addTextoMesclado(aba, getStiloTextoSimples(), "",  indiceLinha, 1, 11, 11);
			addRow(aba);
			
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","orgao", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","nome-segurado-principal", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","plano", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-conjuge", null));		
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-segurado", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-conjuge", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-total", null));
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Situa��o");
			//endere�o
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Endere�o");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Cidade/Estado");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "CEP");
			
		}

		@Override
		public void preencherAba(HSSFWorkbook wb, String aba) {
			for( PagamentoMensalDTO pgt : competenciasRetornadas) {
	       		addRow(aba);
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getOrgao() !=null ? pgt.getOrgao() : null, "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getNomeSegurado(), "-");
	       		addCelulaTexto(aba, getStiloDataSimples(), pgt.getDataNascimento(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(pgt.getCpfSegurado()), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getTipoProposta());
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getNomeConjugue(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getNascimentoConjugue(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(pgt.getCpfConjugue()), "-");
	       		
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), pgt.getCapitalSegurado(), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), pgt.getCapitalConjugue(), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), pgt.getValorFuncionario(), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), pgt.getValorConjugue(), "-");
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), pgt.getValorTotal(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getQuitacao());
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getEnderecoSegurado());
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getCidadeEstadoSegurado());
	       		addCelulaTexto(aba, getStiloTextoSimples(), pgt.getCep());
                
	   			
	       	}
			
		}
}
