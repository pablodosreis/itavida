package br.com.itavida.handlers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.MovimentacaoFinanceiraDAO;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.MovimentacaoFinanceira;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.PropostaAgenciador;
import br.com.itavida.enums.RegiaoSegurado;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.Contexto;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;
import br.com.itavida.util.NumberUtil;

public class RelatorioExtratoSuperApHandler extends ExcelGenerateHandler {
	
	private static final long serialVersionUID = 1L;
	
	private MovimentacaoFinanceiraDAO movimentacaoDAO = Contexto.getMovimentacaoFinanceiraDAO();
	
	private Map<String, List<MovimentacaoFinanceira>> mapaMovimentacao;

	private Integer empresaSelected;
	
	private Integer ufSelected;
	
	private Integer situacaoPropostaSelected;
	
	private Integer modeloPropostaSelected;
	
	private List<String> situacaoPropostaSelectedList = new ArrayList<String>();
	
	private String profissao;
	
	private Integer mesAniversario;
	
	private Date dataAverbacao;
	
	private Date inicio;
	
	private Date fim;
	
	private List<SelectItem> opcoesUfCombo;
	
	private List<SelectItem> opcoesSituacaoPropostaCombo;
	
	private Proposta proposta;
	
	
	public RelatorioExtratoSuperApHandler() {
		limparDados();
	}
	
	@PostConstruct
	public void init() {
		//limparDados();
	}

	private void limparDados() {
		empresaSelected = null;
		ufSelected = null;
		situacaoPropostaSelected = -1;
		dataAverbacao = null;
		populaOpcoesSituacaoPropostaCombo();
		populaOpcoesMesesCombo(true);
		setProposta(new Proposta());
	}
	
	public void gerarRelatorio() {
		
		if (inicio == null) {
			addWarnMessage( "O campo inicio deve ser informado" );
		}
		
		if (fim == null) {
			addWarnMessage( "O campo fim deve ser informado" );			
		}
		
		if (inicio == null || fim == null) {
			return;
		}
		
		this.mapaMovimentacao = getMapMovimentacao();
		if(this.mapaMovimentacao == null || this.mapaMovimentacao.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		super.gerarRelatorio(this.mapaMovimentacao.keySet(), 2, "ExtratoSuperAp.xls");
	}
	

	public Map<String, List<MovimentacaoFinanceira>> getMapMovimentacao() {
		List<SituacaoProposta> lista = new ArrayList<SituacaoProposta>();
		for (String s : situacaoPropostaSelectedList) {
			lista.add(SituacaoProposta.get(Integer.valueOf(s)));
		}
		Integer idProposta = proposta != null ? proposta.getId() : null;
		return movimentacaoDAO.buscarMovimentacoesPorSituacao(inicio, fim, null, idProposta, lista);
	}

	
	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		
		
		int indiceLinha = 0;
		//Cabe�alho
		addRow(aba);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Extrato Super AP", indiceLinha, 1, 0, 10);
		
		
		addRow(aba);
		indiceLinha++;
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Agenciador");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Segurado");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Cadastro");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Averba��o");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Plano");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Vencimento");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Forma de Pgto");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Parcela");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Valor Pago");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Dt. Pagamento");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Valor Comiss�o");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Dt. Comiss�o");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Dt. Previs�o Boleto");
		
		
	}

	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		
		
       	for(MovimentacaoFinanceira movimentacao : mapaMovimentacao.get(aba)) {
       			Proposta proposta = movimentacao.getProposta();
	       		addRow(aba);
	       	
	       		
	       		Double valorComissao;
	       		String plano;
	       		if (proposta.getSuperAp().getPeriodicidade().equals("0")) {
	       			valorComissao = NumberUtil.round(proposta.getValorPremioSegurado()/2, 2);
	       			plano = "MENSAL";
	       		} else {
	       			valorComissao = NumberUtil.round(proposta.getValorPremioSegurado()/10, 2);
	       			plano = "ANUAL";
	       		}
	       		
	       		String agenciador = null;
	       		List<PropostaAgenciador> listaAgenciadores = new ArrayList<PropostaAgenciador>(proposta.getPropostaagenciador_collection());
				if (listaAgenciadores != null && !listaAgenciadores.isEmpty()) {
	       			agenciador = listaAgenciadores.get(0).getFuncionario().getNome();
	       		}
	      
				
				
	       		addCelulaTexto(aba, getStiloTextoSimples(), agenciador, "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getNomeSegurado());
	       		
	       		addCelulaData(aba, getStiloDataSimples(), proposta.getDataCadastro(), "-");
	       		addCelulaData(aba, getStiloTextoSimples(), proposta.getDataAprovacao(), "-");
	       		addCelulaTexto(aba, getStiloTextoSimples(), plano, "-");
	       		
	       		addCelulaNumero(aba, getStiloTextoSimples(), proposta.getSuperAp().getDiaVencimento().doubleValue(), "-");
	       		String formaPagamento = proposta.getFormaPagamento() != null && proposta.getFormaPagamento().getTipo() != null ? proposta.getFormaPagamento().getTipo().getNome() : null;
				addCelulaTexto(aba, getStiloTextoSimples(), formaPagamento, "-");
				
				String numeroParcela = movimentacao.getNumeroParcela() != null ? movimentacao.getNumeroParcela().toString() : null;;
				addCelulaTexto(aba, getStiloTextoSimples(), numeroParcela, "-");  //parcela
				
				addCelulaNumero(aba, getStiloMonetarioSimples(), proposta.getValorPremioSegurado(), "-");
				addCelulaData(aba, getStiloDataSimples(), movimentacao.getDataPagamento(), "-"); // data do pagamento
				addCelulaNumero(aba, getStiloMonetarioSimples(), movimentacao.getValorComissao());
	       		addCelulaData(aba, getStiloTextoSimples(), movimentacao.getDataPagamentoComissao(), "-"); //cr�dito comiss�o itavida
	       		addCelulaData(aba, getStiloDataSimples(), movimentacao.getDataPrevisaoBoleto(), "-");
	       		
	       		
	       		
	       		
	       		
       	}
	}

	

	
	/**
	 * Popula um combo de situa��es da proposta.
	 *  
	 */
	public void populaOpcoesSituacaoPropostaCombo(){
		
		opcoesSituacaoPropostaCombo = new ArrayList<SelectItem>();
		for(SituacaoProposta situacao : SituacaoProposta.values()) {
			opcoesSituacaoPropostaCombo.add( new SelectItem( situacao.getId(), situacao.getNome()));
		}
		
	}
	
	
	/**
	 * Popula um combo de UF
	 */
	public void populaOpcoesUfCombo() {
		
		Session session = HibernateUtil.currentSession();
		CidadeDAO cidadeDAO = new CidadeDAO(session);
		
		List<Estado> estados = cidadeDAO.obterTodosEstados();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (Estado estado : estados) {
			opcoesUfCombo.add( new SelectItem( estado.getCod_estado(), estado.getNom_estado()) );
		}
		
	}
	
	/**
	 * Popula um combo com os tipos de UF existentes no sistema.
	 */
	public void populaOpcoesRegiaoSeguradoCombo() {
		//Busca os valores de regi�o do enum RegiaoSegurado
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (RegiaoSegurado regiao : RegiaoSegurado.values()) {
			opcoesUfCombo.add( new SelectItem( regiao.getId(), regiao.getNome()));
		}
	}
	
	public void cancelar(ActionEvent e) {
		limparDados();
	}
	
	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public Integer getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Integer empresaSelected) {
		this.empresaSelected = empresaSelected;
	}

	public Date getDataAverbacao() {
		return dataAverbacao;
	}

	public void setDataAverbacao(Date dataAverbacao) {
		this.dataAverbacao = dataAverbacao;
	}

	
	public Integer getSituacaoPropostaSelected() {
		return situacaoPropostaSelected;
	}

	public void setSituacaoPropostaSelected(Integer tipoConsultaSelected) {
		this.situacaoPropostaSelected = tipoConsultaSelected;
	}

	public Integer getModeloPropostaSelected() {
		return modeloPropostaSelected;
	}

	public void setModeloPropostaSelected(Integer modeloPropostaSelected) {
		this.modeloPropostaSelected = modeloPropostaSelected;
	}


	
	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public List<SelectItem> getOpcoesUfCombo() {
		return opcoesUfCombo;
	}

	public void setOpcoesUfCombo(List<SelectItem> opcoesUfCombo) {
		this.opcoesUfCombo = opcoesUfCombo;
	}

	public Integer getUfSelected() {
		return ufSelected;
	}

	public void setUfSelected(Integer ufSelected) {
		this.ufSelected = ufSelected;
	}

	public List<SelectItem> getOpcoesSituacaoPropostaCombo() {
		return opcoesSituacaoPropostaCombo;
	}

	public void setOpcoesSituacaoPropostaCombo(List<SelectItem> opcoesTipoConsultaCombo) {
		this.opcoesSituacaoPropostaCombo = opcoesTipoConsultaCombo;
	}

	public List<String> getSituacaoPropostaSelectedList() {
		return situacaoPropostaSelectedList;
	}

	public void setSituacaoPropostaSelectedList(
			List<String> situacaoPropostaSelectedList) {
		this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public Integer getMesAniversario() {
		return mesAniversario;
	}

	public void setMesAniversario(Integer mesAniversario) {
		this.mesAniversario = mesAniversario;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}


}
