package br.com.itavida.handlers;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.HistoricoSinistroDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.BeneficiarioSinistro;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.HistoricoSinistro;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.TipoOcorrencia;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;
import br.com.itavida.util.NumberUtil;

public class HistoricoSinistroHandler extends ExcelGenerateHandler{

	private static final long serialVersionUID = 1L;
	private HistoricoSinistro historicoSinistro;
	private Empresa empresa;
	private Integer empresaSelected;
	private Map<Integer, List<HistoricoSinistro>> resultadoPesquisa;
	private Integer numeroPropostaParameter;
	private String situacaoProcesso;
	private String observacao;
	private boolean tipoSinistroAuxilioFuneral;
	private boolean tipoSinistroConjuge;
	private boolean tipoSinistroSegurado;
	private List<HistoricoSinistro> listaHistoricoSinistro;
	private BeneficiarioSinistro b1, b2, b3, b4, b5, b6, b7, b8, b9, b10;
	private List<String> ajustes;
	


	public List<String> getAjustes() {
		return ajustes;
	}

	public void setAjustes(List<String> ajustes) {
		this.ajustes = ajustes;
	}

	/**
	 * IMPORTANTE: N�O REMOVER ESSE GETTER
	 * @return the keySetResultadoPesquisa
	 */
	public List<Integer> getKeySetResultadoPesquisa() {
		if( getResultadoPesquisa()!= null && getResultadoPesquisa().keySet() != null ){
			 
			List<Integer> listaKeys = new ArrayList<Integer>();
			for (Iterator iterator = getResultadoPesquisa().keySet().iterator(); iterator.hasNext();) {
				Integer key = (Integer) iterator.next();
				listaKeys.add( key );
			}
			Collections.sort( listaKeys );
			return listaKeys ;
		}else{
			return null;
		}
	}

	// Fim Getters and Setters
	

	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public HistoricoSinistroHandler(){
		HttpServletRequest request = ( HttpServletRequest ) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Object idProposta = request.getSession().getAttribute( "idProposta" );
		if( idProposta != null  ){
			setNumeroPropostaParameter( (Integer) idProposta );
			preparaIncluir();
			request.getSession().removeAttribute( "idProposta" );
		}else{
			cancelar( null );
		}
	}
	

	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 * @throws UnsupportedEncodingException 
	 */
	public void pesquisaHistoricoSinistros(ActionEvent e) throws UnsupportedEncodingException{
		
		Session session = HibernateUtil.currentSession();
		HistoricoSinistroDAO dao = new HistoricoSinistroDAO();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		EmpresaDAO empresaDAO = new EmpresaDAO(session);
		
		Empresa empresa = empresaDAO.getEmpresa(empresaSelected);
		historicoSinistro.getProposta().setEmpresa(empresa);
		
		HistoricoSinistro historicoSinistroFiltro = getHistoricoSinistro();
		
		List<HistoricoSinistro> lista = dao.buscaPorFiltro( historicoSinistroFiltro  );
		Map<Integer, List<HistoricoSinistro>> mapa = new HashMap<Integer, List<HistoricoSinistro>>();

		

		
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			HistoricoSinistro historicoSinistro = (HistoricoSinistro) iterator.next();
			if( historicoSinistro.getObservacao() != null ){
				historicoSinistro.setObservacaoString( new String(historicoSinistro.getObservacao(), "ISO-8859-1") );
			}
			if( historicoSinistro.getSituacaoProcesso() != null ){
				historicoSinistro.setSituacaoProcessoString( new String(historicoSinistro.getSituacaoProcesso(), "ISO-8859-1") );
			}
			
			Proposta proposta = historicoSinistro.getProposta();
			propostaDAO.populaCapital( proposta );	
			historicoSinistro.setBeneficiarios(historicoSinistro.getBeneficiarios());
			for (int i = 0; i < historicoSinistro.getBeneficiarios().size(); i++) {
				historicoSinistro.getBeneficiarios().get(i);
				
				
			}
			
			
		}
		setListaHistoricoSinistro(lista);
		
	}
	
	public String getTextoCodificado(byte[] conteudo, boolean removerBr) {
		try { 
			String string = new String(conteudo,"ISO-8859-1");
			if (removerBr) {
				string = string.replaceAll("<br>", " \n");
				string = string.replaceAll("&nbsp;", " ");
			}
			return string;
		} catch (Exception e) {
			return "";
		}
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 * @throws UnsupportedEncodingException 
	 */
	public String relatorioHistoricoSinistros( ) {
		
		List<HistoricoSinistro> lista = getListaHistoricoRelatorio();

		setListaHistoricoSinistro(lista);
		
		return "resultado";
	}

	private List<HistoricoSinistro> getListaHistoricoRelatorio() {
		Session session = HibernateUtil.currentSession();
		HistoricoSinistroDAO dao = new HistoricoSinistroDAO();
		PropostaDAO propostaDAO = new PropostaDAO(session);

		
		EmpresaDAO empresaDAO = new EmpresaDAO(session);
		
		Empresa empresa = empresaDAO.getEmpresa(empresaSelected);
		historicoSinistro.getProposta().setEmpresa(empresa);
		historicoSinistro.setEmpresa(empresa);
		
		List<HistoricoSinistro> lista = dao.buscaPorFiltro( historicoSinistro  );
		
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			HistoricoSinistro historicoSinistro = (HistoricoSinistro) iterator.next();
			if( historicoSinistro.getObservacao() != null ){
				historicoSinistro.setObservacaoString( getTextoCodificado(historicoSinistro.getObservacao(), false) );
			}
			if( historicoSinistro.getSituacaoProcesso() != null ){
				historicoSinistro.setSituacaoProcessoString( getTextoCodificado(historicoSinistro.getSituacaoProcesso(), false) );
			}
			
			Proposta proposta = historicoSinistro.getProposta();
			propostaDAO.populaCapital( proposta );	
			double totalPago = 0.0;
			for(BeneficiarioSinistro beneficiario : historicoSinistro.getBeneficiarios()) {
				totalPago += beneficiario.getValorRecebido();
			}
			historicoSinistro.setValorPagoSinistro( totalPago );
			if(  historicoSinistro.getMotivoSinistro() != null && historicoSinistro.getMotivoSinistro().getId() == 4  ){
				historicoSinistro.setValorAuxFuneral( 3000.0 );
			}else{
				historicoSinistro.setValorAuxFuneral( 1500.0 );
			}
			
		}
		
		cancelar(null);
		return lista;
	}
	
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("dtDataSinistro", "Data do Sinistro");
		mapaCampos.put("txtMotivoSinistro", "Motivo do Sinistro");
		
		if (b1.getNomeBeneficiario() != null && !b1.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario1", "Valor Recebido 1");	
		}
		if (b2.getNomeBeneficiario() != null && !b2.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario2", "valor Recebido 2");	
		}
		if (b3.getNomeBeneficiario() != null && !b3.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario3", "valor Recebido 3");	
		}
		if (b4.getNomeBeneficiario() != null && !b4.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario4", "valor Recebido 4");	
		}
		if (b5.getNomeBeneficiario() != null && !b5.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario5", "valor Recebido 5");	
		}
		if (b6.getNomeBeneficiario() != null && !b6.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario6", "valor Recebido 6");	
		}
		if (b7.getNomeBeneficiario() != null && !b7.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario7", "valor Recebido 7");	
		}
		if (b8.getNomeBeneficiario() != null && !b8.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario8", "valor Recebido 8");	
		}
		if (b9.getNomeBeneficiario() != null && !b9.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario9", "valor Recebido 9");	
		}
		if (b10.getNomeBeneficiario() != null && !b10.getNomeBeneficiario().trim().equals("")) {
			mapaCampos.put("valorBeneficiario10", "valor Recebido 10");	
		}
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	

	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 * @throws UnsupportedEncodingException 
	 */
	public void excluirHistoricoSinistro(ActionEvent e) throws UnsupportedEncodingException{
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirHistoricoSinistro");
		HistoricoSinistro historicoSinistroDeletado  = ( HistoricoSinistro ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<HistoricoSinistro, Integer> dao = new Dao<HistoricoSinistro, Integer>(session, HistoricoSinistro.class);
		dao.delete(historicoSinistroDeletado);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaHistoricoSinistros(e);
	}	
	
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getTipoPagamentoCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "R", "Reembolso Funeral" ) );
		retorno.add( new SelectItem( "A", "Assist�ncia Funeral" ) );
		
		return retorno;
	}	
	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setHistoricoSinistro( new HistoricoSinistro() );
		getHistoricoSinistro().setMotivoSinistro( new TipoOcorrencia() );
		getHistoricoSinistro().setMotivoSinistroNegado( new TipoOcorrencia() );
		getHistoricoSinistro().setProposta( new Proposta() );
		getHistoricoSinistro().getProposta().setOrgao( new Orgao() );
		getHistoricoSinistro().getProposta().setCidade(new Cidade());
		this.situacaoProcesso = null;
		setB1(new BeneficiarioSinistro());
		setB2(new BeneficiarioSinistro());
		setB3(new BeneficiarioSinistro());
		setB4(new BeneficiarioSinistro());
		setB5(new BeneficiarioSinistro());
		setB6(new BeneficiarioSinistro());
		setB7(new BeneficiarioSinistro());
		setB8(new BeneficiarioSinistro());
		setB9(new BeneficiarioSinistro());
		setB10(new BeneficiarioSinistro());
		this.observacao = null;
		this.tipoSinistroAuxilioFuneral = false;
		this.tipoSinistroConjuge = false;
		this.tipoSinistroSegurado = false;
		
		this.resultadoPesquisa = null;
		setEmpresaSelected(null);
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public String cancelarInclusao(  ){
		
		// Realiza o cancelamento das a��es
		cancelar( null );
		
		return "consultarSinistro";
		
	}
	
	// Fluxos de INCLUS�O / EXCLUS�O / ALTERA��O
	
	public String preparaIncluir( ){
		cancelar(null);
		Session session = HibernateUtil.currentSession();
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>( session, Proposta.class );
		Proposta proposta = daoProposta.get( getNumeroPropostaParameter() );
		getHistoricoSinistro().setProposta( proposta );
		
		return "incluirSinistro";
	}
	
	
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 * @throws UnsupportedEncodingException 
	 */
	public String preparaAlterarHistoricoSinistro( ) throws UnsupportedEncodingException{
		
		if( historicoSinistro.getObservacao() != null ){
			setObservacao( new String(historicoSinistro.getObservacao(), "ISO-8859-1") );
		}
		if( historicoSinistro.getSituacaoProcesso() != null ){
			setSituacaoProcesso( new String(historicoSinistro.getSituacaoProcesso(), "ISO-8859-1") );
		}
		
		
		if( Constantes.SIM.equals( historicoSinistro.getTipoSinistroAuxilioFuneral() ) ){
			tipoSinistroAuxilioFuneral = true;
		}else{			
			tipoSinistroAuxilioFuneral = false;
		}
		
		if( Constantes.SIM.equals( historicoSinistro.getTipoSinistroConjuge() ) ){
			tipoSinistroConjuge = true;
		}else{				
			tipoSinistroConjuge = false;
		}
		
		if( Constantes.SIM.equals( historicoSinistro.getTipoSinistroSegurado() ) ){
			tipoSinistroSegurado = true;
		}else{			
			tipoSinistroSegurado = false;
		}		
		
		if( historicoSinistro.getMotivoSinistroNegado() == null ){
			historicoSinistro.setMotivoSinistroNegado( new TipoOcorrencia() );
		}
		
		if (historicoSinistro.getBeneficiarios() != null) {
			if (historicoSinistro.getBeneficiarios().size() >= 1 && historicoSinistro.getBeneficiarios().get(0) != null) {
				setB1(historicoSinistro.getBeneficiarios().get(0));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 2 && historicoSinistro.getBeneficiarios().get(1) != null) {
				setB2(historicoSinistro.getBeneficiarios().get(1));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 3 && historicoSinistro.getBeneficiarios().get(2) != null) {
				setB3(historicoSinistro.getBeneficiarios().get(2));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 4 && historicoSinistro.getBeneficiarios().get(3) != null) {
				setB4(historicoSinistro.getBeneficiarios().get(3));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 5 && historicoSinistro.getBeneficiarios().get(4) != null) {
				setB5(historicoSinistro.getBeneficiarios().get(4));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 6 && historicoSinistro.getBeneficiarios().get(5) != null) {
				setB6(historicoSinistro.getBeneficiarios().get(5));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 7 && historicoSinistro.getBeneficiarios().get(6) != null) {
				setB7(historicoSinistro.getBeneficiarios().get(6));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 8 && historicoSinistro.getBeneficiarios().get(7) != null) {
				setB8(historicoSinistro.getBeneficiarios().get(7));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 9 && historicoSinistro.getBeneficiarios().get(8) != null) {
				setB9(historicoSinistro.getBeneficiarios().get(8));
			}
			if (historicoSinistro.getBeneficiarios().size() >= 10 && historicoSinistro.getBeneficiarios().get(9) != null) {
				setB10(historicoSinistro.getBeneficiarios().get(9));

			}
		}
		
		
		resultadoPesquisa = null;
		
		return "incluirSinistro";
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 * @throws UnsupportedEncodingException 
	 */
	public void gravar( ActionEvent e ) throws UnsupportedEncodingException{
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		
		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao(getHistoricoSinistro().getMotivoSinistro().getId(), "Motivo Sinistro") ){
		
			Dao<HistoricoSinistro, Integer> dao = new Dao<HistoricoSinistro, Integer>(session, HistoricoSinistro.class);
			Dao<BeneficiarioSinistro, Integer> dao2 = new Dao<BeneficiarioSinistro, Integer>(session, BeneficiarioSinistro.class);
			
			if( getHistoricoSinistro().getMotivoSinistroNegado() != null && getHistoricoSinistro().getMotivoSinistroNegado().getId() == null ){
				getHistoricoSinistro().setMotivoSinistroNegado( null );
			}
			
			HistoricoSinistro historicoSinistroGravar = getHistoricoSinistro();
			
			historicoSinistroGravar.setEmpresa(historicoSinistroGravar.getProposta().getEmpresa());
			
			
			if( tipoSinistroAuxilioFuneral ){
				historicoSinistroGravar.setTipoSinistroAuxilioFuneral( Constantes.SIM );
			}else{			
				historicoSinistroGravar.setTipoSinistroAuxilioFuneral( Constantes.NAO );
			}	
			
			if( tipoSinistroConjuge ){
				historicoSinistroGravar.setTipoSinistroConjuge( Constantes.SIM );
			}else{			
				historicoSinistroGravar.setTipoSinistroConjuge( Constantes.NAO );
			}	
			
			if( tipoSinistroSegurado ){
				historicoSinistroGravar.setTipoSinistroSegurado( Constantes.SIM );
			}else{			
				historicoSinistroGravar.setTipoSinistroSegurado( Constantes.NAO );
			}	
			
			if ( getSituacaoProcesso() != null ){
				historicoSinistroGravar.setSituacaoProcesso( getSituacaoProcesso().getBytes("ISO_8859-1") );
			}
			if( getObservacao() != null ){
				historicoSinistroGravar.setObservacao( getObservacao().getBytes("ISO_8859-1") );
			}
			if(historicoSinistroGravar.getId() == null) {
				dao.save( historicoSinistroGravar );
			} else {
				dao.merge( historicoSinistroGravar );
			}
			session.flush();
			if (b1.getNomeBeneficiario() != null && !b1.getNomeBeneficiario().trim().equals("")){
				b1.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b1);
				
			}
			if (b2.getNomeBeneficiario() != null && !b2.getNomeBeneficiario().trim().equals("")){
				b2.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b2);
			}
			if (b3.getNomeBeneficiario() != null && !b3.getNomeBeneficiario().trim().equals("")){
				b3.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b3);
			}
			if (b4.getNomeBeneficiario() != null && !b4.getNomeBeneficiario().trim().equals("")){
				b4.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b4);
			}
			if (b5.getNomeBeneficiario() != null && !b5.getNomeBeneficiario().trim().equals("")){
				b5.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b5);
			}
			if (b6.getNomeBeneficiario() != null && !b6.getNomeBeneficiario().trim().equals("")){
				b6.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b6);
			}
			if (b7.getNomeBeneficiario() != null && !b7.getNomeBeneficiario().trim().equals("")){
				b7.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b7);
			}
			if (b8.getNomeBeneficiario() != null && !b8.getNomeBeneficiario().trim().equals("")){
				b8.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b8);
			}
			if (b9.getNomeBeneficiario() != null && !b9.getNomeBeneficiario().trim().equals("")){
				b9.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b9);
			}
			if (b10.getNomeBeneficiario() != null && !b10.getNomeBeneficiario().trim().equals("")){
				b10.setHistoricoSinistro(historicoSinistroGravar);
				dao2.merge(b10);
			}
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			cancelarInclusao();
			
		}
		
	}
	
	// Getters and Setters
	

	public HistoricoSinistro getHistoricoSinistro() {
		return historicoSinistro;
	}


	public void setHistoricoSinistro(HistoricoSinistro historicoSinistro) {
		this.historicoSinistro = historicoSinistro;
	}

	/**
	 * @return the resultadoPesquisa
	 */
	public Map<Integer, List<HistoricoSinistro>> getResultadoPesquisa() {
		return resultadoPesquisa;
	}


	/**
	 * @param resultadoPesquisa the resultadoPesquisa to set
	 */
	public void setResultadoPesquisa(
			Map<Integer, List<HistoricoSinistro>> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}


	public Integer getNumeroPropostaParameter() {
		return numeroPropostaParameter;
	}


	public void setNumeroPropostaParameter(Integer numeroPropostaParameter) {
		this.numeroPropostaParameter = numeroPropostaParameter;
	}

	




	public String getSituacaoProcesso() {
		return situacaoProcesso;
	}


	public void setSituacaoProcesso(String situacaoProcesso) {
		this.situacaoProcesso = situacaoProcesso;
	}


	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public boolean isTipoSinistroAuxilioFuneral() {
		return tipoSinistroAuxilioFuneral;
	}


	public void setTipoSinistroAuxilioFuneral(boolean tipoSinistroAuxilioFuneral) {
		this.tipoSinistroAuxilioFuneral = tipoSinistroAuxilioFuneral;
	}


	public boolean isTipoSinistroConjuge() {
		return tipoSinistroConjuge;
	}


	public void setTipoSinistroConjuge(boolean tipoSinistroConjuge) {
		this.tipoSinistroConjuge = tipoSinistroConjuge;
	}


	public boolean isTipoSinistroSegurado() {
		return tipoSinistroSegurado;
	}


	public void setTipoSinistroSegurado(boolean tipoSinistroSegurado) {
		this.tipoSinistroSegurado = tipoSinistroSegurado;
	}

	public List<HistoricoSinistro> getListaHistoricoSinistro() {
		return listaHistoricoSinistro;
	}

	public void setListaHistoricoSinistro(
			List<HistoricoSinistro> listaHistoricoSinistro) {
		this.listaHistoricoSinistro = listaHistoricoSinistro;
	}


	public BeneficiarioSinistro getB1() {
		return b1;
	}

	public void setB1(BeneficiarioSinistro b1) {
		this.b1 = b1;
	}

	public BeneficiarioSinistro getB2() {
		return b2;
	}

	public void setB2(BeneficiarioSinistro b2) {
		this.b2 = b2;
	}

	public BeneficiarioSinistro getB3() {
		return b3;
	}

	public void setB3(BeneficiarioSinistro b3) {
		this.b3 = b3;
	}

	public BeneficiarioSinistro getB4() {
		return b4;
	}

	public void setB4(BeneficiarioSinistro b4) {
		this.b4 = b4;
	}

	public BeneficiarioSinistro getB5() {
		return b5;
	}

	public void setB5(BeneficiarioSinistro b5) {
		this.b5 = b5;
	}

	public BeneficiarioSinistro getB6() {
		return b6;
	}

	public void setB6(BeneficiarioSinistro b6) {
		this.b6 = b6;
	}

	public BeneficiarioSinistro getB7() {
		return b7;
	}

	public void setB7(BeneficiarioSinistro b7) {
		this.b7 = b7;
	}

	public BeneficiarioSinistro getB8() {
		return b8;
	}

	public void setB8(BeneficiarioSinistro b8) {
		this.b8 = b8;
	}

	public BeneficiarioSinistro getB9() {
		return b9;
	}

	public void setB9(BeneficiarioSinistro b9) {
		this.b9 = b9;
	}

	public BeneficiarioSinistro getB10() {
		return b10;
	}

	public void setB10(BeneficiarioSinistro b10) {
		this.b10 = b10;
	}
	
	public void gerarRelatorioExcel() {
		List<String> abas = new ArrayList<String>();
		abas.add("Sinistros");
		List<HistoricoSinistro> lista = getListaHistoricoRelatorio();
		setListaHistoricoSinistro(lista);
		if(lista == null || lista.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		super.gerarRelatorio(abas, 3, "relatorioSinistros.xls");
	}

	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String agora = format.format(new Date());
		int indiceLinha = 0;
		addRow(aba);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Relat�rio de Sinistros - Itavida Seguros - " + agora , indiceLinha, 1, 0, 18);
		addRow(aba);
		addCelulaTexto(aba, getStiloTextoTitulo(), "Proposta");
		addCelulaTexto(aba, getStiloTextoTitulo(), "N�mero do Sinistro");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Data do Sinistro");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Data do Envio p/ Seguradora");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Data do Pagamento");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Ap�lice");
		addCelulaTexto(aba, getStiloTextoTitulo(), "�rg�o");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Nome do Segurado Principal");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Nome do Segurado C�njuge");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Motivo do Sinistro");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Motivo do Sinistro Negado");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Capital Funcion�rio");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Valor Pago Sinistro");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Valor Pago Cesta B�sica");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Valor Pago Funeral");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Seguradora");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Situa��o do Processo");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Observa��es");
		addCelulaTexto(aba, getStiloTextoTitulo(), "Benefici�rios");
		
	}

	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		List<HistoricoSinistro> lista = getListaHistoricoRelatorio();
		for (HistoricoSinistro historicoSinistro : lista) {
			addRow(aba);
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getProposta().getId().toString(), "");
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getNumeroSinistro(), "");
			addCelulaData(aba, getStiloTextoSimples(), historicoSinistro.getDataSinistro(), "-");
			addCelulaData(aba, getStiloTextoSimples(), historicoSinistro.getDataEnvioSeguradora(), "-");
			addCelulaData(aba, getStiloTextoSimples(), historicoSinistro.getDataPagamento(), "-");
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getEmpresa() != null ? historicoSinistro.getEmpresa().getNumApolice() + " - "+ historicoSinistro.getEmpresa().getNomeEmpresa() : null, "");
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getProposta().getOrgao().getNomeOrgao(), "");
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getProposta().getNomeSegurado(), "");
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getProposta().getNomeConjuge(), "");
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getMotivoSinistro() == null ? "" :historicoSinistro.getMotivoSinistro().getDescricao(), "");
			addCelulaTexto(aba, getStiloTextoSimples(), historicoSinistro.getMotivoSinistroNegado() == null ? "" :historicoSinistro.getMotivoSinistroNegado().getDescricao(), "");
			addCelulaNumero(aba, getStiloMonetarioSimples(), historicoSinistro.getProposta().getCapitalSegurado(), "");
			addCelulaNumero(aba, getStiloMonetarioSimples(), historicoSinistro.getValorPagoSinistro(), "");
			addCelulaNumero(aba, getStiloMonetarioSimples(), historicoSinistro.getValorCestaBasica(), "");
			addCelulaNumero(aba, getStiloMonetarioSimples(), historicoSinistro.getValorFuneral(), "");
			addCelulaTexto(aba, getStiloTextoSimples(), getSeguradora(historicoSinistro.getSeguradora()), "");
			addCelulaTexto(aba, getStiloTextoSimples(), getTextoCodificado(historicoSinistro.getSituacaoProcesso(), true), "", true);
			addCelulaTexto(aba, getStiloTextoSimples(), getTextoCodificado(historicoSinistro.getObservacao(), true), "", true);
			
			StringBuilder textoBen = new StringBuilder();
			for(BeneficiarioSinistro beneficiario: historicoSinistro.getBeneficiarios()) {
				String valorFormatado = NumberUtil.formatDecimal(beneficiario.getValorRecebido(), 2);
				textoBen.append(beneficiario.getNomeBeneficiario() + " R$ " + valorFormatado);
				textoBen.append("\n");
			}
			
			addCelulaTexto(aba, getStiloTextoSimples(), textoBen.toString(),"", true);
			
			
		}
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Integer getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Integer empresaSelected) {
		this.empresaSelected = empresaSelected;
	}	
	
}
