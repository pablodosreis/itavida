package br.com.itavida.handlers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.SiapeDAO;
import br.com.itavida.entidades.OrgaoSiape;
import br.com.itavida.entidades.Siape;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.File;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class SiapeHandler extends MainHandler {

	
	private ArrayList<File> files = new ArrayList<File>();
	private int uploadsAvailable = 1;
	private Siape siape;
	private OrgaoSiape orgaoSiape;
	private List<Siape> resultadoPesquisa;
	
	private List<Siape> listaServidoresEntraram;
	private List<Siape> listaServidoresSairam;
	private List<Siape> listaServidoresMudaramValorOrgao;
	
	//Totalizadores
	private Integer totalRegistrosSairam;
	private Integer totalRegistrosEntraram;
	private Integer totalRegistrosMudaram;
	
	private Double valorTotalRegistrosSairam;
	private Double valorTotalRegistrosEntraram;
	private Double valorTotalRegistrosMudaramAntigo;
	private Double valorTotalRegistrosMudaramNovo;
	
	public SiapeHandler(){
		cancelar(null);
	}
	
	public int getSize() {
		if (getFiles().size() > 0) {
			return getFiles().size();
		} else {
			return 0;
		}
	}
	
	public void paint(OutputStream stream, Object object) throws IOException {
		stream.write(getFiles().get((Integer) object).getData());
	}

	public void listener(UploadEvent event) throws Exception {
        UploadItem item = event.getUploadItem();
        File file = new File();
        file.setLength(item.getData().length);
        file.setName(item.getFileName());
        file.setData(item.getData());        
        files.add(file);
        uploadsAvailable = 0;
	}  
	      
    public String clearUploadData( ) {
        files.clear();
        uploadsAvailable = 1;
        return null;
    }
    
	
	public ArrayList<File> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<File> files) {
		this.files = files;
	}

	public int getUploadsAvailable() {
		return uploadsAvailable;
	}

	public void setUploadsAvailable(int uploadsAvailable) {
		this.uploadsAvailable = uploadsAvailable;
	}
	

	public void  pesquisa( ActionEvent event ){
		Session session = HibernateUtil.currentSession();
		SiapeDAO siapeDAO = new SiapeDAO( session );
		
		resultadoPesquisa = siapeDAO.buscaPorFiltro( siape );
		
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setSiape( new Siape() );
		setOrgaoSiape( new OrgaoSiape() );
		getSiape().setCodigoOrgao( new OrgaoSiape() );
		this.resultadoPesquisa = null;
		
	}
	
	public String importFile( ){
		
		if( getSize() == 0 ){
			addErrorMessage( "Selecione algum arquivo para realizar a Importa��o dos N�meros de Sorteio." );
		}
		
		File file = files.get(0);
	    
		String codigoOrgao = "";
		
        try {  
        	ByteArrayInputStream inputStreamArquivoD8 = new ByteArrayInputStream( file.getData() );
            BufferedReader arquivoD8 = new BufferedReader( new InputStreamReader( inputStreamArquivoD8  ) );  
            String line;  
            int lineCount = 0;  
            String partesNomeArquivo[] = file.getName().replaceAll(".txt","").replaceAll(".TXT","").split("D8");
            String mesReferencia = partesNomeArquivo[1].trim().substring(0,2);
            String anoReferencia = partesNomeArquivo[1].trim().substring(2,6);
            Date dataReferencia = DateUtil.getDate(mesReferencia+"/"+anoReferencia, "MM/yyyy");
            //Pesquisa no banco para verificar se j� n�o existem referencias com o mesmo 
            Session session = HibernateUtil.currentSession();
            SiapeDAO siapeDAO = new SiapeDAO( session );
            Siape siapeFiltro = new Siape();
            siapeFiltro.setDataReferencia( dataReferencia );
            List<Siape> resultados = siapeDAO.buscaPorFiltro( siapeFiltro );
            if( resultados != null && resultados.size() > 0 ){
            	addErrorMessage( "J� existe importa��o realizada para o per�do: " + mesReferencia + "/" + anoReferencia + ".");
            	throw new Exception();
            }
            
            Dao<Siape, Integer> daoSiape = new Dao<Siape, Integer>(session, Siape.class);
            
            
            while(( line = arquivoD8.readLine() )!=null)  
            {  
            	Siape siapeNet = new Siape();
            	codigoOrgao = line.substring(0, 5);
            	siapeNet.getCodigoOrgao( ).setId( new Integer(codigoOrgao) );
            	siapeNet.setMatricula( line.substring(5, 12) );
            	siapeNet.setCodigoUfPag( line.substring( 12 , 21 ) );
            	siapeNet.setUfpag( line.substring( 21 , 23 ) );
            	siapeNet.setNomeServidor( line.substring( 23 , 73 ) );
            	siapeNet.setCpf( line.substring( 73 , 84 ) );
            	siapeNet.setRubrica( line.substring( 84 , 89 ) );
            	siapeNet.setSequencial( line.substring( 89 , 90 ) );
            	String valorStr =  line.substring( 90 , 99 ) +"."+line.substring(99, 101);
            	siapeNet.setValor( new Double(valorStr) );
            	siapeNet.setPrazo( line.substring(101,104) );
            	siapeNet.setCodigoUGSiafi( line.substring(104,110) );
            	siapeNet.setDataReferencia( dataReferencia );
            	siapeNet.setDataHoraImportacao( new Date() );
            	
            	daoSiape.save( siapeNet );
            	
            	lineCount++;
            }  
            
            addGlobalMessage( lineCount + " registros importados com sucesso!" );
        }catch(ArrayIndexOutOfBoundsException aioe){
        	addErrorMessage( "Nome do arquivo est� em um formato incorreto/desconhecido." );
        	addErrorMessage("Certifique-se que esteja no formato 'ARQUIVO D8MMYYYY.txt' onde MM � o m�s de refer�ncia e YYYY � o ano de refer�ncia.");
        	
        }catch(ParseException aioe){
        	addErrorMessage( "Nome do arquivo est� em um formato incorreto/desconhecido." );
        	addErrorMessage("Certifique-se que esteja no formato 'ARQUIVO D8MMYYYY.txt' onde MM � o m�s de refer�ncia e YYYY � o ano de refer�ncia.");
        	
        }catch( ObjectNotFoundException objectNFE ){
        	addErrorMessage( "O �rg�o de c�digo: " + codigoOrgao + " n�o foi encontrado na base do SISVIDA-WEB. \nFavor realizar o cadastro do mesmo para prosseguir com a importa��o do arquivo."  );
        	
        }catch(ConstraintViolationException constraintException ){
        	addErrorMessage( "O �rg�o de c�digo: " + codigoOrgao + " n�o foi encontrado na base do SISVIDA-WEB. \nFavor realizar o cadastro do mesmo para prosseguir com a importa��o do arquivo."  );
        } catch (Exception e) {         	
            e.printStackTrace();  
        }  
		
        return null;
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravarOrgaoSiape(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<OrgaoSiape, Integer> dao = new Dao<OrgaoSiape, Integer>(session, OrgaoSiape.class);
			dao.merge( getOrgaoSiape() );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtCodigoSiape", "C�digo Siape");
		mapaCampos.put("txtNomeOrgaoSiape", "Nome do �rg�o Siape");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	
	
	public String realizarConferencia() throws Exception{
		
		Session session = HibernateUtil.currentSession();
		SiapeDAO siapeDAO = new SiapeDAO( session );
		
		boolean erro = false;
		if( siape.getPeriodoReferenciaA() == null ){
			addErrorMessage("O Per�odo A deve ser preenchido!");
			erro = true;
		}
		
		if( siape.getPeriodoReferenciaB() == null ){
			addErrorMessage("O Per�odo B deve ser preenchido!");
			erro = true;
		}
		
		if( siape.getPeriodoReferenciaA()!= null && siape.getPeriodoReferenciaB() != null && siape.getPeriodoReferenciaA().after( siape.getPeriodoReferenciaB() ) ){
			addErrorMessage("O Per�odo A deve estar antes do Per�odo B!");
			erro = true;
		}
		if( !erro ){
			siape.setDataReferencia( siape.getPeriodoReferenciaA() );
			List<Siape> listaReferenciaA = siapeDAO.buscaPorFiltro( siape );
			siape.setDataReferencia( siape.getPeriodoReferenciaB() );
			List<Siape> listaReferenciaB = siapeDAO.buscaPorFiltro( siape ); ;
			
			listaServidoresMudaramValorOrgao = new ArrayList<Siape>();
			listaServidoresEntraram = new ArrayList<Siape>();
			listaServidoresSairam = new ArrayList<Siape>();
			
			totalRegistrosSairam = 0;
			totalRegistrosEntraram = 0;
			totalRegistrosMudaram = 0;
			
			valorTotalRegistrosSairam = 0.0;
			valorTotalRegistrosEntraram = 0.0;
			valorTotalRegistrosMudaramAntigo = 0.0;
			valorTotalRegistrosMudaramNovo = 0.0;
			
			Map<String, Siape> mapaA = new HashMap<String, Siape>();
			Map<String, Siape> mapaB = new HashMap<String, Siape>();
			
			for (Siape siapeA : listaReferenciaA) {
				mapaA.put( siapeA.getCpf() , siapeA );
			}
			
			for (Siape siapeB : listaReferenciaB) {
				mapaB.put( siapeB.getCpf() , siapeB );
			}
			
			//Verifica quem SAIU na lista
			for (Siape siapeA : listaReferenciaA) {
				if( mapaB.containsKey( siapeA.getCpf() ) ){
					Siape siapeBTemp = mapaB.get( siapeA.getCpf() );
					//J� que encontrou, verifica se houve mudan�a de �rg�o ou de valor
					if( siapeBTemp.getValor() != siapeA.getValor() ||
					    !siapeBTemp.getCodigoOrgao().getId().equals( siapeA.getCodigoOrgao().getId() ) ){
						siapeA.setSiapeB( siapeBTemp );
						listaServidoresMudaramValorOrgao.add( siapeA );
						
						valorTotalRegistrosMudaramAntigo += siapeA.getValor();
						valorTotalRegistrosMudaramNovo += siapeBTemp.getValor();
					}
				}else{
					listaServidoresSairam.add( siapeA );
					valorTotalRegistrosSairam += siapeA.getValor();
				}
			}
			
			//Verifica quem ENTROU na lista
			for (Siape siapeB : listaReferenciaB) {
				
				if( !mapaA.containsKey( siapeB.getCpf() ) ){
					listaServidoresEntraram.add( siapeB );
					valorTotalRegistrosEntraram += siapeB.getValor();
				}
			}
			
			totalRegistrosEntraram =  listaServidoresEntraram.size();
			totalRegistrosSairam =  listaServidoresSairam.size();
			totalRegistrosMudaram =  listaServidoresMudaramValorOrgao.size();
			return "resultado";
		}
		return null;
	}
	
	
	public Siape getSiape() {
		return siape;
	}

	public void setSiape(Siape siape) {
		this.siape = siape;
	}

	public List<Siape> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Siape> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public OrgaoSiape getOrgaoSiape() {
		return orgaoSiape;
	}

	public void setOrgaoSiape(OrgaoSiape orgaoSiape) {
		this.orgaoSiape = orgaoSiape;
	}

	public List<Siape> getListaServidoresEntraram() {
		return listaServidoresEntraram;
	}

	public void setListaServidoresEntraram(List<Siape> listaServidoresEntraram) {
		this.listaServidoresEntraram = listaServidoresEntraram;
	}

	public List<Siape> getListaServidoresSairam() {
		return listaServidoresSairam;
	}

	public void setListaServidoresSairam(List<Siape> listaServidoresSairam) {
		this.listaServidoresSairam = listaServidoresSairam;
	}

	public List<Siape> getListaServidoresMudaramValorOrgao() {
		return listaServidoresMudaramValorOrgao;
	}

	public void setListaServidoresMudaramValorOrgao(
			List<Siape> listaServidoresMudaramValorOrgao) {
		this.listaServidoresMudaramValorOrgao = listaServidoresMudaramValorOrgao;
	}

	public Integer getTotalRegistrosSairam() {
		return totalRegistrosSairam;
	}

	public void setTotalRegistrosSairam(Integer totalRegistrosSairam) {
		this.totalRegistrosSairam = totalRegistrosSairam;
	}

	public Integer getTotalRegistrosEntraram() {
		return totalRegistrosEntraram;
	}

	public void setTotalRegistrosEntraram(Integer totalRegistrosEntraram) {
		this.totalRegistrosEntraram = totalRegistrosEntraram;
	}

	public Integer getTotalRegistrosMudaram() {
		return totalRegistrosMudaram;
	}

	public void setTotalRegistrosMudaram(Integer totalRegistrosMudaram) {
		this.totalRegistrosMudaram = totalRegistrosMudaram;
	}

	public Double getValorTotalRegistrosSairam() {
		return valorTotalRegistrosSairam;
	}

	public void setValorTotalRegistrosSairam(Double valorTotalRegistrosSairam) {
		this.valorTotalRegistrosSairam = valorTotalRegistrosSairam;
	}

	public Double getValorTotalRegistrosEntraram() {
		return valorTotalRegistrosEntraram;
	}

	public void setValorTotalRegistrosEntraram(Double valorTotalRegistrosEntraram) {
		this.valorTotalRegistrosEntraram = valorTotalRegistrosEntraram;
	}

	public Double getValorTotalRegistrosMudaramAntigo() {
		return valorTotalRegistrosMudaramAntigo;
	}

	public void setValorTotalRegistrosMudaramAntigo(
			Double valorTotalRegistrosMudaramAntigo) {
		this.valorTotalRegistrosMudaramAntigo = valorTotalRegistrosMudaramAntigo;
	}

	public Double getValorTotalRegistrosMudaramNovo() {
		return valorTotalRegistrosMudaramNovo;
	}

	public void setValorTotalRegistrosMudaramNovo(
			Double valorTotalRegistrosMudaramNovo) {
		this.valorTotalRegistrosMudaramNovo = valorTotalRegistrosMudaramNovo;
	}

	

	// fim getters and setters
	// ==================================================
	// =========================================

}
