package br.com.itavida.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.NovosValoresDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dao.ResultadoDAO;
import br.com.itavida.dto.LinhaMensagem;
import br.com.itavida.entidades.ComportamentoPropostaEnum;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.ModeloProposta;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.ResultadoOperacao;
import br.com.itavida.entidades.TipoProposta;
import br.com.itavida.exceptions.ImportacaoExcelException;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Carteiro;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.File;
import br.com.itavida.util.HibernateUtil;

@SuppressWarnings("serial")
public class ImportarNovosValoresHandler extends ExcelGenerateHandler {

	public static final Integer TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL = 9;
	public static final Integer TIPO_PROPOSTA_ATUALIZADO_CASADO_50 = 10;
	public static final Integer TIPO_PROPOSTA_ATUALIZADO_CASADO_100 = 11;
	
	private List<String> impedimentos;
	private List<LinhaMensagem> alertas;
	private List<LinhaMensagem> erros;
	private List<LinhaMensagem> sucessos;
	
	private String periodoImportacao;
	
	
	public static final String RETORNO_IMPORT_VALORES_ERROS = "IMPORT_NOVOS_VALORES_ERROS";
	public static final String RETORNO_IMPORT_VALORES_ALERTAS = "IMPORT_NOVOS_VALORES_ALERTAS";

	private static Logger logger = Logger.getLogger(ImportarNovosValoresHandler.class.toString());

	private ArrayList<File> files = new ArrayList<File>();
	private int uploadsAvailable = 1;

	private static final int QUANTIDADE_COLUNAS = 7;
	private static final int INICIO_DOS_DADOS = 1;
	
	private PropostaDAO propostaDAO;
	
	private ResultadoDAO resultadoDAO;
	
	NovosValoresDAO novosValoresDAO = new NovosValoresDAO();
	
	private Map<String,Proposta> cachePropostas;
	
	/**
	 * Construtor.
	 */
	public ImportarNovosValoresHandler() {
		resultadoDAO = new ResultadoDAO(HibernateUtil.currentSession());
	}



	public int getSize() {
		if (getFiles().size() > 0) {
			return getFiles().size();
		} else {
			return 0;
		}
	}

	public void paint(OutputStream stream, Object object) throws IOException {
		stream.write(getFiles().get((Integer) object).getData());
	}

	public void listener(UploadEvent event) throws Exception {
		UploadItem item = event.getUploadItem();
		File file = new File();
		file.setLength(item.getData().length);
		file.setName(item.getFileName());
		file.setData(item.getData());
		files.add(file);
		uploadsAvailable = 0;
	}

	public String clearUploadData() {
		files.clear();
		uploadsAvailable = 1;
		return null;
	}
	
	public void limparDados(ActionEvent e) {
		clearUploadData();
	}
	
	public Proposta getProposta(String cpfParametro) {
		String cpf = cpfParametro.trim();
		cpf = cpf.replaceAll("\\.", "").replaceAll("-", "").replaceAll("/", "").replaceAll("_", "");
		cpf = StringUtils.leftPad(cpf, 11, '0');
		Proposta proposta = cachePropostas.get(cpf);
//		if(proposta == null) {
//			Proposta filtro = new Proposta();
//			filtro.setCpfSegurado(cpfParametro);
//			filtro.setCpfSeguradoFormatado( cpf);
//			proposta =  propostaDAO.getPropostaPorCPF(filtro);
//		}
		return proposta;
	}
	
	public PropostaDAO getDAO() {
		Session session = HibernateUtil.currentSession();
		propostaDAO = new PropostaDAO(session);
		return propostaDAO;
	}
	
	public void inicializarCachePropostas() {
		resultadoDAO = new ResultadoDAO(HibernateUtil.currentSession());
		cachePropostas = new HashMap<String, Proposta>();
		List<Proposta> propostasNaoCanceladas = getDAO().obterTodasPropostasNaoCanceladas();
		propostaDAO.populaCapital(propostasNaoCanceladas);
		for(Proposta p : propostasNaoCanceladas) {
			String cpf = p.getCpfSegurado().trim();
			cpf = cpf.replaceAll("\\.", "");
			cpf = cpf.replaceAll("-", "");
			cpf = StringUtils.leftPad(cpf, 11, '0');
			if(!cpf.isEmpty()) {
				cachePropostas.put(cpf, p);
			}
		}
	}

	public String importReport() {
		try {
			resultadoDAO = new ResultadoDAO(HibernateUtil.currentSession());
			return importar();
		}  catch (ImportacaoExcelException e) {
			logger.log(Level.WARNING,"Erro ao importar os dados",e);
			addErrorMessage(e.getMessage());
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Erro ao importar os dados",e);
			addErrorMessage("Ocorreu um erro ao importar o arquivo, verifique se o arquivo � v�lido para esta importa��o.");
		}
		return null;
	}
	
	public Date getBaseImportacao() throws ImportacaoExcelException {
		Integer mes = null;
		Integer ano = null;
		Integer dia = null;
		Date dataBase = null;
		if(periodoImportacao == null){
			throw new ImportacaoExcelException(
					"Informe um per�odo para a importa��o (formato DD/MM/AAAA)");
		}else{
			try{
				dataBase = DateUtil.getDate(periodoImportacao, "dd/MM/yyyy");
				String[] splitPeriodo = periodoImportacao.split("/");
				dia = Integer.parseInt(splitPeriodo[0]);
				mes = Integer.parseInt(splitPeriodo[1]);
				ano = Integer.parseInt(splitPeriodo[2]);
			}catch(ParseException ne){
				throw new ImportacaoExcelException(
						"Informe um dia/m�s/ano v�lido (formato MM/AAAA).");
			}
			if(mes < 1 || mes > 12){
				throw new ImportacaoExcelException(
						"O m�s deve ser um n�mero entre 1 e 12.");
			}
		}
		if (getSize() == 0) {
			throw new ImportacaoExcelException(
					"Selecione algum arquivo para realizar a Importa��o dos Novos Valores.");
		}
		
		return dataBase;
		 
	}

	public String importar() throws ImportacaoExcelException {
		String retorno = null;
		StringBuffer textoEmail = new StringBuffer();
		long init = System.currentTimeMillis();
		if (getSize() == 0) {
			throw new ImportacaoExcelException(
					"Selecione algum arquivo para realizar a Importa��o dos Novos Valores.");
		}
		
		Date baseImportacao = getBaseImportacao();

		inicializarCachePropostas();

		impedimentos = new ArrayList<String>();
		alertas = new ArrayList<LinhaMensagem>();
		erros = new ArrayList<LinhaMensagem>();
		sucessos = new ArrayList<LinhaMensagem>();
		List<Proposta> bufferPropostas = new ArrayList<Proposta>();

		File file = files.get(0);
		ByteArrayInputStream excelFIS = null;
		excelFIS = new ByteArrayInputStream(file.getData());

		HSSFWorkbook excelWB = null;

		excelWB = criarExcelWB(excelFIS, excelWB);

		// Passa pelas sheets...
		for (int indexSheet = 0; indexSheet < excelWB.getNumberOfSheets(); indexSheet++) {
			
			Session session = HibernateUtil.currentSession();
			String sheetName = excelWB.getSheetName(indexSheet);
			HSSFSheet sheet = excelWB.getSheetAt(indexSheet);
			int rows = sheet.getPhysicalNumberOfRows();
			
			// Empresa...
			Empresa empresa = getEmpresaSheet(session, sheetName);
			
			if(empresa == null) {
				impedimentos.add(String.format("O nome da aba '%s' n�o representa uma ap�lice v�lida.", sheetName));
				continue;
			}

			// verifica o n�mero de colunas
			if (sheet.getRow(INICIO_DOS_DADOS).getPhysicalNumberOfCells() != QUANTIDADE_COLUNAS) {
				throw new ImportacaoExcelException(
						"O formato desta planilha � incorreto. Favor verificar a planilha selecionada.");
			}
			textoEmail.append("<p style='font-weight: bold'>Registros Importados com Sucesso</p> <br/>");
			// itera sobre as linhas de cada sheet
			for (int rowNumber = INICIO_DOS_DADOS; rowNumber < rows + 1; rowNumber++) {
				boolean erroLinha = false;
				HSSFRow oneRow = sheet.getRow(rowNumber);

				// continua se a linha estiver vazia
				if (oneRow == null || oneRow.getCell(0) == null ) {
					continue;
				}

				// Proposta...
				String cpf;
				HSSFCell cell = oneRow.getCell(0);
				
				Proposta proposta = null;
				if (cell != null
						&& (cell.getCellType() == HSSFCell.CELL_TYPE_STRING))  {
					// CPF proposta...
					cpf = cell.getRichStringCellValue().getString();
				} else {
					cpf = getValorDecimal(cell) != null ? new Long(getValorDecimal(cell).longValue()).toString() : "";
				}
				
				if(cpf == null || cpf.isEmpty()) {
					continue;
				}
				
				proposta = getProposta(cpf);

				if (proposta == null) {
					erros.add(new LinhaMensagem(rowNumber, "Segurado n�o encontrado. CPF: " + cpf));
					continue;
				}
				
				if(!proposta.getEmpresa().equals(empresa)) {
					erros.add((new LinhaMensagem(rowNumber,"Segurado n�o pertence a Ap�lice definida pela aba. CPF: " + cpf)));
					erroLinha = true;
				}
				
				//verifica se o segurado foi atualizadoa p�s a data base da importa��o.
				if(proposta.getDataAprovacao() != null && proposta.getDataAprovacao().getTime() > baseImportacao.getTime() ) {
					erros.add(new LinhaMensagem(rowNumber, "O Segurado foi atualizado ap�s a data base da importa��o. CPF Segurado: " + cpf));
					continue;
				}
				
				// verifica o nome
				cell = oneRow.getCell(1);
				String nome = getTexto(cell);
				if(!proposta.getNomeSegurado().equalsIgnoreCase(nome)) {
					alertas.add((new LinhaMensagem(rowNumber,String.format("O nome %s n�o corresponde ao segurado de CPF %s.", nome, cpf))));
				}

				// verifica a data de nascimento
				cell = oneRow.getCell(2);
				if (cell != null
						&& cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					// TODO verificar se a data de nacimento
				}

				/* 1 - Verifica se houve diminui��o do pr�mio do Titular*/
				cell = oneRow.getCell(3);
				Double premioTit = getValorDecimal(cell);
				if (premioTit == null) {
					erros.add(new LinhaMensagem(rowNumber, "A linha possui um valor inv�lido na coluna Pr�mio do Titular."));
					erroLinha = true;
				} else if (proposta.getPremioSegurado() != null && proposta.getPremioSegurado() > premioTit) {
					alertas.add((new LinhaMensagem(rowNumber,String.format("O segurado de CPF '%s' apresenta atualmente um pr�mio maior que o sendo importado.", cpf))));
				}

				/*2 - Verifica se houve dimuni��o do pr�mio do C�njuge*/
				cell = oneRow.getCell(4);
				Double premioConj = getValorDecimal(cell);
				if(proposta.isCasado()) {
					if (premioConj == null) {
						erros.add(new LinhaMensagem(rowNumber, "A linha possui um valor inv�lido na coluna Pr�mio do C�njuge."));
						erroLinha = true;
					} else if (proposta.getPremioConjuge()!= null && proposta.getPremioConjuge() > premioConj) {
						alertas.add((new LinhaMensagem(rowNumber,String.format("O c�njuge do segurado de CPF '%s' apresenta atualmente um pr�mio maior que o sendo importado.", cpf))));
					}
				}

				/* 3 - Verifica se houve dimuni��o do capital do segurado*/
				cell = oneRow.getCell(5);
				Double capitalTit = getValorDecimal(cell);
				if (capitalTit == null) {
					erros.add((new LinhaMensagem(rowNumber,"A linha possui uma um valor inv�lido na coluna Capital do Titular.")));
					erroLinha = true;
				} else if (proposta.getCapitalSegurado() != null && proposta.getCapitalSegurado() > capitalTit) {
					alertas.add((new LinhaMensagem(rowNumber,String.format("O segurado de CPF '%s' apresenta atualmente um capital maior que o sendo importado.", cpf))));
				}

				/* 4 - Verifica se houve dimuni��o do capital do conjuge*/
				cell = oneRow.getCell(6);
				Double capitalConj = getValorDecimal(cell);
				if(proposta.isCasado()) {
					if (capitalConj == null) {
						erros.add((new LinhaMensagem(rowNumber,"A linha possui uma um valor inv�lido na coluna Capital do C�njuge")));
						erroLinha = true;
					} else if (proposta.getCapitalConjuge() != null && proposta.getCapitalConjuge() > capitalConj) {
						alertas.add((new LinhaMensagem(rowNumber,String.format("O c�njuge do segurado de CPF '%s' apresenta atualmente um capital maior que o sendo importado.", cpf))));
					}
				}
				
				/* 5 - Verifica se existe premio para c�njuge de proposta individual*/
				if(!proposta.isCasado() && (premioConj != null && premioConj > 0) ) {
					erros.add((new LinhaMensagem(rowNumber,String.format("A linha n�o deve possuir valor de pr�mio para o c�njuge do segurado de cpf '%s' porque trata-se de uma proposta individual. ", cpf))));
					erroLinha = true;
				}
				
				
				/*6 - Verifica se existe capital para c�njuge de proposta individual*/
				if(!proposta.isCasado() && (capitalConj != null && capitalConj > 0)) {
					erros.add((new LinhaMensagem(rowNumber,String.format("A linha n�o deve possuir valor de capital para o c�njuge do segurado de cpf '%s' porque trata-se de uma proposta individual. ", cpf))));
					erroLinha = true;
				}
				
				
				if(proposta.getModeloProposta() == null) {
					erros.add(new LinhaMensagem(rowNumber, String.format("A proposta de cpf '%s' n�o possui modelo de proposta cadastrado.", cpf)));
					erroLinha = true;
				}
				
				if(proposta.getTipoProposta() == null) {
					erros.add(new LinhaMensagem(rowNumber, String.format("A proposta de cpf '%s' n�o possui tipo de proposta cadastrado.", cpf)));
					erroLinha = true;
				}
				
				if(proposta.getDataNascimentoSegurado() == null) {
					erros.add(new LinhaMensagem(rowNumber, String.format("A proposta de cpf '%s' n�o possui data de nascimento cadastrada.", cpf)));
					erroLinha = true;
				}
				
				
				if (impedimentos.isEmpty() && !erroLinha) {
					proposta.setModeloProposta(new ModeloProposta(Constantes.MODELO_PROPOSTA_ATUALIZADO));
					boolean tipo50 = proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50;
										
					boolean tipo100 = proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100; 
					tipo100 = tipo100 || proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO;
					
					
					if(tipo50) {
						proposta.setTipoProposta(new TipoProposta(TIPO_PROPOSTA_ATUALIZADO_CASADO_50));
					} else if(tipo100) {
						proposta.setTipoProposta(new TipoProposta(TIPO_PROPOSTA_ATUALIZADO_CASADO_100));
					} else {
						proposta.setTipoProposta(new TipoProposta(TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL));
					}
					proposta.setPremioSegurado(premioTit);
					proposta.setPremioConjuge(premioConj);
					proposta.setCapitalSegurado(capitalTit);
					proposta.setCapitalConjuge(capitalConj);
					
					proposta.setValorPremioSegurado(premioTit);
					proposta.setValorPremioConjuge(premioConj);
					proposta.setValorCapitalSegurado(capitalTit);
					proposta.setValorCapitalConjuge(capitalConj);
					

					proposta.setCapitalSeguradoFuturo(null);
					proposta.setCapitalConjugeFuturo(null);
					proposta.setPremioSeguradoFuturo(null);
					proposta.setPremioConjugeFuturo(null);
					
					proposta.setValorFuturoCapitalConjuge(null);
					proposta.setValorFuturoPremioSegurado(null);
					proposta.setValorFuturoCapitalConjuge(null);
					proposta.setValorFuturoPremioConjuge(null);
					
	
					bufferPropostas.add(proposta);
					sucessos.add(new LinhaMensagem(rowNumber, proposta.getCpfSegurado()));
					textoEmail.append("<br/> " + rowNumber + " - CPF: " + proposta.getCpfSegurado());
					
				} 
			}
		}
		
		textoEmail.append("<br/><br/><p style='font-weight: bold'>Falhas ao importar</p>");
		for (LinhaMensagem erro : erros) {
			textoEmail.append("<br/>" + erro);
		}
		
		if (impedimentos.isEmpty()) {
			
			novosValoresDAO.salvar(bufferPropostas);
			
			retorno = "resultado";
		} else {
			textoEmail.append("<br/><br/> <p style='font-weight: bold'>Impedimentos</p>");
			for (String erro : impedimentos) {
				textoEmail.append("<br/>" + erro);
				addErrorMessage(erro);
			}
			retorno = null;
		}
		
		
		
		new Carteiro().enviar("Sisvida", "pablodosreis@gmail.com,cadastro@itavida.com.br,sinistro@itavida.com.br", "Importa��o de Novos Valores", textoEmail.toString());

		ResultadoOperacao resultadoOperacao = new ResultadoOperacao();
		resultadoOperacao.setData(new Date());
		resultadoOperacao.setConteudo(textoEmail.toString());
		resultadoDAO.salvar(resultadoOperacao);
		
		//liberar cache
		for (Proposta proposta : cachePropostas.values()) {
			if (!bufferPropostas.contains(proposta)) {
				propostaDAO.evict(proposta);
			}
		}
		
		long end = System.currentTimeMillis();
		long diff = end - init;
		System.out.println("Demorou " + (diff / 1000) + " segundos");
		return retorno;
	}



	private Empresa getEmpresaSheet(Session session, String sheetName) {
		Empresa empresa;
		EmpresaDAO empresaoDAO = new EmpresaDAO(session);
		
		String sheetLimpa = sheetName.replaceAll("-", "");
		sheetLimpa = sheetName.replaceAll("\\.", "");
		empresa = empresaoDAO.buscaUnicaPelaApolice(sheetLimpa);
		return empresa;
	}



	private HSSFWorkbook criarExcelWB(ByteArrayInputStream excelFIS,
			HSSFWorkbook excelWB) throws ImportacaoExcelException {
		try {
			excelWB = new HSSFWorkbook(excelFIS);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Erro ao importar os dados", e);
			throw new ImportacaoExcelException(
					"O formato desta planilha � incorreto. Favor verificar a planilha selecionada.");
		}
		return excelWB;
	}
	
	public String getTexto(HSSFCell cell) {
		if(cell != null && cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
			return cell.getRichStringCellValue().getString();
		}
		return null;
	}
	
	public Double getValorDecimal(HSSFCell cell) {
		if(cell == null) {
			return null;
		}
		
		int cellType = cell.getCellType();
		
		
		try {
			if(cell != null && cellType == HSSFCell.CELL_TYPE_NUMERIC) {
				return cell.getNumericCellValue();
			}
			
			if(cell != null && cellType == HSSFCell.CELL_TYPE_STRING) {
				return Double.parseDouble(cell.getRichStringCellValue().getString());
			}
		} catch (Exception e) {
			//faz nada
		}
		
		return null;
		
	}

	public long getTimeStamp() {
		return System.currentTimeMillis();
	}

	public ArrayList<File> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<File> files) {
		this.files = files;
	}

	public int getUploadsAvailable() {
		return uploadsAvailable;
	}

	public void setUploadsAvailable(int uploadsAvailable) {
		this.uploadsAvailable = uploadsAvailable;
	}

	// fim getters and setters
	// ===========================================================================================
	
	public Date getDataAtual() {
		return new Date();
	}


	public List<String> getImpedimentos() {
		return impedimentos;
	}


	public void setImpedimentos(List<String> impedimentos) {
		this.impedimentos = impedimentos;
	}


	public List<LinhaMensagem> getAlertas() {
		return alertas;
	}



	public void setAlertas(List<LinhaMensagem> alertas) {
		this.alertas = alertas;
	}



	public List<LinhaMensagem> getErros() {
		return erros;
	}



	public void setErros(List<LinhaMensagem> erros) {
		this.erros = erros;
	}



	public List<LinhaMensagem> getSucessos() {
		return sucessos;
	}



	public void setSucessos(List<LinhaMensagem> sucessos) {
		this.sucessos = sucessos;
	}



	private int indiceLinha = 0;
	
	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		addRow(aba);
		addTextoMesclado(aba, getStiloTextoTitulo(), "ITAVIDA CLUBE DE SEGUROS", indiceLinha, 1, 0, 1);
		addRow(aba);
		indiceLinha++;
		String dataAtual = new SimpleDateFormat("dd/MM/yy").format(new Date()) ;
		addTextoMesclado(aba, getStiloTextoTitulo(), "RESULTADO IMPORTA��O DE NOVOS VALORES " + dataAtual , indiceLinha, 1, 0, 1);
		addRow(aba);
		indiceLinha++;
		addRow(aba);
		indiceLinha++;
		
	}


	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		
		if(!erros.isEmpty()) {
			addRow(aba);
			addTextoMesclado(aba, getStiloTextoTitulo(), "Linhas n�o importadas devido a erros", indiceLinha++, 1, 0, 1);
			addRow(aba);
			addCelulaTexto(aba, getStiloTextoTitulo(), "Linha");
			addCelulaTexto(aba, getStiloTextoTitulo(), "Descri��o");
			indiceLinha++;
			for(LinhaMensagem valor : erros) {
				indiceLinha++;
				addRow(aba);
				addCelulaTexto(aba, getStiloTextoSimples(), valor.getLinha());
				addCelulaTexto(aba, getStiloTextoSimples(), valor.getMensagem());
			}
		}
		
		if(!alertas.isEmpty()) {
			addRow(aba);
			addTextoMesclado(aba, getStiloTextoTitulo(), "Alertas gerados pela importa��o", indiceLinha++, 1, 0, 1);
			addRow(aba);
			addCelulaTexto(aba, getStiloTextoTitulo(), "Linha");
			addCelulaTexto(aba, getStiloTextoTitulo(), "Descri��o");
			indiceLinha++;
			for(LinhaMensagem valor : alertas) {
				indiceLinha++;
				addRow(aba);
				addCelulaTexto(aba, getStiloTextoSimples(), valor.getLinha());
				addCelulaTexto(aba, getStiloTextoSimples(), valor.getMensagem());
			}
		}
		
		if(!sucessos.isEmpty()) {
			addRow(aba);
			addTextoMesclado(aba, getStiloTextoTitulo(), "Linhas importadas com Sucesso", indiceLinha++, 1, 0, 1);
			addRow(aba);
			addCelulaTexto(aba, getStiloTextoTitulo(), "Linha");
			addCelulaTexto(aba, getStiloTextoTitulo(), "Descri��o");
			indiceLinha++;
			for(LinhaMensagem valor : sucessos) {
				indiceLinha++;
				addRow(aba);
				addCelulaTexto(aba, getStiloTextoSimples(), valor.getLinha());
				addCelulaTexto(aba, getStiloTextoSimples(), valor.getMensagem());
			}
		}
	}
	
	public String gerarRelatorio() {
		indiceLinha = 0;
		List<String> abas = new ArrayList<String>();
		abas.add("RESULTADO");
		super.gerarRelatorio(abas, 0, "ResultadoImportacaoNovosValores.xls");
		return null;
	}



	public String getPeriodoImportacao() {
		return periodoImportacao;
	}



	public void setPeriodoImportacao(String periodoImportacao) {
		this.periodoImportacao = periodoImportacao;
	}
	
	public Integer getNumeroSucessos() {
		return sucessos.size();
	}
	
	public Integer getNumeroErros() {
		return erros.size();
	}
	
	public Integer getNumeroAlertas() {
		return alertas.size();
	}
	
	public List<ResultadoOperacao> getResultados() {
		resultadoDAO = new ResultadoDAO(HibernateUtil.currentSession());
		return resultadoDAO.buscarResultados();		
	}

}
