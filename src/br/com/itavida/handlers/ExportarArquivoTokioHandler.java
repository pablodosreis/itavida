package br.com.itavida.handlers;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.Session;

import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.NumeroSorteio;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;

public class ExportarArquivoTokioHandler extends MainHandler {

	private Integer radioSelected;
	private Integer empresaSelected;
	private List<SelectItem> opcoesEmpresaCombo;

	public ExportarArquivoTokioHandler(){
		populaOpcoesEmpresaCombo();
	}
	
	/**
	 * Popula um combo com op��es do Agenciador 
	 * @return
	 */
	public void populaOpcoesEmpresaCombo(){
		
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO =  new EmpresaDAO( session );
		
		List<Empresa> listaEmpresas = empresaDAO.buscaEmpresas();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesEmpresaCombo = new ArrayList<SelectItem>();
		opcoesEmpresaCombo.add( new SelectItem( null, "Selecione:" ) );
		for (Iterator iterator = listaEmpresas.iterator(); iterator.hasNext();) {
			Empresa empresa = (Empresa) iterator.next();
			opcoesEmpresaCombo.add( new SelectItem( empresa.getId(), empresa.getNomeEmpresa()) );
		}
		
	}
	
	public String exportReportTodos(){
		
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		List<Proposta> propostas = propostaDAO.obterTodasPropostasAtivasPorEmpresa(empresaSelected);
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dfFileName = new SimpleDateFormat("ddMMyyyy");
        DecimalFormat decimalFormat = new DecimalFormat();  
        decimalFormat.setMaximumFractionDigits(2);  
        decimalFormat.setMinimumFractionDigits(2);
        decimalFormat.setGroupingUsed( true );
        decimalFormat.setGroupingSize(3);
		
		boolean falhou = false;
		
		HSSFWorkbook wb = new HSSFWorkbook();  
		HSSFSheet sheet = wb.createSheet();  
		HSSFRow row;  
        
		try {  
                          
            //Estilos
            short formatoMonetario = wb.createDataFormat().getFormat("R$ #,##0.00;[Red]R$ #,##0.00");
			
            HSSFCellStyle estiloMonetario = wb.createCellStyle();
            estiloMonetario.setDataFormat( formatoMonetario );
            
			HSSFFont fontBold = wb.createFont();
			fontBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			
			  //Cabe�alho  
            row = sheet.createRow(0);  
            
            HSSFFont fontCabecalho = wb.createFont();  
            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
            HSSFRichTextString richTextModeloProposta = new HSSFRichTextString("Modelo Proposta");  
            richTextModeloProposta.applyFont(fontCabecalho);             
            HSSFRichTextString richTextTipoProposta = new HSSFRichTextString("Tipo Proposta");  
            richTextTipoProposta.applyFont(fontCabecalho);     
            HSSFRichTextString richTextNomeOrgao = new HSSFRichTextString("Nome �rg�o");  
            richTextNomeOrgao.applyFont(fontCabecalho); 
            HSSFRichTextString richTextUfOrgao = new HSSFRichTextString("UF �rg�o");  
            richTextUfOrgao.applyFont(fontCabecalho); 
            HSSFRichTextString richTextSeguradoPrincipal = new HSSFRichTextString("Nome Segurado Titular");  
            richTextSeguradoPrincipal.applyFont(fontCabecalho);             
            HSSFRichTextString richTextCPFTitular = new HSSFRichTextString("CPF Titular");  
            richTextCPFTitular.applyFont(fontCabecalho);            
            HSSFRichTextString richTextDataNascimentoTitular = new HSSFRichTextString("Data Nasc Titular");  
            richTextDataNascimentoTitular.applyFont(fontCabecalho);  
            HSSFRichTextString richTextTabelaCapital = new HSSFRichTextString("Tabela Capital");  
            richTextTabelaCapital.applyFont(fontCabecalho);             
            HSSFRichTextString richTextSexoTitular = new HSSFRichTextString("Sexo Titular");  
            richTextSexoTitular.applyFont(fontCabecalho);  
            HSSFRichTextString richTextConjuge = new HSSFRichTextString("Nome Conjuge");  
            richTextConjuge.applyFont(fontCabecalho);   
            HSSFRichTextString richTextDataNascimentoConjuge = new HSSFRichTextString("Data Nasc Conjuge");  
            richTextDataNascimentoConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCPFConjuge = new HSSFRichTextString("CPF Conjuge");  
            richTextCPFConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCapitalFuncionario = new HSSFRichTextString("Capital Titular");  
            richTextCapitalFuncionario.applyFont(fontCabecalho);   
            HSSFRichTextString richTextCapitalConjuge = new HSSFRichTextString("Capital Conjuge");  
            richTextCapitalConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextPremioTitular= new HSSFRichTextString("Premio Titular");  
            richTextPremioTitular.applyFont(fontCabecalho);
            HSSFRichTextString richTextPremioConjuge= new HSSFRichTextString("Premio Conjuge");  
            richTextPremioConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextValorTotal= new HSSFRichTextString("Valor Total Premios");  
            richTextValorTotal.applyFont(fontCabecalho);    
            HSSFRichTextString richTextDataAprovacao= new HSSFRichTextString("Data de Aprova��o");  
            richTextDataAprovacao.applyFont(fontCabecalho); 
            HSSFRichTextString richTextDataCadastro= new HSSFRichTextString("Data de Cadastro");  
            richTextDataCadastro.applyFont(fontCabecalho); 
            HSSFRichTextString richTextNumeroSorte = new HSSFRichTextString("N�mero da Sorte");  
            richTextNumeroSorte.applyFont(fontCabecalho); 
            HSSFRichTextString richTextDataUltimaAlteracao = new HSSFRichTextString("Data �ltima Altera��o");  
            richTextDataUltimaAlteracao.applyFont(fontCabecalho);             

            row.createCell(0).setCellValue( richTextModeloProposta );  
            row.createCell(1).setCellValue(richTextTipoProposta);
            row.createCell(2).setCellValue(richTextTabelaCapital);
            row.createCell(3).setCellValue(richTextNomeOrgao);  
            row.createCell(4).setCellValue(richTextUfOrgao); 
            row.createCell(5).setCellValue(richTextSeguradoPrincipal); 
            row.createCell(6).setCellValue(richTextCPFTitular); 
            row.createCell(7).setCellValue(richTextDataNascimentoTitular); 
            row.createCell(8).setCellValue(richTextSexoTitular);          
            row.createCell(9).setCellValue(richTextConjuge); 
            row.createCell(10).setCellValue(richTextDataNascimentoConjuge); 
            row.createCell(11).setCellValue(richTextCPFConjuge); 
            row.createCell(12).setCellValue(richTextCapitalFuncionario);    
            row.createCell(13).setCellValue(richTextCapitalConjuge); 
            row.createCell(14).setCellValue(richTextPremioTitular);       
            row.createCell(15).setCellValue(richTextPremioConjuge);
            row.createCell(16).setCellValue(richTextValorTotal);
            row.createCell(17).setCellValue(richTextDataAprovacao);
            row.createCell(18).setCellValue(richTextDataCadastro);
            row.createCell(19).setCellValue(richTextNumeroSorte);
            row.createCell(20).setCellValue(richTextDataUltimaAlteracao);  
            
            //Fim cabe�alho 
            int inicio = 1;

            for (Proposta proposta : propostas) {
				
            	propostaDAO.populaCapital(proposta);
            	
            	row = sheet.createRow(inicio);  
            	
            	row.createCell(0).setCellValue( new HSSFRichTextString( proposta.getModeloProposta().getDescricao() ));  
                row.createCell(1).setCellValue( new HSSFRichTextString( proposta.getTipoProposta().getDescricao() ));  
                row.createCell(2).setCellValue( new HSSFRichTextString( proposta.getNomeTabela().getNomeTabela() ) );
                row.createCell(3).setCellValue( new HSSFRichTextString( proposta.getOrgao().getNomeOrgao() ) );
                row.createCell(4).setCellValue( new HSSFRichTextString( proposta.getOrgao().getCidade().getCod_estado().getSgl_estado() ) );
                row.createCell(5).setCellValue( new HSSFRichTextString( proposta.getNomeSegurado() ) );  
                row.createCell(6).setCellValue( new HSSFRichTextString( proposta.getCpfSegurado() ) ); 
                row.createCell(7).setCellValue( new HSSFRichTextString( df.format( proposta.getDataNascimentoSegurado() ) ) ); 
                row.createCell(8).setCellValue( new HSSFRichTextString( proposta.getSexoSegurado() ) );
                row.createCell(9).setCellValue( new HSSFRichTextString( proposta.getNomeConjuge() ) );
                if( proposta.getDataNascimentoConjuge() != null ){
                	row.createCell(10).setCellValue( new HSSFRichTextString( df.format( proposta.getDataNascimentoConjuge() ) ) );  
                }else{
                	row.createCell(10).setCellValue( new HSSFRichTextString( "" ) );
                }                
                row.createCell(11).setCellValue( new HSSFRichTextString( proposta.getCpfConjuge() ) ); 
                
                if(  proposta.getCapitalSegurado() != null ){
                	row.createCell(12).setCellValue( proposta.getCapitalSegurado()  ); 
                }else{
                	row.createCell(12).setCellValue( 0.0 );
                }
                row.getCell(12).setCellStyle( estiloMonetario );
                
                if( proposta.getCapitalConjuge() != null ){
                	row.createCell(13).setCellValue( proposta.getCapitalConjuge()  );
                }else{
                	row.createCell(13).setCellValue( 0.0 );
                }
                row.getCell(13).setCellStyle( estiloMonetario );	            
                
                if( proposta.getPremioSegurado() != null){
                	row.createCell(14).setCellValue(  proposta.getPremioSegurado()  ); 
                }else{
                	row.createCell(14).setCellValue(  0.0  );
                }
                row.getCell(14).setCellStyle( estiloMonetario );
                
                if( proposta.getPremioConjuge() != null ){
                	row.createCell(15).setCellValue(  proposta.getPremioConjuge()  );
                }else{
                	row.createCell(15).setCellValue( 0.0  );
                }
                row.getCell(15).setCellStyle( estiloMonetario );
                
                if( proposta.getTotalPremios() != null ){
                	row.createCell(16).setCellValue(  proposta.getTotalPremios() ); 
                }else{
                	row.createCell(16).setCellValue( 0.0 ); 
                }
                row.getCell(16).setCellStyle( estiloMonetario );
                
                if( proposta.getDataAprovacao() != null ){
                	row.createCell(17).setCellValue( new HSSFRichTextString( df.format( proposta.getDataAprovacao() ) ) );  
                }else{
                	row.createCell(17).setCellValue( new HSSFRichTextString( "" ) );
                }
                  
                if( proposta.getDataCadastro() != null ){
                	row.createCell(18).setCellValue( new HSSFRichTextString( df.format( proposta.getDataCadastro() ) ) );   
                }else{
                	row.createCell(18).setCellValue( new HSSFRichTextString( "" ) );
                }
                
                
        		if( proposta.getNumerosorteio_collection() != null && proposta.getNumerosorteio_collection().size() > 0 ){
        			List<NumeroSorteio> listaNumerosSorteio = new ArrayList<NumeroSorteio>( proposta.getNumerosorteio_collection() );
        			NumeroSorteio numeroSorteioTemp = listaNumerosSorteio.get( proposta.getNumerosorteio_collection().size() - 1 );
        			
        			row.createCell(19).setCellValue( new HSSFRichTextString( numeroSorteioTemp.getNumeroSorteio() ) );
        		}else{
        			row.createCell(19).setCellValue( new HSSFRichTextString( "" ) );
        		}
                
                if( proposta.getDataAlteracao()  != null ){
                	row.createCell(20).setCellValue( new HSSFRichTextString( df.format( proposta.getDataAlteracao() ) ) );   
                }else{
                	row.createCell(20).setCellValue( new HSSFRichTextString( "" ) );
                }
        		
                
                inicio++;
            	
			}
            
        }catch (Exception e) {
        	e.printStackTrace();
		}
		
		
		
		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("application/vnd.ms-excel");
			res.setHeader("Content-disposition", "attachment; filename=Rela��o_TokioMarine_Todos_Segurados_Ativos"+ dfFileName.format(new Date()) + ".xls");

		try {
			ServletOutputStream out = res.getOutputStream();
			wb.write(out);
			out.flush();
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		FacesContext faces = FacesContext.getCurrentInstance();
		faces.responseComplete();

		return "export";
	}
	
	
	public String exportReport() {
		if (radioSelected == null || radioSelected == 0) {
			addErrorMessage("Selecione uma op��o para exporta��o do arquivo de N�meros de Sorteio.");
			return null;
		}

		if( radioSelected == 3 ){
			return exportReportTodos();
		}
		// coleta a cole��o

		boolean falhou = false;

		HSSFWorkbook excelWB = null;
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		List<Proposta> propostas = propostaDAO
				.buscarPropostasAtivasPorTipoPlano(radioSelected,empresaSelected);

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dfFileName = new SimpleDateFormat("ddMMyyyy");
		try {
			InputStream excelFIS = null;
			// Verifica o tipo de plano
			if (radioSelected == 1) {
				System.out.println(Constantes.SISVIDA_FILES + "planoIndividual.xls");
				excelFIS = this.getClass().getResourceAsStream(
						Constantes.SISVIDA_FILES + "planoIndividual.xls");
			} else {
				System.out.println(Constantes.SISVIDA_FILES + "planoConjuge.xls");
				excelFIS = this.getClass().getResourceAsStream(
						Constantes.SISVIDA_FILES + "planoConjuge.xls");
			}
			
			
			excelWB = new HSSFWorkbook(excelFIS);

		} catch (Exception e) {
			falhou = true;
			addErrorMessage("O arquivo n�o foi encontrado! Favor verificar se o arquivo est� no local correto.");
		}

		HSSFSheet oneSheet = excelWB.getSheetAt(0);

		if (!falhou) {

			// Estilos
			short formatoMonetario = excelWB.createDataFormat().getFormat(
					"R$ #,##0.00;[Red]R$ #,##0.00");

			HSSFCellStyle estiloMonetario = excelWB.createCellStyle();
			estiloMonetario.setDataFormat(formatoMonetario);
			// Adicionando bordas para o CantoEsquerdo
			estiloMonetario.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloMonetario.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloMonetario.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloMonetario.setLeftBorderColor(HSSFColor.BLACK.index);
			estiloMonetario.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloMonetario.setTopBorderColor(HSSFColor.BLACK.index);
			estiloMonetario.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloMonetario.setRightBorderColor(HSSFColor.BLACK.index);

			HSSFCellStyle estiloFullBorder = excelWB.createCellStyle();
			// Adicionando bordas para o CantoEsquerdo
			estiloFullBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setLeftBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setTopBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setRightBorderColor(HSSFColor.BLACK.index);
			
			HSSFCellStyle estiloCantoEsquerdo = excelWB.createCellStyle();
			//Adicionando bordas para o CantoEsquerdo
			estiloCantoEsquerdo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoEsquerdo.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloCantoEsquerdo.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setTopBorderColor(HSSFColor.BLACK.index);
			estiloCantoEsquerdo.setVerticalAlignment( HSSFCellStyle.VERTICAL_TOP );
			estiloCantoEsquerdo.setWrapText(true);
			
			HSSFCellStyle estiloMeio = excelWB.createCellStyle();
              
           //Adicionando bordas para o Meio
			estiloMeio.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloMeio.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setTopBorderColor(HSSFColor.BLACK.index);
			
			HSSFCellStyle estiloCantoDireito = excelWB.createCellStyle();
              
           //Adicionando bordas para o CantoDireito
			estiloCantoDireito.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setTopBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setRightBorderColor(HSSFColor.GREEN.index);
			estiloCantoDireito.setDataFormat( formatoMonetario );

			int rowNumber = 7;
			HSSFRow row = oneSheet.getRow(rowNumber);
			if (row == null) {
				oneSheet.createRow(7);
				row = oneSheet.getRow(rowNumber);
			}
			for (Proposta proposta : propostas) {

				propostaDAO.populaCapital(proposta);

				if( radioSelected == 1 ){
					planoIndividual(df, estiloMonetario, estiloFullBorder, row,	proposta);
				}else{
					planoCasado(df, estiloMonetario, estiloFullBorder, row,	proposta);
				}

				rowNumber++;
				oneSheet.createRow(rowNumber);
				row = oneSheet.getRow(rowNumber);
			}

			//utiliza um contador m�ximo para o tamanho da row
			int max = 0;

			if( radioSelected == 1 ){
				max = 6;
			}else{
				max = 11;
			}
			
			rowNumber++;
			oneSheet.createRow(rowNumber);
			row = oneSheet.getRow(rowNumber);
			row.setHeight(  (short) 800 );
			row.createCell(0).setCellValue(new HSSFRichTextString("Observa��es"));
			row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
			row.createCell(max).setCellStyle( estiloCantoDireito );
			for(int j=1;j<max;j++){
				row.createCell(j);
				row.getCell( j ).setCellStyle( estiloMeio );
			}
			rowNumber++;
			rowNumber++;
			
			oneSheet.createRow(rowNumber);
			row = oneSheet.getRow(rowNumber);
			row.createCell(0).setCellValue(new HSSFRichTextString("Local e Data"));
			row.setHeight(  (short) 600 );
			row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
			row.createCell(max).setCellStyle( estiloCantoDireito );
			for(int j=1;j<max;j++){
				row.createCell(j);
				row.getCell( j ).setCellStyle( estiloMeio );
			}
			rowNumber++;
			rowNumber++;
			oneSheet.createRow(rowNumber);
			row = oneSheet.getRow(rowNumber);
			row.createCell(0).setCellValue(new HSSFRichTextString("Instru��es de Preenchimento\nPreencher:   * Campos Obrigat�rios"));
			row.setHeight(  (short) 800 );
			row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
			row.createCell(max).setCellStyle( estiloCantoDireito );
			for(int j=1;j<max;j++){
				row.createCell(j);
				row.getCell( j ).setCellStyle( estiloMeio );
			}
			rowNumber++;
			oneSheet.createRow(rowNumber);
			row = oneSheet.getRow(rowNumber);
			row.createCell(0).setCellValue(new HSSFRichTextString("Uso Interno - Protocolo Tokio Marine"));
			row.setHeight(  (short) 800 );
			row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
			row.createCell(max).setCellStyle( estiloCantoDireito );
			for(int j=1;j<max;j++){
				row.createCell(j);
				row.getCell( j ).setCellStyle( estiloMeio );
			}
			// HSSFRow header = oneSheet.getRow(0);
			// for(short column=0;column<header.getLastCellNum(); column++){
			// oneSheet.autoSizeColumn(column);
			// }

		}

		HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("application/vnd.ms-excel");
		if( radioSelected == 1 ){
			res.setHeader("Content-disposition", "attachment; filename=Rela��o_TokioMarine_PlanoIndividual"+ dfFileName.format(new Date()) + ".xls");
		}else{
			res.setHeader("Content-disposition", "attachment; filename=Rela��o_TokioMarine_PlanoCasado"+ dfFileName.format(new Date()) + ".xls");
		}

		try {
			ServletOutputStream out = res.getOutputStream();
			excelWB.write(out);
			out.flush();
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		FacesContext faces = FacesContext.getCurrentInstance();
		faces.responseComplete();

		return "export";
	}

	private void planoIndividual(SimpleDateFormat df,
			HSSFCellStyle estiloMonetario, HSSFCellStyle estiloFullBorder,
			HSSFRow row, Proposta proposta) {
		row.createCell(0).setCellValue(new HSSFRichTextString(proposta.getOrgao().getNomeOrgao()));
		row.getCell(0).setCellStyle(estiloFullBorder);
		row.createCell(1).setCellValue(new HSSFRichTextString(proposta.getNomeSegurado()));
		row.getCell(1).setCellStyle(estiloFullBorder);
		row.createCell(2).setCellValue(new HSSFRichTextString(df.format(proposta.getDataNascimentoSegurado())));
		row.getCell(2).setCellStyle(estiloFullBorder);
		String cpfSemFormatacao = "";
		if (proposta.getCpfSegurado() != null) {
			cpfSemFormatacao = proposta.getCpfSegurado().replace(".","").replace("_", "").replace("-", "");

		}
		row.createCell(3).setCellValue(new HSSFRichTextString(cpfSemFormatacao));
		row.getCell(3).setCellStyle(estiloFullBorder);

		if (proposta.getCapitalSegurado() != null) {
			row.createCell(4).setCellValue(proposta.getCapitalSegurado());
		} else {
			row.createCell(4).setCellValue(0.0);
		}
		row.getCell(4).setCellStyle(estiloMonetario);

		if (proposta.getPremioSegurado() != null) {
			row.createCell(5).setCellValue(proposta.getPremioSegurado());
		} else {
			row.createCell(5).setCellValue(0.0);
		}
		row.getCell(5).setCellStyle(estiloMonetario);

		if (proposta.getTotalPremios() != null) {
			row.createCell(6).setCellValue(proposta.getTotalPremios());
		} else {
			row.createCell(6).setCellValue(0.0);
		}
		row.getCell(6).setCellStyle(estiloMonetario);
	}

	
	private void planoCasado(SimpleDateFormat df,
			HSSFCellStyle estiloMonetario, HSSFCellStyle estiloFullBorder,
			HSSFRow row, Proposta proposta) {
		
		row.createCell(0).setCellValue(new HSSFRichTextString(proposta.getOrgao().getNomeOrgao()));
		row.getCell(0).setCellStyle(estiloFullBorder);
		row.createCell(1).setCellValue(new HSSFRichTextString(proposta.getNomeSegurado()));
		row.getCell(1).setCellStyle(estiloFullBorder);
		row.createCell(2).setCellValue(new HSSFRichTextString(df.format(proposta.getDataNascimentoSegurado())));
		row.getCell(2).setCellStyle(estiloFullBorder);
		String cpfSemFormatacao = "";
		if (proposta.getCpfSegurado() != null) {
			cpfSemFormatacao = proposta.getCpfSegurado().replace(".","").replace("_", "").replace("-", "");

		}
		row.createCell(3).setCellValue(new HSSFRichTextString(cpfSemFormatacao));
		row.getCell(3).setCellStyle(estiloFullBorder);

		row.createCell(4).setCellValue(new HSSFRichTextString(proposta.getNomeConjuge()));
		row.getCell(4).setCellStyle(estiloFullBorder);
		if( proposta.getDataNascimentoConjuge() != null ){
			row.createCell(5).setCellValue(new HSSFRichTextString(df.format(proposta.getDataNascimentoConjuge())));
		}else{
			row.createCell(5).setCellValue( new HSSFRichTextString( "" ) );
		}
		row.getCell(5).setCellStyle(estiloFullBorder);		
		
		String cpfConjugeSemFormatacao = "";
		if (proposta.getCpfConjuge() != null) {
			cpfSemFormatacao = proposta.getCpfConjuge().replace(".","").replace("_", "").replace("-", "");

		}
		row.createCell(6).setCellValue(new HSSFRichTextString(cpfConjugeSemFormatacao));
		row.getCell(6).setCellStyle(estiloFullBorder);
		
		
		if (proposta.getCapitalSegurado() != null) {
			row.createCell(7).setCellValue(proposta.getCapitalSegurado());
		} else {
			row.createCell(7).setCellValue(0.0);
		}
		row.getCell(7).setCellStyle(estiloMonetario);
		
		if (proposta.getCapitalConjuge() != null) {
			row.createCell(8).setCellValue(proposta.getCapitalConjuge());
		} else {
			row.createCell(8).setCellValue(0.0);
		}
		row.getCell(8).setCellStyle(estiloMonetario);		
		

		if (proposta.getPremioSegurado() != null) {
			row.createCell(9).setCellValue(proposta.getPremioSegurado());
		} else {
			row.createCell(9).setCellValue(0.0);
		}
		row.getCell(9).setCellStyle(estiloMonetario);
		
		if (proposta.getPremioConjuge() != null) {
			row.createCell(10).setCellValue(proposta.getPremioConjuge());
		} else {
			row.createCell(10).setCellValue(0.0);
		}
		row.getCell(10).setCellStyle(estiloMonetario);

		if (proposta.getTotalPremios() != null) {
			row.createCell(11).setCellValue(proposta.getTotalPremios());
		} else {
			row.createCell(11).setCellValue(0.0);
		}
		row.getCell(11).setCellStyle(estiloMonetario);
	}
	
	public String generateReport() {

		return "export";

	}

	public Integer getRadioSelected() {
		return radioSelected;
	}

	public void setRadioSelected(Integer radioSelected) {
		this.radioSelected = radioSelected;
	}

	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public Integer getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Integer empresaSelected) {
		this.empresaSelected = empresaSelected;
	}
	
	

	// fim getters and setters
	// ==================================================
	// =========================================

}
