package br.com.itavida.handlers;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dao.QuitacaoDAO;
import br.com.itavida.dto.ComissaoAgenciadorDto;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.PropostaAgenciador;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class RelatorioAgenciadorHandler extends ExcelGenerateHandler {

	private static final long serialVersionUID = 1L;
	
	private Funcionario agenciador;
	
	private Integer orgaoSelected;
	
	private Integer ufSelected;
	
	private List<SelectItem> opcoesUfCombo;
	
	private List<SelectItem> opcoesOrgaoCombo;
	
	private List<SelectItem> opcoesSituacaoCombo;
	
	private List<Funcionario> resultadoPesquisa_old;
	
	private List<ComissaoAgenciadorDto> resultadoPesquisa;
	
	private Double valorTotalPremio;
	
	private Double valorComissao50;
	
	private Double valorComissao25;
	
	private Double valorComissao100;
	
	private Double valorComissaoOutros;
	
	private Date inicio;

	private Date fim;
	
	private Date dataAgencimentoInicio;
	
	private Date dataAgencimentoFim;
	
	private SituacaoProposta situacaoProposta;
	
	private List<Proposta> propostasRelatorio;
	
	private Map<String, List<Proposta>> mapaPropostas = new HashMap<String, List<Proposta>>();
	
	private Integer situacaoPropostaSelected;
	
	private List<String> situacaoPropostaSelectedList = new ArrayList<String>();
	
	static final int QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES = 6;
	
	static final int INDEX_LINHA_CABECALHO = 4;
	
	
	public List<SituacaoProposta> getSituacoesProposta(List<String> situacoes) {
		List<SituacaoProposta> retorno = new ArrayList<SituacaoProposta>();
		for (String id : situacoes) {
			retorno.add(SituacaoProposta.get(new Integer(id)));	
		}
		return retorno;
	}

	@PostConstruct
	public void init() {
		limparCampos();
	}
	
	public boolean validarFormulario(ActionEvent e){
		
		if( agenciador == null || agenciador.getId() == null) {
			String message = JSFUtils.getMessageResourceString("Messages", "javax.faces.component.UIInput.REQUIRED", new Object[]{"Agenciador"});
			addErrorMessage(message);
			return false;
		}
				
		 
		return true;
	}
	
	public List<ComissaoAgenciadorDto> prepararResultado(List<Proposta> propostas) {
		valorComissao25 = 0.0;
		valorComissao50 = 0.0;
		valorComissao100 = 0.0;
		valorComissaoOutros = 0.0;
		Session session = HibernateUtil.currentSession();
		PropostaDAO dao = new PropostaDAO(session);
		dao.populaCapital(propostas);
		List<ComissaoAgenciadorDto> retorno = new ArrayList<ComissaoAgenciadorDto>();
		
		for (Proposta proposta : propostas) {
			if (proposta.getPropostaagenciador_collection() != null) {
				for (PropostaAgenciador pa : proposta.getPropostaagenciador_collection()) {
					ComissaoAgenciadorDto dto = new ComissaoAgenciadorDto();
					dto.setComando(pa.getComando());
					dto.setProposta(proposta.getId().toString());
					dto.setDataAverbacao(proposta.getDataAprovacao());
					dto.setNomeOrgao(proposta.getOrgao().getNomeOrgao());
					dto.setNomeSegurado(proposta.getNomeSegurado());				
					dto.setComissaoAdicional(null);
					dto.setValorPago(pa.getValorPagoDefinido());
					dto.setValorTotalPremio(pa.getValorTotalPremio());
					dto.setValorDescontado(0.0);
					dto.setDataAverbacao(proposta.getDataAprovacao());
					dto.setSituacaoAverbacao(getPosicaoAverbacao(proposta.getDataAprovacao()));
					
					if (pa.getTipoCalculoUtilizado().equals(Constantes.CALCULAR_AUTOMATICAMENTE)) {
						dto.setPercentual(pa.getFuncionario().getPercentualComissao());
						dto.setBaseCalculo(dto.getValorTotalPremio()-dto.getValorDescontado());
						dto.setValorComissao(dto.getBaseCalculo() * dto.getPercentual()/100);
					} else if (pa.getTipoCalculoUtilizado().equals(Constantes.DEFINIR_VALOR_PAGAR)) {
						dto.setPercentual(null);
						dto.setBaseCalculo(null);
						dto.setValorComissao(pa.getValorPagoDefinido());
					} else if (pa.getTipoCalculoUtilizado().equals(Constantes.COMISSAO_ADICIONAL)) {
						dto.setPercentual(pa.getPercentual());
						dto.setBaseCalculo(pa.getValorTotalPremio());
						dto.setValorComissao(pa.getValorTotalPremio()*pa.getPercentual()/100);
					}
					
					if(dto.getPercentual() != null) {
						if(dto.getPercentual().equals(100.0)) {
							valorComissao100 += dto.getValorComissao();						
						} else if(dto.getPercentual().equals(50.0)) {
							valorComissao50 += dto.getValorComissao();
						 }else if(dto.getPercentual().equals(25.0)) {
							 valorComissao25 += dto.getValorComissao();
						 } else {
							 valorComissaoOutros += dto.getValorComissao();
						 }
					} else {
						 valorComissaoOutros += dto.getValorComissao();
					}
						
					retorno.add(dto);
				}
			}
		}
		Collections.sort(retorno);
		return retorno;
	}
	
	public Double getValorTotalComissao() {
		return valorComissao25 + valorComissao50 + valorComissao100 + valorComissaoOutros;
	}
	
	public void verificarAgenciador() {
		
		//suggestionBoxFuncionario_selection
		Object obj = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("txtAgenciador");
		
		boolean nomeVazio = obj == null || obj.toString().trim().isEmpty();
		if(nomeVazio) {
			agenciador = new Funcionario();
		}
	}
	
	public void gerarRelatorio() {		
		verificarAgenciador();
		Session session = HibernateUtil.currentSession();
		mapaPropostas = new HashMap<String, List<Proposta>>();
		PropostaDAO dao = new PropostaDAO(session);
 		Integer agenciadorId = agenciador != null ? agenciador.getId() : null;
		propostasRelatorio = dao.buscarPropostas(getSituacoesProposta(situacaoPropostaSelectedList), empresaSelectedList, ufSelected, orgaoSelected, agenciadorId, true, null, inicio, fim, dataAgencimentoInicio, dataAgencimentoFim, false);
		propostasRelatorio = dao.populaCapital(propostasRelatorio);
		mapaPropostas.clear();
		mapaPropostas.put("PLANILHA DE AGENCIAMENTO", propostasRelatorio);
		super.gerarRelatorio(mapaPropostas.keySet(), INDEX_LINHA_CABECALHO, "Relat�rio de Agenciadores.xls");
	}
	
	public void gerarRelatorioCartonagem() {
		verificarAgenciador();
		mapaPropostas = new HashMap<String, List<Proposta>>();
		Session session = HibernateUtil.currentSession();
		PropostaDAO dao = new PropostaDAO(session);
 		Integer agenciadorId = agenciador != null ? agenciador.getId() : null;
		
		propostasRelatorio = dao.buscarPropostas(getSituacoesProposta(situacaoPropostaSelectedList), empresaSelectedList, ufSelected, orgaoSelected, agenciadorId, true, null, inicio, fim, dataAgencimentoInicio, dataAgencimentoFim, false);
		propostasRelatorio = dao.populaCapital(propostasRelatorio);
		mapaPropostas.clear();
		mapaPropostas.put("PLANILHA DE CARTONAGEM", propostasRelatorio);
		
		RelatorioCartonagemHandler relatorioCartonagemHandler = new RelatorioCartonagemHandler();
		
		relatorioCartonagemHandler.gerarRelatorio(mapaPropostas);
	}
	
	public void limparCampos() {
		situacaoPropostaSelected = -1;
		agenciador = new Funcionario();
		orgaoSelected = null;
		ufSelected = null;
		totalRegistrosRelatorio = 0;
		valorTotalPremio = 0.0;
		inicio = null;
		fim = null;
		
		valorComissao25 = 0.0;
		valorComissao50 = 0.0;
		
		populaOpcoesOrgaoCombo();
		populaOpcoesUfCombo();
		populaOpcoesSituacaoCombo();
		populaOpcoesEmpresaCombo(false);
		populaOpcoesSituacaoPropostaCombo();
	}
	
	public void atualizarCombo(ActionEvent e) {
		populaOpcoesOrgaoCombo();
	}
	
	public void atualizarCombo(ValueChangeEvent e) {
		populaOpcoesOrgaoCombo();
	}
	
	/**
	 * Popula um combo de empresas.
	 *  
	 * @return
	 */
	public void populaOpcoesOrgaoCombo(){
		 
		List<Orgao> orgaos = buscarOrgaosPorRegiao();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesOrgaoCombo = new ArrayList<SelectItem>();
		opcoesOrgaoCombo.add( new SelectItem( null, "--Todos--" ) );
		for (Orgao orgao : orgaos) {
			opcoesOrgaoCombo.add( new SelectItem( orgao.getId(), orgao.getNomeOrgao()) );
		}
	}
	
	/**
	 * Popula um combo de situacaoProposta.
	 *  
	 * @return
	 */
	public void populaOpcoesSituacaoCombo() {
		 
		List<SituacaoProposta> situacoes = Arrays.asList(SituacaoProposta.values());
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesSituacaoCombo = new ArrayList<SelectItem>();
		opcoesSituacaoCombo.add( new SelectItem( null, "--Todas--" ) );
		for (SituacaoProposta situacaoPropposta : situacoes) {
			opcoesSituacaoCombo.add( new SelectItem( situacaoPropposta, situacaoPropposta.getNome()) );
		}
	}

	public List<Orgao> buscarOrgaosPorRegiao() {
		Session session = HibernateUtil.currentSession();
		OrgaoDAO orgaoDAO = new OrgaoDAO(session);
		
		if(ufSelected != null && ufSelected > 0) {
			return orgaoDAO.buscarPorEstado(ufSelected);
		} else if (ufSelected != null && ufSelected < 0) {
			return orgaoDAO.buscarOrgaosVariosEstados();
		} else {
			return new ArrayList<Orgao>();
		}
	}
	
	/**
	 * Popula um combo de UF
	 */
	public void populaOpcoesUfCombo() {
		
		Session session = HibernateUtil.currentSession();
		CidadeDAO cidadeDAO = new CidadeDAO(session);
		
		List<Estado> estados = cidadeDAO.obterTodosEstados();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesUfCombo = new ArrayList<SelectItem>();
		opcoesUfCombo.add( new SelectItem( null, "--Todos--" ) );
		for (Estado estado : estados) {
			opcoesUfCombo.add( new SelectItem( estado.getCod_estado(), estado.getNom_estado()) );
		}
		
	}
	
	public String getPosicaoAverbacao(Date dataAverbacao) {
		if(dataAverbacao != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dataAverbacao);
			int mes = cal.get(Calendar.MONTH);
			int ano = cal.get(Calendar.YEAR);
			return String.format("Averbado em %d/%d", mes, ano); 
		}
		return "";
	}


	public Funcionario getAgenciador() {
		return agenciador;
	}

	public void setAgenciador(Funcionario agenciador) {
		this.agenciador = agenciador;
	}

	public Integer getOrgaoSelected() {
		return orgaoSelected;
	}

	public Integer getUfSelected() {
		return ufSelected;
	}

	public void setUfSelected(Integer ufSelected) {
		this.ufSelected = ufSelected;
	}

	public void setOrgaoSelected(Integer orgaoSelected) {
		this.orgaoSelected = orgaoSelected;
	}

	public List<SelectItem> getOpcoesUfCombo() {
		return opcoesUfCombo;
	}

	public void setOpcoesUfCombo(List<SelectItem> opcoesUfCombo) {
		this.opcoesUfCombo = opcoesUfCombo;
	}

	public List<SelectItem> getOpcoesOrgaoCombo() {
		return opcoesOrgaoCombo;
	}

	public void setOpcoesOrgaoCombo(List<SelectItem> opcoesOrgaoCombo) {
		this.opcoesOrgaoCombo = opcoesOrgaoCombo;
	}

	public Integer getSituacaoPropostaSelected() {
		return situacaoPropostaSelected;
	}

	public void setSituacaoPropostaSelected(Integer situacaoPropostaSelected) {
		this.situacaoPropostaSelected = situacaoPropostaSelected;
	}

	public List<String> getSituacaoPropostaSelectedList() {
		return situacaoPropostaSelectedList;
	}

	public void setSituacaoPropostaSelectedList(
			List<String> situacaoPropostaSelectedList) {
		this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
	}

	public List<Funcionario> getResultadoPesquisa_old() {
		return resultadoPesquisa_old;
	}

	public void setResultadoPesquisa_old(List<Funcionario> listaResultado) {
		this.resultadoPesquisa_old = listaResultado;
	}
	
	public List<ComissaoAgenciadorDto> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<ComissaoAgenciadorDto> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	private Integer totalRegistrosRelatorio;
	
	
	public Integer getTotalRegistrosRelatorio() {
		return totalRegistrosRelatorio;
	}

	public void setTotalRegistrosRelatorio(Integer totalRegistrosRelatorio) {
		this.totalRegistrosRelatorio = totalRegistrosRelatorio;
	}
	
	public Double getValorTotalPremio() {
		return valorTotalPremio;
	}

	public void setValorTotalPremio(Double valorTotalRegistrosRelatorio) {
		this.valorTotalPremio = valorTotalRegistrosRelatorio;
	}
	
	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}
	public Double getValorComissao50() {
		return valorComissao50;
	}

	public void setValorComissao50(Double valorComissao50) {
		this.valorComissao50 = valorComissao50;
	}

	public Double getValorComissao25() {
		return valorComissao25;
	}

	public void setValorComissao25(Double valorComissao25) {
		this.valorComissao25 = valorComissao25;
	}

	public Double getValorComissao100() {
		return valorComissao100;
	}

	public void setValorComissao100(Double valorComissao100) {
		this.valorComissao100 = valorComissao100;
	}

	public Double getValorComissaoOutros() {
		return valorComissaoOutros;
	}

	public void setValorComissaoOutros(Double valorComissaoOutros) {
		this.valorComissaoOutros = valorComissaoOutros;
	}

	public Date getDataAgencimentoInicio() {
		return dataAgencimentoInicio;
	}

	public void setDataAgencimentoInicio(Date dataAgencimentoInicio) {
		this.dataAgencimentoInicio = dataAgencimentoInicio;
	}

	public Date getDataAgencimentoFim() {
		return dataAgencimentoFim;
	}

	public void setDataAgencimentoFim(Date dataAgencimentoFim) {
		this.dataAgencimentoFim = dataAgencimentoFim;
	}

	public SituacaoProposta getSituacaoProposta() {
		return situacaoProposta;
	}

	public void setSituacaoProposta(SituacaoProposta situacaoProposta) {
		this.situacaoProposta = situacaoProposta;
	}

	public List<SelectItem> getOpcoesSituacaoCombo() {
		return opcoesSituacaoCombo;
	}

	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		int indiceLinha = 0;
		addRow(aba);
		addTextoMesclado(aba, getStiloTextoTitulo(), "ITAVIDA CLUBE DE SEGUROS", indiceLinha, 1, 0, QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES);
		addRow(aba);
		indiceLinha++;
		String dataAtual = new SimpleDateFormat("dd/MM/yy").format(new Date()) ;
		addTextoMesclado(aba, getStiloTextoTitulo(), "PLANILHA DE AGENCIAMENTO - MG - " + dataAtual , indiceLinha, 1, 0, QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES);
		addRow(aba);
		indiceLinha++;
		addRow(aba);
		indiceLinha++;

		// addCelulaTexto(aba, getStiloTextoCabecalho(), "PROPOSTA");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "AGENCIADOR");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "NOME DO SEGURADO");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "COMANDO");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "NOME DO �RG�O");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "DE");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "PARA");
		// addCelulaTexto(aba, getStiloTextoCabecalho(), "PERCENTUAL");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "A PAGAR");		
	}

	/*private Double getValorCapital(Proposta proposta) {
		Double valorCapital = 0.0;
		if (proposta.getCapitalSegurado() != null) {
			valorCapital += proposta.getCapitalSegurado();
		}
		
		if (proposta.getCapitalConjuge() != null ) {
			valorCapital += proposta.getCapitalConjuge();
		}
		return valorCapital;
	}*/
	
	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		Double contador = 1d;
		Double total = 0.0;
		
       	for(Proposta proposta : mapaPropostas.get(aba)) {
       		addRow(aba);
       		
       		Double de = proposta.getTotalPremios();
       		if (de == null) {
       			de = 0.0;
       		}
       		Double para = proposta.getTotalPremiosFuturo();
       		if (para == null) {
       			para = 0.0;
       		}
       		Double percentual = 0.0;
       		
       		DecimalFormat df = new DecimalFormat();
       		df.setMaximumFractionDigits(2);
       		
       		Collection<PropostaAgenciador> propostaAgenciadores = proposta.getPropostaagenciador_collection();
       		PropostaAgenciador propostaAgenciador = propostaAgenciadores.iterator().hasNext() ? propostaAgenciadores.iterator().next() : null;
       		String nomeAgenciador = propostaAgenciador != null && propostaAgenciador.getFuncionario() != null ? propostaAgenciador.getFuncionario().getNome() : "";
       		// String comando = propostaAgenciador != null  ? propostaAgenciador.getComando() : "";
       		// String comando = proposta.getComandoComissao() != null && proposta.getComandoComissao().equals("0")  ? "Inclus�o" : "Altera��o";
       		
       		if (propostaAgenciador.getTipoCalculoUtilizado().equals(Constantes.CALCULAR_AUTOMATICAMENTE)) {
       			percentual = propostaAgenciador.getFuncionario().getPercentualComissao();
			} else if (propostaAgenciador.getTipoCalculoUtilizado().equals(Constantes.DEFINIR_VALOR_PAGAR)) {
				try {
					percentual = propostaAgenciador.getValorPagoDefinido() / propostaAgenciador.getValorTotalPremio() * 100.0;
				} catch (Exception e) {
					// percentual continua sendo 0.0
				}
			} else if (propostaAgenciador.getTipoCalculoUtilizado().equals(Constantes.COMISSAO_ADICIONAL)) {
				percentual = propostaAgenciador.getPercentual();
			}
       		
       		// addCelulaNumero(aba, getStiloTextoSimples(),  proposta.getId().doubleValue());
       		// addCelulaTexto(aba, getStiloTextoSimples(),  proposta.getId().toString());
       		addCelulaTexto(aba, getStiloTextoSimples(), nomeAgenciador);
       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getNomeSegurado(), "-");
       		// addCelulaTexto(aba, getStiloTextoSimples(), comando);
       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getComandoComissaoExibicao());
       		addCelulaTexto(aba, getStiloTextoSimples(), proposta.getOrgao() !=null ? proposta.getOrgao().getNomeOrgao() : null, "-");
       		addCelulaNumero(aba, getStiloMonetarioSimples(), de);
       		addCelulaNumero(aba, getStiloMonetarioSimples(), para);
       		// addCelulaTexto(aba, getStiloMonetarioSimples(), String.format("%s%s", percentual, "%"));
       		double valorComissao = 0.0;
       		if (proposta.getSuperAp() != null) {
       			if (proposta.getSuperAp().getPeriodicidade().equals("0")) {
       				valorComissao = proposta.getTotalPremios()*50.0/100.0;
       			} else {
       				valorComissao = proposta.getTotalPremios()*10.0/100.0;
       			}
       			
       			addCelulaNumero(aba, getStiloMonetarioSimples(), valorComissao);
       		} else {
	       		if(para != null && percentual != null) {
	       			valorComissao = para * percentual/100.0;
	       		}
	       		addCelulaNumero(aba, getStiloMonetarioSimples(), valorComissao);
       		}
   			contador++;
   			total += valorComissao;
       	}
       	// Adiciona total a pagar
       	addRow(aba);
       	Integer linhaCelulaTotal = contador.intValue() - INDEX_LINHA_CABECALHO;
       	addTextoMesclado(aba, getStiloTextoSimples(), JSFUtils.getMessageResourceString("Messages","total", null), linhaCelulaTotal, 1, 0, QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES - 1);
  		addCelulaNumero(aba, getStiloMonetarioSimples(),  total);
	}

}