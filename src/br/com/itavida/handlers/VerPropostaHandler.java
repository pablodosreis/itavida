package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.FilialDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.Filial;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class VerPropostaHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private Proposta proposta;
	private String idadeSegurado;

	public VerPropostaHandler() {
		String idProposta = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
		Session session = HibernateUtil.currentSession();
		PropostaDAO pDao = new PropostaDAO(session);
		proposta = pDao.load(Integer.parseInt(idProposta));
		pDao.populaCapital(proposta);
		calculaIdadeSeguradoPrincipal();
	}
	
	public void calculaIdadeSeguradoPrincipal(){
		Date dataNascimento = getProposta().getDataNascimentoSegurado();
		DateTime start = new DateTime(dataNascimento);
		DateTime end = new DateTime(Calendar.getInstance());
		
		if (dataNascimento != null) {
			 Period per = new Period (start, end);
		        // Isto imprime "27 years, 6 months, 3 weeks and 3 days"
		        System.out.println (PeriodFormat.getDefault().print (per));
		        // Isto imprime "27 anos, 6 meses, 3 semanas e 3 dias"
		        PeriodFormatter pf = new PeriodFormatterBuilder()
				    .appendYears()
					.appendSuffix (" ano, ", " anos, ")
		            .appendMonths() 
		            .appendSuffix (" m�s, ", " meses, ")
		            .appendWeeks() 
		            .appendSuffix (" semana e ", " semanas e ")
		            .appendDays()
		            .appendSuffix (" dia ", " dias ")
		            .toFormatter();
		     
		     setIdadeSegurado( pf.print (per));
			 proposta.setIdadeRealSegurado( per.getYears() );
		} else {
			setIdadeSegurado("");
		}
	}
	
	public Proposta getProposta() {
		return proposta;
	}


	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}
	
	public String getEstadoCivilSegurado() {
		Map<String,String> mapa = new HashMap<String,String>();
		mapa.put( "CA", "Casado" );
		mapa.put( "CO", "Companheiro");
		mapa.put( "DE", "Desquitado");
		mapa.put( "DI", "Divorciado");
		mapa.put( "OT", "Outros" );
		mapa.put( "SE", "Separado");
		mapa.put( "SO", "Solteiro");
		mapa.put( "VI", "Vi�vo" );	
		return mapa.get(proposta.getEstadoCivilSegurado());
	}
	
	public String getEstadoCivilConjuge() {
		Map<String,String> mapa = new HashMap<String,String>();
		mapa.put( "CA", "Casado" );
		mapa.put( "CO", "Companheiro");
		mapa.put( "DE", "Desquitado");
		mapa.put( "DI", "Divorciado");
		mapa.put( "OT", "Outros" );
		mapa.put( "SE", "Separado");
		mapa.put( "SO", "Solteiro");
		mapa.put( "VI", "Vi�vo" );	
		return mapa.get(proposta.getEstadoCivilConjuge());
	}
	
	public String getSexoSegurado() {
		if (proposta.getSexoSegurado() == null) {
			return null;
		}
		
		if(proposta.getSexoSegurado().equals("M")) {
			return "Masculino";
		} else if (proposta.getSexoSegurado().equals("F")) {
			return "Feminino";
		} else {
			return null;
		}
	}
	
	public String getSexoConjuge() {
		if (proposta.getSexoConjuge() == null) {
			return null;
		}
		
		if(proposta.getSexoConjuge().equals("M")) {
			return "Masculino";
		} else if (proposta.getSexoConjuge().equals("F")) {
			return "Feminino";
		} else {
			return null;
		}
	}

	public String getIdadeSegurado() {
		return idadeSegurado;
	}

	public void setIdadeSegurado(String idadeSegurado) {
		this.idadeSegurado = idadeSegurado;
	}

	
	
}
