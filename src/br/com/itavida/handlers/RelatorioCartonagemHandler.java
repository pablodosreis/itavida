package br.com.itavida.handlers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;

public class RelatorioCartonagemHandler extends ExcelGenerateHandler {

	private static final long serialVersionUID = 1L;

	private Map<String, List<Proposta>> mapaPropostas = new HashMap<String, List<Proposta>>();

	static final int QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES = 8;

	static final int INDEX_LINHA_CABECALHO = 4;

	int indiceLinha = 0;

	Empresa empresaSeleted;

	public void gerarRelatorio(Map<String, List<Proposta>> mapaPropostasLocal) {
		mapaPropostas = mapaPropostasLocal;
		super.gerarRelatorio(mapaPropostas.keySet(), INDEX_LINHA_CABECALHO,
				"Relat�rio de Cartonagem.xls");
	}

	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		criarCabecalho(wb, aba, false);
	}

	public void criarCabecalho(HSSFWorkbook wb, String aba,
			boolean cabacalhoAlteracoes) {
		addRow(aba);
		int quantidadeColunas = QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES;
		if (cabacalhoAlteracoes) {
			quantidadeColunas++;
		}
		addTextoMesclado(aba, getStiloTextoTitulo(),
				String.format("MIRANDA REIS ASS E CORR DE SEGUROS LTDA"),
				indiceLinha, 1, 0, quantidadeColunas);
		addRow(aba);
		indiceLinha++;
		String dataAtual = new SimpleDateFormat("dd/MM/yy").format(new Date());

		// se est� no cabeca�ho de altera��es
		if (cabacalhoAlteracoes) {
			addTextoMesclado(aba, getStiloTextoTitulo(),
					"AGENCIAMENTO SOBRE O PROTOCOLO DA CARTONAGEM - "
							+ dataAtual + " >>> PARA ALTERA��ES", indiceLinha,
					1, 0, quantidadeColunas);
		} else {
			addTextoMesclado(aba, getStiloTextoTitulo(),
					"AGENCIAMENTO SOBRE O PROTOCOLO DA CARTONAGEM - "
							+ dataAtual, indiceLinha, 1, 0, quantidadeColunas);
		}

		addRow(aba);
		indiceLinha++;
		addRow(aba);
		indiceLinha++;

		// addCelulaTexto(aba, getStiloTextoCabecalho(), "PROPOSTA");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "NOME DO SEGURADO");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "NOME DO �RG�O");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "NOME DA EMPRESA");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "PLANO");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "DT NASC");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "CPF");
		addCelulaTexto(aba, getStiloTextoCabecalho(), "CAPITAL");

		if (cabacalhoAlteracoes) {
			// addCelulaTexto(aba, getStiloTextoCabecalho(), "VL ANT");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "VL ATUAL");

		} else {
			addCelulaTexto(aba, getStiloTextoCabecalho(), "TOTAL");
		}
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Apolice");
	}

	private Double getValorCapital(Proposta proposta) {
		// busca apenas o capital do segurado
		Double valorCapital = 0.0;
		if (proposta.getCapitalSegurado() != null) {
			valorCapital += proposta.getCapitalSegurado();
		}
		return valorCapital;
	}

	private Double getValorAtual(Proposta proposta) {
		return proposta.getPremioSeguradoFuturo();
	}

	private Double getValorTotal(Proposta proposta) {
		Double valorTotal = 0.0;
		if (proposta.getPremioSegurado() != null) {
			valorTotal += proposta.getPremioSegurado();
		}
		if (proposta.getPremioConjuge() != null) {
			valorTotal += proposta.getPremioConjuge();
		}
		return valorTotal;
	}

	private Double getValorApolice(Proposta proposta) {
		Double valorApolice = 0.0;
		if (!StringUtils.isEmpty(proposta.getEmpresa().getNumApolice())) {
			valorApolice = Double
					.valueOf(proposta.getEmpresa().getNumApolice());
		}
		return valorApolice;
	}

	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		Double contador = 1d;
		Double totalInsercoes = 0.0;

		// ///////////////////////////////////////////////////////////////////////////////////////
		// adiciona propostas INSERIDAS...
		// ///////////////////////////////////////////////////////////////////////////////////////
		for (Proposta proposta : mapaPropostas.get(aba)) {
			// Collection<PropostaAgenciador> propostaAgenciadores =
			// proposta.getPropostaagenciador_collection();
			// PropostaAgenciador propostaAgenciador =
			// propostaAgenciadores.iterator().hasNext() ?
			// propostaAgenciadores.iterator().next() : null;

			// if (propostaAgenciador.getComando().equalsIgnoreCase("Inclus�o"))
			// {
			// if (proposta.getComandoComissao().equalsIgnoreCase("0")) {
			if (proposta.getComandoComissao() == null
					|| !proposta.getComandoComissao().equalsIgnoreCase("1")) {

				addRow(aba);

				Double de = proposta.getTotalPremios();
				Double para = proposta.getTotalPremiosFuturo();
				if (de == null)
					de = 0.0;
				if (para == null)
					para = 0.0;

				// addCelulaNumero(aba, getStiloTextoSimples(),
				// proposta.getId().doubleValue());
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getNomeSegurado());
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getOrgao() != null ? proposta.getOrgao()
								.getNomeOrgao() : null, "-");
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getEmpresa() != null ? proposta.getEmpresa()
								.getNomeEmpresa() : null, "-");
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getTipoProposta() != null ? proposta
								.getTipoProposta().getDescricao() : "");
				addCelulaData(aba, getStiloDataSimples(),
						proposta.getDataNascimentoSegurado(), "");
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getCpfSegurado());

				addCelulaNumero(aba, getStiloMonetarioSimples(),
						getValorCapital(proposta));
				Double valorTotal = getValorTotal(proposta);
				addCelulaNumero(aba, getStiloMonetarioSimples(), valorTotal);
				addCelulaNumero(aba, getStiloTextoSimples(),
						getValorApolice(proposta));

				contador++;
				totalInsercoes += valorTotal;
			}
		}
		// Adiciona total a pagar de propostas inseridas
		/*
		 * addRow(aba); Integer linhaCelulaTotal = contador.intValue() -
		 * INDEX_LINHA_CABECALHO; addTextoMesclado(aba, getStiloTextoSimples(),
		 * JSFUtils.getMessageResourceString("Messages","total", null),
		 * linhaCelulaTotal, 1, 0, QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES -
		 * 2); addCelulaNumero(aba, getStiloMonetarioSimples(), totalInsercoes);
		 */

		addRow(aba);
		contador++;
		// ///////////////////////////////////////////////////////////////////////////////////////
		// adiciona propostas ALTERADAS...
		// ///////////////////////////////////////////////////////////////////////////////////////
		Double totalAlteracoes = 0.0;

		contador = contador + INDEX_LINHA_CABECALHO;
		indiceLinha = contador.intValue();
		criarCabecalho(wb, aba, true);

		for (Proposta proposta : mapaPropostas.get(aba)) {
			// Collection<PropostaAgenciador> propostaAgenciadores =
			// proposta.getPropostaagenciador_collection();
			// PropostaAgenciador propostaAgenciador =
			// propostaAgenciadores.iterator().hasNext() ?
			// propostaAgenciadores.iterator().next() : null;

			// if (proposta.getComandoComissao().equalsIgnoreCase("1")) {
			// if (proposta.getSituacao() == SituacaoProposta.PENDENTE) {
			if (proposta.getComandoComissao().equalsIgnoreCase("1")) {

				addRow(aba);

				Double de = proposta.getTotalPremios();
				Double para = proposta.getTotalPremiosFuturo();
				if (de == null)
					de = 0.0;
				if (para == null)
					para = 0.0;

				// addCelulaNumero(aba, getStiloTextoSimples(),
				// proposta.getId().doubleValue());
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getNomeSegurado());
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getOrgao() != null ? proposta.getOrgao()
								.getNomeOrgao() : null, "-");
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getEmpresa() != null ? proposta.getEmpresa()
								.getNomeEmpresa() : null, "-");
				if (proposta.getSuperAp() == null) {
					addCelulaTexto(aba, getStiloTextoSimples(),
							proposta.getTipoProposta() != null ? proposta
									.getTipoProposta().getDescricao() : "");
				} else if (proposta.getSuperAp() != null) {
					if (proposta.getSuperAp().getPeriodicidade().equals("0")) {
						addCelulaTexto(aba, getStiloTextoSimples(), "Mensal");
					} else {
						addCelulaTexto(aba, getStiloTextoSimples(), "Anual");
					}
				}
				addCelulaData(aba, getStiloDataSimples(),
						proposta.getDataNascimentoSegurado(), "");
				addCelulaTexto(aba, getStiloTextoSimples(),
						proposta.getCpfSegurado());

				addCelulaNumero(aba, getStiloMonetarioSimples(),
						getValorCapital(proposta));
				Double valorAtual = getValorAtual(proposta);
				addCelulaNumero(aba, getStiloMonetarioSimples(), valorAtual,
						"-");
				addCelulaNumero(aba, getStiloTextoSimples(),
						getValorApolice(proposta));

				contador++;
				if (valorAtual != null) {
					totalAlteracoes += valorAtual;
				}
			}
		}
		// Adiciona total a pagar de propostas inseridas
		/*
		 * addRow(aba); linhaCelulaTotal = contador.intValue() -
		 * INDEX_LINHA_CABECALHO; addTextoMesclado(aba, getStiloTextoSimples(),
		 * JSFUtils.getMessageResourceString("Messages","total", null),
		 * linhaCelulaTotal, 1, 0, QUANTIDADE_COLUNAS_RELATORIO_AGENCIADORES -
		 * 2); addCelulaNumero(aba, getStiloMonetarioSimples(),
		 * totalAlteracoes);
		 */

	}

}