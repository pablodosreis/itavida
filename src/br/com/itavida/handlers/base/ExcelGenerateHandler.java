package br.com.itavida.handlers.base;


import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.Session;

import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.handlers.MainHandler;
import br.com.itavida.util.HibernateUtil;

/**
 * Classe para controlar as fun��es b�sicas de gera��o de planilha Excel.
 * 
 * @author Pablo
 * 
 */
public abstract class ExcelGenerateHandler extends MainHandler implements
		Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final String VALOR_VAZIO_PADRAO = "-";

	protected HSSFWorkbook workbook;

	private Map<String, Integer> sheetIndex = new HashMap<String, Integer>();

	private Map<String, HSSFRow> sheetRows = new HashMap<String, HSSFRow>();
	
	private HSSFFont fontTitulo;
	
	private HSSFFont fontCabecalho;
	
	private HSSFFont fontCabecalho2;
	
	private HSSFFont fontSimples;
	
	private HSSFCellStyle estiloTitulo;
	
	private HSSFCellStyle estiloCabecalho;
	
	private HSSFCellStyle estiloCabecalho2;
	
	private HSSFCellStyle estiloSimples;
	
	private HSSFCellStyle estiloMonetarioSimples;
	
	private HSSFCellStyle estiloDataSimples;
	
	private static final String NOME_PADRAO_ARQUIVO = "Relatorio.xls";

	public void criarWorkbook(Collection<String> abas) {
		this.workbook = new HSSFWorkbook();
		for (String titulo : abas) {
			workbook.createSheet(titulo);
			sheetIndex.put(titulo, 0);
			sheetRows.put(titulo, null);
		}
		
		fontTitulo = null;
		fontCabecalho = null;
		fontSimples = null;
		estiloTitulo = null;
		estiloCabecalho = null;
		estiloSimples = null;
		estiloMonetarioSimples = null;
		estiloDataSimples = null;
	}

	public abstract void criarCabecalho(HSSFWorkbook wb, String aba);

	public abstract void preencherAba(HSSFWorkbook wb, String aba);

	public void addSheet(String titulo) {
		workbook.createSheet(titulo);
	}
	/**
	 * 
	 * @param aba
	 * @param estilo
	 * @param valor
	 * @param indiceLinha
	 * @param linhasMescladas
	 * @param indiceColuna
	 * @param colunasMescladas
	 */
	public void addTextoMesclado(String aba, HSSFCellStyle estilo, String valor, int indiceLinha, int linhasMescladas, int indiceColuna, int colunasMescladas ) {
		addCelula(aba, estilo, valor, indiceLinha, linhasMescladas, indiceColuna, colunasMescladas);
	}
	
	public void addCelula(String aba, HSSFCellStyle estilo, String valor, int indiceLinha, int linhasMescladas, int indiceColuna, int colunasMescladas ) {
		HSSFRow row = sheetRows.get(aba);
		HSSFSheet sheet = workbook.getSheet(aba);
		int lastCell = row.getLastCellNum();
		lastCell = lastCell > -1 ? lastCell : 0;
		HSSFRichTextString richValue = new HSSFRichTextString(valor);
		HSSFCell celula = row.createCell(lastCell);
		celula.setCellValue(richValue);
		celula.setCellStyle(estilo);
		HSSFCell celula1 = null;
		for(int i=0; i < colunasMescladas - indiceColuna; i++) {
			int indexCell = row.getLastCellNum();
			celula1 = row.createCell(indexCell);
			celula1.setCellValue(i);
			adicionarBordas(estilo);
			celula1.setCellStyle(estilo);
		}
		
		if(celula1 != null) {
			CellRangeAddress cellRangeAddress2 = new CellRangeAddress(celula.getRowIndex(), celula1.getRowIndex(), celula.getColumnIndex(), celula1.getColumnIndex());
			sheet.addMergedRegion(cellRangeAddress2);
		}
	}
	
	private HSSFCell addCelula(String aba, HSSFCellStyle estilo) {
		HSSFRow row = sheetRows.get(aba);
		int lastCell = row.getLastCellNum();
		lastCell = lastCell > -1 ? lastCell : 0;
		HSSFCell celula = row.createCell(lastCell);
		celula.setCellStyle(estilo);
		return celula;
	}
	
//	public void addCelulaTexto(String aba, HSSFCellStyle estilo, String valor, String valorVazio) {
//		if(valor != null && !valor.isEmpty()) {
//			addCelulaTexto(aba, estilo, valor);
//		} else {
//			addCelulaTexto(aba, estilo, valorVazio);
//		}
//	}
	
	public void addCelulaData(String aba, HSSFCellStyle estilo, Date valor, String valorVazio) {
		if(valor != null) {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
   			String data = format.format(valor);
			addCelulaTexto(aba, estilo, data);
		} else {
			addCelulaTexto(aba, estilo, valorVazio);
		}
	}
	
	public void addCelulaNumero(String aba, HSSFCellStyle estilo, Double valor, String valorVazio) {
		if(valor != null) {
			addCelulaNumero(aba, estilo, valor);
		} else {
			estilo.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			addCelulaTexto(aba, estilo, valorVazio);
		}
	}
	public void addCelulaNumeroInteiro(String aba, HSSFCellStyle estilo, Integer valor, String valorVazio) {
		addCelulaNumeroInteiro(aba, estilo, valor != null ? valor.longValue() : null, valorVazio);
	}
	
	public void addCelulaNumeroInteiro(String aba, HSSFCellStyle estilo, Long valor, String valorVazio) {
		if(valor != null) {
			HSSFCell celula = addCelula(aba, estilo);
			celula.setCellValue(valor);
		} else {
			estilo.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			addCelulaTexto(aba, estilo, valorVazio);
		}
	}
	
	public void addCelulaTexto(String aba, HSSFCellStyle estilo, String valor) {
		addCelulaTexto(aba, estilo, valor, "");
	}
	
	public void addCelulaTexto(String aba, HSSFCellStyle estilo, String valor, String valorVazio) {
		addCelulaTexto(aba, estilo, valor, valorVazio, false);
	}
	
	public void addCelulaTexto(String aba, HSSFCellStyle estilo, String valor, String valorVazio, boolean quebrarTexto) {
		if(valor == null || valor.isEmpty()) {
			valor = valorVazio;
		}
		estilo.setWrapText(quebrarTexto);
		HSSFCell celula = addCelula(aba, estilo);
		HSSFRichTextString richValue = new HSSFRichTextString(valor);
		celula.setCellValue(richValue);
		
	}
	
	public void addCelulaNumero(String aba, HSSFCellStyle estilo, Double valor) {
		if(valor != null) {
			HSSFCell celula = addCelula(aba, estilo);
			celula.setCellValue(valor);
		} else {
			estilo.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			addCelulaTexto(aba, estilo, VALOR_VAZIO_PADRAO);
		}
		
	}

	public void addRow(String aba) {
		HSSFSheet sheet = this.workbook.getSheet(aba);
		Integer indice = sheetIndex.get(aba);
		HSSFRow row = sheet.createRow(indice);
		sheetRows.put(aba, row);
		sheetIndex.put(aba, indice + 1);
	}

	public void ajustarColunas(HSSFSheet sheet, int linhaCabecalho) {
		HSSFRow header = sheet.getRow(linhaCabecalho);
		for (short column = 0; column < header.getLastCellNum(); column++) {
			sheet.autoSizeColumn(column);
		}
		
		HSSFRow titulo = sheet.getRow(0);
		titulo.setHeight((short) (titulo.getHeight() * 3));
	}

	public void gerarRelatorio(Collection<String> abas, int linhaCabecalho) {
		gerarRelatorio(abas, linhaCabecalho, NOME_PADRAO_ARQUIVO);
	}
	
	public void gerarRelatorio(int linhaCabecalho, String nomeArquivo) {
		gerarRelatorio(Arrays.asList("Relat�rio Sisvida"), linhaCabecalho, nomeArquivo);
	}

	public void gerarRelatorio(Collection<String> abas, int linhaCabecalho, String nomeArquivo) {
		criarWorkbook(abas);
		for (String aba : abas) {
			criarCabecalho(this.workbook, aba);
			preencherAba(workbook, aba);
			ajustarColunas(workbook.getSheet(aba), linhaCabecalho);
		}
		prepararResponse(this.workbook, nomeArquivo);
	}
	
	

	public void prepararResponse(HSSFWorkbook wb, String nomeArquivo) {
		HttpServletResponse res = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("application/vnd.ms-excel");
		res.setHeader("Content-disposition",
				String.format("attachment; filename=%s", nomeArquivo));

		try {
			ServletOutputStream out = res.getOutputStream();
			wb.write(out);
			out.flush();
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		FacesContext faces = FacesContext.getCurrentInstance();
		faces.responseComplete();

	}

	public void adicionarBordas(HSSFCellStyle estilo) {
		estilo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		estilo.setBottomBorderColor(HSSFColor.BLACK.index);
		
		estilo.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		estilo.setLeftBorderColor(HSSFColor.BLACK.index);
		
		estilo.setBorderTop(HSSFCellStyle.BORDER_THIN);
		estilo.setTopBorderColor(HSSFColor.BLACK.index);
		
		estilo.setBorderRight(HSSFCellStyle.BORDER_THIN);
		estilo.setRightBorderColor(HSSFColor.BLACK.index);
	}

	public HSSFFont getFontTitulo() {
		if (fontTitulo == null) {
			fontTitulo = this.workbook.createFont();
			fontTitulo.setFontName("Arial");
			fontTitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			fontTitulo.setColor((short) HSSFColor.DARK_GREEN.index);
			fontTitulo.setFontHeightInPoints((short) 14);
		}
		return fontTitulo;
	}
	
	
	public HSSFFont getFontCabecalho() {
		if (fontCabecalho == null) {
			fontCabecalho = this.workbook.createFont();
			fontCabecalho.setFontName("Arial");
			fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			fontCabecalho.setColor(HSSFColor.DARK_GREEN.index );
			fontCabecalho.setFontHeightInPoints((short) 12);
		}
		return fontCabecalho;
	}
	
	public HSSFFont getFontCabecalho2() {
		if (fontCabecalho2 == null) {
			fontCabecalho2 = this.workbook.createFont();
			fontCabecalho2.setFontName("Arial");
			fontCabecalho2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			fontCabecalho2.setColor(HSSFColor.DARK_YELLOW.index );
			fontCabecalho2.setFontHeightInPoints((short) 12);
		}
		return fontCabecalho2;
	}

	public HSSFFont getFontSimples() {
		if (fontSimples == null) {
			fontSimples = this.workbook.createFont();
			fontSimples.setFontName("Arial");
			fontSimples.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
			fontSimples.setColor((short) HSSFColor.BLACK.index);
			fontSimples.setFontHeightInPoints((short) 10);
		}
		return fontSimples;
	}
	
	public HSSFCellStyle getStiloTextoTitulo2() {
		if(estiloTitulo == null) {
			estiloTitulo = this.workbook.createCellStyle();
			estiloTitulo.setFont(getFontCabecalho2());
			estiloTitulo.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			estiloTitulo.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			estiloTitulo.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			estiloTitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			estiloTitulo.setWrapText(true);
		}
		return estiloTitulo;
	}
	
	public HSSFCellStyle getStiloTextoTitulo() {
		if(estiloTitulo == null) {
			estiloTitulo = this.workbook.createCellStyle();
			estiloTitulo.setFont(getFontCabecalho());
			estiloTitulo.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			estiloTitulo.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			estiloTitulo.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			estiloTitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			estiloTitulo.setWrapText(true);
		}
		return estiloTitulo;
	}

	
	public HSSFCellStyle getStiloTextoCabecalho() {
		if(estiloCabecalho == null) {
			estiloCabecalho = this.workbook.createCellStyle();
			estiloCabecalho.setFont(getFontCabecalho());
			adicionarBordas(estiloCabecalho);
			estiloCabecalho.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			estiloCabecalho.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			estiloCabecalho.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			estiloCabecalho.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		}
		return estiloCabecalho;
	}
	
	public HSSFCellStyle getStiloTextoCabecalho2() {
		if(estiloCabecalho2 == null) {
			estiloCabecalho2 = this.workbook.createCellStyle();
			estiloCabecalho2.setFont(getFontCabecalho2());
			adicionarBordas(estiloCabecalho2);
			estiloCabecalho2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
			estiloCabecalho2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			estiloCabecalho2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			estiloCabecalho2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		}
		return estiloCabecalho2;
	}

	public HSSFCellStyle getStiloTextoSimples() {
		if (estiloSimples == null) {
			estiloSimples = this.workbook.createCellStyle();
			estiloSimples.setFont(getFontSimples());
			adicionarBordas(estiloSimples);
		}
		return estiloSimples;
	}

	public HSSFCellStyle getStiloMonetarioSimples() {
		if (estiloMonetarioSimples == null) {
			short formatoMonetario = workbook.createDataFormat().getFormat(
					"R$ #,##0.00;[Red]R$ #,##0.00");
			estiloMonetarioSimples = this.workbook.createCellStyle();
			estiloMonetarioSimples.setDataFormat(formatoMonetario);
			estiloMonetarioSimples.setFont(getFontSimples());
			adicionarBordas(estiloMonetarioSimples);
		}
		return estiloMonetarioSimples;
	}

	public HSSFCellStyle getStiloDataSimples() {
		if(estiloDataSimples == null) {
			estiloDataSimples = this.workbook.createCellStyle();
			estiloDataSimples.setFont(getFontSimples());
			adicionarBordas(estiloDataSimples);
		}
		return estiloDataSimples;
	}
	
	public String getCNPJ(String aba) {
		if(aba.toLowerCase().indexOf("itaprevent") >= 0) {
			return "*CNPJ: 11.086.060/0001-95";
		}
		return "*CNPJ: 02.669.204/0001-45";
	}
	
	public String getNumeroApolice(String aba) {
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO = new EmpresaDAO(session);
		return empresaDAO.getNumerApolice(aba);
	}

}
