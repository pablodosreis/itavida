package br.com.itavida.handlers.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.BeneficiarioDAO;
import br.com.itavida.dao.CapitalSeguradoFaixaEtariaDAO;
import br.com.itavida.dao.CapitalSeguradoLimiteIdadeDAO;
import br.com.itavida.dao.ComissaoAgenciadorDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.HistoricoBeneficiarioDAO;
import br.com.itavida.dao.HistoricoCapitalDAO;
import br.com.itavida.dao.HistoricoConjugeDAO;
import br.com.itavida.dao.HistoricoEnderecoSeguradoDAO;
import br.com.itavida.dao.HistoricoPropostaDAO;
import br.com.itavida.dao.MotivoCancelamentoDAO;
import br.com.itavida.dao.NomeTabelaDAO;
import br.com.itavida.dao.NumeroSorteioDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaAgenciadorDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dao.TipoPropostaDAO;
import br.com.itavida.entidades.Beneficiario;
import br.com.itavida.entidades.CapitalSeguradoFaixaEtaria;
import br.com.itavida.entidades.CapitalSeguradoLimiteIdade;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.ComissaoAgenciador;
import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.GrauParentesco;
import br.com.itavida.entidades.HistoricoBeneficiario;
import br.com.itavida.entidades.HistoricoCapital;
import br.com.itavida.entidades.HistoricoConjuge;
import br.com.itavida.entidades.HistoricoEnderecoSegurado;
import br.com.itavida.entidades.HistoricoProposta;
import br.com.itavida.entidades.ModeloProposta;
import br.com.itavida.entidades.MotivoCancelamento;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.entidades.NumeroSorteio;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.PropostaAgenciador;
import br.com.itavida.entidades.TipoProposta;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.handlers.MainHandler;
import br.com.itavida.handlers.PropostaHandler;
import br.com.itavida.handlers.UsuarioHandler;
import br.com.itavida.util.Comparador;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

@SuppressWarnings("serial")
public class NovaPropostaHandler extends MainHandler implements Serializable {

private static Logger logger = Logger.getLogger( PropostaHandler.class );

}
