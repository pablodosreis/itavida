package br.com.itavida.handlers;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.primefaces.event.IdleEvent;

import br.com.itavida.dao.UsuarioDAO;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.util.HibernateUtil;

public class UsuarioHandler extends MainHandler implements Serializable{
	
	/**
	 * Default Serial ID
	 */
	private static final long serialVersionUID = 1L;
	private String mensagemRetorno;
	private Usuario usuario;
	private HtmlInputText inputText;
	private boolean timeOver;
	
	/* A mensagem de sess�o expirada � exibido ap�s 119 minutos,
	 * o tempo m�ximo de sess�o da aplica��o no web.xml(servidor/aplica��o)
	 * deve ser configurado para 120 minutos, desta forma evita-se que a sess�o
	 * j� esteja expirada quando o sistema solicitar a apresenta��o da mensagem.
	 */
	private Integer tempoSessao = 119 * 60 * 1000; //120 minutos
	
	public UsuarioHandler() {
		setUsuario( new Usuario() );
		mensagemRetorno = null;
	}
	
	
	
	public String autenticar(){
		Session session = HibernateUtil.currentSession();
		UsuarioDAO  usuarioDAO = new UsuarioDAO(session);
		Usuario usuarioRecuperado = usuarioDAO.busca(  getUsuario() );
		if( usuarioRecuperado == null ){
			addErrorMessage("Usu�rio Inv�lido!");
			return null;
		}
		
		if( usuarioRecuperado != null && !usuarioRecuperado.getSenha().equals( getUsuario().getSenha() ) ){
			addErrorMessage("Senha Inv�lida!");
			return null;
		}
		
		setUsuario( usuarioRecuperado );
		
		/*if (usuarioRecuperado.getPerfil().equals("FIL")) {
			return "propostaConsultar";
		}*/
		
		return "telaInicial";
	}
	
	public void logout(ActionEvent e) {  
	    FacesContext facesContext = FacesContext.getCurrentInstance();  
	    HttpSession session = (HttpSession) facesContext .getExternalContext().getSession(false);  
	    session.removeAttribute( "usuarioHandler" );
	    session.removeAttribute("_security_filter_applied");
	    session.invalidate();  
	}
	
	public void logoutAction (ActionEvent e) {  
	    FacesContext facesContext = FacesContext.getCurrentInstance();  
	    HttpSession session = (HttpSession) facesContext .getExternalContext().getSession(false);  
	    session.removeAttribute( "usuarioHandler" );
	    session.removeAttribute("_security_filter_applied");
	    session.invalidate();
	    try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsp");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
	}
	
	public Object dadosAction(ActionEvent e) {
		
	
		HttpServletResponse res = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("application/json");
		//res..setResponseCharacterEncoding("UTF-8");
		

		try {
			ServletOutputStream out = res.getOutputStream();
			
			
			out.write("Hello ".getBytes());
			
			out.flush();
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		FacesContext faces = FacesContext.getCurrentInstance();
		faces.responseComplete();
		
		return "{'abc' : '1'}" ;  

	}
	
	

	
	public String goProposta( ){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().setAttribute( "fluxo", "fastSearch" );
		request.getSession().setAttribute( "idPropostaFastSearch", inputText.getValue() );
		return "proposta";
	}
	
	public void goVisualizarProposta( ActionEvent e ){
		//do nothing;
	}
	
    public void idleListener(IdleEvent event) {  
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,   
                                        "Sua sess�o foi encerrada", "O sistema ficou inativo por mais de 120 minutos. Por favor, efetue login novamente!"));  
       
        timeOver = true;
        
    }  
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public HtmlInputText getInputText() {
		return inputText;
	}

	public void setInputText(HtmlInputText inputText) {
		this.inputText = inputText;
	}

	public boolean isTimeOver() {
		return timeOver;
	}

	public void setTimeOver(boolean timeOver) {
		this.timeOver = timeOver;
	}

	public Integer getTempoSessao() {
		return tempoSessao;
	}

	public void setTempoSessao(Integer tempoSessao) {
		this.tempoSessao = tempoSessao;
	}



	public String getMensagemRetorno() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Object attribute = request.getAttribute("mensagemRetorno");
		String mensagemRetorno = attribute != null ? attribute.toString() : null;
		return mensagemRetorno;
	}
	
}
