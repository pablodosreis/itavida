package br.com.itavida.handlers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.ContatoOrgaoDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.ContatoOrgao;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class OrgaoHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private Orgao orgao;
	private List<Orgao> resultadoPesquisa;
	private HtmlSelectOneMenu tipoOrgaoSelecionado;
	private List<Cidade> listaCidades;
	private ContatoOrgao contatoOrgao;
	private List<ContatoOrgao> contatosExcluidos;
	private Orgao[] orgaosSelecionados;
	
	// Getters and Setters
	
	public Orgao getOrgao() {
		return orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public List<Orgao> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Orgao> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public HtmlSelectOneMenu getTipoOrgaoSelecionado() {
		return tipoOrgaoSelecionado;
	}

	public void setTipoOrgaoSelecionado(HtmlSelectOneMenu tipoOrgaoSelecionado) {
		this.tipoOrgaoSelecionado = tipoOrgaoSelecionado;
	}

	public List<Cidade> getListaCidades() {
		return listaCidades;
	}

	public void setListaCidades(List<Cidade> listaCidades) {
		this.listaCidades = listaCidades;
	}

	public ContatoOrgao getContatoOrgao() {
		return contatoOrgao;
	}

	public void setContatoOrgao(ContatoOrgao contatoOrgao) {
		this.contatoOrgao = contatoOrgao;
	}


	public List<ContatoOrgao> getContatosExcluidos() {
		return contatosExcluidos;
	}

	public void setContatosExcluidos(List<ContatoOrgao> contatosExcluidos) {
		this.contatosExcluidos = contatosExcluidos;
	}	
	
	public Orgao[] getOrgaosSelecionados() {
		return orgaosSelecionados;
	}

	public void setOrgaosSelecionados(Orgao[] orgaosSelecionados) {
		this.orgaosSelecionados = orgaosSelecionados;
	}
	
	// Fim Getters and Setters
	





	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public OrgaoHandler(){
		cancelar( null );
	}
	

	/**
	 * Popula um combo com valores dos tipos de org�o
	 * @return
	 */
	public List<SelectItem> getTipoOrgaoCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "E", "Estadual" ) );
		retorno.add( new SelectItem( "F", "Federal" ) );
		retorno.add( new SelectItem( "M", "Municipal" ) );
		retorno.add( new SelectItem( "P", "Privado" ) );
		
		return retorno;
	}	

	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaOrgaos(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		OrgaoDAO dao = new OrgaoDAO( session );
		
		Orgao orgaoFiltro = getOrgao();
		
		Map<String, String> mapa = montaMapaTipoOrgao();
		
		List<Orgao> lista = dao.buscaPorFiltro( orgaoFiltro  );
		
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			Orgao orgao = (Orgao) iterator.next();
			orgao.setTipoOrgaoDescricao( mapa.get( orgao.getTipoOrgao() ) );
		}
		setVisibleMessage( false );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao(getOrgao().getCidade().getId(), "Cidade") ){
		
			Dao<Orgao, Integer> dao = new Dao<Orgao, Integer>(session, Orgao.class);
			Dao<ContatoOrgao, Integer> daoContato = new Dao<ContatoOrgao, Integer>(session, ContatoOrgao.class);
			Orgao orgaoGravar = getOrgao();
			
			for (Iterator iterator = orgaoGravar.getContatoorgao_collection().iterator(); iterator.hasNext();) {
				ContatoOrgao element = (ContatoOrgao) iterator.next();				
				element.setOrgao( orgaoGravar );
			}
			
			//Exclui os contatos marcados para exclus�o
			for (Iterator iterator = getContatosExcluidos().iterator(); iterator.hasNext();) {
				ContatoOrgao contatoExcluido = (ContatoOrgao) iterator.next();	
				daoContato.delete( contatoExcluido );
			}
			//Zera a lista de contatos excluidos
			setContatosExcluidos( new ArrayList<ContatoOrgao>() );
			
			//persiste os dados
			dao.merge( orgaoGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}else{
			//Limpa a lista de cidades;
			listaCidades = null;
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeOrgao", "Nome do Org�o");
		mapaCampos.put("cmbTipoOrgao", "Tipo de Org�o");
		mapaCampos.put("txtCgc", "CGC");
		mapaCampos.put("txtNomeCidade", "Cidade");
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormularioDetalheContato(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeContato", "Nome do Contato");
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarOrgao( ){
		
		//Recupera o item escolhido
		Session session = HibernateUtil.currentSession();
		ContatoOrgaoDAO contatoDAO =  new ContatoOrgaoDAO( session );
		List<ContatoOrgao> listaContatos = contatoDAO.buscaPorOrgao( orgao.getId() );
		getOrgao().setContatoorgao_collection(listaContatos);
		resultadoPesquisa = null;
		setVisibleMessage( false );
		
		return "incluirOrgao";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirOrgao(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirOrgao");
		Orgao orgaoDeletado  = ( Orgao ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<Orgao, Integer> dao = new Dao<Orgao, Integer>(session, Orgao.class);
		dao.delete(orgaoDeletado);	
		
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaOrgaos(e);
	}	
	
	
	/**
	 * Realiza a inclus�o de um novo contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void incluirContatoOrgao( ActionEvent e ){
		
		if( validarFormularioDetalheContato(e) ){
			
			getOrgao().getContatoorgao_collection().remove( getContatoOrgao() );
			getOrgao().getContatoorgao_collection().add( getContatoOrgao() );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_OPERACAO_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelarContatoOrgao(e);
		}else{
			listaCidades = null;
		}
	}
	
	/**
	 * Realiza a exclus�o de um contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void excluirContatoOrgao( ActionEvent e ){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirContatoOrgao");
		ContatoOrgao contatoOrgao = ( ContatoOrgao ) param.getValue();
		if( contatosExcluidos == null ){
			contatosExcluidos = new ArrayList<ContatoOrgao>();
		}
		getContatosExcluidos().add( contatoOrgao );
		getOrgao().getContatoorgao_collection().remove( contatoOrgao );
						
	}
	
	
	/**
	 * Realiza a altera��o de um contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void alterarContatoOrgao( ActionEvent e ){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("editContatoOrgao");
		ContatoOrgao contatoOrgao = ( ContatoOrgao ) param.getValue();
		setContatoOrgao( contatoOrgao );
						
	}
	
	/**
	 * Realiza a inclus�o de um novo contato org�o que ser� persistido no gravar
	 * @param e
	 */
	public void cancelarContatoOrgao( ActionEvent e ){		
		setContatoOrgao( new ContatoOrgao() );	
		setVisibleMessage( false );
		listaCidades = null;
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setOrgao( new Orgao() );
		getOrgao().setCidade(new Cidade());
		setContatoOrgao( new ContatoOrgao() );
		getOrgao().setContatoorgao_collection( new ArrayList<ContatoOrgao>() );
		contatosExcluidos = new ArrayList<ContatoOrgao>();
		this.resultadoPesquisa = null;
		setVisibleMessage( false );
		
	}

	public Cidade getLimparCidade(){
		setVisibleMessage( false );
		return new Cidade();
		
	}
	
	
public String generateMalaDireta(  ){
		
        try {  

        	FileInputStream excelFIS = new FileInputStream("C:\\SisvidaFiles\\etiqueta.xls");
		    HSSFWorkbook wb = new HSSFWorkbook(excelFIS);
            HSSFSheet sheet = wb.getSheetAt(0); 
              
            HSSFRow row;  
            
            
            int rowCountEsquerdo = 1;
            int rowCountDireito = 1;
            row = sheet.getRow(rowCountEsquerdo);	
			HSSFFont fontCabecalho = wb.createFont();  
            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); 
            if(  orgaosSelecionados != null && orgaosSelecionados.length > 0 ){
            
	            for (int i = 0; i < orgaosSelecionados.length; i++) {
	            	Orgao orgao = orgaosSelecionados[i];
	            	HSSFRichTextString richTextNomeSegurado = new HSSFRichTextString( orgao.getNomeOrgao() ); 
	            	HSSFRichTextString richTextEndereco = new HSSFRichTextString( orgao.getEndereco() );
	            	HSSFRichTextString richTextBairro = new HSSFRichTextString( orgao.getBairro() );
	            	HSSFRichTextString richCEPCidade = new HSSFRichTextString( orgao.getCep() + " / " + orgao.getCidade().getNom_cidade() + " - " + orgao.getCidade().getCod_estado().getSgl_estado()  );
	            	if( i % 2 == 0){
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richTextNomeSegurado); 		            
		            	rowCountEsquerdo++;
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richTextEndereco); 		            
		            	rowCountEsquerdo++;	
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richTextBairro); 		            
		            	rowCountEsquerdo++;			
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richCEPCidade); 		            
		            	rowCountEsquerdo++;	
		            	rowCountEsquerdo++;	
		            	rowCountEsquerdo++;	
		            	rowCountEsquerdo++;	
		            }
	            	if( i % 2 != 0){
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richTextNomeSegurado); 		            
			            rowCountDireito++;
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richTextEndereco); 		            
			            rowCountDireito++;	
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richTextBairro); 		            
			            rowCountDireito++;			
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richCEPCidade); 		            
			            rowCountDireito++;	
			            rowCountDireito++;	
			            rowCountDireito++;	
			            rowCountDireito++;	
		            }
				}	          
	            
	            
	            HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();  
	            res.setContentType("application/vnd.ms-excel");  
	            res.setHeader("Content-disposition",  "attachment; filename=MalaDiretaOrgaos.xls");  
	 
	            
	           try {  
	                 ServletOutputStream out = res.getOutputStream();  
	                 wb.write(out);  
	                 out.flush();  
	                 out.close();  
	           } catch (IOException ex) {   
	                   ex.printStackTrace();  
	           }  
	           
	           FacesContext faces = FacesContext.getCurrentInstance();  
	           faces.responseComplete();  
  
            }else{
            	addErrorMessage("Selecione um ou mais �rga�s(s) para Gera��o do Arquivo de Impress�o." );
            }
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        
        return "export";
		
	}	
	
	
}
