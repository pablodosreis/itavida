package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.CapitalSeguradoLimiteIdadeDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.entidades.CapitalSeguradoLimiteIdade;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class CapitalSeguradoLimiteIdadeHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private CapitalSeguradoLimiteIdade capitalSeguradoLimiteIdade;
	private List<CapitalSeguradoLimiteIdade> resultadoPesquisa;

	private List<NomeTabela> listaNomesTabelas;
	
	// Getters and Setters
	
	public List<NomeTabela> getListaNomesTabelas() {
		return listaNomesTabelas;
	}


	public void setListaNomesTabelas(List<NomeTabela> listaNomesTabelas) {
		this.listaNomesTabelas = listaNomesTabelas;
	}
	
	
	public CapitalSeguradoLimiteIdade getCapitalSeguradoLimiteIdade() {
		return capitalSeguradoLimiteIdade;
	}


	public void setCapitalSeguradoLimiteIdade(CapitalSeguradoLimiteIdade capitalSeguradoLimiteIdade) {
		this.capitalSeguradoLimiteIdade = capitalSeguradoLimiteIdade;
	}
	
	
	public List<CapitalSeguradoLimiteIdade> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<CapitalSeguradoLimiteIdade> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	// Fim Getters and Setters
	


	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public CapitalSeguradoLimiteIdadeHandler(){
		cancelar( null );
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaCapitaisSegurados(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		CapitalSeguradoLimiteIdadeDAO dao = new CapitalSeguradoLimiteIdadeDAO( session );
		
		CapitalSeguradoLimiteIdade capitalFiltro = getCapitalSeguradoLimiteIdade();
		
		List<CapitalSeguradoLimiteIdade> lista = dao.buscaPorFiltro( capitalFiltro  );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao(getCapitalSeguradoLimiteIdade().getNomeTabela().getId(), "Nome da Tabela") ){
		
			Dao<CapitalSeguradoLimiteIdade, Integer> dao = new Dao<CapitalSeguradoLimiteIdade, Integer>(session, CapitalSeguradoLimiteIdade.class);
			CapitalSeguradoLimiteIdade capitalGravar = getCapitalSeguradoLimiteIdade();
			
			dao.merge( capitalGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelarIncluir(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeTabela", "Nome da Tabela");
		mapaCampos.put("dtDataVigencia", "Data de Vig�ncia");
		mapaCampos.put("txtFunMorteNatural", "Funcion�rio: Morte Natural");
		mapaCampos.put("txtFuncMorteAcidente", "Funcion�rio: Morte Acidental");
		mapaCampos.put("txtFuncInvalidezPerm", "Funcion�rio: Invalidez Permanente");
		mapaCampos.put("txtFuncLimiteIdade", "Funcion�rio: Limite de Idade");
		
		mapaCampos.put("txtFuncVLIndividual", "Funcion�rio: Valor Individual");
		mapaCampos.put("txtFuncVLCasado50", "Funcion�rio: Valor Casado 50%");
		mapaCampos.put("txtFuncVLCasado100", "Funcion�rio: Valor Casado 100%");
		
		mapaCampos.put("txtConjMorteNatural", "C�njuge: Morte Natural");
		mapaCampos.put("txtConjMorteAcidente", "C�njuge: Morte Acidental");
		mapaCampos.put("txtConjInvalidezPerm", "C�njuge: Invalidez Permanente");
		mapaCampos.put("txtConjLimiteIdade", "C�njuge: Limite de Idade");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarCapitalSegurado( ){
		

		resultadoPesquisa = null;
		
		return "incluirCapitalLimiteIdade";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirCapitalSegurado(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirCapitalSegurado");
		CapitalSeguradoLimiteIdade capitalDeletado  = ( CapitalSeguradoLimiteIdade ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<CapitalSeguradoLimiteIdade, Integer> dao = new Dao<CapitalSeguradoLimiteIdade, Integer>(session, CapitalSeguradoLimiteIdade.class);
		dao.delete(capitalDeletado);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaCapitaisSegurados(e);
	}	
	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setCapitalSeguradoLimiteIdade( new CapitalSeguradoLimiteIdade() );
		getCapitalSeguradoLimiteIdade().setNomeTabela(new NomeTabela());
		this.resultadoPesquisa = null;
		
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelarIncluir( ActionEvent e ){
		CapitalSeguradoLimiteIdade novoCapital = new CapitalSeguradoLimiteIdade();
		novoCapital.setNomeTabela( getCapitalSeguradoLimiteIdade().getNomeTabela() );
		
		// Realiza o cancelamento das a��es
		setCapitalSeguradoLimiteIdade( novoCapital );
		this.resultadoPesquisa = null;
		
	}
	
	public NomeTabela getLimparNomeTabela(){
		return new NomeTabela();
	}
	
}
