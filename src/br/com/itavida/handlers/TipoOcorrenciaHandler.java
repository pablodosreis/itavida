package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.TipoOcorrenciaDAO;
import br.com.itavida.entidades.TipoOcorrencia;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class TipoOcorrenciaHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private TipoOcorrencia tipoOcorrencia;
	private List<TipoOcorrencia> resultadoPesquisa;
	private HtmlSelectOneMenu sinistroNegadoSelecionado;
	
	// Getters and Setters
	

	
	public TipoOcorrencia getTipoOcorrencia() {
		return tipoOcorrencia;
	}


	public void setTipoOcorrencia(TipoOcorrencia tipoOcorrencia) {
		this.tipoOcorrencia = tipoOcorrencia;
	}


	public List<TipoOcorrencia> getResultadoPesquisa() {
		return resultadoPesquisa;
	}


	public void setResultadoPesquisa(List<TipoOcorrencia> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public HtmlSelectOneMenu getSinistroNegadoSelecionado() {
		return sinistroNegadoSelecionado;
	}


	public void setSinistroNegadoSelecionado(
			HtmlSelectOneMenu sinistroNegadoSelecionado) {
		this.sinistroNegadoSelecionado = sinistroNegadoSelecionado;
	}



	// Fim Getters and Setters
	


	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public TipoOcorrenciaHandler(){
		cancelar( null );
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaTipoOcorrenciasPorNomeOuSinistro(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		TipoOcorrenciaDAO dao = new TipoOcorrenciaDAO(session);
		
		TipoOcorrencia tipoOcorrenciaFiltro = getTipoOcorrencia();
		if( getSinistroNegadoSelecionado() != null && getSinistroNegadoSelecionado().getValue()!= null ){
			tipoOcorrenciaFiltro.setSinistroNegado( getSinistroNegadoSelecionado().getValue().toString() );
		}

		List<TipoOcorrencia> lista = dao.busca( tipoOcorrenciaFiltro.getDescricao(), tipoOcorrenciaFiltro.getSinistroNegado()  );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<TipoOcorrencia, Integer> dao = new Dao<TipoOcorrencia, Integer>(session, TipoOcorrencia.class);
			TipoOcorrencia tipoOcorrenciaGravar = getTipoOcorrencia();
			tipoOcorrenciaGravar.setSinistroNegado( getSinistroNegadoSelecionado().getValue().toString() );
			
			dao.merge( tipoOcorrenciaGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtDescricao", "Descri��o");
		mapaCampos.put("cmbSinistroNegado", "Sinistro Negado");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarTipoOcorrencia( ){
		
		//Recupera o item escolhido
	
		//Realiza as a��es de banco: Busca e seta os valores
		sinistroNegadoSelecionado = new HtmlSelectOneMenu();
		sinistroNegadoSelecionado.setValue( getTipoOcorrencia().getSinistroNegado() );
		
		resultadoPesquisa = null;
		
		return "incluirTipoOcorrrencia";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirTipoOcorrencia(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirTipoOcorrencia");
		TipoOcorrencia tipoOcorrenciaDeletada = ( TipoOcorrencia ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<TipoOcorrencia, Integer> dao = new Dao<TipoOcorrencia, Integer>(session, TipoOcorrencia.class);
		dao.delete(tipoOcorrenciaDeletada);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaTipoOcorrenciasPorNomeOuSinistro(e);
	}	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setTipoOcorrencia( new TipoOcorrencia() );
		this.resultadoPesquisa = null;
		
	}

	
}
