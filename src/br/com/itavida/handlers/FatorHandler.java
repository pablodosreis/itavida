package br.com.itavida.handlers;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.hibernate.Session;

import br.com.itavida.dao.CapitalSeguradoFaixaEtariaDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.FatorDAO;
import br.com.itavida.entidades.CapitalSeguradoFaixaEtaria;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Fator;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class FatorHandler extends MainHandler implements Serializable {

	private Fator fator = new Fator();
	private NomeTabela nomeTabela = new NomeTabela();
	private List<Fator> resultadoPesquisa;
	private List<NomeTabela> listaNomesTabelas;
	private List<SelectItem> listaCapitaisFaixaExtarias = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu capitalSegurado;
	private Integer idCapitalSeguradoFaixaEtaria;
	private CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria;

	public FatorHandler() {
		cancelar(null);
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<Fator, Integer> dao = new Dao<Fator, Integer>(session, Fator.class);
				Fator fator = new Fator();
				fator.setCapitalSeguradoFaixaEtaria(capitalSeguradoFaixaEtaria);
				fator.setDataInicio(this.fator.getDataInicio());
				fator.setFator(this.fator.getFator());
				fator.setProcessado(false);
				fator.setId(this.fator.getId());
				dao.merge(fator);
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	public void carregaComboCapitais () {
		listaCapitaisFaixaExtarias.clear();
		Session session = HibernateUtil.currentSession();
		CapitalSeguradoFaixaEtariaDAO capitalSeguradoFaixaEtariaDAO = new CapitalSeguradoFaixaEtariaDAO(session);
		List<CapitalSeguradoFaixaEtaria> capitaisSegurados = capitalSeguradoFaixaEtariaDAO.buscaPorNomeTabela(nomeTabela);
		listaCapitaisFaixaExtarias.add(new SelectItem(-1, ""));
		for (CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria : capitaisSegurados) {
			listaCapitaisFaixaExtarias.add(new SelectItem(capitalSeguradoFaixaEtaria.getId(), capitalSeguradoFaixaEtaria.getNomeTabela().getNomeTabela() + " - Vig�ncia: " + new SimpleDateFormat("dd/MM/yyyy").format(capitalSeguradoFaixaEtaria.getDataVigencia())));
		}
		
	}
	
	public void prepararAlterarFator (ActionEvent e){
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("alterarFator");
		Fator fatorDeletado = ( Fator ) param.getValue();
		if (fatorDeletado.isProcessado()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_VALIDACAO_FATOR_PROCESSADO, null);
			addErrorMessage(message);
		} 
	}	
	
	public String prepararAlterarFator2 (){
		carregaComboCapitais();
		setIdCapitalSeguradoFaixaEtaria(capitalSeguradoFaixaEtaria.getId());
		return "incluirFator";
	}
	
	public void excluirFator(ActionEvent e){
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirFator");
		Fator fatorDeletado = ( Fator ) param.getValue();
		if (fatorDeletado.isProcessado()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_VALIDACAO_FATOR_PROCESSADO, null);
			addErrorMessage(message);
		} else {
			Session session = HibernateUtil.currentSession();
			Dao<Fator, Integer> dao = new Dao<Fator, Integer>(session, Fator.class);
			dao.delete(fatorDeletado);
			// Dispara novamente a pesquisa para manter o v�nculo
			pesquisaFatores(e);
		}
	}	

	
	public void pesquisaFatores(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();

		FatorDAO fatorDAO = new FatorDAO(session);
		
		if (idCapitalSeguradoFaixaEtaria != null && !idCapitalSeguradoFaixaEtaria.equals(-1)) {
			Dao<CapitalSeguradoFaixaEtaria, Integer> dao = new Dao<CapitalSeguradoFaixaEtaria, Integer>(session, CapitalSeguradoFaixaEtaria.class);
			capitalSeguradoFaixaEtaria = dao.get(idCapitalSeguradoFaixaEtaria);
		} else {
			capitalSeguradoFaixaEtaria = null;
		}
		
		List<Fator> fatorLocal = fatorDAO.buscarPorDataInicioListagem(fator.getDataInicio(), capitalSeguradoFaixaEtaria, nomeTabela);
		if (fatorLocal != null && !fatorLocal.isEmpty()) {
			resultadoPesquisa = new ArrayList<Fator>();
			resultadoPesquisa.addAll(fatorLocal);
		} else {
			resultadoPesquisa = new ArrayList<Fator>();
		}
		if (resultadoPesquisa == null || resultadoPesquisa.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
		}
		
//		List<Fator> lista = dao.buscarPorDataInicio(getFator().getDataInicio());
//		setResultadoPesquisa(lista);
//		if (lista == null || lista.isEmpty()) {
//			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
//			addWarnMessage( message );
//		}
			
	}

	
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setFator(new Fator());
		setNomeTabela(new NomeTabela());
		listaCapitaisFaixaExtarias = new ArrayList<SelectItem>();
		//this.resultadoPesquisa = null;
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeTabela", "Nome da Tabela");
		mapaCampos.put("txtFator", "Fator");
		mapaCampos.put("dataInicioFator", "Data de in�cio");
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);

		if (fator.getDataInicio() != null && !fator.getDataInicio().after(Calendar.getInstance().getTime())) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_VALIDACAO_DATA_INICIO_MAIOR_ATUAL, null);
			addErrorMessage(message);
			retorno = false;
		}
		Session session = HibernateUtil.currentSession();

		if (idCapitalSeguradoFaixaEtaria == null || idCapitalSeguradoFaixaEtaria.equals(-1)) {
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_VALIDACAO_CAMPO_OBRIGATORIO, new Object[]{"Capital Segurado"});
			addErrorMessage(message);
			retorno = false;
		} else {;
			Dao<CapitalSeguradoFaixaEtaria, Integer> dao = new Dao<CapitalSeguradoFaixaEtaria, Integer>(session, CapitalSeguradoFaixaEtaria.class);
			capitalSeguradoFaixaEtaria = dao.get(idCapitalSeguradoFaixaEtaria);
		}
		
		
		if (retorno) {

			FatorDAO fatorDAO = new FatorDAO(session);
			Fator fatorLocal = fatorDAO.buscarPorDataInicio(fator.getDataInicio(), capitalSeguradoFaixaEtaria);
			if (fatorLocal != null) {
				if (fatorLocal.isProcessado()) {
					String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_VALIDACAO_FATOR_EXISTENTE_PROCESSADO, new Object[]{new SimpleDateFormat("dd/MM/yyyy").format(fator.getDataInicio())});
					addErrorMessage(message);
					retorno = false;
				} else {
					fator.setId(fatorLocal.getId());
				}
			}
			

		}
		 
		return retorno;
	}
	
	public void teste () {
		System.out.println("ok");
	}

	public Fator getFator() {
		return fator;
	}

	public void setFator(Fator fator) {
		this.fator = fator;
	}

	public List<Fator> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Fator> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public void setListaNomesTabelas(List<NomeTabela> listaNomesTabelas) {
		this.listaNomesTabelas = listaNomesTabelas;
	}
	
	public List<NomeTabela> getListaNomesTabelas() {
		return listaNomesTabelas;
	}

	public NomeTabela getNomeTabela() {
		return nomeTabela;
	}

	public void setNomeTabela(NomeTabela nomeTabela) {
		this.nomeTabela = nomeTabela;
	}
	
	public NomeTabela getLimparNomeTabela(){
		return new NomeTabela();
	}

	public List<SelectItem> getListaCapitaisFaixaExtarias() {
		return listaCapitaisFaixaExtarias;
	}

	public void setListaCapitaisFaixaExtarias(
			List<SelectItem> listaCapitaisFaixaExtarias) {
		this.listaCapitaisFaixaExtarias = listaCapitaisFaixaExtarias;
	}

	public HtmlSelectOneMenu getCapitalSegurado() {
		return capitalSegurado;
	}

	public void setCapitalSegurado(HtmlSelectOneMenu capitalSegurado) {
		this.capitalSegurado = capitalSegurado;
	}

	public Integer getIdCapitalSeguradoFaixaEtaria() {
		return idCapitalSeguradoFaixaEtaria;
	}

	public void setIdCapitalSeguradoFaixaEtaria(Integer idCapitalSeguradoFaixaEtaria) {
		this.idCapitalSeguradoFaixaEtaria = idCapitalSeguradoFaixaEtaria;
	}

	public CapitalSeguradoFaixaEtaria getCapitalSeguradoFaixaEtaria() {
		return capitalSeguradoFaixaEtaria;
	}

	public void setCapitalSeguradoFaixaEtaria(
			CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria) {
		this.capitalSeguradoFaixaEtaria = capitalSeguradoFaixaEtaria;
	}

	
	
	
	
}
