package br.com.itavida.handlers;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.FaturamentoDAO;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.PlanilhaFaturamento;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class FaturamentoHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private PlanilhaFaturamento planilhaFaturamentoConsultar;
	private PlanilhaFaturamento planilhaFaturamento;
	private List<PlanilhaFaturamento> resultadoPesquisa;
	private Integer estadoSelecionado;
	private Boolean fluxoInclusao;
	Map<String, Object[]> mapaRelatorioFaturamento;
	



	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getMesesCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "1", "01" ) );
		retorno.add( new SelectItem( "2", "02" ) );
		retorno.add( new SelectItem( "3", "03" ) );
		retorno.add( new SelectItem( "4", "04" ) );
		retorno.add( new SelectItem( "5", "05" ) );
		retorno.add( new SelectItem( "6", "06" ) );
		retorno.add( new SelectItem( "7", "07" ) );		
		retorno.add( new SelectItem( "8", "08" ) );
		retorno.add( new SelectItem( "9", "09" ) );
		retorno.add( new SelectItem( "10", "10" ) );
		retorno.add( new SelectItem( "11", "11" ) );		
		retorno.add( new SelectItem( "12", "12" ) );		
		
		return retorno;
	}	
	
	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public FaturamentoHandler(){
		String fluxo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fluxo");
		if( fluxo != null ){
			if( fluxo.equals( "incluir" ) ){
				preparaIncluir();
			}else if( fluxo.equals( "consulta" ) ){
				cancelarConsulta( null );
			}
		}
		
		
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaPlanilhaFaturamentos(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		FaturamentoDAO dao = new FaturamentoDAO( session );
		PlanilhaFaturamento planilhaFaturamentoFiltro = getPlanilhaFaturamentoConsultar();
		List<PlanilhaFaturamento> lista = dao.buscaPorFiltro( planilhaFaturamentoFiltro  );

		setResultadoPesquisa( lista );
			
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public String relatorioFaturamentoPesquisar( ){
		
		Session session = HibernateUtil.currentSession();
		FaturamentoDAO dao = new FaturamentoDAO( session );
		PlanilhaFaturamento planilhaFaturamentoFiltro = getPlanilhaFaturamentoConsultar();
		planilhaFaturamentoFiltro.setEstado(  estadoSelecionado );
		List<PlanilhaFaturamento> lista = dao.buscaPorFiltro( planilhaFaturamentoFiltro  );
		//Realiza a quebra por filial
		Map<String, Object[]> mapa = new HashMap<String, Object[]>();
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			PlanilhaFaturamento planilhaFaturamento = (PlanilhaFaturamento) iterator.next();
			String chave = "";
			if( estadoSelecionado != null && estadoSelecionado != 0 ){
				chave = "Estado: " + planilhaFaturamento.getOrgao().getCidade().getCod_estado().getSgl_estado()+ " - " +planilhaFaturamento.getOrgao().getCidade().getCod_estado().getNom_estado();
			}
			
			if( mapa.containsKey(chave) ){
				Object[] listaValorChave = mapa.get(chave);
				List<PlanilhaFaturamento> listaDetalhes =  ( List<PlanilhaFaturamento> )listaValorChave[0];		
				listaDetalhes.add( planilhaFaturamento );
				//Soma valor prolabore
				Double subTotalProlabore = ( Double ) listaValorChave[1];
				subTotalProlabore += planilhaFaturamento.getValorProlabore();
				listaValorChave[1] = subTotalProlabore;
				//Soma valor sem prolabore
				Double valorSemProlabore = ( Double ) listaValorChave[2];
				valorSemProlabore += planilhaFaturamento.getValorSemProlabore();
				listaValorChave[2] = valorSemProlabore;
				//Soma valor valorRelacao
				Double valorRelacao = ( Double ) listaValorChave[3];
				valorRelacao += planilhaFaturamento.getValorRelacao();
				listaValorChave[3] = valorRelacao;
				//Soma valor valorSeguradora
				Double valorSeguradora = ( Double ) listaValorChave[4];
				valorSeguradora += planilhaFaturamento.getValorSeguradora();
				listaValorChave[4] = valorSeguradora;
				//Soma valor valorTaxa
				Double valorTaxa = ( Double ) listaValorChave[5];
				valorTaxa += planilhaFaturamento.getValorTaxa();
				listaValorChave[5] = valorTaxa;
			}else{
				Object[] listaValorChave = new Object[6];
				List<PlanilhaFaturamento> listaDetalhes = new ArrayList<PlanilhaFaturamento>();
				listaDetalhes.add( planilhaFaturamento );
				listaValorChave[0] = listaDetalhes;
				listaValorChave[1] = planilhaFaturamento.getValorProlabore();
				listaValorChave[2] = planilhaFaturamento.getValorSemProlabore();
				listaValorChave[3] = planilhaFaturamento.getValorRelacao();
				listaValorChave[4] = planilhaFaturamento.getValorSeguradora();
				listaValorChave[5] = planilhaFaturamento.getValorTaxa();
				mapa.put( chave , listaValorChave );
			}
		}
		
		setMapaRelatorioFaturamento( mapa );
		
		
		setResultadoPesquisa( lista );
		
		return "resultado";
			
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNumeroVidas", "N�mero de Vidas");
		mapaCampos.put("dtPagamento", "Data do Pagamento");
		mapaCampos.put("txtValorRelacao", "Valor da Rela��o");
		mapaCampos.put("txtValorProlabore", "Valor Prolabore");
		mapaCampos.put("txtValorSemProlabore", "Valor Prolabore");
		mapaCampos.put("dtPeriodoVigencia", "Per�odo de Vig�ncia");
		mapaCampos.put("dtPeriodoCobranca", "Per�odo de Cobran�a");
		mapaCampos.put("txtValorTaxa", "Valor Taxa");
		mapaCampos.put("txtValorSeguradora", "Valor da Seguradora");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirPlanilhaFaturamento(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirPlanilhaFaturamento");
		PlanilhaFaturamento planilhaFaturamentoDeletado  = ( PlanilhaFaturamento ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<PlanilhaFaturamento, Integer> dao = new Dao<PlanilhaFaturamento, Integer>(session, PlanilhaFaturamento.class);
		dao.delete(planilhaFaturamentoDeletado);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaPlanilhaFaturamentos(e);
	}	
	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelarConsulta( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setPlanilhaFaturamentoConsultar( new PlanilhaFaturamento() );
		getPlanilhaFaturamentoConsultar().setOrgao( new Orgao() );
		this.resultadoPesquisa = null;
		this.fluxoInclusao = false;
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public String cancelarInclusao(  ){
		
		// Realiza o cancelamento das a��es
		setPlanilhaFaturamento( new PlanilhaFaturamento() );
		getPlanilhaFaturamento().setOrgao( new Orgao() );
		return "consultarFaturamento";
		
	}
	
	// Fluxos de INCLUS�O / EXCLUS�O / ALTERA��O
	
	public String preparaIncluir( ){
		cancelarInclusao( );
		this.fluxoInclusao = true;
		return "incluirFaturamento";
	}
	
	
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 * @throws UnsupportedEncodingException 
	 */
	public String preparaAlterarPlanilhaFaturamento( ){
		this.fluxoInclusao = true;
		return "incluirFaturamento";
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar( ActionEvent e) {
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao(getPlanilhaFaturamento().getOrgao().getId(), "Org�o") ){
		
			Dao<PlanilhaFaturamento, Integer> dao = new Dao<PlanilhaFaturamento, Integer>(session, PlanilhaFaturamento.class);
			PlanilhaFaturamento planilhaFaturamentoGravar = getPlanilhaFaturamento();
			
			dao.merge( planilhaFaturamentoGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			cancelarInclusao();
			
		}
		
	}
	
	public void calculaValorSemProlabore( ActionEvent e ){
		double valorRelacao = getPlanilhaFaturamento().getValorRelacao() == null ? 0.0 : getPlanilhaFaturamento().getValorRelacao() ;
		double valorProlabore = getPlanilhaFaturamento().getValorProlabore() == null ? 0.0 : getPlanilhaFaturamento().getValorProlabore();
		
		double total = valorRelacao - valorProlabore;
		
		getPlanilhaFaturamento().setValorSemProlabore( total );
	}
	
	public void calculaValorSeguradora( ActionEvent e ){
		double valorSemProlabore = getPlanilhaFaturamento().getValorSemProlabore() == null ? 0.0 : getPlanilhaFaturamento().getValorSemProlabore() ;
		double valorTaxa = getPlanilhaFaturamento().getValorTaxa() == null ? 0.0 : getPlanilhaFaturamento().getValorTaxa();
		
		double total = valorSemProlabore - valorTaxa;
		
		getPlanilhaFaturamento().setValorSeguradora( total );
	}
	
	public Orgao getLimparOrgao( ){
		return  new Orgao() ;
	}

	
	
	public List<SelectItem> getEstadosCombo(){
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		CidadeDAO daoCidade = new CidadeDAO( session );
		List<Estado> estados = daoCidade.obterTodosEstados();
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		for (Iterator iterator = estados.iterator(); iterator.hasNext();) {
			Estado estado = (Estado) iterator.next();
			retorno.add( new SelectItem( estado.getCod_estado(), estado.getSgl_estado() + " - " + estado.getNom_estado() ) );
		}
		
		return retorno;
		
	}
	
    public List<String> getMapKeys(){
        List<String> ret = new ArrayList<String>();
        for (String s : mapaRelatorioFaturamento.keySet())
            ret.add(s);
        return ret;
    }
	
    public Double[] getTotalValor(){
        Double[] total = {0.0,0.0,0.0,0.0,0.0};
        for (String chave : mapaRelatorioFaturamento.keySet()){
        	Object[] listaValorChave = mapaRelatorioFaturamento.get(chave);
        	Double subTotalValorProlabore = ( Double ) listaValorChave[1];
        	total[0] += subTotalValorProlabore;
        	
        	Double subTotalValorSemProlabore = ( Double ) listaValorChave[2];
        	total[1] += subTotalValorSemProlabore;
        	
        	Double subTotalValorRelacao = ( Double ) listaValorChave[3];
        	total[2] += subTotalValorRelacao;
        	
        	Double subTotalValorSeguradora = ( Double ) listaValorChave[4];
        	total[3] += subTotalValorSeguradora;
        	
        	Double subTotalValorTaxa = ( Double ) listaValorChave[5];
        	total[4] += subTotalValorTaxa;
        }
        return total;
    }
    


    
    
    
    // Getters and Setters =========================================================================================
	

	public PlanilhaFaturamento getPlanilhaFaturamento() {
		return planilhaFaturamento;
	}


	public void setPlanilhaFaturamento(PlanilhaFaturamento planilhaFaturamento) {
		this.planilhaFaturamento = planilhaFaturamento;
	}

	/**
	 * @return the resultadoPesquisa
	 */
	public List<PlanilhaFaturamento> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	/**
	 * @param resultadoPesquisa the resultadoPesquisa to set
	 */
	public void setResultadoPesquisa(
			List<PlanilhaFaturamento> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public PlanilhaFaturamento getPlanilhaFaturamentoConsultar() {
		return planilhaFaturamentoConsultar;
	}

	public void setPlanilhaFaturamentoConsultar(
			PlanilhaFaturamento planilhaFaturamentoConsultar) {
		this.planilhaFaturamentoConsultar = planilhaFaturamentoConsultar;
	}


	public Boolean getFluxoInclusao() {
		return fluxoInclusao;
	}

	public void setFluxoInclusao(Boolean fluxoInclusao) {
		this.fluxoInclusao = fluxoInclusao;
	}
	
	
	public Integer getEstadoSelecionado() {
		return estadoSelecionado;
	}


	public void setEstadoSelecionado(Integer estadoSelecionado) {
		this.estadoSelecionado = estadoSelecionado;
	}


	public Map<String, Object[]> getMapaRelatorioFaturamento() {
		return mapaRelatorioFaturamento;
	}
	
	public void setMapaRelatorioFaturamento(
			Map<String, Object[]> mapaRelatorioFaturamento) {
		this.mapaRelatorioFaturamento = mapaRelatorioFaturamento;
	}
	
	
	// Fim Getters and Setters =====================================================

    
    
}
