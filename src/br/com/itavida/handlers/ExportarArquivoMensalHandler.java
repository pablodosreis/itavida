package br.com.itavida.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.Session;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.ExportacaoDAO;
import br.com.itavida.dao.NumeroSorteioDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.NumeroSorteio;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.File;
import br.com.itavida.util.HibernateUtil;

public class ExportarArquivoMensalHandler extends MainHandler{
	
	private List<Integer> listaOrgaosSelecionados = new ArrayList<Integer>(); 
	private List<SelectItem> orgaosSelectItens;

	private ArrayList<File> files = new ArrayList<File>();
	private int uploadsAvailable = 1;

	private Integer radioSelected;
	private Integer empresaSelected;
	private List<SelectItem> opcoesEmpresaCombo;

	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public Integer getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Integer empresaSelected) {
		this.empresaSelected = empresaSelected;
	}
	
	/**
	 * Popula um combo com op��es do Agenciador 
	 * @return
	 */
	public void populaOpcoesEmpresaCombo(){
		
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO =  new EmpresaDAO( session );
		
		List<Empresa> listaEmpresas = empresaDAO.buscaEmpresas();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesEmpresaCombo = new ArrayList<SelectItem>();
		opcoesEmpresaCombo.add( new SelectItem( null, "Selecione:" ) );
		for (Iterator iterator = listaEmpresas.iterator(); iterator.hasNext();) {
			Empresa empresa = (Empresa) iterator.next();
			opcoesEmpresaCombo.add( new SelectItem( empresa.getId(), empresa.getNomeEmpresa()) );
		}
		
	}
	
	public int getSize() {
		if (getFiles().size() > 0) {
			return getFiles().size();
		} else {
			return 0;
		}
	}

	public void paint(OutputStream stream, Object object) throws IOException {
		stream.write(getFiles().get((Integer) object).getData());
	}

	public void listener(UploadEvent event) throws Exception {
        UploadItem item = event.getUploadItem();
        File file = new File();
        file.setLength(item.getData().length);
        file.setName(item.getFileName());
        file.setData(item.getData());        
        files.add(file);
        uploadsAvailable = 0;
	}  
	      
    public String clearUploadData( ) {
        files.clear();
        uploadsAvailable = 1;
        return null;
    }
    

	
	
	
	public ExportarArquivoMensalHandler(){
		populaOrgaosSelectItens(null);
		populaOpcoesEmpresaCombo();
	}
	
	

	/**
	 * Popula um combo com valores dos Tipos de Proposta
	 * @return
	 */
	public void populaOrgaosSelectItens(ActionEvent e){
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		orgaosSelectItens = new ArrayList<SelectItem>();

		OrgaoDAO orgaoDAO = new OrgaoDAO( session );
		List<Orgao> listaOrgaos = orgaoDAO.findAllOrgaos(  );
		
		for (Iterator iterator = listaOrgaos.iterator(); iterator.hasNext();) {
			Orgao orgao = (Orgao) iterator.next();
			orgaosSelectItens.add( new SelectItem( orgao.getId(), orgao.getSiglaOrgao() + " - " + orgao.getNomeOrgao()  ) );
		}		
		
		
	}
	
	
	
	public void limparDados( ActionEvent e ){
		populaOrgaosSelectItens(e);
		listaOrgaosSelecionados = new ArrayList<Integer>();
		clearUploadData( );
	}
	
	public String importReport( ) {
		if( getSize() == 0 ){
			addErrorMessage( "Selecione algum arquivo para realizar a Importa��o dos N�meros de Sorteio." );
			return null;
		}
		
		if (empresaSelected == null) {
			addErrorMessage( "Por favor, informe para qual empresa est� sendo realizada a importa��o." );
			return null;
		}
		
		boolean falhou = false;
		
		File file = files.get(0);
	    ByteArrayInputStream excelFIS = null;
		excelFIS = new ByteArrayInputStream( file.getData() );
		
		HSSFWorkbook excelWB = null;
		
	    try {
			excelWB = new HSSFWorkbook( excelFIS );
		} catch (Exception e) {
			falhou = true;
			addErrorMessage( "O formato desta planilha � incorreto. Favor verificar a planilha selecionada." );
		}
		
    	   HSSFSheet oneSheet = excelWB.getSheetAt(0);
    	// Now get the number of rows in the sheet
    	   int rows = oneSheet.getPhysicalNumberOfRows();
    	   HSSFRow firstRow = oneSheet.getRow(0);
 	      if( firstRow.getPhysicalNumberOfCells() != 4 ){
 	    	  addErrorMessage( "O formato desta planilha � incorreto. Favor verificar a planilha selecionada." );
 	    	  falhou = true;
 	    	  return null;
 	      }
    	  
	      if( !falhou ){
	    	  
	    	  // Recupera todas as propostas do banco para realizar a chave
	  		  Session session = HibernateUtil.currentSession();
	  		  PropostaDAO propostaDAO =  new PropostaDAO(session);
	  		  Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
	  		  NumeroSorteioDAO daoNumeroSorteio = new NumeroSorteioDAO(session);
	    	  List<Proposta> propostas = propostaDAO.obterTodasPropostasAtivas();
	    	  HashMap<String, Proposta> mapPropostas = new HashMap<String, Proposta>();
	    	  
	    	  for (Proposta proposta : propostas) {
	    		  String cpfSemFormatacao = "";
	    		  if( proposta.getCpfSegurado() != null && ! StringUtils.isBlank( proposta.getCpfSegurado() )){
	    			  cpfSemFormatacao = proposta.getCpfSegurado().replace(".", "").replace("_", "").replace("-", "");
	    			  if( ! mapPropostas.containsKey( cpfSemFormatacao ) && ! cpfSemFormatacao.equals("00000000000") ){
	    				  mapPropostas.put( cpfSemFormatacao , proposta );
	    			  }else{
	    				  if( ! mapPropostas.containsKey( proposta.getNomeSegurado().toUpperCase() ) ){
	    					  mapPropostas.put(proposta.getNomeSegurado().toUpperCase(), proposta);
	    				  }
	    			  }
	    		  }else{
    				  if( ! mapPropostas.containsKey( proposta.getNomeSegurado().toUpperCase() ) ){
    					  mapPropostas.put(proposta.getNomeSegurado().toUpperCase(), proposta);
    				  }
	    		  }
			  }

	    	  daoNumeroSorteio.limparTudo(empresaSelected);
	    	  for (int rowNumber = 15; rowNumber < rows; rowNumber++) {
		    	      HSSFRow oneRow = oneSheet.getRow(rowNumber);
		    	      // Skip empty (null) rows.
		    	      if (oneRow != null) {

		    	    	 HSSFCell cellCpf = oneRow.getCell(2);
		    	    	 String cpfNaoFormatado = "";
		    	    	 if( cellCpf.getCellType() == HSSFCell.CELL_TYPE_STRING ){
		    	    		 cpfNaoFormatado = cellCpf.getRichStringCellValue().getString();
		    	    	 }else if( cellCpf.getCellType() == HSSFCell.CELL_TYPE_NUMERIC ){
		    	    		 Double result = cellCpf.getNumericCellValue();
		    	    		 cpfNaoFormatado = String.valueOf( result.longValue() );
		    	    	 }
		    	    	 
		    	    	 String cpfSemFormatacaoKey = "";
		    	    	 Proposta propostaRecover = null;
		    	    	 if( cpfNaoFormatado != null && cpfNaoFormatado != null ){
			    	    	  cpfSemFormatacaoKey = cpfNaoFormatado.replace(".", "").replace("_", "").replace("-", "");
			    	    	  //acrescentado para garantir que o cpf sempre ter� 11 d�gitos
			    	    	  cpfSemFormatacaoKey = StringUtils.leftPad(cpfSemFormatacaoKey, 11, "0");
			    	    	  propostaRecover = mapPropostas.get( cpfSemFormatacaoKey );
			    	    	  if( propostaRecover == null ){
			    	    		  HSSFCell cellNome = oneRow.getCell(0);
			    	    		  propostaRecover = mapPropostas.get( cellNome.getRichStringCellValue().getString().toUpperCase() );
			    	    	  }
		    	    	 }else{
		    	    		 HSSFCell cellNome = oneRow.getCell(0);
		    	    		 propostaRecover = mapPropostas.get( cellNome.getRichStringCellValue().getString().toUpperCase() );
		    	    	 }
		    	    	 
		    	    	 if( propostaRecover != null  && ( propostaRecover.getNumerosorteio_collection() == null || propostaRecover.getNumerosorteio_collection().size() == 0 )  ){
		    	    		 
		    	    		 HSSFCell cellNumeroSorteio = oneRow.getCell(3);
			    	    	 String numeroSorteioString = "";
			    	    	 if( cellNumeroSorteio.getCellType() == HSSFCell.CELL_TYPE_STRING ){
			    	    		 numeroSorteioString = cellNumeroSorteio.getRichStringCellValue().getString();
			    	    	 }else if( cellNumeroSorteio.getCellType() == HSSFCell.CELL_TYPE_NUMERIC ){
			    	    		 Double result = cellNumeroSorteio.getNumericCellValue();
			    	    		 numeroSorteioString = String.valueOf( result.longValue() );
			    	    	 }else if( cellNumeroSorteio.getCellType() == HSSFCell.CELL_TYPE_FORMULA ){
			    	    		 numeroSorteioString = cellNumeroSorteio.getRichStringCellValue().getString();
			    	    	 }
		    	    		 
			    	    	 
		    	    		 NumeroSorteio numeroSorteio = new NumeroSorteio();
		    	    		 numeroSorteio.setNumeroSorteio( StringUtils.leftPad(numeroSorteioString, 5, "0") ) ;
		    	    		 numeroSorteio.setProposta( propostaRecover );
		    	    		 
		    	    		 daoNumeroSorteio.saveOrUpdate( numeroSorteio );
		    	    		 
		    	    		 
		    	    	 }else if( propostaRecover != null && ( propostaRecover.getNumerosorteio_collection() != null && propostaRecover.getNumerosorteio_collection().size() > 0 ) ){
		    	    			 
		    	    		 propostaRecover.getNumerosorteio_collection().clear();
		    	    		 
		    	    		 daoProposta.merge( propostaRecover );
		    	    		 
		    	    		
		    	    		HSSFCell cellNumeroSorteio = oneRow.getCell(3);
			    	    	 String numeroSorteioString = "";
			    	    	 if( cellNumeroSorteio.getCellType() == HSSFCell.CELL_TYPE_STRING ){
			    	    		 numeroSorteioString = cellNumeroSorteio.getRichStringCellValue().getString();
			    	    	 }else if( cellNumeroSorteio.getCellType() == HSSFCell.CELL_TYPE_NUMERIC ){
			    	    		 Double result = cellNumeroSorteio.getNumericCellValue();
			    	    		 numeroSorteioString = String.valueOf( result.longValue() );
			    	    	 }else if( cellNumeroSorteio.getCellType() == HSSFCell.CELL_TYPE_FORMULA ){
			    	    		 numeroSorteioString = cellNumeroSorteio.getRichStringCellValue().getString();
			    	    	 }
		    	    		 
			    	    	 
		    	    		 NumeroSorteio numeroSorteio = new NumeroSorteio();
		    	    		 numeroSorteio.setNumeroSorteio( StringUtils.leftPad(numeroSorteioString, 5, "0") ) ;
		    	    		 numeroSorteio.setProposta( propostaRecover );
		    	    		
		    	    		 daoNumeroSorteio.merge( numeroSorteio );
		    	    		
		    	    	 }
		    	    	 
		    	      }
	    	  }

	      }
		
		 if( !falhou ){
			 addGlobalMessage("N�meros de Sorteios importados com sucesso!");
		 }
		return null;
	}
	
	
	public String exportReport( ) {
		if( radioSelected == null || radioSelected == 0 ){
			addErrorMessage( "Selecione uma op��o para exporta��o do arquivo de N�meros de Sorteio." );
			return null;
		}
		
		if( empresaSelected == null || empresaSelected == 0){
			addErrorMessage( "Campo EMPRESA � de Sele��o Obrigat�ria para esta a��o." );
			return null;
		}
		
		boolean falhou = false;
		
		HSSFWorkbook excelWB = null;
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO( session );
		List<Proposta> propostas = propostaDAO.buscarPropostasAtivasSemNumeroSorteio( radioSelected, empresaSelected );
		Dao<Empresa, Integer> daoEmpresa = new Dao<Empresa, Integer>(session, Empresa.class);
		Empresa empresa = daoEmpresa.get( empresaSelected );
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");  
		SimpleDateFormat dfFileName = new SimpleDateFormat("ddMMyyyy");
	    try {
	    	InputStream excelFIS = this.getClass().getResourceAsStream(Constantes.SISVIDA_FILES + "CapitalizacaoAplub.xls");
	    	excelWB = new HSSFWorkbook(excelFIS);
	        
		} catch (Exception e) {
			falhou = true;
			addErrorMessage( "O arquivo n�o foi encontrado! Favor verificar se o arquivo est� no local correto." );
		}
		
    	   HSSFSheet oneSheet = excelWB.getSheetAt(0);

    	   
    	// Now get the number of rows in the sheet
    	   int rows = oneSheet.getPhysicalNumberOfRows();
    	   HSSFRow firstRow = oneSheet.getRow(0);
	      if( firstRow.getPhysicalNumberOfCells() != 4 ){
	    	  addErrorMessage( "O formato desta planilha � incorreto. Favor verificar a planilha selecionada." );
	    	  falhou = true;
	    	  return null;
	      }
    	  
   	   
	      	HSSFCellStyle estiloFullBorder = excelWB.createCellStyle();
          
          //Adicionando bordas para o CantoEsquerdo
			estiloFullBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloFullBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setTopBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setRightBorderColor(HSSFColor.GREEN.index);
	      
	      if( !falhou ){
	    	  
	    	  int rowNumber = 15;
	    	  HSSFRow row = oneSheet.getRow(rowNumber);
	    	  if( row == null ){
	    		  oneSheet.createRow( 15 );
	    		  row = oneSheet.getRow(rowNumber);
	    	  }
	    	  for (Proposta proposta : propostas) {
	    		  row.createCell(0).setCellValue( new HSSFRichTextString(proposta.getNomeSegurado() ) );  
	    		  row.createCell(1).setCellValue( new HSSFRichTextString( df.format( proposta.getDataNascimentoSegurado() ) ) );
	    		  String cpfSemFormatacao = "";
	    		  if( proposta.getCpfSegurado() != null ){
	    			  cpfSemFormatacao = proposta.getCpfSegurado().replace(".", "").replace("_", "").replace("-", "");
	    			  
	    		  }
	    		  row.createCell(2).setCellValue( new HSSFRichTextString( cpfSemFormatacao ) ); 
	    		  NumeroSorteio numSorteio = null;
	    		  if( proposta.getNumerosorteio_collection() != null && proposta.getNumerosorteio_collection().size() > 0 ){
	    			  for (Iterator iterator = proposta.getNumerosorteio_collection().iterator(); iterator
							.hasNext();) {
						numSorteio = (NumeroSorteio) iterator.next();
						break;
					}
	    		  }
	    		  if( numSorteio != null && numSorteio.getId() != null && numSorteio.getId() != 0){
	    			  row.createCell(3).setCellValue( new HSSFRichTextString( numSorteio.getNumeroSorteio() ) ); 
	    		  }else{
	    			  row.createCell(3).setCellValue( new HSSFRichTextString( "" ) ); 
	    		  }
	    		  
	    		  row.getCell( 0 ).setCellStyle( estiloFullBorder );
	    		  row.getCell( 1 ).setCellStyle( estiloFullBorder );
	    		  row.getCell( 2 ).setCellStyle( estiloFullBorder );
	    		  row.getCell( 3 ).setCellStyle( estiloFullBorder );
	    		  
	    		  rowNumber++;
	    		  oneSheet.createRow( rowNumber );
	    		  row = oneSheet.getRow(rowNumber);
	    	  }
	    	  
	          HSSFRow header = oneSheet.getRow(0); 
			    for(short column=0;column<header.getLastCellNum(); column++){
			    	oneSheet.autoSizeColumn(column);
			    }  
	    	  
	      }
		
		 
         HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();  
         res.setContentType("application/vnd.ms-excel");  
         res.setHeader("Content-disposition",  "attachment; filename=SeguradosSemTitulo_APLUB_"+empresa.getNomeEmpresa()+"_"+dfFileName.format( new Date() ) + ".xls" );  

         
        try {  
              ServletOutputStream out = res.getOutputStream();  
              excelWB.write(out);  
              out.flush();  
              out.close();  
        } catch (IOException ex) {   
                ex.printStackTrace();  
        }  
        
        FacesContext faces = FacesContext.getCurrentInstance();  
        faces.responseComplete();   
		 
        return "export";
	}
	
	public String generateReport(  ){
		
		Session session = HibernateUtil.currentSession();
		ExportacaoDAO exportacaoDAO = new ExportacaoDAO( session );
		PropostaDAO propostaDAO = new PropostaDAO(session);
		Object[] objetosListaOrgao = listaOrgaosSelecionados.toArray();
		Integer[] orgaosSelecionadosInteger = new Integer[objetosListaOrgao.length];
		for (int i = 0; i < objetosListaOrgao.length; i++) {
			orgaosSelecionadosInteger[i] = new Integer( objetosListaOrgao[i].toString().trim() );
		}
		
		List<Proposta> listaPropostas = exportacaoDAO.buscaPorOrgao(orgaosSelecionadosInteger, false );
		
		Map<Integer, List<Proposta>> mapaPropostasPorOrgao = new HashMap<Integer, List<Proposta>>();
		for (Iterator iterator = listaPropostas.iterator(); iterator.hasNext();) {
			Proposta proposta = (Proposta) iterator.next();
			
			if( mapaPropostasPorOrgao.containsKey( proposta.getOrgao().getId() ) ){
				List<Proposta> propostasPorOrgao = mapaPropostasPorOrgao.get( proposta.getOrgao().getId() );
				propostasPorOrgao.add( proposta );
			}else{
				List<Proposta> propostasPorOrgao = new ArrayList<Proposta> ();
				propostasPorOrgao.add( proposta );
				mapaPropostasPorOrgao.put(proposta.getOrgao().getId(), propostasPorOrgao );
			}
		}
		
        try {  
        	
            HSSFWorkbook wb = new HSSFWorkbook();  
            HSSFSheet sheet = wb.createSheet();  
              
            HSSFRow row;  
            
            //Estilos
            short formatoMonetario = wb.createDataFormat().getFormat("R$ #,##0.00;[Red]R$ #,##0.00");
			
            HSSFCellStyle estiloMonetario = wb.createCellStyle();
            estiloMonetario.setDataFormat( formatoMonetario );
            
			HSSFCellStyle estiloCantoEsquerdo = wb.createCellStyle();
			
			HSSFFont fontBold = wb.createFont();
			fontBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			
           //Adicionando bordas para o CantoEsquerdo
			estiloCantoEsquerdo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoEsquerdo.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloCantoEsquerdo.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setTopBorderColor(HSSFColor.BLACK.index);
			estiloCantoEsquerdo.setFont( fontBold );
			
			HSSFCellStyle estiloMeio = wb.createCellStyle();
              
           //Adicionando bordas para o Meio
			estiloMeio.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloMeio.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setTopBorderColor(HSSFColor.BLACK.index);
			
			HSSFCellStyle estiloCantoDireito = wb.createCellStyle();
              
           //Adicionando bordas para o CantoDireito
			estiloCantoDireito.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setTopBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setRightBorderColor(HSSFColor.GREEN.index);
			estiloCantoDireito.setDataFormat( formatoMonetario );
			estiloCantoDireito.setFont( fontBold );
			
			HSSFCellStyle estiloFullBorder = wb.createCellStyle();
              
               //Adicionando bordas para o CantoEsquerdo
			estiloFullBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloFullBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setTopBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setRightBorderColor(HSSFColor.GREEN.index);
			estiloFullBorder.setFont( fontBold );
			
            //Cabe�alho  
            row = sheet.createRow(0);  
            
            HSSFFont fontCabecalho = wb.createFont();  
            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
            HSSFRichTextString richTextOrgao = new HSSFRichTextString("Orgao");  
            richTextOrgao.applyFont(fontCabecalho);  
            HSSFRichTextString richTextUfOrgao = new HSSFRichTextString("UF_Orgao");  
            richTextUfOrgao.applyFont(fontCabecalho); 
            HSSFRichTextString richTextSeguradoPrincipal = new HSSFRichTextString("Segurado_Principal");  
            richTextSeguradoPrincipal.applyFont(fontCabecalho); 
            HSSFRichTextString richTextDataNascimento = new HSSFRichTextString("Data_Nasc_Func");  
            richTextDataNascimento.applyFont(fontCabecalho);  
            HSSFRichTextString richTextMatricula = new HSSFRichTextString("Matricula");  
            richTextMatricula.applyFont(fontCabecalho);             
            HSSFRichTextString richTextRg = new HSSFRichTextString("RG");  
            richTextRg.applyFont(fontCabecalho);  
            HSSFRichTextString richTextCPFFuncionario = new HSSFRichTextString("CPF_Funcionario");  
            richTextCPFFuncionario.applyFont(fontCabecalho);      
            HSSFRichTextString richTextConjuge = new HSSFRichTextString("Conjuge");  
            richTextConjuge.applyFont(fontCabecalho);   
            HSSFRichTextString richTextDataNascimentoConjuge = new HSSFRichTextString("Data_Nasc_Conj");  
            richTextDataNascimentoConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCPFConjuge = new HSSFRichTextString("CPF_Conj");  
            richTextCPFConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextRGConjuge = new HSSFRichTextString("RG_Conj");  
            richTextRGConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCapitalFuncionario = new HSSFRichTextString("Capital_Func");  
            richTextCapitalFuncionario.applyFont(fontCabecalho);   
            HSSFRichTextString richTextCapitalConjuge = new HSSFRichTextString("Capital_Conjuge");  
            richTextCapitalConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextValorFuncionario= new HSSFRichTextString("Valor_Funcionario");  
            richTextValorFuncionario.applyFont(fontCabecalho);
            HSSFRichTextString richTextValorConjuge= new HSSFRichTextString("Valor_Conjuge");  
            richTextValorConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextValorTotal= new HSSFRichTextString("Valor_Total");  
            richTextValorTotal.applyFont(fontCabecalho);              

            row.createCell(0).setCellValue(richTextOrgao);  
            row.createCell(1).setCellValue(richTextUfOrgao);  
            row.createCell(2).setCellValue(richTextSeguradoPrincipal);  
            row.createCell(3).setCellValue(richTextDataNascimento); 
            row.createCell(4).setCellValue(richTextMatricula); 
            row.createCell(5).setCellValue(richTextRg); 
            row.createCell(6).setCellValue(richTextCPFFuncionario); 
            row.createCell(7).setCellValue(richTextConjuge); 
            row.createCell(8).setCellValue(richTextDataNascimentoConjuge);          
            row.createCell(9).setCellValue(richTextCPFConjuge); 
            row.createCell(10).setCellValue(richTextRGConjuge); 
            row.createCell(11).setCellValue(richTextCapitalFuncionario); 
            row.createCell(12).setCellValue(richTextCapitalConjuge);    
            row.createCell(13).setCellValue(richTextValorFuncionario); 
            row.createCell(14).setCellValue(richTextValorConjuge);       
            row.createCell(15).setCellValue(richTextValorTotal);  
            
            //Fim cabe�alho 
            int inicio = 1;
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");  
            DecimalFormat decimalFormat = new DecimalFormat();  
            decimalFormat.setMaximumFractionDigits(2);  
            decimalFormat.setMinimumFractionDigits(2);
            decimalFormat.setGroupingUsed( true );
            decimalFormat.setGroupingSize(3);
            for (Iterator iterator = mapaPropostasPorOrgao.values().iterator(); iterator.hasNext();) {
				List<Proposta> listaPropostasPorOrgao = ( List<Proposta> ) iterator.next();
                
		        // Em ordem crescente 
		        Collections.sort (listaPropostasPorOrgao, new Comparator() {  
		            public int compare(Object o1, Object o2) {  
		                Proposta p1 = (Proposta) o1;  
		                Proposta p2 = (Proposta) o2;  
		                return p1.getNomeSegurado().compareToIgnoreCase( p2.getNomeSegurado() );  
		            }  
		        }); 
				
				double valorTotal = 0.0;
				double valorProlaboreTotal = 0.0;
				
				for (int i = 0; i < listaPropostasPorOrgao.size(); i++){  
					
					Proposta proposta = listaPropostasPorOrgao.get(i);
					
					
					propostaDAO.populaCapital(proposta);
	                
	                //Soma o valor total
	                
	                if( proposta.getTotalPremios() != null ){
	                	valorTotal += proposta.getTotalPremios();
	                }
	                valorProlaboreTotal += proposta.getOrgao().getValorProlabore();
	                
	                row = sheet.createRow(inicio);  
	                row.createCell(0).setCellValue( new HSSFRichTextString( proposta.getOrgao().getNomeOrgao() ));  
	                row.createCell(1).setCellValue( new HSSFRichTextString( proposta.getOrgao().getCidade().getCod_estado().getSgl_estado() ));  
	                row.createCell(2).setCellValue( new HSSFRichTextString( proposta.getNomeSegurado() ) );  
	                row.createCell(3).setCellValue( new HSSFRichTextString( df.format( proposta.getDataNascimentoSegurado() ) ) ); 
	                row.createCell(4).setCellValue( new HSSFRichTextString( proposta.getMatriculaSegurado() ) ); 
	                row.createCell(5).setCellValue( new HSSFRichTextString( proposta.getRgSegurado() ) ); 
	                row.createCell(6).setCellValue( new HSSFRichTextString( proposta.getCpfSegurado() ) ); 
	                row.createCell(7).setCellValue( new HSSFRichTextString( proposta.getNomeConjuge() ) ); 
	                if( proposta.getDataNascimentoConjuge() != null ){
	                	row.createCell(8).setCellValue( new HSSFRichTextString( df.format( proposta.getDataNascimentoConjuge() ) ) );  
	                }else{
	                	row.createCell(8).setCellValue( new HSSFRichTextString( "" ) );
	                }
	                row.createCell(9).setCellValue( new HSSFRichTextString( proposta.getCpfConjuge() ) ); 
	                row.createCell(10).setCellValue( new HSSFRichTextString( proposta.getRgConjuge() ) ); 
	                if(  proposta.getCapitalSegurado() != null ){
	                	row.createCell(11).setCellValue( proposta.getCapitalSegurado()  ); 
	                }else{
	                	row.createCell(11).setCellValue( 0.0 );
	                }
	                row.getCell(11).setCellStyle( estiloMonetario );
	                if( proposta.getCapitalConjuge() != null ){
	                	row.createCell(12).setCellValue( proposta.getCapitalConjuge()  );
	                }else{
	                	row.createCell(12).setCellValue( 0.0 );
	                }
	                row.getCell(12).setCellStyle( estiloMonetario );	            
	                
	                if( proposta.getPremioSegurado() != null){
	                	row.createCell(13).setCellValue(  proposta.getPremioSegurado()  ); 
	                }else{
	                	row.createCell(13).setCellValue(  0.0  );
	                }
	                row.getCell(13).setCellStyle( estiloMonetario );
	                if( proposta.getPremioConjuge() != null ){
	                	row.createCell(14).setCellValue(  proposta.getPremioConjuge()  );
	                }else{
	                	row.createCell(14).setCellValue( 0.0  );
	                }
	                row.getCell(14).setCellStyle( estiloMonetario );
	                if( proposta.getTotalPremios() != null ){
	                	row.createCell(15).setCellValue(  proposta.getTotalPremios() ); 
	                }else{
	                	row.createCell(15).setCellValue( 0.0 ); 
	                }
	                row.getCell(15).setCellStyle( estiloMonetario );
	                inicio++;
	                
	            }  
				
				
				row = sheet.createRow(inicio); 
	            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	            HSSFRichTextString richTextTotalDescontado = new HSSFRichTextString("TOTAL DESCONTADO");  

	            
				row.createCell(0).setCellValue(richTextTotalDescontado); 
				row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
				
				row.createCell(15).setCellValue( valorTotal  );
				row.getCell( 15 ).setCellStyle( estiloCantoDireito );
				
				for(int j=1;j<15;j++){
					row.createCell(j);
					row.getCell( j ).setCellStyle( estiloMeio );
				}
				
				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	            HSSFRichTextString richTextProlabore = new HSSFRichTextString("PR�-LABORE");  
				row.createCell(0).setCellValue(richTextProlabore); 
				row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
				row.createCell(15).setCellValue( valorProlaboreTotal  );
				row.getCell( 15 ).setCellStyle( estiloCantoDireito );
				
				for(int j=1;j<15;j++){
					row.createCell(j);
					row.getCell( j ).setCellStyle( estiloMeio );
				}
				
				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	            HSSFRichTextString richTextValorRepasse = new HSSFRichTextString("VALOR REPASSE");  
				row.createCell(0).setCellValue(richTextValorRepasse); 
				row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
				row.createCell(15).setCellValue( valorTotal - valorProlaboreTotal  );
				row.getCell( 15 ).setCellStyle( estiloCantoDireito );
				for(int j=1;j<15;j++){
					row.createCell(j);
					row.getCell( j ).setCellStyle( estiloMeio );
				}
				
				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight( HSSFFont.BOLDWEIGHT_BOLD );  
	            HSSFRichTextString richTextValorTotalVidas = new HSSFRichTextString(listaPropostasPorOrgao.size() + " VIDAS");  
				row.createCell(0).setCellValue(richTextValorTotalVidas); 
				row.getCell( 0 ).setCellStyle( estiloFullBorder );
				inicio++;
				inicio++;
				
			}
          
            HSSFRow header = sheet.getRow(0); 
		    for(short column=0;column<header.getLastCellNum(); column++){
		    	sheet.autoSizeColumn(column);
		    }    
		    
            HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();  
            res.setContentType("application/vnd.ms-excel");  
            res.setHeader("Content-disposition",  "attachment; filename=ExportacaoMensal.xls");  
 
            
           try {  
                 ServletOutputStream out = res.getOutputStream();  
                 wb.write(out);  
                 out.flush();  
                 out.close();  
           } catch (IOException ex) {   
                   ex.printStackTrace();  
           }  
           
           FacesContext faces = FacesContext.getCurrentInstance();  
           faces.responseComplete();  
  
              
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        
        return "export";
		
	}
	
	
	//Getters and setters
	public List<Integer> getListaOrgaosSelecionados() {
		return listaOrgaosSelecionados;
	}
	public void setListaOrgaosSelecionados(List<Integer> listaOrgaosSelecionados) {
		this.listaOrgaosSelecionados = listaOrgaosSelecionados;
	}
	public List<SelectItem> getOrgaosSelectItens() {
		return orgaosSelectItens;
	}
	public void setOrgaosSelectItens(List<SelectItem> orgaosSelectItens) {
		this.orgaosSelectItens = orgaosSelectItens;
	}

	
    public long getTimeStamp(){
        return System.currentTimeMillis();
    }
    
    public ArrayList<File> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<File> files) { 
        this.files = files;
    }

    public int getUploadsAvailable() {
        return uploadsAvailable;
    }

    public void setUploadsAvailable(int uploadsAvailable) {
        this.uploadsAvailable = uploadsAvailable;
    }

	public Integer getRadioSelected() {
		return radioSelected;
	}

	public void setRadioSelected(Integer radioSelected) {
		this.radioSelected = radioSelected;
	}

    
    
	// fim getters and setters ===========================================================================================
	
}
