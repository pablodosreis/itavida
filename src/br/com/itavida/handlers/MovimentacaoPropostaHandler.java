package br.com.itavida.handlers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.MotivoCancelamentoDAO;
import br.com.itavida.dao.MovimentacaoPropostaAtrasadaDAO;
import br.com.itavida.dao.MovimentacaoPropostaInadimplenteDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.MovimentacaoPropostaAtrasada;
import br.com.itavida.entidades.MovimentacaoPropostaInadimplente;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.exceptions.SistemaException;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class MovimentacaoPropostaHandler extends MainHandler {
	
	private String periodo;
	private Proposta proposta;
	private Proposta propostaAnterior;
	private String periodoQuitacaoAnterior;
	private MovimentacaoPropostaInadimplente movPropIn;
	private MovimentacaoPropostaAtrasada movPropAtr;
	private boolean avancou = false;
	private boolean confimarInadimplentesInc = false;
	private String inadimplentesAInserir;
	private Integer qtdAtrasadas;
	private Integer qtdInadimplentes;
	
	
	
	public MovimentacaoPropostaHandler() {		
		super();		
		proposta = new Proposta();
		propostaAnterior = new Proposta();
		movPropIn = new MovimentacaoPropostaInadimplente();
		movPropAtr = new MovimentacaoPropostaAtrasada();
	}	
	

	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}


	public MovimentacaoPropostaInadimplente getMovPropIn() {
		return movPropIn;
	}


	public void setMovPropIn(MovimentacaoPropostaInadimplente movPropIn) {
		this.movPropIn = movPropIn;
	}
	
	public Date getDataPeriodoQuitacaoAnterior() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = format.parse("01/" + periodoQuitacaoAnterior);
		} catch (ParseException e1) {
			//faz nada
		}
		return date;	
	}
	
	public Date getReferencia() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = format.parse("01/" + periodo);
		} catch (ParseException e1) {
			//faz nada
		}
		return date;
	}
	
	public List<MovimentacaoPropostaInadimplente> getMovimentacoes() {
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoPropostaInadimplenteDAO inadimplenteDAO = new MovimentacaoPropostaInadimplenteDAO(currentSession);
		List<MovimentacaoPropostaInadimplente> buscarMovimentacoes = inadimplenteDAO.buscarMovimentacoes(getReferencia());
		qtdInadimplentes = buscarMovimentacoes.size();
		return buscarMovimentacoes;
	}
	
	public List<MovimentacaoPropostaAtrasada> getMovimentacoesPropostasAtrasadas() {
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoPropostaAtrasadaDAO dao = new MovimentacaoPropostaAtrasadaDAO(currentSession);
		List<MovimentacaoPropostaAtrasada> buscarMovimentacoes = dao.buscarMovimentacoes(getReferencia());
		qtdAtrasadas = buscarMovimentacoes.size();
		return buscarMovimentacoes;
	}
	
	
	public Proposta getPropostaAnterior() {
		return propostaAnterior;
	}


	public MovimentacaoPropostaAtrasada getMovPropAtr() {
		return movPropAtr;
	}


	public void setMovPropAtr(MovimentacaoPropostaAtrasada movPropAtr) {
		this.movPropAtr = movPropAtr;
	}


	public void setPropostaAnterior(Proposta propostaAnterior) {
		this.propostaAnterior = propostaAnterior;
	}


	public String getPeriodoQuitacaoAnterior() {
		return periodoQuitacaoAnterior;
	}


	public void setPeriodoQuitacaoAnterior(String periodoQuitacaoAnterior) {
		this.periodoQuitacaoAnterior = periodoQuitacaoAnterior;
	}


	public void avancar( ActionEvent e ) {
		Session currentSession = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(currentSession);
		//verificar se existem segurados fora de folha n�o incluidos
		List<Proposta> propostasInadimplentes = propostaDAO.getPropostasInadimplentes();
		List<MovimentacaoPropostaInadimplente> atrasadas = getMovimentacoes();
		int numInadimplentes = propostasInadimplentes.size();
		
		inadimplentesAInserir = "";
		for (Proposta proposta : propostasInadimplentes) {
			boolean inserir = true;
			for (MovimentacaoPropostaInadimplente movimentacaoPropostaInadimplente : atrasadas) {
				if (movimentacaoPropostaInadimplente.getProposta().getId().equals(proposta.getId())) {
					numInadimplentes--;
					inserir = false;
				}
			}
			if (inserir) {
				inadimplentesAInserir += proposta.getId() + ",";
			}
		}
		
		if (inadimplentesAInserir.length()>0) {
			inadimplentesAInserir = inadimplentesAInserir.substring(0, inadimplentesAInserir.length()-1);
		}
		
		if (numInadimplentes > 0) {
			//perguntar se deseja incluir os inadimplentes
			confimarInadimplentesInc = true;
		} else {
			//avancar
			avancou = true;
		}
	}
	
	public void avancarSimples(ActionEvent e) {
		avancou = true;
		confimarInadimplentesInc = false;
	}
	
	public void avancarAddInadimplentes( ActionEvent e ) {
		Session currentSession = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(currentSession);
		MovimentacaoPropostaInadimplenteDAO inadimplenteDAO = new MovimentacaoPropostaInadimplenteDAO(currentSession);
		MovimentacaoPropostaAtrasadaDAO atrasadaDAO = new MovimentacaoPropostaAtrasadaDAO(currentSession);

		List<Proposta> propostasInadimplentes = propostaDAO.getPropostasInadimplentes();
		List<MovimentacaoPropostaInadimplente> inadimplentes = getMovimentacoes();
			
		for (Proposta proposta : propostasInadimplentes) {
			boolean incluir = true;
			for (MovimentacaoPropostaInadimplente inadimplente : inadimplentes) {
				if (inadimplente.getProposta().getId().equals(proposta.getId())) {
					incluir = false;
				}
			}
			if (incluir) {
				this.proposta = proposta;
				incluirInadimplente(currentSession, inadimplenteDAO, atrasadaDAO, propostaDAO);
			}
		}	
		
		avancou = true;
		confimarInadimplentesInc = false;
	}
	
	public void incluirQuitacaoAnterior( ActionEvent e ){
		
		if (propostaAnterior ==  null || propostaAnterior.getId() == null) {
			addErrorMessage("Voc� precisa selecionar uma proposta antes de solicitar incluir");
			proposta = null;
			propostaAnterior = null;
			return;
		}
		
		try {
			Session currentSession = HibernateUtil.currentSession();
			MovimentacaoPropostaAtrasadaDAO dao = new MovimentacaoPropostaAtrasadaDAO(currentSession);
			PropostaDAO propostaDAO = new PropostaDAO(currentSession);
			MovimentacaoPropostaAtrasada novaPropostaAtrasada = new MovimentacaoPropostaAtrasada();
			propostaAnterior = propostaDAO.load(propostaAnterior.getId());
			propostaDAO.populaCapital(propostaAnterior);
			novaPropostaAtrasada.setProposta(propostaAnterior);
			novaPropostaAtrasada.setReferencia(getReferencia());
			novaPropostaAtrasada.setPeriodoQuitacaoAnterior(getDataPeriodoQuitacaoAnterior());
			novaPropostaAtrasada.setPremio((propostaAnterior.getPremioSegurado() != null ? propostaAnterior.getPremioSegurado() : 0.0) + (propostaAnterior.getPremioConjuge() != null ? propostaAnterior.getPremioConjuge(): 0.0));
			dao.saveOrUpdate(novaPropostaAtrasada);
			
			if ( !dao.possuiInadimplencia(propostaAnterior)) {
				propostaAnterior.setDataCancelamento(null);
				propostaAnterior.setMotivoCancelamento(null);
				propostaAnterior.setSituacao(SituacaoProposta.ATIVA);
				propostaDAO.update(propostaAnterior);
				currentSession.flush();
				propostaDAO.atualizarSituacao();
			}
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
		} catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}
		
		proposta = new Proposta();
		propostaAnterior = new Proposta();
	}
	
	public void incluirInadimplente( ActionEvent e ){
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoPropostaInadimplenteDAO inadimplenteDAO = new MovimentacaoPropostaInadimplenteDAO(currentSession);
		MovimentacaoPropostaAtrasadaDAO atrasadaDAO = new MovimentacaoPropostaAtrasadaDAO(currentSession);
		PropostaDAO propostaDAO = new PropostaDAO(currentSession);
		
		if (proposta ==  null || proposta.getId() == null) {
			addErrorMessage("Voc� precisa selecionar uma proposta antes de solicitar incluir");
			proposta = null;
			propostaAnterior = null;
			return;
		}
		
		

		incluirInadimplente(currentSession, inadimplenteDAO, atrasadaDAO,
				propostaDAO);
	}


	private void incluirInadimplente(Session currentSession,
			MovimentacaoPropostaInadimplenteDAO inadimplenteDAO,
			MovimentacaoPropostaAtrasadaDAO atrasadaDAO, PropostaDAO propostaDAO) {
		
		proposta = propostaDAO.load(proposta.getId());
		
		try {
			//antes de acessar valor de pr�mio ou capital de uma proposta � necess�rio chamar este m�todo
			propostaDAO.populaCapital(proposta);
			MotivoCancelamentoDAO motivoDAO = new MotivoCancelamentoDAO(currentSession); 
			MovimentacaoPropostaInadimplente novaProposta = new MovimentacaoPropostaInadimplente();
			novaProposta.setProposta(proposta);
			novaProposta.setReferencia(getReferencia());
			novaProposta.setPremio((proposta.getPremioSegurado() != null ? proposta.getPremioSegurado() : 0.0) + (proposta.getPremioConjuge() != null ? proposta.getPremioConjuge(): 0.0));
			//salva a movimenta��o
			inadimplenteDAO.saveOrUpdate(novaProposta);
			
			/*Coloca a proposta como fora de folha, caso ela ainda n�o esteja*/
			if (proposta.getDataCancelamento() == null && atrasadaDAO.possuiInadimplencia(proposta)) {
				Calendar dtCancelamento = Calendar.getInstance();
				dtCancelamento.setTime(getReferencia());
				int ultimoDiaMes = dtCancelamento.getActualMaximum(Calendar.DAY_OF_MONTH);
				dtCancelamento.set(Calendar.DAY_OF_MONTH, ultimoDiaMes);
				proposta.setDataCancelamento(dtCancelamento.getTime());
				proposta.setMotivoCancelamento(motivoDAO.find(12));
				proposta.setSituacao(SituacaoProposta.INADIMPLENTE);
				propostaDAO.update(proposta);
				currentSession.flush();
				propostaDAO.atualizarSituacao();
			}
			
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
		} catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}
		
		proposta = new Proposta();
		propostaAnterior = new Proposta();
	}
	
	public void excluirInadimplente(ActionEvent e){
		
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoPropostaInadimplenteDAO inadimplenteDAO = new MovimentacaoPropostaInadimplenteDAO(currentSession);
		MovimentacaoPropostaAtrasadaDAO atrasadaDAO = new MovimentacaoPropostaAtrasadaDAO(currentSession);
		PropostaDAO propostaDAO = new PropostaDAO(currentSession);
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirInadimplente");
		MovimentacaoPropostaInadimplente movimentacao = ( MovimentacaoPropostaInadimplente ) param.getValue();
		try {
			inadimplenteDAO.delete(movimentacao);
			
			Proposta propostaMov = movimentacao.getProposta();
			if ( !atrasadaDAO.possuiInadimplencia(propostaMov)) {
				propostaMov.setDataCancelamento(null);
				propostaMov.setMotivoCancelamento(null);
				propostaMov.setSituacao(SituacaoProposta.ATIVA);
				propostaDAO.update(propostaMov);
				currentSession.flush();
				propostaDAO.atualizarSituacao();
			}
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_EXCLUSAO_SUCESSO, null);
			addGlobalMessage( message );
		} catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}
		
	}
	
	public void excluirQuitacaoAnterior(ActionEvent e) {
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoPropostaAtrasadaDAO dao = new MovimentacaoPropostaAtrasadaDAO(currentSession);
		PropostaDAO propostaDAO = new PropostaDAO(currentSession);
		MotivoCancelamentoDAO motivoDAO = new MotivoCancelamentoDAO(currentSession);
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirQA");
		MovimentacaoPropostaAtrasada movimentacao = ( MovimentacaoPropostaAtrasada ) param.getValue();
		
		dao.delete(movimentacao);
		
		if (movimentacao.getProposta().getDataCancelamento() == null && dao.possuiInadimplencia(movimentacao.getProposta())) {
			Calendar dtCancelamento = Calendar.getInstance();
			dtCancelamento.setTime(getReferencia());
			int ultimoDiaMes = dtCancelamento.getActualMaximum(Calendar.DAY_OF_MONTH);
			dtCancelamento.set(Calendar.DAY_OF_MONTH, ultimoDiaMes);
			movimentacao.getProposta().setDataCancelamento(dtCancelamento.getTime());
			movimentacao.getProposta().setMotivoCancelamento(motivoDAO.find(12));
			movimentacao.getProposta().setSituacao(SituacaoProposta.INADIMPLENTE);
			propostaDAO.update(movimentacao.getProposta());
			currentSession.flush();
			propostaDAO.atualizarSituacao();
		}
		
		
		String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_EXCLUSAO_SUCESSO, null);
		addGlobalMessage( message );


	}


	/**
	 * @return the avancou
	 */
	public boolean isAvancou() {
		return avancou;
	}


	/**
	 * @param avancou the avancou to set
	 */
	public void setAvancou(boolean avancou) {
		this.avancou = avancou;
	}


	/**
	 * @return the confimrInclusaoInadimplentes
	 */
	public boolean isConfimarInadimplentesInc() {
		return confimarInadimplentesInc;
	}


	/**
	 * @param confimrInclusaoInadimplentes the confimrInclusaoInadimplentes to set
	 */
	public void setConfimarInadimplentesInc(boolean confimrInclusaoInadimplentes) {
		this.confimarInadimplentesInc = confimrInclusaoInadimplentes;
	}


	public String getInadimplentesAInserir() {
		return inadimplentesAInserir;
	}


	public void setInadimplentesAInserir(String inadimplentesAInserir) {
		this.inadimplentesAInserir = inadimplentesAInserir;
	}


	public Integer getQtdAtrasadas() {
		return qtdAtrasadas;
	}


	public Integer getQtdInadimplentes() {
		return qtdInadimplentes;
	}


	public void setQtdAtrasadas(Integer qtdAtrasadas) {
		this.qtdAtrasadas = qtdAtrasadas;
	}


	public void setQtdInadimplentes(Integer qtdInadimplentes) {
		this.qtdInadimplentes = qtdInadimplentes;
	}
	
	


		
	
	
}
