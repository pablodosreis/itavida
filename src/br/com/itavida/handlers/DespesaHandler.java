package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.DespesaDAO;
import br.com.itavida.entidades.Despesa;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class DespesaHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 2L;
	private Despesa despesa;
	private List<Despesa> resultadoPesquisa;
	private HtmlSelectOneMenu tipoDespesaSelecionada;
	
	// Getters and Setters

	public HtmlSelectOneMenu getTipoDespesaSelecionada() {
		return tipoDespesaSelecionada;
	}
	public void setTipoDespesaSelecionada(HtmlSelectOneMenu tipoDespesaSelecionada) {
		this.tipoDespesaSelecionada = tipoDespesaSelecionada;
	}

	public Despesa getDespesa() {
		return despesa;
	}	

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}
	public List<Despesa> getResultadoPesquisa() {
		return resultadoPesquisa;
	}
	public void setResultadoPesquisa(List<Despesa> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}
	// Fim Getters and Setters
	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public DespesaHandler(){
		cancelar( null );
	}
	
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaDespesasPorNomeOuTipo(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		DespesaDAO dao = new DespesaDAO(session);
		
		Despesa despesaFiltro = getDespesa();
		if( getTipoDespesaSelecionada() != null && getTipoDespesaSelecionada().getValue() != null ){
			despesaFiltro.setTipoDespesa( getTipoDespesaSelecionada().getValue().toString() );
		}

		List<Despesa> lista = dao.busca( despesaFiltro.getNomeDespesa(), despesaFiltro.getTipoDespesa()  );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<Despesa, Integer> dao = new Dao<Despesa, Integer>(session, Despesa.class);
			Despesa despesaGravar = getDespesa();
			despesaGravar.setTipoDespesa( getTipoDespesaSelecionada().getValue().toString() );
			
			dao.merge( despesaGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeDespesa", "Nome da Despesa");
		mapaCampos.put("cmbDespesaFixa", "Despesa Fixa");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarDespesa( ){
		
		//Recupera o item escolhido
		
		//Realiza as a��es de Buscar e setar os valores
		tipoDespesaSelecionada = new HtmlSelectOneMenu();
		tipoDespesaSelecionada.setValue( getDespesa().getTipoDespesa() );
		
		resultadoPesquisa = null;
		
		return "incluirDespesa";
	}
	
	/**
	 * Realiza a exclus�o de uma Despesa
	 * @param e
	 */
	public void excluirDespesa(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirDespesa");
		Despesa DespesaDeletada = ( Despesa ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<Despesa, Integer> dao = new Dao<Despesa, Integer>(session, Despesa.class);
			try {
				dao.delete(DespesaDeletada);
			} catch (Throwable e1) {
				addErrorMessage("Este item n�o pode ser exclu�do pois possui relacionamento em outros cadastros.");
			}
			
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaDespesasPorNomeOuTipo(e);
	}	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setDespesa( new Despesa() );
		this.resultadoPesquisa = null;
		
	}

	
}
