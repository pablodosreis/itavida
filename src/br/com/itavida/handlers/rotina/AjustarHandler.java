package br.com.itavida.handlers.rotina;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.HistoricoSinistroDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dto.AjusteSinistroDto;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.HistoricoSinistro;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.handlers.MainHandler;
import br.com.itavida.util.HibernateUtil;

public class AjustarHandler extends MainHandler implements Serializable{
	
	/**
	 * Default Serial ID
	 */
	private static final long serialVersionUID = 1L;
	
	
	public List<String> ajustes;
	
	public AjustarHandler() {
	}
	
	
	public void ajustar(ActionEvent e) {
		
		Session session = HibernateUtil.currentSession();
		HistoricoSinistroDAO dao = new HistoricoSinistroDAO();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		
		List<HistoricoSinistro> sinistros = dao.findAll();
		ajustes = new ArrayList<String>();
		//0 -> id 1 -> proposta 2 - dataalteracao
		List<Object[]> lista = propostaDAO.recuperarHistoricos();
		List<AjusteSinistroDto> parcial = new ArrayList<AjusteSinistroDto>();
		//Map<Integer, List<Object[]>> resultado = new HashMap<Integer, List<Object[]>>();
		
		Integer ultimaProposta = -1;
		Date ultimaData = null;
		for(Object[] objeto : lista) {
			Integer idProposta = (Integer) objeto[1];
			Date dataAlteracao = (Date) objeto[2];
			Integer idEmpresa = (Integer) objeto[3];
			
			if(idProposta.equals(ultimaProposta)) {
				//mesma proposta
				AjusteSinistroDto item = criar(idProposta, idEmpresa, ultimaData, dataAlteracao);
				parcial.add(item);
			} else {
				//proposta mudou
				if(ultimaProposta != -1) {
					Proposta proposta = (Proposta) session.get(Proposta.class,ultimaProposta);
					AjusteSinistroDto item = criar(ultimaProposta, proposta.getEmpresa().getId(), ultimaData, new Date());
					parcial.add(item);
				}
				Calendar inicio = Calendar.getInstance();
				inicio.set(1990, 01, 01);
				AjusteSinistroDto item = criar(idProposta, idEmpresa, inicio.getTime(), dataAlteracao);
				parcial.add(item);
			}
			ultimaData = dataAlteracao;
			ultimaProposta = idProposta;
		}
		Collections.sort(parcial);
		System.out.println("\n\n\n\n\n");
		for(AjusteSinistroDto item : parcial) {
			System.out.println(item);	
		}
		
		System.out.println("\n\n");
		for(HistoricoSinistro historicoSinistro : sinistros) {
			Integer empresaId = buscarApoliceMomentoSinistro(historicoSinistro, parcial);
			Empresa empresa = (Empresa) session.get(Empresa.class, empresaId);
			historicoSinistro.setEmpresa(empresa);
			dao.saveOrUpdate(historicoSinistro);
			System.out.println("Atualizado Sinistro: "+ historicoSinistro.getId() + " Empresa: " + empresa.getNomeEmpresa());
		}
		
		
	}
	
	public Integer buscarApoliceMomentoSinistro(HistoricoSinistro historicoSinistro, List<AjusteSinistroDto> lista) {
		
		for(AjusteSinistroDto item : lista) {
			Date momentoSinistro = historicoSinistro.getDataSinistro();
			Date inicio = item.inicio;
			Date fim = item.fim;
			if(historicoSinistro.getProposta().getId().equals(item.propostaId)) {
				if(momentoSinistro.getTime() >= inicio.getTime() && momentoSinistro.getTime() < fim.getTime()) {
					return item.empresaId;
				}
			}
		}
		
		return 1;
	}
	
	public AjusteSinistroDto criar(Integer propostaId, Integer empresaId, Date inicio, Date fim) {
		Map<Integer, String> mapEmpresa = new HashMap<Integer, String>();
		mapEmpresa.put(1, "ITAVIDA SEGUROS D");
		mapEmpresa.put(2, "ITAPREVENT SEGUROS");
		mapEmpresa.put(3, "ITAVIDA SEGUROS T");
		mapEmpresa.put(4, "ITAVIDA SEGUROS ICATU");
		mapEmpresa.put(5, "ITAVIDA SEGUROS R");
		
		AjusteSinistroDto retorno = new AjusteSinistroDto();
		retorno.propostaId = propostaId;
		retorno.empresaId = empresaId;
		retorno.empresaNome = mapEmpresa.get(empresaId);
		retorno.inicio = inicio;
		retorno.fim = fim;
		return retorno;
	}
	
}
