package br.com.itavida.handlers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.hibernate.Session;
import org.xhtmlrenderer.protocols.data.DataURLConnection;

import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.OrgaoQuitacaoDAO;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.OrgaoQuitacao;
import br.com.itavida.exceptions.ImportacaoExcelException;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class QuitacaoOrgaoHandler extends MainHandler{
	private String periodoQuitacao;
	private String periodoFim;
	private ArrayList<OrgaoQuitacao> listaOrgaoQuitacao;
	private boolean marcarDesmarcarTodos;
	private boolean exibirGravar;
	private String orgaoSelecionado;
	private HtmlSelectOneMenu itemOrgaoSelecionado;
	private String buscarQuitado;
	private HtmlSelectOneMenu itemBuscarQuitado;
	private List<Orgao> listaOrgaos;
	
	public QuitacaoOrgaoHandler (){
		Session session = HibernateUtil.currentSession();
		OrgaoDAO dao = new OrgaoDAO( session );
		listaOrgaos = dao.findAllOrgaos();
		jsfVersion();
	}
	
	public void buscaValores(Date data){
		
		
		Session session = HibernateUtil.currentSession();
		OrgaoQuitacaoDAO orgaoQuitacaoDao = new OrgaoQuitacaoDAO(session);
		
		listaOrgaoQuitacao = (ArrayList<OrgaoQuitacao>) orgaoQuitacaoDao.retornaRegistrosOrgaoQuitacao(listaOrgaos, data);
			
	}
	
	
	public void jsfVersion(){
        Package pkg = FacesContext.class.getPackage();
        System.out.println("vers�o do JSF: "  + pkg.getSpecificationVersion()); 
                //vers�o do JSF: 2.1
        System.out.println("vers�o da implementa��o: "  + pkg.getImplementationVersion()); 
                //vers�o da implementa��o: 1.0.0.0_2-1
    }
	
	
	@SuppressWarnings("unused")
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		OrgaoQuitacaoDAO orgaoQuitacaoDao = new OrgaoQuitacaoDAO(session);
		String respVerficaGravacao = verificacaoGravacao();
		if (respVerficaGravacao == null) {
			orgaoQuitacaoDao.salvaTodos(listaOrgaoQuitacao);
			exibirGravar = false;
			listaOrgaoQuitacao.clear();
			periodoQuitacao = null;
			addGlobalMessage("Quita��o realizada com sucesso!");
			session.clear();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public String verificacaoGravacao(){
		String resposta = null;
		List<OrgaoQuitacao> verificaDoisMeses = new ArrayList<OrgaoQuitacao>();
		Session session = HibernateUtil.currentSession();
		OrgaoQuitacaoDAO orgaoQuitacaoDao = new OrgaoQuitacaoDAO(session);		
		for(OrgaoQuitacao orgaoQuitacao : listaOrgaoQuitacao){
			verificaDoisMeses = orgaoQuitacaoDao.validaNaoQuitacaoDoisMeses(orgaoQuitacao);
			//verifica se existem mais de um m�s sem quitacao
			if(verificaDoisMeses != null){
				if(verificaDoisMeses.size() == 2){
					if(orgaoQuitacao.getQuitado()){
						OrgaoQuitacao orQuitMenororCompetencia = orgaoQuitacaoDao.getPrimeiraCompetenciaNaoQuitada(orgaoQuitacao);
						if(!orQuitMenororCompetencia.getDataQuitacao().equals(orgaoQuitacao.getDataQuitacao())){
							resposta = "Para que n�o haj� janelas, pedimos que seja quitada  primeiramente a compet�ncia: "
									+ orQuitMenororCompetencia.getMesAno() + " do �rg�o (" + orQuitMenororCompetencia.getOrgao().getNomeOrgao()+")";
							addErrorMessage(resposta);
						}
					}else{
						resposta = "Org�o n�o podem ser incluidos devido a n�o quita��o de dois meses anteriores";
						addErrorMessage(resposta);
						for(OrgaoQuitacao orQuit : verificaDoisMeses){
							addErrorMessage("Org�o : " + orQuit.getOrgao().getNomeOrgao() + " - " + orQuit.getMesAno());				
						}//fim for interno						
					}					
				}				
			}			
		}// fim do for
		session.clear();
		System.out.println(resposta);
		return resposta;
	}
		
	
	
	
	public void buscarDados(ActionEvent e) {
		Double mes = null;
		Double ano = null;
		if(periodoQuitacao == null){
			addErrorMessage("Informe um per�odo para a quita��o (formato MM/AAAA)");
			exibirGravar = false;
			return;
		}else{
			try{
				mes = Double.parseDouble(periodoQuitacao.split("/")[0]);
				ano = Double.parseDouble(periodoQuitacao.split("/")[1]);
			}catch(NumberFormatException ne){
				addErrorMessage("Informe um per�odo para a quita��o (formato MM/AAAA).");
				exibirGravar = false;
				return;
			}
			if(mes < 1 || mes > 12){
				addErrorMessage("O m�s deve ser um n�mero entre 1 e 12.");
				exibirGravar = false;
				return;
			}
		}
		
		try {
			Date dataQuitacao = DateUtil.getDate("01/"+mes.intValue()+"/"+ano.intValue(), "dd/MM/yyyy");
			buscaValores(dataQuitacao);
			exibirGravar = true;
		} catch (ParseException e1) {
			addErrorMessage("Verifique a data informada.");
			exibirGravar = false;
			return;
		}
	}
	
	/**
	 * A data poderia ser buscada da tela. Mas o usu�rio poderia alterar a data, clicar em excluir, e os dados exclu�dos n�o
		seriam os exibidos na tela no momento. Por isso, a data a ser exclu�da vai ser identificada atrav�s da listagem exibida.
		Assim, caso queira excluir outra data, o usu�rio dever� fazer a busca dos dados novamente.
		como o bot�o de excluir somente � exibido quando a listagem retorna algum valor, esse m�todo sempre executa com algum item na lista.
		@author Wellington Bruno
	 */
	public void excluirDados(ActionEvent e){
		Date dataAExcluir = listaOrgaoQuitacao.get(0).getDataQuitacao();
		Session session = HibernateUtil.currentSession();
		OrgaoQuitacaoDAO orgaoQuitacaoDao = new OrgaoQuitacaoDAO(session);
		periodoQuitacao = listaOrgaoQuitacao.get(0).getMesAno();
		orgaoQuitacaoDao.excluiDadosPorData(dataAExcluir);
		listaOrgaoQuitacao.clear();
		exibirGravar = false;
		addGlobalMessage("Registros de quita��o para o per�odo "+periodoQuitacao+" excluidos com sucesso!");
		periodoQuitacao= null;
	}
	
	public boolean getPodeExibirExcluir(){
		if (listaOrgaoQuitacao != null && !listaOrgaoQuitacao.isEmpty()) {
			//Se algum item da lista n�o tem id preenchido ent�o existe cadastro
			if(listaOrgaoQuitacao.get(0).getId() != null){
				return true;
			}
		}
		return false;
	}
	
	public void pesquisaQuitacao(ActionEvent e){
		Date dataInicio = null;
		Date dataFim = null;
		Integer idOrgao = null;
		Boolean apenasQuitados = null;
		try{
			if((periodoQuitacao != null && periodoFim == null ) || (periodoQuitacao == null && periodoFim != null )){
				addErrorMessage("Preencha os dois filtros para per�odo.");
				return;
			}
			if(periodoQuitacao != null && !periodoQuitacao.isEmpty()){
				Double mesInicio = null;
				Double anoInicio = null;
				try{
					mesInicio = Double.parseDouble(periodoQuitacao.split("/")[0]);
					anoInicio = Double.parseDouble(periodoQuitacao.split("/")[1]);
				}catch(NumberFormatException ne){
					addErrorMessage("Informe um per�odo inicial no formato MM/AAAA.");
					return;
				}
				if(mesInicio < 1 || mesInicio > 12){
					addErrorMessage("O m�s deve ser um n�mero entre 1 e 12.");
					return;
				}
				dataInicio = DateUtil.getDate("01/"+mesInicio.intValue()+"/"+anoInicio.intValue(), "dd/MM/yyyy");
			}
			
			if(periodoFim != null && !periodoFim.isEmpty()){
				Double mesFim = null;
				Double anoFim = null;
				try{
					mesFim = Double.parseDouble(periodoFim.split("/")[0]);
					anoFim = Double.parseDouble(periodoFim.split("/")[1]);
				}catch(NumberFormatException ne){
					addErrorMessage("Informe um per�odo final no formato MM/AAAA.");
					return;
				}
				if(mesFim < 1 || mesFim > 12){
					addErrorMessage("O m�s deve ser um n�mero entre 1 e 12.");
					return;
				}
				dataFim = DateUtil.getDate("01/"+mesFim.intValue()+"/"+anoFim.intValue(), "dd/MM/yyyy");
			}
			if(itemOrgaoSelecionado != null && itemOrgaoSelecionado.getValue() != null){
				idOrgao = Integer.parseInt(itemOrgaoSelecionado.getValue().toString());
			}
			if(itemBuscarQuitado != null && itemBuscarQuitado.getValue() != null){
				if(itemBuscarQuitado.getValue().toString().equals("S")){
					apenasQuitados = true;
				}else{
					apenasQuitados = false;
				}
			}
			
			Session session = HibernateUtil.currentSession();
			OrgaoQuitacaoDAO orgaoQuitacaoDao = new OrgaoQuitacaoDAO(session);
			
			listaOrgaoQuitacao = (ArrayList<OrgaoQuitacao>) orgaoQuitacaoDao.retornaOrgaoQuitacao(idOrgao, dataInicio, dataFim, apenasQuitados);
			if (listaOrgaoQuitacao == null || listaOrgaoQuitacao.isEmpty()) {
				String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
				addWarnMessage( message );
			}
		}catch (ParseException e1) {
			addErrorMessage("Verifique a data informada.");
			return;
		}
	}
	
	public List<SelectItem> getOrgaoCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		for(Orgao o : listaOrgaos){
			retorno.add( new SelectItem( o.getId(), o.getNomeOrgao() ) );
		}
		return retorno;
	}	
	
	public ArrayList<OrgaoQuitacao> getListaOrgaoQuitacao() {
		return listaOrgaoQuitacao;
	}
	public void setListaOrgaoQuitacao(ArrayList<OrgaoQuitacao> listaOrgaoQuitacao) {
		this.listaOrgaoQuitacao = listaOrgaoQuitacao;
	}
	public String getPeriodoQuitacao() {
		return periodoQuitacao;
	}
	public void setPeriodoQuitacao(String periodoQuitacao) {
		this.periodoQuitacao = periodoQuitacao;
	}

	public boolean isMarcarDesmarcarTodos() {
		return marcarDesmarcarTodos;
	}

	public void setMarcarDesmarcarTodos(boolean marcarDesmarcarTodos) {
		this.marcarDesmarcarTodos = marcarDesmarcarTodos;
	}

	public boolean isExibirGravar() {
		return exibirGravar;
	}

	public void setExibirGravar(boolean exibirGravar) {
		this.exibirGravar = exibirGravar;
	}

	@Override
	public String toString() {
		return "QuitacaoOrgaoHandler [orgaoSelecionado=" + orgaoSelecionado
				+ ", listaOrgaos=" + listaOrgaos + "]";
	}

	public HtmlSelectOneMenu getItemOrgaoSelecionado() {
		return itemOrgaoSelecionado;
	}

	public void setItemOrgaoSelecionado(HtmlSelectOneMenu itemOrgaoSelecionado) {
		this.itemOrgaoSelecionado = itemOrgaoSelecionado;
	}

	public String getPeriodoFim() {
		return periodoFim;
	}

	public void setPeriodoFim(String periodoFim) {
		this.periodoFim = periodoFim;
	}

	public HtmlSelectOneMenu getItemBuscarQuitado() {
		return itemBuscarQuitado;
	}

	public void setItemBuscarQuitado(HtmlSelectOneMenu itemBuscarQuitado) {
		this.itemBuscarQuitado = itemBuscarQuitado;
	}

	public String getBuscarQuitado() {
		return buscarQuitado;
	}

	public void setBuscarQuitado(String buscarQuitado) {
		this.buscarQuitado = buscarQuitado;
	}

	public String getOrgaoSelecionado() {
		return orgaoSelecionado;
	}

	public void setOrgaoSelecionado(String orgaoSelecionado) {
		this.orgaoSelecionado = orgaoSelecionado;
	}

}
