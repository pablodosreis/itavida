package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.NomeTabelaDAO;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class NomeTabelaHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 2L;
	private NomeTabela nomeTabela;
	private List<NomeTabela> resultadoPesquisa;
	
	// Getters and Setters


	public List<NomeTabela> getResultadoPesquisa() {
		return resultadoPesquisa;
	}
	public NomeTabela getNomeTabela() {
		return nomeTabela;
	}
	
	public void setNomeTabela(NomeTabela nomeTabela) {
		this.nomeTabela = nomeTabela;
	}
	public void setResultadoPesquisa(List<NomeTabela> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}
	// Fim Getters and Setters
	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public NomeTabelaHandler(){
		cancelar( null );
	}
	
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaNomesTabelasPorNome(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		NomeTabelaDAO dao = new NomeTabelaDAO(session);
		
		NomeTabela nomeTabelaFiltro = getNomeTabela();

		List<NomeTabela> lista = dao.busca( nomeTabelaFiltro.getNomeTabela(), nomeTabelaFiltro.getNomeTabelaReduzido() );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<NomeTabela, Integer> dao = new Dao<NomeTabela, Integer>(session, NomeTabela.class);
			NomeTabela nomeTabelaGravar = getNomeTabela();
			dao.merge( nomeTabelaGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeTabela", "Nome da Tabela");
		mapaCampos.put("txtNomeReduzidoTabela", "Nome Reduzido da Tabela");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarNomeTabela( ){
		
		resultadoPesquisa = null;
		return "incluirNomeTabela";
	}
	
	/**
	 * Realiza a exclus�o de uma NomeTabela
	 * @param e
	 */
	public void excluirNomeTabela(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirNomeTabela");
		NomeTabela nomeTabelaDeletada = ( NomeTabela ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<NomeTabela, Integer> dao = new Dao<NomeTabela, Integer>(session, NomeTabela.class);
		dao.delete(nomeTabelaDeletada);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaNomesTabelasPorNome(e);
	}	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setNomeTabela( new NomeTabela() );
		this.resultadoPesquisa = null;
		
	}

	
}
