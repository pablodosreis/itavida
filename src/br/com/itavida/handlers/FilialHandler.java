package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.FilialDAO;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.Filial;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class FilialHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private Filial filial;
	private List<Filial> resultadoPesquisa;
	private String cidadeSelecionada;
	private List<Cidade> listaCidades;
	
	// Getters and Setters
	
	public List<Cidade> getListaCidades() {
		return listaCidades;
	}


	public void setListaCidades(List<Cidade> listaCidades) {
		this.listaCidades = listaCidades;
	}
	
	
	public Filial getFilial() {
		return filial;
	}


	public void setFilial(Filial filial) {
		this.filial = filial;
	}
	
	
	public List<Filial> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Filial> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}
	
	public String getCidadeSelecionada() {
		return cidadeSelecionada;
	}


	public void setCidadeSelecionada(String cidadeSelecionada) {
		this.cidadeSelecionada = cidadeSelecionada;
	}

	// Fim Getters and Setters
	

	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public FilialHandler(){
		cancelar( null );
	}

	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaFiliais(ActionEvent e){
		Session session = HibernateUtil.currentSession();
		FilialDAO dao = new FilialDAO( session );
		
		Filial filialFiltro = getFilial();
		
		List<Filial> lista = dao.buscaPorFiltro( filialFiltro  );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao( getFilial().getCidade().getId(), "Cidade") ){
		
			Dao<Filial, Integer> dao = new Dao<Filial, Integer>(session, Filial.class);
			Filial filialGravar = getFilial();
			
			dao.merge( filialGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeFilial", "Nome da Filial");
		mapaCampos.put("txtTelefone", "Telefone");
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarFilial( ){
			
		return "incluirFilial";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirFilial(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirFilial");
		Filial filialDeletada  = ( Filial ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<Filial, Integer> dao = new Dao<Filial, Integer>(session, Filial.class);
		dao.delete(filialDeletada);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaFiliais(e);
	}	
	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setFilial( new Filial() );
		getFilial().setCidade(new Cidade());
		this.resultadoPesquisa = null;
		
	}

	public Cidade getLimparCidade(){
		return new Cidade();
	}
	
}
