package br.com.itavida.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.Session;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.ExportacaoDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dao.QuitacaoDAO;
import br.com.itavida.entidades.ComportamentoPropostaEnum;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.NumeroSorteio;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.PropostaQuitacao;
import br.com.itavida.entidades.Quitacao;
import br.com.itavida.exceptions.ImportacaoExcelException;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.File;
import br.com.itavida.util.HibernateUtil;

public class ImportarQuitacaoHandler extends MainHandler {
	
	private static Logger logger = Logger.getLogger(ImportarQuitacaoHandler.class.toString());

	private List<Integer> listaOrgaosSelecionados = new ArrayList<Integer>();
	private List<SelectItem> orgaosSelectItens;

	private ArrayList<File> files = new ArrayList<File>();
	private int uploadsAvailable = 1;

	private Integer radioSelected;
	private Integer empresaSelected;
	private List<SelectItem> opcoesEmpresaCombo;
	private static final int QUANTIDADE_COLUNAS = 15;
	private static final int INICIO_DOS_DADOS = 7;
	
	private PropostaDAO propostaDAO;
	
	private Map<String,Proposta> propostas;

	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public Integer getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Integer empresaSelected) {
		this.empresaSelected = empresaSelected;
	}

	/**
	 * Popula um combo com op��es do Agenciador
	 * 
	 * @return
	 */
	public void populaOpcoesEmpresaCombo() {

		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO = new EmpresaDAO(session);

		List<Empresa> listaEmpresas = empresaDAO.buscaEmpresas();

		// Realiza as a��es de banco: Busca e seta os valores
		opcoesEmpresaCombo = new ArrayList<SelectItem>();
		opcoesEmpresaCombo.add(new SelectItem(null, "Selecione:"));
		for (Iterator iterator = listaEmpresas.iterator(); iterator.hasNext();) {
			Empresa empresa = (Empresa) iterator.next();
			opcoesEmpresaCombo.add(new SelectItem(empresa.getId(), empresa
					.getNomeEmpresa()));
		}

	}

	public int getSize() {
		if (getFiles().size() > 0) {
			return getFiles().size();
		} else {
			return 0;
		}
	}

	public void paint(OutputStream stream, Object object) throws IOException {
		stream.write(getFiles().get((Integer) object).getData());
	}

	public void listener(UploadEvent event) throws Exception {
		UploadItem item = event.getUploadItem();
		File file = new File();
		file.setLength(item.getData().length);
		file.setName(item.getFileName());
		file.setData(item.getData());
		files.add(file);
		uploadsAvailable = 0;
	}

	public String clearUploadData() {
		files.clear();
		uploadsAvailable = 1;
		return null;
	}

	public ImportarQuitacaoHandler() {
		populaOrgaosSelectItens(null);
		populaOpcoesEmpresaCombo();
	}

	/**
	 * Popula um combo com valores dos Tipos de Proposta
	 * 
	 * @return
	 */
	public void populaOrgaosSelectItens(ActionEvent e) {

		// Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		orgaosSelectItens = new ArrayList<SelectItem>();

		OrgaoDAO orgaoDAO = new OrgaoDAO(session);
		List<Orgao> listaOrgaos = orgaoDAO.findAllOrgaos();

		for (Iterator iterator = listaOrgaos.iterator(); iterator.hasNext();) {
			Orgao orgao = (Orgao) iterator.next();
			orgaosSelectItens.add(new SelectItem(orgao.getId(), orgao
					.getSiglaOrgao() + " - " + orgao.getNomeOrgao()));
		}

	}

	public void limparDados(ActionEvent e) {
		populaOrgaosSelectItens(e);
		listaOrgaosSelecionados = new ArrayList<Integer>();
		clearUploadData();
	}
	
	public Proposta getProposta(String cpfParametro) {
		String cpf = cpfParametro.trim();
		cpf = cpf.replaceAll("\\.", "").replaceAll("-", "").replaceAll("/", "").replaceAll("_", "");
		Proposta proposta = propostas.get(cpf);
		if(proposta == null) {
			Proposta filtro = new Proposta();
			filtro.setCpfSegurado(cpfParametro);
			filtro.setCpfSeguradoFormatado( cpf);
			proposta =  propostaDAO.getPropostaPorCPF(filtro);
		}
		return proposta;
	}
	
	public PropostaDAO getDAO() {
		Session session = HibernateUtil.currentSession();
		propostaDAO = new PropostaDAO(session);
		return propostaDAO;
	}
	
	public void preencherPropostas() {
		propostas = new HashMap<String, Proposta>();
		List<Proposta> propostasNaoCanceladas = getDAO().obterTodasPropostasNaoCanceladas();
		for(Proposta p : propostasNaoCanceladas) {
			String cpf = p.getCpfSegurado().trim();
			cpf = cpf.replaceAll("\\.", "");
			cpf = cpf.replaceAll("-", "");
			propostas.put(cpf, p);
		}
	}

	public String importReport() {
		try {
			return importar();
		}  catch (ImportacaoExcelException e) {
			logger.log(Level.WARNING,"Erro ao importar os dados",e);
			addErrorMessage(e.getMessage());
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Erro ao importar os dados",e);
			addErrorMessage("Ocorreu um erro ao importar o arquivo, verifique se o arquivo � v�lido para esta importa��o.");
		}
		return null;
	}

	public String importar() throws ImportacaoExcelException {
		long init = System.currentTimeMillis(); 
		if (getSize() == 0) {
			addErrorMessage("Selecione algum arquivo para realizar a Importa��o dos N�meros de Sorteio.");
		}

		preencherPropostas();
		
		boolean falhou = false;

		File file = files.get(0);
		ByteArrayInputStream excelFIS = null;
		excelFIS = new ByteArrayInputStream(file.getData());

		HSSFWorkbook excelWB = null;

		try {
			excelWB = new HSSFWorkbook(excelFIS);
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Erro ao importar os dados", e);
			falhou = true;
			addErrorMessage("O formato desta planilha � incorreto. Favor verificar a planilha selecionada.");
		}

		// Passa pelas sheets...
		for (int indexSheet = 0; indexSheet < excelWB.getNumberOfSheets(); indexSheet++) {
		//for (int indexSheet = 0; indexSheet < 1; indexSheet++) {
			
			HSSFSheet sheet = excelWB.getSheetAt(indexSheet);
			int rows = sheet.getPhysicalNumberOfRows();
			
			if (sheet.getRow(INICIO_DOS_DADOS).getPhysicalNumberOfCells() != QUANTIDADE_COLUNAS) {
				addErrorMessage("O formato desta planilha � incorreto. Favor verificar a planilha selecionada.");
				falhou = true;
				return null;
			}

			if (!falhou) {
				Session session = HibernateUtil.currentSession();
				PropostaDAO propostaDAO = getDAO();
				QuitacaoDAO quitacaoDAO = new QuitacaoDAO(session);
				
				List<PropostaQuitacao> propostasQuitacao = new ArrayList<PropostaQuitacao>();
				
				// DataReferncia...
				Date dataReferencia3 = null;
				Date dataReferencia2 = null;
				Date dataReferencia1 = null;
				HSSFRow dataReferenciaRow = sheet.getRow(INICIO_DOS_DADOS-1);
				HSSFCell cellDataReferencia3 = dataReferenciaRow.getCell(12);
				HSSFCell cellDataReferencia2 = dataReferenciaRow.getCell(13);
				HSSFCell cellDataReferencia1 = dataReferenciaRow.getCell(14);
				
				// Empresa...
				EmpresaDAO empresaoDAO = new EmpresaDAO(session);
				Empresa empresa = empresaoDAO.buscaUnicaPeloNome(excelWB.getSheetName(indexSheet));
				
				if (cellDataReferencia3.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					String dataReferenciaString = cellDataReferencia3.getRichStringCellValue().getString();
					dataReferencia3 = converteStringParaData(dataReferenciaString);
				}
				
				if (cellDataReferencia2.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					String dataReferenciaString = cellDataReferencia2.getRichStringCellValue().getString();
					dataReferencia2 = converteStringParaData(dataReferenciaString);
				}
				
				if (cellDataReferencia1.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					String dataReferenciaString = cellDataReferencia1.getRichStringCellValue().getString();
					dataReferencia1 = converteStringParaData(dataReferenciaString);
				}
				
				List<String> erros = new ArrayList<String>();
				
				//TODO testar incrementar um em rows
				for (int rowNumber = INICIO_DOS_DADOS; rowNumber < rows + 1; rowNumber++) {
					System.out.println(rowNumber);
					HSSFRow oneRow = sheet.getRow(rowNumber);
					
					if (oneRow != null) {						
						// Valor Total...
						Double valorTotal = 0.0;
						HSSFCell cellValorTotal = oneRow.getCell(11);
						if (cellValorTotal != null && cellValorTotal.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							valorTotal = cellValorTotal.getNumericCellValue();
						} else {
							continue;
						}
	
						// Proposta...
						HSSFCell cellCpfFuncionario = oneRow.getCell(3);
						Proposta proposta = null;
						if (cellCpfFuncionario != null && (cellCpfFuncionario.getCellType() == HSSFCell.CELL_TYPE_STRING || cellCpfFuncionario.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)) {
							// CPF proposta...
							String cpf = cellCpfFuncionario.getRichStringCellValue().getString();
							proposta =  getProposta(cpf);
							
							if(proposta == null) {
								erros.add("Proposta n�o encontrada. CPF: " + cpf);
								continue;
							}
						} 
						
						
						
						PropostaQuitacao propostaQuitacao = new PropostaQuitacao(
								proposta, empresa, dataReferencia3, valorTotal);
						
						
						try {
							HSSFCell cellMes3 = oneRow.getCell(12);
							Quitacao quitacao3 = new Quitacao(propostaQuitacao, isQuitado(cellMes3), dataReferencia3);
							propostaQuitacao.getQuitacao_collection().add(quitacao3);
						} catch (Exception e) {
							//faz nada
						}
						
						try {
							HSSFCell cellMes2 = oneRow.getCell(13);
							Quitacao quitacao2 = new Quitacao(propostaQuitacao, isQuitado(cellMes2), dataReferencia2);
							propostaQuitacao.getQuitacao_collection().add(quitacao2);
						} catch (Exception e) {
							//faz nada
						}
						
						try {
							HSSFCell cellMes1 = oneRow.getCell(14);
							Quitacao quitacao1 = new Quitacao(propostaQuitacao, isQuitado(cellMes1), dataReferencia1);
							propostaQuitacao.getQuitacao_collection().add(quitacao1);
						} catch (Exception e) {
							//faz nada
						}
						
						propostasQuitacao.add(propostaQuitacao);

					}
				}
				
				if(erros.isEmpty()) {
					quitacaoDAO.importarDados(propostasQuitacao, dataReferencia3);
				} else {
					falhou = true;
					for(String erro : erros) {
						addErrorMessage(erro);
					}
				}
			}
			  
		}
		
		if (!falhou) {
			addGlobalMessage("Importa��o realizada com sucesso!");
		}
		
		  
		/* Coloque aqui seu codigo que demora */  
		long end = System.currentTimeMillis();  
		long diff = end - init;  
		System.out.println("Demorou " + (diff / 1000) + " segundos");  
		return null;
	}
	
	public boolean isQuitado(HSSFCell cell) {
		try {
			String valor = cell.getRichStringCellValue().getString();
			return valor.toUpperCase().contains("QUITADO");
		} catch (Exception e) {
			throw new IllegalArgumentException();
		}
		
	}

	private Date converteStringParaData(String dataReferenciaString) {
		List<String> meses = Arrays.asList("Jan.","Fev.", "Mar.", "Abr.", "Maio", "Jun.", "Jul.", "Ago.", "Set.", "Out.", "Nov.", "Dez.");
		String[] dataReferenciaAux = dataReferenciaString.split("/");
		int mes = meses.indexOf(dataReferenciaAux[0]);
		String anoString = dataReferenciaAux[1];
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, Integer.valueOf(anoString));
		calendar.set(Calendar.MONTH, mes);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		
		DateUtil.zerarDiaHoraMinutoSegundo(calendar);
		return calendar.getTime();
	}

	public String exportReport() {
		if (radioSelected == null || radioSelected == 0) {
			addErrorMessage("Selecione uma op��o para exporta��o do arquivo de N�meros de Sorteio.");
			return null;
		}

		if (empresaSelected == null || empresaSelected == 0) {
			addErrorMessage("Campo EMPRESA � de Sele��o Obrigat�ria para esta a��o.");
			return null;
		}

		boolean falhou = false;

		HSSFWorkbook excelWB = null;
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		List<Proposta> propostas = propostaDAO
				.buscarPropostasAtivasSemNumeroSorteio(radioSelected,
						empresaSelected);
		Dao<Empresa, Integer> daoEmpresa = new Dao<Empresa, Integer>(session,
				Empresa.class);
		Empresa empresa = daoEmpresa.get(empresaSelected);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dfFileName = new SimpleDateFormat("ddMMyyyy");
		try {
			InputStream excelFIS = this.getClass().getResourceAsStream(
					Constantes.SISVIDA_FILES +"CapitalizacaoAplub.xls");
			excelWB = new HSSFWorkbook(excelFIS);

		} catch (Exception e) {
			falhou = true;
			addErrorMessage("O arquivo n�o foi encontrado! Favor verificar se o arquivo est� no local correto.");
		}

		HSSFSheet oneSheet = excelWB.getSheetAt(0);

		// Now get the number of rows in the sheet
		int rows = oneSheet.getPhysicalNumberOfRows();
		HSSFRow firstRow = oneSheet.getRow(0);
		if (firstRow.getPhysicalNumberOfCells() != 4) {
			addErrorMessage("O formato desta planilha � incorreto. Favor verificar a planilha selecionada.");
			falhou = true;
			return null;
		}

		HSSFCellStyle estiloFullBorder = excelWB.createCellStyle();

		// Adicionando bordas para o CantoEsquerdo
		estiloFullBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		estiloFullBorder.setBottomBorderColor(HSSFColor.BLACK.index);
		estiloFullBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		estiloFullBorder.setLeftBorderColor(HSSFColor.GREEN.index);
		estiloFullBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);
		estiloFullBorder.setTopBorderColor(HSSFColor.BLACK.index);
		estiloFullBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);
		estiloFullBorder.setRightBorderColor(HSSFColor.GREEN.index);

		if (!falhou) {

			int rowNumber = 15;
			HSSFRow row = oneSheet.getRow(rowNumber);
			if (row == null) {
				oneSheet.createRow(15);
				row = oneSheet.getRow(rowNumber);
			}
			for (Proposta proposta : propostas) {
				row.createCell(0).setCellValue(
						new HSSFRichTextString(proposta.getNomeSegurado()));
				row.createCell(1).setCellValue(
						new HSSFRichTextString(df.format(proposta
								.getDataNascimentoSegurado())));
				String cpfSemFormatacao = "";
				if (proposta.getCpfSegurado() != null) {
					cpfSemFormatacao = proposta.getCpfSegurado()
							.replace(".", "").replace("_", "").replace("-", "");

				}
				row.createCell(2).setCellValue(
						new HSSFRichTextString(cpfSemFormatacao));
				NumeroSorteio numSorteio = null;
				if (proposta.getNumerosorteio_collection() != null
						&& proposta.getNumerosorteio_collection().size() > 0) {
					for (Iterator iterator = proposta
							.getNumerosorteio_collection().iterator(); iterator
							.hasNext();) {
						numSorteio = (NumeroSorteio) iterator.next();
						break;
					}
				}
				if (numSorteio != null && numSorteio.getId() != null
						&& numSorteio.getId() != 0) {
					row.createCell(3).setCellValue(
							new HSSFRichTextString(numSorteio
									.getNumeroSorteio()));
				} else {
					row.createCell(3).setCellValue(new HSSFRichTextString(""));
				}

				row.getCell(0).setCellStyle(estiloFullBorder);
				row.getCell(1).setCellStyle(estiloFullBorder);
				row.getCell(2).setCellStyle(estiloFullBorder);
				row.getCell(3).setCellStyle(estiloFullBorder);

				rowNumber++;
				oneSheet.createRow(rowNumber);
				row = oneSheet.getRow(rowNumber);
			}

			HSSFRow header = oneSheet.getRow(0);
			for (short column = 0; column < header.getLastCellNum(); column++) {
				oneSheet.autoSizeColumn(column);
			}

		}

		HttpServletResponse res = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		res.setContentType("application/vnd.ms-excel");
		res.setHeader(
				"Content-disposition",
				"attachment; filename=SeguradosSemTitulo_APLUB_"
						+ empresa.getNomeEmpresa() + "_"
						+ dfFileName.format(new Date()) + ".xls");

		try {
			ServletOutputStream out = res.getOutputStream();
			excelWB.write(out);
			out.flush();
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		FacesContext faces = FacesContext.getCurrentInstance();
		faces.responseComplete();

		return "export";
	}

	public String generateReport() {

		Session session = HibernateUtil.currentSession();
		ExportacaoDAO exportacaoDAO = new ExportacaoDAO(session);
		Object[] objetosListaOrgao = listaOrgaosSelecionados.toArray();
		Integer[] orgaosSelecionadosInteger = new Integer[objetosListaOrgao.length];
		for (int i = 0; i < objetosListaOrgao.length; i++) {
			orgaosSelecionadosInteger[i] = new Integer(objetosListaOrgao[i]
					.toString().trim());
		}

		List<Proposta> listaPropostas = exportacaoDAO.buscaPorOrgao(
				orgaosSelecionadosInteger, false);

		Map<Integer, List<Proposta>> mapaPropostasPorOrgao = new HashMap<Integer, List<Proposta>>();
		for (Iterator iterator = listaPropostas.iterator(); iterator.hasNext();) {
			Proposta proposta = (Proposta) iterator.next();

			if (mapaPropostasPorOrgao.containsKey(proposta.getOrgao().getId())) {
				List<Proposta> propostasPorOrgao = mapaPropostasPorOrgao
						.get(proposta.getOrgao().getId());
				propostasPorOrgao.add(proposta);
			} else {
				List<Proposta> propostasPorOrgao = new ArrayList<Proposta>();
				propostasPorOrgao.add(proposta);
				mapaPropostasPorOrgao.put(proposta.getOrgao().getId(),
						propostasPorOrgao);
			}
		}

		try {

			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet();

			HSSFRow row;

			// Estilos
			short formatoMonetario = wb.createDataFormat().getFormat(
					"R$ #,##0.00;[Red]R$ #,##0.00");

			HSSFCellStyle estiloMonetario = wb.createCellStyle();
			estiloMonetario.setDataFormat(formatoMonetario);

			HSSFCellStyle estiloCantoEsquerdo = wb.createCellStyle();

			HSSFFont fontBold = wb.createFont();
			fontBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Adicionando bordas para o CantoEsquerdo
			estiloCantoEsquerdo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoEsquerdo.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloCantoEsquerdo.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setTopBorderColor(HSSFColor.BLACK.index);
			estiloCantoEsquerdo.setFont(fontBold);

			HSSFCellStyle estiloMeio = wb.createCellStyle();

			// Adicionando bordas para o Meio
			estiloMeio.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloMeio.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setTopBorderColor(HSSFColor.BLACK.index);

			HSSFCellStyle estiloCantoDireito = wb.createCellStyle();

			// Adicionando bordas para o CantoDireito
			estiloCantoDireito.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setTopBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setRightBorderColor(HSSFColor.GREEN.index);
			estiloCantoDireito.setDataFormat(formatoMonetario);
			estiloCantoDireito.setFont(fontBold);

			HSSFCellStyle estiloFullBorder = wb.createCellStyle();

			// Adicionando bordas para o CantoEsquerdo
			estiloFullBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloFullBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setTopBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setRightBorderColor(HSSFColor.GREEN.index);
			estiloFullBorder.setFont(fontBold);

			// Cabe�alho
			row = sheet.createRow(0);

			HSSFFont fontCabecalho = wb.createFont();
			fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFRichTextString richTextOrgao = new HSSFRichTextString("Orgao");
			richTextOrgao.applyFont(fontCabecalho);
			HSSFRichTextString richTextUfOrgao = new HSSFRichTextString(
					"UF_Orgao");
			richTextUfOrgao.applyFont(fontCabecalho);
			HSSFRichTextString richTextSeguradoPrincipal = new HSSFRichTextString(
					"Segurado_Principal");
			richTextSeguradoPrincipal.applyFont(fontCabecalho);
			HSSFRichTextString richTextDataNascimento = new HSSFRichTextString(
					"Data_Nasc_Func");
			richTextDataNascimento.applyFont(fontCabecalho);
			HSSFRichTextString richTextMatricula = new HSSFRichTextString(
					"Matricula");
			richTextMatricula.applyFont(fontCabecalho);
			HSSFRichTextString richTextRg = new HSSFRichTextString("RG");
			richTextRg.applyFont(fontCabecalho);
			HSSFRichTextString richTextCPFFuncionario = new HSSFRichTextString(
					"CPF_Funcionario");
			richTextCPFFuncionario.applyFont(fontCabecalho);
			HSSFRichTextString richTextConjuge = new HSSFRichTextString(
					"Conjuge");
			richTextConjuge.applyFont(fontCabecalho);
			HSSFRichTextString richTextDataNascimentoConjuge = new HSSFRichTextString(
					"Data_Nasc_Conj");
			richTextDataNascimentoConjuge.applyFont(fontCabecalho);
			HSSFRichTextString richTextCPFConjuge = new HSSFRichTextString(
					"CPF_Conj");
			richTextCPFConjuge.applyFont(fontCabecalho);
			HSSFRichTextString richTextRGConjuge = new HSSFRichTextString(
					"RG_Conj");
			richTextRGConjuge.applyFont(fontCabecalho);
			HSSFRichTextString richTextCapitalFuncionario = new HSSFRichTextString(
					"Capital_Func");
			richTextCapitalFuncionario.applyFont(fontCabecalho);
			HSSFRichTextString richTextCapitalConjuge = new HSSFRichTextString(
					"Capital_Conjuge");
			richTextCapitalConjuge.applyFont(fontCabecalho);
			HSSFRichTextString richTextValorFuncionario = new HSSFRichTextString(
					"Valor_Funcionario");
			richTextValorFuncionario.applyFont(fontCabecalho);
			HSSFRichTextString richTextValorConjuge = new HSSFRichTextString(
					"Valor_Conjuge");
			richTextValorConjuge.applyFont(fontCabecalho);
			HSSFRichTextString richTextValorTotal = new HSSFRichTextString(
					"Valor_Total");
			richTextValorTotal.applyFont(fontCabecalho);

			row.createCell(0).setCellValue(richTextOrgao);
			row.createCell(1).setCellValue(richTextUfOrgao);
			row.createCell(2).setCellValue(richTextSeguradoPrincipal);
			row.createCell(3).setCellValue(richTextDataNascimento);
			row.createCell(4).setCellValue(richTextMatricula);
			row.createCell(5).setCellValue(richTextRg);
			row.createCell(6).setCellValue(richTextCPFFuncionario);
			row.createCell(7).setCellValue(richTextConjuge);
			row.createCell(8).setCellValue(richTextDataNascimentoConjuge);
			row.createCell(9).setCellValue(richTextCPFConjuge);
			row.createCell(10).setCellValue(richTextRGConjuge);
			row.createCell(11).setCellValue(richTextCapitalFuncionario);
			row.createCell(12).setCellValue(richTextCapitalConjuge);
			row.createCell(13).setCellValue(richTextValorFuncionario);
			row.createCell(14).setCellValue(richTextValorConjuge);
			row.createCell(15).setCellValue(richTextValorTotal);

			// Fim cabe�alho
			int inicio = 1;
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			DecimalFormat decimalFormat = new DecimalFormat();
			decimalFormat.setMaximumFractionDigits(2);
			decimalFormat.setMinimumFractionDigits(2);
			decimalFormat.setGroupingUsed(true);
			decimalFormat.setGroupingSize(3);
			for (Iterator iterator = mapaPropostasPorOrgao.values().iterator(); iterator
					.hasNext();) {
				List<Proposta> listaPropostasPorOrgao = (List<Proposta>) iterator
						.next();

				// Em ordem crescente
				Collections.sort(listaPropostasPorOrgao, new Comparator() {
					public int compare(Object o1, Object o2) {
						Proposta p1 = (Proposta) o1;
						Proposta p2 = (Proposta) o2;
						return p1.getNomeSegurado().compareToIgnoreCase(
								p2.getNomeSegurado());
					}
				});

				double valorTotal = 0.0;
				double valorProlaboreTotal = 0.0;

				for (int i = 0; i < listaPropostasPorOrgao.size(); i++) {

					Proposta proposta = listaPropostasPorOrgao.get(i);

					// Soma o valor total

					// Para propostas do modelo de Limite de Idade
					if (proposta.getModeloProposta() != null
							&& proposta.getModeloProposta().getId() != null
							&& proposta
									.getModeloProposta()
									.getId()
									.equals(Constantes.MODELO_PROPOSTA_VALOR_FIXO)) {

						if (proposta.getCapitalSeguradoLimiteIdade() != null) {

							if (proposta.getTipoProposta() != null
									&& proposta.getTipoProposta().getId() != null
									//&& proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_50)
									&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
									&& proposta.getCapitalSeguradoLimiteIdade()
											.getConjVLMorteNatural() != null) {

								proposta.setCapitalSegurado(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLMorteNatural());
								proposta.setCapitalConjuge(proposta
										.getCapitalSeguradoLimiteIdade()
										.getConjVLMorteNatural());
								proposta.setPremioConjuge(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLCasado50()
										- proposta
												.getCapitalSeguradoLimiteIdade()
												.getFuncVLIndividual());
								proposta.setPremioSegurado(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLIndividual());
								proposta.setTotalPremios(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLCasado50());
							} else if (proposta.getTipoProposta() != null
									&& proposta.getTipoProposta().getId() != null
									//&& proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_100)
									&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
									&& proposta.getCapitalSeguradoLimiteIdade()
											.getConjVLMorteNatural() != null) {
								proposta.setCapitalSegurado(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLMorteNatural());
								proposta.setCapitalConjuge(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLMorteNatural());
								proposta.setPremioConjuge(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLCasado100()
										- proposta
												.getCapitalSeguradoLimiteIdade()
												.getFuncVLIndividual());
								proposta.setPremioSegurado(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLIndividual());
								proposta.setTotalPremios(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLCasado100());
							} else if (proposta.getTipoProposta() != null
									&& proposta.getTipoProposta().getId() != null
									//&& proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_VALOR_FIXO_INDIVIDUAL)
									&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
									&& proposta.getCapitalSeguradoLimiteIdade()
											.getConjVLMorteNatural() != null) {

								proposta.setCapitalSegurado(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLMorteNatural());
								proposta.setCapitalConjuge(null);
								proposta.setPremioConjuge(null);
								proposta.setPremioSegurado(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLIndividual());
								proposta.setTotalPremios(proposta
										.getCapitalSeguradoLimiteIdade()
										.getFuncVLIndividual());

							} else {
								proposta.setCapitalSegurado(null);
								proposta.setCapitalConjuge(null);
								proposta.setPremioConjuge(null);
								proposta.setPremioSegurado(null);
								proposta.setTotalPremios(null);
							}
						}

					}

					// Para propostas do modelo de Faixa Etaria
					if (proposta.getModeloProposta() != null
							&& proposta.getModeloProposta().getId() != null
							&& proposta
									.getModeloProposta()
									.getId()
									.equals(Constantes.MODELO_PROPOSTA_FAIXA_ETARIA)) {

						if (proposta.getDetalheCapitalFaixaEtaria() != null) {

							if (proposta.getTipoProposta() != null
									&& proposta.getTipoProposta().getId() != null
									//&& proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_CASADO)
									&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO
									&& proposta.getDetalheCapitalFaixaEtaria()
											.getCapitalSegurado() != null) {
								proposta.setCapitalSegurado(proposta
										.getDetalheCapitalFaixaEtaria()
										.getCapitalSegurado());
								proposta.setCapitalConjuge(proposta
										.getDetalheCapitalFaixaEtaria()
										.getCapitalSegurado());
								proposta.setPremioConjuge(proposta
										.getDetalheCapitalFaixaEtaria()
										.getSomaTitularConjuge()
										- proposta
												.getDetalheCapitalFaixaEtaria()
												.getTitular());
								proposta.setPremioSegurado(proposta
										.getDetalheCapitalFaixaEtaria()
										.getTitular());
								proposta.setTotalPremios(proposta
										.getDetalheCapitalFaixaEtaria()
										.getSomaTitularConjuge());
							} else if (proposta.getTipoProposta() != null
									&& proposta.getTipoProposta().getId() != null
									//&& proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_INDIVIDUAL)
									&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
									&& proposta.getDetalheCapitalFaixaEtaria()
											.getCapitalSegurado() != null) {
								proposta.setCapitalSegurado(proposta
										.getDetalheCapitalFaixaEtaria()
										.getCapitalSegurado());
								proposta.setCapitalConjuge(null);
								proposta.setPremioConjuge(null);
								proposta.setPremioSegurado(proposta
										.getDetalheCapitalFaixaEtaria()
										.getTitular());
								proposta.setTotalPremios(proposta
										.getDetalheCapitalFaixaEtaria()
										.getTitular());

							} else {
								proposta.setCapitalSegurado(null);
								proposta.setCapitalConjuge(null);
								proposta.setPremioConjuge(null);
								proposta.setPremioSegurado(null);
								proposta.setTotalPremios(null);

							}
						}

					}

					if (proposta.getTotalPremios() != null) {
						valorTotal += proposta.getTotalPremios();
					}
					valorProlaboreTotal += proposta.getOrgao()
							.getValorProlabore();

					row = sheet.createRow(inicio);
					row.createCell(0).setCellValue(
							new HSSFRichTextString(proposta.getOrgao()
									.getNomeOrgao()));
					row.createCell(1).setCellValue(
							new HSSFRichTextString(proposta.getOrgao()
									.getCidade().getCod_estado()
									.getSgl_estado()));
					row.createCell(2).setCellValue(
							new HSSFRichTextString(proposta.getNomeSegurado()));
					row.createCell(3).setCellValue(
							new HSSFRichTextString(df.format(proposta
									.getDataNascimentoSegurado())));
					row.createCell(4).setCellValue(
							new HSSFRichTextString(proposta
									.getMatriculaSegurado()));
					row.createCell(5).setCellValue(
							new HSSFRichTextString(proposta.getRgSegurado()));
					row.createCell(6).setCellValue(
							new HSSFRichTextString(proposta.getCpfSegurado()));
					row.createCell(7).setCellValue(
							new HSSFRichTextString(proposta.getNomeConjuge()));
					if (proposta.getDataNascimentoConjuge() != null) {
						row.createCell(8).setCellValue(
								new HSSFRichTextString(df.format(proposta
										.getDataNascimentoConjuge())));
					} else {
						row.createCell(8).setCellValue(
								new HSSFRichTextString(""));
					}
					row.createCell(9).setCellValue(
							new HSSFRichTextString(proposta.getCpfConjuge()));
					row.createCell(10).setCellValue(
							new HSSFRichTextString(proposta.getRgConjuge()));
					if (proposta.getCapitalSegurado() != null) {
						row.createCell(11).setCellValue(
								proposta.getCapitalSegurado());
					} else {
						row.createCell(11).setCellValue(0.0);
					}
					row.getCell(11).setCellStyle(estiloMonetario);
					if (proposta.getCapitalConjuge() != null) {
						row.createCell(12).setCellValue(
								proposta.getCapitalConjuge());
					} else {
						row.createCell(12).setCellValue(0.0);
					}
					row.getCell(12).setCellStyle(estiloMonetario);

					if (proposta.getPremioSegurado() != null) {
						row.createCell(13).setCellValue(
								proposta.getPremioSegurado());
					} else {
						row.createCell(13).setCellValue(0.0);
					}
					row.getCell(13).setCellStyle(estiloMonetario);
					if (proposta.getPremioConjuge() != null) {
						row.createCell(14).setCellValue(
								proposta.getPremioConjuge());
					} else {
						row.createCell(14).setCellValue(0.0);
					}
					row.getCell(14).setCellStyle(estiloMonetario);
					if (proposta.getTotalPremios() != null) {
						row.createCell(15).setCellValue(
								proposta.getTotalPremios());
					} else {
						row.createCell(15).setCellValue(0.0);
					}
					row.getCell(15).setCellStyle(estiloMonetario);
					inicio++;

				}

				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				HSSFRichTextString richTextTotalDescontado = new HSSFRichTextString(
						"TOTAL DESCONTADO");

				row.createCell(0).setCellValue(richTextTotalDescontado);
				row.getCell(0).setCellStyle(estiloCantoEsquerdo);

				row.createCell(15).setCellValue(valorTotal);
				row.getCell(15).setCellStyle(estiloCantoDireito);

				for (int j = 1; j < 15; j++) {
					row.createCell(j);
					row.getCell(j).setCellStyle(estiloMeio);
				}

				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				HSSFRichTextString richTextProlabore = new HSSFRichTextString(
						"PR�-LABORE");
				row.createCell(0).setCellValue(richTextProlabore);
				row.getCell(0).setCellStyle(estiloCantoEsquerdo);
				row.createCell(15).setCellValue(valorProlaboreTotal);
				row.getCell(15).setCellStyle(estiloCantoDireito);

				for (int j = 1; j < 15; j++) {
					row.createCell(j);
					row.getCell(j).setCellStyle(estiloMeio);
				}

				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				HSSFRichTextString richTextValorRepasse = new HSSFRichTextString(
						"VALOR REPASSE");
				row.createCell(0).setCellValue(richTextValorRepasse);
				row.getCell(0).setCellStyle(estiloCantoEsquerdo);
				row.createCell(15).setCellValue(
						valorTotal - valorProlaboreTotal);
				row.getCell(15).setCellStyle(estiloCantoDireito);
				for (int j = 1; j < 15; j++) {
					row.createCell(j);
					row.getCell(j).setCellStyle(estiloMeio);
				}

				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				HSSFRichTextString richTextValorTotalVidas = new HSSFRichTextString(
						listaPropostasPorOrgao.size() + " VIDAS");
				row.createCell(0).setCellValue(richTextValorTotalVidas);
				row.getCell(0).setCellStyle(estiloFullBorder);
				inicio++;
				inicio++;

			}

			HSSFRow header = sheet.getRow(0);
			for (short column = 0; column < header.getLastCellNum(); column++) {
				sheet.autoSizeColumn(column);
			}

			HttpServletResponse res = (HttpServletResponse) FacesContext
					.getCurrentInstance().getExternalContext().getResponse();
			res.setContentType("application/vnd.ms-excel");
			res.setHeader("Content-disposition",
					"attachment; filename=ExportacaoMensal.xls");

			try {
				ServletOutputStream out = res.getOutputStream();
				wb.write(out);
				out.flush();
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			FacesContext faces = FacesContext.getCurrentInstance();
			faces.responseComplete();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "export";

	}

	// Getters and setters
	public List<Integer> getListaOrgaosSelecionados() {
		return listaOrgaosSelecionados;
	}

	public void setListaOrgaosSelecionados(List<Integer> listaOrgaosSelecionados) {
		this.listaOrgaosSelecionados = listaOrgaosSelecionados;
	}

	public List<SelectItem> getOrgaosSelectItens() {
		return orgaosSelectItens;
	}

	public void setOrgaosSelectItens(List<SelectItem> orgaosSelectItens) {
		this.orgaosSelectItens = orgaosSelectItens;
	}

	public long getTimeStamp() {
		return System.currentTimeMillis();
	}

	public ArrayList<File> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<File> files) {
		this.files = files;
	}

	public int getUploadsAvailable() {
		return uploadsAvailable;
	}

	public void setUploadsAvailable(int uploadsAvailable) {
		this.uploadsAvailable = uploadsAvailable;
	}

	public Integer getRadioSelected() {
		return radioSelected;
	}

	public void setRadioSelected(Integer radioSelected) {
		this.radioSelected = radioSelected;
	}

	public static void main(String[] args) {
		String cpf =  "200.0.0.0-20";
		cpf = cpf.replaceAll("\\.", "").replaceAll("-", "").replaceAll("/", "").replaceAll("_", "");
		System.out.println(cpf);
	}
	// fim getters and setters
	// ===========================================================================================

}
