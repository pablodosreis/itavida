package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.ControleComercialDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.entidades.ControleComercial;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class ControleComercialHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private ControleComercial controleComercial;
	private List<ControleComercial> resultadoPesquisa;
	
	// Getters and Setters
	
	
	
	public ControleComercial getControleComercial() {
		return controleComercial;
	}


	public void setControleComercial(ControleComercial controleComercial) {
		this.controleComercial = controleComercial;
	}
	
	
	public List<ControleComercial> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<ControleComercial> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	// Fim Getters and Setters
	




	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public ControleComercialHandler(){
		
		String fluxo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fluxo");
		if( fluxo.equals( "incluir" ) ){
			preparaIncluir();
		}else if( fluxo.equals( "consulta" ) ){
			cancelar( null );
		}
		
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaControleComercial(ActionEvent e){
		Session session = HibernateUtil.currentSession();
		ControleComercialDAO dao = new ControleComercialDAO( session );
		
		ControleComercial controleComercialFiltro = getControleComercial();
		
		List<ControleComercial> lista = dao.buscaPorFiltro( controleComercialFiltro  );
		setResultadoPesquisa(lista);
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public String resultadoRelatorioControleComercial( ){
		Session session = HibernateUtil.currentSession();
		ControleComercialDAO dao = new ControleComercialDAO( session );
		
		ControleComercial controleComercialFiltro = getControleComercial();
		
		List<ControleComercial> lista = dao.buscaPorFiltro( controleComercialFiltro  );
		setResultadoPesquisa(lista);
		
		return "resultado";
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) 
				&& validaPreenchimentoObrigatorioListaSelecao(getControleComercial().getOrgao().getId(), "Org�o") 
				&& validaPreenchimentoObrigatorioListaSelecao(getControleComercial().getFuncionario().getId(), "Agenciador")){
		
			Dao<ControleComercial, Integer> dao = new Dao<ControleComercial, Integer>(session, ControleComercial.class);
			ControleComercial controleComercialGravar = getControleComercial();
			
			dao.merge( controleComercialGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtOrgao", "�rg�o");
		mapaCampos.put("txtNumeroFuncionarios", "N�mero de Funcion�rios");
		mapaCampos.put("txtAgenciador", "Agenciador");
		mapaCampos.put("txtCustoViagem", "Custo de Viagem");
		mapaCampos.put("txtNumeroProducao", "Produ��o da Viagem");
		mapaCampos.put("dtDataVisitaChegada", "Data da Visita Chegada");
		mapaCampos.put("dtDataVisitaSaida", "Data da Visita Sa�da");		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarControleComercial( ){
		
		resultadoPesquisa = null;
		return "incluirControleComercial";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirControleComercial(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirControleComercial");
		ControleComercial controleComercialDeletada  = ( ControleComercial ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<ControleComercial, Integer> dao = new Dao<ControleComercial, Integer>(session, ControleComercial.class);
		dao.delete(controleComercialDeletada);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaControleComercial(e);
	}	
	
	public void obterQuantidadeVidasAtivas(ActionEvent e) {
		//Obt�m a quantidade de vidas ativas
		Session session = HibernateUtil.currentSession();
		OrgaoDAO orgaoDao = new OrgaoDAO(session);	
		Integer numVidas = orgaoDao.obterQuantidadeVidasAtivas( getControleComercial().getOrgao().getId() );
		getControleComercial().setNumeroVidasAtivas( numVidas );
	}

	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setControleComercial( new ControleComercial() );
		getControleComercial().setOrgao(new Orgao());
		getControleComercial().setFuncionario( new Funcionario() );
		this.resultadoPesquisa = null;
	}
	
	public String cancelarInclusao( ){
		cancelar(null);
		return "cancelarInclusao";
	}
	
	// Fluxos de INCLUS�O / EXCLUS�O / ALTERA��O
	
	public String preparaIncluir( ){
		cancelar(null);
		return "incluirControleComercial";
	}

	public Orgao getLimparOrgao( ){
		return  new Orgao() ;
	}

	public Funcionario getLimparFuncionario( ){
		return  new Funcionario() ;
	}

	
}
