package br.com.itavida.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import br.com.itavida.dao.CompetenciaDAO;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.Competencia;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.exceptions.ImportacaoExcelException;
import br.com.itavida.util.File;
import br.com.itavida.util.HibernateUtil;

public class ImportarPagamentoMensalHandler extends MainHandler {
	
	private String periodoImportacao;
	
	private static Logger logger = Logger.getLogger(ImportarPagamentoMensalHandler.class.toString());

	private ArrayList<File> files = new ArrayList<File>();
	private int uploadsAvailable = 1;

	private static final int QUANTIDADE_COLUNAS = 8;
	private static final int INICIO_DOS_DADOS = 1;
	
	private PropostaDAO propostaDAO;
	
	private CompetenciaDAO competenciaDao = new CompetenciaDAO();
	
	private Map<String,Proposta> cachePropostas;
	
	/**
	 * Construtor.
	 */
	public ImportarPagamentoMensalHandler() {
		
	}



	public int getSize() {
		if (getFiles().size() > 0) {
			return getFiles().size();
		} else {
			return 0;
		}
	}

	public void paint(OutputStream stream, Object object) throws IOException {
		stream.write(getFiles().get((Integer) object).getData());
	}

	public void listener(UploadEvent event) throws Exception {
		UploadItem item = event.getUploadItem();
		File file = new File();
		file.setLength(item.getData().length);
		file.setName(item.getFileName());
		file.setData(item.getData());
		files.add(file);
		uploadsAvailable = 0;
	}

	public String clearUploadData() {
		files.clear();
		uploadsAvailable = 1;
		return null;
	}
	
	public void limparDados(ActionEvent e) {
		clearUploadData();
	}
	
	public Proposta getProposta(String cpfParametro) {
		String cpf = cpfParametro.trim();
		cpf = cpf.replaceAll("\\.", "").replaceAll("-", "").replaceAll("/", "").replaceAll("_", "");
		cpf = StringUtils.leftPad(cpf, 11, '0');
		Proposta proposta = cachePropostas.get(cpf);
		return proposta;
	}
	
	public PropostaDAO getDAO() {
		Session session = HibernateUtil.currentSession();
		propostaDAO = new PropostaDAO(session);
		return propostaDAO;
	}
	
	public void inicializarCachePropostas() {
		cachePropostas = new HashMap<String, Proposta>();
		List<Proposta> propostasNaoCanceladas = getDAO().obterTodasPropostasNaoCanceladas();
		propostaDAO.populaCapital(propostasNaoCanceladas);
		for(Proposta p : propostasNaoCanceladas) {
			String cpf = p.getCpfSegurado().trim();
			cpf = cpf.replaceAll("\\.", "");
			cpf = cpf.replaceAll("-", "");
			cpf = StringUtils.leftPad(cpf, 11, '0');
			if(!cpf.isEmpty()) {
				cachePropostas.put(cpf, p);
			}
		}
	}

	public String importReport() {
		try {
			return importar();
		}  catch (ImportacaoExcelException e) {
			logger.log(Level.WARNING,"Erro ao importar os dados",e);
			addErrorMessage(e.getMessage());
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Erro ao importar os dados",e);
			addErrorMessage("Ocorreu um erro ao importar o arquivo, verifique se o arquivo � v�lido para esta importa��o.");
		}
		return null;
	}

	public String importar() throws ImportacaoExcelException {
		long init = System.currentTimeMillis();
		Double mes = null;
		Double ano = null;
		if(periodoImportacao == null){
			throw new ImportacaoExcelException(
					"Informe um per�odo para a importa��o (formato MM/AAAA)");
		}else{
			try{
				mes = Double.parseDouble(periodoImportacao.split("/")[0]);
				ano = Double.parseDouble(periodoImportacao.split("/")[1]);
			}catch(NumberFormatException ne){
				throw new ImportacaoExcelException(
						"Informe um per�odo para a importa��o (formato MM/AAAA).");
			}
			if(mes < 1 || mes > 12){
				throw new ImportacaoExcelException(
						"O m�s deve ser um n�mero entre 1 e 12.");
			}
		}
		if (getSize() == 0) {
			throw new ImportacaoExcelException(
					"Selecione algum arquivo para realizar a Importa��o dos Novos Valores.");
		}
		

		inicializarCachePropostas();

		List<String> erros = new ArrayList<String>();
		List<Proposta> bufferPropostas = new ArrayList<Proposta>();
		List<Competencia> bufferCompetencias = new ArrayList<Competencia>();

		File file = files.get(0);
		ByteArrayInputStream excelFIS = null;
		excelFIS = new ByteArrayInputStream(file.getData());

		HSSFWorkbook excelWB = null;

		try {
			excelWB = new HSSFWorkbook(excelFIS);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Erro ao importar os dados", e);
			throw new ImportacaoExcelException(
					"O formato desta planilha � incorreto. Favor verificar a planilha selecionada.");
		}

		// Passa pelas sheets...
		for (int indexSheet = 0; indexSheet < excelWB.getNumberOfSheets(); indexSheet++) {
			
			Session session = HibernateUtil.currentSession();
			PropostaDAO propostaDAO = getDAO();

			// Empresa...
			EmpresaDAO empresaoDAO = new EmpresaDAO(session);
			String sheetName = excelWB
					.getSheetName(indexSheet);
			String sheetLimpa = sheetName.replaceAll("-", "");
			sheetLimpa = sheetLimpa.replaceAll("\\.", "");
			Empresa empresa = empresaoDAO.buscaUnicaPelaApolice(sheetLimpa);
			
			if(empresa == null) {
				erros.add(String.format("O nome da aba '%s' n�o representa uma ap�lice v�lida.", sheetName));
				continue;
			}

			HSSFSheet sheet = excelWB.getSheetAt(indexSheet);
			int rows = sheet.getPhysicalNumberOfRows();

			// verifica o n�mero de colunas
			if (sheet.getRow(INICIO_DOS_DADOS).getPhysicalNumberOfCells() != QUANTIDADE_COLUNAS) {
				throw new ImportacaoExcelException(
						"O formato desta planilha � incorreto. Favor verificar a planilha selecionada.");
			}
			
			DecimalFormat decimalFormat = new DecimalFormat();  
            decimalFormat.setMaximumFractionDigits(2);  
            decimalFormat.setMinimumFractionDigits(2);

			// itera sobre as linhas de cada sheet
			for (int rowNumber = INICIO_DOS_DADOS; rowNumber < rows + 1; rowNumber++) {
				System.out.println(rowNumber);
				HSSFRow oneRow = sheet.getRow(rowNumber);
				// continua se a linha estiver vazia
				if (oneRow == null) {
					continue;
				}

				// Proposta...
				String cpf;
				HSSFCell cell = oneRow.getCell(0);
				Proposta proposta = null;
				if (cell != null
						&& (cell.getCellType() == HSSFCell.CELL_TYPE_STRING))  {
					// CPF proposta...
					cpf = cell.getRichStringCellValue().getString();
				} else {
					cpf = getValorDecimal(cell) != null ? new Long(getValorDecimal(cell).longValue()).toString() : "";
				}
				
				proposta = getProposta(cpf);
				if (proposta == null) {
					erros.add("Segurado n�o encontrada. CPF: " + cpf);
					continue;
				}
				
				if(!proposta.getEmpresa().equals(empresa)) {
					erros.add("Segurado n�o pertence a Ap�lice definida pela aba. CPF: " + cpf);
				}

				// verifica o nome
				cell = oneRow.getCell(1);
				String nome = getTexto(cell);
				if(!proposta.getNomeSegurado().equalsIgnoreCase(nome)) {
					erros.add(String.format("O nome %s n�o corresponde ao segurado de CPF %s.", nome, cpf));
				}

				// verifica a data de nascimento
				cell = oneRow.getCell(2);
				if (cell != null
						&& cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					// TODO verificar se a data de nacimento
				}

				cell = oneRow.getCell(3);
				Double premioTit = getValorDecimal(cell);
				if (premioTit == null) {
					erros.add(String.format("A linha %d possui uma um valor inv�lido na coluna Pr�mio do Titular", rowNumber));
				} 

				cell = oneRow.getCell(4);
				Double premioConj = getValorDecimal(cell);

				//linha 5 ignorada por ser o valor total (soma principal + segurado)
				
				cell = oneRow.getCell(6);
				Double mesCell = getValorDecimal(cell);
				
				cell = oneRow.getCell(7);
				Double anoCell = getValorDecimal(cell);
				
				if(mesCell == null || !mesCell.equals(mes) || anoCell == null || !anoCell.equals(ano)){
					erros.add(String.format("O per�odo informado na linha %d n�o corresponde ao informado na tela.", rowNumber));
				}

				
				if (erros.isEmpty()) {
					
					Competencia competencia = new Competencia();
					competencia.setProposta(proposta);
					competencia.setAno(ano.intValue());
					competencia.setMes(mes.intValue());
					competencia.setValorConjugue(premioConj);
					competencia.setValorSegurado(premioTit);
	
					bufferCompetencias.add(competencia);
					
				} 
			}
		}
		
		if (erros.isEmpty()) {
			competenciaDao.salvar(bufferCompetencias);
			addGlobalMessage("Importa��o realizada com sucesso!");
		} else {
			for (String erro : erros) {
				addErrorMessage(erro);
			}
		}

		/* Coloque aqui seu codigo que demora */
		long end = System.currentTimeMillis();
		long diff = end - init;
		System.out.println("Demorou " + (diff / 1000) + " segundos");
		return null;
	}
	
	public String getTexto(HSSFCell cell) {
		if(cell != null && cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
			return cell.getRichStringCellValue().getString();
		}
		return null;
	}
	
	public double converterDoubleDoisDecimais(double precoDouble) {  
	    DecimalFormat fmt = new DecimalFormat("0.00");        
	    String string = fmt.format(precoDouble);  
	    String[] part = string.split("[,]");  
	    String string2 = part[0]+"."+part[1];  
	        double preco = Double.parseDouble(string2);  
	    return preco;  
	}  
	
	public Double getValorDecimal(HSSFCell cell) {
		if(cell == null) {
			return null;
		}
		
		int cellType = cell.getCellType();
		
		
		try {
			if(cell != null && cellType == HSSFCell.CELL_TYPE_NUMERIC) {
				return converterDoubleDoisDecimais(cell.getNumericCellValue());
			}
			
			if(cell != null && cellType == HSSFCell.CELL_TYPE_STRING) {
				return converterDoubleDoisDecimais(Double.parseDouble(cell.getRichStringCellValue().getString()));
			}
		} catch (Exception e) {
			//faz nada
		}
		
		return null;
		
	}

	public long getTimeStamp() {
		return System.currentTimeMillis();
	}

	public ArrayList<File> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<File> files) {
		this.files = files;
	}

	public int getUploadsAvailable() {
		return uploadsAvailable;
	}

	public void setUploadsAvailable(int uploadsAvailable) {
		this.uploadsAvailable = uploadsAvailable;
	}

	public String getPeriodoImportacao() {
		return periodoImportacao;
	}

	public void setPeriodoImportacao(String periodoImportacao) {
		this.periodoImportacao = periodoImportacao;
	}

}
