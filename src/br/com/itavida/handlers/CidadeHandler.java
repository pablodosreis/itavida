package br.com.itavida.handlers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.CapitalSeguradoFaixaEtaria;
import br.com.itavida.entidades.CapitalSeguradoLimiteIdade;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Pagamento;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class CidadeHandler extends MainHandler implements Serializable{
	
	/**
	 * Default Serial ID
	 */
	private static final long serialVersionUID = 1L;
	
	private Cidade cidade = new Cidade();
	private HtmlSelectOneMenu estadoSelecionado;	
	private List<Cidade> resultadoPesquisa;
	
	public void processa( ActionEvent event ){
		
		List<Cidade> listaCidades = getCidades();
		Map<String, Cidade> mapaValores = new HashMap<String, Cidade>();
		for (Iterator iterator = listaCidades.iterator(); iterator.hasNext();) {
			Cidade cidade = (Cidade) iterator.next();
			String chave = cidade.getNom_cidade().toUpperCase().trim() + cidade.getCod_estado().getSgl_estado().toUpperCase();
			mapaValores.put( chave , cidade );
			String nomeCidade = cidade.getNom_cidade().toUpperCase().trim().replaceAll("�", "A");
			nomeCidade = nomeCidade.replaceAll("�", "E");
			nomeCidade = nomeCidade.replaceAll("�", "I");
			nomeCidade = nomeCidade.replaceAll("�", "O");
			nomeCidade = nomeCidade.replaceAll("�", "U");
			nomeCidade = nomeCidade.replaceAll("�", "A");
			nomeCidade = nomeCidade.replaceAll("�", "O");
			nomeCidade = nomeCidade.replaceAll("�", "A");
			nomeCidade = nomeCidade.replaceAll("�", "O");
			nomeCidade = nomeCidade.replaceAll("�", "E");
			nomeCidade = nomeCidade.replaceAll("�", "");
			nomeCidade = nomeCidade.replaceAll("`", "");
			nomeCidade = nomeCidade.replaceAll("'", "");
			nomeCidade = nomeCidade.replaceAll(" - ", "");
			nomeCidade = nomeCidade.replaceAll("-", "");
			String chaveSemAcento = nomeCidade + cidade.getCod_estado().getSgl_estado().toUpperCase();
			if( ! mapaValores.containsKey( chaveSemAcento ) ){
				mapaValores.put(chaveSemAcento, cidade);
			}
			String chaveSemCedilha = chaveSemAcento.replaceAll("�", "C");
			if( ! mapaValores.containsKey( chaveSemCedilha ) ){
				mapaValores.put( chaveSemCedilha , cidade );
			}
			nomeCidade = nomeCidade.replaceAll(" DE ", "");
			nomeCidade = nomeCidade.replaceAll(" DA ", "");
			nomeCidade = nomeCidade.replaceAll(" DAS ", "");
			nomeCidade = nomeCidade.replaceAll(" DO ", "");
			nomeCidade = nomeCidade.replaceAll(" DOS ", "");
			String chaveSemDeDa = nomeCidade + cidade.getCod_estado().getSgl_estado().toUpperCase();
			if( ! mapaValores.containsKey( chaveSemDeDa ) ){
				mapaValores.put( chaveSemDeDa , cidade );
			}
			
		}
		
		
		 try {
			    FileInputStream excelFIS = new FileInputStream(Constantes.SISVIDA_LOGS + "cidades.xls");
			    HSSFWorkbook excelWB = new HSSFWorkbook(excelFIS);
			    
			    for (int sheetNumber = 0; sheetNumber < excelWB.getNumberOfSheets(); sheetNumber++) {
			    	   HSSFSheet oneSheet = excelWB.getSheetAt(sheetNumber);

			    	// Now get the number of rows in the sheet
			    	   int rows = oneSheet.getPhysicalNumberOfRows();

			    	   // Middle Loop: Loop through rows in the sheet

			    	   for (int rowNumber = 0; rowNumber < rows; rowNumber++) {
			    	      HSSFRow oneRow = oneSheet.getRow(rowNumber);

			    	      // Skip empty (null) rows.
			    	      if (oneRow == null) {
			    	         continue;
			    	      }

			    	      // Inner Loop: Loop through each cell in the row
			    	      String chaveBusca = "";
			    	      
			    	      HSSFCell firstCell = oneRow.getCell( 0 );
			    	      HSSFCell secondCell = oneRow.getCell( 1 );
			    	      
			    	      if( firstCell != null && secondCell != null ){
				    	      //Monta a chave
				    	      chaveBusca = firstCell.getStringCellValue().toUpperCase().trim() + secondCell.getStringCellValue().toUpperCase().trim();

			    	      }
			    	      
			    	      Cidade cidade = mapaValores.get( chaveBusca );
			    	      if( cidade != null ){
			    	    	  System.out.println( cidade.getId() );
			    	      }else{
				    	      chaveBusca = chaveBusca.replaceAll("�", "E");
				    	      chaveBusca = chaveBusca.replaceAll("�", "I");
				    	      chaveBusca = chaveBusca.replaceAll("�", "O");
				    	      chaveBusca = chaveBusca.replaceAll("�", "U");
				    	      chaveBusca = chaveBusca.replaceAll("�", "A");
				    	      chaveBusca = chaveBusca.replaceAll("�", "O");
				    	      chaveBusca = chaveBusca.replaceAll("�", "A");
				    	      chaveBusca = chaveBusca.replaceAll("�", "O");
				    	      chaveBusca = chaveBusca.replaceAll("�", "E");
				    	      chaveBusca = chaveBusca.replaceAll("�", "");
				    	      chaveBusca = chaveBusca.replaceAll("`", "");
				    	      chaveBusca = chaveBusca.replaceAll("'", "");
				    	      chaveBusca = chaveBusca.replaceAll(" - ", "");
				    	      chaveBusca = chaveBusca.replaceAll("-", "");				    	      
				    	      chaveBusca = chaveBusca.replaceAll("�", "C");
				    	      chaveBusca = chaveBusca.replaceAll(" DE ", "");
				    	      chaveBusca = chaveBusca.replaceAll(" DA ", "");
				    	      chaveBusca = chaveBusca.replaceAll(" DO ", "");
				    	      chaveBusca = chaveBusca.replaceAll(" DAS ", "");
				    	      chaveBusca = chaveBusca.replaceAll(" DOS ", "");				    	      
				    	      cidade = mapaValores.get( chaveBusca );
				    	      if( cidade != null ){
				    	    	  System.out.println( cidade.getId() );
				    	      }else{
				    	    	  System.out.println( "" );
				    	      }
			    	      }
			    	      
			    	   }
			    	}
			    
			    excelFIS.close();
			 }
			 catch (IOException e) {
			    System.out.println("Input/Output Exception!");
			 }
	}

	public void processaCapital( ActionEvent event ){
		
		Session session = HibernateUtil.currentSession();
		Dao<CapitalSeguradoLimiteIdade, Long> dao = new Dao<CapitalSeguradoLimiteIdade, Long>(session, CapitalSeguradoLimiteIdade.class);
	
		List<CapitalSeguradoLimiteIdade> capitalLimite = dao.list();
		Map<String, CapitalSeguradoLimiteIdade> mapaCapitalLimiteChaveFull = new HashMap<String, CapitalSeguradoLimiteIdade>();
		for (CapitalSeguradoLimiteIdade capitalSeguradoLimiteIdade : capitalLimite) {
			String chave = capitalSeguradoLimiteIdade.getFuncVLMorteNatural().toString() +  "-" + capitalSeguradoLimiteIdade.getNomeTabela().getId().toString()+"-"+capitalSeguradoLimiteIdade.getFuncVLIndividual();
			mapaCapitalLimiteChaveFull.put( chave , capitalSeguradoLimiteIdade);
		}
		
		Map<String, CapitalSeguradoLimiteIdade> mapaCapitalLimiteChaveNormal = new HashMap<String, CapitalSeguradoLimiteIdade>();
		for (CapitalSeguradoLimiteIdade capitalSeguradoLimiteIdade : capitalLimite) {
			String chave = capitalSeguradoLimiteIdade.getFuncVLMorteNatural().toString() +  "-" + capitalSeguradoLimiteIdade.getNomeTabela().getId().toString();
			mapaCapitalLimiteChaveNormal.put( chave , capitalSeguradoLimiteIdade);
		}
		
		Map<String, CapitalSeguradoLimiteIdade> mapaCapitalLimiteChaveValorFunc = new HashMap<String, CapitalSeguradoLimiteIdade>();
		for (CapitalSeguradoLimiteIdade capitalSeguradoLimiteIdade : capitalLimite) {
			String chave = capitalSeguradoLimiteIdade.getFuncVLIndividual().toString();
			if( !mapaCapitalLimiteChaveValorFunc.containsKey(chave) ){
				mapaCapitalLimiteChaveValorFunc.put( chave , capitalSeguradoLimiteIdade);
			}
		}
		
		Dao<DetalheCapitalFaixaEtaria, Long> daoDetalheCapital = new Dao<DetalheCapitalFaixaEtaria, Long>(session, DetalheCapitalFaixaEtaria.class);
		
		List<DetalheCapitalFaixaEtaria> detalheCapitalFaixaFull = daoDetalheCapital.list();
		Map<String, DetalheCapitalFaixaEtaria> mapaDetalheCapitalFaixaChaveFull = new HashMap<String, DetalheCapitalFaixaEtaria>();
		for (DetalheCapitalFaixaEtaria detalheCapitalFaixa : detalheCapitalFaixaFull ) {
			String chave = detalheCapitalFaixa.getCapitalSegurado().toString() +  "-" + detalheCapitalFaixa.getCapitalSeguradoFaixaEtaria().getNomeTabela().getId().toString()+"-"+detalheCapitalFaixa.getTitular();
			mapaDetalheCapitalFaixaChaveFull.put( chave , detalheCapitalFaixa);
		}
		
		Map<String, DetalheCapitalFaixaEtaria> mapaDetalheCapitalFaixaChaveNormal = new HashMap<String, DetalheCapitalFaixaEtaria>();
		for (DetalheCapitalFaixaEtaria detalheCapitalFaixa : detalheCapitalFaixaFull ) {
			String chave = detalheCapitalFaixa.getCapitalSegurado().toString() +  "-" + detalheCapitalFaixa.getCapitalSeguradoFaixaEtaria().getNomeTabela().getId().toString();
			mapaDetalheCapitalFaixaChaveNormal.put( chave , detalheCapitalFaixa);
		}
		
		Map<String, DetalheCapitalFaixaEtaria> mapaDetalheCapitalFaixaChaveValorFunc = new HashMap<String, DetalheCapitalFaixaEtaria>();
		for (DetalheCapitalFaixaEtaria detalheCapitalFaixa : detalheCapitalFaixaFull ) {
			String chave = detalheCapitalFaixa.getTitular().toString();
			mapaDetalheCapitalFaixaChaveValorFunc.put( chave , detalheCapitalFaixa);
		}

		
		 try {
			    FileInputStream excelFIS = new FileInputStream(Constantes.SISVIDA_LOGS + "capital2.xls");
			    HSSFWorkbook excelWB = new HSSFWorkbook(excelFIS);
			    
			    for (int sheetNumber = 0; sheetNumber < excelWB.getNumberOfSheets(); sheetNumber++) {
			    	   HSSFSheet oneSheet = excelWB.getSheetAt(sheetNumber);

			    	// Now get the number of rows in the sheet
			    	   int rows = oneSheet.getPhysicalNumberOfRows();

			    	   // Middle Loop: Loop through rows in the sheet

			    	   for (int rowNumber = 0; rowNumber < rows; rowNumber++) {
			    	      HSSFRow oneRow = oneSheet.getRow(rowNumber);

			    	      // Skip empty (null) rows.
			    	      if (oneRow == null) {
			    	         continue;
			    	      }

			    	      // Inner Loop: Loop through each cell in the row
			    	      String chaveBusca = "";
			    	      HSSFCell primeira = oneRow.getCell( 0 );
			    	      HSSFCell terceira = oneRow.getCell( 2 );
			    	      HSSFCell quarta = oneRow.getCell( 3 );
			    	      HSSFCell quinta = oneRow.getCell( 4 );
			    	      HSSFCell setima = oneRow.getCell( 6 );
			    	      
			    	      
			    	      if( terceira != null && quarta != null && quinta != null  && setima != null ){			    	    	  
				    	      //Monta a chave
			    	    	  chaveBusca = terceira.getNumericCellValue() + "-" + new Double(quarta.getNumericCellValue()).intValue()+"-"+quinta.getNumericCellValue();
			    	      }
			    	      
			    	      CapitalSeguradoLimiteIdade capitalLimiteIdade = new CapitalSeguradoLimiteIdade();
			    	      DetalheCapitalFaixaEtaria detalheCapitalFaixa = new DetalheCapitalFaixaEtaria();
			    	      
			    	      if( primeira.getNumericCellValue() == 1 ){
			    	    	  capitalLimiteIdade = mapaCapitalLimiteChaveFull.get( chaveBusca );			    	    	  
			    	    	  if( capitalLimiteIdade != null && capitalLimiteIdade.getId() != null ){
			    	    		  System.out.println( capitalLimiteIdade.getId() + "\t" + "-" + "\t" + "");
			    	    	  }else{
			    	    		  //tenta de forma diferente, montando nova chave
					    	      if( terceira != null && quarta != null ){			    	    	  
						    	      //Monta a chave
					    	    	  chaveBusca = terceira.getNumericCellValue() + "-" + new Double(quarta.getNumericCellValue()).intValue();
					    	    	  capitalLimiteIdade = mapaCapitalLimiteChaveNormal.get( chaveBusca );	
					    	      }
					    	      if( capitalLimiteIdade != null && capitalLimiteIdade.getId() != null ){
					    	    	  System.out.println( capitalLimiteIdade.getId() + "\t" + "-" + "\t" + "");
					    	      }else{
					    	    	  //Tenta a �ltima vez
						    	      if(  quinta != null  ){			    	    	  
							    	      //Monta a chave
						    	    	  chaveBusca = String.valueOf(quinta.getNumericCellValue());
						    	    	  capitalLimiteIdade = mapaCapitalLimiteChaveValorFunc.get( chaveBusca );	
						    	      }
						    	      
						    	      if( capitalLimiteIdade != null && capitalLimiteIdade.getId() != null ){
						    	    	  System.out.println( capitalLimiteIdade.getId() + "\t" + "-" + "\t" + "");
						    	      }else{
						    	    	  System.out.println( "NULL" + "\t" + "-" + "\t" + "");
						    	      }
					    	      }
			    	    	  }
			    	      }else{
			    	    	  detalheCapitalFaixa = mapaDetalheCapitalFaixaChaveFull.get( chaveBusca );
			    	    	  if( detalheCapitalFaixa != null && detalheCapitalFaixa.getId() != null ){
			    	    		  System.out.println( "-" + "\t" + detalheCapitalFaixa.getId() + "\t" + detalheCapitalFaixa.getCapitalSeguradoFaixaEtaria().getId() );
			    	    	  }else{
			    	    		  
			    	    		  //tenta de forma diferente, montando nova chave
					    	      if( terceira != null && quarta != null ){			    	    	  
						    	      //Monta a chave
					    	    	  chaveBusca = terceira.getNumericCellValue() + "-" + new Double(quarta.getNumericCellValue()).intValue();
					    	    	  detalheCapitalFaixa = mapaDetalheCapitalFaixaChaveNormal.get( chaveBusca );
					    	      }
					    	      
				    	    	  if( detalheCapitalFaixa != null && detalheCapitalFaixa.getId() != null ){
				    	    		  System.out.println( "-" + "\t" + detalheCapitalFaixa.getId() + "\t" + detalheCapitalFaixa.getCapitalSeguradoFaixaEtaria().getId() );
				    	    	  }else{
				    	    		  //Tenta a �ltima vez
						    	      if(  quinta != null  ){			    	    	  
							    	      //Monta a chave
						    	    	  chaveBusca = String.valueOf(quinta.getNumericCellValue());
						    	    	  detalheCapitalFaixa = mapaDetalheCapitalFaixaChaveValorFunc.get( chaveBusca );	
						    	      }
					    	    	  if( detalheCapitalFaixa != null && detalheCapitalFaixa.getId() != null ){
					    	    		  System.out.println( "-" + "\t" + detalheCapitalFaixa.getId() + "\t" + detalheCapitalFaixa.getCapitalSeguradoFaixaEtaria().getId() );
					    	    	  }else{
					    	    		  System.out.println(  "-" + "\t" + "NULL" + "\t" + "");
					    	    	  }
						    	      
				    	    	  }
			    	    		  
			    	    	  }
			    	      }
			    	      
			    	      
			    	   }
			    	}
			    
			    excelFIS.close();
			 }
			 catch (IOException e) {
			    System.out.println("Input/Output Exception!");
			 }
	}
	
	public void migraCapitais( ActionEvent event ) throws IllegalAccessException, InvocationTargetException{
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO( session );
		
		//Busca propostas por �rg�o
		Proposta propostaFiltro = new Proposta();
		propostaFiltro.setOrgao( new Orgao() );
		propostaFiltro.getOrgao().setId( 911 );
		List<Proposta> propostas = propostaDAO.buscaPorFiltroConsulta( propostaFiltro, false );
		
		//Para cada proposta, adquire as tabelas de capitais que sofrer�o altera��o
		Map<Integer , Map<Integer,List<Proposta>> > nomeTabelaMap = new HashMap<Integer, Map<Integer,List<Proposta>> >();
		for (Proposta proposta : propostas) {
			if( proposta.getModeloProposta().getId() == 1 && proposta.getNomeTabela() != null && proposta.getNomeTabela().getId() != 0  ){
				if( nomeTabelaMap.containsKey( proposta.getNomeTabela().getId() ) ){
					Map<Integer,List<Proposta>> mapaCapitais = nomeTabelaMap.get( proposta.getNomeTabela().getId() );
					if( mapaCapitais.containsKey( proposta.getCapitalSeguradoLimiteIdade().getId() ) ){
						List<Proposta> listaPropostas = mapaCapitais.get( proposta.getCapitalSeguradoLimiteIdade().getId() );
						listaPropostas.add( proposta );
					}else{
						List<Proposta> listaPropostas = new ArrayList<Proposta>();
						listaPropostas.add( proposta );
						mapaCapitais.put( proposta.getCapitalSeguradoLimiteIdade().getId() , listaPropostas );
					}
					
				}else{
					Map<Integer,List<Proposta>> mapaCapitais = new HashMap<Integer, List<Proposta>>();
					List<Proposta> listaPropostas = new ArrayList<Proposta>();
					listaPropostas.add( proposta );
					mapaCapitais.put( proposta.getCapitalSeguradoLimiteIdade().getId() , listaPropostas );
					nomeTabelaMap.put( proposta.getNomeTabela().getId(), mapaCapitais );
				}
			}
		}
		
		Dao<CapitalSeguradoLimiteIdade, Integer> daoCapital = new Dao<CapitalSeguradoLimiteIdade, Integer>(session, CapitalSeguradoLimiteIdade.class);
		Dao<NomeTabela, Integer> daoNomeTabela = new Dao<NomeTabela, Integer>(session, NomeTabela.class);
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		
		Integer ultimoIdNomeTabela = daoCapital.retornaUltimoIdInserido( NomeTabela.class );
		
		for (Integer idNomeTabela: nomeTabelaMap.keySet() ) {
			NomeTabela nomeTabela = daoNomeTabela.get( idNomeTabela );
			
			//Cria o novo Nome de Tabela
			NomeTabela nomeTabelaCopy = new NomeTabela();
			BeanUtils.copyProperties( nomeTabelaCopy, nomeTabela );
			nomeTabelaCopy.setId( ultimoIdNomeTabela + 1);
			nomeTabelaCopy.setNomeTabela( nomeTabelaCopy.getNomeTabela() + " +2,90" );
			nomeTabelaCopy.setNomeTabelaReduzido( nomeTabelaCopy.getNomeTabelaReduzido() + " +2,90"  );
			daoNomeTabela.save( nomeTabelaCopy );
			
			Map<Integer,List<Proposta>> capitaisMap = nomeTabelaMap.get( idNomeTabela );
			
			for (Integer idCapital : capitaisMap.keySet() ) {
				
				CapitalSeguradoLimiteIdade capital = daoCapital.get( idCapital );
				//Cria a nova tabela vinculada ao novo nome
				CapitalSeguradoLimiteIdade capitalLimiteIdade = new CapitalSeguradoLimiteIdade();
				BeanUtils.copyProperties( capitalLimiteIdade, capital );
				
				capitalLimiteIdade.setNomeTabela( nomeTabelaCopy );
				capitalLimiteIdade.setFuncVLIndividual( capital.getFuncVLIndividual() + 2.90 );
				capitalLimiteIdade.setFuncVLCasado50( capital.getFuncVLCasado50() + 2.90 );
				capitalLimiteIdade.setFuncVLCasado100( capital.getFuncVLCasado100() + 2.90 );
				capitalLimiteIdade.setId( null );
				daoCapital.save( capitalLimiteIdade );
				
				List<Proposta> propostasCapitais = capitaisMap.get( idCapital );
				for (Proposta propostaCapital : propostasCapitais) {
					propostaCapital.setCapitalSeguradoLimiteIdade( capitalLimiteIdade );
					propostaCapital.setNomeTabela( nomeTabelaCopy );
					
					daoProposta.merge( propostaCapital );
				}
				
			}

			
		}
		
	}
	
	public void migraCapitaisFaixaEtaria( ActionEvent event ) throws IllegalAccessException, InvocationTargetException{
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO( session );
		
		//Busca propostas por �rg�o
		Proposta propostaFiltro = new Proposta();
		propostaFiltro.setOrgao( new Orgao() );
		propostaFiltro.getOrgao().setId( 911 );
		List<Proposta> propostas = propostaDAO.buscaPorFiltroConsulta( propostaFiltro, false);
		
		//Para cada proposta, adquire as tabelas de capitais que sofrer�o altera��o
		Map<Integer , Map<Integer,Map<Integer,List<Proposta>>> > nomeTabelaMap = new HashMap<Integer, Map<Integer,Map<Integer,List<Proposta>>> >();
		for (Proposta proposta : propostas) {
			
			if( proposta.getModeloProposta().getId() == 2 && proposta.getNomeTabela() != null && proposta.getNomeTabela().getId() != 0 && !proposta.getNomeTabela().getNomeTabela().contains( "T09" ) ){
				
				if( nomeTabelaMap.containsKey( proposta.getNomeTabela().getId() ) ){
					Map<Integer,Map<Integer,List<Proposta>>> mapaCapitais = nomeTabelaMap.get( proposta.getNomeTabela().getId() );
					
					if( mapaCapitais.containsKey( proposta.getCapitalSeguradoFaixaEtaria().getId() ) ){
						
						Map<Integer,List<Proposta>> mapaDetalheCapitais = mapaCapitais.get( proposta.getCapitalSeguradoFaixaEtaria().getId() );
						if( mapaDetalheCapitais.containsKey( proposta.getDetalheCapitalFaixaEtaria().getId() ) ){
							List<Proposta> listaPropostas = mapaDetalheCapitais.get( proposta.getDetalheCapitalFaixaEtaria().getId() );
							listaPropostas.add( proposta );
						}else{
							List<Proposta> listaPropostas = new ArrayList<Proposta>();
							listaPropostas.add( proposta );
							mapaDetalheCapitais.put( proposta.getDetalheCapitalFaixaEtaria().getId() , listaPropostas );
							mapaCapitais.put( proposta.getCapitalSeguradoFaixaEtaria().getId() , mapaDetalheCapitais );
						}
						
					}else{
						Map<Integer,List<Proposta>> mapaDetalheCapitais = new HashMap<Integer, List<Proposta>>();
						List<Proposta> listaPropostas = new ArrayList<Proposta>();
						listaPropostas.add( proposta );
						mapaDetalheCapitais.put(  proposta.getDetalheCapitalFaixaEtaria().getId() , listaPropostas );
						
						mapaCapitais.put( proposta.getCapitalSeguradoFaixaEtaria().getId() , mapaDetalheCapitais );
					}
					
				}else{
					
					Map<Integer,List<Proposta>> mapaDetalheCapitais = new HashMap<Integer, List<Proposta>>();
					List<Proposta> listaPropostas = new ArrayList<Proposta>();
					listaPropostas.add( proposta );
					mapaDetalheCapitais.put(  proposta.getDetalheCapitalFaixaEtaria().getId() , listaPropostas );
					
					Map<Integer,Map<Integer,List<Proposta>>> mapaCapitais = new HashMap<Integer, Map<Integer,List<Proposta>>>();
					mapaCapitais.put( proposta.getCapitalSeguradoFaixaEtaria().getId() , mapaDetalheCapitais );
					
					nomeTabelaMap.put( proposta.getNomeTabela().getId(), mapaCapitais );
				}
			}
		}
		
		Dao<CapitalSeguradoFaixaEtaria, Integer> daoCapital = new Dao<CapitalSeguradoFaixaEtaria, Integer>(session, CapitalSeguradoFaixaEtaria.class);
		Dao<DetalheCapitalFaixaEtaria, Integer> daoDetalheCapital = new Dao<DetalheCapitalFaixaEtaria, Integer>(session, DetalheCapitalFaixaEtaria.class);
		Dao<NomeTabela, Integer> daoNomeTabela = new Dao<NomeTabela, Integer>(session, NomeTabela.class);
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		
		Integer ultimoIdNomeTabela = daoCapital.retornaUltimoIdInserido( NomeTabela.class );
		
		for (Integer idNomeTabela: nomeTabelaMap.keySet() ) {
			NomeTabela nomeTabela = daoNomeTabela.get( idNomeTabela );
			
			//Cria o novo Nome de Tabela
			NomeTabela nomeTabelaCopy = new NomeTabela();
			BeanUtils.copyProperties( nomeTabelaCopy, nomeTabela );
			nomeTabelaCopy.setId( ultimoIdNomeTabela + 1);
			nomeTabelaCopy.setNomeTabela( nomeTabelaCopy.getNomeTabela() + " +2,90" );
			nomeTabelaCopy.setNomeTabelaReduzido( nomeTabelaCopy.getNomeTabelaReduzido() + " +2,90"  );
			daoNomeTabela.save( nomeTabelaCopy );
			
			Map<Integer,Map<Integer,List<Proposta>>> capitaisMap = nomeTabelaMap.get( idNomeTabela );
			
			for (Integer idCapital : capitaisMap.keySet() ) {
				
				CapitalSeguradoFaixaEtaria capital = daoCapital.get( idCapital );
				
				//Cria a nova tabela vinculada ao novo nome
				CapitalSeguradoFaixaEtaria capitalFaixaEtaria = new CapitalSeguradoFaixaEtaria();
				BeanUtils.copyProperties( capitalFaixaEtaria, capital );
				
				capitalFaixaEtaria.setNomeTabela( nomeTabelaCopy );
				capitalFaixaEtaria.setDetalhecapitalfaixaetaria_collection( null );
				daoCapital.save( capitalFaixaEtaria );
				
				Map<Integer,List<Proposta>> mapaDetalhes = capitaisMap.get( idCapital );
				List<DetalheCapitalFaixaEtaria> todosDetalhesDoCapital = new ArrayList<DetalheCapitalFaixaEtaria>();
				todosDetalhesDoCapital.addAll( capital.getDetalhecapitalfaixaetaria_collection() );
				//Verifica quais ainda n�o foram para incluir na tabela 
				
				for (Integer idDetalheCapital : mapaDetalhes.keySet() ) {
					
					//Cria a nova tabela vinculada ao novo nome
					DetalheCapitalFaixaEtaria detalheCapital = daoDetalheCapital.get( idDetalheCapital );
					
					for (int i = 0; i < todosDetalhesDoCapital.size(); i++) {
						DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria = todosDetalhesDoCapital.get(i);
						if( detalheCapitalFaixaEtaria.getId().equals( detalheCapital.getId()  ) ){
							todosDetalhesDoCapital.remove(  detalheCapitalFaixaEtaria );
							i=0;
						}
					}
						
					
					DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria = new DetalheCapitalFaixaEtaria();
					BeanUtils.copyProperties( detalheCapitalFaixaEtaria, detalheCapital );
					
					detalheCapitalFaixaEtaria.setCapitalSeguradoFaixaEtaria( capitalFaixaEtaria );
					detalheCapitalFaixaEtaria.setTitular( detalheCapitalFaixaEtaria.getTitular() + 2.90 );
					detalheCapitalFaixaEtaria.setSomaTitularConjuge( detalheCapitalFaixaEtaria.getSomaTitularConjuge() + 2.90 );
					
					daoDetalheCapital.save( detalheCapitalFaixaEtaria );
					
					List<Proposta> propostasCapitais = mapaDetalhes.get( idDetalheCapital );
					for (Proposta propostaCapital : propostasCapitais) {
						propostaCapital.setCapitalSeguradoFaixaEtaria( capitalFaixaEtaria );
						propostaCapital.setDetalheCapitalFaixaEtaria( detalheCapitalFaixaEtaria );
						propostaCapital.setNomeTabela( nomeTabelaCopy );
						
						daoProposta.merge( propostaCapital );
					}
					
				}
				
				//Agora salva os que n�o foram salvos anteriormente
				for (DetalheCapitalFaixaEtaria restanteDetalheFaixa : todosDetalhesDoCapital) {
					
					DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria = new DetalheCapitalFaixaEtaria();
					BeanUtils.copyProperties( detalheCapitalFaixaEtaria, restanteDetalheFaixa );
					
					detalheCapitalFaixaEtaria.setCapitalSeguradoFaixaEtaria( capitalFaixaEtaria );
					detalheCapitalFaixaEtaria.setTitular( detalheCapitalFaixaEtaria.getTitular() + 2.90 );
					detalheCapitalFaixaEtaria.setSomaTitularConjuge( detalheCapitalFaixaEtaria.getSomaTitularConjuge() + 2.90 );
					
					daoDetalheCapital.save( detalheCapitalFaixaEtaria );
				}
				
			}

			
		}
		
	}
	
	public void processaPagamento( ActionEvent event ){
		
		Session session = HibernateUtil.currentSession();
		Dao<Pagamento, Long> dao = new Dao<Pagamento, Long>(session, Pagamento.class);
		
		List<Pagamento> listaPagamento = dao.list();
		
		Map<String, Pagamento> mapaPagamento = new HashMap<String, Pagamento>();
		for (Iterator iterator = listaPagamento.iterator(); iterator.hasNext();) {
			Pagamento pagamento = (Pagamento) iterator.next();
			
			String chave = pagamento.getNumeroDocumento().toUpperCase().trim();
			mapaPagamento.put( chave , pagamento );
		}
		
		
		 try {
			    FileInputStream excelFIS = new FileInputStream(Constantes.SISVIDA_LOGS + "pagamentos.xls");
			    HSSFWorkbook excelWB = new HSSFWorkbook(excelFIS);
			    
			    for (int sheetNumber = 0; sheetNumber < excelWB.getNumberOfSheets(); sheetNumber++) {
			    	   HSSFSheet oneSheet = excelWB.getSheetAt(sheetNumber);

			    	// Now get the number of rows in the sheet
			    	   int rows = oneSheet.getPhysicalNumberOfRows();

			    	   // Middle Loop: Loop through rows in the sheet

			    	   for (int rowNumber = 0; rowNumber < rows; rowNumber++) {
			    	      HSSFRow oneRow = oneSheet.getRow(rowNumber);

			    	      // Skip empty (null) rows.
			    	      if (oneRow == null) {
			    	         continue;
			    	      }

			    	      // Inner Loop: Loop through each cell in the row
			    	      String chaveBusca = "";
			    	      
			    	      HSSFCell firstCell = oneRow.getCell( 0 );
			    	      			    	      
			    	      if( firstCell != null  ){
				    	      //Monta a chave
				    	      chaveBusca = firstCell.getStringCellValue().toUpperCase().trim();
			    	      }
			    	      
			    	      Pagamento pagamento = mapaPagamento.get( chaveBusca );
			    	      if( pagamento != null ){
			    	    	  System.out.println( pagamento.getId() );
			    	      }else{
			    	    	  System.out.println( "" );
			    	      }
			    	      
			    	   }
			    	}
			    
			    excelFIS.close();
			 }
			 catch (IOException e) {
			    System.out.println("Input/Output Exception!");
			 }
	}
	
	
	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public CidadeHandler(){
		cancelar( null );
	}
	
	
	/**
	 * Retorna todas as cidades
	 * @return
	 */
	public List<Cidade> getCidades() {
		Session session = HibernateUtil.currentSession();
		Dao<Cidade, Long> dao = new Dao<Cidade, Long>(session, Cidade.class);
	
		return dao.list();
		
	}
	
	/**
	 * Retorna todos os estados
	 * @return
	 */
	public List<SelectItem> getEstados(){
		Session session = HibernateUtil.currentSession();
		Dao<Estado, Long> dao = new Dao<Estado, Long>(session, Estado.class);
		List<Estado> lista = dao.list();
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		for (Estado estado : lista) {
			retorno.add( new SelectItem(new Integer(estado.getCod_estado()).toString(), estado.getSgl_estado()) );
		}
		
		return retorno;
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaCidadesPorNomeOuEstado(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		CidadeDAO dao = new CidadeDAO(session);	
		
		Integer estado = null;
		if( getEstadoSelecionado() != null && getEstadoSelecionado().getValue() != null){
			
			if ( !"".equals(getEstadoSelecionado().getValue().toString() ) ){
				estado = new Integer( getEstadoSelecionado().getValue().toString() );
			}
		}
		
		
		List<Cidade> lista = dao.buscaPeloNome( getCidade().getNom_cidade(), estado , 1000 );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<Cidade, Integer> dao = new Dao<Cidade, Integer>(session, Cidade.class);
			dao.merge( getCidade() );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("cmbEstados", "Estado (UF)");
		mapaCampos.put("txtNomeCidade", "Nome da Cidade");
		
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarCidade( ){
		
		//Recupera o item escolhido	
		estadoSelecionado = new HtmlSelectOneMenu();
		estadoSelecionado.setValue(getCidade().getCod_estado().getCod_estado() );
		resultadoPesquisa = null;
		
		return "incluirCidade";
	}
	
	/**
	 * Realiza a exclus�o de uma cidade
	 * @param e
	 */
	public void excluirCidade(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirCidade");
		Cidade cidadeDeletada = ( Cidade ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		CidadeDAO cidadeDao = new CidadeDAO(session);
		if( cidadeDao.findPorCidade( cidadeDeletada.getId() ) ){
			addErrorMessage("A Cidade: " + cidadeDeletada.getNom_cidade()+ " n�o pode ser exclu�da pois j� est� sendo utilizada em outros cadastros!");
		}else{
			Dao<Cidade, Integer> dao = new Dao<Cidade, Integer>(session, Cidade.class);		
			dao.delete(cidadeDeletada);
			// Dispara novamente a pesquisa para manter o v�nculo
			pesquisaCidadesPorNomeOuEstado(e);
		}

	}	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setCidade( new Cidade() );
		getCidade().setCod_estado( new Estado() );
		this.resultadoPesquisa = null;
		
	}
	

	// Getters and setters
	public List<Cidade> getResultadoPesquisa() {
		return resultadoPesquisa;
	}
	public void setResultadoPesquisa(List<Cidade> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public HtmlSelectOneMenu getEstadoSelecionado() {
		return estadoSelecionado;
	}

	public void setEstadoSelecionado(HtmlSelectOneMenu estadoSelecionado) {
		this.estadoSelecionado = estadoSelecionado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	
}
