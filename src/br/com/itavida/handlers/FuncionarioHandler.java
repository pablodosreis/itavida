package br.com.itavida.handlers;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.ValueHolder;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.FuncionarioDAO;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class FuncionarioHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 1L;
	private Funcionario funcionario;
	private List<Funcionario> resultadoPesquisa;
	private HtmlSelectOneMenu tipoFuncionarioSelecionado;
	private List<Cidade> listaCidades;
	private String idadeFuncionario;
	private Funcionario[] funcionariosSelecionados;
	

	
	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public FuncionarioHandler(){
		cancelar( null );
	}
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getTipoFuncionarioCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "T", "Agenciador Aut�nomo" ) );
		retorno.add( new SelectItem( "F", "Agenciador Funcion�rio" ) );
		retorno.add( new SelectItem( "A", "Funcion�rio Administrativo" ) );
		
		return retorno;
	}	

	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaFuncionarios(ActionEvent e){
		setVisibleMessage(false);
		Session session = HibernateUtil.currentSession();
		FuncionarioDAO dao = new FuncionarioDAO( session );
		
		Funcionario funcionarioFiltro = getFuncionario();
		
		Map<String, String> mapa = montaMapaTipoFuncionario();
		
		List<Funcionario> lista = dao.buscaPorFiltro( funcionarioFiltro  );
		
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			Funcionario funcionario = (Funcionario) iterator.next();
			funcionario.setTipoFuncionarioDescricao( mapa.get( funcionario.getTipoFuncionario() ) );
		}
		
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		setVisibleMessage(false);
		if( validarFormulario(e) && validaPreenchimentoObrigatorioListaSelecao(getFuncionario().getCidade().getId(), "Cidade") ){
		
			Dao<Funcionario, Integer> dao = new Dao<Funcionario, Integer>(session, Funcionario.class);
			Funcionario funcionarioGravar = getFuncionario();
			
			dao.merge( funcionarioGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNome", "Nome");
		mapaCampos.put("cmbTipoFuncionario", "Tipo de Funcion�rio");
		mapaCampos.put("txtCpf", "CPF");
		mapaCampos.put("txtNomeCidade", "Cidade");
		mapaCampos.put("dtDataAdmissao", "Data de Admiss�o");
		mapaCampos.put("dtDataNascimento", "Data de Nascimento");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		if(!retorno){
			return retorno;
		}else{
			//Recupera o CPF
			UIComponent link = e.getComponent();
			ValueHolder value = (ValueHolder) link.findComponent("txtCpf");
			String cpf = value.getValue().toString();
			retorno = validarCNPJCPF( cpf );
		}
			
		if(!retorno){
			return retorno;
		}else{
			//Verifica Porcentagem
			if( getFuncionario().getPercentualComissao() != null && getFuncionario().getPercentualComissao() > 100 ){
				addErrorMessage("O percentual de comiss�o n�o pode exceder 100%.");
				return false;
			}
		}
		
		return retorno;
	}
	

	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarFuncionario( ){
		
		resultadoPesquisa = null;
		return "incluirFuncionario";
	}
	
	/**
	 * Realiza a exclus�o de uma TipoOcorrencia
	 * @param e
	 */
	public void excluirFuncionario(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirFuncionario");
		Funcionario funcionarioDeletado  = ( Funcionario ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<Funcionario, Integer> dao = new Dao<Funcionario, Integer>(session, Funcionario.class);
		dao.delete(funcionarioDeletado);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaFuncionarios(e);
	}	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setVisibleMessage(false);
		setFuncionario( new Funcionario() );
		getFuncionario().setCidade(new Cidade());
		this.resultadoPesquisa = null;
		
	}

	public Cidade getLimparCidade(){
		return new Cidade();
	}
	
	public void calculaIdadeFuncionario(ActionEvent e){
		Date dataNascimento = getFuncionario().getDataNascimento();
		try{
		Calendar bd = new GregorianCalendar();
		Calendar cd = Calendar.getInstance();
		bd.setTime(dataNascimento);
		int day =0 , month = 0, ageYears, ageMonths, ageDays;
		
		month = bd.get( Calendar.MONTH );
		day = bd.get( Calendar.DAY_OF_MONTH );
		
	    ageYears = cd.get(Calendar.YEAR) - bd.get(Calendar.YEAR);
	    if(cd.before(new GregorianCalendar(cd.get(Calendar.YEAR), month, day))){
	      ageYears--;
	      ageMonths = (12 - (bd.get(Calendar.MONTH) + 1)) + (cd.get(Calendar.MONTH));
	      if(day > cd.get(Calendar.DAY_OF_MONTH)){
	        ageDays = ( 30 - day ) + cd.get(Calendar.DAY_OF_MONTH);
	      }
	      else if(day < cd.get(Calendar.DAY_OF_MONTH)){
	        ageDays = cd.get(Calendar.DAY_OF_MONTH) - day;
	        ageMonths++;
	      }
	      else{
	        ageDays = 0;
	      }
	    }
	    else if(cd.after(new GregorianCalendar(cd.get(Calendar.YEAR), month, day))){
	      ageMonths = (cd.get(Calendar.MONTH) - (bd.get(Calendar.MONTH)));
	      if(day > cd.get(Calendar.DAY_OF_MONTH))
	        ageDays = day - cd.get(Calendar.DAY_OF_MONTH) - day;
	      else if(day < cd.get(Calendar.DAY_OF_MONTH)){
	        ageDays = cd.get(Calendar.DAY_OF_MONTH) - day;
	      }
	      else
	        ageDays = 0;
	    }
	    else{
	      ageYears = cd.get(Calendar.YEAR) - bd.get(Calendar.YEAR);
	      ageMonths = 0;
	      ageDays = 0;
	    }
	    setIdadeFuncionario( ageYears + " anos, " + ageMonths + 	" meses e " + ageDays + " dias.");
		}catch( Exception exc ){
			setIdadeFuncionario("");
		}
	  
	}
	
	
	public String generateMalaDireta(  ){
		
        try {  

        	InputStream excelFIS = this.getClass().getResourceAsStream(Constantes.SISVIDA_FILES + "etiqueta.xls");
		    HSSFWorkbook wb = new HSSFWorkbook(excelFIS);
            HSSFSheet sheet = wb.getSheetAt(0); 
              
            HSSFRow row;  
            
            
            int rowCountEsquerdo = 1;
            int rowCountDireito = 1;
            row = sheet.getRow(rowCountEsquerdo);	
			HSSFFont fontCabecalho = wb.createFont();  
            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); 
            if(  funcionariosSelecionados != null && funcionariosSelecionados.length > 0 ){
            
	            for (int i = 0; i < funcionariosSelecionados.length; i++) {
	            	Funcionario funcionario = funcionariosSelecionados[i];
	            	HSSFRichTextString richTextNomeSegurado = new HSSFRichTextString( funcionario.getNome() ); 
	            	HSSFRichTextString richTextEndereco = new HSSFRichTextString( funcionario.getEndereco() );
	            	HSSFRichTextString richTextBairro = new HSSFRichTextString( funcionario.getBairro() );
	            	HSSFRichTextString richCEPCidade = new HSSFRichTextString( funcionario.getCep() + " / " + funcionario.getCidade().getNom_cidade() + " - " + funcionario.getCidade().getCod_estado().getSgl_estado()  );
	            	if( i % 2 == 0){
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richTextNomeSegurado); 		            
		            	rowCountEsquerdo++;
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richTextEndereco); 		            
		            	rowCountEsquerdo++;	
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richTextBairro); 		            
		            	rowCountEsquerdo++;			
			            row = sheet.getRow( rowCountEsquerdo );
			            if( row == null ){
			            	sheet.createRow( rowCountEsquerdo );
			            	row = sheet.getRow( rowCountEsquerdo );
			            	row.createCell(1);
			            }
			            row.getCell(1).setCellValue(richCEPCidade); 		            
		            	rowCountEsquerdo++;	
		            	rowCountEsquerdo++;	
		            	rowCountEsquerdo++;	
		            	rowCountEsquerdo++;	
		            }
	            	if( i % 2 != 0){
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richTextNomeSegurado); 		            
			            rowCountDireito++;
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richTextEndereco); 		            
			            rowCountDireito++;	
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richTextBairro); 		            
			            rowCountDireito++;			
			            row = sheet.getRow( rowCountDireito );
			            if( row == null ){
			            	sheet.createRow( rowCountDireito );
			            	row = sheet.getRow( rowCountDireito );
			            	row.createCell(3);
			            }
			            row.getCell(3).setCellValue(richCEPCidade); 		            
			            rowCountDireito++;	
			            rowCountDireito++;	
			            rowCountDireito++;	
			            rowCountDireito++;	
		            }
				}	          
	            
	            
	            HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();  
	            res.setContentType("application/vnd.ms-excel");  
	            res.setHeader("Content-disposition",  "attachment; filename=MalaDiretaFuncionario.xls");  
	 
	            
	           try {  
	                 ServletOutputStream out = res.getOutputStream();  
	                 wb.write(out);  
	                 out.flush();  
	                 out.close();  
	           } catch (IOException ex) {   
	                   ex.printStackTrace();  
	           }  
	           
	           FacesContext faces = FacesContext.getCurrentInstance();  
	           faces.responseComplete();  
  
            }else{
            	addErrorMessage("Selecione um ou mais Funcion�rios(s) para Gera��o do Arquivo de Impress�o." );
            }
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        
        return "export";
		
	}	
	
// =====================================================================================Getters and Setters
	
	
	public HtmlSelectOneMenu getTipoFuncionarioSelecionado() {
		return tipoFuncionarioSelecionado;
	}


	public void setTipoFuncionarioSelecionado(
			HtmlSelectOneMenu tipoFuncionarioSelecionado) {
		this.tipoFuncionarioSelecionado = tipoFuncionarioSelecionado;
	}
	
	public List<Cidade> getListaCidades() {
		return listaCidades;
	}


	public void setListaCidades(List<Cidade> listaCidades) {
		this.listaCidades = listaCidades;
	}
	

	public Funcionario getFuncionario() {
		return funcionario;
	}


	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}


	public List<Funcionario> getResultadoPesquisa() {
		return resultadoPesquisa;
	}


	public void setResultadoPesquisa(List<Funcionario> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	public String getIdadeFuncionario() {
		return idadeFuncionario;
	}

	public void setIdadeFuncionario(String idadeFuncionario) {
		this.idadeFuncionario = idadeFuncionario;
	}

	public Funcionario[] getFuncionariosSelecionados() {
		return funcionariosSelecionados;
	}

	public void setFuncionariosSelecionados(Funcionario[] funcionariosSelecionados) {
		this.funcionariosSelecionados = funcionariosSelecionados;
	}	


	// Fim Getters and Setters
}
