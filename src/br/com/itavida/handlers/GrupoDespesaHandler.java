package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.GrupoDespesaDAO;
import br.com.itavida.entidades.GrupoDespesa;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class GrupoDespesaHandler extends MainHandler implements Serializable{

	private static final long serialVersionUID = 2L;
	private GrupoDespesa grupoDespesa;
	private List<GrupoDespesa> resultadoPesquisa;
	
	// Getters and Setters

	public GrupoDespesa getGrupoDespesa() {
		return grupoDespesa;
	}
	public void setGrupoDespesa(GrupoDespesa grupoDespesa) {
		this.grupoDespesa = grupoDespesa;
	}
	
	public List<GrupoDespesa> getResultadoPesquisa() {
		return resultadoPesquisa;
	}
	public void setResultadoPesquisa(List<GrupoDespesa> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}
	// Fim Getters and Setters
	
	/**
	 * Construtor, que inicializa a classe, zerando os atributos
	 */
	public GrupoDespesaHandler(){
		cancelar( null );
	}
	
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaGrupoDespesasPorNome(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		GrupoDespesaDAO dao = new GrupoDespesaDAO(session);
		
		GrupoDespesa grupoDespesaFiltro = getGrupoDespesa();

		List<GrupoDespesa> lista = dao.busca( grupoDespesaFiltro.getNomeGrupoDespesa() );
		setResultadoPesquisa(lista);
			
	}
	
	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<GrupoDespesa, Integer> dao = new Dao<GrupoDespesa, Integer>(session, GrupoDespesa.class);
			GrupoDespesa grupoDespesaGravar = getGrupoDespesa();
			dao.merge( grupoDespesaGravar );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeGrupoDespesa", "Nome da Grupo Despesa");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarGrupoDespesa( ){
		
		//Recupera o item escolhido
		resultadoPesquisa = null;
		
		return "grupoDespesaIncluir";
	}
	
	/**
	 * Realiza a exclus�o de uma GrupoDespesa
	 * @param e
	 */
	public void excluirGrupoDespesa(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirGrupoDespesa");
		GrupoDespesa grupoDespesaDeletada = ( GrupoDespesa ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<GrupoDespesa, Integer> dao = new Dao<GrupoDespesa, Integer>(session, GrupoDespesa.class);
		dao.delete(grupoDespesaDeletada);	
		
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaGrupoDespesasPorNome(e);
	}	
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setGrupoDespesa( new GrupoDespesa() );
		this.resultadoPesquisa = null;
		
	}

	
}
