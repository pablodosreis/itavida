package br.com.itavida.handlers;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;

import br.com.itavida.dao.ArquivoMensalDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.DespesaDAO;
import br.com.itavida.dao.MovimentacaoPropostaAtrasadaDAO;
import br.com.itavida.dao.MovimentacaoPropostaInadimplenteDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.ArquivoMensal;
import br.com.itavida.entidades.Despesa;
import br.com.itavida.entidades.ItemMensal;
import br.com.itavida.entidades.MovimentacaoPropostaAtrasada;
import br.com.itavida.entidades.MovimentacaoPropostaInadimplente;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.exceptions.SistemaException;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Carteiro;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.CpfCnpjUtils;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;
import br.com.itavida.util.NumberUtil;

@SuppressWarnings("serial")
public class GeracaoArquivoMensalHandler extends ExcelGenerateHandler implements Serializable{

	private static Logger logger = Logger.getLogger( GeracaoArquivoMensalHandler.class );

	private String competencia;
	
	private Map<String, List<ItemMensal>> mapaitens;
	
	public GeracaoArquivoMensalHandler() {		
		super();		
	}
	
	public Object atualizarPropostas( ActionEvent event ){ 
		Session currentSession = HibernateUtil.currentSession();
		PropostaDAO dao = new PropostaDAO(currentSession);
		
		List<Proposta> listaPropostas = dao.findAll();
		dao.populaCapital(listaPropostas);
		for (Proposta proposta : listaPropostas) {
			dao.update(proposta);
		}
		
		String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_OPERACAO_SUCESSO, null);
		addGlobalMessage( message );
		
		
		return null;
	}
	
	public Object gerarArquivo( ActionEvent event ){
		Session currentSession = HibernateUtil.currentSession();
		ArquivoMensalDAO dao = new ArquivoMensalDAO(currentSession);
		
		UsuarioHandler usuarioHandler = (UsuarioHandler) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioHandler");
		Usuario usuario = usuarioHandler.getUsuario();
		
		if (StringUtils.isEmpty(competencia)) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.competencia.invalida", null) );
			return null;
		}
		
		if (dao.existeArquivoPeriodoIgualOuAnterior(getCompetenciaData())) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.ja.existe.arquivo.competencia", null) );
			return null;
		}
		
		try {
			dao.gerarArquivo(getCompetenciaData(), usuario.getNomeUsuario());
			new Carteiro().enviar("Sisvida", "pablodosreis@gmail.com,cadastro@itavida.com.br", "Arquivo Mensal gerado com sucesso", "Arquivo ensal gerado com Sucesso");
		} catch(SistemaException e) {
			addErrorMessage(e.getMessage());
		} catch(Exception e) {
			dao.getSession().getTransaction().rollback();
			logger.error("Erro ao gerar arquivo mensal.", e);
			new Carteiro().enviar("Sisvida", "pablodosreis@gmail.com", "Arquivo Mensal Erro", e.getMessage());
			addErrorMessage("Erro inesperado, caso persista contate o administrador do sistema.");
			return null;
		}
		
		String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_OPERACAO_SUCESSO, null);
		addGlobalMessage( message );
		
		return null;
	}
	
	public Date getCompetenciaData() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = format.parse("01/" + competencia);
		} catch (ParseException e1) {
			addErrorMessage( JSFUtils.getMessageResourceString("Messages", "error.competencia.invalida", null) );
		}
		return date;
	}
	
	public String getCompetencia() {
		return competencia;
	}

	public void setCompetencia(String periodo) {
		this.competencia = periodo;
	}
	
	public List<ArquivoMensal> getArquivos() {
		Session currentSession = HibernateUtil.currentSession();
		ArquivoMensalDAO dao = new ArquivoMensalDAO(currentSession);
		return dao.findAllAtivos();
	}
	
	public void excluirArquivo(ActionEvent e) {
		Session currentSession = HibernateUtil.currentSession();
		ArquivoMensalDAO dao = new ArquivoMensalDAO(currentSession);
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluir");
		ArquivoMensal arquivo = ( ArquivoMensal ) param.getValue();
		
		dao.excluirArquivo(arquivo);
		
		String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_EXCLUSAO_SUCESSO, null);
		addGlobalMessage( message );
	}
	
	public void baixarArquivo(ActionEvent e) {
		Session currentSession = HibernateUtil.currentSession();
		ArquivoMensalDAO dao = new ArquivoMensalDAO(currentSession);
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluir");
		ArquivoMensal arquivo = ( ArquivoMensal ) param.getValue();
		
		this.mapaitens = getMapItens(arquivo);
		if(this.mapaitens == null || this.mapaitens.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
			return;
		}
		super.gerarRelatorio(this.mapaitens.keySet(), 6);
		
	}
	
	public Map<String, List<ItemMensal>> getMapItens(ArquivoMensal arquivoMensal) {
		Map<String, List<ItemMensal>> mapa = new HashMap<String, List<ItemMensal>>();
		for(ItemMensal item : arquivoMensal.getItens()) {
			if (item.isItemPago()) {
				List<ItemMensal> lista = mapa.get(item.getEmpresa().getNomeEmpresa());
				if (lista == null) {
					mapa.put(item.getEmpresa().getNomeEmpresa(), new ArrayList<ItemMensal>());
				}
				mapa.get(item.getEmpresa().getNomeEmpresa()).add(item);
			}
		}
		return mapa;
	}

	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		int indiceLinha = 0;
		addRow(aba);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Formul�rio de Movimenta��o - Vida", indiceLinha, 1, 0, 11);
		addRow(aba);
		indiceLinha++;
		addRow(aba);
		indiceLinha++;
		addTextoMesclado(aba, getStiloTextoTitulo(), "Dados da Empresa", indiceLinha, 1, 0, 9);
		addTextoMesclado(aba, getStiloTextoTitulo(), "Dados da Ap�lice", indiceLinha, 1, 10, 11	);
		addRow(aba);
		indiceLinha++;
		addTextoMesclado(aba, getStiloTextoSimples(), "*Raz�o Social/Estipulante: ITAVIDA SEGUROS", indiceLinha, 1, 0, 9);
		addTextoMesclado(aba, getStiloTextoSimples(), "*Compet�ncia Fatura:", indiceLinha, 1, 10, 10 );
		addTextoMesclado(aba, getStiloTextoSimples(), "",  indiceLinha, 1, 11, 11);
		addRow(aba);
		indiceLinha++;
		addTextoMesclado(aba, getStiloTextoSimples(), getCNPJ(aba), indiceLinha, 1, 0, 8);
		addTextoMesclado(aba, getStiloTextoSimples(), "*Sub-Estipulante",  indiceLinha, 1, 9, 9);
		addTextoMesclado(aba, getStiloTextoSimples(), "*N� Ap�lice",  indiceLinha, 1, 10, 10);
		addTextoMesclado(aba, getStiloTextoSimples(), getNumeroApolice(aba),  indiceLinha, 1, 11, 11);
		addRow(aba);
		indiceLinha++;
		addTextoMesclado(aba, getStiloTextoSimples(), "Contato:", indiceLinha, 1, 0, 8);
		addTextoMesclado(aba, getStiloTextoSimples(), "Telefone: (31)3274-5170",  indiceLinha, 1, 9, 9);
		addTextoMesclado(aba, getStiloTextoSimples(), "*N� Sub Estipulante",  indiceLinha, 1, 10, 10);
		addTextoMesclado(aba, getStiloTextoSimples(), "",  indiceLinha, 1, 11, 11);
		addRow(aba);
		indiceLinha++;
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","orgao", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","nome-segurado-principal", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-segurado", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","plano", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","conjuge", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","data-nascimento-conjuge", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","cpf-conjuge", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-segurado", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","capital-conjuge", null));		
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-segurado", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-conjuge", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","premio-total", null));
		addCelulaTexto(aba, getStiloTextoCabecalho(), JSFUtils.getMessageResourceString("Messages","referencia", null));
		
	}

	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		for(ItemMensal item : mapaitens.get(aba)) {
       		addRow(aba);
       	
       		addCelulaTexto(aba, getStiloTextoSimples(), item.getOrgao().getNomeOrgao());
       		addCelulaTexto(aba, getStiloTextoSimples(), item.getNomeSegurado(), "-");
       		addCelulaData(aba, getStiloDataSimples(), item.getDataNascimentoSegurado(), "-");
       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(item.getCpfSegurado()), "-");
       		addCelulaTexto(aba, getStiloTextoSimples(), item.getTipoProposta().getDescricao());
       		addCelulaTexto(aba, getStiloTextoSimples(), item.getNomeConjuge(), "-");
       		addCelulaData(aba, getStiloDataSimples(), item.getDataNascimentoConjuge(), "-");
       		addCelulaTexto(aba, getStiloTextoSimples(), CpfCnpjUtils.getCpfCnpj(item.getCpfConjuge()), "-");
       		addCelulaNumero(aba, getStiloMonetarioSimples(), item.getCapitalSegurado(), "-");
       		addCelulaNumero(aba, getStiloMonetarioSimples(), item.getCapitalConjuge(), "-");
       		
       		
       		Double premioSeguradoArredondado = NumberUtil.round(item.getPremioSegurado(), 2);
       		Double premioConjugeArredondado =  NumberUtil.round(item.getPremioConjuge(), 2);
       		Double totalArredondado = NumberUtil.round(premioSeguradoArredondado + (item.getPremioConjuge() != null ? premioConjugeArredondado : 0.0), 2);
       		
       		addCelulaNumero(aba, getStiloMonetarioSimples(), premioSeguradoArredondado, "-");
       		addCelulaNumero(aba, getStiloMonetarioSimples(), premioConjugeArredondado, "-");
       		addCelulaNumero(aba, getStiloMonetarioSimples(), totalArredondado, "-");
       		
       		addCelulaTexto(aba, getStiloTextoSimples(), item.getReferenciaTxt(), "-");
       		
       	
		}
	}
}
