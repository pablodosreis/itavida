package br.com.itavida.handlers;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.Session;

import br.com.itavida.dao.ExportacaoDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;

public class MalaDiretaSeguradosHandler {
	
	private List<Integer> listaOrgaosSelecionados = new ArrayList<Integer>(); 
	private List<SelectItem> orgaosSelectItens;
	
	
	//Getters and setters
	public List<Integer> getListaOrgaosSelecionados() {
		return listaOrgaosSelecionados;
	}
	public void setListaOrgaosSelecionados(List<Integer> listaOrgaosSelecionados) {
		this.listaOrgaosSelecionados = listaOrgaosSelecionados;
	}
	public List<SelectItem> getOrgaosSelectItens() {
		return orgaosSelectItens;
	}
	public void setOrgaosSelectItens(List<SelectItem> orgaosSelectItens) {
		this.orgaosSelectItens = orgaosSelectItens;
	}

	
	// fim getters and setters ===========================================================================================
	
	
	
	public MalaDiretaSeguradosHandler(){
		populaOrgaosSelectItens(null);
	}
	
	
	
	/**
	 * Popula um combo com valores dos Tipos de Proposta
	 * @return
	 */
	public void populaOrgaosSelectItens(ActionEvent e){
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		orgaosSelectItens = new ArrayList<SelectItem>();

		OrgaoDAO orgaoDAO = new OrgaoDAO( session );
		List<Orgao> listaOrgaos = orgaoDAO.findAllOrgaos(  );
		
		for (Iterator iterator = listaOrgaos.iterator(); iterator.hasNext();) {
			Orgao orgao = (Orgao) iterator.next();
			orgaosSelectItens.add( new SelectItem( orgao.getId(), orgao.getSiglaOrgao() + " - " + orgao.getNomeOrgao()  ) );
		}		
		
		
	}
	
	public void limparDados( ActionEvent e ){
		populaOrgaosSelectItens(e);
		listaOrgaosSelecionados = new ArrayList<Integer>();
	}
	
	public String generateReport(  ){
		
		Session session = HibernateUtil.currentSession();
		ExportacaoDAO exportacaoDAO = new ExportacaoDAO( session );
		PropostaDAO propostaDAO = new PropostaDAO(session);
		Object[] objetosListaOrgao = listaOrgaosSelecionados.toArray();
		Integer[] orgaosSelecionadosInteger = new Integer[objetosListaOrgao.length];
		for (int i = 0; i < objetosListaOrgao.length; i++) {
			orgaosSelecionadosInteger[i] = new Integer( objetosListaOrgao[i].toString().trim() );
		}
		
		List<Proposta> listaPropostas = exportacaoDAO.buscaPorOrgao(orgaosSelecionadosInteger, false );
		
		Map<Integer, List<Proposta>> mapaPropostasPorOrgao = new HashMap<Integer, List<Proposta>>();
		for (Iterator iterator = listaPropostas.iterator(); iterator.hasNext();) {
			Proposta proposta = (Proposta) iterator.next();
			
			if( mapaPropostasPorOrgao.containsKey( proposta.getOrgao().getId() ) ){
				List<Proposta> propostasPorOrgao = mapaPropostasPorOrgao.get( proposta.getOrgao().getId() );
				propostasPorOrgao.add( proposta );
			}else{
				List<Proposta> propostasPorOrgao = new ArrayList<Proposta> ();
				propostasPorOrgao.add( proposta );
				mapaPropostasPorOrgao.put(proposta.getOrgao().getId(), propostasPorOrgao );
			}
		}
		
        try {  
        	
            HSSFWorkbook wb = new HSSFWorkbook();  
            HSSFSheet sheet = wb.createSheet();  
              
            HSSFRow row;  
            
            //Estilos
            short formatoMonetario = wb.createDataFormat().getFormat("R$ #,##0.00;[Red]R$ #,##0.00");
			
            HSSFCellStyle estiloMonetario = wb.createCellStyle();
            estiloMonetario.setDataFormat( formatoMonetario );
            
			HSSFCellStyle estiloCantoEsquerdo = wb.createCellStyle();
			
           //Adicionando bordas para o CantoEsquerdo
			estiloCantoEsquerdo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoEsquerdo.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloCantoEsquerdo.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoEsquerdo.setTopBorderColor(HSSFColor.BLACK.index);
			
			HSSFCellStyle estiloMeio = wb.createCellStyle();
              
           //Adicionando bordas para o Meio
			estiloMeio.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloMeio.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloMeio.setTopBorderColor(HSSFColor.BLACK.index);
			
			HSSFCellStyle estiloCantoDireito = wb.createCellStyle();
              
           //Adicionando bordas para o CantoDireito
			estiloCantoDireito.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setTopBorderColor(HSSFColor.BLACK.index);
			estiloCantoDireito.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloCantoDireito.setRightBorderColor(HSSFColor.GREEN.index);
			estiloCantoDireito.setDataFormat( formatoMonetario );
			
			HSSFCellStyle estiloFullBorder = wb.createCellStyle();
              
               //Adicionando bordas para o CantoEsquerdo
			estiloFullBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setBottomBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setLeftBorderColor(HSSFColor.GREEN.index);
			estiloFullBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setTopBorderColor(HSSFColor.BLACK.index);
			estiloFullBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloFullBorder.setRightBorderColor(HSSFColor.GREEN.index);
            
            
            
            //Cabe�alho  
            row = sheet.createRow(0);  
            
            HSSFFont fontCabecalho = wb.createFont();  
            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
            HSSFRichTextString richTextOrgao = new HSSFRichTextString("Orgao");  
            richTextOrgao.applyFont(fontCabecalho);  
            HSSFRichTextString richTextUfOrgao = new HSSFRichTextString("UF_Orgao");  
            richTextUfOrgao.applyFont(fontCabecalho); 
            HSSFRichTextString richTextSeguradoPrincipal = new HSSFRichTextString("Segurado_Principal");  
            richTextSeguradoPrincipal.applyFont(fontCabecalho); 
            HSSFRichTextString richTextDataNascimento = new HSSFRichTextString("Data_Nasc_Func");  
            richTextDataNascimento.applyFont(fontCabecalho);  
            HSSFRichTextString richTextMatricula = new HSSFRichTextString("Matricula");  
            richTextMatricula.applyFont(fontCabecalho);             
            HSSFRichTextString richTextRg = new HSSFRichTextString("RG");  
            richTextRg.applyFont(fontCabecalho);  
            HSSFRichTextString richTextCPFFuncionario = new HSSFRichTextString("CPF_Funcionario");  
            richTextCPFFuncionario.applyFont(fontCabecalho);      
            HSSFRichTextString richTextConjuge = new HSSFRichTextString("Conjuge");  
            richTextConjuge.applyFont(fontCabecalho);   
            HSSFRichTextString richTextDataNascimentoConjuge = new HSSFRichTextString("Data_Nasc_Conj");  
            richTextDataNascimentoConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCPFConjuge = new HSSFRichTextString("CPF_Conj");  
            richTextCPFConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextRGConjuge = new HSSFRichTextString("RG_Conj");  
            richTextRGConjuge.applyFont(fontCabecalho); 
            HSSFRichTextString richTextCapitalFuncionario = new HSSFRichTextString("Capital_Func");  
            richTextCapitalFuncionario.applyFont(fontCabecalho);   
            HSSFRichTextString richTextCapitalConjuge = new HSSFRichTextString("Capital_Conjuge");  
            richTextCapitalConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextValorFuncionario= new HSSFRichTextString("Valor_Funcionario");  
            richTextValorFuncionario.applyFont(fontCabecalho);
            HSSFRichTextString richTextValorConjuge= new HSSFRichTextString("Valor_Conjuge");  
            richTextValorConjuge.applyFont(fontCabecalho);    
            HSSFRichTextString richTextValorTotal= new HSSFRichTextString("Valor_Total");  
            richTextValorTotal.applyFont(fontCabecalho);              

            row.createCell(0).setCellValue(richTextOrgao);  
            row.createCell(1).setCellValue(richTextUfOrgao);  
            row.createCell(2).setCellValue(richTextSeguradoPrincipal);  
            row.createCell(3).setCellValue(richTextDataNascimento); 
            row.createCell(4).setCellValue(richTextMatricula); 
            row.createCell(5).setCellValue(richTextRg); 
            row.createCell(6).setCellValue(richTextCPFFuncionario); 
            row.createCell(7).setCellValue(richTextConjuge); 
            row.createCell(8).setCellValue(richTextDataNascimentoConjuge);          
            row.createCell(9).setCellValue(richTextCPFConjuge); 
            row.createCell(10).setCellValue(richTextRGConjuge); 
            row.createCell(11).setCellValue(richTextCapitalFuncionario); 
            row.createCell(12).setCellValue(richTextCapitalConjuge);    
            row.createCell(13).setCellValue(richTextValorFuncionario); 
            row.createCell(14).setCellValue(richTextValorConjuge);       
            row.createCell(15).setCellValue(richTextValorTotal);  
            
            //Fim cabe�alho 
            int inicio = 1;
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");  
            DecimalFormat decimalFormat = new DecimalFormat();  
            decimalFormat.setMaximumFractionDigits(2);  
            decimalFormat.setMinimumFractionDigits(2);
            decimalFormat.setGroupingUsed( true );
            decimalFormat.setGroupingSize(3);
            for (Iterator iterator = mapaPropostasPorOrgao.values().iterator(); iterator.hasNext();) {
				List<Proposta> listaPropostasPorOrgao = ( List<Proposta> ) iterator.next();
                
				double valorTotal = 0.0;
				double valorProlaboreTotal = 0.0;
				
				for (int i = 0; i < listaPropostasPorOrgao.size(); i++){  
					
					Proposta proposta = listaPropostasPorOrgao.get(i);
					
					propostaDAO.populaCapital(proposta);
	                
					
	                //Soma o valor total
					/*
	                if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
	        		    && proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_VALOR_FIXO ) ){
	                	if( proposta.getCapitalSeguradoLimiteIdade() != null ){
	                		
	                		if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null 
	        						&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_50 ) 
	        						&& proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
	                			proposta.setCapitalSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
	        					proposta.setCapitalConjuge( proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() );
	        					proposta.setPremioConjuge( proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() );
	        					proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
	        					proposta.setTotalPremios( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() + proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado50() );
	        				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null 
	        						&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_100 )
	        						&& proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
	        					proposta.setCapitalSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
	        					proposta.setCapitalConjuge( proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() );
	        					proposta.setPremioConjuge( proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() );
	        					proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
	        					proposta.setTotalPremios( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() + proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado100() );
	        				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null 
	        						&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_INDIVIDUAL ) 
	        						&& proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
	        					proposta.setCapitalConjuge( 0.0 );
	        					proposta.setPremioConjuge( 0.0 );
	        					proposta.setCapitalSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
	        					proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
	        					proposta.setTotalPremios( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
	        				}	                		
	                	}
	                }else if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
	        				&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_ETARIA ) ){
	        			
	            		if( proposta.getCapitalSeguradoFaixaEtaria() != null ){
	            			Collection<DetalheCapitalFaixaEtaria> detalhesCapital = proposta.getCapitalSeguradoFaixaEtaria().getDetalhecapitalfaixaetaria_collection();
	            			if( detalhesCapital != null && detalhesCapital.size() > 0 ){
	            				DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria = null;
	            				for (Iterator iteratorDetalhe = detalhesCapital.iterator(); iteratorDetalhe.hasNext();) {
	            					detalheCapitalFaixaEtaria = (DetalheCapitalFaixaEtaria) iteratorDetalhe.next();
	            				}
	            				
	            				proposta.setDetalheCapitalFaixaEtaria( detalheCapitalFaixaEtaria );
	            			}
	            		}

	                	if( proposta.getDetalheCapitalFaixaEtaria() != null ){
	        				
	        				if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null 
	        						&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_CASADO ) && proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() != null ){
	        					proposta.setCapitalSegurado( proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() );
	        					proposta.setCapitalConjuge( proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() );
	        					proposta.setPremioConjuge( proposta.getDetalheCapitalFaixaEtaria().getSomaTitularConjuge() - proposta.getDetalheCapitalFaixaEtaria().getTitular()  );
	        					proposta.setPremioSegurado( proposta.getDetalheCapitalFaixaEtaria().getTitular() );
	        					proposta.setTotalPremios( proposta.getDetalheCapitalFaixaEtaria().getSomaTitularConjuge() );					
	        				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null 
	        						&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_INDIVIDUAL ) && proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() != null ){
	        					proposta.setCapitalConjuge( 0.0 );
	        					proposta.setPremioConjuge( 0.0  );
	        					proposta.setCapitalSegurado( proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() );
	        					proposta.setPremioSegurado( proposta.getDetalheCapitalFaixaEtaria().getTitular() );
	        					proposta.setTotalPremios( proposta.getDetalheCapitalFaixaEtaria().getTitular() );					
	        					
	        				}
	        			}
	                }*/
	                
	                if( proposta.getTotalPremios() != null ){
	                	valorTotal += proposta.getTotalPremios();
	                }
	                valorProlaboreTotal += proposta.getOrgao().getValorProlabore();
	                
	                row = sheet.createRow(inicio);  
	                row.createCell(0).setCellValue( new HSSFRichTextString( proposta.getOrgao().getNomeOrgao() ));  
	                row.createCell(1).setCellValue( new HSSFRichTextString( proposta.getOrgao().getCidade().getCod_estado().getSgl_estado() ));  
	                row.createCell(2).setCellValue( new HSSFRichTextString( proposta.getNomeSegurado() ) );  
	                row.createCell(3).setCellValue( new HSSFRichTextString( df.format( proposta.getDataNascimentoSegurado() ) ) ); 
	                row.createCell(4).setCellValue( new HSSFRichTextString( proposta.getMatriculaSegurado() ) ); 
	                row.createCell(5).setCellValue( new HSSFRichTextString( proposta.getRgSegurado() ) ); 
	                row.createCell(6).setCellValue( new HSSFRichTextString( proposta.getCpfSegurado() ) ); 
	                row.createCell(7).setCellValue( new HSSFRichTextString( proposta.getNomeConjuge() ) ); 
	                if( proposta.getDataNascimentoConjuge() != null ){
	                	row.createCell(8).setCellValue( new HSSFRichTextString( df.format( proposta.getDataNascimentoConjuge() ) ) );  
	                }else{
	                	row.createCell(8).setCellValue( new HSSFRichTextString( "" ) );
	                }
	                row.createCell(9).setCellValue( new HSSFRichTextString( proposta.getCpfConjuge() ) ); 
	                row.createCell(10).setCellValue( new HSSFRichTextString( proposta.getRgConjuge() ) ); 
	                if(  proposta.getCapitalSegurado() != null ){
	                	row.createCell(11).setCellValue( proposta.getCapitalSegurado()  ); 
	                }else{
	                	row.createCell(11).setCellValue( 0.0 );
	                }
	                row.getCell(11).setCellStyle( estiloMonetario );
	                if( proposta.getCapitalConjuge() != null ){
	                	row.createCell(12).setCellValue( proposta.getCapitalConjuge()  );
	                }else{
	                	row.createCell(12).setCellValue( 0.0 );
	                }
	                row.getCell(12).setCellStyle( estiloMonetario );	            
	                
	                if( proposta.getPremioSegurado() != null){
	                	row.createCell(13).setCellValue(  proposta.getPremioSegurado()  ); 
	                }else{
	                	row.createCell(13).setCellValue(  0.0  );
	                }
	                row.getCell(13).setCellStyle( estiloMonetario );
	                if( proposta.getPremioConjuge() != null ){
	                	row.createCell(14).setCellValue(  proposta.getPremioConjuge()  );
	                }else{
	                	row.createCell(14).setCellValue( 0.0  );
	                }
	                row.getCell(14).setCellStyle( estiloMonetario );
	                if( proposta.getTotalPremios() != null ){
	                	row.createCell(15).setCellValue(  proposta.getTotalPremios() ); 
	                }else{
	                	row.createCell(15).setCellValue( 0.0 ); 
	                }
	                row.getCell(15).setCellStyle( estiloMonetario );
	                inicio++;
	                
	            }  
				
				
				row = sheet.createRow(inicio); 
	            fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	            HSSFRichTextString richTextTotalDescontado = new HSSFRichTextString("TOTAL DESCONTADO");  

	            
				row.createCell(0).setCellValue(richTextTotalDescontado); 
				row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
				
				row.createCell(15).setCellValue( valorTotal  );
				row.getCell( 15 ).setCellStyle( estiloCantoDireito );
				
				for(int j=1;j<15;j++){
					row.createCell(j);
					row.getCell( j ).setCellStyle( estiloMeio );
				}
				
				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	            HSSFRichTextString richTextProlabore = new HSSFRichTextString("PR�-LABORE");  
				row.createCell(0).setCellValue(richTextProlabore); 
				row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
				row.createCell(15).setCellValue( valorProlaboreTotal  );
				row.getCell( 15 ).setCellStyle( estiloCantoDireito );
				
				for(int j=1;j<15;j++){
					row.createCell(j);
					row.getCell( j ).setCellStyle( estiloMeio );
				}
				
				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	            HSSFRichTextString richTextValorRepasse = new HSSFRichTextString("VALOR REPASSE");  
				row.createCell(0).setCellValue(richTextValorRepasse); 
				row.getCell( 0 ).setCellStyle( estiloCantoEsquerdo );
				row.createCell(15).setCellValue( valorTotal - valorProlaboreTotal  );
				row.getCell( 15 ).setCellStyle( estiloCantoDireito );
				for(int j=1;j<15;j++){
					row.createCell(j);
					row.getCell( j ).setCellStyle( estiloMeio );
				}
				
				inicio++;
				row = sheet.createRow(inicio);
				fontCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	            HSSFRichTextString richTextValorTotalVidas = new HSSFRichTextString(listaPropostasPorOrgao.size() + " VIDAS");  
				row.createCell(0).setCellValue(richTextValorTotalVidas); 
				row.getCell( 0 ).setCellStyle( estiloFullBorder );
				inicio++;
				inicio++;
				
			}
          
            HSSFRow header = sheet.getRow(0); 
		    for(short column=0;column<header.getLastCellNum(); column++){
		    	sheet.autoSizeColumn(column);
		    }    
		    
            HttpServletResponse res = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();  
            res.setContentType("application/vnd.ms-excel");  
            res.setHeader("Content-disposition",  "attachment; filename=ExportacaoMensal.xls");  
 
            
           try {  
                 ServletOutputStream out = res.getOutputStream();  
                 wb.write(out);  
                 out.flush();  
                 out.close();  
           } catch (IOException ex) {   
                   ex.printStackTrace();  
           }  
           
           FacesContext faces = FacesContext.getCurrentInstance();  
           faces.responseComplete();  
  
              
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        
        return "export";
		
	}
	
	
}
