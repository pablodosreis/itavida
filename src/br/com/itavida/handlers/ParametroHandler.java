package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class ParametroHandler extends MainHandler implements Serializable {

	private Empresa empresa = new Empresa();
	private List<Empresa> resultadoPesquisa;

	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		if( validarFormulario(e) ){
		
			Dao<Empresa, Integer> dao = new Dao<Empresa, Integer>(session, Empresa.class);
			dao.merge( getEmpresa() );
			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			
			cancelar(e);
		}
		
	}
	
	private boolean validarExclusaoEmpresa (Empresa empresa) {
		boolean exclusaoValida = true;
		Session session = HibernateUtil.currentSession();
		PropostaDAO propostaDAO = new PropostaDAO(session);
		Integer propostas = propostaDAO.countPropostasPorEmpresa(empresa.getId());
		if (propostas != null && !propostas.equals(0)) {
			exclusaoValida = false;
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_VALIDACAO_EMPRESA_VINCULADA_PROPOSTAS, new Object[]{empresa.getNomeEmpresa()});
			addErrorMessage(message);
		}
		return exclusaoValida;
	}
	
	public void excluirEmpresa(ActionEvent e){
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirEmpresa");
		Empresa empresaDeletada = ( Empresa ) param.getValue();
		if (validarExclusaoEmpresa(empresaDeletada)) {
			//Realiza as a��es de banco: Busca e seta os valores
			Session session = HibernateUtil.currentSession();
			Dao<Empresa, Integer> dao = new Dao<Empresa, Integer>(session, Empresa.class);
			dao.delete(empresaDeletada);
			// Dispara novamente a pesquisa para manter o v�nculo
			pesquisaEmpresas(e);
		}
	}	
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarEmpresa( ){
		return "incluirEmpresa";
	}
	
	public void pesquisaEmpresas(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		EmpresaDAO dao = new EmpresaDAO(session);	
		
		List<Empresa> lista = dao.buscaPeloNome(getEmpresa().getNomeEmpresa());
		setResultadoPesquisa(lista);
		if (lista == null || lista.isEmpty()) {
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA, null);
			addWarnMessage( message );
		}
			
	}

	
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setEmpresa( new Empresa() );
		//this.resultadoPesquisa = null;
		
	}
	
	/**
	 * Realiza todas as valida��es
	 * @return
	 */
	public boolean validarFormulario(ActionEvent e){
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeEmpresa", "Nome da Empresa");
		
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		 
		return retorno;
	}

	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Empresa> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Empresa> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}
	
}
