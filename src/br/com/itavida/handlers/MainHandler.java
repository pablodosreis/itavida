package br.com.itavida.handlers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.ValueHolder;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.brazilutils.br.cpfcnpj.Cnpj;
import org.brazilutils.br.cpfcnpj.Cpf;
import org.brazilutils.validation.ValidationException;
import org.hibernate.Session;

import br.com.itavida.dao.CidadeDAO;
import br.com.itavida.dao.Dao;
import br.com.itavida.dao.DespesaDAO;
import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.FilialDAO;
import br.com.itavida.dao.FuncionarioDAO;
import br.com.itavida.dao.GrauParentescoDAO;
import br.com.itavida.dao.GrupoDespesaDAO;
import br.com.itavida.dao.NomeTabelaDAO;
import br.com.itavida.dao.OrgaoDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dao.TipoOcorrenciaDAO;
import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.Despesa;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Filial;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.GrauParentesco;
import br.com.itavida.entidades.GrupoDespesa;
import br.com.itavida.entidades.ModeloProposta;
import br.com.itavida.entidades.NomeTabela;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.TipoOcorrencia;
import br.com.itavida.enums.SituacaoBoleto;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;

public class MainHandler {
	 
	private boolean visibleMessage;
	
	private boolean mostrarClose;
	
	private Map<String,String> mapaCPF;
	
	protected List<SelectItem> opcoesMesesCombo;
	
	protected List<SelectItem> opcoesSituacaoPropostaCombo;
	
	protected List<SelectItem> opcoesSituacaoBoletoCombo;
	
	protected List<SelectItem> opcoesEmpresaCombo;
	
	protected List<String> situacaoPropostaSelectedList = new ArrayList<String>();
	
	protected List<String> empresaSelectedList = new ArrayList<String>();
	
	public List<SelectItem> getOpcoesEmpresaCombo() {
		return opcoesEmpresaCombo;
	}

	public void setOpcoesEmpresaCombo(List<SelectItem> opcoesEmpresaCombo) {
		this.opcoesEmpresaCombo = opcoesEmpresaCombo;
	}

	public List<SelectItem> getOpcoesSituacaoPropostaCombo() {
		return opcoesSituacaoPropostaCombo;
	}

	public void setOpcoesSituacaoPropostaCombo(
			List<SelectItem> opcoesSituacaoPropostaCombo) {
		this.opcoesSituacaoPropostaCombo = opcoesSituacaoPropostaCombo;
	}

	public boolean isVisibleMessage() {
		return visibleMessage;
	}

	public void setVisibleMessage(boolean visibleMessage) {
		this.visibleMessage = visibleMessage;
	}
	
	
	public boolean isMostrarClose() {
		return mostrarClose;
	}

	public void setMostrarClose(boolean mostrarClose) {
		this.mostrarClose = mostrarClose;
	}

	//Retorna a data atual para a tela de relat�rios
	public Date getDataAtualRelatorio() {
		return new Date();
	}
	
	/**
	 * Popula um combo de Empresa
	 */
	public void populaOpcoesMesesCombo(boolean opcaoVazia) {
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesMesesCombo = new ArrayList<SelectItem>();
		if(opcaoVazia) {
			opcoesMesesCombo.add( new SelectItem( null, "" ) );
		}
		opcoesMesesCombo.add( new SelectItem( 1, "Janeiro"));
		opcoesMesesCombo.add( new SelectItem( 2, "Fevereiro"));
		opcoesMesesCombo.add( new SelectItem( 3, "Mar�o"));
		opcoesMesesCombo.add( new SelectItem( 4, "Abril"));
		opcoesMesesCombo.add( new SelectItem( 5, "Maio"));
		opcoesMesesCombo.add( new SelectItem( 6, "Junho"));
		opcoesMesesCombo.add( new SelectItem( 7, "Julho"));
		opcoesMesesCombo.add( new SelectItem( 8, "Agosto"));
		opcoesMesesCombo.add( new SelectItem( 9, "Setembro"));
		opcoesMesesCombo.add( new SelectItem( 10, "Outubro"));
		opcoesMesesCombo.add( new SelectItem( 11, "Novembro"));
		opcoesMesesCombo.add( new SelectItem( 12, "Dezembro"));
		
	}
	
	/**
	 * Popula um combo de Empresa
	 */
	public void populaOpcoesEmpresaCombo(boolean opcaoVazia) {
		
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO = new EmpresaDAO(session);
		
		List<Empresa> empresas = empresaDAO.buscaEmpresas();
		
		//Realiza as a��es de banco: Busca e seta os valores
		opcoesEmpresaCombo = new ArrayList<SelectItem>();
		if(opcaoVazia) {
			opcoesEmpresaCombo.add( new SelectItem( null, "--Todas--" ) );
		}
		for (Empresa empresa : empresas) {
			opcoesEmpresaCombo.add( new SelectItem( empresa.getId(), empresa.getNomeEmpresa()));
		}
	}
	
	/**
	 * Popula um combo de situa��es da proposta.
	 *  
	 */
	public void populaOpcoesSituacaoPropostaCombo(){
		opcoesSituacaoPropostaCombo = new ArrayList<SelectItem>();
		for(SituacaoProposta situacao : SituacaoProposta.values()) {
			opcoesSituacaoPropostaCombo.add( new SelectItem( situacao.getId(), situacao.getNome()));
		}
	}
	
	public void populaOpcoesSituacaoBoletoCombo(){
		opcoesSituacaoBoletoCombo = new ArrayList<SelectItem>();
		opcoesSituacaoBoletoCombo.add( new SelectItem( null, "--Nenhum--"));
		for(SituacaoBoleto situacao : SituacaoBoleto.values()) {
			opcoesSituacaoBoletoCombo.add( new SelectItem( situacao.getId(), situacao.getNome()));
		}
	}

	/**
	  * Adiciona a(s) mensagem(ns) no Context para ser exibida ao usu�rio
	  * @param message
	  */
	public static void addGlobalMessage(String message) {  
	    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, message, message);  
	    FacesContext.getCurrentInstance().addMessage(null, facesMessage);   
	} 
	
	/**
	  * Adiciona a(s) mensagem(ns) no Context para ser exibida ao usu�rio
	  * @param message
	  */
	public static void addWarnMessage(String message) {  
	    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, message, message);  
	    FacesContext.getCurrentInstance().addMessage(null, facesMessage);   
	} 
	
	 /**
	  * Adiciona a(s) mensagem(ns) no Context para ser exibida ao usu�rio
	  * @param message
	  */
	public static void addErrorMessage(String message) {  
	    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);  
	    FacesContext.getCurrentInstance().addMessage(null, facesMessage);   
	} 
	
	/**
	 * M�todo Auxiliar para validar obrigatoriedade
	 * @param component
	 * @param params
	 */
	public static boolean validaObrigatorios( ValueHolder component, String campo ){
		if( component.getValue() == null || component.getValue().equals("") || component.getValue().toString().equals("0") ){
			String key = Constantes.KEY_VALIDACAO_CAMPO_OBRIGATORIO;
			ArrayList<String> array = new ArrayList<String>();
			array.add(campo);
			String message = JSFUtils.getMessageResourceString("Messages", key, array.toArray());
			addErrorMessage(message);
			return false;
		}
		return true;
	}
	
	/**
	 * M�todo Auxiliar para validar obrigatoriedade de preenchimento pela popup
	 * @param component
	 * @param params
	 */
	public static boolean validaPreenchimentoObrigatorioListaSelecao( Object item, String campo ){
		if( item == null || item.equals("") || item.toString().equals("0") ){
			String key = Constantes.KEY_VALIDACAO_PREENCHIMENTO_LISTA_SELECAO;
			ArrayList<String> array = new ArrayList<String>();
			array.add(campo);
			String message = JSFUtils.getMessageResourceString("Messages", key, array.toArray());
			addErrorMessage(message);
			return false;
		}
		return true;
	}
	
	/**
	 * M�todo Auxiliar para validar obrigatoriedade
	 * @param component
	 * @param params
	 */
	public static boolean validaMaxLength( ValueHolder component, String campo, int tamanhoMaximo ){
		if( component.getValue() != null ){
			if( component.getValue().toString().length() > tamanhoMaximo ){
				String key = Constantes.KEY_VALIDACAO_CAMPO_MAXLENGTH;		
				ArrayList<String> array = new ArrayList<String>();
				array.add(campo);
				array.add( new Integer(tamanhoMaximo).toString() );
				String message = JSFUtils.getMessageResourceString("Messages", key, array.toArray());
				addErrorMessage(message);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * M�todo que valida obrigatoriedade em um mapa <idCampo> <nomeCampo> de campos de um formul�rio
	 * @param campos
	 * @param e
	 * @return
	 */
	public boolean validaObrigatoriedadeFormulario( HashMap<String,String> campos, ActionEvent e ){
		UIComponent link = e.getComponent();
		
		boolean retorno = true;
		for (Iterator iterator = campos.keySet().iterator(); iterator.hasNext();) {
			String componente = (String) iterator.next();
			ValueHolder value = (ValueHolder) link.findComponent(componente);
			if( !validaObrigatorios( value, campos.get(componente) ) ){
				retorno = false;
			}
		}					
		return retorno;
		
	}
	
	/**
	 * M�todo Auxiliar para validar obrigatoriedade
	 * @param component
	 * @param params
	 */
	public static boolean validaInclusaoObrigatoria( Collection lista, String campo ){
		if( lista == null || lista.size() == 0 ){
				String key = Constantes.KEY_VALIDACAO_INCLUSAO_OBRIGATORIA;		
				ArrayList<String> array = new ArrayList<String>();
				array.add(campo);
				String message = JSFUtils.getMessageResourceString("Messages", key, array.toArray());
				addErrorMessage(message);
				return false;
		}
		return true;
	}	

	/**
	 * M�todo Auxiliar para valida gen�rico
	 * @param params
	 */
	public static void validaGenerico( String key, ArrayList<String> array ){
			String message = JSFUtils.getMessageResourceString("Messages", key, array.toArray());
			addErrorMessage(message);
	}	
	
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getSimNaoCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "S", "Sim" ) );
		retorno.add( new SelectItem( "N", "N�o" ) );
		
		return retorno;
	}	
	
	public List<SelectItem> getSimNaoComboInteiro(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		
		retorno.add( new SelectItem( 1, "Sim" ) );
		retorno.add( new SelectItem( 0, "N�o" ) );
		
		return retorno;
	}
	
	public List<SelectItem> getSimNaoComboBoolean(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add( new SelectItem( false, "N�o" ) );
		retorno.add( new SelectItem( true, "Sim" ) );
		
		
		return retorno;
	}
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getSexoCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "F", "Feminino" ) );
		retorno.add( new SelectItem( "M", "Masculino" ) );
		
		return retorno;
	}
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getSeguradoraCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "T", "TOKIO MARINE" ) );
		retorno.add( new SelectItem( "M", "METLIFE" ) );
		retorno.add( new SelectItem( "I", "ITA� SEGUROS" ) );
		retorno.add( new SelectItem( "O", "ICATU SEGUROS" ) );
		
		return retorno;
	}
	
	public String getSeguradora(String codigo) {
		Map<String, String> mapa = new HashMap<String, String>();
		mapa.put("T", "TOKIO MARINE");
		mapa.put("M", "METLIFE");
		mapa.put("I", "ITA� SEGUROS");
		mapa.put("O", "ICATU SEGUROS");
		
		return mapa.get(codigo);
	}
	
	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getEstadoCivilCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "CA", "Casado" ) );
		retorno.add( new SelectItem( "CO", "Companheiro" ) );
		retorno.add( new SelectItem( "DE", "Desquitado" ) );
		retorno.add( new SelectItem( "DI", "Divorciado" ) );
		retorno.add( new SelectItem( "OT", "Outros" ) );
		retorno.add( new SelectItem( "SE", "Separado" ) );
		retorno.add( new SelectItem( "SO", "Solteiro" ) );
		retorno.add( new SelectItem( "VI", "Vi�vo" ) );		
		
		return retorno;
	}	
	
	/**
	 * Monta um mapa para tipos de funcionarios
	 * @return
	 */
	public Map<String,String> montaMapaTipoFuncionario(){
		Map<String,String>  mapa = new HashMap<String, String>();
		mapa.put("T", "Agenciador Aut�nomo");
		mapa.put("F", "Agenciador Funcion�rio");
		mapa.put("A", "Funcion�rio Administrativo");
		
		return mapa;		
	}
	
	/**
	 * Monta um mapa para tipos de funcionarios
	 * @return
	 */
	public Map<String,String> montaMapaTipoOrgao(){
		Map<String,String>  mapa = new HashMap<String, String>();
		mapa.put("E", "Estadual");
		mapa.put("F", "Federal");
		mapa.put("M", "Municipal");
		mapa.put("P", "Privado");
		
		return mapa;		
	}
	
	/**
	 * Popula um combo com valores dos Modelos de Proposta
	 * @return
	 */
	public List<SelectItem> getModeloPropostaCombo(){
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		session.clear();
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		
		Dao<ModeloProposta, Integer> dao = new Dao<ModeloProposta, Integer>(session, ModeloProposta.class);
		List listaModelosAll =  dao.list();
		
		retorno.add(new SelectItem("", "Selecione:"));
		
		for (Iterator iterator = listaModelosAll.iterator(); iterator.hasNext();) {
			ModeloProposta modeloProposta = (ModeloProposta) iterator.next();
			retorno.add( new SelectItem( modeloProposta.getId(), modeloProposta.getDescricao() ) );
		}		
		
		return retorno;
	}	
	
	public void postProcessXLS(Object document) {  
	    HSSFWorkbook wb = (HSSFWorkbook) document;  
	    HSSFSheet sheet = wb.getSheetAt(0);  
	    HSSFRow header = sheet.getRow(0);  
		   
	    HSSFCellStyle cellStyle = wb.createCellStyle();    
	    cellStyle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);  
	    cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
	    cellStyle.setWrapText(false);
	    
	    for(int i=0; i < header.getPhysicalNumberOfCells();i++) {  
	        HSSFCell cell = header.getCell(i);  
	        if( cell != null ){
	        	cell.setCellStyle(cellStyle);	        	
	        }else{
	        	header.createCell(i);
	        	cell = header.getCell(i);  
	        	cell.setCellStyle(cellStyle);
	        }
	    }  
	    
        short formatoMonetario = wb.createDataFormat().getFormat("R$ #,##0.00;[Red]R$ #,##0.00");
		
        HSSFCellStyle estiloMonetario = wb.createCellStyle();
        estiloMonetario.setDataFormat( formatoMonetario );
	    
	    for( int j=0; j<=sheet.getLastRowNum();j++ ){
	    	 HSSFRow linha = sheet.getRow(j);
	 	    for(int k=0; k < linha.getPhysicalNumberOfCells();k++) {  
		        HSSFCell cell = linha.getCell(k);
		        if( cell != null ){
		        	if( cell.getCellType() == HSSFCell.CELL_TYPE_STRING ){
			        	HSSFRichTextString cellText = cell.getRichStringCellValue();
			        	String value = cellText.getString();
			        	if( value.startsWith("R$") ){
			        		value = value.replace("R$", "");
			        		value = value.replace("." , "");
			        		value = value.replace(",", ".");
			        		cell.setCellValue(new Double( value ));
			        		cell.setCellStyle( estiloMonetario );	   
			        	}else{	        		
			        		value = value.replace("/sisvida/propostaVisualizar.jsf?propostaVisualizar=", "");
			        		HSSFRichTextString propostaCellValue = new HSSFRichTextString( value );
			        		cell.setCellValue( propostaCellValue );
			        	}
			        	
			        	
		        	}
		        	     	
		        }
	 	    }
	    }
	    
	    for(short column=0;column<header.getLastCellNum(); column++){
	    	sheet.autoSizeColumn(column);
	    }
	}  
	  
	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {  
	    Document pdf = (Document) document;  
	    ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();  
	    String logo = servletContext.getRealPath("") + File.separator + "images" + File.separator + "logoRelatorio.png";  
	    pdf.setPageSize( new Rectangle( 800, 80 ) )  ;	    
	    pdf.add(Image.getInstance(logo));  
	} 
	
	public List<String> getSituacaoPropostaSelectedList() {
		return situacaoPropostaSelectedList;
	}

	public void setSituacaoPropostaSelectedList(
			List<String> situacaoPropostaSelectedList) {
		this.situacaoPropostaSelectedList = situacaoPropostaSelectedList;
	}

	public List<String> getEmpresaSelectedList() {
		return empresaSelectedList;
	}

	public void setEmpresaSelectedList(List<String> empresaSelectedList) {
		this.empresaSelectedList = empresaSelectedList;
	}

	public void postProcessPDF(Object document) throws IOException, BadElementException, DocumentException {  
	    Document pdf = (Document) document;  
	    pdf.setPageSize( new Rectangle( 800, 80 ) )  ;
	    
	} 
	
	
	/**
	 * M�todo para validar CNPJ E CPF
	 * @param CNPJ_CPF
	 * @return
	 */
	public static boolean validarCNPJCPF( String CNPJ_CPF) {

		if("".equals( CNPJ_CPF ) || CNPJ_CPF == null ){
			return true;
		}
		
		CNPJ_CPF = CNPJ_CPF.replace(".", "").replace("-", "").replace("/", "").replace("_", "");
		
        if(CNPJ_CPF.length() == 11) { //CPF
        		Cpf cpf = null;
				try {
					cpf = new Cpf(CNPJ_CPF);
				} catch (ValidationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
                boolean retorno = cpf.isValid();
                if( !retorno ){
                	addErrorMessage("O CPF digitado � inv�lido!");
                }
                
                return retorno;
        }
        else if(CNPJ_CPF.length() == 14) { //CNPJ
               Cnpj cnpj = null;
			try {
				cnpj = new Cnpj(CNPJ_CPF);
			} catch (ValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
                boolean retorno =  cnpj.isValid( );
                if( !retorno ){
                	addErrorMessage("O CNPJ digitado � inv�lido!");
                }
                return retorno;
        }
        
        addErrorMessage("O CPF/CNPJ digitado � inv�lido!");
        
        return false;
	}
	
	public void verificaCPF(ActionEvent e){
		//Recupera o CPF
		UIComponent link = e.getComponent();
		ValueHolder value = (ValueHolder) link.findComponent("txtCpf");
		if( value == null ){
			value = (ValueHolder) link.findComponent("txtCPFConjuge");
		}
		String cpf = value.getValue().toString();
		visibleMessage = !validarCNPJCPF( cpf );

	}
	
	public void verificaCPFDuplicados (ActionEvent e){
		//Recupera o CPF
			UIComponent link = e.getComponent();
			ValueHolder value = (ValueHolder) link.findComponent("txtCpf");
			
			verificaCPFDuplicado(value);
			
			
	}
	
	public void verificaCPFDuplicadosConjuge (ActionEvent e){
		//Recupera o CPF
			UIComponent link = e.getComponent();
			ValueHolder value = (ValueHolder) link.findComponent("txtCPFConjuge");
			
			verificaCPFDuplicado(value);
			
			
	}

	private void verificaCPFDuplicado(ValueHolder value) {
		String cpf = value.getValue().toString();
		if( !StringUtils.isEmpty( cpf ) && !StringUtils.isEmpty( cpf.replace(".", "").replace("-", "").replace("/", "").replace("_", "") ) ){
			
			visibleMessage = !validarCNPJCPF( cpf );
			mostrarClose = visibleMessage;
			//Busca por cpfs existentes
			Session session = HibernateUtil.currentSession();
			PropostaDAO dao = new PropostaDAO( session );
			Proposta propostaFiltro = new Proposta();
			propostaFiltro.setCpfSegurado( cpf );
			propostaFiltro.setCpfSeguradoFormatado( cpf.replace(".", "").replace("-", "").replace("/", "").replace("_", "") );
			Integer count = dao.countPropostasCPFDuplicidade(propostaFiltro);
			
			//Verifica tamb�m conjuges
			propostaFiltro = new Proposta();
			propostaFiltro.setCpfConjuge( cpf );
			propostaFiltro.setCpfConjugeFormatado( cpf.replace(".", "").replace("-", "").replace("/", "").replace("_", "") );
			count += dao.countPropostasCPFDuplicidade( propostaFiltro );
			if( count != null &&count > 0 ){
				addErrorMessage("Existe Segurado Principal ou Segurado C�njuge com esse mesmo cpf cadastrado na base de dados. <br/> Favor verificar atrav�s da funcionalidade de Relat�rio de Segurados. <br/> A a��o foi bloqueada! Clique no Bot�o 'Voltar' logo abaixo.");
				visibleMessage = true;
				mostrarClose = false;
			}
		}else{
			visibleMessage = false;
		}
	}
	
	public void verificaCNPJ(ActionEvent e){
		//Recupera o CPF
		UIComponent link = e.getComponent();
		ValueHolder value = (ValueHolder) link.findComponent("txtCgc");
		String cgc = value.getValue().toString();
		visibleMessage = !validarCNPJCPF( cgc );
	}
	
	/**
	 * Autocomplete para cidades
	 * @param nome
	 * @return
	 */
	public List<Cidade> cidadeAutocomplete( Object nome ) {
		Session session = HibernateUtil.currentSession();
		CidadeDAO dao = new CidadeDAO(session);	
		List<Cidade> lista = dao.buscaPeloNome( (String) nome, null, Constantes.MAX_REGISTROS_RETORNADOS  );
		
		return lista;
	}
	
	/**
	 * Autocomplete para Nomes de Tabelas
	 * @param nome
	 * @return
	 */
	public List<NomeTabela> nomeTabelaAutocomplete( Object nome ) {
	
		Session session = HibernateUtil.currentSession();
		NomeTabelaDAO dao = new NomeTabelaDAO(session);	
		List<NomeTabela> lista = dao.busca( (String) nome, null);
		return lista;
		
	}
	
	/**
	 * Autocomplete para �rg�os
	 * @param nome
	 * @return
	 */
	public List<Orgao> orgaoAutoComplete( Object nome ){
		Session session = HibernateUtil.currentSession();
		OrgaoDAO dao = new OrgaoDAO(session);	
		List<Orgao> lista = dao.findOrgaoPopup( (String) nome );
		return lista;
	}
	
	

	/**
	 * Autocomplete para Funcion�rios
	 * @param nome
	 * @return
	 */
	public List<Funcionario> funcionarioAutoComplete( Object nome ){
		
		Session session = HibernateUtil.currentSession();
		FuncionarioDAO dao = new FuncionarioDAO(session);
		
		List<Funcionario> lista = dao.findFuncionarioPopup( (String) nome );
		return lista;
	}
	
	
	public List<Proposta> SeguaradoAutoComplete( Object nome ){
		
		Session session = HibernateUtil.currentSession();
		PropostaDAO dao = new PropostaDAO(session);		
		List<Proposta> lista = dao.findPropostaPopup( (String) nome );
		return lista;
	}
	
	public List<Proposta> SeguaradoSuperApAutoComplete( Object nome ){
		
		Session session = HibernateUtil.currentSession();
		PropostaDAO dao = new PropostaDAO(session);		
		List<Proposta> lista = dao.findPropostaSuperAp( (String) nome );
		return lista;
	}
	
	
	

	/**
	 * Autocomplete para Grau de Parentesco
	 * @param descricao
	 * @return
	 */
	public List<GrauParentesco> grauParentescoAutoComplete( Object descricao ){
		Session session = HibernateUtil.currentSession();
		GrauParentescoDAO dao = new GrauParentescoDAO(session);	
		List<GrauParentesco> lista = dao.buscaPorNomePopUp(  (String) descricao );
		return lista;
	}
	
	/**
	 * Autocomplete para Motivo Sinistro
	 * @param descricao
	 * @return
	 */
	public List<TipoOcorrencia> motivoSinistroAutoComplete( Object descricao ){
		Session session = HibernateUtil.currentSession();
		TipoOcorrenciaDAO dao = new TipoOcorrenciaDAO(session);	
						
		List<TipoOcorrencia> lista = dao.busca( (String) descricao , Constantes.NAO  );
		return lista;
	}
	
	/**
	 * Autocomplete para Motivo Sinistro Negado
	 * @param descricao
	 * @return
	 */
	public List<TipoOcorrencia> motivoSinistroNegadoAutoComplete( Object descricao){
		Session session = HibernateUtil.currentSession();
		TipoOcorrenciaDAO dao = new TipoOcorrenciaDAO(session);	
						
		List<TipoOcorrencia> lista = dao.busca( (String) descricao, Constantes.SIM  );
		return lista;
	}
	
	/**
	 * Autocomplete para Depesa
	 * @param descricao
	 * @return
	 */
	public List<Despesa> despesaAutoComplete( Object nome ){
		Session session = HibernateUtil.currentSession();
		DespesaDAO  despesaDAO = new DespesaDAO(session);
		List<Despesa> resultado = despesaDAO.busca( (String) nome, null );
		return resultado;
	}
	
	/**
	 * Utilizado na popup de Despesa
	 * @param e
	 */
	public List<GrupoDespesa> grupoDespesaAutoComplete( Object nome ){
		Session session = HibernateUtil.currentSession();
		GrupoDespesaDAO  grupoDespesaDAO = new GrupoDespesaDAO(session);
		List<GrupoDespesa> resultado = grupoDespesaDAO.busca( (String) nome );
		return resultado;
	}
	
	/**
	 * Utilizado na popup de Filial
	 * @param e
	 */
	public List<Filial> filialAutoComplete( Object nome ){
		Session session = HibernateUtil.currentSession();
		FilialDAO filialDao = new FilialDAO(session);
		List<Filial> resultado = filialDao.buscaPorNomePopUp( (String) nome );
		return resultado;
	}

	public Map<String, String> getMapaCPF() {
		return mapaCPF;
	}

	public void setMapaCPF(Map<String, String> mapaCPF) {
		this.mapaCPF = mapaCPF;
	}

	public List<SelectItem> getOpcoesMesesCombo() {
		return opcoesMesesCombo;
	}

	public void setOpcoesMesesCombo(List<SelectItem> opcoesMesesCombo) {
		this.opcoesMesesCombo = opcoesMesesCombo;
	}

	public List<SelectItem> getOpcoesSituacaoBoletoCombo() {
		return opcoesSituacaoBoletoCombo;
	}

	public void setOpcoesSituacaoBoletoCombo(List<SelectItem> opcoesSituacaoBoletoCombo) {
		this.opcoesSituacaoBoletoCombo = opcoesSituacaoBoletoCombo;
	}
	
	

	
	
	
}
