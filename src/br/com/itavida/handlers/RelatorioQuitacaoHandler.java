package br.com.itavida.handlers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.Session;

import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.dao.QuitacaoDAO;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.handlers.base.ExcelGenerateHandler;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;

public class RelatorioQuitacaoHandler extends ExcelGenerateHandler {

	private static final long serialVersionUID = 1L;
		
	private Map<String, List<Proposta>> mapaPropostas = new HashMap<String, List<Proposta>>();

	static final int INDEX_LINHA_CABECALHO = 4;
	
	static final int QUANTIDADE_COLUNAS_DE_CADA_EMPRESA = 4;
	
	List<Empresa> empresas = new ArrayList<Empresa>();
	
	private Date dataReferencia;
	
	@PostConstruct
	public void init() {
		
	}
	
	public void gerarRelatorio(Map<String, List<Proposta>> mapaPropostasLocal) {
		mapaPropostas = mapaPropostasLocal;
		super.gerarRelatorio(mapaPropostas.keySet(), INDEX_LINHA_CABECALHO, "Relat�rio de Cartonagem");
	}
		
	public void gerarRelatorio() {
		mapaPropostas = new HashMap<String, List<Proposta>>();
		mapaPropostas.put("PLANILHA DE QUITA��O", new ArrayList<Proposta>());
		super.gerarRelatorio(mapaPropostas.keySet(), INDEX_LINHA_CABECALHO, "Relat�rio de Quita��o");
		
	}
	

	@Override
	public void criarCabecalho(HSSFWorkbook wb, String aba) {
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO = new EmpresaDAO(session);
		
		addRow(aba);
		addCelulaTexto(aba, getStiloTextoCabecalho(), "");
		empresas = empresaDAO.buscaEmpresas();
		int indexEmpresaInicial = 1;
		for(Empresa empresa : empresas) {
			addTextoMesclado(aba, getStiloTextoTitulo(), empresa.getNomeEmpresa(), indexEmpresaInicial, 1, 0, QUANTIDADE_COLUNAS_DE_CADA_EMPRESA);
			indexEmpresaInicial += QUANTIDADE_COLUNAS_DE_CADA_EMPRESA;
		}
		
		addRow(aba);
		addCelulaTexto(aba, getStiloTextoCabecalho(), "M�s");
		
		for(Empresa empresa : empresas) {
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Valor do M�s Recebido");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Valor em Atraso Recebido");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Valor Total Recebido");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Vidas");
			addCelulaTexto(aba, getStiloTextoCabecalho(), "Ticket M�dio");	
		}
		addCelulaTexto(aba, getStiloTextoCabecalho(), "Total Geral");
	}

	
	@Override
	public void preencherAba(HSSFWorkbook wb, String aba) {
		Session session = HibernateUtil.currentSession();
		QuitacaoDAO quitacaoDAO = new QuitacaoDAO(session);
		
		Calendar primeiraDataAvaliada = Calendar.getInstance();
		if (dataReferencia != null) {
			primeiraDataAvaliada.setTime(dataReferencia);
		}
		DateUtil.zerarDiaHoraMinutoSegundo(primeiraDataAvaliada);
		
		Calendar ultimaDataAvaliada = (Calendar) primeiraDataAvaliada.clone();
		ultimaDataAvaliada.add(Calendar.MONTH,-Constantes.QUANTIDADE_MES_AVALIADOS);
		
		while (primeiraDataAvaliada.after(ultimaDataAvaliada)) {
			Double valorTotal = 0.0;
			addRow(aba);	
			addCelulaData(aba, getStiloTextoSimples(),  primeiraDataAvaliada.getTime(), "");
			
			for(Empresa empresa : empresas) {
				Double valorMesAtual = quitacaoDAO.getValorMesAtualPorDataEEmpresa(primeiraDataAvaliada.getTime(), empresa);
				Double valor = quitacaoDAO.getValorPorDataEEmpresa(primeiraDataAvaliada.getTime(), dataReferencia, empresa);
				Double valorMesesAnteriores = valor - valorMesAtual;
				Double vidas = quitacaoDAO.getVidasPorDataEEmpresa(primeiraDataAvaliada.getTime(), empresa);
				valorTotal += valor;
				
				
				addCelulaNumero(aba, getStiloTextoSimples(), valorMesAtual);
				addCelulaNumero(aba, getStiloTextoSimples(), valorMesesAnteriores);
				addCelulaNumero(aba, getStiloTextoSimples(), valor);
				addCelulaNumero(aba, getStiloTextoSimples(), vidas);				
				addCelulaNumero(aba, getStiloTextoSimples(), getValorMedio(valor, vidas));
			}
			
			addCelulaNumero(aba, getStiloTextoSimples(), valorTotal);
			primeiraDataAvaliada.add(Calendar.MONTH,-1 );
		}

	}

	private Double getValorMedio(Double valor, Double vidas) {
		Double ticketMedio = 0.0;
		if (vidas != 0) {
			ticketMedio = valor / vidas;
		}
		return ticketMedio;
	}

	public Date getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(Date dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	
}