package br.com.itavida.handlers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

import org.hibernate.Session;

import br.com.itavida.dao.ArquivoMensalDAO;
import br.com.itavida.dao.MovimentacaoOrgaoAtrasadoDAO;
import br.com.itavida.dao.MovimentacaoOrgaoInadimplenteDAO;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.entidades.MovimentacaoOrgaoAtrasado;
import br.com.itavida.entidades.MovimentacaoOrgaoInadimplente;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.exceptions.SistemaException;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class MovimentacaoOrgaoHandler extends MainHandler{

	private String periodo;
	private boolean avancou = false;
	private Orgao orgao;
	private Orgao orgaoAnterior;
	private String periodoQuitacaoAnterior;
	
	
	
	public MovimentacaoOrgaoHandler(){
		orgao = new Orgao();	
		orgaoAnterior = new Orgao();
	}
	
	public Date getReferencia() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = format.parse("01/" + periodo);
		} catch (ParseException e1) {
			//faz nada
		}
		return date;
	}
	
	public Date getDataPeriodoQuitacaoAnterior() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = format.parse("01/" + periodoQuitacaoAnterior);
		} catch (ParseException e1) {
			//faz nada
		}
		return date;	
	}
	
	
	public List<MovimentacaoOrgaoInadimplente> getMovimentacoes() {
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoOrgaoInadimplenteDAO inadimplenteDAO = new MovimentacaoOrgaoInadimplenteDAO(currentSession);
		return inadimplenteDAO.buscarMovimentacoes(getReferencia());
	}
	
	
	public Double calculaPremio(Orgao org){
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoOrgaoInadimplenteDAO movOrgInaDAO = new MovimentacaoOrgaoInadimplenteDAO(currentSession);
		PropostaDAO propostaDAO = new PropostaDAO(currentSession);
		Double somatorio = 0.0;  //TODO Verificar quest�es de arredondamento.					
		List <Proposta> listProposta = movOrgInaDAO.buscaPropostasOrgao(org);			
		// soma os valores das propostas para o orgao selecionado pelo usuario.
		for(Proposta proposta : listProposta){
			//antes de acessar valor de pr�mio ou capital de uma proposta � necess�rio chamar este m�todo
			propostaDAO.populaCapital(proposta);
			somatorio += (proposta.getPremioSegurado() != null ? proposta.getPremioSegurado() : 0.0) + (proposta.getPremioConjuge() != null ? proposta.getPremioConjuge(): 0.0);			
		}
		return somatorio;	
	}
	
	
	public List<MovimentacaoOrgaoAtrasado> getMovimentacoesOrgaosAtrasados() {
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoOrgaoAtrasadoDAO dao = new MovimentacaoOrgaoAtrasadoDAO(currentSession);
		return dao.buscarMovimentacoes(getReferencia());
	}
	
	
			
	public void incluirInadimplente( ActionEvent e ){
		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoOrgaoInadimplenteDAO movOrgInaDAO = new MovimentacaoOrgaoInadimplenteDAO(currentSession);		
		Double somatorio = 0.0;  //TODO Verificar quest�es de arredondamento.
		somatorio = calculaPremio(orgao);
		try {			
			//salva a movimenta��o
			MovimentacaoOrgaoInadimplente orgInad = new MovimentacaoOrgaoInadimplente();
			orgInad.setOrgao(orgao);
			orgInad.setPremio(somatorio);
			orgInad.setReferencia(getReferencia());
			movOrgInaDAO.saveOrUpdate(orgInad);
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );		
		}catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}		
	}
	
	
public void incluirQuitacaoAnterior( ActionEvent e ){		
		try {
			Double somatorio = 0.0;  //TODO Verificar quest�es de arredondamento.
			somatorio = calculaPremio(orgaoAnterior);
			Session currentSession = HibernateUtil.currentSession();
			ArquivoMensalDAO arquivoMensalDao= new ArquivoMensalDAO(currentSession);
			MovimentacaoOrgaoAtrasadoDAO dao = new MovimentacaoOrgaoAtrasadoDAO(currentSession);			
			MovimentacaoOrgaoAtrasado novoOrgaoAtrasado = new MovimentacaoOrgaoAtrasado();
			novoOrgaoAtrasado.setOrgao(orgaoAnterior);
			novoOrgaoAtrasado.setPremio(somatorio);
			novoOrgaoAtrasado.setReferencia(getReferencia());
			novoOrgaoAtrasado.setPeriodoQuitacaoAnterior(getDataPeriodoQuitacaoAnterior());
			dao.saveOrUpdate(novoOrgaoAtrasado);			
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
			orgao = new Orgao();
		} catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}		
	}
	
	public void excluirInadimplente(ActionEvent e) {

		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoOrgaoInadimplenteDAO inadimplenteDAO = new MovimentacaoOrgaoInadimplenteDAO(currentSession);

		// Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirInadimplente");
		MovimentacaoOrgaoInadimplente movimentacao = (MovimentacaoOrgaoInadimplente) param.getValue();
		try {
			inadimplenteDAO.delete(movimentacao);
			String message = JSFUtils.getMessageResourceString("Messages",	Constantes.KEY_MENSAGEM_EXCLUSAO_SUCESSO, null);
			addGlobalMessage(message);
		} catch (SistemaException se) {
			addErrorMessage(se.getMessage());
		}

	}

	
	public void excluirQuitacaoAnterior(ActionEvent e) {

		Session currentSession = HibernateUtil.currentSession();
		MovimentacaoOrgaoAtrasadoDAO atrasadoDAO = new MovimentacaoOrgaoAtrasadoDAO(currentSession);

		// Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirQA");
		MovimentacaoOrgaoAtrasado movimentacao = (MovimentacaoOrgaoAtrasado) param.getValue();

		atrasadoDAO.delete(movimentacao);

		String message = JSFUtils.getMessageResourceString("Messages",	Constantes.KEY_MENSAGEM_EXCLUSAO_SUCESSO, null);
		addGlobalMessage(message);

	}
	
	
	
	
	
	
	public void avancar( ActionEvent e ){
		avancou = true;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public boolean isAvancou() {
		return avancou;
	}

	public void setAvancou(boolean avancou) {
		this.avancou = avancou;
	}
	public Orgao getOrgao() {
		return orgao;
	}
	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public Orgao getOrgaoAnterior() {
		return orgaoAnterior;
	}

	public void setOrgaoAnterior(Orgao orgaoAnterior) {
		this.orgaoAnterior = orgaoAnterior;
	}

	public String getPeriodoQuitacaoAnterior() {
		return periodoQuitacaoAnterior;
	}

	public void setPeriodoQuitacaoAnterior(String periodoQuitacaoAnterior) {
		this.periodoQuitacaoAnterior = periodoQuitacaoAnterior;
	}
	
	
	
	
}
