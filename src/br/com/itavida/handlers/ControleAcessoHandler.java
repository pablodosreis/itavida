package br.com.itavida.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.UsuarioDAO;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.util.Carteiro;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.HibernateUtil;
import br.com.itavida.util.JSFUtils;

public class ControleAcessoHandler extends MainHandler implements Serializable{
	
	/**
	 * Default Serial ID
	 */
	private static final long serialVersionUID = 1L;
	
	private Usuario usuario;
	private List<Usuario> resultadoPesquisa;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public ControleAcessoHandler() {
		setUsuario( new Usuario() );
		//new Carteiro().enviar("Sisvida", "pablodosreis@gmail.com", "Iniciou controle acesso", "Abriu a pagina");
	}
	
	public List<Usuario> getResultadoPesquisa() {
		return resultadoPesquisa;
	}

	public void setResultadoPesquisa(List<Usuario> resultadoPesquisa) {
		this.resultadoPesquisa = resultadoPesquisa;
	}

	/**
	 * Grava uma altera��o ou uma inclus�o
	 * @param e
	 */
	public void gravar(ActionEvent e){
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();
		Dao<Usuario, Integer> dao = new Dao<Usuario, Integer>(session, Usuario.class);
		
		//Realiza as valida��es necess�rias antes de gravar a proposta no banco
		boolean passou = validarUsuario( e );

		if( passou ){
			dao.merge( usuario );
			cancelar( e );
			String message = JSFUtils.getMessageResourceString("Messages", Constantes.KEY_MENSAGEM_SUCESSO, null);
			addGlobalMessage( message );
		}
		
	}
	
	/**
	 * Realiza a pesquisa utilizando clausulas like
	 * @param e
	 */
	public void pesquisaControlesAcesso(ActionEvent e){
		
		Session session = HibernateUtil.currentSession();
		UsuarioDAO dao = new UsuarioDAO(session);	
		
		List<Usuario> lista = dao.buscaTodosUsuarios();
		setResultadoPesquisa(lista);
			
	}

	/**
	 * Popula um combo com sim e nao
	 * @return
	 */
	public List<SelectItem> getPerfilAcessoCombo(){
		
		List<SelectItem> retorno = new ArrayList<SelectItem>();
		retorno.add(new SelectItem("", "Selecione:"));
		retorno.add( new SelectItem( "ADM", "Administrador" ) );
		retorno.add( new SelectItem( "CAD", "Cadastro" ) );
		retorno.add( new SelectItem( "COM", "Comercial" ) );
		retorno.add( new SelectItem( "FAT", "Faturamento" ) );
		retorno.add( new SelectItem( "FIN", "Financeiro" ) );
		retorno.add( new SelectItem( "SIN", "Sinistro" ) );
		retorno.add( new SelectItem( "FIL", "Filial" ) );
		return retorno;
	}
	
	private boolean validarUsuario(ActionEvent e) {
		
		HashMap<String, String> mapaCampos = new HashMap<String, String>();
		mapaCampos.put("txtNomeUsuario", "Nome do Usu�rio");
		mapaCampos.put("txtIdUsuario", "Identifica��o");
		mapaCampos.put("txtSenhaUsuario", "Senha do Usu�rio");
		mapaCampos.put("txtPerfilAcesso", "Perfil de Acesso");
		
		boolean retorno = validaObrigatoriedadeFormulario(mapaCampos, e);
		
		
		return retorno;
	}
	
	/**
	 * Cancela a a��o
	 * @param e
	 */
	public void cancelar( ActionEvent e ){
		
		// Realiza o cancelamento das a��es
		setUsuario( new Usuario() );
		this.resultadoPesquisa = null;
		
	}
	
	/**
	 * Prepara altera��o, setando os dados no formul�rio
	 * @param e
	 */
	public String preparaAlterarUsuario( ){
		
		//Recupera o item escolhido	
		resultadoPesquisa = null;
		
		return "incluirUsuario";
	}
	
	/**
	 * Realiza a exclus�o de uma cidade
	 * @param e
	 */
	public void excluirUsuario(ActionEvent e){
		
		//Recupera o item escolhido
		UIComponent link = e.getComponent();
		UIParameter param = (UIParameter) link.findComponent("excluirUsuario");
		Usuario usuarioDeletado = ( Usuario ) param.getValue();
		
		//Realiza as a��es de banco: Busca e seta os valores
		Session session = HibernateUtil.currentSession();

		Dao<Usuario, Integer> dao = new Dao<Usuario, Integer>(session, Usuario.class);		
		dao.delete(usuarioDeletado);
		// Dispara novamente a pesquisa para manter o v�nculo
		pesquisaControlesAcesso(e);

	}	
	
}
