package br.com.itavida.jobs;

import org.hibernate.Session;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.itavida.dao.EmpresaDAO;
import br.com.itavida.util.HibernateUtil;

public class AtualizacaoValorPropostasJob implements Job {

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		HibernateUtil.openSession();
		Session session = HibernateUtil.currentSession();
		EmpresaDAO empresaDAO = new EmpresaDAO(session);
		empresaDAO.atualizarPropostasReangariadas();
		HibernateUtil.closeCurrentSession();
	}



}
