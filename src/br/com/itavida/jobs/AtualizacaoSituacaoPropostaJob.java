package br.com.itavida.jobs;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.util.HibernateUtil;

public class AtualizacaoSituacaoPropostaJob implements Job {

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		HibernateUtil.openSession();
		
		Session session = HibernateUtil.currentSession();
		Transaction tx = session.beginTransaction();
		
		PropostaDAO propostaDAO = new PropostaDAO(session);
		
		int rowsAffected = propostaDAO.atualizarSituacao();
		System.out.println("Job de atualiza��o da situa��o das propostas executado. Registros atualizados: " + rowsAffected);
		
		tx.commit();
		
		HibernateUtil.closeCurrentSession();
		
	}

}
