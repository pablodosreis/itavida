package br.com.itavida.jobs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AtualizacaoFatoresJob implements Job {

	public static void main(String[] args) throws JobExecutionException {
		AtualizacaoFatoresJob a = new AtualizacaoFatoresJob();
		a.execute(null);
	}
	
	private static final String URL_BD = "jdbc:mysql://localhost:3306/itavida";
	private static final String USER_BD = "root";
	private static final String PASS_BD = "1234";


	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("Inicio Atualiza��o dos fatores: " + new Date() );
		Calendar hoje = Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String hojeString = df.format(hoje.getTime());
		Date hojeFormatado = null;
		try {
			hojeFormatado = df.parse(hojeString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Connection con = null;
		Statement stmt = null;
		Statement stmtBegin = null;
		Statement stmtUpdtFator = null;
		Statement stmtCommit = null;
		Statement stmtRollBack = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(URL_BD, USER_BD, PASS_BD);
			String sqlBuscarAProcessar = "select dataInicio, idcapitalseguradofaixaetaria, fator, id from fator where processado = false order by id";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlBuscarAProcessar);
			while (rs.next()) {
				Date data = rs.getDate(1);
				Integer capitalSeguradoFaixaEtaria = rs.getInt(2);
				Double fator = rs.getDouble(3);
				Integer idFator = rs.getInt(4);
				if (data.compareTo(hojeFormatado) == 0) {
					stmtBegin = con.createStatement();
					stmtBegin.executeUpdate("START TRANSACTION;");
					processarFator(capitalSeguradoFaixaEtaria, fator, data, idFator, con);
					stmtUpdtFator = con.createStatement();
					stmtUpdtFator.executeUpdate("update fator set processado = true where id = " + idFator);
					stmtCommit = con.createStatement();
					stmtCommit.executeUpdate("COMMIT;");
				}
				
			}
			
		} catch (ClassNotFoundException e) {
			System.out.println("Classe n�o encontrada");
		}
		catch (SQLException e) {
			try {
				stmtRollBack = con.createStatement();
				stmtRollBack.executeUpdate("ROLLBACK;");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("Problemas com o BD" + e);
		} finally {
			tratarFechamentosExecuta(con, stmt, stmtBegin, stmtUpdtFator,stmtRollBack, rs);
		}
		System.out.println("Termino Atualiza��o dos fatores: " + new Date() );
	}

	private void tratarFechamentosExecuta(Connection con, Statement stmt,Statement stmtBegin, Statement stmtUpdtFator,
			Statement stmtRollback,
			ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmtUpdtFator != null) {
			try {
				stmtUpdtFator.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmtBegin != null) {
			try {
				stmtBegin.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmtRollback != null) {
			try {
				stmtRollback.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void processarFator(Integer capitalSeguradoFaixaEtaria,
			Double fator, Date dataInicio, Integer idFator, Connection con) {
		String sql = "select id, titular, somaTitularConjuge, capitalSegurado from detalhecapitalfaixaetaria where capitalseguradofaixaEtaria = "
				+ capitalSeguradoFaixaEtaria;
		Statement stmt = null, stmtUpdate = null, stmtInsertHistorico = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Integer id = rs.getInt(1);
				Double titular = rs.getDouble(2);
				Double somaTitularConjuge = rs.getDouble(3);
				Double capitalSegurado = rs.getDouble(4);
				//divide o fator por 100 para calcular o percentual de aumento, em seguida soma com 1 antes de multiplicar pelo valor antigo
				Double fatorAMultiplicar = 1.0 + (fator/100); 
				Double titularAtualizado = titular * fatorAMultiplicar;
				Double somaTitularConjugeAtualizado = somaTitularConjuge * fatorAMultiplicar;
				Double capitalSeguradoAtualizado = capitalSegurado * fatorAMultiplicar;
				String update = montarUpdateAtualizacaoDetalhe(dataInicio, id,
						titularAtualizado, somaTitularConjugeAtualizado,
						capitalSeguradoAtualizado);
				stmtUpdate = con.createStatement();
				int retorno = stmtUpdate.executeUpdate(update);
				if (retorno == 1) {
					stmtInsertHistorico = con.createStatement();
					String insertHistorico = montarInsertHistoricoAtualizacaoDetalhes(dataInicio,
							idFator, id, titular, somaTitularConjuge,
							capitalSegurado, titularAtualizado,
							somaTitularConjugeAtualizado,
							capitalSeguradoAtualizado);
					stmtInsertHistorico.execute(insertHistorico);
				}
			}
		} catch (SQLException e) {
			System.out.println("Problemas com o BD" + e);
		} finally {
			tratarFechamentosProcessarFator(stmt, stmtUpdate,
					stmtInsertHistorico, rs);
			
		}
	}

	private void tratarFechamentosProcessarFator(Statement stmt,
			Statement stmtUpdate, Statement stmtInsertHistorico, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmtUpdate != null) {
			try {
				stmtUpdate.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmtInsertHistorico != null) {
			try {
				stmtUpdate.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private String montarInsertHistoricoAtualizacaoDetalhes(Date dataInicio,
			Integer idFator, Integer id, Double titular,
			Double somaTitularConjuge, Double capitalSegurado,
			Double titularAtualizado, Double somaTitularConjugeAtualizado,
			Double capitalSeguradoAtualizado) {
		DecimalFormat df = new DecimalFormat("0.00");
	    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
	    otherSymbols.setDecimalSeparator('.');
	    df.setDecimalFormatSymbols(otherSymbols);
		return "INSERT " +
				"INTO " +
			    "historicoalteracaodetalhecapitalfaixaEtaria " +
			    "(" +
			        "idDetalheCapitalFaixaEtaria," +
			        "titularValorAntigo," +
			        "somaTitularConjugValorAntigo," +
			        "capitalSeguradoValorAntigo," +
			        "dataAjuste," +
			        "titularValorAjustado," +
			        "somaTitularConjugValorAjustado," +
			        "capitalSeguradoValorAjustado," +
			        "idFator" +
			    ") " +
			    " VALUES" +
			    " (" +
			        id + ", " +
			        df.format(titular) + ", " +
			        df.format(somaTitularConjuge) + ", " +
			        df.format(capitalSegurado) + ", '" +
			        new SimpleDateFormat("yyyy-MM-dd").format(dataInicio) + "', " +
			        df.format(titularAtualizado) + ", " +
			        df.format(somaTitularConjugeAtualizado) + ", " +
			        df.format(capitalSeguradoAtualizado) + ", " +
			        idFator + " " +
			    ")";
	}

	private String montarUpdateAtualizacaoDetalhe(Date dataInicio, Integer id,
			Double titularAtualizado, Double somaTitularConjugeAtualizado,
			Double capitalSeguradoAtualizado) {
		DecimalFormat df = new DecimalFormat("0.00");
	    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
	    otherSymbols.setDecimalSeparator('.');
	    df.setDecimalFormatSymbols(otherSymbols);
		String update = "update detalhecapitalfaixaetaria set titular = " + df.format(titularAtualizado) +
			", somaTitularConjuge = " + df.format(somaTitularConjugeAtualizado) +
			", capitalsegurado = " + df.format(capitalSeguradoAtualizado) +
			", dataAjuste = '" + new SimpleDateFormat("yyyy-MM-dd").format(dataInicio) +
			"' where id = " + id;
		return update;
	}
	
}
