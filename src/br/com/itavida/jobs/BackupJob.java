package br.com.itavida.jobs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;

public class BackupJob implements Job {

	private static final int MAXIMO_DIAS_ARMAZENADOS = 30;
	private static final String EXTENSAO = ".sql";
	private static final String PREFIXO = "BCK_SISVIDA_";
	private static final String DIR_USUARIO = System.getProperty("user.home");
	private static final String DROPBOX = "DropBox";
	private static final String PATH_DROPBOX = DIR_USUARIO + "\\" + DROPBOX
			+ "\\";
	private static final String PATH_MIDIA_EXTERNA = "D:\\";

	public String gerarNomeBackup() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmm");
		StringBuilder arquivo = new StringBuilder();
		arquivo.append(PREFIXO);
		arquivo.append(df.format(new Date()));
		arquivo.append(EXTENSAO);
		return arquivo.toString();
	}

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		String nomeArquivo = gerarNomeBackup();
		executarBackupDropBox(nomeArquivo);
		executarBackupMidiaExterna(nomeArquivo);
	}

	private void logSucesso(String tipoBackup) {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		System.out.println(String.format(
				"Backup \"%s\" executado com sucesso �s "
						+ df.format(new Date()), tipoBackup));
	}

	private void executarBackupMidiaExterna(String nomeArquivo) {
		String pathMidiaExterna = null;
		
		try {
			URL url = this.getClass().getResource(Constantes.SISVIDA_FILES + "caminho_midia_externa.txt");
			File file = new File(url.toURI());
			FileReader fr = new FileReader(file);  
			BufferedReader br = new BufferedReader(fr);
			pathMidiaExterna = br.readLine();
		} catch (Exception e) {
			pathMidiaExterna = PATH_MIDIA_EXTERNA;
			e.printStackTrace();
		}
		
		String pathArquivo = pathMidiaExterna + nomeArquivo;
		
		executarEArmazenarBackup(nomeArquivo, pathArquivo, "MidiaExterna");
		
		limparBackupsAntigos(pathMidiaExterna);
	}

	private void executarBackupDropBox(String nomeArquivo) {

		String pathArquivo = PATH_DROPBOX + nomeArquivo;
		
		executarEArmazenarBackup(nomeArquivo, pathArquivo, "DropBox");
		
		limparBackupsAntigos(PATH_DROPBOX);
	}
	
	private void executarEArmazenarBackup(String nomeArquivo,
			String pathArquivo, String tipoBackup) {
		 
		try {
			ProcessBuilder pb = new ProcessBuilder("mysqldump ", "-uroot", "-p1234", "itavida");
			pb.directory(new File(DIR_USUARIO));
			pb.redirectErrorStream(true);
			Process p = pb.start();
			InputStream is = p.getInputStream();
			InputStreamReader isr = new InputStreamReader(is,"UTF-8");
			BufferedReader br = new BufferedReader(isr);

			FileWriter fstream = new FileWriter(pathArquivo);
			BufferedWriter out = new BufferedWriter(fstream);

			String line;
			while ((line = br.readLine()) != null) {
				out.write(line);
				out.write("\n");
			}
			out.flush();
			out.close();
			logSucesso(tipoBackup);
		} catch (IOException e) {
			throw new RuntimeException(String.format(
					"O backup \"%s\" n�o foi realizado.", tipoBackup), e);
		}
	}
	
	private void limparBackupsAntigos(String path) {
		try {
			File dirBck = new File(path);
			for(File file : dirBck.listFiles()) {
				String nomeArquivo = file.getName();
				if(!nomeArquivo.startsWith(PREFIXO)) {
					continue;
				}
				String textoDataCriacao = nomeArquivo.substring(12,20);
				Date dataCriacao = DateUtil.getDate(textoDataCriacao, "yyyyMMdd");
				Date dataAtual = new Date();
				int dif = DateUtil.diffEmDias(dataAtual, dataCriacao);
				if(dif > MAXIMO_DIAS_ARMAZENADOS) {
					file.delete();
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Erro ao limpar backups antigos", e);
		}
	}


}