package br.com.itavida.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import com.google.gson.Gson;

import br.com.itavida.dao.Dao;
import br.com.itavida.dao.PropostaDAO;
import br.com.itavida.dto.AgenciadorDTO;
import br.com.itavida.dto.BeneficiarioDTO;
import br.com.itavida.dto.PropostaDTO;
import br.com.itavida.dto.SinistroDTO;
import br.com.itavida.entidades.Beneficiario;
import br.com.itavida.entidades.HistoricoSinistro;
import br.com.itavida.entidades.LogAcessos;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.PropostaAgenciador;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;

public class DadosSegurado extends HttpServlet {
	
	private String getEstadoCivil(String sigla) {
		Map<String,String> mapa = new HashMap<String,String>();
		mapa.put( "CA", "Casado" );
		mapa.put( "CO", "Companheiro");
		mapa.put( "DE", "Desquitado");
		mapa.put( "DI", "Divorciado");
		mapa.put( "OT", "Outros" );
		mapa.put( "SE", "Separado");
		mapa.put( "SO", "Solteiro");
		mapa.put( "VI", "Vi�vo" );	
		return mapa.get(sigla);
	}
	
	private String getSexo(String sexo) {
		if (sexo == null) {
			return null;
		}
		if(sexo.equals("M")) {
			return "Masculino";
		} else if (sexo.equals("F")) {
			return "Feminino";
		} else {
			return null;
		}
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("application/json");// setting the content type
		
		res.addHeader("Access-Control-Allow-Origin", "*");
		res.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
		
		boolean erro = false;
		
		String json =  "{\"erro\" : \"N�o iniciado\"}";
		Session session = HibernateUtil.currentSession();
		PropostaDAO pDao = new PropostaDAO(session);
		
		
		
		String cpf = req.getParameter("cpf");
		String dataNascimento = req.getParameter("dtNascimento");
		
		
		Dao<LogAcessos, Integer> logDAO = new Dao<LogAcessos, Integer>(session, LogAcessos.class);
		LogAcessos log = new LogAcessos();
		log.setTipo("APLICATIVO");
		log.setObservacao("Acesso CPF: " + cpf );
		log.setDataHora(new Date());
		logDAO.save(log);
		
		
		
		if (StringUtils.isEmpty(cpf)) {
			json = "{\"erro\" : \"O campo CPF deve ser informado.\"}";
			res.setStatus(500);
			erro = true;
		}
		
		if (StringUtils.isEmpty(dataNascimento)) {
			json = "{\"erro\" : \"O campo data de nascimento deve ser informado.\"}";
			res.setStatus(500);
			erro = true;
		}
		
		if (!erro) {
			try {
				cpf = cpf.replace("-", "").replace(".", "");
				
				
				
				PropostaDTO dto = new PropostaDTO();
				dto.setAgenciadores(new ArrayList<AgenciadorDTO>());
				dto.setBeneficiarios(new ArrayList<BeneficiarioDTO>());
				dto.setSinistros(new ArrayList<SinistroDTO>());
				Proposta proposta = pDao.getPropostaPorCPF(cpf);
				
				if (proposta != null) {
					if (proposta.getSituacao() == SituacaoProposta.CANCELADA ) {
						json = "{\"erro\" : \"Sua proposta est� cancelada, entre com contato pelo telefone 0800 035 3100 para realiza��o de uma nova proposta.\"}";
						res.setStatus(500);
						erro = true;
					}
					
					if (proposta.getSituacao() == SituacaoProposta.INADIMPLENTE) {
						json = "{\"erro\" : \"Sua proposta possui pend�ncias no pagamento, entre com contato pelo 0800 035 3100.\"}";
						res.setStatus(500);
						erro = true;
					}
					
					String data = DateUtil.getFormattedDate(proposta.getDataNascimentoSegurado(), "dd/MM/yyyy");
					if (!data.equals(dataNascimento)) {
						json = "{\"erro\" : \"N�o foram encontrados dados para este CPF e Data de Nascimento.\"}";
						res.setStatus(500);
						erro = true;
					}
				}
				
				if (!erro) {	
					if (proposta != null) {
						pDao.populaCapital(proposta);
						BeanUtils.copyProperties(dto, proposta);
						
						//Preenchimento manual
						dto.setTxtSituacaoProposta(proposta.getStatus());
						dto.setTxtCidade(proposta.getCidade() != null ? proposta.getCidade().getNom_cidade() : null);
						dto.setTxtSiglaUF(proposta.getCidade() != null ? proposta.getCidade().getCod_estado().getSgl_estado(): null);
						dto.setTxtApolice(proposta.getEmpresa() != null ? proposta.getEmpresa().getNomeEmpresa() : null);
						dto.setTxtModeloProposta(proposta.getModeloProposta() != null ? proposta.getModeloProposta().getDescricao() : null);
						dto.setTxtTipoProposta(proposta.getTipoProposta() != null ? proposta.getTipoProposta().getDescricao() : null);
						dto.setTxtOrgao(proposta.getOrgao() != null ? proposta.getOrgao().getNomeOrgao() : null);
						dto.setTxtMotivoCancelamento(proposta.getMotivoCancelamento() != null ? proposta.getMotivoCancelamento().getDescricao(): null);
						dto.setNumeroSorteio(proposta.getNumeroSorteio());
						dto.setSexoSegurado(getSexo(dto.getSexoSegurado()));
						dto.setSexoConjuge(getSexo(dto.getSexoConjuge()));
						dto.setEstadoCivilSegurado(getEstadoCivil(dto.getEstadoCivilSegurado()));
						dto.setEstadoCivilConjuge(getEstadoCivil(dto.getEstadoCivilConjuge()));
						
						
						for (PropostaAgenciador pAgenciador: proposta.getPropostaagenciador_collection()) {
							AgenciadorDTO agenciadorDTO = new AgenciadorDTO();
							agenciadorDTO.setNome(pAgenciador.getFuncionario().getNome());
							agenciadorDTO.setTelefone(pAgenciador.getFuncionario().getTelefone());
							dto.getAgenciadores().add(agenciadorDTO);
						}
						
						for (HistoricoSinistro pSinistro : proposta.getHistoricosinistro_collection()) {
							SinistroDTO sinistroDTO = new SinistroDTO();
							sinistroDTO.setDataEnvioSeguradora(pSinistro.getDataEnvioSeguradora());
							sinistroDTO.setDataPagamento(pSinistro.getDataPagamento());
							sinistroDTO.setDataSinistro(pSinistro.getDataSinistro());
							sinistroDTO.setNumeroSinistro(pSinistro.getNumeroSinistro());
							sinistroDTO.setValorSinistro(pSinistro.getValorSinistro());
							sinistroDTO.setValorPagoSinistro(pSinistro.getValorPagoSinistro());
							dto.getSinistros().add(sinistroDTO);
						}
						
						for (Beneficiario pBenificario : proposta.getBeneficiario_collection()) {
							BeneficiarioDTO beneficiarioDTO = new BeneficiarioDTO();
							beneficiarioDTO.setNome(pBenificario.getNomeBeneficiario());
							beneficiarioDTO.setParentesco(pBenificario.getGrauParentesco() != null ? pBenificario.getGrauParentesco().getDescricao() : null);
							beneficiarioDTO.setPercentual(pBenificario.getPercentual());
							dto.getBeneficiarios().add(beneficiarioDTO);
						}		
						
						Gson gson = new Gson();
						json = gson.toJson(dto);
						res.setStatus(200);
					} else {
						json = "{\"erro\" : \"N�o foi encontrada proposta ativa com o CPF informado.\"}";
						res.setStatus(500);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				json = "{\"erro\" : \"Ocorreu um erro inesperado ao tentar acessar a proposta. Tente novamente mais tarde.\"}";
				res.setStatus(500);
			}
		}
		
		PrintWriter pw = res.getWriter();// get the stream to write the data
		pw.println(json);
		pw.close();// closing the stream
	}
}
