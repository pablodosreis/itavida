package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="orgaosiape")
public class OrgaoSiape implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private String nomeOrgaoSiape;
	
	@OneToMany(mappedBy="codigoOrgao")
	private Set<Siape> siape_collection;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeOrgaoSiape() {
		return nomeOrgaoSiape;
	}

	public void setNomeOrgaoSiape(String nomeOrgaoSiape) {
		this.nomeOrgaoSiape = nomeOrgaoSiape;
	}

	public Set<Siape> getSiape_collection() {
		return siape_collection;
	}

	public void setSiape_collection(Set<Siape> siape_collection) {
		this.siape_collection = siape_collection;
	}

	

}
