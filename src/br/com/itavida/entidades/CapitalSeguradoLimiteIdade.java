package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="capitalseguradolimiteidade")
public class CapitalSeguradoLimiteIdade implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer conjLimiteIdade;

	private Double conjVLInvalidezPerm;

	private Double conjVLMorteAcidente;

	private Double conjVLMorteNatural;

	private Date dataVigencia;

	private Integer funcLimiteIdade;

	private Double funcVLCasado100;

	private Double funcVLCasado50;

	private Double funcVLIndividual;

	private Double funcVLInvalidezPerm;

	private Double funcVLMorteAcidente;

	private Double funcVLMorteNatural;

	@ManyToOne
	@JoinColumn(name="nomeTabela")
	private NomeTabela nomeTabela;

	private static final long serialVersionUID = 1L;

	public CapitalSeguradoLimiteIdade() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getConjLimiteIdade() {
		return this.conjLimiteIdade;
	}

	public void setConjLimiteIdade(Integer conjLimiteIdade) {
		this.conjLimiteIdade = conjLimiteIdade;
	}

	public Double getConjVLInvalidezPerm() {
		return this.conjVLInvalidezPerm;
	}

	public void setConjVLInvalidezPerm(Double conjVLInvalidezPerm) {
		this.conjVLInvalidezPerm = conjVLInvalidezPerm;
	}

	public Double getConjVLMorteAcidente() {
		return this.conjVLMorteAcidente;
	}

	public void setConjVLMorteAcidente(Double conjVLMorteAcidente) {
		this.conjVLMorteAcidente = conjVLMorteAcidente;
	}

	public Double getConjVLMorteNatural() {
		return this.conjVLMorteNatural;
	}

	public void setConjVLMorteNatural(Double conjVLMorteNatural) {
		this.conjVLMorteNatural = conjVLMorteNatural;
	}

	public Date getDataVigencia() {
		return this.dataVigencia;
	}

	public void setDataVigencia(Date dataVigencia) {
		this.dataVigencia = dataVigencia;
	}

	public Integer getFuncLimiteIdade() {
		return this.funcLimiteIdade;
	}

	public void setFuncLimiteIdade(Integer funcLimiteIdade) {
		this.funcLimiteIdade = funcLimiteIdade;
	}

	public Double getFuncVLCasado100() {
		return this.funcVLCasado100;
	}

	public void setFuncVLCasado100(Double funcVLCasado100) {
		this.funcVLCasado100 = funcVLCasado100;
	}

	public Double getFuncVLCasado50() {
		return this.funcVLCasado50;
	}

	public void setFuncVLCasado50(Double funcVLCasado50) {
		this.funcVLCasado50 = funcVLCasado50;
	}

	public Double getFuncVLIndividual() {
		return this.funcVLIndividual;
	}

	public void setFuncVLIndividual(Double funcVLIndividual) {
		this.funcVLIndividual = funcVLIndividual;
	}

	public Double getFuncVLInvalidezPerm() {
		return this.funcVLInvalidezPerm;
	}

	public void setFuncVLInvalidezPerm(Double funcVLInvalidezPerm) {
		this.funcVLInvalidezPerm = funcVLInvalidezPerm;
	}

	public Double getFuncVLMorteAcidente() {
		return this.funcVLMorteAcidente;
	}

	public void setFuncVLMorteAcidente(Double funcVLMorteAcidente) {
		this.funcVLMorteAcidente = funcVLMorteAcidente;
	}

	public Double getFuncVLMorteNatural() {
		return this.funcVLMorteNatural;
	}

	public void setFuncVLMorteNatural(Double funcVLMorteNatural) {
		this.funcVLMorteNatural = funcVLMorteNatural;
	}

	public NomeTabela getNomeTabela() {
		return this.nomeTabela;
	}

	public void setNomeTabela(NomeTabela nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

}
