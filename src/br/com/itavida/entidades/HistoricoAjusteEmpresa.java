package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="historicoajusteempresa")
public class HistoricoAjusteEmpresa implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "ajuste_14_65")
	private Double ajuste14a65;
	
	@Column(name = "ajuste_66_mais")
	private Double ajuste66mais;
	
	@Column(name = "ajuste_ipca")
	private Double ajusteIpca;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dataAjuste")
	private Date dataAjuste;
	
	@Column(name = "nomeUsuario")
	private String nomeUsuario;

	@ManyToOne
	@JoinColumn(name = "idempresa")
	private Empresa empresa;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getAjuste14a65() {
		return ajuste14a65;
	}

	public void setAjuste14a65(Double ajuste14a65) {
		this.ajuste14a65 = ajuste14a65;
	}

	public Double getAjuste66mais() {
		return ajuste66mais;
	}

	public void setAjuste66mais(Double ajuste66mais) {
		this.ajuste66mais = ajuste66mais;
	}

	public Double getAjusteIpca() {
		return ajusteIpca;
	}

	public void setAjusteIpca(Double ajusteIpca) {
		this.ajusteIpca = ajusteIpca;
	}

	public Date getDataAjuste() {
		return dataAjuste;
	}

	public void setDataAjuste(Date dataAjuste) {
		this.dataAjuste = dataAjuste;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoAjusteEmpresa other = (HistoricoAjusteEmpresa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
