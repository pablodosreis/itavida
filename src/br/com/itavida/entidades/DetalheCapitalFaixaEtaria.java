package br.com.itavida.entidades;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="detalhecapitalfaixaetaria")
public class DetalheCapitalFaixaEtaria implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Double capitalSegurado;

	private Integer idadeFinal;

	private Integer idadeInicial;

	private Double somaTitularConjuge;
	
	@Transient
	private String dataAjusteFormatada;

	private Double titular;
	
	private Date dataAjuste;

	@ManyToOne
	@JoinColumn(name="capitalSeguradoFaixaEtaria")
	private CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria;

	private static final long serialVersionUID = 1L;

	public DetalheCapitalFaixaEtaria() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getCapitalSegurado() {
		return this.capitalSegurado;
	}

	public void setCapitalSegurado(Double capitalSegurado) {
		this.capitalSegurado = capitalSegurado;
	}

	public Integer getIdadeFinal() {
		return this.idadeFinal;
	}

	public void setIdadeFinal(Integer idadeFinal) {
		this.idadeFinal = idadeFinal;
	}

	public Integer getIdadeInicial() {
		return this.idadeInicial;
	}

	public void setIdadeInicial(Integer idadeInicial) {
		this.idadeInicial = idadeInicial;
	}

	public Double getSomaTitularConjuge() {
		return this.somaTitularConjuge;
	}

	public void setSomaTitularConjuge(Double somaTitularConjuge) {
		this.somaTitularConjuge = somaTitularConjuge;
	}

	public Double getTitular() {
		return this.titular;
	}

	public void setTitular(Double titular) {
		this.titular = titular;
	}

	public CapitalSeguradoFaixaEtaria getCapitalSeguradoFaixaEtaria() {
		return this.capitalSeguradoFaixaEtaria;
	}

	public void setCapitalSeguradoFaixaEtaria(CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria) {
		this.capitalSeguradoFaixaEtaria = capitalSeguradoFaixaEtaria;
	}

	public Date getDataAjuste() {
		return dataAjuste;
	}

	public void setDataAjuste(Date dataAjuste) {
		this.dataAjuste = dataAjuste;
	}

	public String getDataAjusteFormatada() {
		if (getDataAjuste() != null) {
			return new SimpleDateFormat("dd/MM/yyyy").format(getDataAjuste());
		} else {
			return "-";
		}
		
	}

	public void setDataAjusteFormatada(String dataAjusteFormatada) {
		this.dataAjusteFormatada = dataAjusteFormatada;
	}
	
}
