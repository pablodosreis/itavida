package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import br.com.itavida.dao.OrgaoQuitacaoDAO;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;

@Entity
@Table(name="proposta")
public class Proposta implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="superap_id")
	private SuperAp superAp;
	
	private String lotacao;

	private String bairro;

	private String cep;

	private String complemento;

	private String cpfConjuge;

	private String cpfSegurado;

	private Date dataAprovacao;

	private Date dataCadastro;

	private Date dataCancelamento;

	private Date dataContemplacao;

	private Date dataNascimentoConjuge;

	private Date dataNascimentoSegurado;

	private String descontarTaxa;

	private String enderecoSegurado;
	
	private String numeroEnderecoSegurado;

	private String estadoCivilConjuge;

	private String estadoCivilSegurado;

	private String matriculaConjuge;

	private String matriculaSegurado;

	private String nomeConjuge;

	private String nomeSegurado;

	private String observacao;

	private String rgConjuge;

	private String rgSegurado;

	private String sexoConjuge;

	private String sexoSegurado;
	
	private String profissaoConjuge;

	private String profissaoSegurado;

	private String telefoneCelular;

	private String telefoneComercial;

	private String telefoneCelular2;

	private String telefoneResidencial;
	
	private Date dataAgenciamento;
	
	private Date dataAlteracao;
	
	private Date dataExclusao;
	
	private String nomeUsuarioAlteracao;
	
	private String email;
	
	
	@Enumerated(EnumType.STRING)
	@Column(name = "situacao", length = 15)
	private SituacaoProposta situacao = SituacaoProposta.ATIVA;

	@ManyToOne
	@JoinColumn(name="cidade")
	private Cidade cidade;
	
	@ManyToOne
	@JoinColumn(name="empresa")
	private Empresa empresa;	

	@ManyToOne
	@JoinColumn(name="capitalSeguradoFaixaEtaria")
	private CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria;

	@ManyToOne
	@JoinColumn(name="capitalSeguradoLimiteIdade")
	private CapitalSeguradoLimiteIdade capitalSeguradoLimiteIdade;

	@ManyToOne
	@JoinColumn(name="modeloProposta")
	private ModeloProposta modeloProposta;

	@ManyToOne
	@JoinColumn(name="nomeTabela")
	private NomeTabela nomeTabela;

	@ManyToOne
	@JoinColumn(name="orgao")
	private Orgao orgao;

	@ManyToOne
	@JoinColumn(name="tipoProposta")
	private TipoProposta tipoProposta;
	
	@ManyToOne
	@JoinColumn(name="motivoCancelamento")
	private MotivoCancelamento motivoCancelamento;
	
	@ManyToOne
	@JoinColumn(name="forma_pagamento_id")
	private FormaPagamento formaPagamento;

	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Set<HistoricoSinistro> historicosinistro_collection;
	

	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<Beneficiario> beneficiario_collection;

	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<PropostaAgenciador> propostaagenciador_collection;
	
	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@OrderBy("id") //a ordena��o garante que o �ltimo n�mero do sorteio seja exibido nos relat�rios
	private Collection<NumeroSorteio> numerosorteio_collection;
	
	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<HistoricoBeneficiario> historicobeneficiario_collection;

	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<HistoricoCapital> historicocapital_collection;
	
	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<HistoricoConjuge> historicoconjuge_collection;
	
	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<HistoricoEnderecoSegurado> historicoenderecosegurado_collection;
	
	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<HistoricoProposta> historicoproposta_collection;
	
	@OneToMany(mappedBy="proposta", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<ComissaoAgenciador> comissaoagenciador_collection;
	
	public Collection<HistoricoProposta> getHistoricoproposta_collection() {
		return historicoproposta_collection;
	}


	public void setHistoricoproposta_collection(
			Collection<HistoricoProposta> historicoproposta_collection) {
		this.historicoproposta_collection = historicoproposta_collection;
	}

	@ManyToOne
	@JoinColumn(name="detalheCapitalFaixaEtaria")
	private DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria;
	
	@ManyToOne
	@JoinColumn(name="detalheCapitalFaixaEtariaFuturo")
	private DetalheCapitalFaixaEtaria detalheCapitalFaixaEtariaFuturo;
	
	@Column(name = "dataUltimaAtualizacaoSituacao")
	private Date dataUltimaAtualizacaoSituacao;
	
	/*novos campos para persistir os valores de capital e pr�mio*/
	@Column(name = "valorPremioSegurado")
	private Double valorPremioSegurado;
	
	@Column(name = "valorPremioConjuge")
	private Double valorPremioConjuge;
	
	@Column(name = "valorCapitalSegurado")
	private Double valorCapitalSegurado;
	
	@Column(name = "valorCapitalConjuge")
	private Double valorCapitalConjuge;
	
	/*fim*/
	
	/*M�todos para valores futuro*/
	
	@Column(name = "valorFuturoPremioSegurado")
	private Double valorFuturoPremioSegurado;
	
	@Column(name = "valorFuturoPremioConjuge")
	private Double valorFuturoPremioConjuge;
	
	@Column(name = "valorFuturoCapitalSegurado")
	private Double valorFuturoCapitalSegurado;
	
	@Column(name = "valorFuturoCapitalConjuge")
	private Double valorFuturoCapitalConjuge;
	
	/*Fim*/
	
	@Column(name = "comandoComissao")
	private String comandoComissao;
	
	@Transient
	private Integer idAgenciador;
	@Transient
	private Funcionario funcionario;
	@Transient
	private Date dataNascimentoSeguradoInicio;
	@Transient
	private Date dataNascimentoSeguradoFim;
	@Transient
	private Date dataNascimentoConjugeInicio;
	@Transient
	private Date dataNascimentoConjugeFim;	
	@Transient
	private Date dataAprovacaoInicio;
	@Transient
	private Date dataAprovacaoFim;	
	@Transient
	private Date dataCadastroInicio;		
	@Transient
	private Date dataCadastroFim;		
	@Transient
	private Date dataCancelamentoInicio;	
	@Transient
	private Date dataCancelamentoFim;	
	@Transient
	private String numeroSorteioFiltro;	
	@Transient
	private Double capitalSegurado;
	@Transient
	private Double capitalConjuge;
	@Transient
	private Double premioConjuge;
	@Transient
	private Double premioSegurado;	
	@Transient
	private Double totalPremios;
	@Transient	
	private Double valorDescontado;
	@Transient	
	private Double novoValorFaixaEtaria;
	@Transient	
	private Double diferencaValores;	
	@Transient	
	private Integer idadeRealSegurado;
	@Transient	
	private Integer idadePrevistaSegurado;
	@Transient
	private String numeroSorteio;
	@Transient
	private String cpfSeguradoFormatado;
	@Transient
	private String cpfConjugeFormatado;
	@Transient
	private Double capitalSeguradoFuturo;
	@Transient
	private Double capitalConjugeFuturo;
	@Transient
	private Double premioSeguradoFuturo;
	@Transient
	private Double premioConjugeFuturo;
	@Transient
	private Double totalPremiosFuturo;
	@Transient
	private List<SituacaoProposta> filtroSituacao;
	@Transient
	private Integer mesAniversario;
	@Transient
	private String periodicidadeFormatada;
	
	private static final long serialVersionUID = 1L;

	// Construtor
	public Proposta() {
		super();
	}

	
	//-------------GETTERS AND SETTERS ---------------------------------
	public Integer getIdAgenciador() {
		return idAgenciador;
	}

	public void setIdAgenciador(Integer idAgenciador) {
		this.idAgenciador = idAgenciador;
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCpfConjuge() {
		return this.cpfConjuge;
	}

	public void setCpfConjuge(String cpfConjuge) {
		this.cpfConjuge = cpfConjuge;
	}

	public String getCpfSegurado() {
		return this.cpfSegurado;
	}

	public void setCpfSegurado(String cpfSegurado) {
		this.cpfSegurado = cpfSegurado;
	}

	public Date getDataAprovacao() {
		return this.dataAprovacao;
	}

	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	public Date getDataCadastro() {
		return this.dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataCancelamento() {
		return this.dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public Date getDataContemplacao() {
		return this.dataContemplacao;
	}

	public void setDataContemplacao(Date dataContemplacao) {
		this.dataContemplacao = dataContemplacao;
	}

	public Date getDataNascimentoConjuge() {
		return this.dataNascimentoConjuge;
	}

	public void setDataNascimentoConjuge(Date dataNascimentoConjuge) {
		this.dataNascimentoConjuge = dataNascimentoConjuge;
	}

	public Date getDataNascimentoSegurado() {
		return this.dataNascimentoSegurado;
	}

	public void setDataNascimentoSegurado(Date dataNascimentoSegurado) {
		this.dataNascimentoSegurado = dataNascimentoSegurado;
	}

	public String getDescontarTaxa() {
		return this.descontarTaxa;
	}

	public void setDescontarTaxa(String descontarTaxa) {
		this.descontarTaxa = descontarTaxa;
	}

	public String getEnderecoSegurado() {
		return this.enderecoSegurado;
	}

	public void setEnderecoSegurado(String enderecoSegurado) {
		this.enderecoSegurado = enderecoSegurado;
	}

	public String getEstadoCivilConjuge() {
		return this.estadoCivilConjuge;
	}

	public void setEstadoCivilConjuge(String estadoCivilConjuge) {
		this.estadoCivilConjuge = estadoCivilConjuge;
	}

	public String getEstadoCivilSegurado() {
		return this.estadoCivilSegurado;
	}

	public void setEstadoCivilSegurado(String estadoCivilSegurado) {
		this.estadoCivilSegurado = estadoCivilSegurado;
	}

	public String getMatriculaConjuge() {
		return this.matriculaConjuge;
	}

	public void setMatriculaConjuge(String matriculaConjuge) {
		this.matriculaConjuge = matriculaConjuge;
	}

	public String getMatriculaSegurado() {
		return this.matriculaSegurado;
	}

	public void setMatriculaSegurado(String matriculaSegurado) {
		this.matriculaSegurado = matriculaSegurado;
	}

	public MotivoCancelamento getMotivoCancelamento() {
		return this.motivoCancelamento;
	}

	public void setMotivoCancelamento(MotivoCancelamento motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public String getNomeConjuge() {
		return this.nomeConjuge;
	}

	public void setNomeConjuge(String nomeConjuge) {
		this.nomeConjuge = nomeConjuge;
	}

	public String getNomeSegurado() {
		return this.nomeSegurado;
	}

	public void setNomeSegurado(String nomeSegurado) {
		this.nomeSegurado = nomeSegurado;
	}

	public String getObservacao() {
		return this.observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getRgConjuge() {
		return this.rgConjuge;
	}

	public void setRgConjuge(String rgConjuge) {
		this.rgConjuge = rgConjuge;
	}

	public String getRgSegurado() {
		return this.rgSegurado;
	}

	public void setRgSegurado(String rgSegurado) {
		this.rgSegurado = rgSegurado;
	}

	public String getSexoConjuge() {
		return this.sexoConjuge;
	}

	public void setSexoConjuge(String sexoConjuge) {
		this.sexoConjuge = sexoConjuge;
	}

	public String getSexoSegurado() {
		return this.sexoSegurado;
	}

	public void setSexoSegurado(String sexoSegurado) {
		this.sexoSegurado = sexoSegurado;
	}

	public String getTelefoneCelular() {
		return this.telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getTelefoneComercial() {
		return this.telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	public String getTelefoneCelular2() {
		return this.telefoneCelular2;
	}

	public void setTelefoneCelular2(String telefoneCelular2) {
		this.telefoneCelular2 = telefoneCelular2;
	}

	public String getTelefoneResidencial() {
		return this.telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public Cidade getCidade() {
		return this.cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public CapitalSeguradoFaixaEtaria getCapitalSeguradoFaixaEtaria() {
		return this.capitalSeguradoFaixaEtaria;
	}

	public void setCapitalSeguradoFaixaEtaria(CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria) {
		this.capitalSeguradoFaixaEtaria = capitalSeguradoFaixaEtaria;
	}

	public CapitalSeguradoLimiteIdade getCapitalSeguradoLimiteIdade() {
		return this.capitalSeguradoLimiteIdade;
	}

	public void setCapitalSeguradoLimiteIdade(CapitalSeguradoLimiteIdade capitalSeguradoLimiteIdade) {
		this.capitalSeguradoLimiteIdade = capitalSeguradoLimiteIdade;
	}

	public ModeloProposta getModeloProposta() {
		return this.modeloProposta;
	}

	public void setModeloProposta(ModeloProposta modeloProposta) {
		this.modeloProposta = modeloProposta;
	}

	public NomeTabela getNomeTabela() {
		return this.nomeTabela;
	}

	public void setNomeTabela(NomeTabela nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

	public Orgao getOrgao() {
		return this.orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public TipoProposta getTipoProposta() {
		return this.tipoProposta;
	}

	public void setTipoProposta(TipoProposta tipoProposta) {
		this.tipoProposta = tipoProposta;
	}

	public Collection<Beneficiario> getBeneficiario_collection() {
		return this.beneficiario_collection;
	}

	public void setBeneficiario_collection(Collection<Beneficiario> beneficiario_collection) {
		this.beneficiario_collection = beneficiario_collection;
	}

	public Collection<PropostaAgenciador> getPropostaagenciador_collection() {
		return this.propostaagenciador_collection;
	}

	public void setPropostaagenciador_collection(Collection<PropostaAgenciador> propostaagenciador_collection) {
		this.propostaagenciador_collection = propostaagenciador_collection;
	}
	

	/**
	 * @return the numerosorteio_collection
	 */
	public Collection<NumeroSorteio> getNumerosorteio_collection() {
		return numerosorteio_collection;
	}


	/**
	 * @param numerosorteio_collection the numerosorteio_collection to set
	 */
	public void setNumerosorteio_collection(
			Collection<NumeroSorteio> numerosorteio_collection) {
		this.numerosorteio_collection = numerosorteio_collection;
	}


	public Funcionario getFuncionario() {
		return funcionario;
	}


	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public DetalheCapitalFaixaEtaria getDetalheCapitalFaixaEtaria() {
		return detalheCapitalFaixaEtaria;
	}


	public void setDetalheCapitalFaixaEtaria(
			DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria) {
		this.detalheCapitalFaixaEtaria = detalheCapitalFaixaEtaria;
	}


	/**
	 * @return the dataNascimentoSeguradoInicio
	 */
	public Date getDataNascimentoSeguradoInicio() {
		return dataNascimentoSeguradoInicio;
	}


	/**
	 * @param dataNascimentoSeguradoInicio the dataNascimentoSeguradoInicio to set
	 */
	public void setDataNascimentoSeguradoInicio(Date dataNascimentoSeguradoInicio) {
		this.dataNascimentoSeguradoInicio = dataNascimentoSeguradoInicio;
	}


	/**
	 * @return the dataNascimentoSeguradoFim
	 */
	public Date getDataNascimentoSeguradoFim() {
		return dataNascimentoSeguradoFim;
	}


	/**
	 * @param dataNascimentoSeguradoFim the dataNascimentoSeguradoFim to set
	 */
	public void setDataNascimentoSeguradoFim(Date dataNascimentoSeguradoFim) {
		this.dataNascimentoSeguradoFim = dataNascimentoSeguradoFim;
	}


	/**
	 * @return the dataNascimentoConjugeInicio
	 */
	public Date getDataNascimentoConjugeInicio() {
		return dataNascimentoConjugeInicio;
	}


	/**
	 * @param dataNascimentoConjugeInicio the dataNascimentoConjugeInicio to set
	 */
	public void setDataNascimentoConjugeInicio(Date dataNascimentoConjugeInicio) {
		this.dataNascimentoConjugeInicio = dataNascimentoConjugeInicio;
	}


	/**
	 * @return the dataNascimentoConjugeFim
	 */
	public Date getDataNascimentoConjugeFim() {
		return dataNascimentoConjugeFim;
	}


	/**
	 * @param dataNascimentoConjugeFim the dataNascimentoConjugeFim to set
	 */
	public void setDataNascimentoConjugeFim(Date dataNascimentoConjugeFim) {
		this.dataNascimentoConjugeFim = dataNascimentoConjugeFim;
	}


	/**
	 * @return the dataAprovacaoInicio
	 */
	public Date getDataAprovacaoInicio() {
		return dataAprovacaoInicio;
	}


	/**
	 * @param dataAprovacaoInicio the dataAprovacaoInicio to set
	 */
	public void setDataAprovacaoInicio(Date dataAprovacaoInicio) {
		this.dataAprovacaoInicio = dataAprovacaoInicio;
	}


	/**
	 * @return the dataAprovacaoFim
	 */
	public Date getDataAprovacaoFim() {
		return dataAprovacaoFim;
	}


	/**
	 * @param dataAprovacaoFim the dataAprovacaoFim to set
	 */
	public void setDataAprovacaoFim(Date dataAprovacaoFim) {
		this.dataAprovacaoFim = dataAprovacaoFim;
	}


	/**
	 * @return the dataCadastroInicio
	 */
	public Date getDataCadastroInicio() {
		return dataCadastroInicio;
	}


	/**
	 * @param dataCadastroInicio the dataCadastroInicio to set
	 */
	public void setDataCadastroInicio(Date dataCadastroInicio) {
		this.dataCadastroInicio = dataCadastroInicio;
	}


	/**
	 * @return the dataCadastroFim
	 */
	public Date getDataCadastroFim() {
		return dataCadastroFim;
	}


	/**
	 * @param dataCadastroFim the dataCadastroFim to set
	 */
	public void setDataCadastroFim(Date dataCadastroFim) {
		this.dataCadastroFim = dataCadastroFim;
	}


	/**
	 * @return the dataCancelamentoInicio
	 */
	public Date getDataCancelamentoInicio() {
		return dataCancelamentoInicio;
	}


	/**
	 * @param dataCancelamentoInicio the dataCancelamentoInicio to set
	 */
	public void setDataCancelamentoInicio(Date dataCancelamentoInicio) {
		this.dataCancelamentoInicio = dataCancelamentoInicio;
	}


	/**
	 * @return the dataCancelamentoFim
	 */
	public Date getDataCancelamentoFim() {
		return dataCancelamentoFim;
	}


	/**
	 * @param dataCancelamentoFim the dataCancelamentoFim to set
	 */
	public void setDataCancelamentoFim(Date dataCancelamentoFim) {
		this.dataCancelamentoFim = dataCancelamentoFim;
	}


	/**
	 * @return the numeroSorteioFiltro
	 */
	public String getNumeroSorteioFiltro() {
		return numeroSorteioFiltro;
	}


	/**
	 * @param numeroSorteioFiltro the numeroSorteioFiltro to set
	 */
	public void setNumeroSorteioFiltro(String numeroSorteioFiltro) {
		this.numeroSorteioFiltro = numeroSorteioFiltro;
	}
	
	public Set<HistoricoSinistro> getHistoricosinistro_collection() {
		return historicosinistro_collection;
	}


	public void setHistoricosinistro_collection(
			Set<HistoricoSinistro> historicosinistro_collection) {
		this.historicosinistro_collection = historicosinistro_collection;
	}


	public Double getCapitalConjuge() {
		return capitalConjuge;
	}


	public void setCapitalConjuge(Double capitalConjuge) {
		this.capitalConjuge = capitalConjuge;
	}


	public Double getPremioConjuge() {
		return premioConjuge;
	}


	public void setPremioConjuge(Double premioConjuge) {
		this.premioConjuge = premioConjuge;
	}


	public Double getPremioSegurado() {
		return premioSegurado;
	}


	public void setPremioSegurado(Double premioSegurado) {
		this.premioSegurado = premioSegurado;
	}


	public Double getTotalPremios() {
		return totalPremios;
	}


	public void setTotalPremios(Double totalPremios) {
		this.totalPremios = totalPremios;
	}


	public Double getValorDescontado() {
		return valorDescontado;
	}


	public void setValorDescontado(Double valorDescontado) {
		this.valorDescontado = valorDescontado;
	}


	public Double getCapitalSegurado() {
		return capitalSegurado;
	}


	public void setCapitalSegurado(Double capitalSegurado) {
		this.capitalSegurado = capitalSegurado;
	}


	public Empresa getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}


	public Double getNovoValorFaixaEtaria() {
		return novoValorFaixaEtaria;
	}


	public void setNovoValorFaixaEtaria(Double novoValorFaixaEtaria) {
		this.novoValorFaixaEtaria = novoValorFaixaEtaria;
	}


	public Integer getIdadeRealSegurado() {
		return idadeRealSegurado;
	}


	public void setIdadeRealSegurado(Integer idadeRealSegurado) {
		this.idadeRealSegurado = idadeRealSegurado;
	}


	public Integer getIdadePrevistaSegurado() {
		return idadePrevistaSegurado;
	}


	public void setIdadePrevistaSegurado(Integer idadePrevistaSegurado) {
		this.idadePrevistaSegurado = idadePrevistaSegurado;
	}


	public Date getDataAgenciamento() {
		return dataAgenciamento;
	}


	public void setDataAgenciamento(Date dataAgenciamento) {
		this.dataAgenciamento = dataAgenciamento;
	}


	public Double getDiferencaValores() {
		return diferencaValores;
	}


//	public Double getValorReangariado() {
//		return valorReangariado;
//	}
//
//
//	public void setValorReangariado(Double valorReangariado) {
//		this.valorReangariado = valorReangariado;
//	}


	public void setDiferencaValores(Double diferencaValores) {
		this.diferencaValores = diferencaValores;
	}


	public String getStatus() {
		return getSituacao().getNome();
	}


	public void setStatus(String status) {
		this.situacao = SituacaoProposta.valueOf(status);
	}


	public String getNumeroSorteio() {
		if( numerosorteio_collection != null &&  numerosorteio_collection.size() > 0 ){
			int ultimo = numerosorteio_collection.size() - 1;
			NumeroSorteio numero = (NumeroSorteio) numerosorteio_collection.toArray()[ultimo];
			return numero.getNumeroSorteio();
		}else{
			return numeroSorteio;
		}
		
	}


	public void setNumeroSorteio(String numeroSorteio) {
		
		this.numeroSorteio = numeroSorteio;
	}


	public String getCpfSeguradoFormatado() {
		return cpfSeguradoFormatado;
	}


	public void setCpfSeguradoFormatado(String cpfSeguradoFormatado) {
		this.cpfSeguradoFormatado = cpfSeguradoFormatado;
	}


	public String getCpfConjugeFormatado() {
		return cpfConjugeFormatado;
	}


	public void setCpfConjugeFormatado(String cpfConjugeFormatado) {
		this.cpfConjugeFormatado = cpfConjugeFormatado;
	}


	public Collection<HistoricoBeneficiario> getHistoricobeneficiario_collection() {
		return historicobeneficiario_collection;
	}


	public void setHistoricobeneficiario_collection(
			Collection<HistoricoBeneficiario> historicobeneficiario_collection) {
		this.historicobeneficiario_collection = historicobeneficiario_collection;
	}


	public Collection<HistoricoCapital> getHistoricocapital_collection() {
		return historicocapital_collection;
	}


	public void setHistoricocapital_collection(
			Collection<HistoricoCapital> historicocapital_collection) {
		this.historicocapital_collection = historicocapital_collection;
	}
	
	public Collection<HistoricoConjuge> getHistoricoconjuge_collection() {
		return historicoconjuge_collection;
	}


	public void setHistoricoconjuge_collection(
			Collection<HistoricoConjuge> historicoconjuge_collection) {
		this.historicoconjuge_collection = historicoconjuge_collection;
	}


	public Date getDataAlteracao() {
		return dataAlteracao;
	}


	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}


	public Date getDataExclusao() {
		return dataExclusao;
	}


	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}
	

	public Collection<HistoricoEnderecoSegurado> getHistoricoenderecosegurado_collection() {
		return historicoenderecosegurado_collection;
	}


	public void setHistoricoenderecosegurado_collection(
			Collection<HistoricoEnderecoSegurado> historicoenderecosegurado_collection) {
		this.historicoenderecosegurado_collection = historicoenderecosegurado_collection;
	}


	public String getNomeUsuarioAlteracao() {
		return nomeUsuarioAlteracao;
	}

	public void setNomeUsuarioAlteracao(String nomeUsuarioAlteracao) {
		this.nomeUsuarioAlteracao = nomeUsuarioAlteracao;
	}

	@Transient
	public SituacaoProposta getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoProposta situacao) {
		this.situacao = situacao;
	}


	public Collection<ComissaoAgenciador> getComissaoagenciador_collection() {
		return comissaoagenciador_collection;
	}


	public void setComissaoagenciador_collection(
			Collection<ComissaoAgenciador> comissaoagenciador_collection) {
		this.comissaoagenciador_collection = comissaoagenciador_collection;
	}


	public Double getValorPremioSegurado() {
		return valorPremioSegurado;
	}


	public void setValorPremioSegurado(Double valorPremioSegurado) {
		this.valorPremioSegurado = valorPremioSegurado;
	}


	public Double getValorPremioConjuge() {
		return valorPremioConjuge;
	}


	public void setValorPremioConjuge(Double valorPremioConjuge) {
		this.valorPremioConjuge = valorPremioConjuge;
	}


	public Double getValorCapitalSegurado() {
		return valorCapitalSegurado;
	}


	public void setValorCapitalSegurado(Double valorCapitalSegurado) {
		this.valorCapitalSegurado = valorCapitalSegurado;
	}


	public Double getValorCapitalConjuge() {
		return valorCapitalConjuge;
	}


	public void setValorCapitalConjuge(Double valorCapitalConjuge) {
		this.valorCapitalConjuge = valorCapitalConjuge;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Double getPremioSeguradoFuturo() {
		return premioSeguradoFuturo;
	}


	public void setPremioSeguradoFuturo(Double premioSeguradoFuturo) {
		this.premioSeguradoFuturo = premioSeguradoFuturo;
	}


	public Double getPremioConjugeFuturo() {
		return premioConjugeFuturo;
	}


	public void setPremioConjugeFuturo(Double premioConjugeFuturo) {
		this.premioConjugeFuturo = premioConjugeFuturo;
	}


	public Double getTotalPremiosFuturo() {
		return totalPremiosFuturo;
	}


	public void setTotalPremiosFuturo(Double totalPremiosFuturo) {
		this.totalPremiosFuturo = totalPremiosFuturo;
	}


	public Double getValorFuturoPremioSegurado() {
		return valorFuturoPremioSegurado;
	}


	public void setValorFuturoPremioSegurado(Double valorFuturoPremioSegurado) {
		this.valorFuturoPremioSegurado = valorFuturoPremioSegurado;
	}


	public Double getValorFuturoPremioConjuge() {
		return valorFuturoPremioConjuge;
	}


	public void setValorFuturoPremioConjuge(Double valorFuturoPremioConjuge) {
		this.valorFuturoPremioConjuge = valorFuturoPremioConjuge;
	}


	public Double getValorFuturoCapitalSegurado() {
		return valorFuturoCapitalSegurado;
	}


	public void setValorFuturoCapitalSegurado(Double valorFuturoCapitalSegurado) {
		this.valorFuturoCapitalSegurado = valorFuturoCapitalSegurado;
	}


	public Double getValorFuturoCapitalConjuge() {
		return valorFuturoCapitalConjuge;
	}


	public void setValorFuturoCapitalConjuge(Double valorFuturoCapitalConjuge) {
		this.valorFuturoCapitalConjuge = valorFuturoCapitalConjuge;
	}


	public DetalheCapitalFaixaEtaria getDetalheCapitalFaixaEtariaFuturo() {
		return detalheCapitalFaixaEtariaFuturo;
	}


	public void setDetalheCapitalFaixaEtariaFuturo(DetalheCapitalFaixaEtaria detalheCapitalFaixaEtariaFuturo) {
		this.detalheCapitalFaixaEtariaFuturo = detalheCapitalFaixaEtariaFuturo;
	}


	public Double getCapitalSeguradoFuturo() {
		return capitalSeguradoFuturo;
	}


	public void setCapitalSeguradoFuturo(Double capitalSeguradoFuturo) {
		this.capitalSeguradoFuturo = capitalSeguradoFuturo;
	}


	public Double getCapitalConjugeFuturo() {
		return capitalConjugeFuturo;
	}


	public void setCapitalConjugeFuturo(Double capitalConjugeFuturo) {
		this.capitalConjugeFuturo = capitalConjugeFuturo;
	}


	public Date getDataUltimaAtualizacaoSituacao() {
		return dataUltimaAtualizacaoSituacao;
	}


	public void setDataUltimaAtualizacaoSituacao(Date dataUltimaAtualizacaoSituacao) {
		this.dataUltimaAtualizacaoSituacao = dataUltimaAtualizacaoSituacao;
	}


	public String getComandoComissao() {
		return comandoComissao;
	}

	public String getComandoComissaoExibicao() {
		String comando = "Altera��o";
		if (getComandoComissao() != null) {
			if (getComandoComissao().equals("0")) {
				comando = "Inclus�o";
			} else if (getComandoComissao().equals("2")) {
				comando = "Reangaria��o";
			}
		}
		return comando;
	}
	
	public boolean isCasado() {
		if(getTipoProposta() != null) {
			if(getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO || getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50 || getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100 ) {
				return true;
			}
		}
		return false;
	}

	public void setComandoComissao(String comandoComissao) {
		this.comandoComissao = comandoComissao;
	}
	
	public String getCancelamento() {
		if(getDataCancelamento() == null) {
			return "-";
		}
		String dataFormatado = DateUtil.getFormattedDate(getDataCancelamento(), "dd/MM/yyyy");
		String motivoCancelamento = "";
		if(getMotivoCancelamento() != null) {
			motivoCancelamento = " - " + getMotivoCancelamento().getDescricao();
		}
		return dataFormatado + motivoCancelamento;
	}

	
	public String getUltimoPagamento() {
		Session session = HibernateUtil.currentSession();			
		OrgaoQuitacaoDAO orgaoQuitacaoDAO = new OrgaoQuitacaoDAO(session);
		OrgaoQuitacao orgaoQuitacao = orgaoQuitacaoDAO.getUltimoPagamento(getOrgao());
		
		if(orgaoQuitacao == null || getSituacao() == SituacaoProposta.CANCELADA) {
			return "-";
		} else {
			return orgaoQuitacao.getMesAno();
		}
	}


	/**
	 * @return the filtroSituacao
	 */
	public List<SituacaoProposta> getFiltroSituacao() {
		return filtroSituacao;
	}


	/**
	 * @param filtroSituacao the filtroSituacao to set
	 */
	public void setFiltroSituacao(List<SituacaoProposta> filtroSituacao) {
		this.filtroSituacao = filtroSituacao;
	}

	public String getLotacao() {
		return lotacao;
	}

	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}
	
	


	public String getNumeroEnderecoSegurado() {
		return numeroEnderecoSegurado;
	}


	public void setNumeroEnderecoSegurado(String numeroEnderecoSegurado) {
		this.numeroEnderecoSegurado = numeroEnderecoSegurado;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proposta other = (Proposta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public Integer getMesAniversario() {
		return mesAniversario;
	}


	public void setMesAniversario(Integer mesAniversario) {
		this.mesAniversario = mesAniversario;
	}


	public SuperAp getSuperAp() {
		return superAp;
	}


	public void setSuperAp(SuperAp superAp) {
		this.superAp = superAp;
	}
	

	/**
	 * @return the profissaoConjuge
	 */
	public String getProfissaoConjuge() {
		return profissaoConjuge;
	}


	/**
	 * @return the profissaoSegurado
	 */
	public String getProfissaoSegurado() {
		return profissaoSegurado;
	}


	/**
	 * @param profissaoConjuge the profissaoConjuge to set
	 */
	public void setProfissaoConjuge(String profissaoConjuge) {
		this.profissaoConjuge = profissaoConjuge;
	}


	/**
	 * @param profissaoSegurado the profissaoSegurado to set
	 */
	public void setProfissaoSegurado(String profissaoSegurado) {
		this.profissaoSegurado = profissaoSegurado;
	}


	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}


	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public String getPeriodicidadeFormatada() {
		if (getSuperAp() != null && getSuperAp().getPeriodicidade() != null) {
			periodicidadeFormatada = getSuperAp().getPeriodicidade().equals("0") ? "Mensal" : "Anual"; 
		} else {
			periodicidadeFormatada = "";
		}
		return periodicidadeFormatada;
	}
	
	public double getValorPremioTotal() {
		double total = getValorPremioSegurado() != null ? getValorPremioSegurado() : 0.0;
		total += getValorPremioConjuge() != null ? getValorPremioConjuge() : 0.0;
		return total;
	}


	

	
}
