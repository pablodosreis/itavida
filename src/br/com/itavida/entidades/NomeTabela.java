package br.com.itavida.entidades;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="nometabela")
public class NomeTabela implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String nomeTabela;

	private String nomeTabelaReduzido;

	private static final long serialVersionUID = 1L;

	public NomeTabela() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeTabela() {
		return this.nomeTabela;
	}

	public void setNomeTabela(String nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

	public String getNomeTabelaReduzido() {
		return this.nomeTabelaReduzido;
	}

	public void setNomeTabelaReduzido(String nomeTabelaReduzido) {
		this.nomeTabelaReduzido = nomeTabelaReduzido;
	}

}
