package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tipoocorrencia")
public class TipoOcorrencia implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String descricao;


	private String sinistroNegado;

	@OneToMany(mappedBy="motivoSinistro", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Set<HistoricoSinistro> historicosinistro_collection;

	@OneToMany(mappedBy="motivoSinistroNegado", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Set<HistoricoSinistro> historicosinistroNegado_collection;
	
	private static final long serialVersionUID = 1L;

	public TipoOcorrencia() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSinistroNegado() {
		return this.sinistroNegado;
	}

	public void setSinistroNegado(String sinistroNegado) {
		this.sinistroNegado = sinistroNegado;
	}

	public Set<HistoricoSinistro> getHistoricosinistro_collection() {
		return historicosinistro_collection;
	}

	public void setHistoricosinistro_collection(
			Set<HistoricoSinistro> historicosinistro_collection) {
		this.historicosinistro_collection = historicosinistro_collection;
	}

	public Set<HistoricoSinistro> getHistoricosinistroNegado_collection() {
		return historicosinistroNegado_collection;
	}

	public void setHistoricosinistroNegado_collection(
			Set<HistoricoSinistro> historicosinistroNegado_collection) {
		this.historicosinistroNegado_collection = historicosinistroNegado_collection;
	}
	
	
}
