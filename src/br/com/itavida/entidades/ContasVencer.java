package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sun.istack.internal.Nullable;

@Entity
@Table(name="contasvencer")
public class ContasVencer implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String contaPaga;

	private Date dataVencimento;
	
	private Double valorDespesa;

	@ManyToOne
	@JoinColumn(name="despesa")
	private Despesa despesa;

	@ManyToOne
	@JoinColumn(name="filial")
	private Filial filial;

	@ManyToOne
	@JoinColumn(name="funcionario")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name="grupoDespesa")
	private GrupoDespesa grupoDespesa;
	
	private Date dataPagamento;
	
	@Transient
	private Date dataPagamentoInicio;
	@Transient
	private Date dataPagamentoFim;
	
	@Transient
	private Date dataVencimentoInicio;
	@Transient
	private Date dataVencimentoFim;
	@Transient
	private boolean contaVencida;
	@Transient
	private Integer replicas;
	
	private static final long serialVersionUID = 1L;

	public ContasVencer() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContaPaga() {
		return this.contaPaga;
	}

	public void setContaPaga(String contaPaga) {
		this.contaPaga = contaPaga;
	}

	public Date getDataVencimento() {
		return this.dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Despesa getDespesa() {
		return this.despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public Double getValorDespesa() {
		return valorDespesa;
	}

	public void setValorDespesa(Double valorDespesa) {
		this.valorDespesa = valorDespesa;
	}

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public GrupoDespesa getGrupoDespesa() {
		return grupoDespesa;
	}

	public void setGrupoDespesa(GrupoDespesa grupoDespesa) {
		this.grupoDespesa = grupoDespesa;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Date getDataPagamentoInicio() {
		return dataPagamentoInicio;
	}

	public void setDataPagamentoInicio(Date dataPagamentoInicio) {
		this.dataPagamentoInicio = dataPagamentoInicio;
	}

	public Date getDataPagamentoFim() {
		return dataPagamentoFim;
	}

	public void setDataPagamentoFim(Date dataPagamentoFim) {
		this.dataPagamentoFim = dataPagamentoFim;
	}

	public Date getDataVencimentoInicio() {
		return dataVencimentoInicio;
	}

	public void setDataVencimentoInicio(Date dataVencimentoInicio) {
		this.dataVencimentoInicio = dataVencimentoInicio;
	}

	public Date getDataVencimentoFim() {
		return dataVencimentoFim;
	}

	public void setDataVencimentoFim(Date dataVencimentoFim) {
		this.dataVencimentoFim = dataVencimentoFim;
	}

	public boolean isContaVencida() {
		if( getDataVencimento() != null && getDataVencimento().before( new Date() ) ){
			return true;
		}else{
			return false;
		}
		
	}

	public void setContaVencida(boolean contaVencida) {
		this.contaVencida = contaVencida;
	}

	public Integer getReplicas() {
		return replicas;
	}

	public void setReplicas(Integer replicas) {
		this.replicas = replicas;
	}

	
	
}
