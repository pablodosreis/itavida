package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="motivocancelamento")
public class MotivoCancelamento implements Serializable {
	@Id
	private Integer id;

	private String descricao;

	@OneToMany(mappedBy="motivoCancelamento")
	private Set<Proposta> proposta_collection;

	private static final long serialVersionUID = 1L;

	public MotivoCancelamento() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId( Integer id ) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<Proposta> getProposta_collection() {
		return this.proposta_collection;
	}

	public void setProposta_collection(Set<Proposta> proposta_collection) {
		this.proposta_collection = proposta_collection;
	}

}
