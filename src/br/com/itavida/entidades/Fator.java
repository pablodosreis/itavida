package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="fator")
public class Fator implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Date dataInicio;
	
	private Double fator;
	
	private boolean processado;
	
	@ManyToOne
	@JoinColumn(name="idcapitalseguradofaixaetaria")
	private CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria;

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Double getFator() {
		return fator;
	}

	public void setFator(Double fator) {
		this.fator = fator;
	}

	public CapitalSeguradoFaixaEtaria getCapitalSeguradoFaixaEtaria() {
		return capitalSeguradoFaixaEtaria;
	}

	public void setCapitalSeguradoFaixaEtaria(
			CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria) {
		this.capitalSeguradoFaixaEtaria = capitalSeguradoFaixaEtaria;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isProcessado() {
		return processado;
	}

	public void setProcessado(boolean processado) {
		this.processado = processado;
	}
	
}
