package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="planilhafaturamento")
public class PlanilhaFaturamento implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String banco;

	private Date dataPagamento;

	private Date mesAnoVigencia;
	
	private Date mesAnoCobranca;

	private Integer numeroVidas;

	private Integer numeroVidasSemTaxa;

	private Double valorProlabore;
	
	private Double valorSemProlabore;
	
	private Double valorRelacao;

	private Double valorSeguradora;

	private Double valorTaxa;
	
	@Transient
	private Date mesAnoVigenciaInicio;
	@Transient
	private Date mesAnoVigenciaFim;	
	@Transient
	private Date mesAnoCobrancaInicio;
	@Transient
	private Date mesAnoCobrancaFim;	
	@Transient
	private Date dataPagamentoInicio;
	@Transient
	private Date dataPagamentoFim;
	@Transient
	private Integer estado;
	
	@ManyToOne
	@JoinColumn(name="orgao")
	private Orgao orgao;

	private static final long serialVersionUID = 1L;

	public PlanilhaFaturamento() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBanco() {
		return this.banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public Date getDataPagamento() {
		return this.dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}


	public Date getMesAnoVigencia() {
		return mesAnoVigencia;
	}

	public void setMesAnoVigencia(Date mesAnoVigencia) {
		this.mesAnoVigencia = mesAnoVigencia;
	}

	public Date getMesAnoCobranca() {
		return mesAnoCobranca;
	}

	public void setMesAnoCobranca(Date mesAnoCobranca) {
		this.mesAnoCobranca = mesAnoCobranca;
	}

	public Double getValorSemProlabore() {
		return valorSemProlabore;
	}

	public void setValorSemProlabore(Double valorSemProlabore) {
		this.valorSemProlabore = valorSemProlabore;
	}

	public Integer getNumeroVidas() {
		return this.numeroVidas;
	}

	public void setNumeroVidas(Integer numeroVidas) {
		this.numeroVidas = numeroVidas;
	}

	public Integer getNumeroVidasSemTaxa() {
		return this.numeroVidasSemTaxa;
	}

	public void setNumeroVidasSemTaxa(Integer numeroVidasSemTaxa) {
		this.numeroVidasSemTaxa = numeroVidasSemTaxa;
	}

	public Double getValorProlabore() {
		return this.valorProlabore;
	}

	public void setValorProlabore(Double valorProlabore) {
		this.valorProlabore = valorProlabore;
	}

	public Double getValorRelacao() {
		return this.valorRelacao;
	}

	public void setValorRelacao(Double valorRelacao) {
		this.valorRelacao = valorRelacao;
	}

	public Double getValorSeguradora() {
		return this.valorSeguradora;
	}

	public void setValorSeguradora(Double valorSeguradora) {
		this.valorSeguradora = valorSeguradora;
	}

	public Double getValorTaxa() {
		return this.valorTaxa;
	}

	public void setValorTaxa(Double valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public Orgao getOrgao() {
		return this.orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}


	public Date getDataPagamentoInicio() {
		return dataPagamentoInicio;
	}

	public void setDataPagamentoInicio(Date dataPagamentoInicio) {
		this.dataPagamentoInicio = dataPagamentoInicio;
	}

	public Date getDataPagamentoFim() {
		return dataPagamentoFim;
	}

	public void setDataPagamentoFim(Date dataPagamentoFim) {
		this.dataPagamentoFim = dataPagamentoFim;
	}

	public Date getMesAnoVigenciaInicio() {
		return mesAnoVigenciaInicio;
	}

	public void setMesAnoVigenciaInicio(Date mesAnoVigenciaInicio) {
		this.mesAnoVigenciaInicio = mesAnoVigenciaInicio;
	}

	public Date getMesAnoVigenciaFim() {
		return mesAnoVigenciaFim;
	}

	public void setMesAnoVigenciaFim(Date mesAnoVigenciaFim) {
		this.mesAnoVigenciaFim = mesAnoVigenciaFim;
	}

	public Date getMesAnoCobrancaInicio() {
		return mesAnoCobrancaInicio;
	}

	public void setMesAnoCobrancaInicio(Date mesAnoCobrancaInicio) {
		this.mesAnoCobrancaInicio = mesAnoCobrancaInicio;
	}

	public Date getMesAnoCobrancaFim() {
		return mesAnoCobrancaFim;
	}

	public void setMesAnoCobrancaFim(Date mesAnoCobrancaFim) {
		this.mesAnoCobrancaFim = mesAnoCobrancaFim;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	
	
}
