package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Pablo
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name="comissaoagenciador")
public class ComissaoAgenciador implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;
	
	@ManyToOne
	@JoinColumn(name="funcionario")
	private Funcionario funcionario;
	
	@Column(name = "comando")
	private String comando;
	
	@Column(name = "percentual")
	private Double percentual;
	
	@Column(name = "valorDescontado")
	private Double valorDescontado;
	
	@Column(name = "valorTotalPremio")
	private Double valorTotalPremio;	
	
	@Column(name = "valorComissao")
	private Double valorComissao;
	
	@Column(name = "dataCriacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;
	
	@Column(name = "nomeUsuario")
	private String nomeUsuario;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public String getComando() {
		return comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	public Double getValorDescontado() {
		return valorDescontado;
	}

	public void setValorDescontado(Double valorDescontado) {
		this.valorDescontado = valorDescontado;
	}

	public Double getValorTotalPremio() {
		return valorTotalPremio;
	}

	public void setValorTotalPremio(Double valorTotalPremio) {
		this.valorTotalPremio = valorTotalPremio;
	}

	public Double getValorComissao() {
		return valorComissao;
	}

	public void setValorComissao(Double valorComissao) {
		this.valorComissao = valorComissao;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	
	
}
