package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="siape")
public class Siape implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="codigoOrgao")
	private OrgaoSiape codigoOrgao = new OrgaoSiape();
	
	private String codigoUfPag;

	private String cpf;

	private Date dataHoraImportacao;

	private Date dataReferencia;

	private String matricula;

	private String nomeServidor;

	private String rubrica;

	private String sequencial;

	private String ufpag;
	
	private double valor;
	
	private String prazo;
	
	private String codigoUGSiafi;
	
	@Transient
	private Siape siapeB;
	@Transient
	private Date periodoReferenciaA;
	@Transient
	private Date periodoReferenciaB;
	
	
	private static final long serialVersionUID = 1L;

	
	public Siape() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OrgaoSiape getCodigoOrgao() {
		return codigoOrgao;
	}

	public void setCodigoOrgao(OrgaoSiape codigoOrgao) {
		this.codigoOrgao = codigoOrgao;
	}

	public String getCodigoUfPag() {
		return codigoUfPag;
	}

	public void setCodigoUfPag(String codigoUfPag) {
		this.codigoUfPag = codigoUfPag;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDataHoraImportacao() {
		return dataHoraImportacao;
	}

	public void setDataHoraImportacao(Date dataHoraImportacao) {
		this.dataHoraImportacao = dataHoraImportacao;
	}

	public Date getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(Date dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNomeServidor() {
		return nomeServidor;
	}

	public void setNomeServidor(String nomeServidor) {
		this.nomeServidor = nomeServidor;
	}

	public String getRubrica() {
		return rubrica;
	}

	public void setRubrica(String rubrica) {
		this.rubrica = rubrica;
	}

	public String getSequencial() {
		return sequencial;
	}

	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}

	public String getUfpag() {
		return ufpag;
	}

	public void setUfpag(String ufpag) {
		this.ufpag = ufpag;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getPrazo() {
		return prazo;
	}

	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}

	public String getCodigoUGSiafi() {
		return codigoUGSiafi;
	}

	public void setCodigoUGSiafi(String codigoUGSiafi) {
		this.codigoUGSiafi = codigoUGSiafi;
	}

	public Siape getSiapeB() {
		return siapeB;
	}

	public void setSiapeB(Siape siapeB) {
		this.siapeB = siapeB;
	}

	public Date getPeriodoReferenciaA() {
		return periodoReferenciaA;
	}

	public void setPeriodoReferenciaA(Date periodoReferenciaA) {
		this.periodoReferenciaA = periodoReferenciaA;
	}

	public Date getPeriodoReferenciaB() {
		return periodoReferenciaB;
	}

	public void setPeriodoReferenciaB(Date periodoReferenciaB) {
		this.periodoReferenciaB = periodoReferenciaB;
	}


	
	
}
