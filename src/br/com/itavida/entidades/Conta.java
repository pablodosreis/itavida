package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="conta")
public class Conta implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;

	private String descricao;

	@OneToMany(mappedBy="conta")
	private Set<Pagamento> pagamento_collection;

	private static final long serialVersionUID = 1L;

	public Conta() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<Pagamento> getPagamento_collection() {
		return this.pagamento_collection;
	}

	public void setPagamento_collection(Set<Pagamento> pagamento_collection) {
		this.pagamento_collection = pagamento_collection;
	}

}
