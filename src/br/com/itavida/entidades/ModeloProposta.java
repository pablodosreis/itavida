package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="modeloproposta")
public class ModeloProposta implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String descricao;

	@OneToMany(mappedBy="modeloProposta")
	private Set<TipoProposta> tipoproposta_collection;

	@OneToMany(mappedBy="modeloProposta")
	private Set<Proposta> proposta_collection;

	private static final long serialVersionUID = 1L;

	public ModeloProposta() {
		super();
	}
	
	public ModeloProposta(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<TipoProposta> getTipoproposta_collection() {
		return this.tipoproposta_collection;
	}

	public void setTipoproposta_collection(Set<TipoProposta> tipoproposta_collection) {
		this.tipoproposta_collection = tipoproposta_collection;
	}

	public Set<Proposta> getProposta_collection() {
		return this.proposta_collection;
	}

	public void setProposta_collection(Set<Proposta> proposta_collection) {
		this.proposta_collection = proposta_collection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModeloProposta other = (ModeloProposta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
