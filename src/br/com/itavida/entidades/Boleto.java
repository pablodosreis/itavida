package br.com.itavida.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.itavida.enums.SituacaoBoleto;
import br.com.itavida.enums.SituacaoProposta;

@Entity
@Table(name="boleto")
public class Boleto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "proposta_id")
	private Proposta proposta;
	
	@Temporal(TemporalType.DATE)
	@Column
	private Date referencia;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_previsao_pagamento")
	private Date dataPrevisaoPagamento;
	
	@Column(name = "data_pagamento")
	private Double dataPagamento;
	
	@Column(name = "valor")
	private Double valor;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "situacao", length = 15)
	private SituacaoBoleto situacao = SituacaoBoleto.NAO_GERADO;

	public Integer getId() {
		return id;
	}

	public Proposta getProposta() {
		return proposta;
	}

	public Date getReferencia() {
		return referencia;
	}

	public Date getDataPrevisaoPagamento() {
		return dataPrevisaoPagamento;
	}

	public Double getDataPagamento() {
		return dataPagamento;
	}

	public Double getValor() {
		return valor;
	}

	public SituacaoBoleto getSituacao() {
		return situacao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public void setReferencia(Date referencia) {
		this.referencia = referencia;
	}

	public void setDataPrevisaoPagamento(Date dataPrevisaoPagamento) {
		this.dataPrevisaoPagamento = dataPrevisaoPagamento;
	}

	public void setDataPagamento(Double dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setSituacao(SituacaoBoleto situacao) {
		this.situacao = situacao;
	}

	
	
	
}
