package br.com.itavida.entidades;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cidade")
public class Cidade implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String nom_cidade;

	@ManyToOne
	@JoinColumn(name="cod_estado")
	private Estado cod_estado=new Estado();

	private static final long serialVersionUID = 1L;

	public Cidade() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom_cidade() {
		return this.nom_cidade;
	}

	public void setNom_cidade(String nom_cidade) {
		this.nom_cidade = nom_cidade;
	}

	public Estado getCod_estado() {
		return this.cod_estado;
	}

	public void setCod_estado(Estado cod_estado) {
		this.cod_estado = cod_estado;
	}

	@Override
	public String toString() {
		String descricao = getNom_cidade();
		if(getCod_estado() != null) {
			descricao += "/" + getCod_estado().getSgl_estado();
		}
		return descricao;
	}
	


}
