package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tipoproposta")
public class TipoProposta implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String descricao;

	@ManyToOne
	@JoinColumn(name="modeloProposta")
	private ModeloProposta modeloProposta;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "comportamentoProposta")
	private ComportamentoPropostaEnum comportamento;

	@OneToMany(mappedBy="tipoProposta")
	private Set<Proposta> proposta_collection;

	private static final long serialVersionUID = 1L;

	public TipoProposta(Integer id) {
		this.id = id;
	}
	
	public TipoProposta() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ModeloProposta getModeloProposta() {
		return this.modeloProposta;
	}

	public void setModeloProposta(ModeloProposta modeloProposta) {
		this.modeloProposta = modeloProposta;
	}

	public Set<Proposta> getProposta_collection() {
		return this.proposta_collection;
	}

	public void setProposta_collection(Set<Proposta> proposta_collection) {
		this.proposta_collection = proposta_collection;
	}

	public ComportamentoPropostaEnum getComportamento() {
		return comportamento;
	}

	public void setComportamento(ComportamentoPropostaEnum comportamento) {
		this.comportamento = comportamento;
	}

}
