package br.com.itavida.entidades;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="arquivo_mensal")
public class ArquivoMensal implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_geracao", nullable = false)
	private Date dataGeracao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "competencia")
	private Date competencia;
	
	@Column(name = "usuario")
	private String usuario;
	
	@Column(name = "ativo", nullable = false)
	private boolean ativo;
	
	@OneToMany(mappedBy="arquivoMensal", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<ItemMensal> itens; 
	
	@Transient
	private String referenciaTxt;
	
	@Transient
	private String geracaoTxt;

	private static final long serialVersionUID = 1L;

	public ArquivoMensal() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the dataGeracao
	 */
	public Date getDataGeracao() {
		return dataGeracao;
	}

	/**
	 * @return the competencia
	 */
	public Date getCompetencia() {
		return competencia;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @return the ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param dataGeracao the dataGeracao to set
	 */
	public void setDataGeracao(Date dataGeracao) {
		this.dataGeracao = dataGeracao;
	}

	/**
	 * @param competencia the competencia to set
	 */
	public void setCompetencia(Date competencia) {
		this.competencia = competencia;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @param ativo the ativo to set
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
 
	/**
	 * @return the itens
	 */
	public List<ItemMensal> getItens() {
		return itens;
	}

	/**
	 * @param itens the itens to set
	 */
	public void setItens(List<ItemMensal> itens) {
		this.itens = itens;
	}

	public String getReferenciaTxt() {
		SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");
		return format.format(getCompetencia()); 
	}
	
	public String getGeracaoTxt() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return format.format(getDataGeracao()); 
	}

}
