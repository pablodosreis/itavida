package br.com.itavida.entidades;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="orgao_quitacao")
public class OrgaoQuitacao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Boolean quitado;
	private Date dataQuitacao;
	private Orgao orgao;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="quitado")
	public Boolean getQuitado() {
		return quitado;
	}
	public void setQuitado(Boolean quitado) {
		this.quitado = quitado;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrgaoQuitacao other = (OrgaoQuitacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Column(name="data_quitacao")
	public Date getDataQuitacao() {
		return dataQuitacao;
	}
	public void setDataQuitacao(Date dataQuitacao) {
		this.dataQuitacao = dataQuitacao;
	}
	
	@ManyToOne
	@JoinColumn(name="orgao_id")
	public Orgao getOrgao() {
		return orgao;
	}
	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}
	
	@Transient
	public String getMesAno(){
		SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");  
		return formatter.format( dataQuitacao);  
	}
	
	@Transient
	public String getLabelQuitado(){
		if(quitado){
			return "Sim";
		}
		return "N�o";
	}
	
}
