package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="despesa")
public class Despesa implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String nomeDespesa;

	private String tipoDespesa;
	
	@OneToMany(mappedBy="despesa")
	private Collection<ContasVencer> contasvencer_collection;
	
	private static final long serialVersionUID = 1L;

	public Despesa() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeDespesa() {
		return this.nomeDespesa;
	}

	public void setNomeDespesa(String nomeDespesa) {
		this.nomeDespesa = nomeDespesa;
	}

	public String getTipoDespesa() {
		return this.tipoDespesa;
	}

	public void setTipoDespesa(String tipoDespesa) {
		this.tipoDespesa = tipoDespesa;
	}

	public Collection<ContasVencer> getContasvencer_collection() {
		return contasvencer_collection;
	}

	public void setContasvencer_collection(Collection<ContasVencer> contasvencer_collection) {
		this.contasvencer_collection = contasvencer_collection;
	}

}
