package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="movimentacao_orgao_inadimplente")
public class MovimentacaoOrgaoInadimplente implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "orgao_id")
	private Orgao orgao;
	
	@Temporal(TemporalType.DATE)
	@Column
	private Date referencia;
	
	@Column
	private Double premio;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	
	/**
	 * @return the orgao
	 */
	public Orgao getOrgao() {		
		return orgao;
	}

	/**
	 * @param orgao the orgao to set
	 */
	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	/**
	 * @return the referencia
	 */
	public Date getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(Date referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the premio
	 */
	public Double getPremio() {
		return premio;
	}

	/**
	 * @param premio the premio to set
	 */
	public void setPremio(Double premio) {
		this.premio = premio;
	}
	
	
	

	
}
