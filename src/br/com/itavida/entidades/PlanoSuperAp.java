package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.itavida.util.DateUtil;

@Entity
@Table(name="plano_superap")
public class PlanoSuperAp implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String descricao;

	private Double morteAcidental;
	
	private Double morteInvalidezAcidente;
	
	private Double safIndividual;
	
	private String assistencia;
	
	private Double valorSorteio;
	
	private Double premioMensal;
	
	private Double premioAnual;

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Double getMorteAcidental() {
		return morteAcidental;
	}

	public void setMorteAcidental(Double morteAcidental) {
		this.morteAcidental = morteAcidental;
	}

	public Double getMorteInvalidezAcidente() {
		return morteInvalidezAcidente;
	}

	public void setMorteInvalidezAcidente(Double morteInvalidezAcidente) {
		this.morteInvalidezAcidente = morteInvalidezAcidente;
	}

	public Double getSafIndividual() {
		return safIndividual;
	}

	public void setSafIndividual(Double safIndividual) {
		this.safIndividual = safIndividual;
	}

	public String getAssistencia() {
		return assistencia;
	}

	public void setAssistencia(String assistencia) {
		this.assistencia = assistencia;
	}

	public Double getValorSorteio() {
		return valorSorteio;
	}

	public void setValorSorteio(Double valorSorteio) {
		this.valorSorteio = valorSorteio;
	}

	public Double getPremioMensal() {
		return premioMensal;
	}

	public void setPremioMensal(Double premioMensal) {
		this.premioMensal = premioMensal;
	}

	public Double getPremioAnual() {
		return premioAnual;
	}

	public void setPremioAnual(Double premioAnual) {
		this.premioAnual = premioAnual;
	}

	
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanoSuperAp other = (PlanoSuperAp) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
}
