package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.itavida.util.DateUtil;

@Entity
@Table(name="empresa")
public class Empresa implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "nomeEmpresa")
	private String nomeEmpresa;
	
	@Column(name = "num_apolice")
	private String numApolice;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "aniversario")
	private Date aniversario;
	
	@Column(name = "ajuste_ativo")
	private boolean ajusteAtivo;
	
	@Column(name = "ajuste_14_65")
	private Double ajuste14a65;
	
	@Column(name = "ajuste_66_mais")
	private Double ajuste66mais;
	
	@Column(name = "ipca_ultimo_periodo")
	private Double ipcaUltimoPeriodo;
	
	@OneToMany(mappedBy="empresa", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@OrderBy(value = "dataAjuste desc")
	private List<HistoricoAjusteEmpresa> listaHistoricoAjuste;
	
	public Empresa() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getNumApolice() {
		return numApolice;
	}

	public void setNumApolice(String numApolice) {
		this.numApolice = numApolice;
	}

	public Date getAniversario() {
		return aniversario;
	}

	public void setAniversario(Date aniversario) {
		this.aniversario = aniversario;
	}

	public boolean isAjusteAtivo() {
		return ajusteAtivo;
	}

	public void setAjusteAtivo(boolean ajusteAtivo) {
		this.ajusteAtivo = ajusteAtivo;
	}

	public Double getAjuste14a65() {
		return ajuste14a65;
	}

	public void setAjuste14a65(Double ajuste14a65) {
		this.ajuste14a65 = ajuste14a65;
	}

	public Double getAjuste66mais() {
		return ajuste66mais;
	}

	public void setAjuste66mais(Double ajuste66mais) {
		this.ajuste66mais = ajuste66mais;
	}
	
	public Double getIpcaUltimoPeriodo() {
		return ipcaUltimoPeriodo;
	}

	public void setIpcaUltimoPeriodo(Double ipcaUltimoPeriodo) {
		this.ipcaUltimoPeriodo = ipcaUltimoPeriodo;
	}

	public List<HistoricoAjusteEmpresa> getListaHistoricoAjuste() {
		return listaHistoricoAjuste;
	}

	public void setListaHistoricoAjuste(
			List<HistoricoAjusteEmpresa> listaHistoricoAjuste) {
		this.listaHistoricoAjuste = listaHistoricoAjuste;
	}
	
	public Date getProximaAtualizacao() {
		if(aniversario != null) {
			Calendar hoje = Calendar.getInstance();
			
			Calendar calendarAniversario = Calendar.getInstance();
			calendarAniversario.setTime(aniversario);
			
			Calendar proximaAtualizacao = Calendar.getInstance();
			proximaAtualizacao.setTime(aniversario);
			proximaAtualizacao.set(Calendar.YEAR, hoje.get(Calendar.YEAR));
			
			DateUtil.zerarHorarMinutoSegundo(hoje);
			DateUtil.zerarHorarMinutoSegundo(proximaAtualizacao);
			
			//se a data da proxima atualização menor ou igual a hoje, adiciona um ano
			if(DateUtil.diffEmDias(proximaAtualizacao.getTime(), hoje.getTime()) <= 0) {
				proximaAtualizacao.add(Calendar.YEAR, 1);
			}
			
			return proximaAtualizacao.getTime();
		}
		return null;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
