package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.itavida.util.DateUtil;

@Entity
@Table(name="movimentacao_proposta_atrasada")
public class MovimentacaoPropostaAtrasada implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "proposta_id")
	private Proposta proposta;
	
	@Temporal(TemporalType.DATE)
	@Column
	private Date referencia;
	
	@Column
	private Double premio;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "periodo_quitacao_anterior")
	private Date periodoQuitacaoAnterior;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the proposta
	 */
	public Proposta getProposta() {
		return proposta;
	}

	/**
	 * @param proposta the proposta to set
	 */
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	/**
	 * @return the referencia
	 */
	public Date getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(Date referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the premio
	 */
	public Double getPremio() {
		return premio;
	}

	/**
	 * @param premio the premio to set
	 */
	public void setPremio(Double premio) {
		this.premio = premio;
	}

	/**
	 * @return the periodoQuitacaoAnterior
	 */
	public Date getPeriodoQuitacaoAnterior() {
		return periodoQuitacaoAnterior;
	}

	/**
	 * @param periodoQuitacaoAnterior the periodoQuitacaoAnterior to set
	 */
	public void setPeriodoQuitacaoAnterior(Date periodoQuitacaoAnterior) {
		this.periodoQuitacaoAnterior = periodoQuitacaoAnterior;
	}
	
	public String getPeriodoQuitacaoFormatado() {
		return DateUtil.getFormattedDate(getPeriodoQuitacaoAnterior(), "MM/yyyy");
	}
	
	

	
}
