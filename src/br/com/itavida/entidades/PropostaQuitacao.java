package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="propostaquitacao")
public class PropostaQuitacao implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;
	
	@OneToMany(mappedBy="propostaQuitacao", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<Quitacao> quitacao_collection;
	
	@ManyToOne
	@JoinColumn(name="empresa")
	private Empresa empresa;
	
	@Column(name = "dataReferencia")
	private Date dataReferencia;
	
	@Column(name = "valorTotal")
	private Double valorTotal;

	private static final long serialVersionUID = 1L;

	public PropostaQuitacao() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

		public Proposta getProposta() {
		return this.proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public Collection<Quitacao> getQuitacao_collection() {
		return quitacao_collection;
	}

	public void setQuitacao_collection(Collection<Quitacao> quitacao_collection) {
		this.quitacao_collection = quitacao_collection;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Date getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(Date dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public PropostaQuitacao(Proposta proposta, Empresa empresa,
			Date dataReferencia, Double valorTotal) {
		super();
		this.proposta = proposta;
		this.empresa = empresa;
		this.dataReferencia = dataReferencia;
		this.valorTotal = valorTotal;
		this.setQuitacao_collection(new ArrayList<Quitacao>());
	}	
 
}
