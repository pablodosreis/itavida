package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="pagamento")
public class Pagamento implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private Date dataCancelamento;

	private Date dataDocumento;

	private String motivoCancelamento;

	private String nominal;

	private String numeroDocumento;

	private Double valor;

	@ManyToOne
	@JoinColumn(name="conta")
	private Conta conta;

	@ManyToOne
	@JoinColumn(name="tipoDocumento")
	private TipoDocumento tipoDocumento;

	@OneToMany(mappedBy="pagamento", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<DetalhePagamento> detalhepagamento_collection;

	@Transient
	private Date dataDocumentoInicio;
	@Transient
	private Date dataDocumentoFim;
	@Transient
	private Double valorInicio;
	@Transient
	private Double valorFim;
	@Transient
	private Date dataCancelamentoInicio;
	@Transient
	private Date dataCancelamentoFim;
	@Transient
	private DetalhePagamento detalhePagamentoFiltro;
	
	private static final long serialVersionUID = 1L;

	public Pagamento() {
		super();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataCancelamento() {
		return this.dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public Date getDataDocumento() {
		return this.dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}


	public String getMotivoCancelamento() {
		return this.motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public String getNominal() {
		return this.nominal;
	}

	public void setNominal(String nominal) {
		this.nominal = nominal;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Conta getConta() {
		return this.conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public TipoDocumento getTipoDocumento() {
		return this.tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Collection<DetalhePagamento> getDetalhepagamento_collection() {
		return this.detalhepagamento_collection;
	}

	public void setDetalhepagamento_collection(Collection<DetalhePagamento> detalhepagamento_collection) {
		this.detalhepagamento_collection = detalhepagamento_collection;
	}

	public Date getDataDocumentoInicio() {
		return dataDocumentoInicio;
	}

	public void setDataDocumentoInicio(Date dataDocumentoInicio) {
		this.dataDocumentoInicio = dataDocumentoInicio;
	}

	public Date getDataDocumentoFim() {
		return dataDocumentoFim;
	}

	public void setDataDocumentoFim(Date dataDocumentoFim) {
		this.dataDocumentoFim = dataDocumentoFim;
	}

	public Double getValorInicio() {
		return valorInicio;
	}

	public void setValorInicio(Double valorInicio) {
		this.valorInicio = valorInicio;
	}

	public Double getValorFim() {
		return valorFim;
	}

	public void setValorFim(Double valorFim) {
		this.valorFim = valorFim;
	}

	public Date getDataCancelamentoInicio() {
		return dataCancelamentoInicio;
	}

	public void setDataCancelamentoInicio(Date dataCancelamentoInicio) {
		this.dataCancelamentoInicio = dataCancelamentoInicio;
	}

	public Date getDataCancelamentoFim() {
		return dataCancelamentoFim;
	}

	public void setDataCancelamentoFim(Date dataCancelamentoFim) {
		this.dataCancelamentoFim = dataCancelamentoFim;
	}

	public DetalhePagamento getDetalhePagamentoFiltro() {
		return detalhePagamentoFiltro;
	}

	public void setDetalhePagamentoFiltro(DetalhePagamento detalhePagamentoFiltro) {
		this.detalhePagamentoFiltro = detalhePagamentoFiltro;
	}

		
	
}
