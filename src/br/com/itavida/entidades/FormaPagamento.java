package br.com.itavida.entidades;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.itavida.enums.CartaoCreditoEnum;
import br.com.itavida.enums.TipoPagamentoEnum;


@Entity
@Table(name="formapagamento")
public class FormaPagamento implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Enumerated
	@Column(name = "tipo")
	private TipoPagamentoEnum tipo;
	
	@Enumerated
	@Column(name = "cartao_credito")
	private CartaoCreditoEnum cartaoCredito;
	
	@ManyToOne
	@JoinColumn(name = "banco_id")
	private Banco banco;
	
	@Column(name = "nome_titular")
	private String nomeTitular;
	
	@Column(name = "cpf_titular")
	private String cpfTitular;
	
	@Column(name = "numero_agencia")
	private String numeroAgencia;
	
	@Column(name = "numero_conta")
	private String numeroConta;
	
	@Column(name = "numero_cartao")
	private String numeroCartao;
	
	@Column(name = "validade_cartao")
	private String validadeCartao;
	
	@Column(name = "parentesco")
	private String parentesco;
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TipoPagamentoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoPagamentoEnum tipo) {
		this.tipo = tipo;
	}

	public CartaoCreditoEnum getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCreditoEnum cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public String getCpfTitular() {
		return cpfTitular;
	}

	public void setCpfTitular(String cpfTitular) {
		this.cpfTitular = cpfTitular;
	}

	public String getNumeroAgencia() {
		return numeroAgencia;
	}

	public void setNumeroAgencia(String numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}

	public String getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public String getValidadeCartao() {
		return validadeCartao;
	}

	public void setValidadeCartao(String validadeCartao) {
		this.validadeCartao = validadeCartao;
	}

	public String getParentesco() {
		return parentesco;
	}

	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}
}
