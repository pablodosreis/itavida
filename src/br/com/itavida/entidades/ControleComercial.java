package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="controlecomercial")
public class ControleComercial implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;

	private Double custos;

	private Date dataVisitaChegada;

	private Date dataVisitaSaida;

	private Integer numeroFuncionarios;

	private Integer producao;
	
	@Transient
	private Integer numeroVidasAtivas;
	
	@Transient
	private Date dataVisitaChegadaInicio;
	@Transient
	private Date dataVisitaChegadaFim;
	@Transient
	private Date dataVisitaSaidaInicio;
	@Transient
	private Date dataVisitaSaidaFim;
	@Transient
	private Integer numeroFuncionariosInicio;
	@Transient
	private Integer numeroFuncionariosFim;
	
	@ManyToOne
	@JoinColumn(name="funcionario")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name="orgao")
	private Orgao orgao;

	private static final long serialVersionUID = 1L;

	public ControleComercial() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getCustos() {
		return this.custos;
	}

	public void setCustos(Double custos) {
		this.custos = custos;
	}

	public Date getDataVisitaChegada() {
		return this.dataVisitaChegada;
	}

	public void setDataVisitaChegada(Date dataVisitaChegada) {
		this.dataVisitaChegada = dataVisitaChegada;
	}

	public Date getDataVisitaSaida() {
		return this.dataVisitaSaida;
	}

	public void setDataVisitaSaida(Date dataVisitaSaida) {
		this.dataVisitaSaida = dataVisitaSaida;
	}

	public Integer getNumeroFuncionarios() {
		return this.numeroFuncionarios;
	}

	public void setNumeroFuncionarios(Integer numeroFuncionarios) {
		this.numeroFuncionarios = numeroFuncionarios;
	}

	public Integer getProducao() {
		return this.producao;
	}

	public void setProducao(Integer producao) {
		this.producao = producao;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Orgao getOrgao() {
		return this.orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public Date getDataVisitaChegadaInicio() {
		return dataVisitaChegadaInicio;
	}

	public void setDataVisitaChegadaInicio(Date dataVisitaChegadaInicio) {
		this.dataVisitaChegadaInicio = dataVisitaChegadaInicio;
	}

	public Date getDataVisitaChegadaFim() {
		return dataVisitaChegadaFim;
	}

	public void setDataVisitaChegadaFim(Date dataVisitaChegadaFim) {
		this.dataVisitaChegadaFim = dataVisitaChegadaFim;
	}

	public Date getDataVisitaSaidaInicio() {
		return dataVisitaSaidaInicio;
	}

	public void setDataVisitaSaidaInicio(Date dataVisitaSaidaInicio) {
		this.dataVisitaSaidaInicio = dataVisitaSaidaInicio;
	}

	public Date getDataVisitaSaidaFim() {
		return dataVisitaSaidaFim;
	}

	public void setDataVisitaSaidaFim(Date dataVisitaSaidaFim) {
		this.dataVisitaSaidaFim = dataVisitaSaidaFim;
	}

	public Integer getNumeroFuncionariosInicio() {
		return numeroFuncionariosInicio;
	}

	public void setNumeroFuncionariosInicio(Integer numeroFuncionariosInicio) {
		this.numeroFuncionariosInicio = numeroFuncionariosInicio;
	}

	public Integer getNumeroFuncionariosFim() {
		return numeroFuncionariosFim;
	}

	public void setNumeroFuncionariosFim(Integer numeroFuncionariosFim) {
		this.numeroFuncionariosFim = numeroFuncionariosFim;
	}

	public Integer getNumeroVidasAtivas() {
		return numeroVidasAtivas;
	}

	public void setNumeroVidasAtivas(Integer numeroVidasAtivas) {
		this.numeroVidasAtivas = numeroVidasAtivas;
	}

	
}
