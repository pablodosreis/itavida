package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="estado")
public class Estado implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer cod_estado;

	private String nom_estado;

	private String sgl_estado;

	@ManyToOne
	@JoinColumn(name="cod_pais")
	private Pais cod_pais;

	@OneToMany(mappedBy="cod_estado")
	private Set<Cidade> cidade_collection;

	private static final long serialVersionUID = 1L;

	public Estado() {
		super();
	}

	public Integer getCod_estado() {
		return this.cod_estado;
	}

	public void setCod_estado(Integer cod_estado) {
		this.cod_estado = cod_estado;
	}

	public String getNom_estado() {
		return this.nom_estado;
	}

	public void setNom_estado(String nom_estado) {
		this.nom_estado = nom_estado;
	}

	public String getSgl_estado() {
		return this.sgl_estado;
	}

	public void setSgl_estado(String sgl_estado) {
		this.sgl_estado = sgl_estado;
	}

	public Pais getCod_pais() {
		return this.cod_pais;
	}

	public void setCod_pais(Pais cod_pais) {
		this.cod_pais = cod_pais;
	}

	public Set<Cidade> getCidade_collection() {
		return this.cidade_collection;
	}

	public void setCidade_collection(Set<Cidade> cidade_collection) {
		this.cidade_collection = cidade_collection;
	}

}
