package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="historicosinistro")
public class HistoricoSinistro implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Date dataEnvioSeguradora;

	private Date dataPagamento;

	private Date dataSinistro;

	private String numeroSinistro;
	
	private Double valorSinistro;

	@Lob
	private byte[] observacao;

	private String seguradora;
	
	@OneToMany(mappedBy="historicoSinistro")
	private List<BeneficiarioSinistro> beneficiarios;


	@Lob
	private byte[] situacaoProcesso;

	private String tipoPagamento;

	private String tipoSinistroAuxilioFuneral;

	private String tipoSinistroConjuge;

	private String tipoSinistroSegurado;

	@ManyToOne
	@JoinColumn(name="motivoSinistro")
	private TipoOcorrencia motivoSinistro;

	@ManyToOne
	@JoinColumn(name="motivoSinistroNegado")
	private TipoOcorrencia motivoSinistroNegado;

	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;
	
	private Double valorFuneral;
	
	private Double valorCestaBasica;
	
	@ManyToOne
	@JoinColumn(name="empresa")
	private Empresa empresa;

	@Transient
	private Date dataSinistroInicio;
	@Transient
	private Date dataSinistroFim;	
	@Transient
	private String situacaoProcessoString;
	@Transient
	private String observacaoString;
	@Transient
	private Double valorPagoSinistro;
	@Transient
	private Double valorAuxFuneral;
	
	private static final long serialVersionUID = 1L;

	public HistoricoSinistro() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataEnvioSeguradora() {
		return this.dataEnvioSeguradora;
	}

	public void setDataEnvioSeguradora(Date dataEnvioSeguradora) {
		this.dataEnvioSeguradora = dataEnvioSeguradora;
	}

	public Date getDataPagamento() {
		return this.dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Date getDataSinistro() {
		return this.dataSinistro;
	}

	public void setDataSinistro(Date dataSinistro) {
		this.dataSinistro = dataSinistro;
	}

	public String getNumeroSinistro() {
		return this.numeroSinistro;
	}

	public void setNumeroSinistro(String numeroSinistro) {
		this.numeroSinistro = numeroSinistro;
	}

	public byte[] getObservacao() {
		return this.observacao;
	}

	public void setObservacao(byte[] observacao) {
		this.observacao = observacao;
	}

	public String getSeguradora() {
		return this.seguradora;
	}

	public void setSeguradora(String seguradora) {
		this.seguradora = seguradora;
	}

	public byte[] getSituacaoProcesso() {
		return this.situacaoProcesso;
	}

	public void setSituacaoProcesso(byte[] situacaoProcesso) {
		this.situacaoProcesso = situacaoProcesso;
	}


	public String getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(String tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public String getTipoSinistroAuxilioFuneral() {
		return this.tipoSinistroAuxilioFuneral;
	}

	public void setTipoSinistroAuxilioFuneral(String tipoSinistroAuxilioFuneral) {
		this.tipoSinistroAuxilioFuneral = tipoSinistroAuxilioFuneral;
	}

	public String getTipoSinistroConjuge() {
		return this.tipoSinistroConjuge;
	}

	public void setTipoSinistroConjuge(String tipoSinistroConjuge) {
		this.tipoSinistroConjuge = tipoSinistroConjuge;
	}

	public String getTipoSinistroSegurado() {
		return this.tipoSinistroSegurado;
	}

	public void setTipoSinistroSegurado(String tipoSinistroSegurado) {
		this.tipoSinistroSegurado = tipoSinistroSegurado;
	}

	public TipoOcorrencia getMotivoSinistro() {
		return this.motivoSinistro;
	}

	public void setMotivoSinistro(TipoOcorrencia motivoSinistro) {
		this.motivoSinistro = motivoSinistro;
	}

	public TipoOcorrencia getMotivoSinistroNegado() {
		return this.motivoSinistroNegado;
	}

	public void setMotivoSinistroNegado(TipoOcorrencia motivoSinistroNegado) {
		this.motivoSinistroNegado = motivoSinistroNegado;
	}

	public Proposta getProposta() {
		return this.proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	/**
	 * @return the dataSinistroInicio
	 */
	public Date getDataSinistroInicio() {
		return dataSinistroInicio;
	}

	/**
	 * @param dataSinistroInicio the dataSinistroInicio to set
	 */
	public void setDataSinistroInicio(Date dataSinistroInicio) {
		this.dataSinistroInicio = dataSinistroInicio;
	}

	/**
	 * @return the dataSinistroFim
	 */
	public Date getDataSinistroFim() {
		return dataSinistroFim;
	}

	/**
	 * @param dataSinistroFim the dataSinistroFim to set
	 */
	public void setDataSinistroFim(Date dataSinistroFim) {
		this.dataSinistroFim = dataSinistroFim;
	}

	public String getSituacaoProcessoString() {
		return situacaoProcessoString;
	}

	public void setSituacaoProcessoString(String situacaoProcessoString) {
		this.situacaoProcessoString = situacaoProcessoString;
	}

	public String getObservacaoString() {
		return observacaoString;
	}

	public void setObservacaoString(String observacaoString) {
		this.observacaoString = observacaoString;
	}

	public Double getValorPagoSinistro() {
		return valorPagoSinistro;
	}

	public void setValorPagoSinistro(Double valorPagoSinistro) {
		this.valorPagoSinistro = valorPagoSinistro;
	}

	public Double getValorAuxFuneral() {
		return valorAuxFuneral;
	}

	public void setValorAuxFuneral(Double valorAuxFuneral) {
		this.valorAuxFuneral = valorAuxFuneral;
	}

	public Double getValorFuneral() {
		return valorFuneral;
	}

	public void setValorFuneral(Double valorFuneral) {
		this.valorFuneral = valorFuneral;
	}

	public Double getValorSinistro() {
		return valorSinistro;
	}

	public void setValorSinistro(Double valorSinistro) {
		this.valorSinistro = valorSinistro;
	}

	public List<BeneficiarioSinistro> getBeneficiarios() {
		return beneficiarios;
	}

	public void setBeneficiarios(List<BeneficiarioSinistro> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Double getValorCestaBasica() {
		return valorCestaBasica;
	}

	public void setValorCestaBasica(Double valorCestaBasica) {
		this.valorCestaBasica = valorCestaBasica;
	}
	
	

}
