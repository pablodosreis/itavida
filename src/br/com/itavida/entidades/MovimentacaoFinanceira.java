package br.com.itavida.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.itavida.enums.SituacaoBoleto;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.util.DateUtil;

@Entity
@Table(name="movimentacao_financeira")
public class MovimentacaoFinanceira {

	/*Dados b�sicos da movimenta��o*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "proposta_id")
	private Proposta proposta;
	
	@Temporal(TemporalType.DATE)
	@Column
	private Date referencia;
	
	/*Dados do pagamento*/
	
	@Column(name = "data_pagamento")
	private Date dataPagamento;
	
	@Column(name = "valor_pago")
	private Double valor;
	
	@Column(name = "numero_parcela")
	private Integer numeroParcela;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_comissao")
	private Date dataPagamentoComissao;
	
	@Column(name = "valor_comissao")
	private Double valorComissao;
	
	/*Dados de pagamento via boleto*/
	@Enumerated(EnumType.STRING)
	@Column(name = "situacao_boleto", length = 40)
	private SituacaoBoleto situacaoBoleto = SituacaoBoleto.NAO_GERADO;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_previsao_boleto")
	private Date dataPrevisaoBoleto;
	
	@Column(name = "observacao")
	private String observacao;
	
	
	/*Dados da Proposta no momento do pagamento*/
	@Enumerated(EnumType.STRING)
	@Column(name = "situacao", length = 15)
	private SituacaoProposta situacao;
	
	@ManyToOne
	@JoinColumn(name="empresa_id")
	private Empresa empresa;
	
	@ManyToOne
	@JoinColumn(name="orgao_id")
	private Orgao orgao;
	
	@Column(name = "idade")
	private Integer idade;
	
	@Column(name = "sexo")
	private String sexo;
	
	@Column(name = "valor_premio")
	private Double valorPremio;
	
	@Column(name = "valor_premio_conjuge")
	private Double valorPremioConjuge;
	
	@Column(name = "valor_capital")
	private Double valorCapital;
	
	@Column(name = "valor_capital_conjuge")
	private Double valorCapitalConjuge;
	
	/*Campos para visualiza��o*/
	
	@Transient
	private String dataPagamentoFormatada;
	
	@Transient
	private String dataComissaoFormatada;
	
	@Transient
	private Date dataPrevisaoBoletoFormatada;
	
	@Transient
	private String periodicidadeFormatada;

	public Integer getId() {
		return id;
	}

	public Proposta getProposta() {
		return proposta;
	}

	public Date getReferencia() {
		return referencia;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public Double getValor() {
		return valor;
	}

	public Integer getNumeroParcela() {
		return numeroParcela;
	}

	public SituacaoProposta getSituacao() {
		return situacao;
	}

	public Date getDataPagamentoComissao() {
		return dataPagamentoComissao;
	}

	public Double getValorComissao() {
		return valorComissao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public void setReferencia(Date referencia) {
		this.referencia = referencia;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public void setValor(Double valorMovimentacao) {
		this.valor = valorMovimentacao;
	}

	public void setNumeroParcela(Integer numeroParcela) {
		this.numeroParcela = numeroParcela;
	}

	public void setSituacao(SituacaoProposta situacao) {
		this.situacao = situacao;
	}

	public void setDataPagamentoComissao(Date dataPagamentoComissao) {
		this.dataPagamentoComissao = dataPagamentoComissao;
	}

	public void setValorComissao(Double valorComissao) {
		this.valorComissao = valorComissao;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public Orgao getOrgao() {
		return orgao;
	}

	public Integer getIdade() {
		return idade;
	}

	public String getSexo() {
		return sexo;
	}

	public Double getValorPremio() {
		return valorPremio;
	}

	public Double getValorPremioConjuge() {
		return valorPremioConjuge;
	}

	public Double getValorCapital() {
		return valorCapital;
	}

	public Double getValorCapitalConjuge() {
		return valorCapitalConjuge;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public void setValorPremio(Double valorPremio) {
		this.valorPremio = valorPremio;
	}

	public void setValorPremioConjuge(Double valorPremioConjuge) {
		this.valorPremioConjuge = valorPremioConjuge;
	}

	public void setValorCapital(Double valorCapital) {
		this.valorCapital = valorCapital;
	}

	public void setValorCapitalConjuge(Double valorCapitalConjuge) {
		this.valorCapitalConjuge = valorCapitalConjuge;
	}

	public String getDataPagamentoFormatada() {
		return DateUtil.getFormattedDate(getDataPagamento(), "dd/MM/yyyy");
	}

	public void setDataPagamentoFormatada(String dataPagamentoFormatada) {
		this.dataPagamentoFormatada = dataPagamentoFormatada;
	}

	public String getDataComissaoFormatada() {
		return DateUtil.getFormattedDate(getDataPagamentoComissao(), "dd/MM/yyyy");
	}
	
	public String getDataPrevisaoBoletoFormatada() {
		return DateUtil.getFormattedDate(getDataPrevisaoBoleto(), "dd/MM/yyyy");
	}

	public void setDataComissaoFormatada(String dataComissaoFormatada) {
		this.dataComissaoFormatada = dataComissaoFormatada;
	}

	public SituacaoBoleto getSituacaoBoleto() {
		return situacaoBoleto;
	}

	public void setSituacaoBoleto(SituacaoBoleto situacaoBoleto) {
		this.situacaoBoleto = situacaoBoleto;
	}

	public Date getDataPrevisaoBoleto() {
		return dataPrevisaoBoleto;
	}

	public void setDataPrevisaoBoleto(Date dataPrevisaoBoleto) {
		this.dataPrevisaoBoleto = dataPrevisaoBoleto;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setDataPrevisaoBoletoFormatada(Date dataPrevisaoBoletoFormatada) {
		this.dataPrevisaoBoletoFormatada = dataPrevisaoBoletoFormatada;
	}

	public String getPeriodicidadeFormatada() {
		return getProposta().getPeriodicidadeFormatada();
	}

	public void setPeriodicidadeFormatada(String periodicidadeFormatada) {
		this.periodicidadeFormatada = periodicidadeFormatada;
	}
	
	

	
	
}
