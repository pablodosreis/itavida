package br.com.itavida.entidades;

public enum ComportamentoPropostaEnum {

	INDIVIDUAL,
	CASADO,
	CASADO50,
	CASADO100;
	
}
