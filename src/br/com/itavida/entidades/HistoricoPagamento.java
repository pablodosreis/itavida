package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="historicopagamento")
public class HistoricoPagamento implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_referencia")
	private Date dataReferencia;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_pagamento")
	private Date dataPagamento;
	
	@Column(name = "valor_premio")
	private Double valorPremio;
	
	@Column(name = "valor_premio_conjuge")
	private Double valorPremioConjuge;
	
	@Column(name = "valor_capital")
	private Double valorCapital;
	
	@Column(name = "valor_capital_conjuge")
	private Double valorCapitalConjuge;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_cadastro")
	private Date dataCadastro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
	private Date dataAlteracao;
	
	@Column(name = "usuario_cadastro")
	private String usuarioCadastro;
	
	@Column(name = "usuario_alteracao")
	private String usuarioAlteracao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(Date dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Double getValorPremio() {
		return valorPremio;
	}

	public void setValorPremio(Double valorPremio) {
		this.valorPremio = valorPremio;
	}

	public Double getValorPremioConjuge() {
		return valorPremioConjuge;
	}

	public void setValorPremioConjuge(Double valorPremioConjuge) {
		this.valorPremioConjuge = valorPremioConjuge;
	}

	public Double getValorCapital() {
		return valorCapital;
	}

	public void setValorCapital(Double valorCapital) {
		this.valorCapital = valorCapital;
	}

	public Double getValorCapitalConjuge() {
		return valorCapitalConjuge;
	}

	public void setValorCapitalConjuge(Double valorCapitalConjuge) {
		this.valorCapitalConjuge = valorCapitalConjuge;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(String usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public String getUsuarioAlteracao() {
		return usuarioAlteracao;
	}

	public void setUsuarioAlteracao(String usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}
	
	
	


	

}
