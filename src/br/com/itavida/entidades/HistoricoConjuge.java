package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="historicoseguradoconjuge")
public class HistoricoConjuge implements Serializable {
	
	public static Map<String, String> mapaEstadoCivil = new HashMap<String, String>();
	
	static {
		mapaEstadoCivil.put( "CA", "Casado" );
		mapaEstadoCivil.put( "CO", "Companheiro" );
		mapaEstadoCivil.put( "DE", "Desquitado" ) ;
		mapaEstadoCivil.put( "DI", "Divorciado" );
		mapaEstadoCivil.put( "OT", "Outros" );
		mapaEstadoCivil.put( "SE", "Separado" );
		mapaEstadoCivil.put( "SO", "Solteiro" );
		mapaEstadoCivil.put( "VI", "Vi�vo" );
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String cpfConjuge;

	private Date dataNascimentoConjuge;

	private String estadoCivilConjuge;

	private String matriculaConjuge;

	private String nomeConjuge;

	private String rgConjuge;

	private String sexoConjuge;
	
	private Double capital;
	
	private Double premio;

	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;
	
	private Date dataAlteracao;

	private String nomeUsuario;
	
	private String profissaoConjuge;
	
	private static final long serialVersionUID = 1L;
	
	@Transient
	public String getSexoExibicao() {
		if(getSexoConjuge() == null) {
			return "-";
		}
		if(getSexoConjuge().equals("F")) {
			return "Feminino";
		}
		if(getSexoConjuge().equals("M")) {
			return "Masculino";
		}
		return "-";
	}
	
	@Transient
	public String getEstadoCivilExibicao() {
		if(getEstadoCivilConjuge() != null) {
			return mapaEstadoCivil.get(getEstadoCivilConjuge());
		}
		return "-";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCpfConjuge() {
		return cpfConjuge;
	}

	public void setCpfConjuge(String cpfConjuge) {
		this.cpfConjuge = cpfConjuge;
	}

	public Date getDataNascimentoConjuge() {
		return dataNascimentoConjuge;
	}

	public void setDataNascimentoConjuge(Date dataNascimentoConjuge) {
		this.dataNascimentoConjuge = dataNascimentoConjuge;
	}

	public String getEstadoCivilConjuge() {
		return estadoCivilConjuge;
	}

	public void setEstadoCivilConjuge(String estadoCivilConjuge) {
		this.estadoCivilConjuge = estadoCivilConjuge;
	}

	public String getMatriculaConjuge() {
		return matriculaConjuge;
	}

	public void setMatriculaConjuge(String matriculaConjuge) {
		this.matriculaConjuge = matriculaConjuge;
	}

	public String getNomeConjuge() {
		return nomeConjuge;
	}

	public void setNomeConjuge(String nomeConjuge) {
		this.nomeConjuge = nomeConjuge;
	}

	public String getRgConjuge() {
		return rgConjuge;
	}

	public void setRgConjuge(String rgConjuge) {
		this.rgConjuge = rgConjuge;
	}

	public String getSexoConjuge() {
		return sexoConjuge;
	}

	public void setSexoConjuge(String sexoConjuge) {
		this.sexoConjuge = sexoConjuge;
	}

	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public Double getCapital() {
		return capital;
	}

	public void setCapital(Double capital) {
		this.capital = capital;
	}

	public Double getPremio() {
		return premio;
	}

	public void setPremio(Double premio) {
		this.premio = premio;
	}

	/**
	 * @return the profissaoConjuge
	 */
	public String getProfissaoConjuge() {
		return profissaoConjuge;
	}

	/**
	 * @param profissaoConjuge the profissaoConjuge to set
	 */
	public void setProfissaoConjuge(String profissaoConjuge) {
		this.profissaoConjuge = profissaoConjuge;
	}


}
