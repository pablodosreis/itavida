package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.List;

public class DashBoardDataHolder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer totalPropostas;
	private Integer totalPropostasCanceladas;
	private Integer totalPropostasAtivas;
	private Integer totalPropostasSemAverbacao;
	private Integer totalMudancasFaixasProx30dias;
	private Integer totalSinistros;
	private Integer totalSinistros30dias;
	
	private Integer totalOrgaos;
	private Integer totalOrgaosEstaduais;
	private Integer totalOrgaosFederais;
	private Integer totalOrgaosMunicipais;
	private Integer totalOrgaosPrivados;
	
	private Integer totalFuncionarios;
	private Integer funcionarioComMaisAgenciamentos;
	private Integer funcionarioComMenosAgenciamentos;
	
	private List<Object[]> grafico;
	private String graficoConteudo;
	
	//Getters and setters
	public Integer getTotalPropostas() {
		return totalPropostas;
	}
	public void setTotalPropostas(Integer totalPropostas) {
		this.totalPropostas = totalPropostas;
	}
	public Integer getTotalPropostasCanceladas() {
		return totalPropostasCanceladas;
	}
	public void setTotalPropostasCanceladas(Integer totalPropostasCanceladas) {
		this.totalPropostasCanceladas = totalPropostasCanceladas;
	}
	public Integer getTotalPropostasAtivas() {
		return totalPropostasAtivas;
	}
	public void setTotalPropostasAtivas(Integer totalPropostasAtivas) {
		this.totalPropostasAtivas = totalPropostasAtivas;
	}
	public Integer getTotalPropostasSemAverbacao() {
		return totalPropostasSemAverbacao;
	}
	public void setTotalPropostasSemAverbacao(Integer totalPropostasSemAverbacao) {
		this.totalPropostasSemAverbacao = totalPropostasSemAverbacao;
	}
	public Integer getTotalSinistros() {
		return totalSinistros;
	}
	public void setTotalSinistros(Integer totalSinistros) {
		this.totalSinistros = totalSinistros;
	}
	public Integer getTotalOrgaos() {
		return totalOrgaos;
	}
	public void setTotalOrgaos(Integer totalOrgaos) {
		this.totalOrgaos = totalOrgaos;
	}
	public Integer getTotalFuncionarios() {
		return totalFuncionarios;
	}
	public void setTotalFuncionarios(Integer totalFuncionarios) {
		this.totalFuncionarios = totalFuncionarios;
	}
	public Integer getFuncionarioComMaisAgenciamentos() {
		return funcionarioComMaisAgenciamentos;
	}
	public void setFuncionarioComMaisAgenciamentos(
			Integer funcionarioComMaisAgenciamentos) {
		this.funcionarioComMaisAgenciamentos = funcionarioComMaisAgenciamentos;
	}
	public Integer getFuncionarioComMenosAgenciamentos() {
		return funcionarioComMenosAgenciamentos;
	}
	public void setFuncionarioComMenosAgenciamentos(
			Integer funcionarioComMenosAgenciamentos) {
		this.funcionarioComMenosAgenciamentos = funcionarioComMenosAgenciamentos;
	}
	public Integer getTotalSinistros30dias() {
		return totalSinistros30dias;
	}
	public void setTotalSinistros30dias(Integer totalSinistros30dias) {
		this.totalSinistros30dias = totalSinistros30dias;
	}
	public Integer getTotalOrgaosEstaduais() {
		return totalOrgaosEstaduais;
	}
	public void setTotalOrgaosEstaduais(Integer totalOrgaosEstaduais) {
		this.totalOrgaosEstaduais = totalOrgaosEstaduais;
	}
	public Integer getTotalOrgaosFederais() {
		return totalOrgaosFederais;
	}
	public void setTotalOrgaosFederais(Integer totalOrgaosFederais) {
		this.totalOrgaosFederais = totalOrgaosFederais;
	}
	public Integer getTotalOrgaosMunicipais() {
		return totalOrgaosMunicipais;
	}
	public void setTotalOrgaosMunicipais(Integer totalOrgaosMunicipais) {
		this.totalOrgaosMunicipais = totalOrgaosMunicipais;
	}
	public Integer getTotalOrgaosPrivados() {
		return totalOrgaosPrivados;
	}
	public void setTotalOrgaosPrivados(Integer totalOrgaosPrivados) {
		this.totalOrgaosPrivados = totalOrgaosPrivados;
	}
	public Integer getTotalMudancasFaixasProx30dias() {
		return totalMudancasFaixasProx30dias;
	}
	public void setTotalMudancasFaixasProx30dias(
			Integer totalMudancasFaixasProx30dias) {
		this.totalMudancasFaixasProx30dias = totalMudancasFaixasProx30dias;
	}
	public List<Object[]> getGrafico() {
		return grafico;
	}
	public void setGrafico(List<Object[]> grafico) {
		this.grafico = grafico;
	}
	public String getGraficoConteudo() {
		return graficoConteudo;
	}
	public void setGraficoConteudo(String graficoConteudo) {
		this.graficoConteudo = graficoConteudo;
	}
	
	
	
	
}
