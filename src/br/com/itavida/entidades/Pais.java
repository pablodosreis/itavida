package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="pais")
public class Pais implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer cod_pais;

	private String nom_pais;

	private String sgl_pais;

	@OneToMany(mappedBy="cod_pais")
	private Set<Estado> estado_collection;

	private static final long serialVersionUID = 1L;

	public Pais() {
		super();
	}

	public Integer getCod_pais() {
		return this.cod_pais;
	}

	public void setCod_pais(Integer cod_pais) {
		this.cod_pais = cod_pais;
	}

	public String getNom_pais() {
		return this.nom_pais;
	}

	public void setNom_pais(String nom_pais) {
		this.nom_pais = nom_pais;
	}

	public String getSgl_pais() {
		return this.sgl_pais;
	}

	public void setSgl_pais(String sgl_pais) {
		this.sgl_pais = sgl_pais;
	}

	public Set<Estado> getEstado_collection() {
		return this.estado_collection;
	}

	public void setEstado_collection(Set<Estado> estado_collection) {
		this.estado_collection = estado_collection;
	}

}
