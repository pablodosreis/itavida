
package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="orgao")
public class Orgao implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String cep;

	private String cgc;

	private String endereco;
	
	private String bairro;
	
	private String fax;

	private String homePage;

	private String nomeOrgao;

	private String siglaOrgao;

	private String telefone;

	private String tipoOrgao;
	
	@Transient
	private String tipoOrgaoDescricao;

	private Double valorProlabore;

	private Double valorTaxa;
	
	private String email;

	@ManyToOne
	@JoinColumn(name="cidade")
	private Cidade cidade;

	@OneToMany(mappedBy="orgao", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<ContatoOrgao> contatoorgao_collection;

	private static final long serialVersionUID = 1L;

	public Orgao() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCgc() {
		return this.cgc;
	}

	public void setCgc(String cgc) {
		this.cgc = cgc;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	
	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getHomePage() {
		return this.homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public String getNomeOrgao() {
		return this.nomeOrgao;
	}

	public void setNomeOrgao(String nomeOrgao) {
		this.nomeOrgao = nomeOrgao;
	}

	public String getSiglaOrgao() {
		return this.siglaOrgao;
	}

	public void setSiglaOrgao(String siglaOrgao) {
		this.siglaOrgao = siglaOrgao;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getTipoOrgao() {
		return this.tipoOrgao;
	}

	public void setTipoOrgao(String tipoOrgao) {
		this.tipoOrgao = tipoOrgao;
	}


	public String getTipoOrgaoDescricao() {
		return tipoOrgaoDescricao;
	}

	public void setTipoOrgaoDescricao(String tipoOrgaoDescricao) {
		this.tipoOrgaoDescricao = tipoOrgaoDescricao;
	}

	
	public Double getValorProlabore() {
		return this.valorProlabore;
	}

	public void setValorProlabore(Double valorProlabore) {
		this.valorProlabore = valorProlabore;
	}

	public Double getValorTaxa() {
		return this.valorTaxa;
	}

	public void setValorTaxa(Double valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public Cidade getCidade() {
		return this.cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Collection<ContatoOrgao> getContatoorgao_collection() {
		return this.contatoorgao_collection;
	}

	public void setContatoorgao_collection(Collection<ContatoOrgao> contatoorgao_collection) {
		this.contatoorgao_collection = contatoorgao_collection;
	}
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orgao other = (Orgao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
