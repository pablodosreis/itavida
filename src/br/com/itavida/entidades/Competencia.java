package br.com.itavida.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="competencia")
public class Competencia implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idCompetencia;
	private Integer mes;
	private Integer ano;
	private Proposta proposta;
	private Double valorSegurado;
	private Double valorConjugue;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_competencia")
	public Integer getIdCompetencia() {
		return idCompetencia;
	}
	public void setIdCompetencia(Integer idCompetencia) {
		this.idCompetencia = idCompetencia;
	}
	
	@Column(name="mes")
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	
	@Column(name="ano")
	public Integer getAno() {
		return ano;
	}
	
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	
	@ManyToOne
	@JoinColumn(name="proposta_id")
	public Proposta getProposta() {
		return proposta;
	}
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}
	
	@Column(name="valor_segurado")
	public Double getValorSegurado() {
		return valorSegurado;
	}
	public void setValorSegurado(Double valorSegurado) {
		this.valorSegurado = valorSegurado;
	}
	
	@Column(name="valor_conjugue")
	public Double getValorConjugue() {
		return valorConjugue;
	}
	public void setValorConjugue(Double valorConjugue) {
		this.valorConjugue = valorConjugue;
	}
	
}
