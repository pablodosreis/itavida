package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="capitalseguradofaixaetaria")
public class CapitalSeguradoFaixaEtaria implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="nomeTabela")
	private NomeTabela nomeTabela;

	private Date dataVigencia;

	@OneToMany(mappedBy="capitalSeguradoFaixaEtaria", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Collection<DetalheCapitalFaixaEtaria> detalhecapitalfaixaetaria_collection;

	private static final long serialVersionUID = 1L;

	public CapitalSeguradoFaixaEtaria() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public NomeTabela getNomeTabela() {
		return nomeTabela;
	}

	public void setNomeTabela(NomeTabela nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

	public Date getDataVigencia() {
		return dataVigencia;
	}

	public void setDataVigencia(Date dataVigencia) {
		this.dataVigencia = dataVigencia;
	}

	public Collection<DetalheCapitalFaixaEtaria> getDetalhecapitalfaixaetaria_collection() {
		return this.detalhecapitalfaixaetaria_collection;
	}

	public void setDetalhecapitalfaixaetaria_collection(Collection<DetalheCapitalFaixaEtaria> detalhecapitalfaixaetaria_collection) {
		this.detalhecapitalfaixaetaria_collection = detalhecapitalfaixaetaria_collection;
	}

}
