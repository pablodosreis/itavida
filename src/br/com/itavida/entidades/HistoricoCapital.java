package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="historicocapital")
public class HistoricoCapital implements Serializable, Comparable<HistoricoCapital> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Double premioAntigo;

	private Double capitalAntigo;
	
	private Date dataAlteracao;
	
	private String nomeUsuario;
	
	private String email;
	
	private String sexo;
	
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	
	private String matricula;
	
	private String cpf;
	
	private String rg;
	
	private String estadocivil;
	
	private String nomeSegurado;
	
	private String profissaoSegurado;

	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;

	private String idadeAntigo;
	private String tipoModeloProposta;
	
	
	private static final long serialVersionUID = 1L;

	public HistoricoCapital() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public Double getPremioAntigo() {
		return premioAntigo;
	}

	public void setPremioAntigo(Double premioAntigo) {
		this.premioAntigo = premioAntigo;
	}

	public Double getCapitalAntigo() {
		return capitalAntigo;
	}

	public void setCapitalAntigo(Double capitalAntigo) {
		this.capitalAntigo = capitalAntigo;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Proposta getProposta() {
		return this.proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getIdadeAntigo() {
		return idadeAntigo;
	}

	public void setIdadeAntigo(String idadeAntigo) {
		this.idadeAntigo = idadeAntigo;
	}

	public String getTipoModeloProposta() {
		return tipoModeloProposta;
	}

	public void setTipoModeloProposta(String tipoModeloProposta) {
		this.tipoModeloProposta = tipoModeloProposta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getEstadocivil() {
		return estadocivil;
	}

	public void setEstadocivil(String estadocivil) {
		this.estadocivil = estadocivil;
	}
	
	@Transient
	public String getEstadoCivilExibicao() {
		if(getEstadocivil() != null) {
			return HistoricoConjuge.mapaEstadoCivil.get(getEstadocivil());
		}
		return "-";
	}
	
	@Transient
	public String getSexoExibicao() {
		if(getSexo() == null) {
			return "-";
		}
		if(getSexo().equals("F")) {
			return "Feminino";
		}
		if(getSexo().equals("M")) {
			return "Masculino";
		}
		return "-";
	}

	public int compareTo(HistoricoCapital o) {
		return this.getDataAlteracao().compareTo(o.getDataAlteracao());
	}

	public String getNomeSegurado() {
		return nomeSegurado;
	}

	public void setNomeSegurado(String nomeSegurado) {
		this.nomeSegurado = nomeSegurado;
	}

	/**
	 * @return the profissaoSegurado
	 */
	public String getProfissaoSegurado() {
		return profissaoSegurado;
	}

	/**
	 * @param profissaoSegurado the profissaoSegurado to set
	 */
	public void setProfissaoSegurado(String profissaoSegurado) {
		this.profissaoSegurado = profissaoSegurado;
	}
	
	
}
