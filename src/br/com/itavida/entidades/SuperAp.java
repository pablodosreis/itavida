package br.com.itavida.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.Session;

import br.com.itavida.dao.OrgaoQuitacaoDAO;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.util.DateUtil;
import br.com.itavida.util.HibernateUtil;

@Entity
@Table(name="superap")
public class SuperAp implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String numero; 
	
	private Integer modulo;
	
	private Integer renda;
	
	private boolean pessoaPoliticamenteExposta;
	
	@ManyToOne
	@JoinColumn(name="beneficiario_id")
	private Beneficiario beneficiario;
	
	@ManyToOne
	@JoinColumn(name="plano_id")
	private PlanoSuperAp plano;
	
	@ManyToOne
	@JoinColumn(name="cobertura1_id")
	private CoberturaOpcionalSuperAp coberturaOpcional1;
	
	@ManyToOne
	@JoinColumn(name="cobertura2_id")
	private CoberturaOpcionalSuperAp coberturaOpcional2;
	
	private Integer diaVencimento;
	
	private String periodicidade; //0 mensal 1 anual
	
	@Column(name = "certificado", length = 50)
	private String certificado;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_envio_certificado")
	private Date dataEnvioCertificado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getModulo() {
		return modulo;
	}

	public void setModulo(Integer modulo) {
		this.modulo = modulo;
	}

	public Integer getRenda() {
		return renda;
	}

	public void setRenda(Integer renda) {
		this.renda = renda;
	}

	public boolean isPessoaPoliticamenteExposta() {
		return pessoaPoliticamenteExposta;
	}

	public void setPessoaPoliticamenteExposta(boolean pessoaPoliticamenteExposta) {
		this.pessoaPoliticamenteExposta = pessoaPoliticamenteExposta;
	}

	public PlanoSuperAp getPlano() {
		return plano;
	}

	public void setPlano(PlanoSuperAp plano) {
		this.plano = plano;
	}

	public CoberturaOpcionalSuperAp getCoberturaOpcional1() {
		return coberturaOpcional1;
	}

	public void setCoberturaOpcional1(CoberturaOpcionalSuperAp coberturaOpcional1) {
		this.coberturaOpcional1 = coberturaOpcional1;
	}

	public CoberturaOpcionalSuperAp getCoberturaOpcional2() {
		return coberturaOpcional2;
	}

	public void setCoberturaOpcional2(CoberturaOpcionalSuperAp coberturaOpcional2) {
		this.coberturaOpcional2 = coberturaOpcional2;
	}

	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}

	public String getPeriodicidade() {
		return periodicidade;
	}

	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}
	
	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}

	
	public String getCertificado() {
		return certificado;
	}

	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	public Date getDataEnvioCertificado() {
		return dataEnvioCertificado;
	}

	public void setDataEnvioCertificado(Date dataEnvioCertificado) {
		this.dataEnvioCertificado = dataEnvioCertificado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SuperAp other = (SuperAp) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
}
