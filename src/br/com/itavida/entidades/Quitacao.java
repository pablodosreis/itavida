package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="quitacao")
public class Quitacao implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="propostaquitacao")
	private PropostaQuitacao propostaQuitacao;

	@Column(name = "quitado")
	private boolean quitado;
	
	@Column(name = "data")
	private Date data;

	public Quitacao() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public Quitacao(PropostaQuitacao propostaQuitacao, boolean quitado,
			Date data) {
		super();
		this.propostaQuitacao = propostaQuitacao;
		this.quitado = quitado;
		this.data = data;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PropostaQuitacao getPropostaQuitacao() {
		return propostaQuitacao;
	}

	public void setPropostaQuitacao(PropostaQuitacao propostaQuitacao) {
		this.propostaQuitacao = propostaQuitacao;
	}

	public boolean isQuitado() {
		return quitado;
	}

	public void setQuitado(boolean quitado) {
		this.quitado = quitado;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}
