package br.com.itavida.entidades;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="propostaagenciador")
public class PropostaAgenciador implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Double percentual;

	private Double valorPagoDefinido;
	
	private String tipoCalculoUtilizado;
	
	private Integer ordemAgenciamento;
	
	private Double taxaCarregamento;

	private Double valorTotalPremio;
	
	private String comando;
	
	private Double diferenca;

	@ManyToOne
	@JoinColumn(name="funcionario")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;

	private static final long serialVersionUID = 1L;

	public PropostaAgenciador() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getPercentual() {
		return this.percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	public Double getValorPagoDefinido() {
		return this.valorPagoDefinido;
	}

	public void setValorPagoDefinido(Double valorPagoDefinido) {
		this.valorPagoDefinido = valorPagoDefinido;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Proposta getProposta() {
		return this.proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public String getTipoCalculoUtilizado() {
		return tipoCalculoUtilizado;
	}

	public void setTipoCalculoUtilizado(String tipoCalculoUtilizado) {
		this.tipoCalculoUtilizado = tipoCalculoUtilizado;
	}

	public Integer getOrdemAgenciamento() {
		return ordemAgenciamento;
	}

	public void setOrdemAgenciamento(Integer ordemAgenciamento) {
		this.ordemAgenciamento = ordemAgenciamento;
	}

	public Double getTaxaCarregamento() {
		return taxaCarregamento;
	}

	public void setTaxaCarregamento(Double taxaCarregamento) {
		this.taxaCarregamento = taxaCarregamento;
	}

	public Double getValorTotalPremio() {
		return valorTotalPremio;
	}

	public void setValorTotalPremio(Double valorTotalPremio) {
		this.valorTotalPremio = valorTotalPremio;
	}

	public String getComando() {
		return comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	public Double getDiferenca() {
		return diferenca;
	}

	public void setDiferenca(Double diferenca) {
		this.diferenca = diferenca;
	}

	
	
}
