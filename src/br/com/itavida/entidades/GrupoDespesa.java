package br.com.itavida.entidades;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="grupodespesa")
public class GrupoDespesa implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String nomeGrupoDespesa;

	private static final long serialVersionUID = 1L;

	public GrupoDespesa() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeGrupoDespesa() {
		return this.nomeGrupoDespesa;
	}

	public void setNomeGrupoDespesa(String nomeGrupoDespesa) {
		this.nomeGrupoDespesa = nomeGrupoDespesa;
	}

}
