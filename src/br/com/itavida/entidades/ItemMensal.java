package br.com.itavida.entidades;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="item_mensal")
public class ItemMensal implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="arquivo_mensal_id")
	private ArquivoMensal arquivoMensal;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_ultima_atualizacao")
	private Date dataUltimaAtualizacao;
	
	@Column(name = "usuario_ultima_atualizacao")
	private String usuarioUltimaAtualizacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "referencia")
	private Date referencia;
	
	@Column(name = "nome_orgao")
	private String nomeOrgao;
	
	@Column(name = "nome_segurado")
	private String nomeSegurado;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento_segurado ")
	private Date dataNascimentoSegurado;
	
	@Column(name = "cpf_segurado")
	private String cpfSegurado;
	
	@Column(name = "premio_segurado")
	private Double premioSegurado;
	
	@Column(name = "capital_segurado")
	private Double capitalSegurado;
	
	@Column(name = "descricao_plano")
	private String descricaoPlano;
	
	@Column(name = "nome_conjuge ")
	private String nomeConjuge;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento_conjuge")
	private Date dataNascimentoConjuge;
	
	@Column(name = "cpf_conjuge")
	private String cpfConjuge;
	
	@Column(name = "premio_conjuge")
	private Double premioConjuge;
	
	@Column(name = "capital_conjuge")
	private Double capitalConjuge;
	
	@Column
	private String endereco;
	
	@Column
	private String bairro;
	
	@Column
	private String cidade;
	
	@Column
	private String uf;
	
	@Column
	private String cep;
	
	@ManyToOne
	@JoinColumn(name="empresa_id")
	private Empresa empresa;
	
	@ManyToOne
	@JoinColumn(name="tipo_proposta_id")
	private TipoProposta tipoProposta;
	
	@ManyToOne
	@JoinColumn(name="proposta_id")
	private Proposta proposta;
	
	@ManyToOne
	@JoinColumn(name="orgao_id")
	private Orgao orgao;
	
	@Column(name = "item_pago", columnDefinition = "TINYINT(1) DEFAULT 1")
	private boolean itemPago = true;
	
	@Transient
	private String referenciaTxt;
	
	private static final long serialVersionUID = 1L;

	public ItemMensal() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the arquivoMensal
	 */
	public ArquivoMensal getArquivoMensal() {
		return arquivoMensal;
	}

	/**
	 * @return the dataUltimaAtualizacao
	 */
	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	/**
	 * @return the usuarioUltimaAtualizacao
	 */
	public String getUsuarioUltimaAtualizacao() {
		return usuarioUltimaAtualizacao;
	}

	/**
	 * @return the referencia
	 */
	public Date getReferencia() {
		return referencia;
	}

	/**
	 * @return the nomeOrgao
	 */
	public String getNomeOrgao() {
		return nomeOrgao;
	}

	/**
	 * @return the nomeSegurado
	 */
	public String getNomeSegurado() {
		return nomeSegurado;
	}

	/**
	 * @return the dataNascimentoSegurado
	 */
	public Date getDataNascimentoSegurado() {
		return dataNascimentoSegurado;
	}

	/**
	 * @return the cpfSegurado
	 */
	public String getCpfSegurado() {
		return cpfSegurado;
	}

	/**
	 * @return the premioSegurado
	 */
	public Double getPremioSegurado() {
		return premioSegurado;
	}

	/**
	 * @return the capitalSegurado
	 */
	public Double getCapitalSegurado() {
		return capitalSegurado;
	}

	/**
	 * @return the descricaoPlano
	 */
	public String getDescricaoPlano() {
		return descricaoPlano;
	}

	/**
	 * @return the nomeConjuge
	 */
	public String getNomeConjuge() {
		return nomeConjuge;
	}

	/**
	 * @return the dataNascimentoConjuge
	 */
	public Date getDataNascimentoConjuge() {
		return dataNascimentoConjuge;
	}

	/**
	 * @return the cpfConjuge
	 */
	public String getCpfConjuge() {
		return cpfConjuge;
	}

	/**
	 * @return the premioConjuge
	 */
	public Double getPremioConjuge() {
		return premioConjuge;
	}

	/**
	 * @return the capitalConjuge
	 */
	public Double getCapitalConjuge() {
		return capitalConjuge;
	}

	/**
	 * @return the endereo
	 */
	public String getEndereco() {
		return endereco;
	}

	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * @return the cep
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * @return the tipoProposta
	 */
	public TipoProposta getTipoProposta() {
		return tipoProposta;
	}

	/**
	 * @return the proposta
	 */
	public Proposta getProposta() {
		return proposta;
	}

	/**
	 * @return the orgao
	 */
	public Orgao getOrgao() {
		return orgao;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param arquivoMensal the arquivoMensal to set
	 */
	public void setArquivoMensal(ArquivoMensal arquivoMensal) {
		this.arquivoMensal = arquivoMensal;
	}

	/**
	 * @return the referenciaTxt
	 */
	public String getReferenciaTxt() {
		SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");
		return format.format(getReferencia());
	}

	/**
	 * @param referenciaTxt the referenciaTxt to set
	 */
	public void setReferenciaTxt(String referenciaTxt) {
		this.referenciaTxt = referenciaTxt;
	}

	/**
	 * @param dataUltimaAtualizacao the dataUltimaAtualizacao to set
	 */
	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	/**
	 * @param usuarioUltimaAtualizacao the usuarioUltimaAtualizacao to set
	 */
	public void setUsuarioUltimaAtualizacao(String usuarioUltimaAtualizacao) {
		this.usuarioUltimaAtualizacao = usuarioUltimaAtualizacao;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(Date referencia) {
		this.referencia = referencia;
	}

	/**
	 * @param nomeOrgao the nomeOrgao to set
	 */
	public void setNomeOrgao(String nomeOrgao) {
		this.nomeOrgao = nomeOrgao;
	}

	/**
	 * @param nomeSegurado the nomeSegurado to set
	 */
	public void setNomeSegurado(String nomeSegurado) {
		this.nomeSegurado = nomeSegurado;
	}

	/**
	 * @param dataNascimentoSegurado the dataNascimentoSegurado to set
	 */
	public void setDataNascimentoSegurado(Date dataNascimentoSegurado) {
		this.dataNascimentoSegurado = dataNascimentoSegurado;
	}

	/**
	 * @param cpfSegurado the cpfSegurado to set
	 */
	public void setCpfSegurado(String cpfSegurado) {
		this.cpfSegurado = cpfSegurado;
	}

	/**
	 * @param premioSegurado the premioSegurado to set
	 */
	public void setPremioSegurado(Double premioSegurado) {
		this.premioSegurado = premioSegurado;
	}

	/**
	 * @param capitalSegurado the capitalSegurado to set
	 */
	public void setCapitalSegurado(Double capitalSegurado) {
		this.capitalSegurado = capitalSegurado;
	}

	/**
	 * @param descricaoPlano the descricaoPlano to set
	 */
	public void setDescricaoPlano(String descricaoPlano) {
		this.descricaoPlano = descricaoPlano;
	}

	/**
	 * @param nomeConjuge the nomeConjuge to set
	 */
	public void setNomeConjuge(String nomeConjuge) {
		this.nomeConjuge = nomeConjuge;
	}

	/**
	 * @param dataNascimentoConjuge the dataNascimentoConjuge to set
	 */
	public void setDataNascimentoConjuge(Date dataNascimentoConjuge) {
		this.dataNascimentoConjuge = dataNascimentoConjuge;
	}

	/**
	 * @param cpfConjuge the cpfConjuge to set
	 */
	public void setCpfConjuge(String cpfConjuge) {
		this.cpfConjuge = cpfConjuge;
	}

	/**
	 * @param premioConjuge the premioConjuge to set
	 */
	public void setPremioConjuge(Double premioConjuge) {
		this.premioConjuge = premioConjuge;
	}

	/**
	 * @param capitalConjuge the capitalConjuge to set
	 */
	public void setCapitalConjuge(Double capitalConjuge) {
		this.capitalConjuge = capitalConjuge;
	}

	/**
	 * @param endereo the endereo to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * @param uf the uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}

	/**
	 * @param cep the cep to set
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	/**
	 * @param tipoProposta the tipoProposta to set
	 */
	public void setTipoProposta(TipoProposta tipoProposta) {
		this.tipoProposta = tipoProposta;
	}

	/**
	 * @param proposta the proposta to set
	 */
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	/**
	 * @param orgao the orgao to set
	 */
	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	/**
	 * @return the empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * @param empresa the empresa to set
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * @return the exibirNoRelatorio
	 */
	public boolean isItemPago() {
		return itemPago;
	}

	/**
	 * @param exibirNoRelatorio the exibirNoRelatorio to set
	 */
	public void setItemPago(boolean exibirNoRelatorio) {
		this.itemPago = exibirNoRelatorio;
	}
	
	
	

}
