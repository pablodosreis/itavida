package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.itavida.util.DateUtil;

@Entity
@Table(name="cobertura_superap")
public class CoberturaOpcionalSuperAp implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String descricao;
	
	private Double capitalSegurado;
	
	private Double premioMensal;
	
	private Double premioAnual;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getCapitalSegurado() {
		return capitalSegurado;
	}

	public void setCapitalSegurado(Double capitalSegurado) {
		this.capitalSegurado = capitalSegurado;
	}

	public Double getPremioMensal() {
		return premioMensal;
	}

	public void setPremioMensal(Double premioMensal) {
		this.premioMensal = premioMensal;
	}

	public Double getPremioAnual() {
		return premioAnual;
	}

	public void setPremioAnual(Double premioAnual) {
		this.premioAnual = premioAnual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoberturaOpcionalSuperAp other = (CoberturaOpcionalSuperAp) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
