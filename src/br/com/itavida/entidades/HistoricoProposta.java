package br.com.itavida.entidades;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="historicoproposta")
public class HistoricoProposta implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="empresa")
	private Empresa empresa;	

	@ManyToOne
	@JoinColumn(name="nomeTabela")
	private NomeTabela nomeTabela;

	@ManyToOne
	@JoinColumn(name="orgao")
	private Orgao orgao;

	@ManyToOne
	@JoinColumn(name="tipoProposta")
	private TipoProposta tipoProposta;
	
	@ManyToOne
	@JoinColumn(name="modeloProposta")
	private ModeloProposta modeloProposta;
	
	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;
	
	private Date dataAlteracao;

	private String nomeUsuario;
	
	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public NomeTabela getNomeTabela() {
		return nomeTabela;
	}

	public void setNomeTabela(NomeTabela nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

	public Orgao getOrgao() {
		return orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public TipoProposta getTipoProposta() {
		return tipoProposta;
	}

	public void setTipoProposta(TipoProposta tipoProposta) {
		this.tipoProposta = tipoProposta;
	}

	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public ModeloProposta getModeloProposta() {
		return modeloProposta;
	}

	public void setModeloProposta(ModeloProposta modeloProposta) {
		this.modeloProposta = modeloProposta;
	}

}
