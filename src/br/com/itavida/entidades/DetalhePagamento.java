package br.com.itavida.entidades;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="detalhepagamento")
public class DetalhePagamento implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Double valor;

	@ManyToOne
	@JoinColumn(name="pagamento")
	private Pagamento pagamento;

	@ManyToOne
	@JoinColumn(name="despesa")
	private Despesa despesa;

	@ManyToOne
	@JoinColumn(name="filial")
	private Filial filial;

	@ManyToOne
	@JoinColumn(name="funcionario")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name="grupoDespesa")
	private GrupoDespesa grupoDespesa;

	@Transient
	private ContasVencer contasVencer;
	
	private static final long serialVersionUID = 1L;

	public DetalhePagamento() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Pagamento getPagamento() {
		return this.pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public Despesa getDespesa() {
		return this.despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public Filial getFilial() {
		return this.filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public GrupoDespesa getGrupoDespesa() {
		return this.grupoDespesa;
	}

	public void setGrupoDespesa(GrupoDespesa grupoDespesa) {
		this.grupoDespesa = grupoDespesa;
	}

	public ContasVencer getContasVencer() {
		return contasVencer;
	}

	public void setContasVencer(ContasVencer contasVencer) {
		this.contasVencer = contasVencer;
	}
	
	

}
