package br.com.itavida.entidades;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="beneficiario")
public class Beneficiario implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String nomeBeneficiario;

	private Double percentual;
	@Transient
	private Integer tipoPercentual;

	@ManyToOne
	@JoinColumn(name="grauParentesco")
	private GrauParentesco grauParentesco;

	@ManyToOne
	@JoinColumn(name="proposta")
	private Proposta proposta;

	private static final long serialVersionUID = 1L;

	public Beneficiario() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeBeneficiario() {
		return this.nomeBeneficiario;
	}

	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	public Double getPercentual() {
		return this.percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	public GrauParentesco getGrauParentesco() {
		return this.grauParentesco;
	}

	public void setGrauParentesco(GrauParentesco grauParentesco) {
		this.grauParentesco = grauParentesco;
	}

	public Proposta getProposta() {
		return this.proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public Integer getTipoPercentual() {
		return tipoPercentual;
	}

	public void setTipoPercentual(Integer tipoPercentual) {
		this.tipoPercentual = tipoPercentual;
	}


}
