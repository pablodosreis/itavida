package br.com.itavida.listener;

import javax.servlet.ServletContextEvent;

import com.sun.faces.config.ConfigureListener;

public class Config extends ConfigureListener {

    public void contextInitialized(ServletContextEvent event) {
        System.setProperty("org.apache.el.parser.COERCE_TO_ZERO", "false");
        super.contextInitialized(event);
    }

}