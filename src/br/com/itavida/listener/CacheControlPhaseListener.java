package br.com.itavida.listener;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.servlet.http.HttpServletResponse;

public class CacheControlPhaseListener implements javax.faces.event.PhaseListener {

	public void afterPhase(PhaseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void beforePhase(PhaseEvent event) {
		FacesContext facesContext = event.getFacesContext(); 
		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse(); 
		response.setHeader("Pragma", "No-Cache"); 
		response.setHeader("Cache-Control", "no-cache,"); 
		response.setHeader("Cache-Control", "no-store"); 
		response.setHeader("Cache-Control", "private"); 
		response.setHeader("Cache-Control", "must-revalidate"); 
		response.setHeader("Cache-Control", "max-stale=0"); 
		response.setHeader("Cache-Control", "max-age=0"); 
		response.setDateHeader("Expires", 1); 		
		response.setCharacterEncoding("ISO-8859-1");  
	}

	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE; 
	}
}
