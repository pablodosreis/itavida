package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.GrupoDespesa;

public class GrupoDespesaDAO {

	private Session session;
	
	public GrupoDespesaDAO( Session session ){
		this.session = session;
	}
	
	
	public List<GrupoDespesa> busca( String nome ){
		Criteria c = this.session.createCriteria( GrupoDespesa.class );
		c.add( Restrictions.ilike("nomeGrupoDespesa", nome, MatchMode.ANYWHERE ) );			
		return c.list();
	}
	
	
}
