package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.Funcionario;

public class CidadeDAO {

	private Session session;
	
	public CidadeDAO( Session session ){
		this.session = session;
	}
	
	
	public List<Cidade> buscaPeloNome( String nome, Integer codigoEstado, Integer maxRegistros ){
		Criteria c = this.session.createCriteria( Cidade.class );

		if( nome != null  && !"".equals(nome)){
			c.add( Restrictions.ilike("nom_cidade", nome, MatchMode.ANYWHERE ) );
		}
		if( codigoEstado != null && codigoEstado != 0){
			c.createAlias("cod_estado", "e");
			c.add( Restrictions.eq("e.cod_estado", codigoEstado ) );
		}	
		c.setMaxResults(maxRegistros);
		c.addOrder( Order.asc("nom_cidade") );
		return c.list();
	}
	
	public boolean findPorCidade( Integer idCidade ){
		
		Criteria funcionarioCriteria = this.session.createCriteria( Funcionario.class );
		funcionarioCriteria.createAlias( "cidade" , "cidade" );
		funcionarioCriteria.add( Restrictions.eq("cidade.id", idCidade ) );
		List resultado = funcionarioCriteria.list();
		
		if( resultado != null && resultado.size() > 0 ){
			return true;
		}
		
		return false;
	}
	
	public List<Estado> obterTodosEstados( ){
		
		Criteria estadoCriteria = this.session.createCriteria( Estado.class );
		return estadoCriteria.list();
		
	}
	
}
