package br.com.itavida.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.dao.base.GenericDAO;
import br.com.itavida.entidades.TipoProposta;

public class TipoPropostaDAO extends GenericDAO<TipoProposta> {

	public TipoPropostaDAO(){
		super(TipoProposta.class);
	}
	
	/**
	 * Busca todos os tipos de proposta de acordo o modelo escolhido
	 * @param idModelo
	 * @return
	 */
	public List<TipoProposta> busca( Object idModelo ){
		Criteria c = getSession().createCriteria( TipoProposta.class );
		if( idModelo != null ){
			c.createAlias("modeloProposta", "modeloProposta");
			c.add( Restrictions.eq("modeloProposta.id", idModelo ) );
			return c.list();
		}else{
			return new ArrayList<TipoProposta>();
		}
		
	}
	
}
