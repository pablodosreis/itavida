package br.com.itavida.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.itavida.dto.MovimentacaoPagamento;
import br.com.itavida.entidades.MovimentacaoPropostaInadimplente;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.exceptions.SistemaException;

public class MovimentacaoPropostaInadimplenteDAO {
	
	private Session session;
	
	public MovimentacaoPropostaInadimplenteDAO( Session session ){
		this.session = session;
	}
	
	public void saveOrUpdate(MovimentacaoPropostaInadimplente obj) {
		
		if (obj.getProposta().getSituacao() == SituacaoProposta.CANCELADA) {
			throw new SistemaException("O usu�rio informado est� cancelado.");
		}
		
		if (jaExisteMovimentacao(obj)) {
			throw new SistemaException("J� existe um registro cadastrado para este segurado.");
		}
		
		session.saveOrUpdate(obj);
	}
	
	public void delete(MovimentacaoPropostaInadimplente obj) {
		MovimentacaoPropostaAtrasadaDAO atrasadaDao = new MovimentacaoPropostaAtrasadaDAO(session);
		if (atrasadaDao.jaExisteMovimentacao(obj.getProposta(), obj.getReferencia())) {
			throw new SistemaException("N�o � poss�vel excluir esta movimenta��o porque j� existe uma movimenta��o informando a sua quita��o.");
		}
		session.delete(obj);
	}
	
	public MovimentacaoPropostaInadimplente getMovimentacao(Date referencia, Proposta proposta) {
		String hql = " from MovimentacaoPropostaInadimplente where referencia = :referencia and proposta = :proposta order by proposta.nomeSegurado ";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", referencia);
		query.setParameter("proposta", proposta);
		return (MovimentacaoPropostaInadimplente) query.uniqueResult();	
	}
	
	public boolean jaExisteMovimentacao(Date referencia, Proposta proposta) {
		MovimentacaoPropostaInadimplente obj = new MovimentacaoPropostaInadimplente();
		obj.setReferencia(referencia);
		obj.setProposta(proposta);
		return jaExisteMovimentacao(obj);
		
	}
	
	
	public boolean jaExisteMovimentacao(MovimentacaoPropostaInadimplente obj) {
		String hql = " from MovimentacaoPropostaInadimplente where referencia = :referencia and proposta = :proposta order by proposta.nomeSegurado ";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", obj.getReferencia());
		query.setParameter("proposta", obj.getProposta());
		return query.uniqueResult() != null;	
	}
	
	public List<MovimentacaoPropostaInadimplente> buscarMovimentacoes(Date referencia) {
		String hql = " from MovimentacaoPropostaInadimplente where  referencia = :referencia order by proposta.nomeSegurado ";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", referencia);
		return query.list();
	}
	
	public List<MovimentacaoPagamento> buscarHistoricoMovimentacoes(Integer idProposta) {
		
		List<MovimentacaoPagamento> retorno = new ArrayList<MovimentacaoPagamento>();
		List<Object[]> lista = buscarMovimentacoes(idProposta);
		for (Object[] objects : lista) {
			MovimentacaoPagamento novo = new MovimentacaoPagamento();
			novo.setPago(objects[4] != null ? "sim" : "n�o");
			novo.setPeriodo(objects[1].toString());
			novo.setQuitacao(objects[4] != null ? objects[4].toString() : "-");
			novo.setSegurado(objects[0].toString());
			novo.setValor(new Double(objects[3].toString()));
			retorno.add(novo);
		}
		
		
		return retorno;
	}
	
	public List<Object[]> buscarMovimentacoes(Integer idProposta) {
		String hql = "Select p.nomeSegurado, mpi.referencia, mpi.premio, mpa.premio, mpa.periodo_quitacao_anterior  "
				+ "from movimentacao_proposta_inadimplente mpi "
				+ " left join movimentacao_proposta_atrasada mpa on mpa.proposta_id = mpi.proposta_id and mpa.periodo_quitacao_anterior = mpi.referencia "
				+ " left join proposta p on p.id = mpi.proposta_id "
				+ " where  mpi.proposta_id = :idProposta "
				+ " order by mpi.referencia ";
		Query query = session.createSQLQuery(hql);
		query.setParameter("idProposta", idProposta);
		return query.list();
	}
	
	

}
