package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;

public class DetalheCapitalSeguradoFaixaEtariaDAO {

	private Session session;
	
	public DetalheCapitalSeguradoFaixaEtariaDAO( Session session ){
		this.session = session;
	}
	
	
	public List<DetalheCapitalFaixaEtaria> buscaPorCapitalFaixaEtaria( Integer idCapitalFaixaEtaria ){
		Criteria c = this.session.createCriteria( DetalheCapitalFaixaEtaria.class );
		c.add( Restrictions.eq("capitalSeguradoFaixaEtaria.id", idCapitalFaixaEtaria ) );
		
		return c.list();
	}
	
	
}
