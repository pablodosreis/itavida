package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.NumeroSorteio;
import br.com.itavida.entidades.ResultadoOperacao;

public class NumeroSorteioDAO {

	private Session session;
	
	public NumeroSorteioDAO( Session session ){
		this.session = session;
	}
	
	
	public List<NumeroSorteio> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( NumeroSorteio.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		c.addOrder( Order.asc("id") );
		return c.list();
	}

	public void deletePorProposta( Integer idProposta ){
		String hql = "delete NumeroSorteio numeroSorteio where numeroSorteio.proposta.id = ?";
		session.createQuery(hql).setInteger(0, idProposta).executeUpdate();
	}
	
	public void limparTudo(Integer empresaId){
		String hql = "delete n from numerosorteio n, proposta p where n.proposta = p.id and p.empresa = ? ";
		session.createSQLQuery(hql).setInteger(0, empresaId).executeUpdate();
	}
	
	public void saveOrUpdate(NumeroSorteio entidade) {
		session.saveOrUpdate(entidade);
	}
	
	public void merge(NumeroSorteio entidade) {
		session.merge(entidade);
	}
	
	
	String deleteByNavKeyQ = "";
	
	
}
