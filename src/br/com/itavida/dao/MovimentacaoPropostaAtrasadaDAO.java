package br.com.itavida.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.itavida.entidades.MovimentacaoPropostaAtrasada;
import br.com.itavida.entidades.MovimentacaoPropostaInadimplente;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.exceptions.SistemaException;

public class MovimentacaoPropostaAtrasadaDAO {
	
private Session session;
	
	public MovimentacaoPropostaAtrasadaDAO( Session session ){
		this.session = session;
	}
	
	
	public void saveOrUpdate(MovimentacaoPropostaAtrasada obj) {
		
		MovimentacaoPropostaInadimplenteDAO inadimplenteDAO = new MovimentacaoPropostaInadimplenteDAO(session);
		
		if (obj.getProposta().getSituacao() == SituacaoProposta.CANCELADA) {
			throw new SistemaException("O usu�rio informado est� cancelado.");
		}
		
		if (jaExisteMovimentacao(obj)) {
			throw new SistemaException("J� existe um registro cadastrado para este segurado no m�s/ano informados.");
		}
		
		if (obj.getPeriodoQuitacaoAnterior().getTime() >= obj.getReferencia().getTime()) {
			throw new SistemaException("O M�s/Ano deve ser inferior ao per�odo de refer�ncia da movimenta��o.");
		}
		
		MovimentacaoPropostaInadimplente inadimplente = inadimplenteDAO.getMovimentacao(obj.getPeriodoQuitacaoAnterior(), obj.getProposta());
		if (inadimplente == null) {
			throw new SistemaException("A proposta informada n�o est� inadimplente no m�s de refer�ncia informado. Portanto n�o ser� poss�vel cadastrar uma quita��o para este m�s.");
		}
		obj.setPremio(inadimplente.getPremio());
		
		session.saveOrUpdate(obj);
	}
	
	public boolean possuiInadimplencia(Proposta proposta) {
		String hql = "Select count(*) from MovimentacaoPropostaInadimplente i "
				+ " where i.proposta = :proposta "
				+ "   and not exists (select id from MovimentacaoPropostaAtrasada a where a.periodoQuitacaoAnterior =  i.referencia and a.proposta = :proposta) ";
		Query query = session.createQuery(hql);
		query.setParameter("proposta", proposta);
		
		return ((Long) query.uniqueResult()) > 0;
	}
	
	public void delete(MovimentacaoPropostaAtrasada obj) {
		session.delete(obj);
	}
	
	public boolean jaExisteMovimentacao(Proposta proposta, Date quitacao) {
		String hql = " from MovimentacaoPropostaAtrasada where proposta = :proposta and periodoQuitacaoAnterior = :periodoQuitacaoAnterior";
		Query query = session.createQuery(hql);
		query.setParameter("proposta", proposta);
		query.setParameter("periodoQuitacaoAnterior", quitacao);
		return query.uniqueResult() != null;
	}
	
	public boolean jaExisteMovimentacao(MovimentacaoPropostaAtrasada obj) {
		String hql = " from MovimentacaoPropostaAtrasada where referencia = :referencia and proposta = :proposta and periodoQuitacaoAnterior = :periodoQuitacaoAnterior";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", obj.getReferencia());
		query.setParameter("proposta", obj.getProposta());
		query.setParameter("periodoQuitacaoAnterior", obj.getPeriodoQuitacaoAnterior());
		return query.uniqueResult() != null;	
	}
	
	public List<MovimentacaoPropostaAtrasada> buscarMovimentacoes(Date referencia) {
		String hql = " from MovimentacaoPropostaAtrasada where referencia = :referencia order by proposta.nomeSegurado";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", referencia);
		return query.list();
	}
	

	

}
