package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.TipoOcorrencia;

public class TipoOcorrenciaDAO {

	private Session session;
	
	public TipoOcorrenciaDAO( Session session ){
		this.session = session;
	}
	
	
	public List<TipoOcorrencia> busca( String nome, String tipo ){
		Criteria c = this.session.createCriteria( TipoOcorrencia.class );

		c.add( Restrictions.ilike("descricao", nome, MatchMode.ANYWHERE ) );
		if( tipo != null && !"".equals( tipo ) ){
			c.add( Restrictions.eq("sinistroNegado", tipo ) );
		}			
		
		return c.list();
	}
	
	
}
