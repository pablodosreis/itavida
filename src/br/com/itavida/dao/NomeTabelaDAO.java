package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.NomeTabela;

public class NomeTabelaDAO {

	private Session session;
	
	public NomeTabelaDAO( Session session ){
		this.session = session;
	}
	
	
	public List<NomeTabela> busca( String nome, String nomeReduzido ){
		Criteria c = this.session.createCriteria( NomeTabela.class );
		c.add( Restrictions.ilike("nomeTabela", nome, MatchMode.ANYWHERE ) );	
		if( nomeReduzido != null ){
			c.add( Restrictions.ilike("nomeTabelaReduzido", nomeReduzido, MatchMode.ANYWHERE ) );
		}
		c.addOrder( Order.asc("nomeTabela") );
		return c.list();
	}
	
	public NomeTabela buscaNomeTabela( String nome){
		Criteria c = this.session.createCriteria( NomeTabela.class );
		c.add( Restrictions.ilike("nomeTabela", nome) );	
		return (NomeTabela) c.uniqueResult();
	}
	
	public NomeTabela buscaNomeTabela(Integer codigo) {
		Criteria c = this.session.createCriteria( NomeTabela.class );
		c.add( Restrictions.eq("id", codigo) );	
		return (NomeTabela) c.uniqueResult();
	}
	
	public NomeTabela bucaTabelaIcatu() {
		return buscaNomeTabela(101);
	}
	
}
