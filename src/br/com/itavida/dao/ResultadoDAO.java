package br.com.itavida.dao;

import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Filial;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.ResultadoOperacao;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.handlers.UsuarioHandler;

public class ResultadoDAO {

	private Session session;
	
	public ResultadoDAO( Session session ){
		this.session = session;
	}
	
	
	public List<ResultadoOperacao> buscarResultados(){
		Criteria c = this.session.createCriteria( ResultadoOperacao.class );

		c.addOrder(Order.desc("id"));
					
		return c.list();
	}
	
	
	public void salvar(ResultadoOperacao resultado) {
		session.saveOrUpdate(resultado);
	}
	
}
