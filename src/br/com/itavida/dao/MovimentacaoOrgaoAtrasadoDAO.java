package br.com.itavida.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.itavida.entidades.MovimentacaoOrgaoAtrasado;
import br.com.itavida.entidades.MovimentacaoOrgaoInadimplente;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.exceptions.SistemaException;

public class MovimentacaoOrgaoAtrasadoDAO {
	
private Session session;


	public MovimentacaoOrgaoAtrasadoDAO( Session session ){
		this.session = session;
	}
	
	public void saveOrUpdate(MovimentacaoOrgaoAtrasado obj) {
		MovimentacaoOrgaoInadimplenteDAO inadimplenteDAO = new MovimentacaoOrgaoInadimplenteDAO(session);
		
			if (jaExisteMovimentacao(obj)) {
				throw new SistemaException("J� existe um registro cadastrado para este segurado no m�s/ano informados.");
			}
			
			if (obj.getPeriodoQuitacaoAnterior().getTime() >= obj.getReferencia().getTime()) {
				throw new SistemaException("O M�s/Ano deve ser inferior ao per�odo de refer�ncia da movimenta��o.");
			}
			
			MovimentacaoOrgaoInadimplente inadimplente = inadimplenteDAO.getMovimentacao(obj.getPeriodoQuitacaoAnterior(), obj.getOrgao());
			if (inadimplente == null) {
				throw new SistemaException("O �rg�o informado n�o est� inadimplente no m�s de refer�ncia informado. Portanto n�o ser� poss�vel cadastrar uma quita��o para este m�s.");
			}
			obj.setPremio(inadimplente.getPremio());
			
			session.saveOrUpdate(obj);
		}
	
	public boolean jaExisteMovimentacao(Orgao orgao, Date quitacaoAnterior) {
		String hql = " from MovimentacaoOrgaoAtrasado where orgao = :orgao and periodoQuitacaoAnterior = :periodoQuitacaoAnterior";
		Query query = session.createQuery(hql);
		query.setParameter("orgao", orgao);
		query.setParameter("periodoQuitacaoAnterior", quitacaoAnterior);
		return query.uniqueResult() != null;
	}
	
	public boolean jaExisteMovimentacao(MovimentacaoOrgaoAtrasado obj) {
		String hql = " from MovimentacaoOrgaoAtrasado where referencia = :referencia and orgao = :orgao and periodoQuitacaoAnterior = :periodoQuitacaoAnterior";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", obj.getReferencia());
		query.setParameter("orgao", obj.getOrgao());
		query.setParameter("periodoQuitacaoAnterior", obj.getPeriodoQuitacaoAnterior());
		return query.uniqueResult() != null;	
	}
	
	public List<MovimentacaoOrgaoAtrasado> buscarMovimentacoes(Date referencia) {
		String hql = " from MovimentacaoOrgaoAtrasado where referencia = :referencia";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", referencia);
		return query.list();
	}
	
	
	public void delete(MovimentacaoOrgaoAtrasado obj) {
		session.delete(obj);
	}
	

}
