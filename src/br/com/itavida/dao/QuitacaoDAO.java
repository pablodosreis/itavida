package br.com.itavida.dao;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.PropostaQuitacao;
import br.com.itavida.entidades.Quitacao;
import br.com.itavida.util.DateUtil;

public class QuitacaoDAO {
	private Session session;

	static final int QUANTIDADE_MESES_AVALIADOS = 3;

	public QuitacaoDAO(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	public List<PropostaQuitacao> buscarPropostasQuitacao() {
		String query = "FROM PropostaQuitacao";
		Query q1 = session.createQuery(query);

		return q1.list();
	}

	@SuppressWarnings("unchecked")
	public List<PropostaQuitacao> buscarPropostasQuitacaoPorDataEEmpresa(
			Date dataReferencia, Empresa empresa) {
		String queryString = "FROM PropostaQuitacao pq WHERE 1 = 1 ";
		if (dataReferencia != null) {
			queryString += " and pq.dataReferencia = :dataReferencia";
		}

		if (empresa != null) {
			queryString += " and pq.empresa = :empresa";
		}

		queryString += " order by pq.dataReferencia ";

		// Parametros
		Query query = session.createQuery(queryString);
		if (dataReferencia != null) {
			query.setParameter("dataReferencia", dataReferencia);
		}

		if (empresa != null) {
			query.setParameter("empresa", empresa);
		}

		return query.list();
	}

	public Double getVidasPorDataEEmpresa(Date dataReferencia, Empresa empresa) {
		String queryString = "SELECT COUNT(*) FROM PropostaQuitacao pq ";
		queryString += " WHERE 1 = 1";

		if (dataReferencia != null) {
			queryString += " and pq.dataReferencia = :dataReferencia";
		}

		if (empresa != null) {
			queryString += " and pq.empresa = :empresa";
		}

		// Parametros
		Query query = session.createQuery(queryString);
		if (dataReferencia != null) {
			query.setParameter("dataReferencia", dataReferencia);
		}

		if (empresa != null) {
			query.setParameter("empresa", empresa);
		}

		return Double.valueOf(query.uniqueResult().toString());
	}

	// TODO comentar e modularizar esse c�digo
	public Double getValorPorDataEEmpresa(Date dataReferencia, Date primeiraDataReferencia, Empresa empresa) {
		String queryString = "FROM Quitacao q left join fetch q.propostaQuitacao pq  WHERE 1 = 1 ";		

		if(dataReferencia != null) {
			queryString += " and pq.dataReferencia between :ultimaDataAvaliadaPQ and :dataReferencia ";
			queryString += " and q.data between :ultimaDataAvaliadaQ and :dataReferencia ";
		}
		
		if(empresa != null) {
			queryString += " and pq.empresa = :empresa ";
		}
		
		// queryString += " order by q.data desc ";
		queryString += " order by pq.proposta.id, q.data desc, pq.dataReferencia desc ";
		
		// Parametros
		Query query = session.createQuery(queryString);
		Calendar ultimaDataAvaliadaPQ = Calendar.getInstance();
		Calendar ultimaDataAvaliadaQ = Calendar.getInstance();
		
		if(dataReferencia != null) {
			ultimaDataAvaliadaPQ.setTime(dataReferencia);
			ultimaDataAvaliadaPQ.add(Calendar.MONTH,-2);
			
			query.setParameter("ultimaDataAvaliadaPQ", DateUtil.getUltimaDiaMes(ultimaDataAvaliadaPQ.getTime()));
			query.setParameter("dataReferencia", DateUtil.getUltimaDiaMes(dataReferencia));
			
			int quantidadeQuitcacoesPorDataEEmpresa = countQuantidadeQuitcacoesPorDataEEmpresa(dataReferencia, empresa);
			
			ultimaDataAvaliadaQ.setTime(dataReferencia);
			ultimaDataAvaliadaQ.add(Calendar.MONTH, -quantidadeQuitcacoesPorDataEEmpresa);
			query.setParameter("ultimaDataAvaliadaQ", DateUtil.getUltimaDiaMes(ultimaDataAvaliadaQ.getTime()));
		}

		if(empresa != null) {
			query.setParameter("empresa", empresa);
		}

		List<Quitacao> quitacoes = query.list();
		double valor = 0.0;
		
		if (quitacoes == null || quitacoes.isEmpty()) {
			return valor;
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(quitacoes.get(0).getData());
		// mes inexistente para que o primeiro valor seja 	valor += quitacao.getPropostaQuitacao().getValorTotal();
		Integer mes = 13;
		Integer propostaAnterior = -1;
		

		Calendar dataDeParada = Calendar.getInstance();
		dataDeParada.setTime(quitacoes.get(quitacoes.size()-1).getPropostaQuitacao().getDataReferencia());
		
		Calendar ultimaDataPropostaQuitacaoAvaliada = Calendar.getInstance();
		ultimaDataPropostaQuitacaoAvaliada.setTime(dataDeParada.getTime());
		ultimaDataPropostaQuitacaoAvaliada.add(Calendar.MONTH,1);
		
		Calendar ultimaDataPropostaQuitacaoAvaliadaAux = Calendar.getInstance();
		ultimaDataPropostaQuitacaoAvaliadaAux.setTime(dataDeParada.getTime());
		// System.out.println("\n +++++++++++++++++++ " + dataDeParada.getTime() +  " +++++++++++++++++++ ");		
		
		for (Quitacao quitacao : quitacoes) {
			calendar.setTime(quitacao.getData());
			Double valorParcial = quitacao.isQuitado() ? quitacao.getPropostaQuitacao().getValorTotal() : 0.0;

			Calendar ultimaDataAvaliadaPorQuitaca = Calendar.getInstance();
			ultimaDataAvaliadaPorQuitaca.setTime(quitacao.getPropostaQuitacao().getDataReferencia());
			ultimaDataAvaliadaPorQuitaca.add(Calendar.MONTH,-2);
			
            if (!quitacao.getData().before(ultimaDataAvaliadaPorQuitaca.getTime())) {
            	
            	boolean ultimaPropostaQuitacao = !dataReferencia.after(dataDeParada.getTime()) && !dataDeParada.before(dataDeParada.getTime()); 
            	boolean primeiraQuitacaoUltimaPropostaQuitacao = ultimaPropostaQuitacao && !quitacao.getData().before(ultimaDataPropostaQuitacaoAvaliadaAux.getTime());
        		// System.out.println("\n --------------------- pq = " + quitacao.getPropostaQuitacao().getDataReferencia() + ", q = " + quitacao.getData() + ", p = " + quitacao.getPropostaQuitacao().getProposta().getId() + ", passa = " + (!ultimaPropostaQuitacao || primeiraQuitacaoUltimaPropostaQuitacao) );
            	
				if (!ultimaPropostaQuitacao || primeiraQuitacaoUltimaPropostaQuitacao)  {
					
	            	Integer idPropostaSegurado = quitacao.getPropostaQuitacao().getProposta().getId();
					boolean propostaIgual = idPropostaSegurado.equals(propostaAnterior);
	            	
					if (mes.equals(calendar.get(Calendar.MONTH)) && propostaIgual) {
						valor -= valorParcial;	
					} else {
						valor += valorParcial;
					}
					
					mes = calendar.get(Calendar.MONTH);
	            	propostaAnterior = idPropostaSegurado;
				}
            }
		}
		
		return valor;
	}

	public void removerQuitacoes(Date dataReferencia) {
		String queryString = "DELETE FROM PropostaQuitacao WHERE dataReferencia = :data ";
		Query query = session.createQuery(queryString);
		query.setParameter("data", dataReferencia);
		query.executeUpdate();
	}

	public void importarDados(Collection<PropostaQuitacao> propostasQuitacao,
			Date dataReferencia) {
		Transaction tx = session.beginTransaction();
		removerQuitacoes(dataReferencia);
		for (PropostaQuitacao propostaQuitacao : propostasQuitacao) {
			session.save(propostaQuitacao);
		}
		tx.commit();

	}

	public int countQuantidadeQuitcacoesPorDataEEmpresa(Date dataReferencia,
			Empresa empresa) {
		String queryString = "FROM Quitacao q left join fetch q.propostaQuitacao pq  WHERE 1 = 1 ";

		if (dataReferencia != null) {
			queryString += " and pq.dataReferencia = :dataReferencia ";
		}

		if (empresa != null) {
			queryString += " and pq.empresa = :empresa ";
		}

		// Parametros
		Query query = session.createQuery(queryString);
		if (dataReferencia != null) {
			query.setParameter("dataReferencia", dataReferencia);
		}

		if (empresa != null) {
			query.setParameter("empresa", empresa);
		}

		int quantidadeQuitcacoes = query.list().size();
		int quantidadeVidas = getVidasPorDataEEmpresa(dataReferencia, empresa)
				.intValue();
		int resp = 0;

		if (quantidadeVidas != 0) {
			resp = quantidadeQuitcacoes / quantidadeVidas;
		}

		return resp;
	}

	public Double getValorMesAtualPorDataEEmpresa(Date dataReferencia,
			Empresa empresa) {
		String queryString = "FROM Quitacao q left join fetch q.propostaQuitacao pq  WHERE 1 = 1 ";

		if (dataReferencia != null) {
			queryString += " and q.data = :dataReferencia and pq.dataReferencia = :dataReferencia";
		}
		if (empresa != null) {
			queryString += " and pq.empresa = :empresa ";
		}

		// Parametros
		Query query = session.createQuery(queryString);

		if (dataReferencia != null) {
			query.setParameter("dataReferencia", dataReferencia);
		}
		if (empresa != null) {
			query.setParameter("empresa", empresa);
		}

		List<Quitacao> quitacoes = query.list();
		double valor = 0.0;

		if (quitacoes == null || quitacoes.isEmpty()) {
			return valor;
		}

		for (Quitacao quitacao : quitacoes) {
			valor += quitacao.isQuitado() ? quitacao.getPropostaQuitacao()
					.getValorTotal() : 0.0;
		}

		return valor;
	}
}