package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Beneficiario;

public class BeneficiarioDAO {

	private Session session;
	
	public BeneficiarioDAO( Session session ){
		this.session = session;
	}
	
	
	public List<Beneficiario> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( Beneficiario.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		return c.list();
	}
	
	
}
