package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.HistoricoCapital;
import br.com.itavida.entidades.HistoricoConjuge;

public class HistoricoConjugeDAO {

	private Session session;
	
	public HistoricoConjugeDAO( Session session ){
		this.session = session;
	}
	
	
	public List<HistoricoConjuge> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( HistoricoConjuge.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		c.addOrder(Order.desc("dataAlteracao"));
		return c.list();
	}
	
	
}
