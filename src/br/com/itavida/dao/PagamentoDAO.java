package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Cidade;
import br.com.itavida.entidades.DetalhePagamento;
import br.com.itavida.entidades.Pagamento;

public class PagamentoDAO {

	private Session session;
	
	public PagamentoDAO( Session session ){
		this.session = session;
	}
	
	
	public List<Pagamento> buscaPorFiltro( Pagamento pagamentoFiltro ){
		Criteria c = this.session.createCriteria( Pagamento.class );

		if( pagamentoFiltro.getConta() != null  && pagamentoFiltro.getConta().getId() != null && pagamentoFiltro.getConta().getId() != 0){
			c.createAlias("conta", "conta");
			c.add( Restrictions.eq("conta.id", pagamentoFiltro.getConta().getId() ) );
		}
		
		if( pagamentoFiltro.getTipoDocumento() != null  && pagamentoFiltro.getTipoDocumento().getId() != null && pagamentoFiltro.getTipoDocumento().getId() != 0){
			c.createAlias("tipoDocumento", "tipoDocumento");
			c.add( Restrictions.eq("tipoDocumento.id", pagamentoFiltro.getTipoDocumento().getId() ) );
		}
		
		if( pagamentoFiltro.getNumeroDocumento() != null  && !"".equals( pagamentoFiltro.getNumeroDocumento() ) ){
			c.add( Restrictions.ilike("numeroDocumento", pagamentoFiltro.getNumeroDocumento(), MatchMode.ANYWHERE ) );
		}
		
		if( pagamentoFiltro.getNominal() != null  && !"".equals( pagamentoFiltro.getNominal() ) ){
			c.add( Restrictions.ilike("nominal", pagamentoFiltro.getNominal(), MatchMode.ANYWHERE ) );
		}
		
		if( pagamentoFiltro.getDataDocumento() != null  ) {
			c.add( Restrictions.eq("dataDocumento", pagamentoFiltro.getDataDocumento() ) );
		}
		
		if( pagamentoFiltro.getValor() != null && pagamentoFiltro.getValor() != 0.0 ) {
			c.add( Restrictions.eq("valor", pagamentoFiltro.getValor() ) );
		}
		
		if( pagamentoFiltro.getDataCancelamento() != null ) {
			c.add( Restrictions.eq("dataCancelamento", pagamentoFiltro.getDataCancelamento() ) );
		}
		
		if( pagamentoFiltro.getMotivoCancelamento() != null  && !"".equals( pagamentoFiltro.getMotivoCancelamento() ) ){
			c.add( Restrictions.ilike("motivoCancelamento", pagamentoFiltro.getMotivoCancelamento(), MatchMode.ANYWHERE ) );
		}
		

		return c.list();
	}
	
	
	public List<DetalhePagamento> buscaPorFiltroRelatorio( Pagamento pagamentoFiltro ){
		Criteria c = this.session.createCriteria( DetalhePagamento.class );
		c.createAlias("pagamento", "pagamento");
		
		if( pagamentoFiltro.getConta() != null  && pagamentoFiltro.getConta().getId() != null && pagamentoFiltro.getConta().getId() != 0){
			c.createAlias("pagamento.conta", "pagamento.conta");
			c.add( Restrictions.eq("pagamento.conta.id", pagamentoFiltro.getConta().getId() ) );
		}
		
		if( pagamentoFiltro.getTipoDocumento() != null  && pagamentoFiltro.getTipoDocumento().getId() != null && pagamentoFiltro.getTipoDocumento().getId() != 0){
			c.createAlias("pagamento.tipoDocumento", "pagamento.tipoDocumento");
			c.add( Restrictions.eq("pagamento.tipoDocumento.id", pagamentoFiltro.getTipoDocumento().getId() ) );
		}
		
		if( pagamentoFiltro.getNumeroDocumento() != null  && !"".equals( pagamentoFiltro.getNumeroDocumento() ) ){
			
			c.add( Restrictions.ilike("pagamento.numeroDocumento", pagamentoFiltro.getNumeroDocumento(), MatchMode.ANYWHERE ) );
		}
		
		if( pagamentoFiltro.getNominal() != null  && !"".equals( pagamentoFiltro.getNominal() ) ){
			c.add( Restrictions.ilike("pagamento.nominal", pagamentoFiltro.getNominal(), MatchMode.ANYWHERE ) );
		}
		
		if( pagamentoFiltro.getDataDocumentoInicio() != null  ) {
			c.add( Restrictions.ge("pagamento.dataDocumento", pagamentoFiltro.getDataDocumentoInicio() ) );
		}
		
		if( pagamentoFiltro.getDataDocumentoFim() != null  ) {
			c.add( Restrictions.le("pagamento.dataDocumento", pagamentoFiltro.getDataDocumentoFim() ) );
		}
		
		if( pagamentoFiltro.getValorInicio() != null && pagamentoFiltro.getValorInicio() != 0.0 ) {
			c.add( Restrictions.ge("pagamento.valor", pagamentoFiltro.getValorInicio() ) );
		}
		
		if( pagamentoFiltro.getValorFim() != null && pagamentoFiltro.getValorFim() != 0.0 ) {
			c.add( Restrictions.ge("pagamento.valor", pagamentoFiltro.getValorFim() ) );
		}
		
		if( pagamentoFiltro.getDataCancelamentoInicio() != null ) {
			c.add( Restrictions.ge("pagamento.dataCancelamento", pagamentoFiltro.getDataCancelamentoInicio() ) );
		}
		
		if( pagamentoFiltro.getDataCancelamentoFim() != null ) {
			c.add( Restrictions.ge("pagamento.dataCancelamento", pagamentoFiltro.getDataCancelamentoFim() ) );
		}
		
		if( pagamentoFiltro.getMotivoCancelamento() != null  && !"".equals( pagamentoFiltro.getMotivoCancelamento() ) ){
			c.add( Restrictions.ilike("pagamento.motivoCancelamento", pagamentoFiltro.getMotivoCancelamento(), MatchMode.ANYWHERE ) );
		}
		
		if( pagamentoFiltro.getDetalhePagamentoFiltro().getDespesa() != null && pagamentoFiltro.getDetalhePagamentoFiltro().getDespesa().getId() != null ){
			c.createAlias("despesa", "despesa");
			c.add( Restrictions.eq( "despesa.id", pagamentoFiltro.getDetalhePagamentoFiltro().getDespesa().getId() ) );
		}
		
		if( pagamentoFiltro.getDetalhePagamentoFiltro().getFilial() != null && pagamentoFiltro.getDetalhePagamentoFiltro().getFilial().getId() != null ){
			c.createAlias("filial", "filial");
			c.add( Restrictions.eq( "filial.id", pagamentoFiltro.getDetalhePagamentoFiltro().getFilial().getId() ) );
		}
		if( pagamentoFiltro.getDetalhePagamentoFiltro().getFuncionario() != null && pagamentoFiltro.getDetalhePagamentoFiltro().getFuncionario().getId() != null ){
			c.createAlias("funcionario", "funcionario");
			c.add( Restrictions.eq( "funcionario.id", pagamentoFiltro.getDetalhePagamentoFiltro().getFuncionario().getId() ) );
		}		
		if( pagamentoFiltro.getDetalhePagamentoFiltro().getGrupoDespesa() != null && pagamentoFiltro.getDetalhePagamentoFiltro().getGrupoDespesa().getId() != null ){
			c.createAlias("grupoDespesa", "grupoDespesa");
			c.add( Restrictions.eq( "grupoDespesa.id", pagamentoFiltro.getDetalhePagamentoFiltro().getGrupoDespesa().getId() ) );
		}	
		
		
		return c.list();
	}	
	
	
	
	
}
