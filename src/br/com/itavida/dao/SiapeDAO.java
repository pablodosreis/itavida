package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Siape;

public class SiapeDAO {

	private Session session;
	
	public SiapeDAO( Session session ){
		this.session = session;
	}
	
	public List<Siape> buscaPorFiltro( Siape siape ){
		Criteria c = session.createCriteria( Siape.class );
		
		if( siape.getDataReferencia() != null ){
			c.add( Restrictions.eq("dataReferencia", siape.getDataReferencia() ) );		
		}
		
		if( siape.getNomeServidor() != null && !"".equals( siape.getNomeServidor() ) ){
			c.add( Restrictions.ilike("nomeServidor", siape.getNomeServidor(), MatchMode.ANYWHERE ) );
		}
		
		if( siape.getUfpag() != null && !"".equals( siape.getUfpag() ) ){
			c.add( Restrictions.ilike("ufpag", siape.getUfpag(), MatchMode.ANYWHERE ) );
		}
		
		c.addOrder( Order.asc("nomeServidor") );
		
		return c.list();
	}
	
}
