package br.com.itavida.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.itavida.entidades.MovimentacaoOrgaoAtrasado;
import br.com.itavida.entidades.MovimentacaoOrgaoInadimplente;
import br.com.itavida.entidades.MovimentacaoPropostaInadimplente;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.exceptions.SistemaException;

public class MovimentacaoOrgaoInadimplenteDAO {

private Session session;
	
	public MovimentacaoOrgaoInadimplenteDAO( Session session ){
		this.session = session;
	}
	/**
	 * Busca as propostas relacionadas com um dado orgao
	 * @param orgao - orgao selecionado pelo usuario
	 * @return - Lista de propostas do orgao passado como parametro
	 */
	public List<Proposta> buscaPropostasOrgao(Orgao orgao){
		String hql = " from Proposta where  orgao = :orgao";
		Query query = session.createQuery(hql);
		query.setParameter("orgao", orgao);
		return query.list();
		
	}
	
	public void saveOrUpdate(MovimentacaoOrgaoInadimplente obj) {	
		
		if (jaExisteMovimentacao(obj)) {
			throw new SistemaException("J� existe um registro cadastrado para este �rg�o.");
		}		
		session.saveOrUpdate(obj);
	}
	
	public void delete(MovimentacaoOrgaoInadimplente obj) {
		MovimentacaoOrgaoAtrasadoDAO atrasadoDao = new MovimentacaoOrgaoAtrasadoDAO(session);
		if (atrasadoDao.jaExisteMovimentacao(obj.getOrgao(), obj.getReferencia())) {
			throw new SistemaException("N�o � poss�vel excluir esta movimenta��o porque j� existe uma movimenta��o informando a sua quita��o.");
		}
		session.delete(obj);
	}
	
	
	public List<MovimentacaoOrgaoInadimplente> buscarMovimentacoes(Date referencia) {
		String hql = " from MovimentacaoOrgaoInadimplente where  referencia = :referencia order by orgao.nomeOrgao ";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", referencia);
		return query.list();
	}
	
	public boolean jaExisteMovimentacao(Date referencia, Orgao orgao) {
		MovimentacaoOrgaoInadimplente obj = new MovimentacaoOrgaoInadimplente();
		obj.setReferencia(referencia);
		obj.setOrgao(orgao);
		return jaExisteMovimentacao(obj);
		
	}
	
	public boolean jaExisteMovimentacao(MovimentacaoOrgaoInadimplente obj) {
		String hql = " from MovimentacaoOrgaoInadimplente where referencia = :referencia and orgao = :orgao order by orgao.nomeOrgao ";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", obj.getReferencia());
		query.setParameter("orgao", obj.getOrgao());
		return query.uniqueResult() != null;	
	}
	
	public MovimentacaoOrgaoInadimplente getMovimentacao(Date referencia, Orgao orgao) {
		String hql = " from MovimentacaoOrgaoInadimplente where referencia = :referencia and orgao = :orgao order by orgao.nomeOrgao ";
		Query query = session.createQuery(hql);
		query.setParameter("referencia", referencia);
		query.setParameter("orgao", orgao);
		return (MovimentacaoOrgaoInadimplente) query.uniqueResult();	
	}

}
