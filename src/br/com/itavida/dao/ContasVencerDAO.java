package br.com.itavida.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.ContasVencer;
import br.com.itavida.util.Constantes;

public class ContasVencerDAO {

	private Session session;
	
	public ContasVencerDAO( Session session ){
		this.session = session;
	}
	
	
	public List<ContasVencer> busca( ContasVencer contasVencerFiltro ){
		Criteria c = this.session.createCriteria( ContasVencer.class );

		if( contasVencerFiltro.getDespesa() != null && contasVencerFiltro.getDespesa().getId() != null ){
			c.createAlias("despesa", "despesa");
			c.add( Restrictions.eq("despesa.id", contasVencerFiltro.getDespesa().getId() ) );
		}
		
		if( contasVencerFiltro.getValorDespesa() != null && contasVencerFiltro.getValorDespesa() != 0.0 ){
			c.add( Restrictions.eq("valorDespesa", contasVencerFiltro.getValorDespesa() ) );
		}		
		
		if( contasVencerFiltro.getDataVencimentoInicio() != null  ){
			c.add( Restrictions.ge("dataVencimento", contasVencerFiltro.getDataVencimentoInicio() ) );
		}
		
		if( contasVencerFiltro.getDataVencimentoFim() != null  ){
			c.add( Restrictions.le("dataVencimento", contasVencerFiltro.getDataVencimentoFim() ) );
		}
		
		if( contasVencerFiltro.getContaPaga() != null  && !"".equals( contasVencerFiltro.getContaPaga() )){
			c.add( Restrictions.eq("contaPaga", contasVencerFiltro.getContaPaga() ) );
		}
		
		if( contasVencerFiltro.getDataPagamentoInicio() != null ){
			c.add( Restrictions.ge("dataPagamento", contasVencerFiltro.getDataPagamentoInicio() ) );
		}
		
		if( contasVencerFiltro.getDataPagamentoFim() != null ){
			c.add( Restrictions.le("dataPagamento", contasVencerFiltro.getDataPagamentoFim() ) );
		}
		
		return c.list();
	}
	
	public List<ContasVencer> buscaContasVencidasOuAVencer(){
		Criteria c = this.session.createCriteria( ContasVencer.class );
		Date dataAtual =  new Date();
		c.add( Restrictions.eq("contaPaga", Constantes.NAO ) );
		c.add( Restrictions.le("dataVencimento", dataAtual ) );
		return c.list();
	}
	
	
	public List<ContasVencer> buscaContasNaoPagas(){
		Criteria c = this.session.createCriteria( ContasVencer.class );
		c.add( Restrictions.eq("contaPaga", Constantes.NAO ) );
		return c.list();
	}
	
}
