package br.com.itavida.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import org.hibernate.Session;

import br.com.itavida.dao.base.BaseDAO;
import br.com.itavida.entidades.Beneficiario;
import br.com.itavida.entidades.HistoricoBeneficiario;
import br.com.itavida.entidades.HistoricoCapital;
import br.com.itavida.entidades.HistoricoConjuge;
import br.com.itavida.entidades.HistoricoEnderecoSegurado;
import br.com.itavida.entidades.HistoricoProposta;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.Usuario;
import br.com.itavida.handlers.UsuarioHandler;
import br.com.itavida.util.Comparador;
import br.com.itavida.util.HibernateUtil;

public class NovosValoresDAO extends BaseDAO {

	
	public void salvar(List<Proposta> propostas) {
		Session session = getSession();
		int row = 0;
		for (Proposta proposta : propostas) {
			System.out.println(row++ + "Salvar Proposta[" + proposta.getId() + "]" );
			salvar(proposta);
		}
	}

	public void salvar(Proposta propostaGravar) {
		
		UsuarioHandler usuarioHandler = (UsuarioHandler) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioHandler");
		Usuario usuario = usuarioHandler.getUsuario();
		
		if (propostaGravar.getId() != null && propostaGravar.getId() != 0) {
			// SOMENTE PARA FLUXO DE ALTERA��O

			// Realiza a verifica��o de Hist�rico de Mudan�as para Benefici�rios
			verificaMudancasBeneficiarios(propostaGravar, usuario);

			// Realiza a verifica��o de Hist�rico de Mudan�as para Capital
			verificaMudancasCapital(propostaGravar, usuario);

			// Realiza a verifica��o de Hist�rico de Mudan�as para Capital
			verificaMudancasConjuge( propostaGravar, usuario);

			// Realiza a verifica��o de Hist�rico de Mudan�as para Capital
			verificaMudancasEndereco(propostaGravar, usuario);

			// Realiza a verifica��o de Hist�rico de Mudan�as para Capital
			verificaMudancasProposta(propostaGravar, usuario);

			// � uma altera��o
			propostaGravar.setDataAlteracao(new Date());
			propostaGravar.setNomeUsuarioAlteracao(usuario.getNomeUsuario());
			
			getSession().saveOrUpdate(propostaGravar);
		}
	}
	
	
	private Double calcularDiferenca(Proposta propostaGravar) {
		Session session = getSession();
 		if(propostaGravar.getId() != null) {
			Proposta propostaAnterior = (Proposta) session.get(Proposta.class, propostaGravar.getId());
	 		if(propostaAnterior.getTotalPremios() != null && propostaGravar.getTotalPremios() != null) {
	 			return propostaGravar.getTotalPremios() - propostaAnterior.getTotalPremios();
	 		}
 		}
 		return 0.0;
	}

	/**
	 * @param session
	 * @param propostaGravar
	 * @param usuario 
	 */
	private void verificaMudancasBeneficiarios(Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		Session newSession = HibernateUtil.openNewSession();
		BeneficiarioDAO beneficiarioDAO =  new BeneficiarioDAO( newSession );
		List<Beneficiario> listaBeneficiariosBanco = beneficiarioDAO.buscaPorProposta( propostaGravar.getId() );
		for (Beneficiario beneficiarioTmp : listaBeneficiariosBanco) {
			newSession.evict( beneficiarioTmp );
		}
		
		newSession.flush();
		List<HistoricoBeneficiario> listaAlterados = new ArrayList<HistoricoBeneficiario>();
		
		for (Beneficiario beneficiarioBanco : listaBeneficiariosBanco) {
			boolean achou = false;
			for (Beneficiario beneficiario : propostaGravar.getBeneficiario_collection()) {
				if( beneficiarioBanco.getNomeBeneficiario().trim().toUpperCase().equals( beneficiario.getNomeBeneficiario().trim().toUpperCase() ) &&
					beneficiarioBanco.getGrauParentesco().getId().equals( beneficiario.getGrauParentesco().getId() ) &&
					beneficiarioBanco.getPercentual().equals( beneficiario.getPercentual() ) ){
					achou = true;
				}
			}
			
			if( !achou ){
				HistoricoBeneficiario historicoBeneficiario = new HistoricoBeneficiario();
				historicoBeneficiario.setNomeBeneficiario( beneficiarioBanco.getNomeBeneficiario() );
				historicoBeneficiario.setGrauParentesco( beneficiarioBanco.getGrauParentesco() );
				historicoBeneficiario.setPercentual( beneficiarioBanco.getPercentual() );
				historicoBeneficiario.setDataAlteracao( new Date() );
				historicoBeneficiario.setProposta( propostaGravar );
				historicoBeneficiario.setNomeUsuario( usuario.getNomeUsuario() );
				listaAlterados.add( historicoBeneficiario );
			}
		}
		
		if( propostaGravar.getHistoricobeneficiario_collection() == null  ){
			propostaGravar.setHistoricobeneficiario_collection( new ArrayList<HistoricoBeneficiario>() );
		}
		propostaGravar.getHistoricobeneficiario_collection().addAll( listaAlterados );
	}
	private boolean verificaString (String s) {
		return s != null && !s.trim().equals("");
	}
	private void verificaMudancasConjuge(Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(HibernateUtil.openNewSession(), Proposta.class);
		Proposta p = daoProposta.get(propostaGravar.getId());
		List<HistoricoConjuge> listaAlterados = new ArrayList<HistoricoConjuge>();
			boolean achouMudanda = false;
			boolean cpfDiferente = verificaString(propostaGravar.getCpfConjuge()) && verificaString(p.getCpfConjuge()) && !propostaGravar.getCpfConjuge().equals(p.getCpfConjuge());
			boolean dataNascimentoDiferente = propostaGravar.getDataNascimentoConjuge() != null && p.getDataNascimentoConjuge() != null &&  
				propostaGravar.getDataNascimentoConjuge().compareTo(p.getDataNascimentoConjuge()) != 0;
			boolean estadoCivilDiferente = verificaString(propostaGravar.getEstadoCivilConjuge()) && !propostaGravar.getEstadoCivilConjuge().equals(p.getEstadoCivilConjuge());
			boolean matriculaDiferente = verificaString(propostaGravar.getMatriculaConjuge()) && !propostaGravar.getMatriculaConjuge().equals(p.getMatriculaConjuge());
			boolean nomeConjugeDiferente = verificaString(propostaGravar.getNomeConjuge()) && !propostaGravar.getNomeConjuge().equals(p.getNomeConjuge());
			boolean rgConjuge = verificaString(propostaGravar.getRgConjuge()) && !propostaGravar.getRgConjuge().equals(p.getRgConjuge());
			boolean sexoConjuge = verificaString(propostaGravar.getSexoConjuge()) && !propostaGravar.getSexoConjuge().equals(p.getSexoConjuge());
			boolean premioDiferente = Comparador.houveAlteracao(p.getValorPremioConjuge(), propostaGravar.getValorPremioConjuge());
			boolean capitalDiferente = Comparador.houveAlteracao(p.getValorCapitalConjuge(), propostaGravar.getValorCapitalConjuge());
			
			achouMudanda = cpfDiferente || dataNascimentoDiferente || estadoCivilDiferente || matriculaDiferente || nomeConjugeDiferente ||
				rgConjuge || sexoConjuge || premioDiferente || capitalDiferente;
			if(achouMudanda){
				HistoricoConjuge historicoConjuge = new HistoricoConjuge();
				historicoConjuge.setCpfConjuge(p.getCpfConjuge());
				historicoConjuge.setDataAlteracao(new Date());
				historicoConjuge.setDataNascimentoConjuge(p.getDataNascimentoConjuge());
				historicoConjuge.setEstadoCivilConjuge(p.getEstadoCivilConjuge());
				historicoConjuge.setMatriculaConjuge(p.getMatriculaConjuge());
				historicoConjuge.setNomeConjuge(p.getNomeConjuge());
				historicoConjuge.setNomeUsuario(usuario.getNomeUsuario());
				historicoConjuge.setProposta(propostaGravar);
				historicoConjuge.setRgConjuge(p.getRgConjuge());
				historicoConjuge.setSexoConjuge(p.getSexoConjuge());
				historicoConjuge.setPremio(propostaGravar.getValorPremioConjuge());
				historicoConjuge.setCapital(propostaGravar.getValorCapitalConjuge());
				listaAlterados.add( historicoConjuge );
			}
		
		if( propostaGravar.getHistoricobeneficiario_collection() == null  ){
			propostaGravar.setHistoricobeneficiario_collection( new ArrayList<HistoricoBeneficiario>() );
		}
		propostaGravar.getHistoricoconjuge_collection().addAll( listaAlterados );
	}
	
	private void verificaMudancasProposta(Proposta propostaGravar, Usuario usuario) {
		//Precisa verificar se houve mudan�a de benefici�rio
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(HibernateUtil.openNewSession(), Proposta.class);
		Proposta p = daoProposta.get(propostaGravar.getId());
		List<HistoricoProposta> listaAlterados = new ArrayList<HistoricoProposta>();
		boolean achouDiferenca = false;
		boolean empresaIgual = propostaGravar.getEmpresa() != null && propostaGravar.getEmpresa().getId().equals(p.getEmpresa().getId());
		boolean nomeTabelaIgual = true;
		if(propostaGravar.getNomeTabela() != null && p.getNomeTabela() != null) {
			if(propostaGravar.getNomeTabela().getId() != null && p.getNomeTabela().getId() != null) {
				nomeTabelaIgual = propostaGravar.getNomeTabela() != null && propostaGravar.getNomeTabela().getId().equals(p.getNomeTabela().getId());
			}
		}
		boolean orgaoIgual = propostaGravar.getOrgao() != null && propostaGravar.getOrgao().getId().equals(p.getOrgao().getId());
		boolean modeloIgual = propostaGravar.getModeloProposta() != null && propostaGravar.getModeloProposta().getId().equals(p.getModeloProposta().getId());
		boolean tipoIgual = propostaGravar.getTipoProposta() != null && propostaGravar.getTipoProposta().getId().equals(p.getTipoProposta().getId());
		if (!empresaIgual || !nomeTabelaIgual || !orgaoIgual || !modeloIgual || !tipoIgual) {
			achouDiferenca = true;
		}
		if (achouDiferenca) {
			HistoricoProposta historicoProposta = new HistoricoProposta();
			historicoProposta.setEmpresa(p.getEmpresa());
			historicoProposta.setDataAlteracao(new Date());
			historicoProposta.setNomeTabela(p.getNomeTabela());
			historicoProposta.setOrgao(p.getOrgao());
			historicoProposta.setProposta(propostaGravar);
			historicoProposta.setModeloProposta(p.getModeloProposta());
			historicoProposta.setTipoProposta(p.getTipoProposta());
			historicoProposta.setNomeUsuario(usuario.getNomeUsuario());
			listaAlterados.add(historicoProposta);
		}
		
		if( propostaGravar.getHistoricobeneficiario_collection() == null  ){
			propostaGravar.setHistoricobeneficiario_collection( new ArrayList<HistoricoBeneficiario>() );
		}
		propostaGravar.getHistoricoproposta_collection().addAll( listaAlterados );
	}
	
	private void verificaMudancasEndereco(Proposta propostaGravar, Usuario usuario) {
		//cria uma nova sess�o para garantir que o objeto recuperado ser� diferente do sendo gravado
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(HibernateUtil.openNewSession(), Proposta.class);
		Proposta p = daoProposta.get(propostaGravar.getId());
		List<HistoricoEnderecoSegurado> listaAlterados = new ArrayList<HistoricoEnderecoSegurado>();
			boolean houveMudancas = false;
			boolean mudouBairro = verificaString(propostaGravar.getBairro()) && !propostaGravar.getBairro().equals(p.getBairro());
			boolean mudouCep = verificaString(propostaGravar.getCep()) && !propostaGravar.getCep().equals(p.getCep());
			boolean mudouCidade = propostaGravar.getCidade() != null && !propostaGravar.getCidade().getId().equals(p.getCidade().getId());
			boolean mudouComplemento = verificaString(propostaGravar.getComplemento()) && !propostaGravar.getComplemento().equals(p.getComplemento());
			boolean mudouEndereco = verificaString(propostaGravar.getEnderecoSegurado()) && !propostaGravar.getEnderecoSegurado().equals(p.getEnderecoSegurado());
			boolean mudouObs = verificaString(propostaGravar.getObservacao()) && !propostaGravar.getObservacao().equals(p.getObservacao());
			boolean mudouTelCel1 = verificaString(propostaGravar.getTelefoneCelular()) && !propostaGravar.getTelefoneCelular().equals(p.getTelefoneCelular());
			boolean mudouTelCel2 = verificaString(propostaGravar.getTelefoneCelular2()) && !propostaGravar.getTelefoneCelular2().equals(p.getTelefoneCelular2());
			boolean mudouTelRes = verificaString(propostaGravar.getTelefoneResidencial()) && !propostaGravar.getTelefoneResidencial().equals(p.getTelefoneResidencial());
			boolean mudouTelCom = verificaString(propostaGravar.getTelefoneComercial()) && !propostaGravar.getTelefoneComercial().equals(p.getTelefoneComercial());
			houveMudancas = mudouBairro || mudouCep || mudouCidade || mudouComplemento || mudouEndereco || mudouObs || mudouTelCel1 || mudouTelCel2 ||
					mudouTelRes || mudouTelCom;
			if( houveMudancas ){
				HistoricoEnderecoSegurado historicoEnderecoSegurado = new HistoricoEnderecoSegurado();
				historicoEnderecoSegurado.setBairro(p.getBairro());
				historicoEnderecoSegurado.setCep(p.getCep());
				historicoEnderecoSegurado.setCidade(p.getCidade());
				historicoEnderecoSegurado.setComplemento(p.getComplemento());
				historicoEnderecoSegurado.setDataAlteracao(new Date());
				historicoEnderecoSegurado.setEnderecoSegurado(p.getEnderecoSegurado());
				historicoEnderecoSegurado.setNomeUsuario(usuario.getNomeUsuario());
				historicoEnderecoSegurado.setObservacao(p.getObservacao());
				historicoEnderecoSegurado.setProposta(propostaGravar);
				historicoEnderecoSegurado.setTelefoneCelular1(p.getTelefoneCelular());
				historicoEnderecoSegurado.setTelefoneCelular2(p.getTelefoneCelular2());
				historicoEnderecoSegurado.setTelefoneComercial(p.getTelefoneComercial());
				historicoEnderecoSegurado.setTelefoneResidencial(p.getTelefoneResidencial());
				listaAlterados.add( historicoEnderecoSegurado );
			}
		
		if( propostaGravar.getHistoricoenderecosegurado_collection() == null  ){
			propostaGravar.setHistoricoenderecosegurado_collection( new ArrayList<HistoricoEnderecoSegurado>() );
		}
		propostaGravar.getHistoricoenderecosegurado_collection().addAll( listaAlterados );
	}
	
	
	/**
	 * @param session
	 * @param propostaGr
	 * avar
	 */
	private void verificaMudancasCapital(Proposta propostaGravar, Usuario usuario) {
		//cria uma nova sess�o para garantir que o objeto recuperado ser� diferente do sendo gravado
		Session newSession = HibernateUtil.openNewSession();
		PropostaDAO propostaDAO = new PropostaDAO(newSession);
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(newSession, Proposta.class);
		Proposta propostaBanco = daoProposta.get( propostaGravar.getId() );
		propostaDAO.populaCapital(propostaBanco);
		propostaDAO.populaCapital(propostaGravar);
		
		boolean capitalAlterado = Comparador.houveAlteracao(propostaBanco.getCapitalSegurado(),propostaGravar.getCapitalSegurado());
		boolean premioAlterado = Comparador.houveAlteracao(propostaBanco.getPremioSegurado(),propostaGravar.getPremioSegurado());
		boolean modeloProposta = Comparador.houveAlteracao(propostaBanco.getModeloProposta(),propostaGravar.getModeloProposta());
		boolean emailAlterado = Comparador.houveAlteracao(propostaBanco.getEmail(), propostaGravar.getEmail());
		boolean sexoAlterado = Comparador.houveAlteracao(propostaBanco.getSexoSegurado(), propostaGravar.getSexoSegurado());
		boolean dataNascimentoAlterado = Comparador.houveAlteracao(propostaBanco.getDataNascimentoSegurado(), propostaGravar.getDataNascimentoSegurado());
		boolean matriculaAlterado = Comparador.houveAlteracao(propostaBanco.getMatriculaSegurado(), propostaGravar.getMatriculaSegurado());
		boolean cpfAlterado = Comparador.houveAlteracao(propostaBanco.getCpfSegurado(), propostaGravar.getCpfSegurado());
		boolean rgAlterado = Comparador.houveAlteracao(propostaBanco.getRgSegurado(), propostaGravar.getRgSegurado());
		boolean estadoCivilAlterado = Comparador.houveAlteracao(propostaBanco.getEstadoCivilSegurado(), propostaGravar.getEstadoCivilSegurado());
		boolean nomeSeguradoAlterado = Comparador.houveAlteracao(propostaBanco.getNomeSegurado(), propostaGravar.getNomeSegurado());
		
		boolean houveAlteracao = capitalAlterado || premioAlterado 
				|| modeloProposta || emailAlterado || sexoAlterado
				|| dataNascimentoAlterado || matriculaAlterado || cpfAlterado
				|| rgAlterado || estadoCivilAlterado || nomeSeguradoAlterado; 
		
		if(!houveAlteracao) {
			return;
		}
		
		newSession.evict( propostaBanco );
		List<HistoricoCapital> listaHistoricoCapital = new ArrayList<HistoricoCapital>();
		
		HistoricoCapital historicoCapital = new HistoricoCapital();
		historicoCapital.setDataAlteracao(new Date());
		historicoCapital.setNomeUsuario( usuario.getNomeUsuario() );
		historicoCapital.setTipoModeloProposta(propostaBanco.getModeloProposta().getDescricao());
		historicoCapital.setProposta( propostaGravar );
		historicoCapital.setPremioAntigo(propostaBanco.getPremioSegurado() != null ? propostaBanco.getPremioSegurado() : 0.0);
		historicoCapital.setCapitalAntigo(propostaBanco.getCapitalSegurado());
		historicoCapital.setEmail(propostaBanco.getEmail());
		historicoCapital.setSexo(propostaBanco.getSexoSegurado());
		historicoCapital.setDataNascimento(propostaBanco.getDataNascimentoSegurado());
		historicoCapital.setMatricula(propostaBanco.getMatriculaSegurado());
		historicoCapital.setCpf(propostaBanco.getCpfSegurado());
		historicoCapital.setRg(propostaBanco.getRgSegurado());
		historicoCapital.setEstadocivil(propostaBanco.getEstadoCivilSegurado());
		historicoCapital.setNomeSegurado(propostaBanco.getNomeSegurado());
		
		listaHistoricoCapital.add( historicoCapital );
		
		Collections.sort(listaHistoricoCapital);
		
		propostaGravar.getHistoricocapital_collection().addAll( listaHistoricoCapital );
		
		
		
	}
}
