package br.com.itavida.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.dao.base.GenericDAO;
import br.com.itavida.entidades.HistoricoSinistro;

public class HistoricoSinistroDAO extends GenericDAO<HistoricoSinistro>{

	
	public HistoricoSinistroDAO(){
		super(HistoricoSinistro.class);
	}
	
	public Integer countSinistrosNosUltimos30dias(){
		
		Calendar bd = GregorianCalendar.getInstance();
		bd.add( Calendar.DAY_OF_MONTH , -30 );
		
		Criteria c = getSession().createCriteria( HistoricoSinistro.class );
		c.add( Restrictions.ge( "dataSinistro", bd.getTime() ) );
		c.add( Restrictions.le( "dataSinistro", new Date() ) );
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	
	
	public List<HistoricoSinistro> buscaPorFiltro( HistoricoSinistro filtro ){
		Criteria c = getSession().createCriteria( HistoricoSinistro.class );
		
		c.createAlias("proposta", "proposta" );
		
		if( filtro.getProposta().getId() != null && filtro.getProposta().getId() != 0 ){
			c.add( Restrictions.eq("proposta.id", filtro.getProposta().getId() ) );
		}
		if( filtro.getProposta().getNomeSegurado() != null && !"".equals(filtro.getProposta().getNomeSegurado()) ){
			c.add( Restrictions.ilike("proposta.nomeSegurado", filtro.getProposta().getNomeSegurado(), MatchMode.ANYWHERE ) );
		}
		if( filtro.getProposta().getNomeConjuge() != null && !"".equals(filtro.getProposta().getNomeConjuge()) ){
			c.add( Restrictions.ilike("proposta.nomeConjuge", filtro.getProposta().getNomeConjuge(), MatchMode.ANYWHERE ) );			
		}
		
		if( filtro.getNumeroSinistro()!=null && !"".equals(filtro.getNumeroSinistro()) ){
			c.add( Restrictions.eq("numeroSinistro", filtro.getNumeroSinistro()  ) );
		}
		
		// Busca por Org�o
		if( filtro.getProposta().getOrgao() != null && filtro.getProposta().getOrgao().getId() != null && filtro.getProposta().getOrgao().getId() != 0){
			c.createAlias("proposta.orgao", "proposta.orgao");
			c.add( Restrictions.eq("proposta.orgao.id", filtro.getProposta().getOrgao().getId() ) );
		}	
		
		if( filtro.getDataSinistroInicio() != null ){
			c.add( Restrictions.ge( "dataSinistro", filtro.getDataSinistroInicio() ) );
		}
		
		if( filtro.getDataSinistroFim() != null ){
			c.add( Restrictions.le( "dataSinistro", filtro.getDataSinistroFim() ) );
		}
		
		if( filtro.getMotivoSinistro() != null &&  filtro.getMotivoSinistro().getId() !=null && filtro.getMotivoSinistro().getId() != 0 ){
			c.createAlias("motivoSinistro", "motivoSinistro" );
			c.add( Restrictions.eq( "motivoSinistro.id", filtro.getMotivoSinistro().getId() ) );
		}
		
		if( filtro.getMotivoSinistroNegado() != null &&  filtro.getMotivoSinistroNegado().getId() !=null &&  filtro.getMotivoSinistroNegado().getId() != 0 ){
			c.createAlias( "motivoSinistroNegado", "motivoSinistroNegado" );
			c.add( Restrictions.eq( "motivoSinistroNegado.id", filtro.getMotivoSinistroNegado().getId() ) );
		}
		
		if (filtro.getProposta().getEmpresa() != null) {
			c.createAlias( "empresa", "empresa" );
			c.add( Restrictions.eq( "empresa.id", filtro.getProposta().getEmpresa().getId()));
		}
		
		if (filtro.getProposta().getCidade() != null && filtro.getProposta().getCidade().getId() != null) {
			c.createAlias( "proposta.cidade", "cidade" );
			c.add( Restrictions.eq( "cidade.id", filtro.getProposta().getCidade().getId()));
		}
		
		c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return  c.list();
	}
	
}
