package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.MotivoCancelamento;
import br.com.itavida.entidades.Proposta;

public class MotivoCancelamentoDAO {

	
	private Session session;
	
	public MotivoCancelamentoDAO( Session session ){
		this.session = session;
	}
	
	public List<MotivoCancelamento> buscaMotivosCancelamentos( ){
		Criteria c = this.session.createCriteria( MotivoCancelamento.class );
		return c.list();
	}
	
	public MotivoCancelamento find(Integer id) {
		Criteria c = this.session.createCriteria( MotivoCancelamento.class );
		c.add( Restrictions.eq("id", id ) );
		return (MotivoCancelamento) c.uniqueResult();
	}
}
