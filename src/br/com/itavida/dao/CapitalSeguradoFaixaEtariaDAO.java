package br.com.itavida.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.CapitalSeguradoFaixaEtaria;
import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;
import br.com.itavida.entidades.NomeTabela;

public class CapitalSeguradoFaixaEtariaDAO {

	private Session session;
	
	public CapitalSeguradoFaixaEtariaDAO( Session session ){
		this.session = session;
	}
	
	
	public List<CapitalSeguradoFaixaEtaria> buscaPorFiltro( CapitalSeguradoFaixaEtaria capitalFiltro ){
		Criteria c = this.session.createCriteria( CapitalSeguradoFaixaEtaria.class );

		if( capitalFiltro.getDataVigencia() != null ){
			c.add( Restrictions.eq("dataVigencia", capitalFiltro.getDataVigencia() ) );
		}
		
		if( capitalFiltro.getNomeTabela() != null && capitalFiltro.getNomeTabela().getId() != null){
			c.add( Restrictions.eq("nomeTabela.id", capitalFiltro.getNomeTabela().getId() ) );
		}	
		
		
		return c.list();
	}
	
	public List<CapitalSeguradoFaixaEtaria> buscaPorNomeTabela( NomeTabela nomeTabela){
		Criteria c = this.session.createCriteria( CapitalSeguradoFaixaEtaria.class );
		
		if( nomeTabela != null && nomeTabela.getId() != null){
			c.add( Restrictions.eq("nomeTabela.id", nomeTabela.getId() ) );
		}	
		
		return c.list();
	}
	
	
	public List<DetalheCapitalFaixaEtaria> buscaPorCapital( double capital, NomeTabela nomeTabela ){
		Criteria criteriaCapital = this.session.createCriteria( CapitalSeguradoFaixaEtaria.class );
		
		if( nomeTabela != null && nomeTabela.getId() != null){
			criteriaCapital.createAlias("nomeTabela", "nomeTabela");
			criteriaCapital.add( Restrictions.eq("nomeTabela.id", nomeTabela.getId() ) );
		}
		
		List<CapitalSeguradoFaixaEtaria> listaCapitalFaixaEtaria = criteriaCapital.list();
				
		
		List<DetalheCapitalFaixaEtaria> listaDetalheCapital = new ArrayList<DetalheCapitalFaixaEtaria>();
		for (Iterator iterator = listaCapitalFaixaEtaria.iterator(); iterator.hasNext();) {
			CapitalSeguradoFaixaEtaria capitalSeguradoFaixaEtaria = (CapitalSeguradoFaixaEtaria) iterator.next();
			Criteria c = this.session.createCriteria( DetalheCapitalFaixaEtaria.class );
			c.add( Restrictions.between("capitalSegurado", capital-1000, capital+1000 ) );
			
			if( capitalSeguradoFaixaEtaria != null && capitalSeguradoFaixaEtaria.getId() != null){				
				c.createAlias("capitalSeguradoFaixaEtaria", "capitalSeguradoFaixaEtaria");
				c.add( Restrictions.eq("capitalSeguradoFaixaEtaria.id", capitalSeguradoFaixaEtaria.getId() ) );
			}	
			
			c.addOrder( Order.asc("capitalSegurado") );
			c.addOrder( Order.asc("idadeInicial") );
			c.addOrder( Order.asc("idadeFinal") );
			
			listaDetalheCapital.addAll( c.list() );
			c = null;
		}
		
		return listaDetalheCapital;
	}
	
	
	public List<DetalheCapitalFaixaEtaria> buscaPorCapitalFaixaEtaria( Integer idCapitalFaixaEtaria ){
		Criteria c = this.session.createCriteria( DetalheCapitalFaixaEtaria.class );
		c.createAlias("capitalSeguradoFaixaEtaria", "capitalSeguradoFaixaEtaria");
		c.add( Restrictions.eq("capitalSeguradoFaixaEtaria.id", idCapitalFaixaEtaria ) );
		
		return c.list();
	}
}
