package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Usuario;

public class UsuarioDAO {

	private Session session;
	
	public UsuarioDAO( Session session ){
		this.session = session;
	}
	
	
	public Usuario busca( Usuario usuarioFiltro ){
		Criteria c = this.session.createCriteria( Usuario.class );

		c.add( Restrictions.eq("usuario", usuarioFiltro.getUsuario() ) );
		
		return (Usuario) c.uniqueResult();
	}
	
	public List<Usuario> buscaTodosUsuarios( ){
		Criteria c = this.session.createCriteria( Usuario.class );
		return c.list();
	}
	
}
