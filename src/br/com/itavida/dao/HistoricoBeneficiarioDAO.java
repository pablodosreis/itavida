package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.HistoricoBeneficiario;

public class HistoricoBeneficiarioDAO {

	private Session session;
	
	public HistoricoBeneficiarioDAO( Session session ){
		this.session = session;
	}
	
	
	public List<HistoricoBeneficiario> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( HistoricoBeneficiario.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		return c.list();
	}
	
	
}
