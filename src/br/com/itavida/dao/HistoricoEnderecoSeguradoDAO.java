package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.HistoricoEnderecoSegurado;

public class HistoricoEnderecoSeguradoDAO {

	private Session session;
	
	public HistoricoEnderecoSeguradoDAO( Session session ){
		this.session = session;
	}
	
	
	public List<HistoricoEnderecoSegurado> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( HistoricoEnderecoSegurado.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		c.addOrder(Order.desc("dataAlteracao"));
		return c.list();
	}
	
	
}
