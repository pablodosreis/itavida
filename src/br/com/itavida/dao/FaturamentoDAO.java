package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.PlanilhaFaturamento;

public class FaturamentoDAO {

	private Session session;
	
	public FaturamentoDAO( Session session ){
		this.session = session;
	}
	
	
	public List<PlanilhaFaturamento> buscaPorFiltro( PlanilhaFaturamento filtro ){
		Criteria c = this.session.createCriteria( PlanilhaFaturamento.class );
		
		
		if( filtro.getOrgao() != null && filtro.getOrgao().getId() != null ){
			c.createAlias("orgao", "orgao" );
			c.add( Restrictions.eq("orgao.id", filtro.getOrgao().getId() ) );
		}
		if( filtro.getMesAnoVigenciaInicio() != null  ){
			c.add( Restrictions.ge("mesAnoVigencia", filtro.getMesAnoVigenciaInicio() ) );
		}
		
		if( filtro.getMesAnoVigenciaFim() != null  ){
			c.add( Restrictions.le("mesAnoVigencia", filtro.getMesAnoVigenciaFim() ) );
		}
		
		if( filtro.getMesAnoCobrancaInicio() != null ){
			c.add( Restrictions.ge("mesAnoCobranca", filtro.getMesAnoCobrancaInicio() ) );
		}		
		if( filtro.getMesAnoCobrancaFim() != null ){
			c.add( Restrictions.le("mesAnoCobranca", filtro.getMesAnoCobrancaFim() ) );
		}			
		
		if( filtro.getDataPagamentoInicio() != null  ){
			c.add( Restrictions.ge("dataPagamento", filtro.getDataPagamentoInicio() ) );
		}
		
		if( filtro.getDataPagamentoFim() != null  ){
			c.add( Restrictions.le("dataPagamento", filtro.getDataPagamentoFim() ) );
		}
		if( filtro.getEstado() != null && filtro.getEstado() != 0 ){
			c.createAlias("orgao.cidade.cod_estado", "estado" );
			c.add( Restrictions.eq("estado.cod_estado", filtro.getEstado() ) );
		}
		
		return  c.list();
	}
	
}
