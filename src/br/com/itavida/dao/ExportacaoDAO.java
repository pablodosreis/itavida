package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Proposta;

public class ExportacaoDAO {

	private Session session;
	
	public ExportacaoDAO( Session session ){
		this.session = session;
	}

	
	public List<Proposta> buscaPorOrgao( Integer[] orgaosSelecionados, boolean allOrgaos ){
		Criteria c = this.session.createCriteria( Proposta.class );
		
		// Busca por Org�o
		if( orgaosSelecionados != null && orgaosSelecionados.length > 0 && !allOrgaos ){
			c.createAlias("orgao", "orgao");
			c.add( Restrictions.in("orgao.id", orgaosSelecionados ) );
		}else if( allOrgaos ){
			c.createAlias("orgao", "orgao");
		}

		// Busca somente propostas ativas
		c.add( Restrictions.isNull("dataCancelamento") );
		c.add( Restrictions.isNotNull("dataAprovacao") );
		
		c.addOrder( Order.asc("id") );
		
		return  c.list() ;
	}

	
}
