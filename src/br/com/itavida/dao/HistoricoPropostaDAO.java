package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.HistoricoCapital;
import br.com.itavida.entidades.HistoricoConjuge;
import br.com.itavida.entidades.HistoricoProposta;

public class HistoricoPropostaDAO {

	private Session session;
	
	public HistoricoPropostaDAO( Session session ){
		this.session = session;
	}
	
	
	public List<HistoricoProposta> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( HistoricoProposta.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		c.addOrder(Order.desc("dataAlteracao"));
		return c.list();
	}
	
	
}
