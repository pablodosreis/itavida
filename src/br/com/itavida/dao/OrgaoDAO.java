package br.com.itavida.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.dto.RelatorioOrgaoDTO;
import br.com.itavida.entidades.Estado;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.OrgaoQuitacao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoProposta;

public class OrgaoDAO {

	private Session session;
	
	public OrgaoDAO( Session session ){
		this.session = session;
	}
	
	public Integer countOrgaosPorTipo(String tipo){
		Criteria c = session.createCriteria(Orgao.class);
		c.add( Restrictions.ilike( "tipoOrgao", tipo, MatchMode.ANYWHERE ) );
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	
	public List<Orgao> buscarOrgaosVariosEstados() {
		String query = "from Orgao where cidade.cod_estado.cod_estado not in (:listEstado) order by nomeOrgao "; 
		Query q = session.createQuery(query);
		q.setParameterList("listEstado", new Integer[]{1,3,4,8,13,22,23});
		return q.list();
	}
	
	public List<Orgao> buscarPorEstado(Integer codigoEstado) {
		String query = "from Orgao where cidade.cod_estado.cod_estado = :codEstado order by nomeOrgao "; 
		Query q = session.createQuery(query);
		q.setParameter("codEstado", codigoEstado);
		return q.list();
	}
	
	public List<Orgao> buscaPorFiltro( Orgao orgaoFiltro ){
		Criteria c = this.session.createCriteria( Orgao.class );
		
		if (!StringUtils.isEmpty(orgaoFiltro.getNomeOrgao())) {
			c.add( Restrictions.ilike("nomeOrgao", orgaoFiltro.getNomeOrgao(), MatchMode.ANYWHERE ) );
		}
		if (!StringUtils.isEmpty(orgaoFiltro.getTipoOrgao())) {
			c.add( Restrictions.ilike( "tipoOrgao", orgaoFiltro.getTipoOrgao(), MatchMode.ANYWHERE ) );
		}
		
		if( orgaoFiltro.getCidade() != null && orgaoFiltro.getCidade().getId() != null ){
			c.createAlias("cidade", "cidade");
			c.add( Restrictions.eq("cidade.id", orgaoFiltro.getCidade().getId() ) );
		}	
		
		if( orgaoFiltro.getValorProlabore() != null && orgaoFiltro.getValorProlabore().doubleValue() != 0 ){
			c.add( Restrictions.eq("valorProlabore", orgaoFiltro.getValorProlabore() ) );	
		}
		if( orgaoFiltro.getValorTaxa() != null && orgaoFiltro.getValorProlabore().doubleValue() != 0 ){
			c.add( Restrictions.eq("valorTaxa", orgaoFiltro.getValorTaxa() ) );		
		}
	
		c.addOrder( Order.asc("nomeOrgao") );
		
		return c.list();
	}
	
	
	public List<Orgao> findOrgaoPopup(String nomeOrgao) {

		Criteria c = this.session.createCriteria( Orgao.class );
		
		c.add( Restrictions.ilike("nomeOrgao", nomeOrgao, MatchMode.ANYWHERE ) );

		List<Orgao> result = c.list();
								 
		return result;
	}
	
	public List<Orgao> findAllOrgaos() {

		Criteria c = this.session.createCriteria( Orgao.class );
		c.addOrder( Order.asc("nomeOrgao") );
		List<Orgao> result = c.list();
								 
		return result;
	}
	
	public Integer obterQuantidadeVidasAtivas( Integer idOrgao ){

			Criteria c = this.session.createCriteria( Proposta.class );
			c.createAlias("orgao", "orgao" );
			c.add( Restrictions.eq("orgao.id", idOrgao ) );
			c.add( Restrictions.isNull( "dataCancelamento" ) );
			c.add( Restrictions.isNotNull( "dataAprovacao" ) );
			c.setProjection( Projections.countDistinct( "id" ) );
		
		return  (Integer) c.uniqueResult() ;
	}
	
	public List<RelatorioOrgaoDTO> getOrgaosDto(Integer codUf, String nomeOrgao) {
		List<RelatorioOrgaoDTO> retorno = new ArrayList<RelatorioOrgaoDTO>();
		String query = "select o.cidade.cod_estado.sgl_estado "
				+ " ,o.nomeOrgao "
				+ " ,o.endereco "
				+ " ,sum(case when p.situacao = :situacaoAtiva then 1 else 0 end) "
				+ " ,sum(case when p.situacao = :situacaoPendente then 1 else 0 end) "
				+ " ,sum(case when p.situacao in (:situacaoAtiva, :situacaoPendente) then 1 else 0 end) "
				+ " ,sum(case when p.situacao in (:situacaoAtiva, :situacaoPendente) then (ifnull(p.valorPremioSegurado,0) + ifnull(p.valorPremioConjuge,0)) else 0 end) "
				+ " ,sum(case when p.situacao = :situacaoInadimplente then 1 else 0 end) "
				+ " from Orgao o , Proposta p"
				+ " where (o.cidade.cod_estado.cod_estado = :codUf or :codUf is null)"
				+ "   and o.nomeOrgao LIKE :nomeOrgao "
				+ "   and o.id = p.orgao.id "
				+ " group by o.cidade.cod_estado.sgl_estado, o.nomeOrgao, o.endereco "
				+ " order by o.cidade.cod_estado.sgl_estado, o.nomeOrgao "; 
		Query q = session.createQuery(query);
		q.setParameter("codUf", codUf);
		q.setParameter("situacaoAtiva", SituacaoProposta.ATIVA);
		q.setParameter("situacaoPendente", SituacaoProposta.PENDENTE);
		q.setParameter("situacaoInadimplente", SituacaoProposta.INADIMPLENTE);
		q.setParameter("nomeOrgao", "%" + nomeOrgao + "%");
		for (Object objeto : q.list()) {
			Object[] item = (Object[]) objeto;
			RelatorioOrgaoDTO dto = new RelatorioOrgaoDTO();
			dto.setUf(item[0].toString());
			dto.setNome(item[1].toString());
			dto.setEndereco(item[2].toString());
			dto.setVidasAtivas((Long) item[3]);
			dto.setVidasPendentes((Long) item[4]);
			dto.setTotalVidas((Long) item[5]);
			dto.setTotalPremios((Double) item[6]);
			dto.setVidasInadimplentes((Long) item[7]);
			retorno.add(dto);
		}
		
		return retorno;
	}
	
}
