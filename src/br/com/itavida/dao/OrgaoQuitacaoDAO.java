package br.com.itavida.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.OrgaoQuitacao;

public class OrgaoQuitacaoDAO {
	
private Session session;
	
	public OrgaoQuitacaoDAO( Session session ){
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	public List<OrgaoQuitacao> retornaOrgaoQuitacao(Integer idOrgao, Date periodoInicial, Date periodoFinal, Boolean apenasQuitados){
		Criteria criteria = session.createCriteria(OrgaoQuitacao.class);
		if(idOrgao != null){
			criteria.add(Restrictions.eq("orgao.id", idOrgao));
		}
		if(periodoInicial != null && periodoFinal != null){
			criteria.add(Restrictions.between("dataQuitacao", periodoInicial, periodoFinal));
		}
		if(apenasQuitados != null){
			criteria.add(Restrictions.eq("quitado", apenasQuitados));
		}
		return criteria.list();
	}
	
	/**
	 * Precisa receber uma lista de �rgaos porque caso algum �rg�o n�o esteja na tabela de quitacao, ele precisa ser inserido na lista resultante
	 * mesmo assim
	 * @param orgaos
	 * @param dataQuitacao
	 * @return
	 */
	public List<OrgaoQuitacao> retornaRegistrosOrgaoQuitacao(List<Orgao> orgaos, Date dataQuitacao){
		List<OrgaoQuitacao> listaOrgaoQuitacao = new ArrayList<OrgaoQuitacao>();
		for(Orgao orgao : orgaos){
			List<OrgaoQuitacao> listaResultado = buscaRegistrosOrgaoQuitacao(dataQuitacao, orgao);
			if(listaResultado.isEmpty()){
				OrgaoQuitacao orgaoNovo = constroiNovoOrgaoQuitacao(dataQuitacao, orgao);
				listaOrgaoQuitacao.add(orgaoNovo);
			}else{
				listaOrgaoQuitacao.addAll(listaResultado);
			}
		}
		return listaOrgaoQuitacao;

		
	}

	private OrgaoQuitacao constroiNovoOrgaoQuitacao(Date dataQuitacao,Orgao orgao) {
		OrgaoQuitacao orgaoNovo = new OrgaoQuitacao();
		orgaoNovo.setQuitado(false);
		
		orgaoNovo.setDataQuitacao(dataQuitacao);
		orgaoNovo.setOrgao(orgao);
		
		return orgaoNovo;
	}

	@SuppressWarnings("unchecked")
	private List<OrgaoQuitacao> buscaRegistrosOrgaoQuitacao(Date dataQuitacao,
			Orgao orgao) {
		Criteria criteria = session.createCriteria(OrgaoQuitacao.class);
		criteria.add(Restrictions.eq("orgao", orgao));
		if(dataQuitacao != null){
			criteria.add(Restrictions.eq("dataQuitacao", dataQuitacao));
		}
		List<OrgaoQuitacao> listaResultado = (List<OrgaoQuitacao>) criteria.list();
		return listaResultado;
	}

	public void salvaTodos(ArrayList<OrgaoQuitacao> listaOrgaoQuitacao) {
		session.beginTransaction();
		for(OrgaoQuitacao orgaoQuitacao : listaOrgaoQuitacao){			
			session.saveOrUpdate(orgaoQuitacao);
		}
		session.flush();
	}
	
	
	/**
	 * Metodo busca o que tem a data de n�o quitacao mais antiga.
	 * @param orQuit - Objeto orgao quitacao a ser verificado
	 * @return Objeto com a data de n�o quita��o mais antiga.
	 */
	public OrgaoQuitacao getPrimeiraCompetenciaNaoQuitada (OrgaoQuitacao orQuit){
		Criteria criteria = session.createCriteria(OrgaoQuitacao.class);
		criteria.add(Restrictions.eq("orgao", orQuit.getOrgao()));
		criteria.add(Restrictions.eq("quitado", false));
		criteria.addOrder(Order.asc("dataQuitacao"));
		session.flush();
		return (OrgaoQuitacao) criteria.list().get(0);	
	}
	
		
	/**
	 * Metodo busca por orgaos que estejam sem quitacao a dois meses
	 * @param orQuit - Objeto orgao quitacao a ser verificado
	 * @return List com os orgao que tenham duas quitacoes em aberto ou caso tenho menos de duas null.
	 */
	public List<OrgaoQuitacao> validaNaoQuitacaoDoisMeses (OrgaoQuitacao orQuit){
		Criteria criteria = session.createCriteria(OrgaoQuitacao.class);
		criteria.add(Restrictions.eq("orgao", orQuit.getOrgao()));
		criteria.add(Restrictions.eq("quitado", false));
		List<OrgaoQuitacao> listaResultado = (List<OrgaoQuitacao>) criteria.list();
		if(listaResultado.size() < 2){
			listaResultado = null;
		}
		session.flush();
		return listaResultado;
	}

	public void excluiDadosPorData(Date dataAExcluir) {
		session.beginTransaction();
		String sql = "delete from orgao_quitacao where data_quitacao = :dataQuitacao ";
		SQLQuery query = session.createSQLQuery(sql);
		query.setParameter("dataQuitacao", dataAExcluir);
		query.executeUpdate();
		session.flush();
	}
	
	
	public OrgaoQuitacao buscaOrgQuitacaoPorData(Orgao orgao, Date dataBusc) {
		Criteria criteria = session.createCriteria(OrgaoQuitacao.class);
		criteria.add(Restrictions.eq("orgao", orgao));		
		criteria.add(Restrictions.eq("dataQuitacao", dataBusc));
		criteria.setMaxResults(1);
		return (OrgaoQuitacao) criteria.uniqueResult();
		
	}
	
	public OrgaoQuitacao getUltimoPagamento(Orgao orgao) {
		Criteria criteria = session.createCriteria(OrgaoQuitacao.class);
		criteria.add(Restrictions.eq("orgao", orgao));
		criteria.add(Restrictions.eq("quitado", true));
		criteria.addOrder(Order.desc("dataQuitacao"));
		criteria.setMaxResults(1);
		return (OrgaoQuitacao) criteria.uniqueResult();
	}
}
