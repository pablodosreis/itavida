package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.ContatoOrgao;
import br.com.itavida.entidades.DetalhePagamento;

public class DetalhePagamentoDAO {

	private Session session;
	
	public DetalhePagamentoDAO( Session session ){
		this.session = session;
	}
	
	
	public List<DetalhePagamento> buscaPorPagamento( Integer idPagamento ){
		Criteria c = this.session.createCriteria( DetalhePagamento.class );
		c.createAlias("pagamento", "pagamento");
		c.add( Restrictions.eq("pagamento.id", idPagamento ) );
		return c.list();
	}
	
	
}
