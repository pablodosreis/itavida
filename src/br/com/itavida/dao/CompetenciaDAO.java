package br.com.itavida.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;

import br.com.itavida.dao.base.BaseDAO;
import br.com.itavida.dto.PagamentoMensalDTO;
import br.com.itavida.entidades.Competencia;
import br.com.itavida.enums.SituacaoProposta;

public class CompetenciaDAO extends BaseDAO {

	public void salvar(List<Competencia> bufferCompetencias) {
		getSession().beginTransaction();
		
		//Apaga os registros antigos para evitar duplicidade de dados
		if(!bufferCompetencias.isEmpty()){
			apagaCompetenciasMesAno(bufferCompetencias.get(0).getMes(), bufferCompetencias.get(0).getAno());
		}
		
		for (Competencia comp : bufferCompetencias) {
			getSession().saveOrUpdate(comp);
		}
		getSession().flush();
	}
	
	private void apagaCompetenciasMesAno(int mes, int ano){
		String hql = "delete from Competencia where mes= :mes and ano = :ano ";
		Query query = getSession().createQuery(hql);
		query.setParameter("mes", mes);
		query.setParameter("ano", ano);
		query.executeUpdate();
	}

	public List<PagamentoMensalDTO> buscaCompetenciasPorPagamentoMensal(
			String filtroMes, String filtroAno,
			String cpfSegurado, Integer empresaId, String nomeSegurado, List<SituacaoProposta> situacaoPropostaSelectedList) {
		String sql = "select ";
		sql += " o.nomeOrgao, "; // linha 0
		sql += " p.nomeSegurado, ";
		sql += " p.dataNascimentoSegurado, ";
		sql += " p.cpfSegurado, ";
		sql += " tp.descricao, ";
		sql += " p.nomeConjuge, "; //linha 5
		sql += " p.dataNascimentoConjuge, ";
		sql += " p.cpfConjuge, ";
		sql += " p.valorCapitalSegurado, ";
		sql += " p.valorCapitalConjuge, ";
		sql += " c.valor_segurado, "; //linha 10
		sql += " c.valor_conjugue, ";
		sql += " c.mes, ";
		sql += " c.ano, ";
		sql += " p.enderecoSegurado, ";
		sql += " concat(cid.nom_cidade, '/', e.sgl_estado), ";
		sql += " p.cep, ";
		sql += " IF(c.id_competencia IS NULL,'Inadimplente', 'Quitado') ";
		sql += " from proposta p";
		sql += " left join orgao o on o.id = p.orgao ";
		sql += " left join tipoproposta tp on tp.id = p.tipoProposta ";
		sql += " left join competencia c on c.proposta_id = p.id and c.mes = " + filtroMes + " and c.ano = " + filtroAno;
		sql += " left join cidade cid on p.cidade = cid.id ";
		sql += " left join estado e on cid.cod_estado = e.cod_estado ";
		sql += " where 1 = 1 ";

//		if (filtroMesAnoInicio != null && !filtroMesAnoInicio.isEmpty()) {
//			double mes = Double.parseDouble(filtroMesAnoInicio.split("/")[0]);
//			double ano = Double.parseDouble(filtroMesAnoInicio.split("/")[1]);
//			sql += " and c.mes >= " + mes + " and c.ano >= " + ano + " ";
//		}

//		if (filtroMesAnoFim != null && !filtroMesAnoInicio.isEmpty()) {
//			double mes = Double.parseDouble(filtroMesAnoFim.split("/")[0]);
//			double ano = Double.parseDouble(filtroMesAnoFim.split("/")[1]);
//			sql += " and c.mes <= " + mes + " and c.ano <= " + ano + " ";
//		}

		if (cpfSegurado != null && !"".equals(cpfSegurado.trim())) {
			sql += " and p.cpfSegurado = '" + cpfSegurado + "' ";
		}

		if (empresaId != null) {
			sql += " and p.empresa = " + empresaId + " ";
		}

		if (nomeSegurado != null && !"".equals(nomeSegurado.trim())) {
			sql += " and p.nomeSegurado = '" + nomeSegurado + "' ";
		}
		
		if(situacaoPropostaSelectedList != null && !situacaoPropostaSelectedList.isEmpty()) {
			sql += " and p.situacao IN (";
			for (SituacaoProposta s : situacaoPropostaSelectedList) {
				sql += ("'" + s.toString() + "', ");
			}
			sql = sql.substring(0, sql.length() - 2);
			sql += ")";
		}

		SQLQuery query = getSession().createSQLQuery(sql);
		List<Object[]> result = query.list();

		return transformaListaResultado(result);

	}

	private List<PagamentoMensalDTO> transformaListaResultado(
			List<Object[]> result) {

		List<PagamentoMensalDTO> list = new ArrayList<PagamentoMensalDTO>();

		for (Object[] obj : result) {
			PagamentoMensalDTO pgt = new PagamentoMensalDTO();
			pgt.setOrgao(obj[0].toString());
			pgt.setNomeSegurado(obj[1].toString());
			pgt.setDataNascimento(getDataPorExtenso((Date)obj[2]));
			pgt.setCpfSegurado(obj[3].toString());
			pgt.setTipoProposta(obj[4].toString());
			pgt.setNomeConjugue(obj[5] != null? obj[5].toString():"-");
			pgt.setNascimentoConjugue(getDataPorExtenso((Date)obj[6]));
			pgt.setCpfConjugue(obj[7] != null? obj[7].toString():"-");
			pgt.setCapitalSegurado( (Double)obj[8]);
			pgt.setCapitalConjugue( (Double)obj[9]);
			pgt.setValorFuncionario((Double)obj[10]);
			pgt.setValorConjugue((Double)obj[11]);
			pgt.setMes((Integer)obj[12]);
			pgt.setAno((Integer)obj[13]);
			pgt.setEnderecoSegurado(obj[14].toString());
			pgt.setCidadeEstadoSegurado(obj[15].toString());
			pgt.setCep(obj[16].toString());
			pgt.setQuitacao(obj[17].toString());
			list.add(pgt);
		}
		return list;
	}
	
	public static String getDataPorExtenso(Date data){ 
		if(data == null){
			return "";
		}
	       return new SimpleDateFormat("dd/MM/yyyy").format( data );  
	} 

}
