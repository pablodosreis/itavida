package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Funcionario;

public class FuncionarioDAO {

	private Session session;
	
	public FuncionarioDAO( Session session ){
		this.session = session;
	}
	
	
	public List<Funcionario> buscaPorFiltro( Funcionario funcionarioFiltro ){
		Criteria c = this.session.createCriteria( Funcionario.class );

		c.add( Restrictions.ilike("nome", funcionarioFiltro.getNome(), MatchMode.ANYWHERE ) );
		c.add( Restrictions.ilike( "tipoFuncionario", funcionarioFiltro.getTipoFuncionario(), MatchMode.ANYWHERE ) );
		if( funcionarioFiltro.getCidade() != null && funcionarioFiltro.getCidade().getId() != null){
			c.createAlias("cidade", "cidade");
			c.add( Restrictions.eq("cidade.id", funcionarioFiltro.getCidade().getId() ) );
		}	
		
		if( funcionarioFiltro.getDataAdmissao() != null ){
			c.add( Restrictions.eq("dataAdmissao", funcionarioFiltro.getDataAdmissao() ) );	
		}
		if( funcionarioFiltro.getDataDesligamento() != null ){
			c.add( Restrictions.eq("dataDesligamento", funcionarioFiltro.getDataDesligamento() ) );		
		}
		
		c.addOrder( Order.asc("nome") );
		
		return c.list();
	}
	
	public List<Funcionario> findFuncionarioPopup(String nome) {

		Criteria c = this.session.createCriteria( Funcionario.class );

		c.add( Restrictions.ilike("nome", nome, MatchMode.ANYWHERE ) );
		c.addOrder( Order.asc("nome") );
		
		return c.list();
	}
	
	
}
