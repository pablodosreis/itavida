package br.com.itavida.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.CapitalSeguradoFaixaEtaria;
import br.com.itavida.entidades.Fator;
import br.com.itavida.entidades.NomeTabela;

public class FatorDAO {

	private Session session;
	
	public FatorDAO( Session session ){
		this.session = session;
	}
	
	public Fator buscarPorDataInicio( Date dataInicio, CapitalSeguradoFaixaEtaria capitalSegurado){
		Criteria c = this.session.createCriteria( Fator.class );
		c.add( Restrictions.eq("dataInicio", dataInicio ) );
		c.add( Restrictions.eq("capitalSeguradoFaixaEtaria", capitalSegurado ) );
		return (Fator) c.uniqueResult();
	}
	
	public List<Fator> buscarPorDataInicioListagem ( Date dataInicio, CapitalSeguradoFaixaEtaria capitalSegurado, NomeTabela nomeTabela){
		Criteria c = this.session.createCriteria( Fator.class );
		if (dataInicio != null)
			c.add( Restrictions.eq("dataInicio", dataInicio ) );
		if (capitalSegurado != null) {
			c.add( Restrictions.eq("capitalSeguradoFaixaEtaria", capitalSegurado ) );
		} else if (nomeTabela != null && nomeTabela.getId() != null) {
			c.createAlias("capitalSeguradoFaixaEtaria", "capitalSeguradoFaixaEtaria");
			c.createAlias("capitalSeguradoFaixaEtaria.nomeTabela", "nomeTabela");
			c.add(Restrictions.eq("nomeTabela.id", nomeTabela.getId()));
		}
		return (List<Fator>) c.list();
	}
}
