package br.com.itavida.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.StringType;

import br.com.itavida.entidades.ComportamentoPropostaEnum;
import br.com.itavida.entidades.DetalheCapitalFaixaEtaria;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.Funcionario;
import br.com.itavida.entidades.HistoricoProposta;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.util.Constantes;
import br.com.itavida.util.DateUtil;

public class PropostaDAO {

	private static final int MOTIVO_FORA_DE_FOLHA = 12;
	private Session session;
	
	public PropostaDAO( Session session ){
		this.session = session;
	}
	
//	public void atualizarSituacao(Proposta proposta) {
//		if(proposta.getDataCancelamento() != null) {
//			if(proposta.getMotivoCancelamento() != null && proposta.getMotivoCancelamento().getId() == MOTIVO_FORA_DE_FOLHA) {
//				
//			}
//		}
//	}
	
	public void update(Proposta proposta) {
		session.update(proposta);
	}
	
	public int atualizarSituacao() {
		
		int rowsAffected = 0;
		
		String sqlQueryAtiva = "update proposta set situacao = :situacaoproposta where dataCancelamento is null and dataAprovacao is not null and situacao not in (:listasituacaoproposta)";

		//Alterado de 2 para 5 a quantidade meses m�xima que uma proposta pode ficar como inadimplente
		//2019-07-04 Alterado de 5 para 3 a quantidade de meses que uma proposta pode ficar inadimplente
		String sqlQueryInadimplente = "update proposta set situacao = :situacaoproposta where situacao <> :situacaoproposta and motivoCancelamento = 12 and dataCancelamento is not null and TIMESTAMPDIFF(MONTH, dataCancelamento, curdate()) <= 3 ";		//
		String sqlQueryCancelada = "update proposta set situacao = :situacaoproposta where situacao <> :situacaoproposta and dataCancelamento is not null and (motivoCancelamento <> 12 or motivoCancelamento is null or (motivoCancelamento = 12 and TIMESTAMPDIFF(MONTH, dataCancelamento, curdate()) > 3)) ";
		
		/*Aguardando averba��o s�o as propostas com menos de 4 meses da data de cadastro e que n�o possuem data de aprova��o*/
		String sqlQueryAguardandoAverbacao = "update proposta set situacao = :situacaoproposta where situacao <> :situacaoproposta and dataCancelamento is null and dataAprovacao is null and TIMESTAMPDIFF(MONTH, dataCadastro, curdate()) <= 3";
		
		/*Consultar para obter as propostas n�o averbadaso, s�o as propostas cadastradas a mais de 4 meses e que n�o possuem data de aprova��o e n�o foram canceladas*/
		String sqlQueryNaoAverbada = "update proposta set situacao = :situacaoproposta where situacao <> :situacaoproposta and dataCancelamento is null and dataAprovacao is null and TIMESTAMPDIFF(MONTH, dataCadastro, curdate()) >= 4";
		
		Query queryAtiva = this.session.createSQLQuery(sqlQueryAtiva);
		queryAtiva.setParameter("situacaoproposta", SituacaoProposta.ATIVA.toString());
		queryAtiva.setParameterList("listasituacaoproposta", new String[] {SituacaoProposta.ATIVA.toString(),SituacaoProposta.PENDENTE.toString()});
		rowsAffected += queryAtiva.executeUpdate();
		
		Query queryInadimplente = this.session.createSQLQuery(sqlQueryInadimplente);
		queryInadimplente.setParameter("situacaoproposta", SituacaoProposta.INADIMPLENTE.toString());
		rowsAffected += queryInadimplente.executeUpdate();
		
		Query queryCancelada = this.session.createSQLQuery(sqlQueryCancelada);
		queryCancelada.setParameter("situacaoproposta", SituacaoProposta.CANCELADA.toString());
		rowsAffected += queryCancelada.executeUpdate();
		
		Query queryAguardandoAverbacao = this.session.createSQLQuery(sqlQueryAguardandoAverbacao);
		queryAguardandoAverbacao.setParameter("situacaoproposta", SituacaoProposta.AGUARDANDO_AVERBACAO.toString());
		rowsAffected += queryAguardandoAverbacao.executeUpdate();
		
		Query queryNaoAverbada = this.session.createSQLQuery(sqlQueryNaoAverbada);
		queryNaoAverbada.setParameter("situacaoproposta", SituacaoProposta.NAO_AVERBADO.toString());
		rowsAffected += queryNaoAverbada.executeUpdate();
		
		
		return rowsAffected;
	}
	
	public Map<String,List<Proposta>> buscarPorEmpresa(List<SituacaoProposta> situacoes, Integer empresa, Integer uf, Integer orgao, Integer agenciador, Date dataAverbacao, Integer modeloProposta, Date dataCancelamentoInicio, Date dataCancelamentoFim, String tipoOrgao, String profissao, Integer mesAniversario) {
		
		List<String> empresas = null;
		if(empresa != null) {
			empresas = new ArrayList<String>();
			empresas.add(empresa.toString());
		}
		List<Proposta> lista = buscarPropostasRelatorioSeguradosEmpresa(situacoes, empresas, uf,
				orgao, agenciador, dataAverbacao, null, null,modeloProposta, false,
				dataCancelamentoInicio, dataCancelamentoFim, tipoOrgao, profissao, mesAniversario);
		populaCapital(lista);
		Map<String,List<Proposta>> mapa = new HashMap<String, List<Proposta>>();
		for(Proposta proposta : lista) {
			List<Proposta> propostas = mapa.get(proposta.getEmpresa().getNomeEmpresa());
			if(propostas == null) {
				propostas = new ArrayList<Proposta>();
			}
			propostas.add(proposta);
			mapa.put(proposta.getEmpresa().getNomeEmpresa(), propostas);
		}
		return mapa;
	}

	public List<Proposta> buscarPropostas(List<SituacaoProposta> situacoesProposta,
			List<String> empresas, Integer uf, Integer orgao, Integer agenciador,
			Date dataAverbacao) {
		return buscarPropostas(situacoesProposta, empresas, uf, orgao, agenciador, false, dataAverbacao, null, null, null, null, false);
	}
	
	public List<Proposta> buscarPropostasRelatorioSeguradosEmpresa(List<SituacaoProposta> situacoes,
			List<String> empresas, Integer uf, Integer orgao, Integer agenciador,
			Date dataAverbacao, Date dataAverbacaoInicio, Date dataAverbacaoFim, Integer modeloProposta, boolean comAverbacao,
			Date dataCancelamentoInicio, Date dataCancelamentoFim, String tipoOrgao, String profissao, Integer mesAniversario) {
		String query = "FROM Proposta p left join fetch p.propostaagenciador_collection pagenciador WHERE 1 = 1 ";
		
		if(situacoes != null && !situacoes.isEmpty()) {
			query += " and p.situacao IN (";
			for (SituacaoProposta s : situacoes) {
				query += ("'" + s.toString() + "', ");
			}
			query = query.substring(0, query.length() - 2);
			query += ")";
		} 
		
		if(empresas != null && !empresas.isEmpty()) {
			query += " and p.empresa.id IN (";
			for (String codigo : empresas) {
				query += ("'" + codigo + "', ");
			}
			query = query.substring(0, query.length() - 2);
			query += ")";
		}
		
		if (!StringUtils.isEmpty(profissao)) {
			query += " and p.profissaoSegurado like :profissao" ;
		}
		
		if (!StringUtils.isEmpty(tipoOrgao)) {
			query += " and p.orgao.tipoOrgao = :tipoOrgao" ;
		}
		
		if(uf != null && uf > 0) {
			query += " and p.orgao.cidade.cod_estado.id = " + uf;
		}
		
		if(uf != null && uf < 0) {
			query += " and p.orgao.cidade.cod_estado.id not in (:listEstado) ";
		}
		
		if (orgao != null && orgao > 0) {
			query += " and p.orgao.id = " + orgao;
		}
		
		if (agenciador != null && agenciador > 0) {
			query += " and pagenciador.funcionario.id = " + agenciador;
		}
		
		if (dataAverbacao != null) {
			query += " and p.dataAprovacao = :dataAprovacao";
		}
		
		if(dataAverbacaoInicio != null) {
			query += " and p.dataAprovacao >= :dataAprovacaoInicio";
		}
		
		if(dataAverbacaoFim != null) {
			query += " and p.dataAprovacao <= :dataAprovacaoFim";
		}
		
		if (comAverbacao) {
			query += " and p.dataAprovacao is not null ";
		}
		
		if(modeloProposta != null) {
			query += " and p.modeloProposta.id = :modeloProposta ";
		}
		
		
		if(dataCancelamentoInicio != null) {
			query += " and p.dataCancelamento >= :dataCancelamentoInicio";
		}
		
		if(dataCancelamentoFim != null) {
			query += " and p.dataCancelamento <= :dataCancelamentoFim";
		}
		
		if (mesAniversario != null) {
			query += " and EXTRACT(MONTH FROM p.dataNascimentoSegurado) = :mesAniversario";
		}
		
		query += " group by p.id ";
		
		query += " order by p.orgao.nomeOrgao, p.nomeSegurado ";
		
		Query q = session.createQuery(query);
		
		if (!StringUtils.isEmpty(profissao)) {
			q.setParameter("profissao",  "%" + profissao + "%");
		}
		
		if (!StringUtils.isEmpty(tipoOrgao)) {
			q.setParameter("tipoOrgao", tipoOrgao);
		}
		
		if (dataAverbacao != null) {
			q.setParameter("dataAprovacao", dataAverbacao);
		}
		
		if (uf != null && uf < 0) {
			q.setParameterList("listEstado", new Integer[]{1,3,4,8,13,22,23});
		}
		
		if(dataAverbacaoInicio != null) {
			q.setParameter("dataAprovacaoInicio", dataAverbacaoInicio);
		}
		
		if(dataAverbacaoFim != null) {
			q.setParameter("dataAprovacaoFim", dataAverbacaoFim);
		}
		
		if(modeloProposta != null) {
			q.setParameter("modeloProposta", modeloProposta);
		}
		
		if(dataCancelamentoInicio != null) {
			q.setParameter("dataCancelamentoInicio", dataCancelamentoInicio);
		}
		
		if(dataCancelamentoFim != null) {
			q.setParameter("dataCancelamentoFim", dataCancelamentoFim);
		}
		
		if (mesAniversario != null) {
			q.setParameter("mesAniversario", mesAniversario);
		}
		
		List<Proposta> lista = q.list();
		return lista;
	}
	
	public List<Proposta> buscarPropostas(List<SituacaoProposta> situacoesProposta,
			List<String> empresas, Integer uf, Integer orgao, Integer agenciador, boolean somenteComAgenciador,
			Date dataAverbacao, Date dataAverbacaoInicio, Date dataAverbacaoFim, Date dataAgenciamentoInicio, Date dataAgenciamentoFim, boolean comAverbacao) {
		String query = "FROM Proposta p left join fetch p.propostaagenciador_collection pagenciador WHERE 1 = 1 ";
		
		if(situacoesProposta != null && !situacoesProposta.isEmpty()) {
			query += " and p.situacao IN (";
			for (SituacaoProposta s : situacoesProposta) {
				query += ("'" + s.toString() + "', ");
			}
			query = query.substring(0, query.length() - 2);
			query += ")";
		}
		
		if(empresas != null && !empresas.isEmpty()) {
			query += " and p.empresa.id IN (";
			for (String codigo : empresas) {
				query += ("'" + codigo + "', ");
			}
			query = query.substring(0, query.length() - 2);
			query += ")";
		}
		
		if(uf != null && uf > 0) {
			query += " and p.orgao.cidade.cod_estado.id = " + uf;
		}
		
		if(uf != null && uf < 0) {
			query += " and p.orgao.cidade.cod_estado.id not in (:listEstado) ";
		}
		
		if (orgao != null && orgao > 0) {
			query += " and p.orgao.id = " + orgao;
		}
		
		if (agenciador != null && agenciador > 0) {
			query += " and pagenciador.funcionario.id = " + agenciador;
		}
		
		if (somenteComAgenciador) {
			query += " and pagenciador.funcionario.id is not null ";
		}
		
		if (dataAverbacao != null) {
			query += " and p.dataAprovacao = :dataAprovacao";
		}
		
		if(dataAverbacaoInicio != null) {
			query += " and p.dataAprovacao >= :dataAprovacaoInicio";
		}
		
		if(dataAverbacaoFim != null) {
			query += " and p.dataAprovacao <= :dataAprovacaoFim";
		}
		
		if(dataAgenciamentoInicio != null) {
			query += " and p.dataAgenciamento >= :dataAgenciamentoInicio";
		}
		
		if(dataAgenciamentoFim != null) {
			query += " and p.dataAgenciamento <= :dataAgenciamentoFim";
		}
		
		if (comAverbacao) {
			query += " and p.dataAprovacao is not null ";
		}
		
		query += " group by p.id ";
		
		query += " order by p.orgao.nomeOrgao, p.nomeSegurado ";
		
		Query q = session.createQuery(query);
		
//		if (situacoesProposta != null && !situacoesProposta.isEmpty()) {
//			q.setParameterList("situacaoProposta", situacoesProposta);
//		}
		
		if (dataAverbacao != null) {
			q.setParameter("dataAprovacao", dataAverbacao);
		}
		
		if (uf != null && uf < 0) {
			q.setParameterList("listEstado", new Integer[]{1,3,4,8,13,22,23});
		}
		
		if(dataAverbacaoInicio != null) {
			q.setParameter("dataAprovacaoInicio", dataAverbacaoInicio);
		}
		
		if(dataAverbacaoFim != null) {
			q.setParameter("dataAprovacaoFim", dataAverbacaoFim);
		}
		
		if(dataAgenciamentoInicio != null) {
			q.setParameter("dataAgenciamentoInicio", dataAgenciamentoInicio);
		}
		
		if(dataAgenciamentoFim != null) {
			q.setParameter("dataAgenciamentoFim", dataAgenciamentoFim);
		}
		
		List<Proposta> lista = q.list();
		return lista;
	}
	
	public List<Proposta> buscarPropostasAtivasSemNumeroSorteio(Integer tipo, Integer empresa ){
		String query = "";
		if( tipo == 1 ){
			query = "FROM Proposta p WHERE p.id not in( select n.proposta from NumeroSorteio n ) and p.dataAprovacao is not null and p.dataCancelamento is null and p.empresa.id = " + empresa + " order by p.nomeSegurado";
		}else if( tipo == 2 ){
			query = "FROM Proposta p WHERE p.id not in( select n.proposta from NumeroSorteio n ) and p.empresa.id = " + empresa + "  order by p.nomeSegurado";
		}else if( tipo == 3 ){
			query = "FROM Proposta p order by p.nomeSegurado";
		}else if( tipo == 4 ){
			query = "FROM Proposta p WHERE p.dataAprovacao is not null and p.dataCancelamento is null and p.empresa.id = " + empresa + "  order by p.nomeSegurado";
		}
		
		Query q1 = session.createQuery(query);
		
		return q1.list();
		
	}
	
	public List<Proposta> buscarPropostasAtivasPorTipoPlano(Integer tipo, Integer idEmpresa){
		
		Criteria c = session.createCriteria(Proposta.class);

		//Busca por empresa
		if( idEmpresa != null && idEmpresa != 0 ){
			c.createAlias("empresa", "empresa");
			c.add( Restrictions.eq("empresa.id", idEmpresa ) );
		}
		
		if( tipo == 1 ){
			
			c.createAlias("tipoProposta", "tipoProposta");
			c.add( Restrictions.in("tipoProposta.id", new Integer[]{1,4}  ) );
			c.add( Restrictions.isNotNull("dataAprovacao") );
			c.add( Restrictions.isNull("dataCancelamento") );
			
		}else if( tipo == 2 ){
			
			c.createAlias("tipoProposta", "tipoProposta");
			c.add( Restrictions.in("tipoProposta.id", new Integer[]{2,3,5}  ) );
			c.add( Restrictions.isNotNull("dataAprovacao") );
			c.add( Restrictions.isNull("dataCancelamento") );
		
		}
		
		c.createAlias("orgao", "orgao");
		c.addOrder( Order.asc("orgao.nomeOrgao") );
		c.addOrder( Order.asc("nomeSegurado") );
		
		return c.list();
		
	}
	
	public List<Proposta> findAll(){
		Criteria c = session.createCriteria(Proposta.class);
		return c.list();
		
	}
	
	public Proposta load(Integer id){
		Criteria c = session.createCriteria(Proposta.class);
		c.add(Restrictions.eq("id", id));
		return (Proposta) c.uniqueResult();
		
	}

	public Integer countPropostasAtivas(){
		Criteria c = session.createCriteria(Proposta.class);
		c.add( Restrictions.isNotNull( "dataAprovacao" ) );
		c.add( Restrictions.isNull( "dataCancelamento" ) );
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	
	public List<Proposta> obterTodasPropostasAtivas(){
		Criteria c = session.createCriteria(Proposta.class);
		c.add( Restrictions.isNotNull( "dataAprovacao" ) );
		c.add( Restrictions.isNull( "dataCancelamento" ) );
		return c.list();
	}
	
	public List<Proposta> obterTodasPropostasNaoCanceladas(){
		Criteria c = session.createCriteria(Proposta.class);
		c.add(Restrictions.ne("situacao", SituacaoProposta.CANCELADA));
		return c.list();
	}
	
	public List<Proposta> obterTodasPropostasAtivasPorEmpresa(Integer idEmpresa){
		Criteria c = session.createCriteria(Proposta.class);
		
		c.createAlias("empresa", "empresa");
		c.add( Restrictions.eq("empresa.id", idEmpresa ) );
		c.add( Restrictions.isNotNull( "dataAprovacao" ) );
		c.add( Restrictions.isNull( "dataCancelamento" ) );
		return c.list();
	}
	
	public List<Proposta> findPropostaPopup(String nome) {
		Criteria c = this.session.createCriteria( Proposta.class );
		c.add( Restrictions.ilike("nomeSegurado", nome, MatchMode.ANYWHERE ) );
		c.addOrder( Order.asc("nomeSegurado") );
		c.setMaxResults(20);
		return c.list();
	}
	
	public List<Proposta> findPropostaSuperAp(String nome) {
		Criteria c = this.session.createCriteria( Proposta.class );
		c.add( Restrictions.ilike("nomeSegurado", nome, MatchMode.ANYWHERE ) );
		c.add( Restrictions.isNotNull("superAp"));
		c.addOrder( Order.asc("nomeSegurado") );
		c.setMaxResults(20);
		return c.list();
	}
	
	
	
	
	public Integer countPropostasPorEmpresa (Integer idEmpresa){
		Criteria c = session.createCriteria(Proposta.class);
		c.createAlias("empresa", "empresa");
		c.add( Restrictions.eq("empresa.id", idEmpresa ) );
		c.setProjection(Projections.rowCount());
		return (Integer) c.uniqueResult();
	}
	
	public Integer countPropostasCanceladas(){
		Criteria c = session.createCriteria(Proposta.class);
		c.add( Restrictions.isNotNull( "dataCancelamento" ) );
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	
	public Integer countPropostasSemAverbacao(){
		Criteria c = session.createCriteria(Proposta.class);
		c.add( Restrictions.isNull( "dataAprovacao" ) );
		c.add( Restrictions.isNull( "dataCancelamento" ) );
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	public Integer countPropostasPorSexo( String sexo ){
		Criteria c = session.createCriteria(Proposta.class);
		c.add( Restrictions.ilike("sexoSegurado", sexo, MatchMode.ANYWHERE ) );	
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	
	public Integer countPropostasPlanoConjugado(){
		Criteria c = session.createCriteria(Proposta.class);
		c.createAlias("tipoProposta", "tipoProposta");
		c.add( Restrictions.in("tipoProposta.id", new Integer[]{2,3,5}  ) );
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	public Integer countPropostasPlanoIndividual(){
		Criteria c = session.createCriteria(Proposta.class);
		c.createAlias("tipoProposta", "tipoProposta");
		c.add( Restrictions.in("tipoProposta.id", new Integer[]{1,4}  ) );
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	
	public Integer countPropostasCPFDuplicidade(Proposta propostaFiltro){
		Criteria c = session.createCriteria(Proposta.class);
		
			// Busca por CPF Segurado Principal
			Criterion cpfSeguradoCriterion = Restrictions.eq("cpfSegurado", propostaFiltro.getCpfSegurado() );
			Criterion cpfSeguradoFormatadoCriterion = Restrictions.eq("cpfSegurado", propostaFiltro.getCpfSeguradoFormatado() );
			Criterion orCpfSegurado = Restrictions.or(cpfSeguradoCriterion,cpfSeguradoFormatadoCriterion );
			
			//Adiciona o crit�rio
			c.add( orCpfSegurado );
			c.add( Restrictions.ne("situacao", SituacaoProposta.CANCELADA));
			c.add( Restrictions.isNull("superAp"));
			
			if(propostaFiltro.getId() != null) {
				c.add( Restrictions.ne("id", propostaFiltro.getId()));
			}
		
		c.setProjection( Projections.countDistinct( "id" ) );
		return (Integer) c.uniqueResult();
	}
	
	public List<Proposta> buscaPorFiltro( Proposta propostaFiltro, Integer tipoRelatorio ){
		Criteria c = this.session.createCriteria( Proposta.class );
		
		//Busca pelo Id
		if( propostaFiltro.getId() != null && !"".equals( propostaFiltro.getId() ) && propostaFiltro.getId() != 0 ){
			c.add( Restrictions.eq("id", propostaFiltro.getId() ) );
		}
		
		//Busca por empresa
		if( propostaFiltro.getEmpresa() != null && propostaFiltro.getEmpresa().getId() != null && propostaFiltro.getEmpresa().getId() != 0 ){
			c.createAlias("empresa", "empresa");
			c.add( Restrictions.eq("empresa.id", propostaFiltro.getEmpresa().getId() ) );
		}
		
		// Busca por Modelo de Proposta
		if( propostaFiltro.getModeloProposta() != null && propostaFiltro.getModeloProposta().getId() != null && propostaFiltro.getModeloProposta().getId() != 0){
			c.createAlias("modeloProposta", "modeloProposta");
			c.add( Restrictions.eq("modeloProposta.id", propostaFiltro.getModeloProposta().getId() ) );
		}
		
		// Busca por Tipo de Proposta
		if( propostaFiltro.getTipoProposta() != null && propostaFiltro.getTipoProposta().getId() != null && propostaFiltro.getTipoProposta().getId() != 0){
			c.createAlias("tipoProposta", "tipoProposta");
			c.add( Restrictions.eq("tipoProposta.id", propostaFiltro.getTipoProposta().getId() ) );
		}
		
		// Busca por Org�o
		if( propostaFiltro.getOrgao() != null && propostaFiltro.getOrgao().getId() != null && propostaFiltro.getOrgao().getId() != 0){
			c.createAlias("orgao", "orgao");
			c.add( Restrictions.eq("orgao.id", propostaFiltro.getOrgao().getId() ) );
		}	
		
		// Busca por Tipo de Org�o
		if( propostaFiltro.getOrgao() != null && propostaFiltro.getOrgao().getTipoOrgao() != null && !"".equals(propostaFiltro.getOrgao().getTipoOrgao())){
			c.createAlias("orgao", "orgao");
			c.add( Restrictions.eq("orgao.tipoOrgao", propostaFiltro.getOrgao().getTipoOrgao() ) );
		}
		
		// Busca por Nome da Tabela
		if( propostaFiltro.getNomeTabela() != null && propostaFiltro.getNomeTabela().getId() != null && propostaFiltro.getNomeTabela().getId() != 0){
			c.createAlias("nomeTabela", "nomeTabela");
			c.add( Restrictions.eq("nomeTabela.id", propostaFiltro.getNomeTabela().getId() ) );			
		}
		
		// Busca por Nome do Segurado Principal
		if( !StringUtils.isEmpty( propostaFiltro.getNomeSegurado() ) ){
			c.add( Restrictions.ilike("nomeSegurado", propostaFiltro.getNomeSegurado(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por Sexo do Segurado Principal
		if( !StringUtils.isEmpty( propostaFiltro.getSexoSegurado() ) ){
			c.add( Restrictions.ilike("sexoSegurado", propostaFiltro.getSexoSegurado(), MatchMode.ANYWHERE ) );	
		}
		
		// Busca por Data Nascimento Segurado Principal
		if( propostaFiltro.getDataNascimentoSeguradoInicio() != null ){
			c.add( Restrictions.ge("dataNascimentoSegurado", propostaFiltro.getDataNascimentoSeguradoInicio() ) );		
		}	
		
		if( propostaFiltro.getDataNascimentoSeguradoFim() != null ){
			c.add( Restrictions.le("dataNascimentoSegurado", propostaFiltro.getDataNascimentoSeguradoFim() ) );		
		}
		
		// Busca por Matricula Segurado Principal
		if( propostaFiltro.getMatriculaSegurado() != null ){
			c.add( Restrictions.ilike("matriculaSegurado", propostaFiltro.getMatriculaSegurado(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por CPF Segurado Principal
		if( !StringUtils.isEmpty( propostaFiltro.getCpfSegurado() ) ){
			c.add( Restrictions.eq("cpfSegurado", propostaFiltro.getCpfSegurado() ) );
		}
		
		// Busca por RG Segurado Principal
		if( !StringUtils.isEmpty( propostaFiltro.getRgSegurado() ) ){
			c.add( Restrictions.ilike("rgSegurado", propostaFiltro.getRgSegurado(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por Tabela de Capital
		if( propostaFiltro.getModeloProposta()!=null && Constantes.MODELO_PROPOSTA_VALOR_FIXO.equals( propostaFiltro.getModeloProposta().getId() ) ){
			c.createAlias("capitalSeguradoLimiteIdade", "capitalSeguradoLimiteIdade");
			c.add( Restrictions.eq("capitalSeguradoLimiteIdade.id", propostaFiltro.getCapitalSeguradoLimiteIdade().getId() ) );
		}else if( propostaFiltro.getModeloProposta() != null && Constantes.MODELO_PROPOSTA_FAIXA_ETARIA.equals( propostaFiltro.getModeloProposta().getId() ) ){
			c.createAlias("capitalSeguradoFaixaEtaria", "capitalSeguradoFaixaEtaria");
			c.add( Restrictions.eq("capitalSeguradoFaixaEtaria.id", propostaFiltro.getCapitalSeguradoFaixaEtaria().getId() ) );			
		}
		
		// Busca por Nome do Segurado Conjuge
		if( !StringUtils.isEmpty( propostaFiltro.getNomeConjuge() ) ){
			c.add( Restrictions.ilike("nomeConjuge", propostaFiltro.getNomeConjuge(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por Sexo do Segurado Conjuge
		if( !StringUtils.isEmpty( propostaFiltro.getSexoConjuge() ) ){
			c.add( Restrictions.ilike("sexoConjuge", propostaFiltro.getSexoConjuge(), MatchMode.ANYWHERE ) );	
		}
		
		// Busca por Data Nascimento Segurado Conjuge
		if( propostaFiltro.getDataNascimentoConjugeInicio() != null ){
			c.add( Restrictions.ge("dataNascimentoConjuge", propostaFiltro.getDataNascimentoConjugeInicio() ) );		
		}	
		
		// Busca por Data Nascimento Segurado Conjuge
		if( propostaFiltro.getDataNascimentoConjugeFim() != null ){
			c.add( Restrictions.le("dataNascimentoConjuge", propostaFiltro.getDataNascimentoConjugeFim() ) );		
		}	
		
		// Busca por Matricula Segurado Conjuge
		if( !StringUtils.isEmpty( propostaFiltro.getMatriculaConjuge() ) ){
			c.add( Restrictions.ilike("matriculaConjuge", propostaFiltro.getMatriculaConjuge(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por CPF Segurado Conjuge
		if( !StringUtils.isEmpty( propostaFiltro.getCpfConjuge() ) ){
			c.add( Restrictions.eq("cpfConjuge", propostaFiltro.getCpfConjuge() ) );
		}
		
		// Busca por RG Segurado Conjuge
		if( !StringUtils.isEmpty( propostaFiltro.getRgConjuge() ) ){
			c.add( Restrictions.ilike("rgConjuge", propostaFiltro.getRgConjuge(), MatchMode.ANYWHERE ) );
		}
		
		
		// Busca por ENDERE�O
		if( !StringUtils.isEmpty( propostaFiltro.getEnderecoSegurado() ) ){
			c.add( Restrictions.ilike("enderecoSegurado", propostaFiltro.getEnderecoSegurado(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por COMPLEMENTO ENDERE�O
		if( !StringUtils.isEmpty( propostaFiltro.getComplemento() ) ){
			c.add( Restrictions.ilike("complemento", propostaFiltro.getComplemento(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por BAIRRO
		if( !StringUtils.isEmpty( propostaFiltro.getBairro() ) ){
			c.add( Restrictions.ilike("bairro", propostaFiltro.getBairro(), MatchMode.ANYWHERE ) );		
		}
		
		//Busca Por Cidade
		if( propostaFiltro.getCidade() != null && propostaFiltro.getCidade().getId() != null && propostaFiltro.getCidade().getId() != 0){
			c.createAlias("cidade", "cidade");
			c.add( Restrictions.eq("cidade.id", propostaFiltro.getCidade().getId() ) );
		}	
		
		// Busca por CEP
		if( !StringUtils.isEmpty( propostaFiltro.getCep() ) ){
			c.add( Restrictions.ilike("cep", propostaFiltro.getCep(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por Telefone Residencial
		if( !StringUtils.isEmpty( propostaFiltro.getTelefoneResidencial() ) ){
			c.add( Restrictions.ilike("telefoneResidencial", propostaFiltro.getTelefoneResidencial(), MatchMode.ANYWHERE ) );	
		}
		
		// Busca por Telefone Comercial
		if( !StringUtils.isEmpty( propostaFiltro.getTelefoneComercial() ) ){
			c.add( Restrictions.ilike("telefoneComercial", propostaFiltro.getTelefoneComercial(), MatchMode.ANYWHERE ) );
		}
		
		// Busca por Telefone Celular
		if( !StringUtils.isEmpty( propostaFiltro.getTelefoneCelular() ) ){
			c.add( Restrictions.ilike("telefoneCelular", propostaFiltro.getTelefoneCelular(), MatchMode.ANYWHERE ) );	
		}
		
		// Busca por Telefone Celular 2
		if( !StringUtils.isEmpty( propostaFiltro.getTelefoneCelular2() ) ){
			c.add( Restrictions.ilike("telefoneCelular2", propostaFiltro.getTelefoneCelular2(), MatchMode.ANYWHERE ) );	
		}
		
		// Busca por observacao
		if( !StringUtils.isEmpty( propostaFiltro.getObservacao() ) ){
			c.add( Restrictions.ilike("observacao", propostaFiltro.getObservacao(), MatchMode.ANYWHERE ) );
		}
		
		
		//Busca Por Agenciador
		if( propostaFiltro.getFuncionario() != null && propostaFiltro.getFuncionario().getId() != null ){
			c.createAlias("propostaagenciador_collection", "propostaagenciador");
			c.add( Restrictions.eq("propostaagenciador.funcionario.id", propostaFiltro.getFuncionario().getId() ) );
		}	
		
		//Busca Por Numero Sorteio
		if( !StringUtils.isEmpty( propostaFiltro.getNumeroSorteioFiltro() ) ){
			c.createAlias("numerosorteio_collection", "numeroSorteio");
			c.add( Restrictions.like("numeroSorteio.numeroSorteio", propostaFiltro.getNumeroSorteioFiltro(), MatchMode.ANYWHERE  ) );
		}	
		
		//Busca por Data de aprova��o
		if( propostaFiltro.getDataAprovacaoInicio() != null ){
			c.add( Restrictions.ge("dataAprovacao", propostaFiltro.getDataAprovacaoInicio() ) );
		}
		if( propostaFiltro.getDataAprovacaoFim() != null ){
			c.add( Restrictions.le("dataAprovacao", propostaFiltro.getDataAprovacaoFim() ) );
		}
		
		//Busca por Data de Cadastramento
		if( propostaFiltro.getDataCadastroInicio() != null ){
			c.add( Restrictions.ge("dataCadastro", propostaFiltro.getDataCadastroInicio() ) );
		}
		if( propostaFiltro.getDataCadastroFim() != null ){
			c.add( Restrictions.le("dataCadastro", propostaFiltro.getDataCadastroFim() ) );
		}	
		
		//Busca por Data de Cancelamento
		if( propostaFiltro.getDataCancelamentoInicio() != null ){
			c.add( Restrictions.ge("dataCancelamento", propostaFiltro.getDataCancelamentoInicio() ) );
		}
		if( propostaFiltro.getDataCancelamentoFim() != null ){
			c.add( Restrictions.le("dataCancelamento", propostaFiltro.getDataCancelamentoFim() ) );
		}			
		
		if( tipoRelatorio == 1 ){
			//Cancelados
			c.add( Restrictions.eq( "situacao", SituacaoProposta.CANCELADA ) );
		}else if( tipoRelatorio == 2 ){
			//Segurados sem aprova��o
			c.add( Restrictions.eq( "situacao", SituacaoProposta.AGUARDANDO_AVERBACAO ) );
		}else if( tipoRelatorio == 3 ){
			
			//Mudan�a de Plano
			c.add( Restrictions.isNull( "dataCancelamento" ) );
			c.add( Restrictions.isNotNull( "dataAprovacao" ) );
			
			try {
				c.add( Restrictions.ne("dataNascimentoSegurado",  DateUtil.getDate("1111-11-11", "yyyy-MM-dd") ) );
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return populaCapital( relatorioMudancaFaixaEtaria( c.list() ), tipoRelatorio );
		}else if( tipoRelatorio == 4 ){
			//Segurados ativos
			c.add( Restrictions.in( "situacao", new SituacaoProposta[]{SituacaoProposta.ATIVA, SituacaoProposta.PENDENTE} ) );
		}else if( tipoRelatorio == 9 ){
			//limita para tela de sinistro
			c.setMaxResults( 100 );
		}else if( tipoRelatorio == 10 ){
			//Comiss�o de altera��o
			c.add( Restrictions.sizeGt("propostaagenciador_collection", 1));
		}else if( tipoRelatorio == 11 ){
			c.add( Restrictions.eq( "situacao", SituacaoProposta.INADIMPLENTE) );
		}
		
		c.addOrder( Order.asc("id") );
		return populaCapital(c.list(), tipoRelatorio);
	}

	public List<Proposta> buscaPorFiltroConsulta( Proposta propostaFiltro, boolean filtrarAtivas){
		return buscaPorFiltroConsulta(propostaFiltro, filtrarAtivas, null);
	}
	
	public List<Proposta> buscaPorFiltroConsulta( Proposta propostaFiltro, boolean filtrarAtivas, Integer limite ){
		Criteria c = this.session.createCriteria( Proposta.class );
		
		//Busca pelo Id
		if( propostaFiltro.getId() != null && !"".equals( propostaFiltro.getId() ) && propostaFiltro.getId() != 0 ){
			c.add( Restrictions.eq("id", propostaFiltro.getId() ) );
		}
		
		//Busca por empresa
		if( propostaFiltro.getEmpresa() != null && propostaFiltro.getEmpresa().getId() != null && propostaFiltro.getEmpresa().getId() != 0 ){
			c.createAlias("empresa", "empresa");
			c.add( Restrictions.eq("empresa.id", propostaFiltro.getEmpresa().getId() ) );
		}
	
		// Busca por Org�o
		if( propostaFiltro.getOrgao() != null && propostaFiltro.getOrgao().getId() != null){
			c.createAlias("orgao", "orgao");
			c.add( Restrictions.eq("orgao.id", propostaFiltro.getOrgao().getId() ) );
		}		
		
		// Busca por Nome do Segurado Principal
		if( StringUtils.isNotEmpty(propostaFiltro.getNomeSegurado())){
			c.add( Restrictions.ilike("nomeSegurado", propostaFiltro.getNomeSegurado(), MatchMode.ANYWHERE ) );
		}
		
		if (StringUtils.isNotEmpty(propostaFiltro.getProfissaoSegurado())) {
			c.add( Restrictions.ilike("profissaoSegurado", propostaFiltro.getProfissaoSegurado(), MatchMode.ANYWHERE ) );
		}
		
		if (StringUtils.isNotEmpty(propostaFiltro.getProfissaoConjuge())) {
			c.add( Restrictions.ilike("profissaoConjuge", propostaFiltro.getProfissaoConjuge(), MatchMode.ANYWHERE ) );
		}
		
		if (filtrarAtivas) {
			// Busca somente propostas ativas
			c.add( Restrictions.isNull("dataCancelamento") );
			c.add( Restrictions.isNotNull("dataAprovacao") );
		
		}
		
		if (propostaFiltro.getMesAniversario() != null) {
			c.add(Restrictions.sqlRestriction("EXTRACT(MONTH FROM dataNascimentoSegurado) =  " +  propostaFiltro.getMesAniversario()));
		}

		
		if (propostaFiltro.getFiltroSituacao() != null && !propostaFiltro.getFiltroSituacao().isEmpty()) {
			c.add(Restrictions.in("situacao", propostaFiltro.getFiltroSituacao()));
		}
		
		// Busca por CPF Segurado Principal
		if( propostaFiltro.getCpfSegurado() != null && !"".equals(propostaFiltro.getCpfSegurado()) ){
			// Busca por CPF Segurado Principal
			Criterion cpfSeguradoCriterion = Restrictions.eq("cpfSegurado", propostaFiltro.getCpfSegurado() );
			Criterion cpfSeguradoFormatadoCriterion = Restrictions.eq("cpfSegurado", propostaFiltro.getCpfSeguradoFormatado() );
			Criterion orCpfSegurado = Restrictions.or(cpfSeguradoCriterion,cpfSeguradoFormatadoCriterion );
			
			c.add( orCpfSegurado );
		}
		
		
		// Busca por Nome do Segurado Conjuge
		if( StringUtils.isNotEmpty(propostaFiltro.getNomeConjuge()) ){
			c.add( Restrictions.ilike("nomeConjuge", propostaFiltro.getNomeConjuge(), MatchMode.ANYWHERE ) );
		}
		
		//Busca Por Cidade
		if( propostaFiltro.getCidade() != null && propostaFiltro.getCidade().getId() != null && propostaFiltro.getCidade().getId() != 0){
			c.createAlias("cidade", "cidade");
			c.add( Restrictions.eq("cidade.id", propostaFiltro.getCidade().getId() ) );
		}	
		
		if( propostaFiltro.getOrgao() != null && propostaFiltro.getOrgao().getId() != null && propostaFiltro.getOrgao().getId() != 0){
			c.createAlias("orgao", "orgao");
			c.add( Restrictions.eq("orgao.nomeOrgao", propostaFiltro.getOrgao().getNomeOrgao() ) );
		}
		
		c.addOrder( Order.asc("nomeSegurado") );
		
		if (limite != null) {
			c.setMaxResults(limite);
		}
		
		return c.list() ;
	}
	
	
	public List<Proposta> relatorioMudancaFaixaEtaria( List<Proposta> listaSegurados ){
		List<Proposta> listaSeguradosNecessitamTrocaFaixaEtaria = new ArrayList<Proposta>();
		Dao<DetalheCapitalFaixaEtaria, Long> daoDetalheCapital = new Dao<DetalheCapitalFaixaEtaria, Long>(session, DetalheCapitalFaixaEtaria.class);
		
		List<DetalheCapitalFaixaEtaria> detalheCapitalFaixaFull = daoDetalheCapital.list();
		Map<Integer, List<DetalheCapitalFaixaEtaria> > mapaDetalheCapitalFaixaChaveFull = new HashMap<Integer, List<DetalheCapitalFaixaEtaria> >();
		for (DetalheCapitalFaixaEtaria detalheCapitalFaixa : detalheCapitalFaixaFull ) {
			
			Integer chave = detalheCapitalFaixa.getCapitalSeguradoFaixaEtaria().getNomeTabela().getId();
			if( mapaDetalheCapitalFaixaChaveFull.containsKey( chave ) ){
				List<DetalheCapitalFaixaEtaria> listaDetalhesFaixaEtaria = mapaDetalheCapitalFaixaChaveFull.get(chave);
				listaDetalhesFaixaEtaria.add( detalheCapitalFaixa );
			}else{
				List<DetalheCapitalFaixaEtaria> listaDetalhesFaixaEtaria = new ArrayList<DetalheCapitalFaixaEtaria>();
				listaDetalhesFaixaEtaria.add( detalheCapitalFaixa );
				mapaDetalheCapitalFaixaChaveFull.put( chave , listaDetalhesFaixaEtaria );
			}
		}
		
		for (Iterator iterator = listaSegurados.iterator(); iterator.hasNext();) {
			//Para o relat�rio de faixa et�ria devemos pegar o segurado, pegar o DetalheCapitalFaixaEtaria dele e verificar se a idade � condizente com o valor que ele est� pagando.
			
			Proposta proposta = (Proposta) iterator.next();
			
			if( proposta.getModeloProposta() != null && Constantes.MODELO_PROPOSTA_FAIXA_ETARIA.equals( proposta.getModeloProposta().getId() ) ){
				int idadeSegurado = calculaIdadeSegurado( proposta.getDataNascimentoSegurado(), -30 );				
				
				DetalheCapitalFaixaEtaria detalheCapitalSegurado = proposta.getDetalheCapitalFaixaEtaria();
				
					// Verifica se a idade atual do segurado condiz com a condi��o que foi escolhida pra ele
					if( idadeSegurado > 65 || idadeSegurado < 18 || ( idadeSegurado >= detalheCapitalSegurado.getIdadeInicial() && idadeSegurado <= detalheCapitalSegurado.getIdadeFinal() ) ){
						continue;
					}else{
						//Se o range est� fora, ent�o verifica a qual range pertence usando o mapa de detalhes
						Integer chave = detalheCapitalSegurado.getCapitalSeguradoFaixaEtaria().getNomeTabela().getId();
						List<DetalheCapitalFaixaEtaria> listaDetalhesFaixaEtaria = mapaDetalheCapitalFaixaChaveFull.get(chave);
						DetalheCapitalFaixaEtaria detalheCapitalFaixaEtariaTemp = new DetalheCapitalFaixaEtaria();
						for (DetalheCapitalFaixaEtaria detalheCapitalFaixaEtaria : listaDetalhesFaixaEtaria) {
							if( idadeSegurado >= detalheCapitalFaixaEtaria.getIdadeInicial() && idadeSegurado <= detalheCapitalFaixaEtaria.getIdadeFinal() ){
								// Verifica o mesmo capital
								if( detalheCapitalSegurado.getCapitalSegurado().equals( detalheCapitalFaixaEtaria.getCapitalSegurado() ) ){
									//Se encontrou o range adquire o capital para extrair informa��es
									detalheCapitalFaixaEtariaTemp = detalheCapitalFaixaEtaria;
									break;
								}
							}
						}
						proposta.setIdadePrevistaSegurado( idadeSegurado );
						int idadeReal = calculaIdadeSegurado( proposta.getDataNascimentoSegurado(), 0 );
						proposta.setIdadeRealSegurado( idadeReal );
						if( proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO ){
						//if( proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_CASADO ) ){
							proposta.setNovoValorFaixaEtaria( detalheCapitalFaixaEtariaTemp.getSomaTitularConjuge() );
						}else{
							proposta.setNovoValorFaixaEtaria( detalheCapitalFaixaEtariaTemp.getTitular() );
						}
						listaSeguradosNecessitamTrocaFaixaEtaria.add( proposta );
					}
					
	
					
			}
		}
		
		return listaSeguradosNecessitamTrocaFaixaEtaria;
	}
	
	public int calculaIdadeSegurado( Date dataNascimento, int diasPrevisao ){
		try{
		Calendar bd = new GregorianCalendar();
		Calendar cd = Calendar.getInstance();
		bd.setTime(dataNascimento);
		//Adiciona 30 dias na data, para previs�o
		bd.add( Calendar.DAY_OF_MONTH , diasPrevisao );
		int day =0 , month = 0, ageYears, ageMonths, ageDays;
		
		month = bd.get( Calendar.MONTH );
		day = bd.get( Calendar.DAY_OF_MONTH );
		
	    ageYears = cd.get(Calendar.YEAR) - bd.get(Calendar.YEAR);
	    if(cd.before(new GregorianCalendar(cd.get(Calendar.YEAR), month, day))){
	      ageYears--;
	      ageMonths = (12 - (bd.get(Calendar.MONTH) + 1)) + (cd.get(Calendar.MONTH));
	      if(day > cd.get(Calendar.DAY_OF_MONTH)){
	        ageDays = ( 30 - day ) + cd.get(Calendar.DAY_OF_MONTH);
	      }
	      else if(day < cd.get(Calendar.DAY_OF_MONTH)){
	        ageDays = cd.get(Calendar.DAY_OF_MONTH) - day;
	        ageMonths++;
	      }
	      else{
	        ageDays = 0;
	      }
	    }
	    else if(cd.after(new GregorianCalendar(cd.get(Calendar.YEAR), month, day))){
	      ageMonths = (cd.get(Calendar.MONTH) - (bd.get(Calendar.MONTH)));
	      if(day > cd.get(Calendar.DAY_OF_MONTH))
	        ageDays = day - cd.get(Calendar.DAY_OF_MONTH) - day;
	      else if(day < cd.get(Calendar.DAY_OF_MONTH)){
	        ageDays = cd.get(Calendar.DAY_OF_MONTH) - day;
	      }
	      else
	        ageDays = 0;
	    }
	    else{
	      ageYears = cd.get(Calendar.YEAR) - bd.get(Calendar.YEAR);
	      ageMonths = 0;
	      ageDays = 0;
	    }
	    return ageYears ;
		}catch( Exception exc ){
			return 0;
		}
	  
	}
	
	public List<Proposta> populaCapital(Proposta proposta) {
		List<Proposta> listaPropostas = new ArrayList<Proposta>();
		listaPropostas.add(proposta);
		return populaCapital(listaPropostas, -1);
	}
	
	/**
	 * Popula os dados de capital da propostas com base no plano e nas tabelas de valores. 
	 * 
	 * @param listaPropostas
	 * @return Lista das propostas com o valor do capital atualizado
	 */
	public List<Proposta> populaCapital( List<Proposta> listaPropostas) {
		return populaCapital(listaPropostas, -1);
	}
	
	/**
	 * � partir de um capital escolhido, popula o do conjuge se o plano for casado
	 * @param tipoRelatorio 
	 * @param e
	 */
	public List<Proposta> populaCapital( List<Proposta> listaPropostas, Integer tipoRelatorio ){		
		for (Proposta proposta : listaPropostas) {
			if( tipoRelatorio != 3 ){
				Integer idade = calculaIdadeSegurado( proposta.getDataNascimentoSegurado(), 0 );
				proposta.setIdadeRealSegurado( idade );
			}
			
			// Para propostas do modelo de Limite de Idade
			if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
					&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_VALOR_FIXO ) ){
				
				if( proposta.getCapitalSeguradoLimiteIdade() != null ){
				
					if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
							&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
							//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_50 ) 
							&& proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
						
						proposta.setCapitalSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
						proposta.setCapitalConjuge( proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() );
						proposta.setPremioConjuge( proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado50() - proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
						proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
						proposta.setTotalPremios(  proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado50() );
					}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
							&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
							//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_CASADO_100 )
							&& proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
						proposta.setCapitalSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
						proposta.setCapitalConjuge( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
						proposta.setPremioConjuge( proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado100() - proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
						proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
						proposta.setTotalPremios( proposta.getCapitalSeguradoLimiteIdade().getFuncVLCasado100() );
					}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
							&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
							//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_VALOR_FIXO_INDIVIDUAL ) 
							&& proposta.getCapitalSeguradoLimiteIdade().getConjVLMorteNatural() != null ){
	
						proposta.setCapitalSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLMorteNatural() );
						proposta.setCapitalConjuge( null );
						proposta.setPremioConjuge( null );
						proposta.setPremioSegurado( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual() );
						proposta.setTotalPremios( proposta.getCapitalSeguradoLimiteIdade().getFuncVLIndividual()  );
	
						
					}else{
						proposta.setCapitalSegurado( null );
						proposta.setCapitalConjuge( null );
						proposta.setPremioConjuge( null );
						proposta.setPremioSegurado( null );
						proposta.setTotalPremios( null );
					}
				}
			}
			
			// Para propostas do modelo de Limite de Idade - VALOR FUTURO
			if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
					&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_VALOR_FIXO ) ){
				proposta.setCapitalSeguradoFuturo( proposta.getValorFuturoCapitalSegurado());
				proposta.setCapitalConjugeFuturo( proposta.getValorFuturoCapitalConjuge() );
				proposta.setPremioSeguradoFuturo( proposta.getValorFuturoPremioSegurado() );
				proposta.setPremioConjugeFuturo( proposta.getValorFuturoPremioConjuge() );
				
				proposta.setTotalPremiosFuturo((proposta.getValorFuturoPremioSegurado() != null ? proposta.getValorFuturoPremioSegurado() : 0) + (proposta.getValorFuturoPremioConjuge() != null ? proposta.getValorFuturoPremioConjuge() : 0.0));
				
			}
			
			
			// Para propostas do modelo de Faixa Etaria
			if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
					&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_ETARIA ) ){
				
				if( proposta.getDetalheCapitalFaixaEtaria() != null ){
				
					if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
							&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO
							//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_CASADO ) 
							&& proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() != null ){
						proposta.setCapitalSegurado( proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() );
						proposta.setCapitalConjuge( proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() );
						proposta.setPremioConjuge( proposta.getDetalheCapitalFaixaEtaria().getSomaTitularConjuge() - proposta.getDetalheCapitalFaixaEtaria().getTitular()  );
						proposta.setPremioSegurado( proposta.getDetalheCapitalFaixaEtaria().getTitular() );
						proposta.setTotalPremios( proposta.getDetalheCapitalFaixaEtaria().getSomaTitularConjuge() );					
					}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
							&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
							//proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_INDIVIDUAL ) 
							&& proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() != null ){
						proposta.setCapitalSegurado( proposta.getDetalheCapitalFaixaEtaria().getCapitalSegurado() );
						proposta.setCapitalConjuge( null );
						proposta.setPremioConjuge( null  );
						proposta.setPremioSegurado( proposta.getDetalheCapitalFaixaEtaria().getTitular() );
						proposta.setTotalPremios( proposta.getDetalheCapitalFaixaEtaria().getTitular() );					
						
					}else{
						proposta.setCapitalSegurado( null );
						proposta.setCapitalConjuge( null );
						proposta.setPremioConjuge( null );
						proposta.setPremioSegurado( null );
						proposta.setTotalPremios( null );					
	
					}
				}
				
				//FAIXA ET�RIA VALOR FUTURO
				if( proposta.getDetalheCapitalFaixaEtariaFuturo() != null ){
					
					if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
							&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO
							//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_CASADO ) 
							&& proposta.getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() != null ){
						proposta.setCapitalSeguradoFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() );
						proposta.setCapitalConjugeFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() );
						proposta.setPremioConjugeFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getSomaTitularConjuge() - proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular()  );
						proposta.setPremioSeguradoFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular() );
						proposta.setTotalPremiosFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getSomaTitularConjuge() );					
					}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
							&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
							//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_FAIXA_ETARIA_INDIVIDUAL ) 
							&& proposta.getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() != null ){
						proposta.setCapitalSeguradoFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getCapitalSegurado() );
						proposta.setCapitalConjugeFuturo( null );
						proposta.setPremioConjugeFuturo( null  );
						proposta.setPremioSeguradoFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular() );
						proposta.setTotalPremiosFuturo( proposta.getDetalheCapitalFaixaEtariaFuturo().getTitular() );					
						
					}else{
						proposta.setCapitalSeguradoFuturo( null );
						proposta.setCapitalConjugeFuturo( null );
						proposta.setPremioConjugeFuturo( null );
						proposta.setPremioSeguradoFuturo( null );
						proposta.setTotalPremiosFuturo( null );					
	
					}
				}
			}	
			
			//propostas reangariadas
			if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
					&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO ) 
					){
				
				if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_50 ) 
						){
					
					proposta.setCapitalSegurado( proposta.getValorCapitalSegurado());
					proposta.setCapitalConjuge( proposta.getValorCapitalConjuge() );
					proposta.setPremioSegurado( proposta.getValorPremioSegurado() );
					proposta.setPremioConjuge( proposta.getValorPremioConjuge() );
					proposta.setTotalPremios(  proposta.getPremioSegurado() + (proposta.getPremioConjuge() != null ? proposta.getPremioConjuge() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_100 
						){
					proposta.setCapitalSegurado( proposta.getValorCapitalSegurado());
					proposta.setCapitalConjuge( proposta.getValorCapitalConjuge() );
					proposta.setPremioSegurado( proposta.getValorPremioSegurado() );
					proposta.setPremioConjuge( proposta.getValorPremioConjuge() );
					proposta.setTotalPremios(  proposta.getPremioSegurado() + (proposta.getPremioConjuge() != null ? proposta.getPremioConjuge() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL ) 
						){

					proposta.setCapitalSegurado( proposta.getValorCapitalSegurado());
					proposta.setCapitalConjuge( null );
					proposta.setPremioSegurado( proposta.getValorPremioSegurado() );
					proposta.setPremioConjuge( null );
					proposta.setTotalPremios(  proposta.getPremioSegurado());
				}
			}
				

			//REANGARIADO - VALOR FUTURO
			if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
					&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_FAIXA_REANGARIADO ) ){
				
				if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_50 ) 
						){
					
					proposta.setCapitalSeguradoFuturo( proposta.getValorFuturoCapitalSegurado());
					proposta.setCapitalConjugeFuturo( proposta.getValorFuturoCapitalConjuge() );
					proposta.setPremioSeguradoFuturo( proposta.getValorFuturoPremioSegurado() );
					proposta.setPremioConjugeFuturo( proposta.getValorFuturoPremioConjuge() );
					proposta.setTotalPremiosFuturo(  (proposta.getPremioSeguradoFuturo() != null ? proposta.getPremioSeguradoFuturo() : 0.0) + (proposta.getPremioConjugeFuturo() != null ? proposta.getPremioConjugeFuturo() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_CASADO_100 )
						){
					proposta.setCapitalSeguradoFuturo( proposta.getValorFuturoCapitalSegurado());
					proposta.setCapitalConjugeFuturo( proposta.getValorFuturoCapitalConjuge() );
					proposta.setPremioSeguradoFuturo( proposta.getValorFuturoPremioSegurado() );
					proposta.setPremioConjugeFuturo( proposta.getValorFuturoPremioConjuge() );
					proposta.setTotalPremiosFuturo(  (proposta.getPremioSeguradoFuturo() != null? proposta.getPremioSeguradoFuturo() : 0.0) + (proposta.getPremioConjugeFuturo() != null ? proposta.getPremioConjugeFuturo() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL ) 
						){

					proposta.setCapitalSeguradoFuturo( proposta.getValorFuturoCapitalSegurado());
					proposta.setCapitalConjugeFuturo( null );
					proposta.setPremioSeguradoFuturo( proposta.getValorFuturoPremioSegurado() );
					proposta.setPremioConjugeFuturo( null );
					proposta.setTotalPremiosFuturo(  proposta.getPremioSeguradoFuturo());
				}
			}


			//propostas atualizadas
			if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
					&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_ATUALIZADO ) ){
				
				if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_50 ) 
						){
					
					proposta.setCapitalSegurado( proposta.getValorCapitalSegurado());
					proposta.setCapitalConjuge( proposta.getValorCapitalConjuge() );
					proposta.setPremioSegurado( proposta.getValorPremioSegurado() );
					proposta.setPremioConjuge( proposta.getValorPremioConjuge() );
					proposta.setTotalPremios(  (proposta.getPremioSegurado() != null ? proposta.getPremioSegurado() : 0.0) + (proposta.getPremioConjuge() != null ? proposta.getPremioConjuge() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_100 )
						){
					proposta.setCapitalSegurado( proposta.getValorCapitalSegurado());
					proposta.setCapitalConjuge( proposta.getValorCapitalConjuge() );
					proposta.setPremioSegurado( proposta.getValorPremioSegurado() );
					proposta.setPremioConjuge( proposta.getValorPremioConjuge() );
					proposta.setTotalPremios(  (proposta.getPremioSegurado() != null ? proposta.getPremioSegurado() : 0.0) + (proposta.getPremioConjuge() != null ? proposta.getPremioConjuge() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL ) 
						){

					proposta.setCapitalSegurado( proposta.getValorCapitalSegurado());
					proposta.setCapitalConjuge( null );
					proposta.setPremioSegurado( proposta.getValorPremioSegurado() );
					proposta.setPremioConjuge( null );
					proposta.setTotalPremios(  proposta.getPremioSegurado());
				}
			}
			
				

			//ATUALIZADO - VALOR FUTURO
			if( proposta.getModeloProposta() != null && proposta.getModeloProposta().getId() != null 
					&& proposta.getModeloProposta().getId().equals( Constantes.MODELO_PROPOSTA_ATUALIZADO ) ){
				
				if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null 
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO50
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_50 ) 
						){
					
					proposta.setCapitalSeguradoFuturo( proposta.getValorFuturoCapitalSegurado());
					proposta.setCapitalConjugeFuturo( proposta.getValorFuturoCapitalConjuge() );
					proposta.setPremioSeguradoFuturo( proposta.getValorFuturoPremioSegurado() );
					proposta.setPremioConjugeFuturo( proposta.getValorFuturoPremioConjuge() );
					proposta.setTotalPremiosFuturo(  (proposta.getPremioSeguradoFuturo() != null ? proposta.getPremioSeguradoFuturo() : 0.0) + (proposta.getPremioConjugeFuturo() != null ? proposta.getPremioConjugeFuturo() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null 
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.CASADO100
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_CASADO_100 )
						){
					proposta.setCapitalSeguradoFuturo( proposta.getValorFuturoCapitalSegurado());
					proposta.setCapitalConjugeFuturo( proposta.getValorFuturoCapitalConjuge() );
					proposta.setPremioSeguradoFuturo( proposta.getValorFuturoPremioSegurado() );
					proposta.setPremioConjugeFuturo( proposta.getValorFuturoPremioConjuge() );
					proposta.setTotalPremiosFuturo(  (proposta.getPremioSeguradoFuturo() != null? proposta.getPremioSeguradoFuturo() : 0.0) + (proposta.getPremioConjugeFuturo() != null ? proposta.getPremioConjugeFuturo() : 0.0 ));
				}else if( proposta.getTipoProposta() != null && proposta.getTipoProposta().getId() != null
						&& proposta.getTipoProposta().getComportamento() == ComportamentoPropostaEnum.INDIVIDUAL
						//&& proposta.getTipoProposta().getId().equals( Constantes.TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL ) 
						){

					proposta.setCapitalSeguradoFuturo( proposta.getValorFuturoCapitalSegurado());
					proposta.setCapitalConjugeFuturo( null );
					proposta.setPremioSeguradoFuturo( proposta.getValorFuturoPremioSegurado() );
					proposta.setPremioConjugeFuturo( null );
					proposta.setTotalPremiosFuturo(  proposta.getPremioSeguradoFuturo());
				}
				
			}
			
			
			//Se for relatorio de faixa etaria calcula a diferen�a
			if( tipoRelatorio == 3 ){
				if( proposta.getNovoValorFaixaEtaria() != null &&  proposta.getTotalPremios() != null ){
					proposta.setDiferencaValores( proposta.getNovoValorFaixaEtaria() - proposta.getTotalPremios() );
				}
			}
			
			
			//Prepara para persistir o valor do pr�mio e do capital nos novos campos
			proposta.setValorPremioSegurado(proposta.getPremioSegurado());
			proposta.setValorPremioConjuge(proposta.getPremioConjuge());
			proposta.setValorCapitalSegurado(proposta.getCapitalSegurado());
			proposta.setValorCapitalConjuge(proposta.getCapitalConjuge());
			
			proposta.setValorFuturoCapitalSegurado(proposta.getCapitalSeguradoFuturo());
			proposta.setValorFuturoCapitalConjuge(proposta.getCapitalConjugeFuturo());
			proposta.setValorFuturoPremioSegurado(proposta.getPremioSeguradoFuturo());
			proposta.setValorFuturoPremioConjuge(proposta.getPremioConjugeFuturo());
			
			
		}
		
		return listaPropostas;
	
	}
	
	public List<Proposta> getPropostasInadimplentes() {
		Criteria c = this.session.createCriteria( Proposta.class );
		c.add(Restrictions.eq("situacao", SituacaoProposta.INADIMPLENTE));
		return c.list();
	}
	
	public List<Proposta> getPropostasReangariadas(Empresa empresa) {
		Criteria c = this.session.createCriteria( Proposta.class );
		c.createAlias("modeloProposta", "m");
		c.add(Restrictions.eq("empresa", empresa));
		c.add(Restrictions.eq("m.id", 3));
		c.add(Restrictions.ne("situacao", SituacaoProposta.CANCELADA));
		return c.list();
	}
	
	
	public Proposta getPropostaPorCPF(String cpf) {
		
		String hql = "from Proposta where  replace(replace(cpfSegurado,'.',''),'-','') like :cpf  order by id desc ";
		
		Query query = session.createQuery(hql);
		query.setParameter("cpf", cpf);
		query.setMaxResults(1);
		return (Proposta) query.uniqueResult();
		


	}
	
	public Proposta getPropostaPorCPF(Proposta propostaFiltro) {

		Criteria c = session.createCriteria(Proposta.class);
		
		Criterion cpfSeguradoCriterion = Restrictions.eq("cpfSegurado", propostaFiltro.getCpfSegurado() );
		Criterion cpfSeguradoFormatadoCriterion = Restrictions.eq("cpfSegurado", propostaFiltro.getCpfSeguradoFormatado() );
		Criterion orCpfSegurado = Restrictions.or(cpfSeguradoCriterion,cpfSeguradoFormatadoCriterion );
		
		c.add( orCpfSegurado );
		c.add(Restrictions.isNull("dataCancelamento"));
	
		/*if (propostaFiltro.getCpfSegurado().length() < 11 && propostaFiltro.getNomeSegurado() != null) {
			c.add( Restrictions.ilike("nomeSegurado", propostaFiltro.getNomeSegurado(), MatchMode.ANYWHERE ) );		
		}*/
		
		//System.out.println("\n ---> cpf=" + propostaFiltro.getCpfSegurado() + ", cpfFormatado=" + propostaFiltro.getCpfSeguradoFormatado());	
		
		Proposta proposta = null;
		try {
			proposta = (Proposta)c.uniqueResult();	
		} catch (Exception e) {
			proposta = null;
		}
		
		return proposta;
	}
	
	public void removerNumeroSorte(Proposta proposta) {
		Query query = this.session.createQuery("delete from NumeroSorteio where proposta = :proposta");
		query.setParameter("proposta", proposta);
		query.executeUpdate();	
	}
	
	public List<Object[]> recuperarHistoricos() {
	
		String query = "select h.modeloProposta.id, h.proposta.id, h.dataAlteracao, h.empresa.id  from HistoricoProposta h order by h.proposta.id, h.dataAlteracao asc";
		Query q1 = session.createQuery(query);
		
		return q1.list();
		
	}
	
	public void evict(Proposta proposta) {
		session.evict(proposta);
	}
	
	
	
}
