package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Filial;

public class FilialDAO {

	private Session session;
	
	public FilialDAO( Session session ){
		this.session = session;
	}
	
	
	public List<Filial> buscaPorFiltro( Filial filialFiltro ){
		Criteria c = this.session.createCriteria( Filial.class );

		c.add( Restrictions.ilike("nomeFilial", filialFiltro.getNomeFilial(), MatchMode.ANYWHERE ) );

		
		if( filialFiltro.getCidade() != null && filialFiltro.getCidade().getId() != null){
			c.createAlias("cidade", "cidade");
			c.add( Restrictions.eq("cidade.id", filialFiltro.getCidade().getId() ) );
		}	
		
					
		return c.list();
	}
	
	public List<Filial> buscaPorNomePopUp( String nome ){
		Criteria c = this.session.createCriteria( Filial.class );

		c.add( Restrictions.ilike("nomeFilial", nome, MatchMode.ANYWHERE ) );
		
		return c.list();
	}
}
