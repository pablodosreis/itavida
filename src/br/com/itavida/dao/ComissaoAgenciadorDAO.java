package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.ComissaoAgenciador;

public class ComissaoAgenciadorDAO {

	private Session session;

	public ComissaoAgenciadorDAO(Session session) {
		this.session = session;
	}

	public List<ComissaoAgenciador> buscaPorProposta(Integer idProposta) {
		Criteria c = this.session.createCriteria(ComissaoAgenciador.class);
		c.createAlias("proposta", "proposta");
		c.add(Restrictions.eq("proposta.id", idProposta));
		return c.list();
	}

}
