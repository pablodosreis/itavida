package br.com.itavida.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.dao.base.BaseDAO;
import br.com.itavida.entidades.MovimentacaoFinanceira;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.entidades.SuperAp;
import br.com.itavida.enums.SituacaoBoleto;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.exceptions.SistemaException;

public class MovimentacaoFinanceiraDAO extends BaseDAO {
	
	
	public MovimentacaoFinanceiraDAO( ){

	}
	
	public MovimentacaoFinanceira load(Integer id){
		Criteria c = getSession().createCriteria(MovimentacaoFinanceira.class);
		c.add(Restrictions.eq("id", id));
		return (MovimentacaoFinanceira) c.uniqueResult();
		
	}


	
	public void saveOrUpdate(MovimentacaoFinanceira obj) {
		
		if (obj.getProposta() == null) {
			throw new SistemaException("A proposta da movimenta��o deve ser informada.");
		}
		
//		if (obj.getProposta().getSituacao() == SituacaoProposta.CANCELADA) {
//			throw new SistemaException("A proposta informado est� cancelada.");
//		}
		
		if (jaExisteMovimentacao(obj)) {
			throw new SistemaException("J� existe um registro cadastrado para este segurado.");
		}
		
		if (obj.getValor() == null) {
			throw new SistemaException("O valor da movimenta��o deve ser informado.");
		}
		
		getSession().saveOrUpdate(obj);
	}
	
	public void delete(MovimentacaoFinanceira obj) {
		getSession().delete(obj);
	}
	
	public MovimentacaoFinanceira getMovimentacao(Date referencia, Proposta proposta) {
		String hql = " from MovimentacaoFinanceira where referencia = :referencia and proposta = :proposta order by proposta.nomeSegurado ";
		Query query = getSession().createQuery(hql);
		query.setParameter("referencia", referencia);
		query.setParameter("proposta", proposta);
		return (MovimentacaoFinanceira) query.uniqueResult();	
	}
	
	public boolean jaExisteMovimentacao(Date referencia, Proposta proposta) {
		MovimentacaoFinanceira obj = new MovimentacaoFinanceira();
		obj.setReferencia(referencia);
		obj.setProposta(proposta);
		return jaExisteMovimentacao(obj);
		
	}
	
	
	public boolean jaExisteMovimentacao(MovimentacaoFinanceira obj) {
		String hql = " from MovimentacaoFinanceira where id <> :id and referencia = :referencia and proposta = :proposta order by proposta.nomeSegurado ";
		Query query = getSession().createQuery(hql);
		query.setParameter("referencia", obj.getReferencia());
		query.setParameter("proposta", obj.getProposta());
		query.setParameter("id", obj.getId());
		return query.uniqueResult() != null;	
	}
	
	public List<MovimentacaoFinanceira> buscarMovimentacoes(Date referencia) {
		String hql = " from MovimentacaoFinanceira where referencia = :referencia order by proposta.nomeSegurado ";
		Query query = getSession().createQuery(hql);
		query.setParameter("referencia", referencia);
		return query.list();
	}
	
	public List<MovimentacaoFinanceira> buscarMovimentacoes(Integer idProposta) {
		String hql = " from MovimentacaoFinanceira where proposta.id = :propostaId order by proposta.nomeSegurado ";
		Query query = getSession().createQuery(hql);
		query.setParameter("propostaId", idProposta);
		return query.list();
	}
	
	public void atualizarSituacaoSuperAp() {
		
		
		String hql = "Select distinct proposta from MovimentacaoFinanceira where proposta.superAP is not null and proposta.situacao in (:situacoes) and situacaoBoleto = :situacaoBoleto ";
		
		/*Caso exista boleto em atraso registra a proposta como inadimplente*/
		Query query = getSession().createQuery(hql);
		query.setParameter("situacaoBoleto", SituacaoBoleto.EM_ATRASO);
		query.setParameterList("situacoes", new SituacaoProposta[]{ SituacaoProposta.ATIVA, SituacaoProposta.PENDENTE});
		for (Proposta proposta : (List<Proposta>) query.list()) {
			proposta.setSituacao(SituacaoProposta.INADIMPLENTE);
			getSession().update(proposta);
		}
		
		/*Caso todos os boletos esteja quitados marca a proposta como ativa*/
/*		hql = "from Proposta p where p.superAp is not null and p.situacao in (:situacoes) and not exists (select m.id from MovimentacaoFinanceira m where m.proposta.id = p.id and m.situacaoBoleto <> :situacaoBoleto) ";
		query = getSession().createQuery(hql);
		query.setParameter("situacaoBoleto", SituacaoBoleto.EM_ATRASO);
		query.setParameterList("situacoes", new SituacaoProposta[]{ SituacaoProposta.INADIMPLENTE, SituacaoProposta.PENDENTE});
		for (Proposta proposta : (List<Proposta>) query.list()) {
			proposta.setSituacao(SituacaoProposta.INADIMPLENTE);
			getSession().update(proposta);
		}*/
		
	}
	
	public Map<String,List<MovimentacaoFinanceira>> buscarMovimentacoesPorSituacao(Date inicio, Date fim, Integer uf, Integer propostaId, List<SituacaoProposta> situacoes) {
		Map<String,List<MovimentacaoFinanceira>> mapa = new TreeMap<String, List<MovimentacaoFinanceira>>();
		List<MovimentacaoFinanceira> lista = buscarMovimentacoes(inicio, fim, uf, propostaId, situacoes);
		for (MovimentacaoFinanceira movimentacaoFinanceira : lista) {
			String situacao = movimentacaoFinanceira.getProposta().getSituacao().getNome();
			if (movimentacaoFinanceira.getProposta().getSituacao() == SituacaoProposta.ATIVA) {
				SuperAp superAp = movimentacaoFinanceira.getProposta().getSuperAp();
				situacao += superAp.getPeriodicidade().equals("0") ? " MENSAL" : " ANUAL";
			}
			List<MovimentacaoFinanceira> movs = mapa.get(situacao);
			if (movs == null) {
				mapa.put(situacao, new ArrayList<MovimentacaoFinanceira>());
			}
			mapa.get(situacao).add(movimentacaoFinanceira);
		}
		
		String situacaoAguardando = SituacaoProposta.AGUARDANDO_AVERBACAO.getNome();
		List<MovimentacaoFinanceira> movs = mapa.get(situacaoAguardando);
		if (movs == null) {
			mapa.put(situacaoAguardando, new ArrayList<MovimentacaoFinanceira>());
		}
		//buscar super app
		for (Proposta proposta : getPropostasSuperApAguardando()) {
			MovimentacaoFinanceira movimentacaoFinanceira = new MovimentacaoFinanceira();
			movimentacaoFinanceira.setProposta(proposta);
			movimentacaoFinanceira.setNumeroParcela(0);
			movimentacaoFinanceira.setSituacaoBoleto(SituacaoBoleto.NAO_GERADO);
			movimentacaoFinanceira.setSituacao(SituacaoProposta.AGUARDANDO_AVERBACAO);
			mapa.get(situacaoAguardando).add(movimentacaoFinanceira);
		}
		
		return mapa;
		
	}
	
	public List<Proposta> getPropostasSuperApAguardando() {
		
		String hql  = " from Proposta where superAp is not null and situacao = :situacao ";
		Query query = getSession().createQuery(hql);
		query.setParameter("situacao", SituacaoProposta.AGUARDANDO_AVERBACAO);
		return query.list();
	}
	
	public List<MovimentacaoFinanceira> buscarMovimentacoes(Date inicio, Date fim, Integer uf, Integer propostaId, List<SituacaoProposta> situacoes) {
		String hql = " from MovimentacaoFinanceira "
				+ " where ((referencia between :inicio and :fim)  or (dataPagamentoComissao between :inicio and :fim)) ";
		
		if (situacoes != null && !situacoes.isEmpty()) {
			hql += "  and proposta.situacao in (:situacoes)  ";
		}
		
		if (propostaId != null) {
			hql += "  and proposta.id = :propostaId ";
		}
		
		hql += "  order by proposta.nomeSegurado ";
		Query query = getSession().createQuery(hql);
		query.setParameter("inicio", inicio);
		query.setParameter("fim", fim);
		if (situacoes != null && !situacoes.isEmpty()) {
			query.setParameterList("situacoes", situacoes);
		}
		if (propostaId != null) {
			query.setParameter("propostaId", propostaId);
		}
		return query.list();	
	}
	
	public Long proximaParcela(Proposta proposta) {
		String hql = "Select max(numeroParcela) from MovimentacaoFinanceira where proposta.id = :propostaId ";
		Query query = getSession().createQuery(hql);
		query.setParameter("propostaId", proposta.getId());
		Object uniqueResult = query.uniqueResult();
		if (uniqueResult != null) {
			return (new Long(uniqueResult.toString())) + 1;
		} else {
			return 1L;
		}
	}
	

}
