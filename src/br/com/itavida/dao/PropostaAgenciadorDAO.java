package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.PropostaAgenciador;

public class PropostaAgenciadorDAO {

	private Session session;
	
	public PropostaAgenciadorDAO( Session session ){
		this.session = session;
	}
	
	
	public List<PropostaAgenciador> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( PropostaAgenciador.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		return c.list();
	}
	
	
}
