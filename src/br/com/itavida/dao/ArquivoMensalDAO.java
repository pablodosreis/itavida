package br.com.itavida.dao;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.ArquivoMensal;
import br.com.itavida.entidades.ItemMensal;
import br.com.itavida.entidades.MovimentacaoOrgaoAtrasado;
import br.com.itavida.entidades.MovimentacaoPropostaAtrasada;
import br.com.itavida.entidades.Orgao;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.enums.SituacaoProposta;
import br.com.itavida.exceptions.SistemaException;

public class ArquivoMensalDAO {

	private Session session;
	
	private PropostaDAO propostaDAO;
	
	public ArquivoMensalDAO( Session session ){
		this.session = session;
		propostaDAO = new PropostaDAO(session);
	}
	
	public List<ArquivoMensal> findAllAtivos() {
		Criteria criteria = this.session.createCriteria( ArquivoMensal.class );
		criteria.add(Restrictions.eq("ativo", true));
		criteria.addOrder(Order.desc("competencia"));
		return criteria.list();
	}
	
	public ArquivoMensal find(Integer id) {
		Criteria criteria = this.session.createCriteria( ArquivoMensal.class );
		criteria.add(Restrictions.eq("id", id));
		return (ArquivoMensal) criteria.uniqueResult();
	}
	
	public ArquivoMensal find(Date competencia) {
		Criteria criteria = this.session.createCriteria( ArquivoMensal.class );
		criteria.add(Restrictions.eq("competencia", competencia));
		criteria.add(Restrictions.eq("ativo", true));
		criteria.setMaxResults(1);
		return (ArquivoMensal) criteria.uniqueResult();
	}
	
	public ArquivoMensal excluirArquivo(ArquivoMensal arquivo) {
		arquivo.setAtivo(false);
		this.session.update(arquivo);
		return arquivo;
	}
	
	public ArquivoMensal excluirArquivo(Date competencia) {
		ArquivoMensal arquivo = find(competencia);
		if (arquivo != null) {
			excluirArquivo(arquivo);
		}
		return arquivo;
	}
	
	public boolean existeArquivoPeriodoIgualOuAnterior(Date competencia) {
		Criteria criteria = this.session.createCriteria( ArquivoMensal.class );
		criteria.setProjection(Projections.rowCount());
		criteria.add(Restrictions.ge("competencia", competencia));
		criteria.add(Restrictions.eq("ativo", true));
		criteria.setMaxResults(1);
		return ((Integer) criteria.uniqueResult()) > 0;
	}
	
	public void gerarArquivo(Date competencia, String usuario) {
		ArquivoMensal arquivoMensal = new ArquivoMensal();
		arquivoMensal.setAtivo(true);
		arquivoMensal.setUsuario(usuario);
		arquivoMensal.setCompetencia(competencia);
		arquivoMensal.setDataGeracao(new Date());
		arquivoMensal.setItens(new ArrayList<ItemMensal>());

		/*Adiciona as propostas pagas no m�s da compet�ncia*/
		List<Proposta> propostasDoMes = findPropostasPeriodo(competencia);
		for (Proposta proposta : propostasDoMes) {
			arquivoMensal.getItens().add(transform(proposta, competencia, true));
		}
		
		/*Adiciona propostas n�o pagas, apenas para registrar o valor para o pr�ximo m�s*/
		List<Proposta> propostasNaoPagas = findPropostasPeriodoNaoPagas(competencia);
		for (Proposta proposta : propostasNaoPagas) {
			arquivoMensal.getItens().add(transform(proposta, competencia, false));
		}
		
		/*Adiciona propostas quitando meses anteriores*/
		List<MovimentacaoPropostaAtrasada> mPropostasAtrasadas = findMovimentacoesPropostasAtrasadas(competencia);
		for (MovimentacaoPropostaAtrasada movimentacaoPropostaAtrasada : mPropostasAtrasadas) {
			ItemMensal itemNaoPago = getItem(movimentacaoPropostaAtrasada.getPeriodoQuitacaoAnterior(), movimentacaoPropostaAtrasada.getProposta());
			if (itemNaoPago == null) {
				throw new SistemaException("N�o encontrado valor da proposta " + movimentacaoPropostaAtrasada.getProposta().getId() + " no arquivo de refer�ncia " + movimentacaoPropostaAtrasada.getPeriodoQuitacaoFormatado());
			}
			arquivoMensal.getItens().add(transform(itemNaoPago, movimentacaoPropostaAtrasada.getPeriodoQuitacaoAnterior(), true));
		}
		
		/*Adiciona propostas de �rg�os com quita��es anteriores*/
		List<MovimentacaoOrgaoAtrasado> mOrgaosAtrasadas = findMovimentacoesOrgaosAtrasados(competencia);
		for (MovimentacaoOrgaoAtrasado movimentacaoOrgaoAtrasado : mOrgaosAtrasadas) {
			List<Proposta> propostasDoOrgao =  findPropostasOrgaoPeriodo(movimentacaoOrgaoAtrasado.getOrgao(), movimentacaoOrgaoAtrasado.getPeriodoQuitacaoAnterior());
			for (Proposta proposta : propostasDoOrgao) {
				ItemMensal itemNaoPago = getItem(movimentacaoOrgaoAtrasado.getPeriodoQuitacaoAnterior(), proposta);
				if (itemNaoPago == null) {
					continue;
					//throw new SistemaException("N�o encontrado valor da proposta " + proposta.getId() + " no arquivo de refer�ncia " + movimentacaoOrgaoAtrasado.getPeriodoQuitacaoFormatado());
				}
				arquivoMensal.getItens().add(transform(itemNaoPago, movimentacaoOrgaoAtrasado.getPeriodoQuitacaoAnterior(), true));
			}
		}
		
		
		/*Define o arquivo mensal em todos os itens*/
		for (ItemMensal item : arquivoMensal.getItens()) {
			item.setArquivoMensal(arquivoMensal);
		}
		
		session.saveOrUpdate(arquivoMensal);
		
	}
	
	public ItemMensal getItem(Date competencia, Proposta proposta) {
		String hql = " from ItemMensal i"
				+ " where i.arquivoMensal.competencia = :competencia"
				+ "   and i.referencia = :referencia  "
				+ "   and i.arquivoMensal.ativo = true "
				+ "   and i.proposta = :proposta ";
		Query query = session.createQuery(hql);
		query.setParameter("competencia", competencia);
		query.setParameter("referencia", competencia);
		query.setParameter("proposta", proposta);
		return (ItemMensal) query.uniqueResult();
	}
	
	public ItemMensal transform(ItemMensal item, Date referencia, boolean pago) {
		ItemMensal itemMensal = new ItemMensal();
		itemMensal.setDataUltimaAtualizacao(null);
		itemMensal.setUsuarioUltimaAtualizacao(null);
		itemMensal.setReferencia(referencia);
		itemMensal.setNomeOrgao(item.getOrgao().getNomeOrgao());
		itemMensal.setNomeSegurado(item.getNomeSegurado());
		itemMensal.setDataNascimentoSegurado(item.getDataNascimentoSegurado());
		itemMensal.setCpfSegurado(item.getCpfSegurado());
		itemMensal.setPremioSegurado(item.getPremioSegurado());
		itemMensal.setCapitalSegurado(item.getCapitalSegurado());
		itemMensal.setDescricaoPlano(item.getTipoProposta().getDescricao());
		itemMensal.setNomeConjuge(item.getNomeConjuge());
		itemMensal.setDataNascimentoConjuge(item.getDataNascimentoConjuge());
		itemMensal.setPremioConjuge(item.getPremioConjuge());
		itemMensal.setCapitalConjuge(item.getCapitalConjuge());
		itemMensal.setEndereco(item.getEndereco());
		itemMensal.setBairro(item.getBairro());
		itemMensal.setCidade(item.getCidade());
		itemMensal.setUf(item.getUf());
		itemMensal.setCep(item.getCep());
		itemMensal.setTipoProposta(item.getTipoProposta());
		itemMensal.setProposta(item.getProposta());
		itemMensal.setOrgao(item.getOrgao());
		itemMensal.setEmpresa(item.getEmpresa());
		itemMensal.setItemPago(pago);
		return itemMensal;
	}
	
	public ItemMensal transform(Proposta proposta, Date referencia, boolean pago) {
		ItemMensal itemMensal = new ItemMensal();
		propostaDAO.populaCapital(proposta);
		itemMensal.setDataUltimaAtualizacao(null);
		itemMensal.setUsuarioUltimaAtualizacao(null);
		itemMensal.setReferencia(referencia);
		itemMensal.setNomeOrgao(proposta.getOrgao().getNomeOrgao());
		itemMensal.setNomeSegurado(proposta.getNomeSegurado());
		itemMensal.setDataNascimentoSegurado(proposta.getDataNascimentoSegurado());
		itemMensal.setCpfSegurado(proposta.getCpfSegurado());
		itemMensal.setPremioSegurado(proposta.getPremioSegurado());
		itemMensal.setCapitalSegurado(proposta.getCapitalSegurado());
		itemMensal.setDescricaoPlano(proposta.getTipoProposta().getDescricao());
		itemMensal.setNomeConjuge(proposta.getNomeConjuge());
		itemMensal.setDataNascimentoConjuge(proposta.getDataNascimentoConjuge());
		itemMensal.setPremioConjuge(proposta.getPremioConjuge());
		itemMensal.setCapitalConjuge(proposta.getCapitalConjuge());
		itemMensal.setEndereco(proposta.getEnderecoSegurado());
		itemMensal.setBairro(proposta.getBairro());
		itemMensal.setCidade(proposta.getCidade().getNom_cidade());
		itemMensal.setUf(proposta.getCidade().getCod_estado().getSgl_estado());
		itemMensal.setCep(proposta.getCep());
		itemMensal.setTipoProposta(proposta.getTipoProposta());
		itemMensal.setProposta(proposta);
		itemMensal.setOrgao(proposta.getOrgao());
		itemMensal.setEmpresa(proposta.getEmpresa());
		itemMensal.setItemPago(pago);
		return itemMensal;
	}
	
	public List<Proposta> findPropostasPeriodoNaoPagas(Date competencia) {
		String hql = " from Proposta p"
				+ " where p.situacao in (:situacoes) "
				+ "   and ("
				+ "			exists (select id from MovimentacaoPropostaInadimplente mp where mp.proposta.id = p.id and mp.referencia = :competencia  ) "
				+ " 		  or "
				+ "			exists (select id from MovimentacaoOrgaoInadimplente mo where mo.orgao.id = p.orgao.id and mo.referencia = :competencia  ) "
				+ "		  ) ";
		Query query = session.createQuery(hql);
		query.setParameterList("situacoes", new SituacaoProposta[]{SituacaoProposta.ATIVA, SituacaoProposta.PENDENTE, SituacaoProposta.INADIMPLENTE});
		query.setParameter("competencia", competencia);
		return query.list();
	}
	
	public List<Proposta> findPropostasPeriodo(Date competencia) {
		String hql = " from Proposta p"
				+ " where p.situacao in (:situacoes) "
				+ "   and not exists (select id from MovimentacaoPropostaInadimplente mp where mp.proposta.id = p.id and mp.referencia = :competencia  ) "
				+ "   and not exists (select id from MovimentacaoOrgaoInadimplente mo where mo.orgao.id = p.orgao.id and mo.referencia = :competencia  ) ";
		Query query = session.createQuery(hql);
		query.setParameterList("situacoes", new SituacaoProposta[]{SituacaoProposta.ATIVA, SituacaoProposta.PENDENTE, SituacaoProposta.INADIMPLENTE});
		query.setParameter("competencia", competencia);
		return query.list();
	}
	
	public List<Proposta> findPropostasOrgaoPeriodo(Orgao orgao, Date competencia) {
		String hql = " from Proposta p"
				+ " where p.situacao in (:situacoes) "
				+ "   and p.orgao.id = :idOrgao"
				+ "   and not exists (select id from MovimentacaoPropostaInadimplente mp where mp.proposta.id = p.id and mp.referencia = :competencia  ) ";
		Query query = session.createQuery(hql);
		query.setParameterList("situacoes", new SituacaoProposta[]{SituacaoProposta.ATIVA, SituacaoProposta.PENDENTE, SituacaoProposta.INADIMPLENTE});
		query.setParameter("competencia", competencia);
		query.setParameter("idOrgao", orgao.getId());
		return query.list();
	}
	
	public List<MovimentacaoPropostaAtrasada> findMovimentacoesPropostasAtrasadas(Date competencia) {
		String hql = " from MovimentacaoPropostaAtrasada m "
				+ "    where m.referencia = :competencia ";
		Query query = session.createQuery(hql);
		query.setParameter("competencia", competencia);
		return query.list();
	}
	
	public List<MovimentacaoOrgaoAtrasado> findMovimentacoesOrgaosAtrasados(Date competencia) {
		String hql = " from MovimentacaoOrgaoAtrasado m "
				+ "    where m.referencia = :competencia ";
		Query query = session.createQuery(hql);
		query.setParameter("competencia", competencia);
		return query.list();
	}
	
	public ItemMensal getItemNaoPago(Date competencia, Date referencia, Proposta proposta) {
		String hql = " from ItemMensal im "
				+ "    where im.itemPago = false "
				+ "		 and im.proposta = :proposta "
				+ "		 and im.referencia = :referencia "
				+ "		 and im.arquivoMensal.competencia = :competencia ";
		Query query = session.createQuery(hql);
		query.setParameter("competencia", competencia);
		query.setParameter("referencia", referencia);
		query.setParameter("proposta", proposta);
		return (ItemMensal) query.uniqueResult();
	}
	
	public List<Object[]> getDadosHistoricos(Date inicio, Date fim) {
		
		String sql = " select  " +
				" convert(concat('''',lpad(month(m.data_comissao), 2, \'0\'), \'/\', year(m.data_comissao),''''), char) competencia " +
				" ,avg(m.idade) idade " +
				" ,sum(ifnull(m.valor_premio,0)) premios_total " +
				" ,sum(ifnull(m.valor_capital,0)) capital_total " +
				" ,count(DISTINCT m.proposta_id) quantidade_propostas " +
				" ,count(DISTINCT m.orgao_id) total_orgaos " +
				" ,IFNULL(sinistros.quantidade,0) total_sinistros " +
				" ,sum(case when m.sexo = \'M\' then 1 else 0 end) numero_homens " +
				" ,sum(case when m.sexo = \'F\' then 1 else 0 end) numero_mulheres " +
				" ,sum(IFNULL(m.valor_premio, 0)- IFNULL(m.valor_premio_conjuge, 0)) premio_segurado " +
				" ,sum(IFNULL(m.valor_premio_conjuge, 0)) premio_conjuge " +
				" ,sum(IFNULL(m.valor_capital, 0)- IFNULL(m.valor_capital_conjuge, 0)) capital_conjuge " +
				" ,IFNULL(sinistros.valor,0) valor_sinistros " +
				" ,convert(concat(\'01/\', lpad(month(m.data_comissao), 2, \'0\'), \'/\', year(m.data_comissao)), char)  competencia_completa " +
				" from movimentacao_financeira m " +
				" LEFT JOIN " +
				"   (SELECT concat(year(dataSinistro), \'-\', lpad(month(dataSinistro), 2, \'0\'), \'-01\') competencia, " +
				"           count(*) quantidade, " +
				"           sum(ifnull(valorSinistro, 0) + ifnull(valorCestaBasica, 0) + ifnull(valorFuneral, 0)) valor " +
				"    FROM historicosinistro " +
				"    GROUP BY year(dataSinistro), " +
				"             month(dataSinistro)) sinistros ON sinistros.competencia = m.referencia " +
				" where m.data_comissao between :inicio and :fim " +
				" group by convert(concat(lpad(month(m.data_comissao), 2, \'0\'), \'/\', year(m.data_comissao)), char) ";
		
		Query query = session.createSQLQuery(sql);
		query.setParameter("inicio", inicio);
		query.setParameter("fim", fim);
		
		return query.list();
	}

	public Session getSession() {
		return session;
	}
}
