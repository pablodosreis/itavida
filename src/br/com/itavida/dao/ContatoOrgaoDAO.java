package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.ContatoOrgao;

public class ContatoOrgaoDAO {

	private Session session;
	
	public ContatoOrgaoDAO( Session session ){
		this.session = session;
	}
	
	
	public List<ContatoOrgao> buscaPorOrgao( Integer idOrgao ){
		Criteria c = this.session.createCriteria( ContatoOrgao.class );
		c.createAlias("orgao", "orgao");
		c.add( Restrictions.eq("orgao.id", idOrgao ) );
		return c.list();
	}
	
	
}
