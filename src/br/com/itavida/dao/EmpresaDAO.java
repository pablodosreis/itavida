package br.com.itavida.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.ComportamentoPropostaEnum;
import br.com.itavida.entidades.Empresa;
import br.com.itavida.entidades.HistoricoAjusteEmpresa;
import br.com.itavida.entidades.HistoricoCapital;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.DateUtil;

public class EmpresaDAO {

	
	private Session session;
	
	public EmpresaDAO( Session session ){
		this.session = session;
	}
	
	public void atualizarPropostasReangariadas() {
	
		Date hoje = new Date();

		for(Empresa empresa : buscaEmpresas()) {
			Calendar prox = Calendar.getInstance();
			prox.setTime(empresa.getProximaAtualizacao());
			prox.add(Calendar.YEAR, -1);
			
			boolean diaExecucao = DateUtil.isMesmoDia(hoje, prox.getTime());
			boolean ajusteAtivo = empresa.isAjusteAtivo();
			boolean possuiIpca = empresa.getIpcaUltimoPeriodo() != null;
			if(ajusteAtivo && diaExecucao && possuiIpca) {
				atualizarValorPropostas(empresa, "SISTEMA");
			}
		}
	}
	
	public void atualizarValorPropostas(Empresa empresa, String nomeUsuario) {

		Transaction tx = session.beginTransaction();
		
		PropostaDAO propostaDAO = new PropostaDAO(session);
		Dao<Proposta, Integer> daoProposta = new Dao<Proposta, Integer>(session, Proposta.class);
		Dao<HistoricoCapital, Integer> daoHistoricoCapital = new Dao<HistoricoCapital, Integer>(session, HistoricoCapital.class);

		Double ajuste14_65 = empresa.getAjuste14a65();
		Double ajuste66_mais = empresa.getAjuste66mais();
		Double ajusteIpca = empresa.getIpcaUltimoPeriodo();

		List<Proposta> reangariadas = propostaDAO.getPropostasReangariadas(empresa);
		propostaDAO.populaCapital(reangariadas);
		for (Proposta proposta : reangariadas) {
			
			Double valorCapitalAntigo =  proposta.getValorCapitalSegurado();
			Double valorPremioAntigo = proposta.getValorPremioSegurado();
			Double valorCapitalConjugeAntigo = proposta.getValorCapitalConjuge();
			Double valorPremioConjugeAntigo = proposta.getValorPremioConjuge();
			
			Double valorCapitalSegurado = valorCapitalAntigo + (valorCapitalAntigo * ajusteIpca /100);
			Double valorPremioSegurado = valorPremioAntigo + (valorPremioAntigo * ajusteIpca /100);
			
			Double valorCapitalConjuge = null;
			Double valorPremioConjuge = null;
			if(proposta.getTipoProposta().getComportamento() != ComportamentoPropostaEnum.INDIVIDUAL) {
			//if(!proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL)) {
				valorCapitalConjuge = valorCapitalConjugeAntigo + (valorCapitalConjugeAntigo * ajusteIpca /100);
				valorPremioConjuge = valorPremioConjugeAntigo + (valorPremioConjugeAntigo * ajusteIpca /100);
			}
			
			if(proposta.getIdadeRealSegurado() > 65 && ajuste66_mais != null) {
				valorCapitalSegurado = valorCapitalSegurado + (valorCapitalSegurado * ajuste66_mais /100);
				valorPremioSegurado = valorPremioSegurado + (valorPremioSegurado * ajuste66_mais  /100);
				if(proposta.getTipoProposta().getComportamento() != ComportamentoPropostaEnum.INDIVIDUAL) {
				//if(!proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL)) {
					valorCapitalConjuge = valorCapitalConjuge + (valorCapitalConjuge * ajuste66_mais  /100);
					valorPremioConjuge = valorPremioConjuge + (valorPremioConjuge * ajuste66_mais  /100);
				}
			} else if (ajuste14_65 != null) {
				valorCapitalSegurado = valorCapitalSegurado + (valorCapitalSegurado * ajuste14_65  /100);
				valorPremioSegurado = valorPremioSegurado + (valorPremioSegurado * ajuste14_65  /100);
				if(proposta.getTipoProposta().getComportamento() != ComportamentoPropostaEnum.INDIVIDUAL) {
				//if(!proposta.getTipoProposta().getId().equals(Constantes.TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL)) {
					valorCapitalConjuge = valorCapitalConjuge + (valorCapitalConjuge * ajuste14_65  /100);
					valorPremioConjuge = valorPremioConjuge + (valorPremioConjuge * ajuste14_65  /100);
				}
			} else {
				//faz nada
			}
			
			proposta.setValorCapitalSegurado(valorCapitalSegurado);
			proposta.setValorPremioSegurado(valorPremioSegurado);
			proposta.setValorCapitalConjuge(valorCapitalConjuge);
			proposta.setValorPremioConjuge(valorPremioConjuge);
			
			HistoricoCapital historicoCapital = new HistoricoCapital();
			historicoCapital.setCapitalAntigo( valorCapitalAntigo );
			historicoCapital.setPremioAntigo( valorPremioAntigo );
			historicoCapital.setDataAlteracao(new Date());
			historicoCapital.setNomeUsuario( nomeUsuario);
			historicoCapital.setIdadeAntigo( "-"  );
			historicoCapital.setTipoModeloProposta( "Reangariado" );
			historicoCapital.setProposta( proposta );
			
			daoProposta.merge(proposta);
			//armazena o hist�rico
			daoHistoricoCapital.merge(historicoCapital);
		}

		// historico
		HistoricoAjusteEmpresa historico = new HistoricoAjusteEmpresa();
		historico.setAjuste14a65(ajuste14_65);
		historico.setAjuste66mais(ajuste66_mais);
		historico.setAjusteIpca(ajusteIpca);
		historico.setDataAjuste(new Date());
		historico.setNomeUsuario(nomeUsuario);
		historico.setEmpresa(empresa);
		historico.setId(null);

		Dao<HistoricoAjusteEmpresa, Integer> dao = new Dao<HistoricoAjusteEmpresa, Integer>(
				session, HistoricoAjusteEmpresa.class);
		dao.merge(historico);
		
		
		Dao<Empresa, Integer> daoEmpresa = new Dao<Empresa, Integer>(session, Empresa.class);
		//remove o �ltimo IPCA informado
		empresa.setIpcaUltimoPeriodo(null);
		daoEmpresa.merge(empresa);

		tx.commit();
		
	}
	public Empresa getEmpresa(Integer id) {
		Criteria c = this.session.createCriteria( Empresa.class );
		c.add(Restrictions.eq("id", id));
		return (Empresa)c.uniqueResult();
	}
	
	public List<Empresa> buscaEmpresas( ){
		Criteria c = this.session.createCriteria( Empresa.class );
		c.addOrder(Order.asc("nomeEmpresa"));
		return c.list();
	}
	
	public List<Empresa> buscaPeloNome(String nomeEmpresa) {
		Criteria c = this.session.createCriteria(Empresa.class);
		c.add(Restrictions.like("nomeEmpresa", nomeEmpresa, MatchMode.START));
		c.addOrder(Order.asc("nomeEmpresa"));
		return c.list();
	}
	
	public Empresa buscaUnicaPeloNome(String nomeEmpresa) {
		Criteria c = this.session.createCriteria(Empresa.class);
		c.add(Restrictions.like("nomeEmpresa", nomeEmpresa, MatchMode.START));
		c.addOrder(Order.asc("nomeEmpresa"));
		return (Empresa)c.uniqueResult();
	}
	
	public Empresa buscaUnicaPelaApolice(String numeroApolice) {
		Criteria c = this.session.createCriteria(Empresa.class);
		c.add(Restrictions.like("numApolice", numeroApolice, MatchMode.START));
		c.addOrder(Order.asc("numApolice"));
		return (Empresa)c.uniqueResult();
	}
	
	public String getNumerApolice(String nomeEmpresa) {
		Criteria c = this.session.createCriteria(Empresa.class);
		c.setProjection(Projections.property("numApolice"));
		c.add(Restrictions.like("nomeEmpresa", nomeEmpresa, MatchMode.EXACT));
		c.addOrder(Order.asc("nomeEmpresa"));
		c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		c.setMaxResults(1);
		return (String) c.uniqueResult();
	}
}
