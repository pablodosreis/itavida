package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.CapitalSeguradoLimiteIdade;
import br.com.itavida.entidades.NomeTabela;

public class CapitalSeguradoLimiteIdadeDAO {

	private Session session;
	
	public CapitalSeguradoLimiteIdadeDAO( Session session ){
		this.session = session;
	}
	
	public List<CapitalSeguradoLimiteIdade> buscaPorFiltro( CapitalSeguradoLimiteIdade f ){
		Example exemplo = Example.create(f).ignoreCase().enableLike( MatchMode.ANYWHERE );
		exemplo.excludeZeroes();
		return session.createCriteria( CapitalSeguradoLimiteIdade.class ).add( exemplo ).list();
	}
	
	
//	public List<CapitalSeguradoLimiteIdade> buscaPorFiltro( CapitalSeguradoLimiteIdade capitalFiltro ){
//		Criteria c = this.session.createCriteria( CapitalSeguradoLimiteIdade.class );
//
//
//		
//		if( capitalFiltro.getDataVigencia() != null ){
//			c.add( Restrictions.eq("dataVigencia", capitalFiltro.getDataVigencia() ) );
//		}
//		
//		if( capitalFiltro.getNomeTabela() != null && capitalFiltro.getNomeTabela().getId() != null){
//			c.createAlias("nomeTabela", "nomeTabela");
//			c.add( Restrictions.eq("nomeTabela.id", capitalFiltro.getNomeTabela().getId() ) );
//		}	
//		
//		return c.list();
//	}
	
	
	public List<CapitalSeguradoLimiteIdade> buscaPorCapital( Double capital, NomeTabela nomeTabela ){
		Criteria c = this.session.createCriteria( CapitalSeguradoLimiteIdade.class );

		c.add( Restrictions.between("funcVLMorteNatural", capital-100, capital+100  ) );
		
		if( nomeTabela != null && nomeTabela.getId() != null){
			c.createAlias("nomeTabela", "nomeTabela");
			c.add( Restrictions.eq("nomeTabela.id", nomeTabela.getId() ) );
		}	
		
		c.addOrder( Order.asc("funcVLMorteNatural") );
		c.addOrder( Order.asc("funcLimiteIdade") );
		c.addOrder( Order.asc("conjLimiteIdade") );
		
		return c.list();
	}
	
}
