package br.com.itavida.dao.base;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

import br.com.itavida.util.HibernateUtil;

public abstract class GenericDAO <T> {
	
	private Session session;
	
	private Class<T> type;
	
	public GenericDAO(Class<T> type) {
		this.type = type; 
	}
	
	
	public Session getSession() {
		this.session = HibernateUtil.currentSession();
		if (session.isOpen()) {
			return session;
		}
		return HibernateUtil.openSession();
	}
	
	public T load(Integer id) {
		return (T) getSession().get(type, id);
	}
	
	public void saveOrUpdate(T objeto) {
		getSession().saveOrUpdate(objeto);
	}
	
	public List<T> findAll() {
		Criteria c = getSession().createCriteria( type );
		c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return c.list();
	}
}
