package br.com.itavida.dao.base;

import org.hibernate.Session;

import br.com.itavida.util.HibernateUtil;

public class BaseDAO {
	
	private Session session;
	
	public Session getSession() {
		this.session = HibernateUtil.currentSession();
		if (session.isOpen()) {
			return session;
		}
		return HibernateUtil.openSession();
	}

}
