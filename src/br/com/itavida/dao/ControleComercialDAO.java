package br.com.itavida.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.ControleComercial;
import br.com.itavida.entidades.Proposta;

public class ControleComercialDAO {

	private Session session;
	
	public ControleComercialDAO( Session session ){
		this.session = session;
	}
	
	
	public List<ControleComercial> buscaPorFiltro( ControleComercial filtro ){
		Criteria c = this.session.createCriteria( ControleComercial.class );
		
		if( filtro.getOrgao() != null && filtro.getOrgao().getId() != null ){
			c.createAlias("orgao", "orgao" );
			c.add( Restrictions.eq("orgao.id", filtro.getOrgao().getId() ) );
		}

		if( filtro.getDataVisitaChegadaInicio() != null ){
			c.add( Restrictions.ge( "dataVisitaChegada", filtro.getDataVisitaChegadaInicio() ) );
		}
		
		if( filtro.getDataVisitaChegadaFim() != null ){
			c.add( Restrictions.le( "dataVisitaChegada", filtro.getDataVisitaChegadaFim() ) );
		}
		
		if( filtro.getDataVisitaSaidaInicio() != null ){
			c.add( Restrictions.ge( "dataVisitaSaida", filtro.getDataVisitaSaidaInicio() ) );
		}
		
		if( filtro.getDataVisitaSaidaFim() != null ){
			c.add( Restrictions.le( "dataVisitaSaida", filtro.getDataVisitaSaidaFim() ) );
		}
		
		if( filtro.getNumeroFuncionariosInicio() != null ){
			c.add( Restrictions.ge( "numeroFuncionarios", filtro.getNumeroFuncionariosInicio() ) );
		}
		if( filtro.getNumeroFuncionariosFim() != null && filtro.getNumeroFuncionariosFim() != 0 ){
			c.add( Restrictions.le( "numeroFuncionarios", filtro.getNumeroFuncionariosFim() ) );
		}
		
		if( filtro.getFuncionario() != null && filtro.getFuncionario().getId() != null ){
			c.createAlias("funcionario", "funcionario" );
			c.add( Restrictions.eq("funcionario.id", filtro.getFuncionario().getId() ) );
		}
		
		List<ControleComercial>  resultado = verificaQuantidadeVidasAtivas( c.list() );
		
		return  resultado;
	}
	
	public List<ControleComercial> verificaQuantidadeVidasAtivas( List<ControleComercial> controles ){

		for (Iterator iterator = controles.iterator(); iterator.hasNext();) {
			ControleComercial controleComercial = (ControleComercial) iterator.next();
			Criteria c = this.session.createCriteria( Proposta.class );
			c.createAlias("orgao", "orgao" );
			c.add( Restrictions.eq("orgao.id", controleComercial.getOrgao().getId() ) );
			c.add( Restrictions.isNull( "dataCancelamento" ) );
			c.add( Restrictions.isNotNull( "dataAprovacao" ) );
			c.setProjection( Projections.countDistinct( "id" ) );
			controleComercial.setNumeroVidasAtivas( (Integer) c.uniqueResult() );
		}
		
		return controles;
	}
	
}
