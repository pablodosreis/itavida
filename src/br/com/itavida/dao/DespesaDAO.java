package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.Despesa;

public class DespesaDAO {

	private Session session;
	
	public DespesaDAO( Session session ){
		this.session = session;
	}
	
	
	public List<Despesa> busca( String nome, String tipo ){
		Criteria c = this.session.createCriteria( Despesa.class );

		c.add( Restrictions.ilike("nomeDespesa", nome, MatchMode.ANYWHERE ) );
		if( tipo != null && !"".equals( tipo )){
			c.add( Restrictions.eq("tipoDespesa", tipo ) );
		}			
		
		c.addOrder( Order.asc("nomeDespesa") );
		
		return c.list();
	}
	
	
}
