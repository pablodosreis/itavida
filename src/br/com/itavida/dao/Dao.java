package br.com.itavida.dao;

import java.io.Serializable;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

public class Dao<T, X extends Serializable > {
	
	private static Logger logger = Logger.getLogger( Dao.class );
	private Class<T> persistentClass;
	private Session session;
	
	public Dao( Session session, Class<T> persistentClass ){
		this.session = session;
		this.persistentClass = persistentClass;
	}
	
	@SuppressWarnings("unchecked")
	public T load( X id ){
		logger.info("lendo " + persistentClass + " com id " + id );
		return ( T ) session.load( persistentClass, id );
	}
	
	@SuppressWarnings("unchecked")
	public T get( X id ){
		logger.info("lendo " + persistentClass + " com id " + id );
		return ( T ) session.get( persistentClass, id );
	}
	
	public void save( T t ){
		logger.info("salvando: " + t );
		session.save( t );
	}
	
	public void delete( T t )  {
		logger.info("deletando: " + t );
		session.delete(t);
		session.flush();
	}
	
	
	public List<T> list(){
		logger.info("Listando todos" );
		@SuppressWarnings("unchecked")
		List<T> list = session.createCriteria(persistentClass).list();
		return list;
	}
	
	public Integer countList(){
		logger.info("Listando todos" );
		return (Integer) session.createCriteria(persistentClass).setProjection( Projections.countDistinct( "id" ) ).uniqueResult();
	}
	
	public void merge(T t){
		logger.info("Salvando ou Atualizando" + t );
		session.merge(t);
	}
	
	public int retornaUltimoIdInserido( Class clazz ){
		Criteria c = this.session.createCriteria( clazz );
		c.setProjection( Projections.max("id") );
		if( c.uniqueResult() != null ){
			return (Integer) c.uniqueResult();
		}else{
			return 1;
		}
	}
	
}
