package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.HistoricoCapital;

public class HistoricoCapitalDAO {

	private Session session;
	
	public HistoricoCapitalDAO( Session session ){
		this.session = session;
	}
	
	
	public List<HistoricoCapital> buscaPorProposta( Integer idProposta ){
		Criteria c = this.session.createCriteria( HistoricoCapital.class );
		c.createAlias("proposta", "proposta");
		c.add( Restrictions.eq("proposta.id", idProposta ) );
		return c.list();
	}
	
	
}
