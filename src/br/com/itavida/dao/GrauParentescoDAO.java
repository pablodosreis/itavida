package br.com.itavida.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.itavida.entidades.GrauParentesco;

public class GrauParentescoDAO {

	private Session session;
	
	public GrauParentescoDAO( Session session ){
		this.session = session;
	}
	
	public List<GrauParentesco> buscaPorNomePopUp( String descricao ){
		Criteria c = this.session.createCriteria( GrauParentesco.class );

		c.add( Restrictions.ilike("descricao", descricao, MatchMode.ANYWHERE ) );
		
		return c.list();
	}
}
