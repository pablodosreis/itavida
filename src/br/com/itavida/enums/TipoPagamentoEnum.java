package br.com.itavida.enums;

public enum TipoPagamentoEnum {

	BOLETO(0, "Boleto"), 
	DEBITO_EM_CONTA(1, "D�bito em Conta"), 
	CARTAO_CREDITO(2,"Cart�o de Cr�dito");
	
	private Integer id;

	private String nome;

	private TipoPagamentoEnum(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public static TipoPagamentoEnum get(Integer id) {
		switch (id) {
		case 0:
			return BOLETO;
		case 1:
			return DEBITO_EM_CONTA;
		case 2:
			return CARTAO_CREDITO;
		default:
			return null;
		}
	}
}
