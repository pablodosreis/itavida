package br.com.itavida.enums;

public enum CartaoCreditoEnum {

	VISA(0, "Visa"), 
	MASTER(1, "Master Card"), 
	AMEX(2,"Amex");
	
	private Integer id;

	private String nome;

	private CartaoCreditoEnum(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public static CartaoCreditoEnum get(Integer id) {
		switch (id) {
		case 0:
			return VISA;
		case 1:
			return MASTER;
		case 2:
			return AMEX;
		default:
			return null;
		}
	}
}
