package br.com.itavida.enums;

public enum SituacaoProposta {

	ATIVA(0, "Ativa"), 
	CANCELADA(1, "Cancelada"), 
	INADIMPLENTE(2,"Inadimplente"),
	AGUARDANDO_AVERBACAO(3,"Aguardando Averba��o"),
	PENDENTE(4,"Pendente"),
	NAO_AVERBADO(5, "N�o Averbada");

	private Integer id;

	private String nome;

	private SituacaoProposta(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public static SituacaoProposta get(Integer id) {
		switch (id) {
		case 0:
			return ATIVA;
		case 1:
			return CANCELADA;
		case 2:
			return INADIMPLENTE;
		case 3:
			return AGUARDANDO_AVERBACAO;
		case 4:
			return PENDENTE;
		case 5:
			return NAO_AVERBADO;
		default:
			return null;
		}
	}

}
