package br.com.itavida.enums;

public enum SituacaoBoleto {

	CANCELADO(0, "Cancelado"),
	NAO_GERADO(1, "N�o Gerado"), 
	GERADO_EM_ABERTO(2, "Gerado em aberto"), 
	PAGO(3,"Pago"),
	EM_ATRASO(4,"Em atraso");
	

	private Integer id;

	private String nome;

	private SituacaoBoleto(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public static SituacaoBoleto get(Integer id) {
		if (id == null) return null;
		switch (id) {
		case 0:
			return CANCELADO;
		case 1:
			return NAO_GERADO;
		case 2:
			return GERADO_EM_ABERTO;
		case 3:
			return PAGO;
		case 4:
			return EM_ATRASO;
		default:
			return null;
		}
	}

}
