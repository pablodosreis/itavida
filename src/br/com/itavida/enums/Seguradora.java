package br.com.itavida.enums;


public enum Seguradora {
 
 	T(0, "TOKIO MARINE"), 
	M(1, "Cancelada"), 
	I(2,"Inadimplente"),
	O(3,"ICATU SEGUROS");

	private Integer id;

	private String nome;

	private Seguradora(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

}
