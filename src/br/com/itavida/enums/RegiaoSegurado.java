package br.com.itavida.enums;

public enum RegiaoSegurado {

	AC(1, "Acre"),
	AP(3, "Amap�"),
	AM(4, "Amazonas"),
	ES(8, "Esp�rito Santo"),
	MG(13, "Minas Gerais"),
	RO(22, "Rond�nia"),
	RR(23, "Roraima"),
	VARIOS_ESTADOS(-1, "V�rios Estados");
	
	private Integer id;
	
	private String nome;
	
	private RegiaoSegurado(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	
}
