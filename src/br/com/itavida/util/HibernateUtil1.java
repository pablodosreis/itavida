package br.com.itavida.util;

import org.apache.log4j.Logger;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.stat.Statistics;

public class HibernateUtil1 {


	public static SessionFactory sessionFactory;

	public static Session sessao;

	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure()
					.buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("A cria��o da SessionFactory inicial falhou."
					+ ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static final ThreadLocal<Session> session = new ThreadLocal<Session>();

	public static Session currentSession() throws HibernateException {
		if (sessao == null) {
			SessionFactory sessionFactory = null;

			// Get the session factory we can use for persistence
			sessionFactory = new AnnotationConfiguration().configure()
					.buildSessionFactory();
			sessionFactory.evictQueries();
			// Ask for a session using the JDBC information we've configured
			sessao = sessionFactory.openSession();
			sessao.setFlushMode(FlushMode.ALWAYS);
		}
		return sessao;
	}

	public static void closeSession() throws HibernateException {
		Session s = (Session) session.get();
		if (s != null)
			s.close();
		session.set(null);
		sessao.close();
		sessao = null;
	}	
}
