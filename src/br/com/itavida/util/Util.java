package br.com.itavida.util;

import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class Util {

	private static final int[] pesoCNPJ = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
	
	
	public static String primeiroNome(String nomeCompleto) {
		if (StringUtils.isNotEmpty(nomeCompleto)) {
			return nomeCompleto.split(" ")[0];
		}
		return "";
	}
	
	public static boolean isEmpty(String valor) {
		return valor == null || valor.isEmpty(); 
	}
	
	public static String formatarMonetario(Double valor) {
		Locale locale = new Locale("pt", "BR");      
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
		return currencyFormatter.format(valor);
	}
	
	public static String formatar(Double valor) {
		if (valor == null) return null;
		return String.format(new Locale("pt", "BR"), "%.2f", valor);
	}
	
	public static String formatar1casa(double valor) {
		return String.format(new Locale("pt", "BR"), "%.1f", valor);
	}
	
	public static String formatar8casas(double valor) {
		return String.format(new Locale("pt", "BR"), "%.8f", valor);
	}
	
	/**
	 * Remove toda a acentua��o da string substituindo por caracteres simples sem acento.
	 */
	public static String unaccent(String src) {
        String normalized = Normalizer
                .normalize(src, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
        char[] array = normalized.toCharArray();
        String retorno = "";
        for (int i = 0; i < array.length; i++) {
            char b = array[i];
            if(Character.isLetter(b)){
                retorno+=b;
            }
        }
        return retorno;
    }
	
	public static String codificarEmail(String email) {
		String emailCodificado = email;
		if(email.contains("@")) {
			String emailHead = email.substring(0, email.indexOf("@"));
			String emailTail = email.substring(email.indexOf("@"));
			if(emailHead.length() >= 4 ) {
				emailHead = emailHead.substring(0, 4) + "...";
			} else {
				emailHead = emailHead + "..";
			}
			emailCodificado = emailHead + emailTail;
		}		
		return emailCodificado;
	}
	
	public static Double string2Double(String valor) {
		try {
			//valor = valor.replace(".", "");
			valor = valor.replace(",", ".");
			return Double.parseDouble(valor);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Double string2DoubleIgnoreEmpty(String valor) {
		
		if (valor == null || valor.isEmpty()) {
			return null;
		}
		
		try {
			//valor = valor.replace(".", "");
			valor = valor.replace(",", ".");
			return Double.parseDouble(valor);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static boolean string2Boolean(String valor, boolean padrao) {
		
		if (valor == null || valor.isEmpty()) {
			return padrao;
		}
		
		if (valor.toLowerCase().startsWith("n�o") || valor.toLowerCase().startsWith("nao") || valor.toLowerCase().startsWith("0")) {
			return false;
		}
		return true;
	}
	
	public static String conversorDiaSemana(Integer dia){
		switch(dia){
			case 1:
				return "Domingo";
			case 2:
				return "Segunda";
			case 3:
				return "Ter�a";
			case 4:
				return "Quarta";
			case 5:
				return "Quinta";
			case 6:
				return "Sexta";
			case 7:
				return "S�bado";
			default:
				return "INDEFINIDO";
		}
	}
	
	public static String calculaDigitoMod11(String dado, int numDig, int limMult, boolean x10) {

	    int mult, soma, i, n, dig;
	    
	    if(!x10) numDig = 1;
	    for(n=1; n<=numDig; n++){
	        soma = 0;
	        mult = 2;
	        for(i=dado.length() - 1; i >= 0; i--){
	            soma += (mult * Integer.parseInt(dado.substring(i, i + 1)));
	            if(++mult > limMult) mult = 2;
	        }
	        if(x10){
	            dig = ((soma * 10) % 11) % 10;
	        } else {
	            dig = soma % 11;
	        }
	        if(dig == 10){
	        	dado += "X";
	        } else {
	        	dado += String.valueOf(dig);
	        }
	    }
	    return dado.substring(dado.length() - numDig, dado.length());
	}
	
	/*Calculo de distancia*/
	public static double distance(double lat1, double lat2, double lon1, double lon2,
	        double el1, double el2) {
	 
	    final int R = 6371; // Radius of the earth
	 
	    Double latDistance = deg2rad(lat2 - lat1);
	    Double lonDistance = deg2rad(lon2 - lon1);
	    Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
	            + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
	            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	    Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distance = R * c * 1000; // convert to meters
	 
	    double height = el1 - el2;
	    distance = Math.pow(distance, 2) + Math.pow(height, 2);
	    return Math.sqrt(distance);
	}
	 
	public static double deg2rad(double deg) {
	    return (deg * Math.PI / 180.0);
	}
	
	public static String prepararLinha(String conteudo) {
		return "\n" + conteudo;
	}
	
	public static String repeteTexto(String texto, int quantidade) {
		StringBuilder retorno = new StringBuilder();
		for (int i = 1; i < quantidade; i++) {
			retorno.append(texto);
		}
		return retorno.toString();
	}
	
	public static boolean isCodigoClienteValido(String codigo) {
		int digito = 0;
		int numero = 0;
		int digitoCalculado = 0;
		if (codigo.length() > 2) {
			try {
				digito = Integer.parseInt(codigo.substring(codigo.length()-1, codigo.length()));
				numero = Integer.parseInt(codigo.substring(0, codigo.length()-1));
			} catch(Exception e) {
				return false;
			}
		} else {
			return false;
		}
		digitoCalculado = ((numero+17)*17)%10; 
		return digitoCalculado == digito; 
	}
	
	private static int calcularDigito(String str, int[] peso) {
	      int soma = 0;
	      for (int indice=str.length()-1, digito; indice >= 0; indice-- ) {
	         digito = Integer.parseInt(str.substring(indice,indice+1));
	         soma += digito*peso[peso.length-str.length()+indice];
	      }
	      soma = 11 - soma % 11;
	      return soma > 9 ? 0 : soma;
	   }
	
	public static boolean isCNPJValido(String cnpj) {		
	      if ((cnpj==null)||(cnpj.length()!=14)) return false;
	      Integer digito1 = calcularDigito(cnpj.substring(0,12), pesoCNPJ);
	      Integer digito2 = calcularDigito(cnpj.substring(0,12) + digito1, pesoCNPJ);
	      return cnpj.equals(cnpj.substring(0,12) + digito1.toString() + digito2.toString());
	}
	
	 public static boolean isCPFValido(String CPF) {
		// considera-se erro CPF's formados por uma sequencia de numeros iguais
		    if (CPF.equals("00000000000") || CPF.equals("11111111111") ||
		        CPF.equals("22222222222") || CPF.equals("33333333333") ||
		        CPF.equals("44444444444") || CPF.equals("55555555555") ||
		        CPF.equals("66666666666") || CPF.equals("77777777777") ||
		        CPF.equals("88888888888") || CPF.equals("99999999999") ||
		       (CPF.length() != 11))
		       return(false);

		    char dig10, dig11;
		    int sm, i, r, num, peso;

		// "try" - protege o codigo para eventuais erros de conversao de tipo (int)
		    try {
		// Calculo do 1o. Digito Verificador
		      sm = 0;
		      peso = 10;
		      for (i=0; i<9; i++) {              
		// converte o i-esimo caractere do CPF em um numero:
		// por exemplo, transforma o caractere '0' no inteiro 0         
		// (48 eh a posicao de '0' na tabela ASCII)         
		        num = (int)(CPF.charAt(i) - 48); 
		        sm = sm + (num * peso);
		        peso = peso - 1;
		      }

		      r = 11 - (sm % 11);
		      if ((r == 10) || (r == 11))
		         dig10 = '0';
		      else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

		// Calculo do 2o. Digito Verificador
		      sm = 0;
		      peso = 11;
		      for(i=0; i<10; i++) {
		        num = (int)(CPF.charAt(i) - 48);
		        sm = sm + (num * peso);
		        peso = peso - 1;
		      }

		      r = 11 - (sm % 11);
		      if ((r == 10) || (r == 11))
		         dig11 = '0';
		      else dig11 = (char)(r + 48);

		// Verifica se os digitos calculados conferem com os digitos informados.
		      if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
		         return(true);
		      else return(false);
		    } catch (InputMismatchException erro) {
		        return(false);
		    }
		  }
	 
	 public static boolean validarEmail(String email)
	    {
	        boolean isEmailIdValid = false;
	        if (email != null && email.length() > 0) {
	            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
	            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
	            Matcher matcher = pattern.matcher(email);
	            if (matcher.matches()) {
	                isEmailIdValid = true;
	            }
	        }
	        return isEmailIdValid;
	    }
	 

	 public static String getIdentificadorMomento() {
		 return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	 }
	 
	 public static String getMomento() {
		 return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	 }
	 
	 public static Integer cep2Int(String cep) {
		 if (cep == null) {
			 return null;
		 }
		 
		 Integer retorno;
		 cep = cep.trim();
		 cep = cep.replace("-", "");
		 
		 try {
			 return Integer.parseInt(cep);
		 } catch (NumberFormatException e) {
			 return null;
		 }
		 
	 }
	 
	 public static boolean isASCII (String linha) {
		 	linha = linha.toLowerCase();
			Pattern padrao = Pattern.compile("[([a-z]|[0-9]|.|-|-|'|(|)|:|_|=|/|%|)]+");
			Matcher pesquisa = padrao.matcher(linha);
			if (pesquisa.matches()) {
				return true;
			}
			else {
				return false;
			}
	}
	 
	public static String removerFormatacao(String texto) {
		if (texto == null)  return "";
		return texto.replace(".", "").replace("(", "").replace(")", "").replace("-", "").replace(" ", "").replace("/", "").replace("\\", "");
		
	}
	
	public static String getVariavelAmbiente(String chave, String valorDefault) {
		
		String valor = System.getenv(chave); 
		System.out.println("Valor get env: " + chave + ":" + valor);
		
		if (StringUtils.isEmpty(valor)) {
			valor = System.getProperty(chave);
			System.out.println("Valor getProperty: " + chave + ":" + valor);
		}
		 
		if (StringUtils.isEmpty(valor)) {
			valor = valorDefault;
			System.out.println("Valor default: " + chave + ":" + valor);
		}
	 
		return valor;
	}

}
