package br.com.itavida.util;


public class Constantes {
	
	public static final String SIM = "S";
	public static final String NAO = "N";

	// Mensagens de Valida��o
	public static final String KEY_VALIDACAO_CAMPO_OBRIGATORIO = "error.required";
	public static final String KEY_VALIDACAO_CAMPO_MAXLENGTH = "error.maxlength"; 
	public static final String KEY_VALIDACAO_PREENCHIMENTO_LISTA_SELECAO = "error.preenchimento.listaSelecao";
	public static final String KEY_VALIDACAO_INCLUSAO_OBRIGATORIA="error.inclusao.obrigatoria";
	public static final String KEY_VALIDACAO_CAMPO1_MAIORQ_CAMPO2="error.campo1.maiorQue.campo2";	
	public static final String KEY_VALIDACAO_CAMPO_DEVE_SER_PREENCHIDO="error.campo.deve.ser.preenchido";	
	public static final String KEY_VALIDACAO_EMPRESA_VINCULADA_PROPOSTAS="erros.empresa.vinculada.proposta";	
	public static final String KEY_VALIDACAO_TABELA_SELECIONADA="error.nome.tabela.inexistente";	
	public static final String KEY_VALIDACAO_DATA_INICIO_MAIOR_ATUAL="error.data.fator.maior.atual";
	public static final String KEY_VALIDACAO_FATOR_EXISTENTE_PROCESSADO="error.fator.existente.processado";
	public static final String KEY_VALIDACAO_FATOR_PROCESSADO="error.fator.processado";
	



	
	
	//Mensagens informativas
	public static final String KEY_MENSAGEM_SUCESSO = "mensagem.sucesso";
	public static final String KEY_MENSAGEM_OPERACAO_SUCESSO = "mensagem.operacao.sucesso";
	public static final String KEY_MENSAGEM_PROPOSTA_NAO_ENCONTRADA="error.proposta.nao.encontrada";
	public static final String KEY_MENSAGEM_EXCLUSAO_SUCESSO = "mensagem.exclusao.sucesso";
	
	public static final String KEY_MENSAGEM_ALERTA_PESQUISA_VAZIA="warn.lista.vazia";
	
	public static final Integer MAX_REGISTROS_RETORNADOS = 50;
	
	//PROPOSTA
	
	//Modelos
	public static final Integer MODELO_PROPOSTA_VALOR_FIXO = 1;
	public static final Integer MODELO_PROPOSTA_FAIXA_ETARIA = 2;
	public static final Integer MODELO_PROPOSTA_FAIXA_REANGARIADO = 3;
	public static final Integer MODELO_PROPOSTA_ATUALIZADO = 4;
	
	/*
	//Tipo proposta valor fixo
	public static final Integer TIPO_PROPOSTA_VALOR_FIXO_INDIVIDUAL = 1;
	public static final Integer TIPO_PROPOSTA_VALOR_FIXO_CASADO_50 = 2;
	public static final Integer TIPO_PROPOSTA_VALOR_FIXO_CASADO_100 = 3;
	//Tipo proposta Faixa etaria
	public static final Integer TIPO_PROPOSTA_FAIXA_ETARIA_INDIVIDUAL = 4;
	public static final Integer TIPO_PROPOSTA_FAIXA_ETARIA_CASADO = 5;
	//Tipo proposta reangariado
	public static final Integer TIPO_PROPOSTA_REANGARIADO_INDIVIDUAL = 6;
	public static final Integer TIPO_PROPOSTA_REANGARIADO_CASADO_50 = 7;
	public static final Integer TIPO_PROPOSTA_REANGARIADO_CASADO_100 = 8;
	
	//Tipo proposta atualizado
	public static final Integer TIPO_PROPOSTA_ATUALIZADO_INDIVIDUAL = 9;
	public static final Integer TIPO_PROPOSTA_ATUALIZADO_CASADO_50 = 10;
	public static final Integer TIPO_PROPOSTA_ATUALIZADO_CASADO_100 = 11;*/
		
	
	//Tipo Percentual Beneficiario
	public static final Integer TIPO_PERCENTUAL_PARTES_IGUAIS= 1;
	public static final Integer TIPO_PERCENTUAL_VALOR_INFORMADO = 2;
	
	//Formas de c�lculo para c�lculo da comiss�o
	public static final String CALCULAR_AUTOMATICAMENTE = "A";
	public static final String DEFINIR_VALOR_PAGAR = "D";
	public static final String COMISSAO_ADICIONAL = "C";
	
	//Tipos de funcion�rio
	public static final String FUNCIONARIO_AUTONOMO = "A";
	
	public static final String COMANDO_INCLUSAO = "Inclus�o";
	public static final String COMANDO_ALTERACAO = "Altera��o";
	
	//Relat�rio Quita��o
	public static final int QUANTIDADE_MES_AVALIADOS = 3;
	
	public static String SISVIDA_FILES = "/sisvidafiles/";
	public static String SISVIDA_LOGS = "/opt/logs/";
}
