package br.com.itavida.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.itavida.entidades.Usuario;
import br.com.itavida.handlers.UsuarioHandler;

public class HibernateSessionFilter implements Filter {
    private final static String FILTER_APPLIED = "_security_filter_applied"; 
    
	public void destroy() {

	}

	
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain fc)
			throws IOException, ServletException {

	      	HttpServletRequest hreq = (HttpServletRequest) req;  
	        HttpServletResponse hresp = (HttpServletResponse) res;  
	        HttpSession session = hreq.getSession();  
	  
	        hreq.getPathInfo();  
	        String paginaAtual = new String(hreq.getRequestURL());  
	        
	        /*redireciona o acesso do caminho server:7070/itavida para a p�gina de login*/
	        if(paginaAtual.endsWith("itavida/") || paginaAtual.endsWith("sisvida/") || paginaAtual.endsWith(".com/") || paginaAtual.endsWith(".br/")) {
	        	hresp.sendRedirect(hreq.getContextPath() + "/login.jsf");
                return;
	        }
	        
	        // dont filter login.jsp because otherwise an endless loop.  
	        // & only filter .jsp otherwise it will filter all images etc as well.  
	        if ((req.getAttribute(FILTER_APPLIED) == null) && paginaAtual != null  
	                && (!paginaAtual.endsWith("login.jsf")
	                && (!paginaAtual.contains("dadosSegurado.jsf")))
	                && (paginaAtual.endsWith(".jsf"))
	                ) {  
	        	req.setAttribute(FILTER_APPLIED, Boolean.TRUE);  
	  
	            // If the session bean is not null get the session bean property  
	            // username.  
	            Usuario user = null;  
	            if (((UsuarioHandler) session  
	                    .getAttribute("usuarioHandler")) != null) {  
	                 user =  
	                 ((UsuarioHandler)session.getAttribute("usuarioHandler")).getUsuario();  
	            }  
	  
	            if ((user == null) || (user.getId() == null )) {  
	                hresp.sendRedirect(hreq.getContextPath() + "/login.jsf");  
	                return;  
	            }  
	  
	        }
		
		
		HibernateUtil.openSession();
		try {
			HibernateUtil.currentSession().beginTransaction();
			fc.doFilter(req, res);
			HibernateUtil.currentSession().getTransaction().commit();
		} catch (Exception e) {
			throw new ServletException(e);

		} finally {
			HibernateUtil.closeCurrentSession();
		}

	}

	public void init(FilterConfig arg0) throws ServletException {

	}

}
