package br.com.itavida.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * M�todos utilit�rios para manipula��o e convers�o de datas.
 * 
 * 
 */
public class DateUtil {
 
    
	public static void zerarHorarMinutoSegundo(Calendar calendar) {
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
	}
	
	public static void zerarDiaHoraMinutoSegundo(Calendar calendar) {
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1, 0, 0, 0);
	}
	
	public static Date getData(int dia, int mes, int ano) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(ano, mes-1, dia, 0, 0, 0);
		return calendar.getTime();
	}
	
    /**
     * Retorna uma data formatada de acordo com o formato informado.
     * 
     * @param date
     *            the date
     * @param pattern
     *            the pattern
     * @return the formatted date
     */
    public static String getFormattedDate( final Date date, final String pattern ) {
    	if (date == null) return "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( );
        simpleDateFormat.applyPattern( pattern );
        String dateString = simpleDateFormat.format( date );
        return dateString;
    }
    
    /**
     * Obt�m a data no formato desejado.
     * 
     * @param dateString
     *            String contendo uma data.
     * @param pattern
     *            Padr�o para formata��o da data.
     * @return o objeto data formatado.
     * @throws ParseException
     *             erro na formata��o da data.
     */
    public static Date getDate( final String dateString, final String pattern )
            throws ParseException {
        Date date = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( );
        simpleDateFormat.applyPattern( pattern );
        date = simpleDateFormat.parse( dateString );
        return date;
    }
    
    public static boolean isMesmoDia(Date d1, Date d2 ) {
    	
    	Calendar cMaior = Calendar.getInstance();
    	cMaior.setTime(d1);
    	Calendar cMenor = Calendar.getInstance();
    	cMenor.setTime(d2);
    	
    	boolean mDia = cMaior.get(Calendar.DAY_OF_MONTH) == cMenor.get(Calendar.DAY_OF_MONTH);
    	boolean mMes = cMaior.get(Calendar.MONTH) == cMenor.get(Calendar.MONTH);
    	boolean mAno = cMaior.get(Calendar.YEAR) == cMenor.get(Calendar.YEAR);
    	
    	return mDia && mMes && mAno;
    }
    
    /**
     * Calcula a diferen�a em dias entre duas datas.
     * 
     */
    public static int diffEmDias(Date dataMaior, Date dataMenor ) {
    	
    	if(isMesmoDia(dataMaior, dataMenor)) {
    		return 0;
    	}
    	
    	long dt1 = dataMaior.getTime();  
        long dt2 = dataMenor.getTime();
        long dt = dt1 - dt2;
        return (int) ((dt / 86400000) + 1);  	
    }
    
    public static Date getHoje() {
    	return new Date();
    }
    
    public static Date getAmanha() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DAY_OF_MONTH, 1);
    	return calendar.getTime();
    }
    
    public static Date getMesesAnteriores(int meses) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.MONTH, -meses);
    	return calendar.getTime();
    }
    
    public static Date getOntem() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DAY_OF_MONTH, -1);
    	return calendar.getTime();
    }
    
    public static Date getUltimaDiaMes(Date date) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	calendar.add(Calendar.MONTH,1);
    	calendar.add(Calendar.DAY_OF_MONTH,-1);
    	return calendar.getTime();
    }
    
    public static Date getPrimeiroDiaMes(Date date) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	calendar.set(Calendar.DAY_OF_MONTH, 1);
    	zerarDiaHoraMinutoSegundo(calendar);
    	return calendar.getTime();
    }
}
