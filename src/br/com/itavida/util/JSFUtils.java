package br.com.itavida.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class JSFUtils {

	/**
	 * Util para bundle no sistema	
	 * @param bundleName
	 * @param key
	 * @param params
	 * @param locale
	 * @return
	 */
	public static String getMessageResourceString( String bundleName, String key, Object params[] ){
		
		String text = null;
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		ResourceBundle bundle = ResourceBundle.getBundle( bundleName, locale, getCurrentClassLoader() );
		
		try{
			text = bundle.getString(key);			
		}catch (MissingResourceException e) {
			text = "?? Chave " + key + " n�o encontrada. ??"; 
		}
		
		if( params != null ){
			MessageFormat mf = new MessageFormat( text, locale );
			text = mf.format( params, new StringBuffer(), null ).toString();
		}
		
		return text;
	}
	
	/**
	 * Adquire o class loader corrente
	 * @param defaultObject
	 * @return
	 */
	protected static ClassLoader getCurrentClassLoader(){
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if( loader == null ){
			loader = FacesContext.getCurrentInstance().getClass().getClassLoader();
		}
		
		return loader;
	}
}
