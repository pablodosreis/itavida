package br.com.itavida.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Carteiro {
	
	private static Logger LOG = Logger.getLogger("Carteiro");

	
	public void enviarSincrono(final String nomeRemetente, final String destinatario, final String assunto, final String conteudo) throws MessagingException, UnsupportedEncodingException {
		Thread threadEmail = new Thread() {
			@Override
			public void run() {
					enviar(nomeRemetente, destinatario, assunto, conteudo);
			}
		};
		
		threadEmail.start();
	}
	
	
	public void enviar(String nomeRemetente, String destinatario, String assunto, String conteudo){
		
		try {
			nomeRemetente = nomeRemetente == null ? "Sisvida" : nomeRemetente;
			
			Properties props = getProperties();		
			
			Session session = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
									"AKIAJLKMIFWQBSAHUWZA", "AidqLUp0V5cwMw8eWuSgQATnMLJXPpZl14x1J4X6Q3QZ");
						}
					});
	
			Message message = new MimeMessage(session);
			
				message.setFrom(new InternetAddress("contato@appclientefiel.com.br", nomeRemetente));
			 
	
			// Destinatário(s)
			Address[] toUser = InternetAddress.parse(destinatario);
	
			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(assunto);// Assunto
			message.setContent(conteudo, "text/html; charset=utf-8");
			message.setSentDate(new Date());
			
			/** Método para enviar a mensagem criada */
			Transport.send(message);
		
		} catch (UnsupportedEncodingException e) {
			LOG.log(Level.SEVERE, "Erro ao tentar enviar email", e);
		} catch (MessagingException e) {
			LOG.log(Level.SEVERE, "Erro ao tentar enviar email", e);			
		}
	}

	private Properties getProperties() {
		Properties props = new Properties();
		
		
		
		/*props.put("mail.smtp.host", "smtp.zoho.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",e
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");*/
		
		props.put("mail.smtp.host", "email-smtp.us-east-1.amazonaws.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		
		return props;
	}
	
	public static void main(String[] args) throws MessagingException, UnsupportedEncodingException {
		Carteiro carteiro = new Carteiro();
		carteiro.enviar("Eu", "pablodosreis@gmail.com", "TEste", "TEste");	
	}
	
	
}
