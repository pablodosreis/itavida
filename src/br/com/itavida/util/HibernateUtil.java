package br.com.itavida.util;

import org.apache.log4j.Logger;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.stat.Statistics;

public class HibernateUtil {

	private static Logger logger = Logger.getLogger( HibernateUtil.class );
	private static SessionFactory factory;
	private static ThreadLocal<Session> sessions = new ThreadLocal<Session>();
	
	static {
		Configuration conf = new AnnotationConfiguration();
		conf.configure();
		
		factory = conf.buildSessionFactory();
	}
	
	public static Session openNewSession() {
		return factory.openSession();
	}
	
	public static Session openSession(){
		if( sessions.get() != null ){
			logger.error("Tem uma sess�o j� associada pra essa tread");
			//grave, pois algu�m n�o fechou uma j� aberta
		}
		sessions.set( factory.openSession() );
		return sessions.get();
	}
	
	public static void closeCurrentSession(){
		sessions.get().close();
		sessions.set(null);
	}
	
	public static Session currentSession(){
		return sessions.get();
	}
	
	public static Session getSession(){
		
		logger.info("Abrindo nova sess�o");
		return factory.openSession();
		

	}
	
	public static Statistics getStatistics( ){
		return factory.getStatistics();
	}


}