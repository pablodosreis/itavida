package br.com.itavida.util;

public class File {

    private String Name;
    private String mime;
    private long length;
    private byte[] data;
    public byte[] getData() {
        return data;
    }
    
    private java.io.File ioFile;
    
    public void setData(byte[] data) {
        this.data = data;
    }
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
        int extDot = name.lastIndexOf('.');
        if(extDot > 0){
            String extension = name.substring(extDot +1);
            if("xls".equals(extension)){
                mime="excel/document";
            } else {
                mime = "excel/unknown";
            }
        }
    }
    public long getLength() {
        return length;
    }
    public void setLength(long length) {
        this.length = length;
    }
    
    public String getMime(){
        return mime;
    }
	public java.io.File getIoFile() {
		return ioFile;
	}
	public void setIoFile(java.io.File ioFile) {
		this.ioFile = ioFile;
	}
    
    
}