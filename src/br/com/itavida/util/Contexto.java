package br.com.itavida.util;

import org.hibernate.Session;

import br.com.itavida.dao.MovimentacaoFinanceiraDAO;
import br.com.itavida.dao.PropostaDAO;

public class Contexto {

	private static MovimentacaoFinanceiraDAO movimentacaoFinanceiraDAO;
	
	private static PropostaDAO propostaDAO;
	
	public static Session getSession() {
		return HibernateUtil.currentSession();
	}
	
	public static MovimentacaoFinanceiraDAO getMovimentacaoFinanceiraDAO() {
		if (movimentacaoFinanceiraDAO == null) {
			movimentacaoFinanceiraDAO = new MovimentacaoFinanceiraDAO();
		}
		return movimentacaoFinanceiraDAO;
	}
	
	public static PropostaDAO getPropostaDAO() {
		if (propostaDAO == null) {
			propostaDAO = new PropostaDAO(getSession());
		}
		return new PropostaDAO(getSession());
	}
	
}
