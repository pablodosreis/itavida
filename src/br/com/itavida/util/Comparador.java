package br.com.itavida.util;

import java.util.Date;

public class Comparador {

	private Comparador() {}
	
	public static boolean houveAlteracao(Object valorInicial, Object valorAtual) {
		if(valorInicial == null && valorAtual == null) {
			return false;
		}
		if((valorInicial == null && valorAtual != null) || (valorAtual == null && valorInicial != null)) {
			return true;
		}
		if(valorInicial instanceof Date && valorAtual instanceof Date) {
			Date dtInicio = (Date) valorInicial;
			Date dtFim = (Date) valorAtual;
			return dtInicio.compareTo(dtFim) != 0;
		}
		return !valorInicial.equals(valorAtual);
	}
}
