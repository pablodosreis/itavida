package br.com.itavida.util;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Pablo Reis
 * 
 */
public class CpfCnpjUtils {

	/**
	 * Construtor privado: Classe utilit�ria.
	 */
	private CpfCnpjUtils() {
	}
	
	/**
	 * Inicializa��o da vari�vel est�tica CNPJ_DIGITS.
	 */
	public static final int CNPJ_DIGITS = 14;
	/**
	 * Inicializa��o da vari�vel est�tica CNPJ_MASK.
	 */
	public static final String CNPJ_MASK = "##.###.###/####-##";
	/**
	 * Inicializa��o da vari�vel est�tica CPF_DIGITS.
	 */
	public static final int CPF_DIGITS = 11;
	/**
	 * Inicializa��o da vari�vel est�tica CPF_MASK.
	 */
	public static final String CPF_MASK = "###.###.###-##";

	/**
	 * M�todo que retorna o cpfcnpj.
	 * 
	 * @param number
	 *            String
	 * @return number.
	 */
	public static String getCpfCnpj(String number) {
		
		if (StringUtils.isEmpty(number)) {
			return null;
		}
		
		number = number.replaceAll("\\.", "").replaceAll("-", "");
		if (number != null) {
			if (isCpf(number)) {
				return number.replaceAll("([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})", "$1\\.$2\\.$3-$4");
			} else {
				return number.replaceAll("([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})",
						"$1\\.$2\\.$3/$4-$5");
			}
		} else {
			return null;
		}
	}

	/**
	 * M�todo boolean para cnpj.
	 * 
	 * @param number
	 *            String
	 * @return number.
	 */
	public static boolean isCnpj(String number) {
		return number != null && number.length() == CNPJ_DIGITS;
	}

	/**
	 * M�todo booleano para cpf.
	 * 
	 * @param number
	 *            String
	 * @return number.
	 */
	public static boolean isCpf(String number) {
		return number != null && number.length() == CPF_DIGITS;
	}

}
