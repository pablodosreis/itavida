package br.com.itavida.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PropostaDTO implements Serializable {

	private Integer id;
	
	private String lotacao;

	private String bairro;

	private String cep;

	private String complemento;

	private String cpfConjuge;

	private String cpfSegurado;

	private Date dataAprovacao;

	private Date dataCadastro;

	private Date dataCancelamento;

	private Date dataContemplacao;

	private Date dataNascimentoConjuge;

	private Date dataNascimentoSegurado;


	private String enderecoSegurado;
	
	private String numeroEnderecoSegurado;

	private String estadoCivilConjuge;

	private String estadoCivilSegurado;

	private String matriculaConjuge;

	private String matriculaSegurado;

	private String nomeConjuge;

	private String nomeSegurado;

	private String observacao;

	private String rgConjuge;

	private String rgSegurado;

	private String sexoConjuge;

	private String sexoSegurado;
	
	private String profissaoConjuge;

	private String profissaoSegurado;

	private String telefoneCelular;

	private String telefoneComercial;

	private String telefoneCelular2;

	private String telefoneResidencial;
	
	private Date dataAgenciamento;
	
	private Date dataAlteracao;
	
	private Date dataExclusao;
	
	private String nomeUsuarioAlteracao;
	
	private String email;
	
	
	/*Preencher manual*/
	private String txtSituacaoProposta;

	private String txtCidade;
	
	private String txtSiglaUF;
	
	private String txtApolice;
	
	private String txtModeloProposta;
	
	private String txtTipoProposta;

	private String txtOrgao;
	
	private String txtMotivoCancelamento;
	
	private String numeroSorteio;
	
	private List<BeneficiarioDTO> beneficiarios;
	
	private List<AgenciadorDTO> agenciadores;
	
	private List<SinistroDTO> sinistros;
	
	
	
	/*private Double valorPremioSegurado;
	
	private Double valorPremioConjuge;
	
	private Double valorCapitalSegurado;
	
	private Double valorCapitalConjuge;
	
	private String comandoComissao;*/
	
	
	private Double capitalSegurado;
	private Double capitalConjuge;
	private Double premioConjuge;
	private Double premioSegurado;	
	private Double totalPremios;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLotacao() {
		return lotacao;
	}
	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getCpfConjuge() {
		return cpfConjuge;
	}
	public void setCpfConjuge(String cpfConjuge) {
		this.cpfConjuge = cpfConjuge;
	}
	public String getCpfSegurado() {
		return cpfSegurado;
	}
	public void setCpfSegurado(String cpfSegurado) {
		this.cpfSegurado = cpfSegurado;
	}
	public Date getDataAprovacao() {
		return dataAprovacao;
	}
	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Date getDataCancelamento() {
		return dataCancelamento;
	}
	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}
	public Date getDataContemplacao() {
		return dataContemplacao;
	}
	public void setDataContemplacao(Date dataContemplacao) {
		this.dataContemplacao = dataContemplacao;
	}
	public Date getDataNascimentoConjuge() {
		return dataNascimentoConjuge;
	}
	public void setDataNascimentoConjuge(Date dataNascimentoConjuge) {
		this.dataNascimentoConjuge = dataNascimentoConjuge;
	}
	public Date getDataNascimentoSegurado() {
		return dataNascimentoSegurado;
	}
	public void setDataNascimentoSegurado(Date dataNascimentoSegurado) {
		this.dataNascimentoSegurado = dataNascimentoSegurado;
	}
	public String getEnderecoSegurado() {
		return enderecoSegurado;
	}
	public void setEnderecoSegurado(String enderecoSegurado) {
		this.enderecoSegurado = enderecoSegurado;
	}
	public String getNumeroEnderecoSegurado() {
		return numeroEnderecoSegurado;
	}
	public void setNumeroEnderecoSegurado(String numeroEnderecoSegurado) {
		this.numeroEnderecoSegurado = numeroEnderecoSegurado;
	}
	public String getEstadoCivilConjuge() {
		return estadoCivilConjuge;
	}
	public void setEstadoCivilConjuge(String estadoCivilConjuge) {
		this.estadoCivilConjuge = estadoCivilConjuge;
	}
	public String getEstadoCivilSegurado() {
		return estadoCivilSegurado;
	}
	public void setEstadoCivilSegurado(String estadoCivilSegurado) {
		this.estadoCivilSegurado = estadoCivilSegurado;
	}
	public String getMatriculaConjuge() {
		return matriculaConjuge;
	}
	public void setMatriculaConjuge(String matriculaConjuge) {
		this.matriculaConjuge = matriculaConjuge;
	}
	public String getMatriculaSegurado() {
		return matriculaSegurado;
	}
	public void setMatriculaSegurado(String matriculaSegurado) {
		this.matriculaSegurado = matriculaSegurado;
	}
	public String getNomeConjuge() {
		return nomeConjuge;
	}
	public void setNomeConjuge(String nomeConjuge) {
		this.nomeConjuge = nomeConjuge;
	}
	public String getNomeSegurado() {
		return nomeSegurado;
	}
	public void setNomeSegurado(String nomeSegurado) {
		this.nomeSegurado = nomeSegurado;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getRgConjuge() {
		return rgConjuge;
	}
	public void setRgConjuge(String rgConjuge) {
		this.rgConjuge = rgConjuge;
	}
	public String getRgSegurado() {
		return rgSegurado;
	}
	public void setRgSegurado(String rgSegurado) {
		this.rgSegurado = rgSegurado;
	}
	public String getSexoConjuge() {
		return sexoConjuge;
	}
	public void setSexoConjuge(String sexoConjuge) {
		this.sexoConjuge = sexoConjuge;
	}
	public String getSexoSegurado() {
		return sexoSegurado;
	}
	public void setSexoSegurado(String sexoSegurado) {
		this.sexoSegurado = sexoSegurado;
	}
	public String getProfissaoConjuge() {
		return profissaoConjuge;
	}
	public void setProfissaoConjuge(String profissaoConjuge) {
		this.profissaoConjuge = profissaoConjuge;
	}
	public String getProfissaoSegurado() {
		return profissaoSegurado;
	}
	public void setProfissaoSegurado(String profissaoSegurado) {
		this.profissaoSegurado = profissaoSegurado;
	}
	public String getTelefoneCelular() {
		return telefoneCelular;
	}
	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}
	public String getTelefoneComercial() {
		return telefoneComercial;
	}
	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}
	public String getTelefoneCelular2() {
		return telefoneCelular2;
	}
	public void setTelefoneCelular2(String telefoneCelular2) {
		this.telefoneCelular2 = telefoneCelular2;
	}
	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}
	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}
	public Date getDataAgenciamento() {
		return dataAgenciamento;
	}
	public void setDataAgenciamento(Date dataAgenciamento) {
		this.dataAgenciamento = dataAgenciamento;
	}
	public Date getDataAlteracao() {
		return dataAlteracao;
	}
	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}
	public Date getDataExclusao() {
		return dataExclusao;
	}
	public void setDataExclusao(Date dataExclusao) {
		this.dataExclusao = dataExclusao;
	}
	public String getNomeUsuarioAlteracao() {
		return nomeUsuarioAlteracao;
	}
	public void setNomeUsuarioAlteracao(String nomeUsuarioAlteracao) {
		this.nomeUsuarioAlteracao = nomeUsuarioAlteracao;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTxtSituacaoProposta() {
		return txtSituacaoProposta;
	}
	public void setTxtSituacaoProposta(String txtSituacaoProposta) {
		this.txtSituacaoProposta = txtSituacaoProposta;
	}
	public String getTxtCidade() {
		return txtCidade;
	}
	public void setTxtCidade(String txtCidade) {
		this.txtCidade = txtCidade;
	}
	public String getTxtSiglaUF() {
		return txtSiglaUF;
	}
	public void setTxtSiglaUF(String txtSiglaUF) {
		this.txtSiglaUF = txtSiglaUF;
	}
	public String getTxtApolice() {
		return txtApolice;
	}
	public void setTxtApolice(String txtApolice) {
		this.txtApolice = txtApolice;
	}
	public String getTxtModeloProposta() {
		return txtModeloProposta;
	}
	public void setTxtModeloProposta(String txtModeloProposta) {
		this.txtModeloProposta = txtModeloProposta;
	}
	public String getTxtTipoProposta() {
		return txtTipoProposta;
	}
	public void setTxtTipoProposta(String txtTipoProposta) {
		this.txtTipoProposta = txtTipoProposta;
	}
	public String getTxtOrgao() {
		return txtOrgao;
	}
	public void setTxtOrgao(String txtOrgao) {
		this.txtOrgao = txtOrgao;
	}
	public String getTxtMotivoCancelamento() {
		return txtMotivoCancelamento;
	}
	public void setTxtMotivoCancelamento(String txtMotivoCancelamento) {
		this.txtMotivoCancelamento = txtMotivoCancelamento;
	}
	public List<BeneficiarioDTO> getBeneficiarios() {
		return beneficiarios;
	}
	public void setBeneficiarios(List<BeneficiarioDTO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}
	public List<AgenciadorDTO> getAgenciadores() {
		return agenciadores;
	}
	public void setAgenciadores(List<AgenciadorDTO> agenciadores) {
		this.agenciadores = agenciadores;
	}
	public List<SinistroDTO> getSinistros() {
		return sinistros;
	}
	public void setSinistros(List<SinistroDTO> sinistros) {
		this.sinistros = sinistros;
	}
	public Double getCapitalSegurado() {
		return capitalSegurado;
	}
	public void setCapitalSegurado(Double capitalSegurado) {
		this.capitalSegurado = capitalSegurado;
	}
	public Double getCapitalConjuge() {
		return capitalConjuge;
	}
	public void setCapitalConjuge(Double capitalConjuge) {
		this.capitalConjuge = capitalConjuge;
	}
	public Double getPremioConjuge() {
		return premioConjuge;
	}
	public void setPremioConjuge(Double premioConjuge) {
		this.premioConjuge = premioConjuge;
	}
	public Double getPremioSegurado() {
		return premioSegurado;
	}
	public void setPremioSegurado(Double premioSegurado) {
		this.premioSegurado = premioSegurado;
	}
	public Double getTotalPremios() {
		return totalPremios;
	}
	public void setTotalPremios(Double totalPremios) {
		this.totalPremios = totalPremios;
	}
	public String getNumeroSorteio() {
		return numeroSorteio;
	}
	public void setNumeroSorteio(String numeroSorteio) {
		this.numeroSorteio = numeroSorteio;
	}
	
	
	
	
	
	
	
	
	
}
