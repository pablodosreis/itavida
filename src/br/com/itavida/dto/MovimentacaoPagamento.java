package br.com.itavida.dto;

public class MovimentacaoPagamento {

	private String segurado;
	private String periodo;
	private Double valor;
	private String pago;
	private String quitacao;
	
	public String getSegurado() {
		return segurado;
	}
	public String getPeriodo() {
		return periodo;
	}
	public Double getValor() {
		return valor;
	}
	public String getQuitacao() {
		return quitacao;
	}
	public void setSegurado(String segurado) {
		this.segurado = segurado;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setQuitacao(String quitacao) {
		this.quitacao = quitacao;
	}
	public String getPago() {
		return pago;
	}
	public void setPago(String pago) {
		this.pago = pago;
	}
	
	
	
	
}
