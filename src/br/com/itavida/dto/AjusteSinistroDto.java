package br.com.itavida.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AjusteSinistroDto implements Comparable<AjusteSinistroDto> {

	public Date inicio;
	
	public Date fim;
	
	public Integer empresaId;
	
	public String empresaNome;
	
	public Integer propostaId;

	@Override
	public String toString() {
		String inicioF = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(inicio);
		String fimF = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(fim);
		return inicioF + "\t" +
			   fimF + "\t" +
				empresaNome + "\t" +
			   propostaId;
	
	}

	public int compareTo(AjusteSinistroDto o) {
		 if(propostaId.compareTo(o.propostaId) != 0) {
			 return propostaId.compareTo(o.propostaId);
		 }
		return inicio.compareTo(o.inicio) ;
	}
	
	
	
}
