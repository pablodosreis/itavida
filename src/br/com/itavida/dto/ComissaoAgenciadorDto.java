package br.com.itavida.dto;

import java.util.Date;

public class ComissaoAgenciadorDto implements Comparable<ComissaoAgenciadorDto>{

	private String proposta;
	
	private String comando;
	
	private String nomeSegurado;
	
	private String nomeOrgao;
	
	private Date dataAverbacao;
	
	private String situacaoAverbacao;
	
	private String situacaoAtual;
	
	private Double comissaoAdicional;
	
	private Double percentual;
	
	private Double valorPago;
	
	private Double taxaCarregamento;
	
	private Double valorTotalPremio;
	
	private String tipoCalculoUtilizado;
	
	private Double valorProlabore;
	
	private Double valorTaxa;
	
	private Double valorComissao;
	
	private Double valorDescontado;
	
	private Double baseCalculo;

	
//	public Double getValorComissao() {
//		//<h:outputText value="#{ (item.propostaagenciador_collection[0].funcionario.percentualComissao * item.totalPremios)/100 - (item.orgao.valorProlabore + item.orgao.valorTaxa ) }" />
//		return ( valorTotalPremio- 2) * 0.5;
//	}
	
	public Double getValorDescontado() {
		return valorDescontado;
	}

	public void setValorDescontado(Double valorDesconto) {
		this.valorDescontado = valorDesconto;
	}

	public Double getValorComissao() {
		return valorComissao;
	}

	public void setValorComissao(Double valorComissao) {
		this.valorComissao = valorComissao;
	}

	public String getProposta() {
		return proposta;
	}

	public void setProposta(String proposta) {
		this.proposta = proposta;
	}

	public String getNomeSegurado() {
		return nomeSegurado;
	}

	public void setNomeSegurado(String nomeSegurado) {
		this.nomeSegurado = nomeSegurado;
	}

	public String getNomeOrgao() {
		return nomeOrgao;
	}

	public void setNomeOrgao(String nomeOrgao) {
		this.nomeOrgao = nomeOrgao;
	}

	public Date getDataAverbacao() {
		return dataAverbacao;
	}

	public void setDataAverbacao(Date dataAverbacao) {
		this.dataAverbacao = dataAverbacao;
	}

	public String getSituacaoAverbacao() {
		return situacaoAverbacao;
	}

	public void setSituacaoAverbacao(String situacaoAverbacao) {
		this.situacaoAverbacao = situacaoAverbacao;
	}

	public String getSituacaoAtual() {
		return situacaoAtual;
	}

	public void setSituacaoAtual(String situacaoAtual) {
		this.situacaoAtual = situacaoAtual;
	}

	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {
		this.valorPago = valorPago;
	}

	public Double getTaxaCarregamento() {
		return taxaCarregamento;
	}

	public void setTaxaCarregamento(Double taxaCarregamento) {
		this.taxaCarregamento = taxaCarregamento;
	}

	public Double getValorTotalPremio() {
		return valorTotalPremio;
	}

	public void setValorTotalPremio(Double valorTotalPremio) {
		this.valorTotalPremio = valorTotalPremio;
	}

	public String getTipoCalculoUtilizado() {
		return tipoCalculoUtilizado;
	}

	public void setTipoCalculoUtilizado(String tipoCalculoUtilizado) {
		this.tipoCalculoUtilizado = tipoCalculoUtilizado;
	}

	public Double getComissaoAdicional() {
		return comissaoAdicional;
	}

	public void setComissaoAdicional(Double comissaoAdicional) {
		this.comissaoAdicional = comissaoAdicional;
	}

	public Double getValorProlabore() {
		return valorProlabore;
	}

	public void setValorProlabore(Double valorProlabore) {
		this.valorProlabore = valorProlabore;
	}

	public Double getValorTaxa() {
		return valorTaxa;
	}

	public void setValorTaxa(Double valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public String getComando() {
		return comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	public Double getBaseCalculo() {
		return baseCalculo;
	}

	public void setBaseCalculo(Double baseCalculo) {
		this.baseCalculo = baseCalculo;
	}

	public int compareTo(ComissaoAgenciadorDto o) {
		if(this.getDataAverbacao() == null || o.getDataAverbacao() == null) {
			return 0;
		}
		return this.getDataAverbacao().compareTo(o.getDataAverbacao());
	}
	
	
	
}
