package br.com.itavida.dto;

public class LinhaMensagem {

	private String linha;
	
	private String mensagem;
	
	public LinhaMensagem(String linha, String mensagem) {
		super();
		this.linha = linha;
		this.mensagem = mensagem;
	}
	
	public LinhaMensagem(Integer linha, String mensagem) {
		super();
		linha++;
		this.linha = linha.toString();
		this.mensagem = mensagem;
	}

	public String getLinha() {
		return linha;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public String toString() {
		return linha + ": "+ mensagem ;
	}
	
	
}
