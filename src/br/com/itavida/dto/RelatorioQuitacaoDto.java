package br.com.itavida.dto;

import java.util.Date;

public class RelatorioQuitacaoDto {

	private Date dataReferencia;
	
	private String dataQuitacao;

	public RelatorioQuitacaoDto(Date dataReferencia, String dataQuitacao) {
		super();
		this.dataReferencia = dataReferencia;
		this.dataQuitacao = dataQuitacao;
	}

	public Date getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(Date dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	public String getDataQuitacao() {
		return dataQuitacao;
	}

	public void setDataQuitacao(String dataQuitacao) {
		this.dataQuitacao = dataQuitacao;
	}
	
		
}
