package br.com.itavida.dto;


public class PagamentoMensalDTO {
	
	private String cpfSegurado;
	private String nomeSegurado;
	private String orgao;
	private String dataNascimento;
	private String tipoProposta;
	private String nomeConjugue;
	private String nascimentoConjugue;
	private String cpfConjugue;
	private String enderecoSegurado;
	private String cidadeEstadoSegurado;
	private String cep;
	private Double capitalSegurado;
	private Double capitalConjugue;
	private Double valorFuncionario;
	private Double valorConjugue;
	private String quitacao;
	private Integer mes;
	private Integer ano;
	
	private static final String []meses = {"Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro",
		"Outubro", "Novembro", "Dezembro"};
	
	
	public double getValorTotal(){
		return (valorFuncionario == null?0:valorFuncionario)+(valorConjugue == null?0:valorConjugue);
	}
	
	public String getDescricaoMes(){
		return mes == null?"":meses[mes-1];//mes come�a em 1
	}

	public String getCpfSegurado() {
		return cpfSegurado;
	}

	public void setCpfSegurado(String cpfSegurado) {
		this.cpfSegurado = cpfSegurado;
	}

	public String getNomeSegurado() {
		return nomeSegurado;
	}

	public void setNomeSegurado(String nomeSegurado) {
		this.nomeSegurado = nomeSegurado;
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}


	public String getTipoProposta() {
		return tipoProposta;
	}

	public void setTipoProposta(String tipoProposta) {
		this.tipoProposta = tipoProposta;
	}

	public String getNomeConjugue() {
		return nomeConjugue;
	}

	public void setNomeConjugue(String nomeConjugue) {
		this.nomeConjugue = nomeConjugue;
	}

	public String getCpfConjugue() {
		return cpfConjugue;
	}

	public void setCpfConjugue(String cpfConjugue) {
		this.cpfConjugue = cpfConjugue;
	}

	public Double getCapitalSegurado() {
		return capitalSegurado;
	}

	public void setCapitalSegurado(Double capitalSegurado) {
		this.capitalSegurado = capitalSegurado;
	}

	public Double getCapitalConjugue() {
		return capitalConjugue;
	}

	public void setCapitalConjugue(Double capitalConjugue) {
		this.capitalConjugue = capitalConjugue;
	}

	public Double getValorFuncionario() {
		return valorFuncionario;
	}

	public void setValorFuncionario(Double valorFuncionario) {
		this.valorFuncionario = valorFuncionario;
	}

	public Double getValorConjugue() {
		return valorConjugue;
	}

	public void setValorConjugue(Double valorConjugue) {
		this.valorConjugue = valorConjugue;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public String getQuitacao() {
		return quitacao;
	}

	public void setQuitacao(String quitacao) {
		this.quitacao = quitacao;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public static String[] getMeses() {
		return meses;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNascimentoConjugue() {
		return nascimentoConjugue;
	}

	public void setNascimentoConjugue(String nascimentoConjugue) {
		this.nascimentoConjugue = nascimentoConjugue;
	}

	public String getEnderecoSegurado() {
		return enderecoSegurado;
	}

	public void setEnderecoSegurado(String enderecoSegurado) {
		this.enderecoSegurado = enderecoSegurado;
	}

	public String getCidadeEstadoSegurado() {
		return cidadeEstadoSegurado;
	}

	public void setCidadeEstadoSegurado(String cidadeEstadoSegurado) {
		this.cidadeEstadoSegurado = cidadeEstadoSegurado;
	}
	
}