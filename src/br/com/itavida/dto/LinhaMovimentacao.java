package br.com.itavida.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.itavida.entidades.MovimentacaoFinanceira;
import br.com.itavida.entidades.Proposta;
import br.com.itavida.util.DateUtil;

public class LinhaMovimentacao implements Comparable<LinhaMovimentacao>{

	private Proposta proposta;
	
	private String anoMes;
	
	private Integer parcela;
	
	private List<MovimentacaoFinanceira> movimentacoes;

	public LinhaMovimentacao(Proposta proposta, Integer parcela, Date referencia) {
		super();
		this.proposta = proposta;
		this.parcela = parcela;
		this.anoMes = DateUtil.getFormattedDate(referencia, "MM/yyyy");
	}
	
	public LinhaMovimentacao(Proposta proposta, Integer parcela, Date referencia, List<MovimentacaoFinanceira> movimentacoes) {
		super();
		this.anoMes = DateUtil.getFormattedDate(referencia, "MM/yyyy");
		this.proposta = proposta;
		this.parcela = parcela;
		this.movimentacoes = movimentacoes;
	}

	public Proposta getProposta() {
		return proposta;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}

	public Integer getParcela() {
		return parcela;
	}

	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public List<MovimentacaoFinanceira> getMovimentacoes() {
		return movimentacoes;
	}

	public void setMovimentacoes(List<MovimentacaoFinanceira> moviementacoes) {
		this.movimentacoes = moviementacoes;
	}
	
	

	public String getAnoMes() {
		return anoMes;
	}

	public void setAnoMes(String anoMes) {
		this.anoMes = anoMes;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((proposta == null) ? 0 : proposta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinhaMovimentacao other = (LinhaMovimentacao) obj;
		if (proposta == null) {
			if (other.proposta != null)
				return false;
		} else if (!proposta.equals(other.proposta))
			return false;
		return true;
	}

	public int compareTo(LinhaMovimentacao o) {
		if (!this.getProposta().getCpfSegurado().equals(o.getProposta().getCpfSegurado())) {
			return this.getProposta().getNomeSegurado().compareTo(o.getProposta().getNomeSegurado());
		}
		return 0;
	}
	
	public List<MovimentacaoFinanceira> getMovimentacoes(String chaveMesAno) {
		List<MovimentacaoFinanceira>  retorno = new ArrayList<MovimentacaoFinanceira>();
		for (MovimentacaoFinanceira movimentacaoFinanceira : movimentacoes) {
			if (movimentacaoFinanceira.getDataPagamentoComissao() != null) {
				String dataAnoMes = DateUtil.getFormattedDate(movimentacaoFinanceira.getDataPagamentoComissao(), "MM-yyyy");
				if (dataAnoMes.equals(chaveMesAno)) {
					retorno.add(movimentacaoFinanceira);
				}
			}
		}
		return retorno;
	}
	

}
