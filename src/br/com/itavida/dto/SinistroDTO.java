package br.com.itavida.dto;

import java.util.Date;

public class SinistroDTO {

	private Date dataEnvioSeguradora;

	private Date dataPagamento;

	private Date dataSinistro;

	private String numeroSinistro;
	
	private Double valorSinistro;
	
	private Double valorPagoSinistro;
	
	private String observacao;

	public Date getDataEnvioSeguradora() {
		return dataEnvioSeguradora;
	}

	public void setDataEnvioSeguradora(Date dataEnvioSeguradora) {
		this.dataEnvioSeguradora = dataEnvioSeguradora;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Date getDataSinistro() {
		return dataSinistro;
	}

	public void setDataSinistro(Date dataSinistro) {
		this.dataSinistro = dataSinistro;
	}

	public String getNumeroSinistro() {
		return numeroSinistro;
	}

	public void setNumeroSinistro(String numeroSinistro) {
		this.numeroSinistro = numeroSinistro;
	}

	public Double getValorSinistro() {
		return valorSinistro;
	}

	public void setValorSinistro(Double valorSinistro) {
		this.valorSinistro = valorSinistro;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Double getValorPagoSinistro() {
		return valorPagoSinistro;
	}

	public void setValorPagoSinistro(Double valorPagoSinistro) {
		this.valorPagoSinistro = valorPagoSinistro;
	}

	
	
	
	
}
