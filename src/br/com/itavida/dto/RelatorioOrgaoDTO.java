package br.com.itavida.dto;

public class RelatorioOrgaoDTO {

	private Long id;
	private String uf;
	private String nome;
	private String endereco;
	private Long vidasPendentes;
	private Long vidasAtivas;
	private Long vidasInadimplentes;
	private Long totalVidas;
	private Double totalPremios;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @return the endereco
	 */
	public String getEndereco() {
		return endereco;
	}
	/**
	 * @return the vidasPendentes
	 */
	public Long getVidasPendentes() {
		return vidasPendentes;
	}
	/**
	 * @return the vidasAtivas
	 */
	public Long getVidasAtivas() {
		return vidasAtivas;
	}
	/**
	 * @return the totalVidas
	 */
	public Long getTotalVidas() {
		return totalVidas;
	}
	/**
	 * @return the totalPremios
	 */
	public Double getTotalPremios() {
		return totalPremios;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @param uf the uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	/**
	 * @param vidasPendentes the vidasPendentes to set
	 */
	public void setVidasPendentes(Long vidasPendentes) {
		this.vidasPendentes = vidasPendentes;
	}
	/**
	 * @param vidasAtivas the vidasAtivas to set
	 */
	public void setVidasAtivas(Long vidasAtivas) {
		this.vidasAtivas = vidasAtivas;
	}
	/**
	 * @param totalVidas the totalVidas to set
	 */
	public void setTotalVidas(Long totalVidas) {
		this.totalVidas = totalVidas;
	}
	/**
	 * @param totalPremios the totalPremios to set
	 */
	public void setTotalPremios(Double totalPremios) {
		this.totalPremios = totalPremios;
	}
	/**
	 * @return the vidasInadimplentes
	 */
	public Long getVidasInadimplentes() {
		return vidasInadimplentes;
	}
	/**
	 * @param vidasInadimplentes the vidasInadimplentes to set
	 */
	public void setVidasInadimplentes(Long vidasInadimplentes) {
		this.vidasInadimplentes = vidasInadimplentes;
	}
	
	
}
