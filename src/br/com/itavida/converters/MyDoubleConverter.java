package br.com.itavida.converters;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

public class MyDoubleConverter implements Converter {

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorTela) throws ConverterException {

		if(valorTela == null || valorTela.toString().trim().equals("")){
			return null;

		} else {
			valorTela = valorTela.replaceAll(Pattern.quote("."), "");

			try{
				NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));
				nf.setMaximumFractionDigits(2);

				return nf.parse(valorTela).doubleValue();

			}catch (Exception e) {
				return 0.0d;
			}
		}
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object valorTela) throws ConverterException {

		if(valorTela == null || valorTela.toString().trim().equals("")){
			return "";

		} else {
			NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);

			return nf.format(Double.valueOf(valorTela.toString()));
		}
	}
}
