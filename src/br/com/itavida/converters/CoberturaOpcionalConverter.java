package br.com.itavida.converters;



import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.entidades.CoberturaOpcionalSuperAp;
import br.com.itavida.util.HibernateUtil;



public class CoberturaOpcionalConverter implements Converter {

    
    public Object getAsObject(FacesContext context, UIComponent component, String value) {                

        if(value == null || value.isEmpty()){
            return null;
        }else{
        	Session session = HibernateUtil.getSession();
        	Dao<CoberturaOpcionalSuperAp, Integer> coberturaDAO = new Dao<CoberturaOpcionalSuperAp, Integer>(session, CoberturaOpcionalSuperAp.class);
            Integer id = Integer.parseInt(value);
            CoberturaOpcionalSuperAp cobertura = coberturaDAO.load(id);
            System.out.println(cobertura.getDescricao());
            return cobertura;
        }

    }

    
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        CoberturaOpcionalSuperAp unidade = (CoberturaOpcionalSuperAp)value;
        if(unidade != null){
            return String.valueOf(unidade.getId());
        }else{
            return null;
        }

    }

}
