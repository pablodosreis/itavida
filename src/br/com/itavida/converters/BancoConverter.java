package br.com.itavida.converters;



import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.hibernate.Session;

import br.com.itavida.dao.Dao;
import br.com.itavida.entidades.Banco;
import br.com.itavida.util.HibernateUtil;



public class BancoConverter implements Converter {

    
    public Object getAsObject(FacesContext context, UIComponent component, String value) {                

        if(value == null || value.isEmpty()){
            return null;
        }else{
        	Session session = HibernateUtil.getSession();
        	Dao<Banco, Integer> dao = new Dao<Banco, Integer>(session, Banco.class);
            Integer id = Integer.parseInt(value);
            Banco objeto = dao.load(id);
            return objeto;
        }

    }

    
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Banco objeto = (Banco)value;
        if(objeto != null){
            return String.valueOf(objeto.getId());
        }else{
            return null;
        }

    }

}
