package br.com.itavida.exceptions;

import java.util.ArrayList;
import java.util.List;

public class SistemaException extends RuntimeException {

	private List<String> erros;
	
	public SistemaException(String message) {
		super(message);
		erros = new ArrayList<String>();
	}

	public List<String> getErros() {
		return erros;
	}

	public void setErros(List<String> erros) {
		this.erros = erros;
	}
	
	
}
