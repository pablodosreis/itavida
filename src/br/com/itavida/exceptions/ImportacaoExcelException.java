package br.com.itavida.exceptions;

public class ImportacaoExcelException extends Exception {

	public ImportacaoExcelException(String message) {
		super(message);
	}
}
