alter table empresa add column aniversario date null;
alter table empresa add column ajuste_ativo boolean not null default false;
alter table empresa add column ajuste_14_65 double null;
alter table empresa add column ajuste_66_mais double null;
alter table empresa add column ipca_ultimo_periodo double null;

create table historicoajusteempresa (
  id int unsigned not null AUTO_INCREMENT,
  ajuste_14_65 double null,
  ajuste_66_mais double null,
  ajuste_ipca double null,
  dataAjuste datetime not null,
  nomeUsuario varchar(255) not null,
  idempresa int unsigned,
  constraint pk_historicoajusteempresa primary key (id),
  constraint fk_empresa_historicoajuste foreign key (idempresa) references empresa (id)	
);

/*Alterações na proposta*/
alter table proposta add column valorPremioSegurado double null;
alter table proposta add column valorPremioConjuge double null;
alter table proposta add column valorCapitalSegurado double null;
alter table proposta add column valorCapitalConjuge double null;
insert into modeloproposta(id, descricao) values(3, 'Reangariado');
insert into tipoproposta values (6, 3, 'Individual');
insert into tipoproposta values (7, 3, 'Casado 50%');
insert into tipoproposta values (8, 3, 'Casado 100%');
alter table proposta add column valorReangariado double;
alter table proposta add column email varchar(255);


/*remove obrigatoriedade do campo nomeTabela em proposta*/
ALTER TABLE `proposta` DROP FOREIGN KEY `FK_proposta_nomeTabela` ;
ALTER TABLE `proposta` CHANGE COLUMN `nomeTabela` `nomeTabela` INT(10) UNSIGNED NULL  , 
  ADD CONSTRAINT `FK_proposta_nomeTabela`
  FOREIGN KEY (`nomeTabela` )
  REFERENCES `nometabela` (`id`);
    
ALTER TABLE `itavida`.`historicoproposta` DROP FOREIGN KEY `FK_proposta_nomeTabela_hp` ;
ALTER TABLE `itavida`.`historicoproposta` CHANGE COLUMN `nomeTabela` `nomeTabela` INT(10) UNSIGNED NULL  , 
  ADD CONSTRAINT `FK_proposta_nomeTabela_hp`
  FOREIGN KEY (`nomeTabela` )
  REFERENCES `itavida`.`nometabela` (`id` );

