delimiter $$

DROP TABLE IF EXISTS propostaquitacao $$

CREATE TABLE `propostaquitacao` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `proposta` int(10) unsigned NOT NULL,
  `datareferencia` date DEFAULT NULL,
  `valortotal` double DEFAULT NULL,
  `empresa` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PropostaQuitacaoPropostaa_idx` (`proposta`),
  KEY `FK_PropostaQuitacaoEmpresa_idx` (`empresa`),
  CONSTRAINT `FK_PropostaQuitacaoProposta` FOREIGN KEY (`proposta`) REFERENCES `proposta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PropostaQuitacaoEmpresa` FOREIGN KEY (`empresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$



delimiter $$

DROP TABLE IF EXISTS quitacao $$

CREATE TABLE `quitacao` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `quitado` tinyint(1) NOT NULL DEFAULT '0',
  `propostaquitacao` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_QuitacaoPropostaQuitacao_idx` (`propostaquitacao`),
  CONSTRAINT `FK_QuitacaoPropostaQuitacao` FOREIGN KEY (`propostaquitacao`) REFERENCES `propostaquitacao` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

delimiter ;