insert into modeloproposta(id, descricao) values (4, 'Atualizado');
insert into tipoproposta(id,modeloProposta, descricao) values(9,4,'Individual');
insert into tipoproposta(id,modeloProposta, descricao) values(10,4,'Casado 50%');
insert into tipoproposta(id,modeloProposta, descricao) values(11,4,'Casado 100%');
alter table historicoCapital add column nomeSegurado varchar(255) null;

CREATE TABLE `itavida`.`competencia` (
  `id_competencia` INT NOT NULL AUTO_INCREMENT,
  `mes` INT NULL,
  `ano` INT NULL,
  `proposta_id` BIGINT NOT NULL,
  `valor_segurado` DOUBLE NULL,
  `valor_conjugue` DOUBLE NULL,
  PRIMARY KEY (`id_competencia`));
