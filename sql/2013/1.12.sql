
alter table tipoproposta add column comportamentoProposta tinyint null;

-- individual
update tipoProposta set comportamentoProposta = 0 where id in (1,4,6,9);
-- casado
update tipoProposta set comportamentoProposta = 1 where id in (5);
-- casado 50%
update tipoProposta set comportamentoProposta = 2 where id in (2,7,10);
-- casado 100%
update tipoProposta set comportamentoProposta = 3 where id in (3,8,11);

insert into tipoProposta(id, modeloProposta, descricao, comportamentoProposta) values(12,2, 'Senior', 0);
insert into tipoProposta(id, modeloProposta, descricao, comportamentoProposta) values(13,2, 'PM Individual', 0);
insert into tipoProposta(id, modeloProposta, descricao, comportamentoProposta) values(14,2, 'PM Casado', 1);