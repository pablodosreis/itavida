CREATE TABLE `itavida`.`competencia` (
  `id_competencia` INT NOT NULL AUTO_INCREMENT,
  `mes` INT NULL,
  `ano` INT NULL,
  `proposta_id` BIGINT NOT NULL,
  `valor_segurado` DOUBLE NULL,
  `valor_conjugue` DOUBLE NULL,
  PRIMARY KEY (`id_competencia`));
