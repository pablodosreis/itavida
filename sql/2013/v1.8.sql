alter table proposta add column valorFuturoPremioSegurado double null;
alter table proposta add column valorFuturoPremioConjuge double null;
alter table proposta add column valorFuturoCapitalSegurado double null;
alter table proposta add column valorFuturoCapitalConjuge double null;

alter table proposta add column detalheCapitalFaixaEtariaFuturo int(10) unsigned null;
alter table proposta add constraint fk_detalheCapitalFaixaEtariaFuturo
	foreign key (detalheCapitalFaixaEtariaFuturo) references detalhecapitalfaixaetaria (id);
	
	
alter table proposta add column dataUltimaAtualizacaoSituacao datetime null;
alter table proposta add column comandoComissao tinyint default 0;