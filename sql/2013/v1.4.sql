alter table historicosinistro add column nomeBeneficiario VARCHAR(200);
alter table historicosinistro add column valorSinistro double;
alter table historicosinistro add column valorFuneral double;
alter table detalhecapitalfaixaetaria add column dataAjuste date;
create table fator (
    id int unsigned not null AUTO_INCREMENT,
	dataInicio date not null, 
	fator double not null, 
	idcapitalseguradofaixaetaria int unsigned not null,
	processado boolean not null,
	constraint pk_fator primary key (id),
	constraint fk_fator_capfaixaetaria foreign key (idcapitalseguradofaixaetaria) references capitalseguradofaixaetaria (id)
);
create table historicoalteracaodetalhecapitalfaixaEtaria (id int unsigned not null auto_increment primary key);
alter table historicoalteracaodetalhecapitalfaixaEtaria add column idDetalheCapitalFaixaEtaria int unsigned not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column titularValorAntigo double not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column somaTitularConjugValorAntigo double not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column capitalSeguradoValorAntigo double not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column dataAjuste date not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column titularValorAjustado double not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column somaTitularConjugValorAjustado double not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column capitalSeguradoValorAjustado double not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add column idFator int unsigned not null;
alter table historicoalteracaodetalhecapitalfaixaEtaria add constraint fk_hist_detalhecapitalfaixaEtaria
	foreign key (idDetalheCapitalFaixaEtaria) references detalhecapitalfaixaetaria (id);
alter table historicoalteracaodetalhecapitalfaixaEtaria add constraint fk_hist_fator
	foreign key (idFator) references fator (id);

/*Cria e atualiza situacao das propostas*/
alter table proposta add column situacao varchar(40) not null default 'ATIVA';
update proposta set situacao = 'ATIVA' where situacao <> 'ATIVA' and dataCancelamento is null and dataAprovacao is not null;
update proposta set situacao = 'NAO_AVERBADA' where situacao <> 'NAO_AVERBADA' and dataCancelamento is null and dataAprovacao is null;
update proposta set situacao = 'INADIMPLENTE' where situacao <> 'INADIMPLENTE' and motivoCancelamento = 12 and dataCancelamento is not null and TIMESTAMPDIFF(MONTH, dataCancelamento, curdate()) <= 2;
update proposta set situacao = 'CANCELADA' where situacao <> 'CANCELADA' and dataCancelamento is not null and (motivoCancelamento <> 12 or motivoCancelamento is null or (motivoCancelamento = 12 and TIMESTAMPDIFF(MONTH, dataCancelamento, curdate()) > 2));

ALTER TABLE historicosinistro DROP COLUMN nomeBeneficiario;

CREATE TABLE beneficiariosinistro (
	id int unsigned not null AUTO_INCREMENT,
	historicoSinistro int unsigned not null,
	nomeBeneficiario VARCHAR(200),
	valorRecebido double not null,
	constraint pk_beneficiariosinistro primary key (id),
	constraint fk_beneficiario_historico foreign key (historicoSinistro) references historicosinistro (id)
);

/*
Consulta para verificar se existem propostas associadas a empresa incorreta.
A consulta n�o deve retornar nenhum registro quando todas as propostas estiverem corretamente associadas �s empresas.
 
select p.id, cfe.id, nt.id, nt.nomeTabela, empresa, dataCancelamento
from proposta p
left join capitalseguradofaixaetaria cfe on cfe.id = p.capitalSeguradoFaixaEtaria
left join nometabela nt on nt.id = cfe.nomeTabela
where p.empresa = 1
and nt.id in (37,38,40,41,42,43,44,45,46,47,48,51,52,54,94,95,100);
*/

/* Atualiza as propostas Tokio(T) associadas a empresa incorreta (D) */
update proposta p
left join capitalseguradofaixaetaria cfe on cfe.id = p.capitalSeguradoFaixaEtaria
left join nometabela nt on nt.id = cfe.nomeTabela
set p.empresa = 3
where p.empresa = 1
and nt.id in (37,38,40,41,42,43,44,45,46,47,48,51,52,54,94,95,100);

alter table historicosinistro modify column tipoPagamento char(1) null;