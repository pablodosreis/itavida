/*acrescentados campos ao histórico de conjuge*/
alter table historicoseguradoconjuge add column capital double null;
alter table historicoseguradoconjuge add column premio double null;

/*acrescentados campos ao histórico de capital (segurado)*/
alter table historicocapital add column email varchar(255) null;
alter table historicocapital add column sexo char(1) null;
alter table historicocapital add column dataNascimento date null;
alter table historicocapital add column matricula varchar(20) null;
alter table historicocapital add column cpf varchar(20) null;
alter table historicocapital add column rg varchar(20) null;
alter table historicocapital add column estadocivil char(2) null;
