
create table orgao_quitacao(
	id int not null auto_increment,
	data_quitacao date not null,
	orgao_id int(10) unsigned not null,
	quitado tinyint,
	CONSTRAINT orgao_quitacao_par UNIQUE (orgao_id,data_quitacao),
	PRIMARY KEY  (`id`)
);