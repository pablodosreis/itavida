-- tabela geral de movimentacao
CREATE TABLE log_acesso (
  id INT NOT NULL AUTO_INCREMENT,
  tipo varchar(15) not null,
  observacao varchar(255) not null,
  data datetime not null,
  PRIMARY KEY (`id`));