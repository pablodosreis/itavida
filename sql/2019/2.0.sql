-- tabela geral de movimentacao
CREATE TABLE movimentacao_financeira (
  id INT NOT NULL AUTO_INCREMENT,
  proposta_id int(10) unsigned NOT NULL,
  referencia date not null,
  data_pagamento date null,
  valor_pago double not null,
  numero_parcela int null,
  situacao varchar(15) not null,
  boleto_id int null,
  data_comissao date null,
  valor_comissao double null,
  empresa_id int(10) unsigned NOT NULL,
  orgao_id int(10) unsigned NOT NULL,
  idade int NULL, 
  sexo varchar(15) null,
  valor_premio double null,
  valor_premio_conjuge double null,
  valor_capital double null,
  valor_capital_conjuge double null,
  PRIMARY KEY (`id`));
  
  
ALTER TABLE movimentacao_financeira 
ADD CONSTRAINT `fk_movfinanceira_proposta`
  FOREIGN KEY (`proposta_id`)
  REFERENCES `proposta` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE movimentacao_financeira 
ADD CONSTRAINT `fk_movfinanceira_boleto`
  FOREIGN KEY (`boleto_id`)
  REFERENCES `boleto` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE movimentacao_financeira 
ADD CONSTRAINT `fk_movfinanceira_empresa`
  FOREIGN KEY (`empresa_id`)
  REFERENCES `empresa` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE movimentacao_financeira 
ADD CONSTRAINT `fk_movfinanceira_orgao`
  FOREIGN KEY (`orgao_id`)
  REFERENCES `orgao` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  

  
 alter table movimentacao_financeira modify column situacao varchar(40) ;
 alter table movimentacao_financeira add column observacao text ;
 alter table movimentacao_financeira add column situacao_boleto varchar(40) ;
 alter table movimentacao_financeira add column data_previsao_boleto date;
 
 alter table superap add column data_envio_certificado date;
 alter table superap add column certificado varchar(50);