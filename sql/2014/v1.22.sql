alter table historicosinistro add column empresa int(10) unsigned null;
alter table historicosinistro add constraint fk_historicosinistro_empresa
	foreign key (empresa) references empresa (id);