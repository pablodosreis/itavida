alter table proposta add column profissaoSegurado varchar(100);
alter table proposta add column profissaoConjuge varchar(100);
alter table historicocapital add column profissaoSegurado varchar(100);
alter table historicoseguradoconjuge add column profissaoConjuge varchar(100);
alter table item_mensal add column item_pago TINYINT(1) DEFAULT 1;
alter table item_mensal ADD CONSTRAINT im_unique UNIQUE (arquivo_mensal_id,proposta_id,referencia); 