-- tabela de planos do super ap
CREATE TABLE `plano_superap` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `morteAcidental` DOUBLE NOT NULL,
  `morteInvalidezAcidente` DOUBLE NOT NULL,
  `safIndividual` DOUBLE NOT NULL,
  `assistencia` VARCHAR(255) NOT NULL,
  `valorSorteio` DOUBLE NOT NULL,
  `premioMensal` DOUBLE NOT NULL,
  `premioAnual` DOUBLE NOT NULL,
  PRIMARY KEY (`id`));

-- tabela cobertura do super ap  
  CREATE TABLE `cobertura_superap` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `capitalSegurado` DOUBLE NOT NULL,
  `premioMensal` DOUBLE NOT NULL,
  `premioAnual` DOUBLE NOT NULL,
  PRIMARY KEY (`id`));
  
  
-- tabela super ap
  CREATE TABLE `superap` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `numero` VARCHAR(45) NULL,
  `modulo` INT NOT NULL,
  `renda` INT NULL,
  `pessoaPoliticamenteExposta` BIT NOT NULL DEFAULT 0,
  `plano_id` INT NOT NULL,
  `cobertura1_id` INT NULL,
  `cobertura2_id` INT NULL,
  `diaVencimento` INT NOT NULL,
  `periodicidade` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

  
  ALTER TABLE `superap` 
ADD INDEX `fkplanosuperap_idx` (`plano_id` ASC),
ADD INDEX `fkcobertura1superap_idx` (`cobertura1_id` ASC),
ADD INDEX `fkcobertura2superap_idx` (`cobertura2_id` ASC);
ALTER TABLE `superap` 
ADD CONSTRAINT `fkplanosuperap`
  FOREIGN KEY (`plano_id`)
  REFERENCES `plano_superap` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fkcobertura1superap`
  FOREIGN KEY (`cobertura1_id`)
  REFERENCES `cobertura_superap` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fkcobertura2superap`
  FOREIGN KEY (`cobertura2_id`)
  REFERENCES `cobertura_superap` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  

  

 ALTER TABLE `proposta` 
ADD COLUMN `superap_id` INT NULL AFTER `numeroEnderecoSegurado`,
ADD INDEX `fk_superapproposta_idx` (`superap_id` ASC);
ALTER TABLE`proposta` 
ADD CONSTRAINT `fk_superapproposta`
  FOREIGN KEY (`superap_id`)
  REFERENCES `superap` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
  
  ALTER TABLE `superap` 
ADD COLUMN `beneficiario_id` INT(10) NULL AFTER `periodicidade`;

ALTER TABLE `plano_superap` 
ADD COLUMN `descricao` varchar(255) NULL;


INSERT INTO `plano_superap` (`morteAcidental`, `morteInvalidezAcidente`, `safIndividual`, `assistencia`, `valorSorteio`, `premioMensal`, `premioAnual`, `descricao`) VALUES ('25000', '25000', '3300', 'Desconto farm�cia', '25000', '10', '70', '25 mil');
INSERT INTO `plano_superap` (`morteAcidental`, `morteInvalidezAcidente`, `safIndividual`, `assistencia`, `valorSorteio`, `premioMensal`, `premioAnual`, `descricao`) VALUES ('50000', '50000', '3300', 'Desconto farm�cia', '50000', '13', '100', '50 mil');
INSERT INTO `plano_superap` (`morteAcidental`, `morteInvalidezAcidente`, `safIndividual`, `assistencia`, `valorSorteio`, `premioMensal`, `premioAnual`, `descricao`) VALUES ('10000', '10000', '3300', 'Desconto farm�cia', '100000', '20', '180', '100 mil');
INSERT INTO `plano_superap` (`morteAcidental`, `morteInvalidezAcidente`, `safIndividual`, `assistencia`, `valorSorteio`, `premioMensal`, `premioAnual`, `descricao`) VALUES ('150000', '150000', '3300', 'Desconto farm�cia', '150000', '25', '250', '150 mil');
INSERT INTO `plano_superap` (`morteAcidental`, `morteInvalidezAcidente`, `safIndividual`, `assistencia`, `valorSorteio`, `premioMensal`, `premioAnual`, `descricao`) VALUES ('200000', '200000', '3300', 'Desconto farm�cia', '200000', '35', '300', '200 mil');

INSERT INTO `orgao` (`id`, `nomeOrgao`, `siglaOrgao`, `tipoOrgao`, `cgc`, `endereco`, `cidade`, `cep`, `telefone`, `fax`, `valorTaxa`, `valorProlabore`, `homePage`, `bairro`, `email`) VALUES ('0', 'SUPER AP', 'SUPER AP', 'P', '0', '-', '1', '00000000', '0', '0', '0', '0', '-', '-', '-');

INSERT INTO `cobertura_superap` (`descricao`, `capitalSegurado`, `premioMensal`, `premioAnual`) VALUES ('Despesas m�dicas, hospitalares e odontol�gicas', '500', '3', '32');
INSERT INTO `cobertura_superap` (`descricao`, `capitalSegurado`, `premioMensal`, `premioAnual`) VALUES ('Fratura �ssea', '1000', '6.60', '70');

ALTER TABLE `cobertura_superap` 
modify COLUMN `descricao` varchar(255) NULL;

-- Inclu�do em 27/10/2018 �s 18:27

CREATE TABLE `banco` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(3) NOT NULL,
  `nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `codigo_UNIQUE` (`codigo` ASC));

insert into banco(codigo,nome) values ('237', 'Bradesco');
insert into banco(codigo,nome) values ('041', 'Banrisul');
insert into banco(codigo,nome) values ('104', 'Caixa Econ�mica Federal');
insert into banco(codigo,nome) values ('756', 'Bancoob');
insert into banco(codigo,nome) values ('399', 'HSBC');
insert into banco(codigo,nome) values ('033', 'Santander');
insert into banco(codigo,nome) values ('341', 'Ita�');
insert into banco(codigo,nome) values ('745', 'Citibank');


CREATE TABLE `formapagamento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipo` INT NOT NULL,
  `cartao_credito` INT NULL,
  `banco_id` INT NULL,
  `nome_titular` VARCHAR(100) NULL,
  `cpf_titular` VARCHAR(11) NULL,
  `numero_agencia` VARCHAR(15) NULL,
  `numero_conta` VARCHAR(15) NULL,
  `numero_cartao` VARCHAR(45) NULL,
  `validade_cartao` VARCHAR(10) NULL,
  `parentesco` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_pagamento_banco_idx` (`banco_id` ASC),
  CONSTRAINT `FK_pagamento_banco`
    FOREIGN KEY (`banco_id`)
    REFERENCES `banco` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);

    
    CREATE TABLE `historicopagamento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `proposta` INT(10) UNSIGNED NOT NULL,
  `tipo` VARCHAR(45) NULL,
  `data_referencia` DATE NOT NULL,
  `data_pagamento` DATE NOT NULL,
  `valor_premio` DOUBLE NOT NULL,
  `valor_premio_conjuge` DOUBLE NOT NULL DEFAULT 0,
  `valor_capital` DOUBLE NOT NULL,
  `valor_capital_conjuge` DOUBLE NOT NULL DEFAULT 0,
  `data_cadastro` DATETIME NULL,
  `data_alteracao` DATETIME NULL,
  `usuario_cadastro` VARCHAR(100) NULL,
  `usuario_alteracao` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pagamento_proposta_idx` (`proposta` ASC),
  CONSTRAINT `fk_pagamento_proposta`
    FOREIGN KEY (`proposta`)
    REFERENCES `proposta` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);
    
    
ALTER TABLE `proposta` 
ADD COLUMN `forma_pagamento_id` INT NULL AFTER `superap_id`,
ADD INDEX `fk_proposta_forma_pagamento_idx` (`forma_pagamento_id` ASC);
ALTER TABLE `proposta` 
ADD CONSTRAINT `fk_proposta_forma_pagamento`
  FOREIGN KEY (`forma_pagamento_id`)
  REFERENCES `formapagamento` (`id`)
  ON DELETE  RESTRICT
  ON UPDATE  RESTRICT;
