
CREATE TABLE `movimentacao_proposta_inadimplente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `proposta_id` int(10) unsigned NOT NULL,
  `referencia` DATE NOT NULL,
  `premio` DOUBLE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE(`proposta_id`, `referencia`),
  KEY `FK_MovPropostaInadimplente01_idx` (`proposta_id`),
  CONSTRAINT `FK_MovPropostaInadimplente01` FOREIGN KEY (`proposta_id`) REFERENCES `proposta` (`id`)
) ENGINE=InnoDB;



CREATE TABLE `movimentacao_proposta_atrasada` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `proposta_id` int(10) unsigned NOT NULL,
  `referencia` DATE NOT NULL,
  `premio` DOUBLE NOT NULL,
  `periodo_quitacao_anterior` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE(`proposta_id`, `referencia`, `periodo_quitacao_anterior`),
  KEY `FK_MovPropostaAtrasada01_idx` (`proposta_id`),
  CONSTRAINT `FK_MovPropostaAtrasada01` FOREIGN KEY (`proposta_id`) REFERENCES `proposta` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `movimentacao_orgao_inadimplente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `orgao_id` int(10) unsigned NOT NULL,
  `referencia` DATE NOT NULL,
  `premio` DOUBLE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE(`orgao_id`, `referencia`),
  KEY `FK_MovOrgaoInadimplente01_idx` (`orgao_id`),
  CONSTRAINT `FK_MovOrgaoInadimplente01` FOREIGN KEY (`orgao_id`) REFERENCES `orgao` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `movimentacao_orgao_atrasado` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `orgao_id` int(10) unsigned NOT NULL,
  `referencia` DATE NOT NULL,
  `premio` DOUBLE NOT NULL,
  `periodo_quitacao_anterior` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE(`orgao_id`, `referencia`, `periodo_quitacao_anterior`),
  KEY `FK_MovPropostaOrgao01_idx` (`orgao_id`),
  CONSTRAINT `FK_MovPropostaOrgao01` FOREIGN KEY (`orgao_id`) REFERENCES `orgao` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `arquivo_mensal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `competencia` DATE NOT NULL,
  `data_geracao` DATETIME NOT NULL,
  `usuario` varchar(100) NULL,
  `ativo` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `item_mensal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `data_ultima_atualizacao` DATETIME NULL,
  `usuario_ultima_atualizacao` varchar(100) NULL,
  `referencia` DATE NOT NULL,
  `nome_orgao` varchar(100) NULL,
  `nome_segurado` varchar(100) NULL,
  `data_nascimento_segurado` DATE NULL,
  `cpf_segurado` varchar(20) NULL,
  `premio_segurado` double NULL,
  `capital_segurado` double NULL,
  `descricao_plano` varchar(50) NULL,
  
  `nome_conjuge` varchar(100) NULL,
  `data_nascimento_conjuge` DATE NULL,
  `cpf_conjuge` varchar(20) NULL,
  `premio_conjuge` double NULL,
  `capital_conjuge` double NULL,
  
  `endereco` varchar(255) NULL,
  `bairro` varchar(100) NULL,
  `cidade` varchar(100) NULL,
  `uf` varchar(2) NULL,
  `cep` varchar(20) NULL,
  
  `tipo_proposta_id` int(10) unsigned NOT NULL,
  `proposta_id` int(10) unsigned NOT NULL,
  `orgao_id` int(10) unsigned NOT NULL,
  `empresa_id` int(10) unsigned NOT NULL,
  `arquivo_mensal_id` INT NOT NULL,
  
  PRIMARY KEY (`id`),
  UNIQUE(`referencia`, `arquivo_mensal_id`, `proposta_id`),
  KEY `FK_ItemMensalTipoProposta_idx` (`tipo_proposta_id`),
  KEY `FK_ItemMensalProposta_idx` (`proposta_id`),
  KEY `FK_ItemMensalOrgao_idx` (`orgao_id`),
  KEY `FK_ItemMensalEmpresa_idx` (`empresa_id`),
  KEY `FK_ItemMensalArquivo_idx` (`arquivo_mensal_id`),
  CONSTRAINT `FK_ItemMensalTipoProposta` FOREIGN KEY (`tipo_proposta_id`) REFERENCES `tipoproposta` (`id`),
  CONSTRAINT `FK_ItemMensalProposta` FOREIGN KEY (`proposta_id`) REFERENCES `proposta` (`id`),
  CONSTRAINT `FK_ItemMensalOrgao` FOREIGN KEY (`orgao_id`) REFERENCES `orgao` (`id`),
  CONSTRAINT `FK_ItemMensalEmpresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`),
  CONSTRAINT `FK_ItemMensalArquivo` FOREIGN KEY (`arquivo_mensal_id`) REFERENCES `arquivo_mensal` (`id`)
) ENGINE=InnoDB;